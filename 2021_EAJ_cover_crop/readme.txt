 A trait-based analysis to assess the ability of cover crops to control weeds in a tropical island. Christina Mathias, Negrier Adrien, Marnotte Pascal, Viaud Pauline, Mansuy Alizé, Auzoux Sandrine, Técher Patrick, Hoarau Emmanuel, Chabanne André. 2021. European Journal of Agronomy, 128:126316, 10 p.
https://doi.org/10.1016/j.eja.2021.126316
