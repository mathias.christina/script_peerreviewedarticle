require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")

setwd(set1)

DTL11 <- read.table('1_watsoilt.dat')
DTL12 <- read.table('2_watsoilt.dat')
DTL13 <- read.table('3_watsoilt (2).dat')
DTL21 <- read.table('4_watsoilt.dat')
DTL22 <- read.table('5_watsoilt.dat')
DTL23 <- read.table('6_watsoilt (2).dat')
DTL31 <- read.table('7_watsoilt.dat')
DTL32 <- read.table('9_watsoilt.dat')
DTL33 <- read.table('9_watsoilt (2).dat')
DTL41 <- read.table('10_watsoilt.dat')
DTL42 <- read.table('11_watsoilt.dat')
DTL43 <- read.table('12_watsoilt (2).dat')
dat131 <- read.table('1_watbal.dat',skip=37)
dat132 <- read.table('2_watbal.dat',skip=37)
dat133 <- read.table('3_watbal (2).dat',skip=37)
dat231 <- read.table('4_watbal.dat',skip=37)
dat232 <- read.table('5_watbal.dat',skip=37)
dat233 <- read.table('6_watbal (2).dat',skip=37)
dat331 <- read.table('7_watbal.dat',skip=37)
dat332 <- read.table('8_watbal.dat',skip=37)
dat333 <- read.table('9_watbal (2).dat',skip=37)
dat431 <- read.table('10_watbal.dat',skip=37)
dat432 <- read.table('11_watbal.dat',skip=37)
dat433 <- read.table('12_watbal (2).dat',skip=37)
names(dat131) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat132) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat133) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat231) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat232) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat233) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat331) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat332) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat333) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat431) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat432) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
 names(dat433) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')


DTL1 <- rbind(DTL11,DTL12,DTL13)
DTL2 <- rbind(DTL21,DTL22,DTL23)
DTL3 <- rbind(DTL31,DTL32,DTL33)
DTL4 <- rbind(DTL41,DTL42,DTL43)

TL1 <- rep(NA,17)
TL2 <- rep(NA,17)
TL3 <- rep(NA,17)
for (i in 1:1096){
x1 <-c(); for (j in 1:17){
x1 <- c(x1,mean(DTL1[((i-1)*48+1):(i*48),j]))}
x2 <-c(); for (j in 1:17){
x2 <- c(x2,mean(DTL2[((i-1)*48+1):(i*48),j]))}
x3 <-c(); for (j in 1:17){
x3 <- c(x3,mean(DTL3[((i-1)*48+1):(i*48),j]))}
TL1 <- rbind(TL1,x1)
TL2 <- rbind(TL2,x2)
TL3 <- rbind(TL3,x3)
}
TL1 <- TL1[2:1097,]
TL2 <- TL2[2:1097,]
TL3 <- TL3[2:1097,]

setwd(set2)
Temp <- read.table('Temp2010to2012.txt',header=T)
setwd(set1)

par(mar=c(4.5,4.5,1,1))
plot(DTL3[,2][((i-1)*48+1):(i*48)],type='l',lwd=2,col=2,xlab='Hour of the Day',ylab='Soil surface Temperature (�C)',cex.lab=1.2)
points(Temp$T1[((i-1)*48+1):(i*48)],type='l',col=1,lwd=2)
points(B$TAIR[((i-1)*48+1):(i*48)],type='l',col=3,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Measured T 1cm','Simulated T 1st layer','Air temperature'),lty=1,pch=NA,col=1:3,bg='white',box.col=0,cex=1.2);box()


TMeas <- rep(NA,13)
for (i in 1:1096){
x <-c()
for (j in 1:13){
x <- c(x,mean(Temp[((i-1)*48+1):(i*48),3+j]))
}
TMeas <- rbind(TMeas,x)
}
TMeas <- TMeas[2:1097,]

DY<-c();for (i in 1:1096){DY<-c(DY,rep(i,48))}
HR <- rep(c(1:48),1096)

Pr <- c('Liti�re','1 cm','2 cm','4 cm','8 cm','16 cm','32 cmm','64 cm')

par(mar=c(4,4.5,2,1))
par(mfrow=c(3,1))
plot((TMeas[,1]),ylim=c(10,32),ylab=expression('Simulated Temperature'),xlab='Day from Jan 2010 to Dec 2012',cex.lab=1,type='p',pch=16)
points(TL1[,2],col=2)
legend('topright',legend=c('Simulated','Measured 1cm'),pch=c(1,16),col=2:1,bty='n')
title('Simulation 1.5-3cm')
plot(TMeas[,5],ylim=c(10,32),ylab=expression('Simulated Temperature'),xlab='Day from Jan 2010 to Sept 2012',cex.lab=1,type='p',pch=16)
points(TL1[,3],col=2)
legend('topright',legend=c('Simulated','measured 16cm'),pch=c(1,16),col=2:1,bty='n')
title('Simulation 3-35 cm')
plot((TMeas[,7]),ylim=c(10,32),ylab=expression('Simulated Temperature'),xlab='Day from Jan 2010 to Sept 2012',cex.lab=1,type='p',pch=16)
points(TL1[,4],col=2)
legend('topright',legend=c('Simulated','Measured 64cm'),pch=c(1,16),col=2:1,bty='n')
title('Simulation 35-65 cm')

TM <- c(TMeas[,1],TMeas[,5],TMeas[,7])
TS1 <- c(TL1[,2],TL1[,3],TL1[,4])
TS2 <- c(TL2[,2],TL2[,3],TL2[,4])
TS3 <- c(TL3[,2],TL3[,3],TL3[,4])

BIA1 <- (TS1-TM)/TM
BIA1 <- sum(BIA1,na.rm=T)/(length(BIA1[!is.na(BIA1)])-1)
BIA2 <- (TS2-TM)/TM
BIA2 <- sum(BIA2,na.rm=T)/(length(BIA2[!is.na(BIA2)])-1)
BIA3 <- (TS3-TM)/TM
BIA3 <- sum(BIA3,na.rm=T)/(length(BIA3[!is.na(BIA3)])-1)
SCR1 <- (TS1 - TM)^2
SCR2 <- (TS2 - TM)^2
SCR3 <- (TS3 - TM)^2
RMSE1 <- sqrt(sum(SCR1,na.rm=T)/(length(SCR1[!is.na(SCR1)])-1))
RMSE2 <- sqrt(sum(SCR2,na.rm=T)/(length(SCR2[!is.na(SCR2)])-1))
RMSE3 <- sqrt(sum(SCR3,na.rm=T)/(length(SCR3[!is.na(SCR3)])-1))

ft1 <- lm(TM~TS1-1)
ft2 <- lm(TM~TS2-1)
ft3 <- lm(TM~TS3-1)

par(mar=c(4.5,4.5,0.2,0.2))
par(yaxs='i',xaxs='i')
plot(TS2,TM,xlab='Simulated soil T (�C)',ylab='Measured soil T (�C)',cex.lab=1.2,ylim=c(10,30),xlim=c(10,30))
abline(ft1,col=4,lwd=2)
grid(nx=NULL,ny=NULL,col=1)
legend('topleft',legend=c(paste('RMSE = ',round(RMSE1,5)),paste('Mean Biai = ',round(BIA1,5))),pch=NA,box.col=0,bg='white',cex=1)
legend('bottomright',legend=c('Regression curve','   Y = 1.01 * X','   R� = 0.996'),lty=c(1,1,NA,NA),pch=NA,col=c(2,4),bg='white',box.col=0,cex=1)
box()


# validation flux
#dat <- read.table("flux2010.txt",header=T)
dat <- read.table('Hflux_bon.txt',header=T)
attach(dat)
DATE2 <- DATE - min(DATE)+1

setwd(set1)
H1 <- readhrflux('4_hrflux.dat')
H2 <- readhrflux('5_hrflux.dat')
H3 <- readhrflux('6_hrflux.dat')

H2 <- readhrflux('2_hrflux.dat')
H3 <- readhrflux('3_hrflux.dat')
H4 <- readhrflux('4_hrflux.dat')

H1 <- read.table('1_hrflux.dat',skip=40)
#H2 <- read.table('4_hrflux.dat',skip=40)
H3 <- read.table('7_hrflux.dat',skip=40)
H4 <- read.table('10_hrflux.dat',skip=40)
names(H1) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H2) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H3) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H4) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')


PSIL1 <-c() ;
for (i in 1:(365*48)){
PSIL1 <- c(PSIL1, H1$PSIL[((i-1)*84+1):(i*84)][1])}
PSIL2 <-c()
for (i in 1:(366*48)){
PSIL2 <- c(PSIL2, H2$PSIL[((i-1)*84+1):(i*84)][1])}

plot(PSIL1[(i*48+1):(i*48+48)],ylab='Psi leaf (MPa)',type='l',lwd=2)


HT1 <-c();HT2 <- c(); HT3 <-c() ; HT4 <- c()
for (i in 1:365){
for (j in 1:48){
HT1 <- c(HT1, sum(H1$hrH[H1$DOY==i][H1$HOUR[H1$DOY==1]==j],na.rm=T)/481.790)
#HT2 <- c(HT2, sum(H2$hrH[H2$DOY==i][H2$HOUR[H2$DOY==1]==j],na.rm=T)/502.36)
HT3 <- c(HT3, sum(H3$hrH[H3$DOY==i][H3$HOUR[H3$DOY==1]==j],na.rm=T)/504.13)
HT4 <- c(HT4, sum(H4$hrH[H4$DOY==i][H4$HOUR[H4$DOY==1]==j],na.rm=T)/496.05)
}}
#HT1 en kJ / m� /s
H2 <- rep(NA,length(HT1))

HSIM1 <- -dat131$qh[1:dim(dat131)[1]] +HT1*1000                             
HSIM2 <- -dat231$qh[1:dim(dat231)[1]] +HT2*1000                             
HSIM3 <- -dat331$qh[1:dim(dat331)[1]] +HT3*1000
HSIM4 <- -dat431$qh[1:dim(dat431)[1]] +HT4*1000

SIMDAT <- data.frame(dat131$day[1:(304*48)],dat131$hour[1:(304*48)],HSIM1[1:(304*48)],HSIM2[1:(304*48)],HSIM3[1:(304*48)],HSIM4[1:(304*48)])
names(SIMDAT) <- c('DATE2','HOUR2','Hsim1','Hsim2','Hsim3','Hsim4')

HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
#OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
OBSDAT <- data.frame(DATE2,HOUR2,H)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))

obsLE <- FH$LE#replace(FH$H,c(FH$LE <=0),NA)
obsH <- FH$H

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(HOUR,LE,ylab='latent heat flux (W/m�)',cex.lab=1.2)
plot(HOUR,H,ylab='Sensible heat flux (W/m�)',cex.lab=1.2)


par(mfrow=c(4,5))
par(mar=c(3.5,4.5,2,1))
for (i in c(1:20)*15){
plot(FH$HOUR2[((i-1)*48+1):(i*48)],obsH[((i-1)*48+1):(i*48)],ylab='Flux (W/m�)',xlab='half-hour',type='l',lwd=2,col=2)
points(FH$HOUR2[((i-1)*48+1):(i*48)],obsLE[((i-1)*48+1):(i*48)],type='l',lwd=2,col=4)
title(paste('Day ',i,spe=""))
}


fit1<- lm(FH$Hsim1~obsH-1)
fit2<- lm(FH$Hsim2~obsH-1)
fit3<- lm(FH$Hsim3~obsH-1)
fit4<- lm(FH$Hsim4~obsH-1)

BIA1 <- (FH$Hsim1-obsH)/obsH ; BIA1 <- sum(BIA1,na.rm=T)/(length(BIA1[!is.na(BIA1)])-1)
BIA2 <- (FH$Hsim2-obsH)/obsH ; BIA2 <- sum(BIA2,na.rm=T)/(length(BIA2[!is.na(BIA2)])-1)
BIA3 <- (FH$Hsim3-obsH)/obsH ; BIA3 <- sum(BIA3,na.rm=T)/(length(BIA3[!is.na(BIA3)])-1)
BIA4 <- (FH$Hsim4-obsH)/obsH ; BIA4 <- sum(BIA4,na.rm=T)/(length(BIA4[!is.na(BIA4)])-1)
SCR1 <- (FH$Hsim1 - obsH)^2 ; RMSE1 <- sqrt(sum(SCR1,na.rm=T)/(length(SCR1[!is.na(SCR1)])-1))
SCR2 <- (FH$Hsim2 - obsH)^2 ; RMSE2 <- sqrt(sum(SCR2,na.rm=T)/(length(SCR2[!is.na(SCR1)])-1))
SCR3 <- (FH$Hsim3 - obsH)^2 ; RMSE3 <- sqrt(sum(SCR3,na.rm=T)/(length(SCR3[!is.na(SCR1)])-1))
SCR4 <- (FH$Hsim4 - obsH)^2 ; RMSE4 <- sqrt(sum(SCR4,na.rm=T)/(length(SCR4[!is.na(SCR1)])-1))

par(mar=c(4.5,4.5,1,1))
plot(rep(obsH,3),c(FH$Hsim1,FH$Hsim4,FH$Hsim3),xlab='Measured latent heat flux (W/m�)',ylab='Simulated latent heat flux (W/m�)',cex.lab=1.2)
abline(fit1,col=2,lwd=2)
abline(fit3,col=3,lwd=2)
abline(fit4,col=4,lwd=2)
grid(nx=NULL,ny=NULL,col=1)
legend('topleft',legend=c('Plot1: Y = 1.54 * X ; R� = 0.7731',
                           'Plot3: Y = 1.49 * X ; R� = 0.8245',
                           'Plot4: Y = 1.51 * X ; R� = 0.8239'),lty=1,col=2:4,bg='white',box.col=0,cex=1)
legend('bottomright',legend=c(paste('Plot1 : RMSE = ',round(RMSE1,2),' ; <Biai> = ',round(BIA1,2)),
                              paste('Plot3 : RMSE = ',round(RMSE3,2),' ; <Biai> = ',round(BIA3,2)),
                              paste('Plot4 : RMSE = ',round(RMSE4,2),' ; <Biai> = ',round(BIA4,2))),lty=1,col=2:4,bg='white',box.col=0,cex=1)
box()


mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31))
Hsim <- c(FH$Hsim1+FH$Hsim3+FH$Hsim4)/3    #+FH$Hsim2
#Hsim <- FH$Hsim1    #+FH$Hsim2
hour <- rep(1:48,304)

HES <-c() ; HEO <-c()
for (i in 1:10){
for (j in 1:48){
HES <- c(HES, mean(Hsim[mois==i][hour[mois==i]==j],na.rm=T))
HEO <- c(HEO, mean(obsH[mois==i][hour[mois==i]==j],na.rm=T))
}}

MOI <- c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.')

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(HEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Sensible heat flux (W/m�)',cex.lab=1.2,ylim=c(-100,350))
points(HES,type='l',col=2,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=-75,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=-75,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured latent heat flux','Simulated latent heat flux'),col=c(1,2),lty=1,pch=NA,bg='white',box.col=0)
box()


dy <-c() ; for (i in 1:365){ dy <- c(dy,rep(i,48))}

TOTH <-c()  ; totHobs<-c();TOTH2 <-c()  
TOTHM <-c()  ; totHobsM<-c();TOTH2M <-c()  
for (i in 1:304){
TOTH <- c(TOTH, sum(Hsim[dy==i],na.rm=T)*30*60*10^-6)
TOTH2 <- c(TOTH2, sum(c(dat11$totH[dat11$DOY==i],dat31$totH[dat31$DOY==i],dat41$totH[dat41$DOY==i])/3,na.rm=T)/482)
totHobs  <- c(totHobs, sum(FH$H[FH$DATE2==i],na.rm=T)*30*60*10^-6)
TOTHM <- c(TOTHM, mean(HSIM1[dy==i],na.rm=T))
TOTH2M <- c(TOTH2M, mean(dat11$totH[dat11$DOY==i],na.rm=T)/482)
totHobsM  <- c(totHobsM, mean(FH$H[FH$DATE2==i],na.rm=T))
}

par(mfrow=c(2,1))
par(mar=c(4.5,4.5,1,1))
plot(totHobs,pch=16,ylim=c(-2,15),ylab='Daily sensible heat flux (MJ/m�/d)',xlab='Day from Jan. to Oct. 2010',cex.lab=1.2)
points(TOTH,col=2,pch=16)
points(TOTH2,col=3)
legend('top',legend=c('Measured  SHF','Simulated SHF','Simulated SHF from trees only'),pch=c(16,16,1),col=1:3,cex=1.2)


plot(totHobsM,pch=16,ylim=c(-60,300),ylab='Daily mean sensible heat flux (J/m�/s)',cex.lab=1)
points(TOTHM,col=2,pch=16)


# evaporation fin 2009

setwd(set3)
DT <- read.table('evap_2009.txt',header=T)

LESIM1 <- c(dat131$evapstore[1:dim(dat131)[1]] +
            dat131$soilevap[1:dim(dat131)[1]]   +
            dat131$et[1:dim(dat131)[1]])
lesim <-c() ; for (i in 1:365){lesim <- c(lesim,sum(LESIM1[dy==i]))}
ESsim <-c() ; for (i in 1:365){ESsim <- c(ESsim,sum(dat131$soilevap[1:dim(dat131)[1]][dy==i]))}

par(mar=c(4.5,4.5,1,1))
plot(1:81,DT[,1],col=5,ylab='evapotranspiration (mm)',pch=16,xlim=c(0,365+81),ylim=c(0,4),xlab='Day from Oct 2009 to Dec 2010',cex.lab=1.2)
#points(82:(365+81),lesim,pch=16,col=4)
points(82:(365+81),ESsim,pch=16,col=2)
legend('topright',legend=c('Measured evapotranspiration','Simulated evapotranspiration','Simulated soil evaporation'),
      pch=16,col=c(5,4,2),bg='white',box.col=0,cex=1.2)
box()


HSIM1 LESIM1
DHS <-c() ; DLES <-c()
for (i in 1:365){
DHS <- c(DHS, sum(HSIM1[dy==i])*30*60)
DLES <- c(DLES, sum(LESIM1[dy==i])*30*60)
}



# sensible vs latent + RNET 5 premiers mois / 5 derniers mois

lesim <- c(FH$lesim1+FH$lesim3+FH$lesim4)/3
RNET <- c(dat131$rnet[1:(304*48)],dat231$rnet[1:(304*48)],dat331$rnet[1:(304*48)],dat431$rnet[1:(304*48)])/4

RNET1 <- c() ; LES1<-c();LEO1<-c();HES1<-c();HEO1<-c()
for (j in 1:48){
RNET1 <- c(RNET1, mean(RNET[mois==1|mois==2|mois==3|mois==4|mois==5][hour[mois==1|mois==2|mois==3|mois==4|mois==5]==j],na.rm=T))
LES1 <- c(LES1, mean(lesim[mois==1|mois==2|mois==3|mois==4|mois==5][hour[mois==1|mois==2|mois==3|mois==4|mois==5]==j],na.rm=T))
LEO1 <- c(LEO1, mean(obsle[mois==1|mois==2|mois==3|mois==4|mois==5][hour[mois==1|mois==2|mois==3|mois==4|mois==5]==j],na.rm=T))
HES1 <- c(HES1, mean(Hsim[mois==1|mois==2|mois==3|mois==4|mois==5][hour[mois==1|mois==2|mois==3|mois==4|mois==5]==j],na.rm=T))
HEO1 <- c(HEO1, mean(obsH[mois==1|mois==2|mois==3|mois==4|mois==5][hour[mois==1|mois==2|mois==3|mois==4|mois==5]==j],na.rm=T))
}
RNET2 <- c() ; LES2<-c();LEO2<-c();HES2<-c();HEO2<-c()
for (j in 1:48){
RNET2 <- c(RNET2, mean(RNET[mois==6|mois==7|mois==8|mois==9|mois==10][hour[mois==6|mois==7|mois==8|mois==9|mois==10]==j],na.rm=T))
LES2 <- c(LES2, mean(lesim[mois==6|mois==7|mois==8|mois==9|mois==10][hour[mois==6|mois==7|mois==8|mois==9|mois==10]==j],na.rm=T))
LEO2 <- c(LEO2, mean(obsle[mois==6|mois==7|mois==8|mois==9|mois==10][hour[mois==6|mois==7|mois==8|mois==9|mois==10]==j],na.rm=T))
HES2 <- c(HES2, mean(Hsim[mois==6|mois==7|mois==8|mois==9|mois==10][hour[mois==6|mois==7|mois==8|mois==9|mois==10]==j],na.rm=T))
HEO2 <- c(HEO2, mean(obsH[mois==6|mois==7|mois==8|mois==9|mois==10][hour[mois==6|mois==7|mois==8|mois==9|mois==10]==j],na.rm=T))
}
RNET1 <- c() ; LES1<-c();LEO1<-c();HES1<-c();HEO1<-c()
for (j in 1:48){
RNET1 <- c(RNET1, mean(RNET[mois==1|mois==2|mois==3][hour[mois==1|mois==2|mois==3]==j],na.rm=T))
LES1 <- c(LES1, mean(lesim[mois==1|mois==2|mois==3][hour[mois==1|mois==2|mois==3]==j],na.rm=T))
LEO1 <- c(LEO1, mean(obsle[mois==1|mois==2|mois==3][hour[mois==1|mois==2|mois==3]==j],na.rm=T))
HES1 <- c(HES1, mean(Hsim[mois==1|mois==2|mois==3][hour[mois==1|mois==2|mois==3]==j],na.rm=T))
HEO1 <- c(HEO1, mean(obsH[mois==1|mois==2|mois==3][hour[mois==1|mois==2|mois==3]==j],na.rm=T))
}
RNET2 <- c() ; LES2<-c();LEO2<-c();HES2<-c();HEO2<-c()
for (j in 1:48){
RNET2 <- c(RNET2, mean(RNET[mois==8|mois==9|mois==10][hour[mois==8|mois==9|mois==10]==j],na.rm=T))
LES2 <- c(LES2, mean(lesim[mois==8|mois==9|mois==10][hour[mois==8|mois==9|mois==10]==j],na.rm=T))
LEO2 <- c(LEO2, mean(obsle[mois==8|mois==9|mois==10][hour[mois==8|mois==9|mois==10]==j],na.rm=T))
HES2 <- c(HES2, mean(Hsim[mois==8|mois==9|mois==10][hour[mois==8|mois==9|mois==10]==j],na.rm=T))
HEO2 <- c(HEO2, mean(obsH[mois==8|mois==9|mois==10][hour[mois==8|mois==9|mois==10]==j],na.rm=T))
}

par(mar=c(4.5,4.5,3,1))
par(mfrow=c(1,2))
plot(1:48,LEO1,type='l',lwd=2,col=4,xlab='Hour of the day',ylab='Measured heat flux (W/m�)',cex.lab=1.2,ylim=c(-50,300))
points(1:48,HEO1,type='l',col=2,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Latent heat flux','Sensible heat flux'),pch=NA,lty=1,col=c(4,2),bg='white',box.col=0,cex=1.2)
box()
title('January to March 2010')
plot(1:48,LEO2,type='l',lwd=2,col=4,xlab='Hour of the day',ylab='Measured heat flux (W/m�)',cex.lab=1.2,ylim=c(-50,300))
points(1:48,HEO2,type='l',col=2,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Latent heat flux','Sensible heat flux'),pch=NA,lty=1,col=c(4,2),bg='white',box.col=0,cex=1.2)
title('August to October 2010')
box()

par(mar=c(4.5,4.5,3,1))
par(mfrow=c(1,2))
plot(1:48,LES1,type='l',lwd=2,col=4,xlab='Hour of the day',ylab='Simulted heat flux (W/m�)',cex.lab=1.2,ylim=c(-50,300))
points(1:48,HES1,type='l',col=2,lwd=2)
points(1:48,RNET1,type='l',col=3,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Latent heat flux','Sensible heat flux','Net radiation above canopy'),pch=NA,lty=1,col=c(4,2,3),bg='white',box.col=0,cex=1)
box()
title('January to March 2010')
plot(1:48,LES2,type='l',lwd=2,col=4,xlab='Hour of the day',ylab='Simulted heat flux (W/m�)',cex.lab=1.2,ylim=c(-50,300))
points(1:48,HES2,type='l',col=2,lwd=2)
points(1:48,RNET2,type='l',col=3,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Latent heat flux','Sensible heat flux','Net radiation above canopy'),pch=NA,lty=1,col=c(4,2,3),bg='white',box.col=0,cex=1)
title('August to October 2010')
box()



LEDsim <-c() ; for (i in 1:304){LEDsim <- c(LEDsim,sum(lesim[dy==i]))}
HDsim <-c() ; for (i in 1:304){HDsim <- c(HDsim,sum(Hsim[dy==i]))}
LEDobs <-c() ; for (i in 1:304){LEDobs <- c(LEDobs,sum(obsle[dy==i],na.rm=T))}
HDobs <-c() ; for (i in 1:304){HDobs <- c(HDobs,sum(obsH[dy==i],na.rm=T))}




