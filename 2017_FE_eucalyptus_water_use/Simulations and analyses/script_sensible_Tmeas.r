require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")

setwd(set1)

dat431 <- read.table('10_watbal (2).dat',skip=37)
names(dat431) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
H4 <- read.table('10_hrflux (2).dat',skip=40)
names(H4) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
dat41 <- read.table('10_Dayflx (2).dat',skip=27)
names(dat41) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")


HT4<-c()
for (i in 1:365){
for (j in 1:48){
HT4 <- c(HT4, sum(H4$hrH[H4$DOY==i][H4$HOUR[H4$DOY==1]==j],na.rm=T)/496.05)
}}
#HT1 en kJ / m� /s

HSIM4 <- -dat431$qh[1:dim(dat431)[1]] +HT4*1000

LESIM4 <- c(dat431$evapstore[1:dim(dat431)[1]]/(30*60)*2.45*10^6 +
            dat431$soilevap[1:dim(dat431)[1]]*2.45*10^6/(30*60)   +
            dat431$et[1:dim(dat431)[1]]*2.45*10^6/(30*60))



dat <- read.table("flux2010.txt",header=T)
attach(dat)
DATE2 <- DATE - min(DATE)+1

SIMDAT <- data.frame(dat131$day[1:(304*48)],dat131$hour[1:(304*48)],LESIM4[1:(304*48)],HSIM4[1:(304*48)])
names(SIMDAT) <- c('DATE2','HOUR2','lesim4','Hsim4')
HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))



mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31))
esim <- FH$lesim4
Hsim <- FH$Hsim4 
hour <- rep(1:48,304)

HES <-c() ; HEO <-c()
LES <-c() ; LEO <-c()
for (i in 1:10){
for (j in 1:48){
LES <- c(LES, mean(esim[mois==i][hour[mois==i]==j],na.rm=T))
MEO <- c(LEO, mean(obsle[mois==i][hour[mois==i]==j],na.rm=T))
HES <- c(HES, mean(Hsim[mois==i][hour[mois==i]==j],na.rm=T))
HEO <- c(HEO, mean(obsH[mois==i][hour[mois==i]==j],na.rm=T))
}}

MOI <- c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.')
