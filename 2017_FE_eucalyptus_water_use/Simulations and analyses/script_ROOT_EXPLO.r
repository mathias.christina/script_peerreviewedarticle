require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")
set11<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Test")

setwd(set11)

# plot 3 tout �a
dat11 <- read.table('1_Dayflx.dat')
dat12 <- read.table('2_Dayflx.dat')
dat21 <- read.table('4_Dayflx.dat')
dat22 <- read.table('5_Dayflx.dat')
dat31 <- read.table('7_Dayflx.dat')
dat32 <- read.table('8_Dayflx.dat')
dat41 <- read.table('10_Dayflx.dat')
dat42 <- read.table('11_Dayflx.dat')
setwd(set1)
dat13 <- read.table('3_Dayflx (3).dat',skip=27)
dat23 <- read.table('6_Dayflx (3).dat',skip=27)
dat33 <- read.table('9_Dayflx (3).dat',skip=27)
dat43 <- read.table('12_Dayflx (3).dat',skip=27);setwd(set11)
dat1 <- rbind(dat11,dat12,dat13)
dat2 <- rbind(dat21,dat22,dat23)
dat3 <- rbind(dat31,dat32,dat33)
dat4 <- rbind(dat41,dat42,dat43)

setwd(set11)
dat11 <- read.table('37_Dayflx.dat')
dat12 <- read.table('38_Dayflx.dat')
dat13 <- read.table('39_Dayflx.dat')[1:(972*84),]  # essayer avec ,fill=T
dat14 <- read.table('40_Dayflx.dat')[1:(970*84),]
dat21 <- read.table('42_Dayflx.dat',skip=27)
dat22 <- read.table('43_Dayflx.dat')[1:(907*84),]
dat23 <- read.table('44_Dayflx.dat')
dat25 <- read.table('46_Dayflx.dat')
dat32 <- read.table('48_Dayflx.dat')
dat33 <- read.table('49_Dayflx.dat')[1:(899*84),]
dat34 <- read.table('50_Dayflx.dat')[1:(880*84),]
dat35 <- read.table('51_Dayflx.dat')
dat41 <- read.table('52_Dayflx.dat')[1:(935*84),]
dat42 <- read.table('53_Dayflx.dat')[1:(927*84),]
dat44 <- read.table('55_Dayflx.dat')[1:(932*84),]
dat45 <- read.table('56_Dayflx.dat')

NAMES <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat11) <- NAMES ; names(dat12) <- NAMES ; names(dat13) <- NAMES; names(dat14) <- NAMES
names(dat21) <- NAMES ; names(dat22) <- NAMES  ;names(dat23) <- NAMES ;names(dat25) <- NAMES
names(dat32) <- NAMES; names(dat33) <- NAMES ;names(dat34) <- NAMES;names(dat35) <- NAMES
names(dat41) <- NAMES; names(dat42) <- NAMES ;names(dat44) <- NAMES;names(dat45) <- NAMES
names(dat1) <- NAMES; names(dat2) <- NAMES ;names(dat3) <- NAMES;names(dat4) <- NAMES

DY <- c() ; for (i in 1:1096){DY <- c(DY, rep(i, 84))}
dat1[,1] <- DY
dat2[,1] <- DY
dat3[,1] <- DY
dat4[,1] <- DY


TOTLE11<-c();TOTLE12<-c();TOTLE13<-c();TOTLE14<-c();TOTLE15<-c()
TOTLE21<-c();TOTLE22<-c();TOTLE23<-c();TOTLE24<-c();TOTLE25<-c()
TOTLE41<-c();TOTLE42<-c();TOTLE43<-c();TOTLE44<-c();TOTLE45<-c()
TOTLE1<-c();TOTLE2<-c();TOTLE3<-c();TOTLE4<-c()
for (i in 1:1096){
TOTLE11 <- c(TOTLE11,sum(dat11$totLE1[dat11$DOY==i])/481.790*18*10^-3)
TOTLE12 <- c(TOTLE12,sum(dat12$totLE1[dat12$DOY==i])/481.790*18*10^-3)
TOTLE13 <- c(TOTLE13,sum(dat13$totLE1[dat13$DOY==i])/481.790*18*10^-3)
TOTLE14 <- c(TOTLE14,sum(dat14$totLE1[dat14$DOY==i])/481.790*18*10^-3)
TOTLE21 <- c(TOTLE21,sum(dat21$totLE1[dat21$DOY==i])/502.360*18*10^-3)
TOTLE22 <- c(TOTLE22,sum(dat22$totLE1[dat22$DOY==i])/502.360*18*10^-3)
TOTLE23 <- c(TOTLE23,sum(dat23$totLE1[dat23$DOY==i])/502.360*18*10^-3)
TOTLE25 <- c(TOTLE25,sum(dat25$totLE1[dat25$DOY==i])/502.360*18*10^-3)
TOTLE41 <- c(TOTLE41,sum(dat41$totLE1[dat41$DOY==i])/496.050*18*10^-3)
TOTLE42 <- c(TOTLE42,sum(dat42$totLE1[dat42$DOY==i])/496.050*18*10^-3)
TOTLE44 <- c(TOTLE44,sum(dat44$totLE1[dat44$DOY==i])/496.050*18*10^-3)
TOTLE45 <- c(TOTLE45,sum(dat45$totLE1[dat45$DOY==i])/496.050*18*10^-3)
TOTLE1 <- c(TOTLE1,sum(dat1$totLE1[dat1$DOY==i])/481.790*18*10^-3)
TOTLE2 <- c(TOTLE2,sum(dat2$totLE1[dat2$DOY==i])/502.360*18*10^-3)
TOTLE4 <- c(TOTLE4,sum(dat4$totLE1[dat4$DOY==i])/496.050*18*10^-3)
}
totPs11<-c();totPs12<-c();totPs13<-c();totPs14<-c();totPs15<-c()
totPs21<-c();totPs22<-c();totPs23<-c();totPs24<-c();totPs25<-c()
totPs41<-c();totPs42<-c();totPs43<-c();totPs44<-c();totPs45<-c()
totPs1<-c();totPs2<-c();totPs3<-c();totPs4<-c()
for (i in 1:1096){
totPs11 <- c(totPs11,sum(dat11$totPs[dat11$DOY==i])/481.790*18*10^-3)
totPs12 <- c(totPs12,sum(dat12$totPs[dat12$DOY==i])/481.790*18*10^-3)
totPs13 <- c(totPs13,sum(dat13$totPs[dat13$DOY==i])/481.790*18*10^-3)
totPs14 <- c(totPs14,sum(dat14$totPs[dat14$DOY==i])/481.790*18*10^-3)
totPs21 <- c(totPs21,sum(dat21$totPs[dat21$DOY==i])/502.360*18*10^-3)
totPs22 <- c(totPs22,sum(dat22$totPs[dat22$DOY==i])/502.360*18*10^-3)
totPs23 <- c(totPs23,sum(dat23$totPs[dat23$DOY==i])/502.360*18*10^-3)
totPs25 <- c(totPs25,sum(dat25$totPs[dat25$DOY==i])/502.360*18*10^-3)
totPs41 <- c(totPs41,sum(dat41$totPs[dat41$DOY==i])/496.050*18*10^-3)
totPs42 <- c(totPs42,sum(dat42$totPs[dat42$DOY==i])/496.050*18*10^-3)
totPs44 <- c(totPs44,sum(dat44$totPs[dat44$DOY==i])/496.050*18*10^-3)
totPs45 <- c(totPs45,sum(dat45$totPs[dat45$DOY==i])/496.050*18*10^-3)
totPs1 <- c(totPs1,sum(dat1$totPs[dat1$DOY==i])/481.790*18*10^-3)
totPs2 <- c(totPs2,sum(dat2$totPs[dat2$DOY==i])/502.360*18*10^-3)
totPs4 <- c(totPs4,sum(dat4$totPs[dat4$DOY==i])/496.050*18*10^-3)
}

LE110 <- c(sum(TOTLE1[1:365]),sum(TOTLE11[1:365]),sum(TOTLE12[1:365]),sum(TOTLE13[1:365]),sum(TOTLE14[1:365]),NA)
LE210 <- c(sum(TOTLE2[1:365]),sum(TOTLE21[1:365]),sum(TOTLE22[1:365]),sum(TOTLE23[1:365]),NA,sum(TOTLE25[1:365]))
LE410 <- c(sum(TOTLE4[1:365]),sum(TOTLE41[1:365]),sum(TOTLE42[1:365]),NA,sum(TOTLE44[1:365]),sum(TOTLE45[1:365]))
LE1d10 <- c(sum(TOTLE1[181:272]),sum(TOTLE11[181:272]),sum(TOTLE12[181:272]),sum(TOTLE13[181:272]),sum(TOTLE14[181:272]),NA)
LE2d10 <- c(sum(TOTLE2[181:272]),sum(TOTLE21[181:272]),sum(TOTLE22[181:272]),sum(TOTLE23[181:272]),NA,sum(TOTLE25[181:272]))
LE4d10 <- c(sum(TOTLE4[181:272]),sum(TOTLE41[181:272]),sum(TOTLE42[181:272]),NA,sum(TOTLE44[181:272]),sum(TOTLE45[181:272]))
PS110 <- c(sum(totPs1[1:365],na.rm=T),sum(totPs11[1:365],na.rm=T),sum(totPs12[1:365],na.rm=T),sum(totPs13[1:365],na.rm=T),sum(totPs14[1:365],na.rm=T),NA)
PS210 <- c(sum(totPs2[1:365],na.rm=T),sum(totPs21[1:365],na.rm=T),sum(totPs22[1:365],na.rm=T),sum(totPs23[1:365],na.rm=T),NA,sum(totPs25[1:365],na.rm=T))
PS410 <- c(sum(totPs4[1:365],na.rm=T),sum(totPs41[1:365],na.rm=T),sum(totPs42[1:365],na.rm=T),NA,sum(totPs44[1:365],na.rm=T),sum(totPs45[1:365],na.rm=T))
PS1d10 <- c(sum(totPs1[181:272],na.rm=T),sum(totPs11[181:272],na.rm=T),sum(totPs12[181:272],na.rm=T),sum(totPs13[181:272],na.rm=T),sum(totPs14[181:272],na.rm=T),NA)
PS2d10 <- c(sum(totPs2[181:272],na.rm=T),sum(totPs21[181:272],na.rm=T),sum(totPs22[181:272],na.rm=T),sum(totPs23[181:272],na.rm=T),NA,sum(totPs25[181:272],na.rm=T))
PS4d10 <- c(sum(totPs4[181:272],na.rm=T),sum(totPs41[181:272],na.rm=T),sum(totPs42[181:272],na.rm=T),NA,sum(totPs44[181:272],na.rm=T),sum(totPs45[181:272],na.rm=T))

LE111 <- c(sum(TOTLE1[366:730]),sum(TOTLE11[366:730]),sum(TOTLE12[366:730]),sum(TOTLE13[366:730]),sum(TOTLE14[366:730]),NA)
LE211 <- c(sum(TOTLE2[366:730]),sum(TOTLE21[366:730]),sum(TOTLE22[366:730]),sum(TOTLE23[366:730]),NA,sum(TOTLE25[366:730]))
LE411 <- c(sum(TOTLE4[366:730]),sum(TOTLE41[366:730]),sum(TOTLE42[366:730]),NA,sum(TOTLE44[366:730]),sum(TOTLE45[366:730]))
LE1d11 <- c(sum(TOTLE1[546:637]),sum(TOTLE11[546:637]),sum(TOTLE12[546:637]),sum(TOTLE13[546:637]),sum(TOTLE14[546:637]),NA)
LE2d11 <- c(sum(TOTLE2[546:637]),sum(TOTLE21[546:637]),sum(TOTLE22[546:637]),sum(TOTLE23[546:637]),NA,sum(TOTLE25[546:637]))
LE4d11 <- c(sum(TOTLE4[546:637]),sum(TOTLE41[546:637]),sum(TOTLE42[546:637]),NA,sum(TOTLE44[546:637]),sum(TOTLE45[546:637]))
PS111 <- c(sum(totPs1[366:730],na.rm=T),sum(totPs11[366:730],na.rm=T),sum(totPs12[366:730],na.rm=T),sum(totPs13[366:730],na.rm=T),sum(totPs14[366:730],na.rm=T),NA)
PS211 <- c(sum(totPs2[366:730],na.rm=T),sum(totPs21[366:730],na.rm=T),sum(totPs22[366:730],na.rm=T),sum(totPs23[366:730],na.rm=T),NA,sum(totPs25[366:730],na.rm=T))
PS411 <- c(sum(totPs4[366:730],na.rm=T),sum(totPs41[366:730],na.rm=T),sum(totPs42[366:730],na.rm=T),NA,sum(totPs44[366:730],na.rm=T),sum(totPs45[366:730],na.rm=T))
PS1d11 <- c(sum(totPs1[546:637],na.rm=T),sum(totPs11[546:637],na.rm=T),sum(totPs12[546:637],na.rm=T),sum(totPs13[546:637],na.rm=T),sum(totPs14[546:637],na.rm=T),NA)
PS2d11 <- c(sum(totPs2[546:637],na.rm=T),sum(totPs21[546:637],na.rm=T),sum(totPs22[546:637],na.rm=T),sum(totPs23[546:637],na.rm=T),NA,sum(totPs25[546:637],na.rm=T))
PS4d11 <- c(sum(totPs4[546:637],na.rm=T),sum(totPs41[546:637],na.rm=T),sum(totPs42[546:637],na.rm=T),NA,sum(totPs44[546:637],na.rm=T),sum(totPs45[546:637],na.rm=T))

LE112 <- c(sum(TOTLE1[731:1096]),sum(TOTLE11[731:1096]),sum(TOTLE12[731:1096]),sum(TOTLE13[731:1096]),sum(TOTLE14[731:1096]),NA)
LE212 <- c(sum(TOTLE2[731:1096]),sum(TOTLE21[731:1096]),sum(TOTLE22[731:1096]),sum(TOTLE23[731:1096]),NA,sum(TOTLE25[731:1096]))
LE412 <- c(sum(TOTLE4[731:1096]),sum(TOTLE41[731:1096]),sum(TOTLE42[731:1096]),NA,sum(TOTLE44[731:1096]),sum(TOTLE45[731:1096]))
LE1d12 <- c(sum(TOTLE1[911:1002]),sum(TOTLE11[911:1002]),sum(TOTLE12[911:1002]),sum(TOTLE13[911:1002]),sum(TOTLE14[911:1002]),NA)
LE2d12 <- c(sum(TOTLE2[911:1002]),sum(TOTLE21[911:1002]),sum(TOTLE22[911:1002]),sum(TOTLE23[911:1002]),NA,sum(TOTLE25[911:1002]))
LE4d12 <- c(sum(TOTLE4[911:1002]),sum(TOTLE41[911:1002]),sum(TOTLE42[911:1002]),NA,sum(TOTLE44[911:1002]),sum(TOTLE45[911:1002]))

PS112 <- c(sum(totPs1[731:1096],na.rm=T),sum(totPs11[731:1096],na.rm=T),sum(totPs12[731:1096],na.rm=T),sum(totPs13[731:1096],na.rm=T),sum(totPs14[731:1096],na.rm=T),NA)
PS212 <- c(sum(totPs2[731:1096],na.rm=T),sum(totPs21[731:1096],na.rm=T),sum(totPs22[731:1096],na.rm=T),sum(totPs23[731:1096],na.rm=T),NA,sum(totPs25[731:1096],na.rm=T))
PS412 <- c(sum(totPs4[731:1096],na.rm=T),sum(totPs41[731:1096],na.rm=T),sum(totPs42[731:1096],na.rm=T),NA,sum(totPs44[731:1096],na.rm=T),sum(totPs45[731:1096],na.rm=T))
PS1d12 <- c(sum(totPs1[911:1002],na.rm=T),sum(totPs11[911:1002],na.rm=T),sum(totPs12[911:1002],na.rm=T),sum(totPs13[911:1002],na.rm=T),sum(totPs14[911:1002],na.rm=T),NA)
PS2d12 <- c(sum(totPs2[911:1002],na.rm=T),sum(totPs21[911:1002],na.rm=T),sum(totPs22[911:1002],na.rm=T),sum(totPs23[911:1002],na.rm=T),NA,sum(totPs25[911:1002],na.rm=T))
PS4d12 <- c(sum(totPs4[911:1002],na.rm=T),sum(totPs41[911:1002],na.rm=T),sum(totPs42[911:1002],na.rm=T),NA,sum(totPs44[911:1002],na.rm=T),sum(totPs45[911:1002],na.rm=T))

LE10 <- c() ;LEd10 <- c()
PS10 <- c() ;PSd10 <- c()
LE11 <- c() ;LEd11 <- c()
PS11 <- c() ;PSd11 <- c()
LE12 <- c() ;LEd12 <- c()
PS12 <- c() ;PSd12 <- c()
for (i in 2:6){
LE10 <-c(LE10, mean(c(LE110[i]/LE110[1],LE210[i]/LE210[1],LE410[i]/LE410[1]),na.rm=T))
PS10 <-c(PS10, mean(c(PS110[i]/PS110[1],PS210[i]/PS210[1],PS410[i]/PS410[1]),na.rm=T))
LEd10 <-c(LEd10, mean(c(LE1d10[i]/LE1d10[1],LE2d10[i]/LE2d10[1],LE4d10[i]/LE4d10[1]),na.rm=T))
PSd10 <-c(PSd10, mean(c(PS1d10[i]/PS1d10[1],PS2d10[i]/PS2d10[1],PS4d10[i]/PS4d10[1]),na.rm=T))
LE11 <-c(LE11, mean(c(LE111[i]/LE111[1],LE211[i]/LE211[1],LE411[i]/LE411[1]),na.rm=T))
PS11 <-c(PS11, mean(c(PS111[i]/PS111[1],PS211[i]/PS211[1],PS411[i]/PS411[1]),na.rm=T))
LEd11 <-c(LEd11, mean(c(LE1d11[i]/LE1d11[1],LE2d11[i]/LE2d11[1],LE4d11[i]/LE4d11[1]),na.rm=T))
PSd11 <-c(PSd11, mean(c(PS1d11[i]/PS1d11[1],PS2d11[i]/PS2d11[1],PS4d11[i]/PS4d11[1]),na.rm=T))
LE12 <-c(LE12, mean(c(LE112[i]/LE112[1],LE212[i]/LE212[1],LE412[i]/LE412[1]),na.rm=T))
PS12 <-c(PS12, mean(c(PS112[i]/PS112[1],PS212[i]/PS212[1],PS412[i]/PS412[1]),na.rm=T))
LEd12 <-c(LEd12, mean(c(LE1d12[i]/LE1d12[1],LE2d12[i]/LE2d12[1],LE4d12[i]/LE4d12[1]),na.rm=T))
PSd12 <-c(PSd12, mean(c(PS1d12[i]/PS1d12[1],PS2d12[i]/PS2d12[1],PS4d12[i]/PS4d12[1]),na.rm=T))
}


par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(PS11,type='b',pch=16,lty=1,lwd=2,
      ylim=c(0,1),xlab='Root growth velocity decrease (%)',ylab='E (% of control)            A (% of control)',
      cex.lab=1.2,axes=F,xlim=c(0,6))
points(PSd11,type='b',pch=16,lty=2,lwd=2)
abline(h=0.5)
points(LE11-0.5,type='b',lty=1,pch=16,lwd=2)
points(LEd11-0.5,type='b',lty=2,pch=16,lwd=2)
axis(1,at=1:5,labels=(-1:-5)*10)
axis(2,at=(0:10)/10,labels=c(-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80))
legend(x=3.5,y=0.7,legend=c('Annually','Dry season'),pch=16,lty=1:2,bg='white',box.col=0,cex=1.2)
box()


# drainage
setwd(set1)
dat131 <- read.table('1_watbal (2).dat',skip=37)
dat132 <- read.table('2_watbal (2).dat',skip=37)
dat133 <- read.table('3_watbal (3).dat',skip=37)
dat231 <- read.table('4_watbal (2).dat',skip=37)
dat232 <- read.table('5_watbal (2).dat',skip=37)
dat233 <- read.table('6_watbal (3).dat',skip=37)
dat331 <- read.table('7_watbal (2).dat',skip=37)
dat332 <- read.table('8_watbal (2).dat',skip=37)
dat333 <- read.table('9_watbal (3).dat',skip=37)
dat431 <- read.table('10_watbal (3).dat',skip=37)
dat432 <- read.table('11_watbal (2).dat',skip=37)
dat433 <- read.table('12_watbal (3).dat',skip=37)
datW1 <- rbind(dat131,dat132,dat133)
datW2 <- rbind(dat231,dat232,dat233)
datW3 <- rbind(dat331,dat332,dat333)
datW4 <- rbind(dat431,dat432,dat433)

NAMES2 <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(datW1)<-NAMES2
names(datW2)<-NAMES2
names(datW3)<-NAMES2
names(datW4)<-NAMES2

setwd(set11)
datW11 <- read.table('37_watbal.dat',fill=T)
datW12 <- read.table('38_watbal.dat',fill=T)
datW13 <- read.table('39_watbal.dat',fill=T)
datW14 <- read.table('40_watbal.dat',fill=T)
datW21 <- read.table('42_watbal.dat',skip=37)
datW22 <- read.table('43_watbal.dat',fill=T)
datW23 <- read.table('44_watbal.dat',fill=T)
datW25 <- read.table('46_watbal.dat',fill=T)
datW32 <- read.table('48_watbal.dat',fill=T)
datW33 <- read.table('49_watbal.dat',fill=T)
datW34 <- read.table('50_watbal.dat',fill=T)
datW41 <- read.table('52_watbal.dat',fill=T)
datW42 <- read.table('53_watbal.dat',fill=T)
datW44 <- read.table('55_watbal.dat',fill=T)
datW35 <- read.table('51_watbal.dat',fill=T)
datW45 <- read.table('56_watbal.dat',fill=T)
names(datW11) <- NAMES2 ; names(datW12) <- NAMES2 ; names(datW13) <- NAMES2; names(datW14) <- NAMES2
names(datW21) <- NAMES2 ; names(datW22) <- NAMES2  ;names(datW23) <- NAMES2 ;names(datW25) <- NAMES2
names(datW32) <- NAMES2; names(datW33) <- NAMES2 ;names(datW34) <- NAMES2;names(datW35) <- NAMES2
names(datW41) <- NAMES2; names(datW42) <- NAMES2 ;names(datW44) <- NAMES2;names(datW45) <- NAMES2

DW110 <- c(sum(datW1$discharge[1:(365*48)]),sum(datW11$discharge[2:(365*48+1)]),sum(datW12$discharge[2:(365*48+1)]),sum(datW13$discharge[2:(365*48+1)]),sum(datW14$discharge[2:(365*48+1)]),NA)
DW210 <- c(sum(datW2$discharge[1:(365*48)]),sum(datW21$discharge[2:(365*48+1)]),sum(datW22$discharge[2:(365*48+1)]),sum(datW23$discharge[2:(365*48+1)]),NA,sum(datW25$discharge[2:(365*48+1)]))
DW410 <- c(sum(datW4$discharge[1:(365*48)]),sum(datW41$discharge[2:(365*48+1)]),sum(datW42$discharge[2:(365*48+1)]),NA,sum(datW44$discharge[2:(365*48+1)]),sum(datW44$discharge[2:(365*48+1)]))
DW111 <- c(sum(datW1$discharge[(365*48+1):(730*48)]),sum(datW11$discharge[(365*48+2):(730*48+1)]),sum(datW12$discharge[(365*48+2):(730*48+1)]),sum(datW13$discharge[(365*48+2):(730*48+1)]),sum(datW14$discharge[(365*48+2):(730*48+1)]),NA)
DW211 <- c(sum(datW2$discharge[(365*48+1):(730*48)]),sum(datW21$discharge[(365*48+2):(730*48+1)]),sum(datW22$discharge[(365*48+2):(730*48+1)]),sum(datW23$discharge[(365*48+2):(730*48+1)]),NA,sum(datW25$discharge[(365*48+2):(730*48+1)]))
DW411 <- c(sum(datW4$discharge[(365*48+1):(730*48)]),sum(datW41$discharge[(365*48+2):(730*48+1)]),sum(datW42$discharge[(365*48+2):(730*48+1)]),NA,sum(datW44$discharge[(365*48+2):(730*48+1)]),sum(datW44$discharge[(365*48+2):(730*48+1)]))

DW112 <- c(sum(datW1$discharge[(730*48+1):(43600)]),sum(datW11$discharge[(730*48+2):43601]),sum(datW12$discharge[(730*48+2):43601]),sum(datW13$discharge[(730*48+2):43601]),sum(datW14$discharge[(730*48+2):43601]),NA)
DW212 <- c(sum(datW2$discharge[(730*48+1):(43600)]),sum(datW21$discharge[(730*48+2):43601]),sum(datW22$discharge[(730*48+2):43601])/(43601-730*48+1),sum(datW23$discharge[(730*48+2):43902])/(43902-730*48+1),NA,sum(datW25$discharge[(730*48+2):43601]))
DW412 <- c(sum(datW4$discharge[(730*48+1):(43600)]),sum(datW41$discharge[(730*48+2):43601]),sum(datW42$discharge[(730*48+2):43601]),NA,sum(datW44$discharge[(730*48+2):43601]),sum(datW44$discharge[(730*48+2):43601]))

DW112 <- c(sum(datW1$discharge[(730*48+1):(44000)])/(44000-48*730),sum(datW11$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW12$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW13$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW14$discharge[(730*48+2):44000])/(44000-730*48+1),NA)
DW212 <- c(sum(datW2$discharge[(730*48+1):(44000)])/(44000-48*730),sum(datW21$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW22$discharge[(730*48+2):43601])/(43601-730*48+1),sum(datW23$discharge[(730*48+2):43902])/(43902-730*48+1),NA,sum(datW25$discharge[(730*48+2):44000])/(44000-730*48+1))
DW412 <- c(sum(datW4$discharge[(730*48+1):(44000)])/(44000-48*730),sum(datW41$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW42$discharge[(730*48+2):44000])/(44000-730*48+1),NA,sum(datW44$discharge[(730*48+2):44000])/(44000-730*48+1),sum(datW44$discharge[(730*48+2):44000])/(44000-730*48+1))

DW10<-c();DW11 <-c();DW12<-c()
for (i in 2:6){
DW10 <-c(DW10,mean(c(DW110[i]/DW110[1],DW210[i]/DW210[1],DW410[i]/DW410[1]),na.rm=T))
DW11 <-c(DW11,mean(c(DW111[i]/DW111[1],DW211[i]/DW211[1],DW411[i]/DW411[1]),na.rm=T))
DW12 <-c(DW12,mean(c(DW112[i]/DW112[1],DW212[i]/DW212[1],DW412[i]/DW412[1]),na.rm=T))
}


DW12 <-  c(1.05, DW12[2:5])

# un seul graph
par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(PS11,type='b',pch=16,lty=1,lwd=2,
      ylim=c(0.2,2),xlab='Root growth velocity decrease (%)',ylab='E and GPP (% of control)            Deep drainage (% of control)',
      cex.lab=1.2,axes=F,xlim=c(0,6))
points(PSd11,type='b',pch=16,lty=2,lwd=2)
points(LE11,type='b',lty=1,pch=4,lwd=2)
points(LEd11,type='b',lty=2,pch=4,lwd=2)
axis(1,at=1:5,labels=(-1:-5)*10)
axis(2,at=c(c(2:10)/10,c(10:20)/10+0.03),labels=c((8:0)*-10,(0:10)*10))
rect(0,1,6,1.03,col='grey',border=NA)
abline(h=1)
abline(h=1.03)
points(DW10+0.03,type='b',lwd=2,lty=1,pch=15)
points(DW11+0.03,type='b',lwd=2,lty=2,pch=15)
points(DW12+0.03,type='b',lwd=2,lty=3,pch=15)
legend('bottomleft',legend=c(expression(2^'nd'~year),'GPP, annually','GPP, dry season','E, annually','E, dry season'),pch=c(NA,16,16,4,4),lty=c(NA,1,2,1,2),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(expression('Drain,'~1^'st'~year),expression('Drain,'~2^'nd'~year),expression('Drain,'~3^'rd'~year)),pch=c(15),lty=c(1:3),bg='white',box.col=0,cex=1.2)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)


# 3 graphs
par(mfrow=c(1,3))
par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(PS10,type='b',pch=16,lty=1,lwd=2,
      ylim=c(0.2,2),xlab='Root growth velocity decrease (%)',ylab='Impact on E and A (%)            Impact on deep drainage (%)',
      cex.lab=1.2,axes=F,xlim=c(0,6))
points(PSd10,type='b',pch=16,lty=2,lwd=2)
points(LE10,type='b',lty=1,pch=4,lwd=2)
points(LEd10,type='b',lty=2,pch=4,lwd=2)
axis(1,at=1:5,labels=(-1:-5)*10)
axis(2,at=c(c(2:10)/10,c(10:20)/10+0.03),labels=c(2:10,10:20)/10,1)
rect(0,1,6,1.03,col='grey',border=NA)
abline(h=1)
abline(h=1.03)
points(DW10+0.03,type='b',lwd=2,lty=1,pch=15)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)
legend('bottomleft',legend=c('A, annually','A, dry season','E, annually','E, dry season'),pch=c(16,16,4,4),lty=c(1,2,1,2),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('1 year-old','Drain'),pch=c(NA,15),lty=c(NA,1),bg='white',box.col=0,cex=1.2)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)

plot(PS11,type='b',pch=16,lty=1,lwd=2,
      ylim=c(0.2,2),xlab='Root growth velocity decrease (%)',ylab='Impact on E and A (%)            Impact on deep drainage (%)',
      cex.lab=1.2,axes=F,xlim=c(0,6))
points(PSd11,type='b',pch=16,lty=2,lwd=2)
points(LE11,type='b',lty=1,pch=4,lwd=2)
points(LEd11,type='b',lty=2,pch=4,lwd=2)
axis(1,at=1:5,labels=(-1:-5)*10)
axis(2,at=c(c(2:10)/10,c(10:20)/10+0.03),labels=c(2:10,10:20)/10,1)
rect(0,1,6,1.03,col='grey',border=NA)
abline(h=1)
abline(h=1.03)
points(DW11+0.03,type='b',lwd=2,lty=1,pch=15)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)
legend('bottomleft',legend=c('A, annually','A, dry season','E, annually','E, dry season'),pch=c(16,16,4,4),lty=c(1,2,1,2),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('2 year-old','Drain'),pch=c(NA,15),lty=c(NA,1),bg='white',box.col=0,cex=1.2)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)

plot(PS12,type='b',pch=16,lty=1,lwd=2,
      ylim=c(0.2,2),xlab='Root growth velocity decrease (%)',ylab='Impact on E and A (%)            Impact on deep drainage (%)',
      cex.lab=1.2,axes=F,xlim=c(0,6))
points(PSd12,type='b',pch=16,lty=2,lwd=2)
points(LE12,type='b',lty=1,pch=4,lwd=2)
points(LEd12,type='b',lty=2,pch=4,lwd=2)
axis(1,at=1:5,labels=(-1:-5)*10)
axis(2,at=c(c(2:10)/10,c(10:20)/10+0.03),labels=c(2:10,10:20)/10,1)
rect(0,1,6,1.03,col='grey',border=NA)
points(DW12+0.03,type='b',lwd=2,lty=1,pch=15)
box()
rect(0,1,6,1.03,col='white',border='white')
legend('bottomleft',legend=c('A, annually','A, dry season','E, annually','E, dry season'),pch=c(16,16,4,4),lty=c(1,2,1,2),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('3 year-old','Drain'),pch=c(NA,15),lty=c(NA,1),bg='white',box.col=0,cex=1.2)
box()
rect(0,1,6,1.03,col='white',border='white')
abline(h=1.03)
abline(h=1)




# dry season
le1 <- c(sum(TOTLE1[181:272]),sum(TOTLE1[546:637]),sum(TOTLE1[912:1003]))
le2 <- c(sum(TOTLE2[181:272]),sum(TOTLE2[546:637]),sum(TOTLE2[912:1003]))
le3 <- c(sum(TOTLE3[181:272]),sum(TOTLE3[546:637]),sum(TOTLE3[912:1003]))
le4 <- c(sum(TOTLE4[181:272]),sum(TOTLE4[546:637]),sum(TOTLE4[912:1003]))
le5 <- c(sum(TOTLE5[181:272]),sum(TOTLE5[546:637]),sum(TOTLE5[912:1003]))

MAT1 <- data.frame(LE1,LE2,LE3,LE4,LE5)
MAT1 <- as.matrix(MAT1)
MAT2 <- data.frame(le1,le2,le3,le4,le5)
MAT2 <- as.matrix(MAT2)

barplot(MAT2/92,beside=T)

MAT <- data.frame(MAT1[,3:5]/MAT1[,1],MAT2[,3:5]/MAT2[,1])
MAT <- as.matrix(MAT)

Palette<-(colorRampPalette(c("lightblue","darkblue"),bias=1) )

par(mar=c(4.2,4.2,1,1))
par(xaxs='i',yaxs='i')
barplot(MAT,beside=T,names.arg=rep(c('-20%','-30%','-40%'),2),ylim=c(0,1.2),cex.lab=1.2,
        xlab='Sensitivity to root velocity growth',ylab='% Tree water loss',
        space=c(0,0.5),border=NA,axes=F,col=Palette(3),
        legend.text=c('2010','2011','2012'),args.legend=list(x='topleft',bg='white',box.col=0,cex=1.1))
#        space=data.frame(rep(0,6),c(0.2,0.5,0.5,1,0.5,0.5)))
abline(v=10.75)
abline(h=1,lty=2)
legend(x=8,y=1.2,legend='Annually',cex=1.2,pch=NA,bg='white',box.col=0)
legend(x=18,y=1.2,legend='Dry season',cex=1.2,pch=NA,bg='white',box.col=0)
axis(side=2,at=c(0:5)*0.2,labels=c(0:5)*20)
box()


NB1 <- c(365,365,366)
NB2 <- c(92,92,92)
EFF <- c(1,0.9,0.8,0.7,0.6)

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(MAT1[1,]/365,EFF,type='b',lwd=2,col=1,xlim=c(0,8),ylim=c(0.5,1.1))
points(MAT1[2,]/365,EFF,type='b',lwd=2,col=2)
points(MAT1[3,]/366,EFF,type='b',lwd=2,col=4)
abline(v=4)
points(MAT2[1,]/92+4,EFF,type='b',lwd=2,col=1,xlim=c(0,4))
points(MAT2[2,]/92+4,EFF,type='b',lwd=2,col=2)
points(MAT2[3,]/92+4,EFF,type='b',lwd=2,col=4)

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(MAT1[1,]/MAT1[1,1],EFF,type='b',lwd=2,col=1,xlim=c(0.5,1.5),ylim=c(0.5,1.1))
points(MAT1[2,]/MAT1[2,1],EFF,type='b',lwd=2,col=2)
points(MAT1[3,]/MAT1[3,1],EFF,type='b',lwd=2,col=4)
abline(v=4)
points(MAT2[1,]/MAT2[1,1]+0.5,EFF,type='b',lwd=2,col=1)
points(MAT2[2,]/MAT2[2,1]+0.5,EFF,type='b',lwd=2,col=2)
points(MAT2[3,]/MAT2[3,1]+0.5,EFF,type='b',lwd=2,col=4)


# figure A, A/E, GS

setwd(set11)

dat11 <- read.table('1_Dayflx.dat')
dat12 <- read.table('2_Dayflx.dat')
dat13 <- read.table('3_Dayflx.dat')
dat21 <- read.table('4_Dayflx.dat')
dat22 <- read.table('5_Dayflx.dat')
dat23 <- read.table('6_Dayflx.dat')
dat31 <- read.table('7_Dayflx.dat')
dat32 <- read.table('8_Dayflx.dat')
dat33 <- read.table('9_Dayflx.dat')
dat41 <- read.table('10_Dayflx.dat')
dat42 <- read.table('11_Dayflx.dat')
dat43 <- read.table('12_Dayflx.dat')
names(dat11) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat12) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat13) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat21) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat22) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat23) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat31) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat32) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat33) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat41) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat42) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat43) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

TOTLE1<-c();TOTLE2<-c();TOTLE3<-c();TOTLE4<-c()
for (i in 1:365){
TOTLE1 <- c(TOTLE1,sum(dat11$totLE1[dat11$DOY==i])/481.790*18*10^-3)
TOTLE2 <- c(TOTLE2,sum(dat21$totLE1[dat21$DOY==i])/502.360*18*10^-3)
TOTLE3 <- c(TOTLE3,sum(dat31$totLE1[dat31$DOY==i])/504.130*18*10^-3)
TOTLE4 <- c(TOTLE4,sum(dat41$totLE1[dat41$DOY==i])/496.050*18*10^-3)}
for (i in 1:365){
TOTLE1 <- c(TOTLE1,sum(dat12$totLE1[dat12$DOY==i])/481.790*18*10^-3)
TOTLE2 <- c(TOTLE2,sum(dat22$totLE1[dat22$DOY==i])/502.360*18*10^-3)
TOTLE3 <- c(TOTLE3,sum(dat32$totLE1[dat32$DOY==i])/504.130*18*10^-3)
TOTLE4 <- c(TOTLE4,sum(dat42$totLE1[dat42$DOY==i])/496.050*18*10^-3)}
for (i in 1:366){
TOTLE1 <- c(TOTLE1,sum(dat13$totLE1[dat13$DOY==i])/481.790*18*10^-3)
TOTLE2 <- c(TOTLE2,sum(dat23$totLE1[dat23$DOY==i])/502.360*18*10^-3)
TOTLE3 <- c(TOTLE3,sum(dat33$totLE1[dat33$DOY==i])/504.130*18*10^-3)
TOTLE4 <- c(TOTLE4,sum(dat43$totLE1[dat43$DOY==i])/496.050*18*10^-3)}
TOTPS1<-c();TOTPS2<-c();TOTPS3<-c();TOTPS4<-c()
for (i in 1:365){
TOTPS1 <- c(TOTPS1,sum(dat11$totPs[dat11$DOY==i],na.rm=T)/481.790*12)
TOTPS2 <- c(TOTPS2,sum(dat21$totPs[dat21$DOY==i],na.rm=T)/502.360*12)
TOTPS3 <- c(TOTPS3,sum(dat31$totPs[dat31$DOY==i],na.rm=T)/504.130*12)
TOTPS4 <- c(TOTPS4,sum(dat41$totPs[dat41$DOY==i],na.rm=T)/496.050*12)}
for (i in 1:365){
TOTPS1 <- c(TOTPS1,sum(dat12$totPs[dat12$DOY==i],na.rm=T)/481.790*12)
TOTPS2 <- c(TOTPS2,sum(dat22$totPs[dat22$DOY==i],na.rm=T)/502.360*12)
TOTPS3 <- c(TOTPS3,sum(dat32$totPs[dat32$DOY==i],na.rm=T)/504.130*12)
TOTPS4 <- c(TOTPS4,sum(dat42$totPs[dat42$DOY==i],na.rm=T)/496.050*12)}
for (i in 1:366){
TOTPS1 <- c(TOTPS1,sum(dat13$totPs[dat13$DOY==i],na.rm=T)/481.790*12)
TOTPS2 <- c(TOTPS2,sum(dat23$totPs[dat23$DOY==i],na.rm=T)/502.360*12)
TOTPS3 <- c(TOTPS3,sum(dat33$totPs[dat33$DOY==i],na.rm=T)/504.130*12)
TOTPS4 <- c(TOTPS4,sum(dat43$totPs[dat43$DOY==i],na.rm=T)/496.050*12)}

H11 <- read.table('4_hrflux.dat')
H12 <- read.table('5_hrflux.dat')
H13 <- read.table('6_hrflux.dat')
names(H11) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H12) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H13) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')

GS <- c(H11$Gscan[H11$HOUR==24],H12$Gscan[H12$HOUR==24],H13$Gscan[H13$HOUR==24])
PSIL <- c(H11$PSIL[H11$HOUR==24],H12$PSIL[H12$HOUR==24],H13$PSIL[H13$HOUR==24])
DAY <- c(H11$DOY[H11$HOUR==24],H12$DOY[H12$HOUR==24],H13$DOY[H13$HOUR==24])

GS2<-c();PSIL2<-c()
for (i in 1:1096){
GS2 <- c(GS2, mean(GS[((i-1)*84+1):(i*84)],na.rm=T))
PSIL2 <- c(PSIL2, mean(PSIL[((i-1)*84+1):(i*84)],na.rm=T))}

GS2 <- c(GS2[1:1095],3)

DA <- 1:1096
lisGS <- loess(GS2~DA,span=0.2,degree=2);DGS <- predict(lisGS,DA)
A<-(TOTPS1+TOTPS2+TOTPS3+TOTPS4)/4 ; A <- c(A[1:1095],A[1095])
lisPS <- loess(A~DA,span=0.2,degree=2);DPS <- predict(lisPS,DA)
AE<-(TOTPS1+TOTPS2+TOTPS3+TOTPS4)/(TOTLE1+TOTLE2+TOTLE3+TOTLE4) ; AE <- c(AE[1:1095],AE[1095])
lisAE <- loess(AE~DA,span=0.2,degree=2);DAE <- predict(lisAE,DA)

par(mfrow=c(3,1))
par(mar=c(0,4,1,1))
plot(A,axes=F,ann=F);box()
points(DA,DPS,col=4,type='l',lwd=2)
par(mar=c(0,4,0,1))
plot(GS2,axes=F,ann=F);box()
points(DA,DGS,col=4,type='l',lwd=2)
par(mar=c(4,4,0,1))
plot(AE,axes=F,ann=F);box()
points(DA,DAE,col=4,type='l',lwd=2)                                                    


Palette<-(colorRampPalette(c("white","darkblue"),bias=1) )
Palette<- (colorRampPalette(c("lightblue","darkblue"),bias=1) )

par(mfrow=c(2,1))
plot(DA,A)
image(kde2d(DA,A,h=c(75,width.SJ(A)),n=500,lims=c(0,1096,0,20)),col=Palette(12))

par(mfrow=c(3,1))
par(mar=c(0,4.5,1,1))
image(kde2d(DA,A,n=500,h=c(75,width.SJ(A)),lims=c(0,1096,0,20)),col=Palette(10),ann=F,axes=F)
axis(2,at=c(0:4)*5,labels=c(0:4)*5)
mtext('A (gC/m�/s)',side=2,line=3,cex=1)
box()
par(mar=c(0,4.5,0,1))
image(kde2d(DA,GS2,n=500,h=c(75,width.SJ(GS2)),lims=c(0,1096,0,6)),col=Palette(10),ann=F,axes=F)
axis(2,at=c(0:5),labels=c(0:5))
mtext('Gs (molCO2/tree/s)',side=2,line=3,cex=1)
box()
par(mar=c(4.5,4.5,0,1))
image(kde2d(DA,AE,n=500,h=c(75,width.SJ(AE)),lims=c(0,1096,0,8)),col=Palette(10),ann=F,axes=F)
axis(2,at=c(0:7),labels=c(0:7))
axis(1,at=c(0:5)*200,labels=c(0:5)*200)
mtext('AE (gC/L)',side=2,line=3,cex=1)
mtext('Stand age (day after planting)',side=1,line=3,cex=1)
box()




image(kde2d(TOTLE1,TOTLE2,n=250,lims = c(-1,7,-1,7)),col=Palette(12))










