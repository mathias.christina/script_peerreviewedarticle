setwd(set3)

palette('default')
mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols2 <- adjustcolor(palette(), alpha.f = 0.8)
Pr <- c('0-33cm','33-66cm','0.66-1m','1-2m','2-3m','3-4m','4-5m','5-6m','6-7m','7-8m','8-9m','9-10m')

#feuille (21, 29.7)

WIDTH <- 20*0.4#0.39370078740157477#2.79
HEIGHT <- 29.7*0.4#0.39370078740157477#8.2
TAIL1 <- 0.9

windows(WIDTH,HEIGHT)#,pointsize)

tiff(filename = "fig2_art1.tif", width=20,height=29.7,#width = 8.4, height = 24.68,
     units = "cm", #pointsize = 7,
     compression = c("none", "rle", "lzw", "jpeg", "zip"),
     bg = "white", res = 600,
     restoreConsole = TRUE)


#par(mfrow=c(14,1))

par(xaxs='i',yaxs='i')

par(mai=c(HEIGHT-TAIL1-0.2,0.8,0.15,0.7))

barplot(PPT2,border=T,ann=F,axes=F,ylim=c(0,125),col=1)
box(); axis(4,at=c(0,2,4)*25,labels=c(0,2,4)*25,cex.axis=1)
mtext(expression('PP'~(mm~d^-1)),side=4,line=2.5,cex=1.2)
par(new=T);plot(1,1,col=0,ann=F,axes=F,xlim=c(0,1096))
#abline(v=365.5,lty=1)
#abline(v=730.5,lty=1)
text(200,100,'2010',font=4,cex=1.2)
text(550,100,'2011',font=4,cex=1.2)
text(850,100,'2012',font=4,cex=1.2)

par(new=T)
par(mai=c(HEIGHT-2*TAIL1-0.2,0.8,TAIL1,0.7))

plot(c(TOTLE1+TOTLE2+TOTLE4)/3*18*10^-3,col=mycols[4],pch=16,ylim=c(0,8),cex=0.6,axes=F,ann=F)
points(c(Evap1+Evap2+Evap3+Evap4)/4,col=mycols[5],pch=16,cex=0.6)
axis(2,at=c(0,2,4,6,8),labels=c(0,2,4,6,8),cex.axis=1)
mtext(expression('Daily E'~(mm~d^-1)),side=2,line=2.5,cex=1.2)
legend('topleft',c('Canopy transpiration','Soil evaporation'),col=c(mycols[4],mycols[5]),pch=c(16,16),bty='n',cex=1)
#abline(v=365.5,lty=1)
#abline(v=730.5,lty=1)
box()

par(new=T)
par(mai=c(0.8,0.8,2*TAIL1,0.7))

plot((watMeas[,1]+watMeas[,2])/2+0.25*11,ylim=c(0,0.25*12),
      type='l',lwd=2,lty=1,col=mycols2[1],axes=F,ann=F)
points(watDay2[,3]+0.25*11,type='l',lwd=2,lty=2,col=4)
abline(h=0.25*11)
legend(x=1,y=0.25*11+0.13,legend=Pr[1],pch=NA,bty='n',cex=1.2)

for (i in 3:13){
points(watMeas[,i]+0.25*(13-i),type='l',lwd=2,lty=1,col=mycols2[1])
points(watDay2[,i+1]+0.25*(13-i),type='l',lwd=2,lty=2,col=4)
abline(h=0.25*(13-i))
legend(x=1,y=0.25*(13-i)+0.13,legend=Pr[i-1],pch=NA,bty='n',cex=1.2)
}

AT<-c();for (i in 1:12){ AT <- c(AT, c(0,0.1,0.2)+(i-1)*0.25)}
axis(2,at=AT,labels=rep(c(0,0.1,0.2),12),cex.axis=1)
#abline(v=365.5,lty=1)
#abline(v=730.5,lty=1)
mtext(expression('Daily SWC'~(m^3~m^-3)),side=2,line=2.5,cex=1.2)
axis(1,at=c(0:5)*200,labels=c(0:5)*200,cex.axis=1)
mtext('Stand age (day after planting)',side=1,line=2.5,cex=1.2)
legend('right',legend=c('Measured SWC','Predicted SWC'),pch=NA,lty=1:2,col=c(1,4),bty='n',cex=1.2)
box()


dev.off()
dev.set()



