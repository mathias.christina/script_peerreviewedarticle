require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")
set9<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/runMAESPA")
set10<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test2")

setwd(set1)

# et maintenant dans les donn�es simul�es

dat1 <- read.table('110_watbal.dat',skip=37)
dat2 <- read.table('210_watbal.dat',skip=37)
dat3 <- read.table('310_watbal.dat',skip=37)
dat4 <- read.table('410_watbal.dat',skip=37)
names(dat1) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat2) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat3) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')
names(dat4) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2,fracw1', 'fracw2')

qe1 <- dat1$qe ; qe2 <-dat2$qe ; qe3 <- dat3$qe ; qe4 <- dat4$qe
qh1 <- dat1$qh ; qh2 <-dat2$qh ; qh3 <- dat3$qh ; qh4 <- dat4$qh
qn1 <- dat1$qn ; qn2 <-dat2$qn ; qn3 <- dat3$qn ; qn4 <- dat4$qn
qc1 <- dat1$qc ; qc2 <-dat2$qc ; qc3 <- dat3$qc ; qc4 <- dat4$qc

par(mfrow=c(1,3))
par(mar=c(4.5,4.5,3,1))
plot(qe1+qh1+qc1+qn1,xlab='half-hour during 2010')
title('2 layer, tort=1.5')
plot(qe2+qh2+qc2+qn2,xlab='half-hour during 2010')
title('1 layer, tort=0.66')
plot(qe3+qh3+qc3+qn3,xlab='half-hour during 2010')
title('1 layer, tort=1.5')

flux1 <- -qe1-qh1
flux2 <- -qe2-qh2
flux3 <- -qe3-qh3
flux4 <- -qe4-qh4
fit1 <- lm(qn1~flux1)
fit2 <- lm(qn2~flux2)
fit3 <- lm(qn3~flux3)
fit4 <- lm(qn4~flux4)

par(mfrow=c(1,3))
par(mar=c(4.5,4.5,2,1))
plot(-qe1-qh1,qn1,xlab='Soil LE + H (W/m�)',ylab='Soil Rnet (W/m�)',cex.lab=1.2)
grid(col=1)
abline(fit1,col=2,lwd=2)
legend('topleft',legend=c('Rnet = 1.46 * (LE+H) - 30.08'),box.col=0,bg='white',cex=1.2)
title('2 layer, tort=1.5')
box()
plot(-qe2-qh2,qn2,xlab='Soil LE + H (W/m�)',ylab='Soil Rnet (W/m�)',cex.lab=1.2)
grid(col=1)
abline(fit2,col=2,lwd=2)
legend('topleft',legend=c('Rnet = 1.08 * (LE+H) - 4.38'),box.col=0,bg='white',cex=1.2)
title('1 layer, tort=0.66')
box()
plot(-qe3-qh3,qn3,xlab='Soil LE + H (W/m�)',ylab='Soil Rnet (W/m�)',cex.lab=1.2)
grid(col=1)
abline(fit3,col=2,lwd=2)
legend('topleft',legend=c('Rnet = 1.08 * (LE+H) - 4.25'),box.col=0,bg='white',cex=1.2)
title('1 layer, tort=1.5')
box()

datLE <- read.table("LEflux_bon.txt",header=T)
datH <- read.table("Hflux_bon.txt",header=T)
#attach(dat)
DATEH <- datH$DATE - min(datH$DATE)+1
DATELE <- datLE$DATE - min(datLE$DATE)+1

# latent
LESIM1 <- c(dat1$evapstore[1:dim(dat1)[1]]/(30*60)*2.45*10^6 +
            dat1$soilevap[1:dim(dat1)[1]]*2.45*10^6/(30*60)   +
            dat1$et[1:dim(dat1)[1]]*2.45*10^6/(30*60))
LESIM2 <- c(dat2$evapstore[1:dim(dat2)[1]]/(30*60)*2.45*10^6 +
            dat2$soilevap[1:dim(dat2)[1]]*2.45*10^6/(30*60)   +
            dat2$et[1:dim(dat2)[1]]*2.45*10^6/(30*60))
LESIM3 <- c(dat3$evapstore[1:dim(dat3)[1]]/(30*60)*2.45*10^6 +
            dat3$soilevap[1:dim(dat3)[1]]*2.45*10^6/(30*60)   +
            dat3$et[1:dim(dat3)[1]]*2.45*10^6/(30*60))
LESIM4 <- c(dat4$evapstore[1:dim(dat4)[1]]/(30*60)*2.45*10^6 +
            dat4$soilevap[1:dim(dat4)[1]]*2.45*10^6/(30*60)   +
            dat4$et[1:dim(dat4)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(dat1$day[1:(304*48)],dat1$hour[1:(304*48)],LESIM1[1:(304*48)],LESIM2[1:(304*48)],LESIM3[1:(304*48)],LESIM4[1:(304*48)])
names(SIMDAT) <- c('DATELE','HOUR2','lesim1','lesim2','lesim3','lesim4')
HOUR2 <-c()
for (i in 1:dim(datLE)[1]){
if (datLE$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+1)}
else if (datLE$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+2)}}
OBSDAT <- data.frame(DATELE,HOUR2,datLE$LE)
FLE <- merge(SIMDAT, OBSDAT,all=T,by=c('DATELE','HOUR2'))

H1 <- read.table('110_hrflux.dat',skip=40)
H2 <- read.table('210_hrflux.dat',skip=40)
H3 <- read.table('310_hrflux.dat',skip=40)
H4 <- read.table('410_hrflux.dat',skip=40)
names(H1) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H2) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H3) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H4) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')


HT4 <-c();HT1 <-c();HT2 <- c(); HT3 <-c()
for (i in 1:365){
for (j in 1:48){
#HT1 <- c(HT1, sum(H1$hrH[H1$DOY==i][H1$HOUR[H1$DOY==1]==j],na.rm=T)/496.05)
#HT2 <- c(HT2, sum(H2$hrH[H2$DOY==i][H2$HOUR[H2$DOY==1]==j],na.rm=T)/496.05)
#HT3 <- c(HT3, sum(H3$hrH[H3$DOY==i][H3$HOUR[H3$DOY==1]==j],na.rm=T)/496.05)
HT4 <- c(HT4, sum(H4$hrH[H4$DOY==i][H4$HOUR[H3$DOY==1]==j],na.rm=T)/496.05)
}}
#sauv

HSIM1 <- -dat1$qh[1:dim(dat110)[1]] +HT1*1000
HSIM2 <- -dat2$qh[1:dim(dat210)[1]] +HT2*1000
HSIM3 <- -dat3$qh[1:dim(dat310)[1]] +HT3*1000
HSIM4 <- -dat4$qh[1:dim(dat310)[1]] +HT4*1000

SIMDAT <- data.frame(dat110$day[1:(304*48)],dat110$hour[1:(304*48)],HSIM1[1:(304*48)],HSIM2[1:(304*48)],HSIM3[1:(304*48)],HSIM4[1:(304*48)])
names(SIMDAT) <- c('DATEH','HOUR2','Hsim1','Hsim2','Hsim3','Hsim4')

HOUR2 <-c()
for (i in 1:dim(datH)[1]){
if (datH$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+1)}
else if (datH$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+2)}}
OBSDAT <- data.frame(DATEH,HOUR2,datH$H)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATEH','HOUR2'))

obsLE <- FLE$datLE.LE
obsH <- FH$datH.H

mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31))
hour <- rep(1:48,304)

LEO<-c() ; HEO <-c()
HES1 <-c() ; HES2 <-c() ;HES3 <-c();HES4 <-c()
LES1 <-c() ; LES2 <-c() ;LES3 <-c();LES4 <-c()
for (i in 1:10){
for (j in 1:48){
HEO <- c(HEO, mean(obsH[mois==i][hour[mois==i]==j],na.rm=T))
LEO <- c(LEO, mean(obsLE[mois==i][hour[mois==i]==j],na.rm=T))
HES1 <- c(HES1, mean(FH$Hsim1[mois==i][hour[mois==i]==j],na.rm=T))
HES2 <- c(HES2, mean(FH$Hsim2[mois==i][hour[mois==i]==j],na.rm=T))
HES3 <- c(HES3, mean(FH$Hsim3[mois==i][hour[mois==i]==j],na.rm=T))
HES4 <- c(HES4, mean(FH$Hsim4[mois==i][hour[mois==i]==j],na.rm=T))
LES1 <- c(LES1, mean(FLE$lesim1[mois==i][hour[mois==i]==j],na.rm=T))
LES2 <- c(LES2, mean(FLE$lesim2[mois==i][hour[mois==i]==j],na.rm=T))
LES3 <- c(LES3, mean(FLE$lesim3[mois==i][hour[mois==i]==j],na.rm=T))
LES4 <- c(LES4, mean(FLE$lesim4[mois==i][hour[mois==i]==j],na.rm=T))
}}


MOI <- c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.')

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(LEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Latent heat flux (W/m�)',cex.lab=1.2,ylim=c(-100,350))
points(LES1,type='l',col=2,lwd=2)
points(LES2,type='l',col=3,lwd=2)
points(LES3,type='l',col=4,lwd=2)
points(LES4,type='l',col=5,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=-75,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=-75,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured LE','Sim LE 2layer, tort=1.5','Sim LE 1layer, tort=0.66','Sim LE 1layer, tort=1.5','No litter, tort=1.5'),col=1:5,lty=1,pch=NA,bg='white',box.col=0)
box()

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(HEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Sensible heat flux (W/m�)',cex.lab=1.2,ylim=c(-100,450))
points(HES1,type='l',col=2,lwd=2)
points(HES2,type='l',col=3,lwd=2)
points(HES3,type='l',col=4,lwd=2)
points(HES4,type='l',col=5,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=-75,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=-75,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured H','Sim H 2layer, tort=1.5','Sim H 1layer, tort=0.66','Sim H 1layer, tort=1.5','No litter, tort=1.5'),col=1:5,lty=1,pch=NA,bg='white',box.col=0)
box()
