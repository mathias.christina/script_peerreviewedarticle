require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
set11<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Test")
set12<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations2")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")
set9<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/runMAESPA")
set10<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test2")

setwd(set11)
setwd(set12)

dat110 <- read.table('4_watbal.dat',skip=37)
dat111 <- read.table('5_watbal.dat',skip=37)
dat112 <- read.table('6_watbal.dat',skip=37)
dat210 <- read.table('16_watbal.dat')
dat211 <- read.table('17_watbal.dat')
dat212 <- read.table('18_watbal.dat')
dat310 <- read.table('28_watbal.dat')
dat311 <- read.table('29_watbal.dat')
dat312 <- read.table('30_watbal.dat')
names(dat110) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat111) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat112) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat210) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat211) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat212) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat310) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat311) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
names(dat312) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')


# 110 2 couches TORT = 1.5, 210 1 couche TORT=0.66, 310 1 couche TORT==1.5

setwd(set11)
datLE <- read.table("LEmathias2_mod.txt",header=T)
datH <- read.table("Hmathias2_mod.txt",header=T)
#attach(dat)
DATEH <- datH$DATE - min(datH$DATE)+1
DATELE <- datLE$DATE - min(datLE$DATE)+1


LESIM1 <- c(dat110$evapstore[1:dim(dat110)[1]]/(30*60)*2.45*10^6 +
            dat110$soilevap[1:dim(dat110)[1]]*2.45*10^6/(30*60)   +
            dat110$et[1:dim(dat110)[1]]*2.45*10^6/(30*60),
            dat111$evapstore[1:dim(dat111)[1]]/(30*60)*2.45*10^6 +
            dat111$soilevap[1:dim(dat111)[1]]*2.45*10^6/(30*60)   +
            dat111$et[1:dim(dat111)[1]]*2.45*10^6/(30*60))
LESIM2 <- c(dat210$evapstore[2:dim(dat210)[1]]/(30*60)*2.45*10^6 +
            dat210$soilevap[2:dim(dat210)[1]]*2.45*10^6/(30*60)   +
            dat210$et[2:dim(dat210)[1]]*2.45*10^6/(30*60),
            dat211$evapstore[2:dim(dat211)[1]]/(30*60)*2.45*10^6 +
            dat211$soilevap[2:dim(dat211)[1]]*2.45*10^6/(30*60)   +
            dat211$et[2:dim(dat211)[1]]*2.45*10^6/(30*60))
LESIM3 <- c(dat310$evapstore[2:dim(dat310)[1]]/(30*60)*2.45*10^6 +
            dat310$soilevap[2:dim(dat310)[1]]*2.45*10^6/(30*60)   +
            dat310$et[2:dim(dat310)[1]]*2.45*10^6/(30*60),
            dat311$evapstore[2:dim(dat311)[1]]/(30*60)*2.45*10^6 +
            dat311$soilevap[2:dim(dat311)[1]]*2.45*10^6/(30*60)   +
            dat311$et[2:dim(dat311)[1]]*2.45*10^6/(30*60))

SIMDAT <- data.frame(c(dat110$day[1:17520],dat111$day[1:17520]+365),c(dat110$hour[1:17520],dat111$hour[1:17520]),LESIM1,LESIM2,LESIM3)
names(SIMDAT) <- c('DATELE','HOUR2','lesim1','lesim2','lesim3')


HOUR2 <-c()
for (i in 1:dim(datLE)[1]){
if (datLE$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+1)}
else if (datLE$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+2)}}
OBSDAT <- data.frame(DATELE,HOUR2,datLE$LE)
FLE <- merge(SIMDAT, OBSDAT,all=T,by=c('DATELE','HOUR2'))


H11 <- read.table('4_hrflux.dat',skip=38)
H12 <- read.table('5_hrflux.dat',skip=40)
H21 <- read.table('16_hrflux.dat')
H22 <- read.table('17_hrflux.dat')
H31 <- read.table('28_hrflux.dat')
H32 <- read.table('29_hrflux.dat')
names(H11) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H21) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H31) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H12) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H22) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H32) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')


HT11 <-c();HT21 <- c(); HT31 <-c()
HT12 <-c();HT22 <- c(); HT32 <-c()
for (i in 1:365){
for (j in 1:48){
HT11 <- c(HT11, sum(H11$hrH[H11$DOY==i][H11$HOUR[H11$DOY==1]==j],na.rm=T)/502.36)
HT21 <- c(HT21, sum(H21$hrH[H21$DOY==i][H21$HOUR[H21$DOY==1]==j],na.rm=T)/502.36)
HT31 <- c(HT31, sum(H31$hrH[H31$DOY==i][H31$HOUR[H31$DOY==1]==j],na.rm=T)/502.36)
HT12 <- c(HT12, sum(H12$hrH[H12$DOY==i][H12$HOUR[H12$DOY==1]==j],na.rm=T)/502.36)
HT22 <- c(HT22, sum(H22$hrH[H22$DOY==i][H22$HOUR[H22$DOY==1]==j],na.rm=T)/502.36)
HT32 <- c(HT32, sum(H32$hrH[H32$DOY==i][H32$HOUR[H32$DOY==1]==j],na.rm=T)/502.36)
print(i)
}}
#sauv


HSIM1 <- c(-dat110$qh[1:dim(dat110)[1]] +HT11*1000 , -dat111$qh[1:dim(dat111)[1]] + HT12*1000)
HSIM2 <- c(-dat210$qh[1:dim(dat210)[1]] +HT21*1000 , -dat211$qh[1:dim(dat211)[1]] + HT22*1000)
HSIM3 <- c(-dat310$qh[1:dim(dat310)[1]] +HT31*1000 , -dat311$qh[1:dim(dat311)[1]] + HT32*1000)

SIMDAT <- data.frame(c(dat110$day[1:17520],dat111$day[1:17520]+365),c(dat110$hour[1:17520],dat111$hour[1:17520]),
                      HSIM1,HSIM2,HSIM3)
names(SIMDAT) <- c('DATEH','HOUR2','Hsim1','Hsim2','Hsim3')

HOUR2 <-c()
for (i in 1:dim(datH)[1]){
if (datH$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+1)}
else if (datH$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+2)}}
OBSDAT <- data.frame(DATEH,HOUR2,datH$H)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATEH','HOUR2'))

obsH <- FH$datH.H
obsLE <- FLE$datLE.LE


HS10 <- HSIM1[1:(365*48)]
HS11 <- HSIM1[(365*48+1):(730*48)]
HO10 <- obsH[1:(365*48)]
HO11 <- obsH[(365*48+1):(730*48)]

fit1 <- lm(HS10~HO10-1)
fit2 <- lm(HS11~HO11-1)

par(mar=c(4.5,4.5,3,1))
par(mfrow=c(1,2))
plot(HO10,HS10,xlab='Half-hourly measured H',ylab='Half-hourly simulated H')
abline(0,1,lty=2)
abline(fit1,col=2)
legend('topleft',legend=c('Hsim = 1.56 *Hmeas'),pch=NA,lty=1,col=2,bty='n')
title('2010')
plot(HO11,HS11,xlab='Half-hourly measured H',ylab='Half-hourly simulated H')
abline(0,1,lty=2)
abline(fit2,col=2)
legend('topleft',legend=c('Hsim = 1.97 *Hmeas'),pch=NA,lty=1,col=2,bty='n')
title('2011')

mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31),rep(11,48*30),rep(12,48*31))
mois <- c(mois,mois+12)
hour <- rep(1:48,730)

LEO<-c() ; HEO <-c()
HES1 <-c() ; HES2 <-c() ;HES3 <-c()
LES1 <-c() ; LES2 <-c() ;LES3 <-c()
for (i in 1:24){
for (j in 1:48){
HEO <- c(HEO, mean(obsH[mois==i][hour[mois==i]==j],na.rm=T))
LEO <- c(LEO, mean(obsLE[mois==i][hour[mois==i]==j],na.rm=T))
HES1 <- c(HES1, mean(FH$Hsim1[mois==i][hour[mois==i]==j],na.rm=T))
HES2 <- c(HES2, mean(FH$Hsim2[mois==i][hour[mois==i]==j],na.rm=T))
HES3 <- c(HES3, mean(FH$Hsim3[mois==i][hour[mois==i]==j],na.rm=T))
LES1 <- c(LES1, mean(FLE$lesim1[mois==i][hour[mois==i]==j],na.rm=T))
LES2 <- c(LES2, mean(FLE$lesim2[mois==i][hour[mois==i]==j],na.rm=T))
LES3 <- c(LES3, mean(FLE$lesim3[mois==i][hour[mois==i]==j],na.rm=T))
}}

fit1 <- lm(LESIM1~obsLE-1)
fit2 <- lm(LESIM2~obsLE-1)
fit3 <- lm(LESIM3~obsLE-1)

LES10 <- LESIM3[1:17520] ; LES11 <- LESIM3[17521:35040]
LEO10 <- obsLE[1:17520] ; LEO11 <- obsLE[17521:35040]
fit10 <- lm(LES10~LEO10) ; fit11 <- lm(LES11~LEO11)


MOI <- rep(c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.','Nov.','Dec.'),2)

par(mfrow=c(2,1))
par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(LEO10,LES10,xlim=c(-80,1680),ylim=c(-50,800),xlab='Measured half-hourly latent heat flux (W/m�)',
                  ylab='Predicted hr LE (W/m�)',cex.lab=1.2,axes=F)
points(LEO11+880,LES11)
abline(v=800)
X_10 <- seq(-80,800,length.out=100) ; Y_10 <- summary(fit10)$coef[1]*X_10
X_11 <- seq(800,1680,length.out=100) ; Y_11 <- summary(fit11)$coef[1] *(X_11-880)
points(X_10,Y_10,type='l',col=2)
points(X_11,Y_11,type='l',col=2)
legend(400,150,legend=c(expression(LE[pred]~'='~0.85~LE[meas]),'R� = 0.839'),pch=NA,bg='white',box.col=0,cex=1)
legend(1300,150,legend=c(expression(LE[pred]~'='~1.01~LE[meas]),'R� = 0.901'),pch=NA,bg='white',box.col=0,cex=1)
legend(0,800,legend='2010',pch=NA,bg='white',box.col=0,cex=1.2)
legend(880,800,legend='2011',pch=NA,bg='white',box.col=0,cex=1.2)
axis(1,at=c(0,200,400,600,800,1000,1200,1400,1600),labels=c(0,200,400,600,0,200,400,600,800))
axis(2,at=c(0:4)*200,labels=c(0:4)*200)
box()

plot(LEO,type='l',lwd=2,axes=F,xlab='Month',ylab='Hourly LE (W/m�)',cex.lab=1.2,ylim=c(-20,500))
points(LES1,type='l',col=4,lwd=2)
#points(LES2,type='l',col=3,lwd=2)
#points(LES3,type='l',col=4,lwd=2)
axis(1,at=c(0:23)*48+24,labels=MOI,cex=1)
axis(2,at=c(0:10)*50,labels=c(0:10)*50)
legend('topleft',legend=c('Measured latent heat flux','Predicted latent heat flux'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
abline(v=48*12)
box()

plot(HEO,type='l',lwd=2,axes=F,xlab='Month',ylab='Hourly LE (W/m�)',cex.lab=1.2,ylim=c(-20,500))
points(HES1,type='l',col=2,lwd=2)
#points(LES2,type='l',col=3,lwd=2)
#points(LES3,type='l',col=4,lwd=2)
axis(1,at=c(0:23)*48+24,labels=MOI,cex=1)
axis(2,at=c(0:10)*50,labels=c(0:10)*50)
legend('topleft',legend=c('Measured latent heat flux','Predicted latent heat flux'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
abline(v=48*12)
box()

# deuxi�me graph
library(MASS)
leo10<-c();leo11<-c();les10<-c();les11<-c()
for (i in 1:17520){
if(is.na(LEO10[i])){
leo10 <- leo10 ; les10 <-les10}
else {leo10 <- c(leo10,LEO10[i]) ; les10 <-c(les10,LES10[i])}}
for (i in 1:17520){
if(is.na(LEO11[i])){
leo11 <- leo11 ; les11 <-les11}
else {leo11 <- c(leo11,LEO11[i]) ; les11 <-c(les11,LES11[i])}}

Palette<-(colorRampPalette(c("white","darkblue"),bias=1) )


split.screen(c(2, 1))
split.screen(c(1, 2), screen = 1)

screen(3) 
par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')

par(mfrow=c(2,2))
plot(leo10,les10,xlim=c(-50,600))
abline(0,1,col=2)
image(kde2d(leo10,les10,n=100,lims=c(-50,600,-50,600),
      h = 300),col=Palette(12))#),ann=F,axes=F)
abline(0,1,col=2);box()
plot(leo11,les11,xlim=c(-50,600))
abline(0,1,col=2)
image(kde2d(leo11,les11,n=100,lims=c(-50,600,-50,600),
      h = 300),col=Palette(12))#),ann=F,axes=F)
abline(0,1,col=2);box()


plot(LEO10,LES10,xlim=c(-80,1680),ylim=c(-50,800),xlab='Measured half-hourly latent heat flux (W/m�)',
                  ylab='Predicted hr LE (W/m�)',cex.lab=1.2,axes=F)
points(LEO11+880,LES11)
abline(v=800)
X_10 <- seq(-80,800,length.out=100) ; Y_10 <- summary(fit10)$coef[1]*X_10
X_11 <- seq(800,1680,length.out=100) ; Y_11 <- summary(fit11)$coef[1] *(X_11-880)
points(X_10,Y_10,type='l',col=2)
points(X_11,Y_11,type='l',col=2)
legend(400,150,legend=c(expression(LE[pred]~'='~0.85~LE[meas]),'R� = 0.839'),pch=NA,bg='white',box.col=0,cex=1)
legend(1300,150,legend=c(expression(LE[pred]~'='~1.01~LE[meas]),'R� = 0.901'),pch=NA,bg='white',box.col=0,cex=1)
legend(0,800,legend='2010',pch=NA,bg='white',box.col=0,cex=1.2)
legend(880,800,legend='2011',pch=NA,bg='white',box.col=0,cex=1.2)
axis(1,at=c(0,200,400,600,800,1000,1200,1400,1600),labels=c(0,200,400,600,0,200,400,600,800))
axis(2,at=c(0:4)*200,labels=c(0:4)*200)
box()

plot(LEO,type='l',lwd=2,axes=F,xlab='Month',ylab='Hourly LE (W/m�)',cex.lab=1.2,ylim=c(-20,500))
points(LES1,type='l',col=4,lwd=2)
#points(LES2,type='l',col=3,lwd=2)
#points(LES3,type='l',col=4,lwd=2)
axis(1,at=c(0:23)*48+24,labels=MOI,cex=1)
axis(2,at=c(0:10)*50,labels=c(0:10)*50)
legend('topleft',legend=c('Measured latent heat flux','Predicted latent heat flux'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
abline(v=48*12)
box()




par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(HEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Sensible heat flux (W/m�)',cex.lab=1.2,ylim=c(-100,500))
points(HES1,type='l',col=2,lwd=2)
points(HES2,type='l',col=3,lwd=2)
points(HES3,type='l',col=4,lwd=2)
axis(1,at=c(0:23)*48+24,labels=MOI,cex=1)
axis(2,at=c(0:10)*50,labels=c(0:10)*50)
legend('topleft',legend=c('Measured H','Predicted H'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
abline(v=48*12)
box()






lesim1<-c();lesim2<-c();lesim3<-c();Dleobs<-c()
lesim4 <<-c()
for (i in 1:48){
lesim1 <- c(lesim1, mean(LESIM1[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim2 <- c(lesim2, mean(LESIM2[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim3 <- c(lesim3, mean(LESIM3[1:min(c(1488,length(LESIM3)))][FH$HOUR2[1:min(c(1488,length(LESIM3)))]==i],na.rm=T))
lesim4 <- c(lesim4, mean(LESIM4[1:min(c(1488,length(LESIM4)))][FH$HOUR2[1:min(c(1488,length(LESIM4)))]==i],na.rm=T))
Dleobs <- c(Dleobs, mean(FH$LE[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM2[1:336],col=2,type='l',lwd=2)
points(LESIM3[1:336],col=3,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
points(LESIM4[1:336],col=5,type='l',lwd=2)
#legend('topleft',legend=c('LE meas','LE sim ini','LE sim NT 2cm','LE sim T 2cm'),col=c(1,4,2,3),lty=1,pch=NA,bty='n')
#legend('topleft',legend=c('LE meas','LE sim NT 2cm'),col=c(1,2),lty=1,pch=NA,bty='n')
legend('topleft',legend=c('LE meas','LE sim ini','LE sim T 2cm','LE sim T 4cm','LE sim T 6cm'),col=c(1,4,2,3,5),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesim2,col=2,type='l',lwd=2)
points(lesim3,col=3,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
points(lesim4,col=5,type='l',lwd=2)
#legend('topleft',legend=c('LE meas','LE sim NT 2cm'),col=c(1,2),lty=1,pch=NA,bty='n')
#legend('topleft',legend=c('LE meas','LE sim ini','LE sim NT 2cm','LE sim T 2cm'),col=c(1,4,2,3),lty=1,pch=NA,bty='n')
legend('topleft',legend=c('LE meas','LE sim ini','LE sim T 2cm','LE sim T 4cm','LE sim T 6cm'),col=c(1,4,2,3,5),lty=1,pch=NA,bty='n')


# liti�re de 3cm, on fait varier la tortuoisit�

setwd(set9)
dat1 <- readwatbal('watbal_T25.dat')
dat1 <- dat1[2:(48*31+1),]
setwd(set10)
dat2 <- readwatbal('watbal_T02.dat')
dat2 <- dat2[2:(48*31+1),]
dat3 <- readwatbal('watbal_T14.dat')
dat3 <- dat3[2:(48*31+1),]


LESIM2 <- c(dat1$evapstore[1:dim(dat1)[1]]/(30*60)*2.45*10^6 +
            dat1$soilevap[1:dim(dat1)[1]]*2.45*10^6/(30*60)   +
            dat1$et[1:dim(dat1)[1]]*2.45*10^6/(30*60))
LESIM3 <- c(dat2$evapstore[1:dim(dat2)[1]]/(30*60)*2.45*10^6 +
            dat2$soilevap[1:dim(dat2)[1]]*2.45*10^6/(30*60)   +
            dat2$et[1:dim(dat2)[1]]*2.45*10^6/(30*60))
LESIM4 <- c(dat3$evapstore[1:dim(dat3)[1]]/(30*60)*2.45*10^6 +
            dat3$soilevap[1:dim(dat3)[1]]*2.45*10^6/(30*60)   +
            dat3$et[1:dim(dat3)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(dat1$day[1:(48*304)],dat1$hour[1:(48*304)],LESIM1[1:(48*304)],c(LESIM2,rep(NA,14592-length(LESIM2))),c(LESIM3,rep(NA,14592-length(LESIM3))),c(LESIM4,rep(NA,14592-length(LESIM4))))
names(SIMDAT) <- c('DATE2','HOUR2','lesim1','lesim2','lesim3','lesim4')

HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))

lesim1<-c();lesim2<-c();lesim3<-c();Dleobs<-c()
lesim4 <<-c()
for (i in 1:48){
lesim1 <- c(lesim1, mean(LESIM1[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim2 <- c(lesim2, mean(LESIM2[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim3 <- c(lesim3, mean(LESIM3[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim4 <- c(lesim4, mean(LESIM4[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
Dleobs <- c(Dleobs, mean(FH$LE[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM2[1:336],col=2,type='l',lwd=2)
points(LESIM3[1:336],col=3,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
points(LESIM4[1:336],col=5,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE sim Tort 1.4','LE sim Tort 2.5','LE sim Tort 0.2'),col=c(1,4,5,2,3),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2,ylim=c(0,250))
points(lesim2,col=2,type='l',lwd=2)
points(lesim3,col=3,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
points(lesim4,col=5,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE sim Tort 1.4','LE sim Tort 2.5','LE sim Tort 0.2'),col=c(1,4,5,2,3),lty=1,pch=NA,bty='n')

# test sans litter
setwd(set9)
dat4 <- readwatbal('watbal_NL_T066.dat')
dat4 <- dat4[2:(48*31+1),]

LESIM5 <- c(dat4$evapstore[1:dim(dat4)[1]]/(30*60)*2.45*10^6 +
            dat4$soilevap[1:dim(dat4)[1]]*2.45*10^6/(30*60)   +
            dat4$et[1:dim(dat4)[1]]*2.45*10^6/(30*60))
lesim5 <-c()
for (i in 1:48){
lesim5 <- c(lesim5, mean(LESIM5[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM5[1:336],col=2,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE no litter'),col=c(1,4,2),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesim5,col=2,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE no litter'),col=c(1,4,2),lty=1,pch=NA,bty='n')


# test ancienne version, nouvelle version avec 1 seul couche (JU2)
require(Maeswrap)

setwd(set8)
day1 <- readdayflux('Dayflx_JU1.dat')
bal1 <- readwatbal('watbal_JU1.dat')
wat1 <- read.table('watlay_JU1.dat')
tem1 <- read.table('watsoilt_JU1.dat')
setwd(set9)
day2 <- readdayflux('Dayflx_JU2.dat')
bal2 <- readwatbal('watbal_JU2.dat')
wat2 <- read.table('watlay_JU2.dat')
tem2 <- read.table('watsoilt_JU2.dat')

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(1,3))
plot(day1$totPs/day2$totPs)
abline(h=1,col=2,lwd=2)
plot(day1$totLE1/day2$totLE1)
abline(h=1,col=2,lwd=2)
plot(day1$totH/day2$totH)
abline(h=1,col=2,lwd=2)

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,4))
plot(bal1$soilevap,bal2$soilevap)
abline(0,1,col=2,lwd=2)
plot(-bal1$qh,-bal2$qh)
abline(0,1,col=2,lwd=2)
plot(-bal1$qe,-bal2$qe)
abline(0,1,col=2,lwd=2)
plot(bal1$qn,bal2$qn)
abline(0,1,col=2,lwd=2)
plot(-bal1$qc,-bal2$qc)
abline(0,1,col=2,lwd=2)
plot(bal1$soilt1,bal2$soilt1)
abline(0,1,col=2,lwd=2)
plot(bal1$soilt2,bal2$soilt2)
abline(0,1,col=2,lwd=2)
plot(bal1$fracw1,bal2$fracw1)
abline(0,1,col=2,lwd=2)

LESIMmod <- c(bal2$evapstore[1:dim(bal2)[1]]/(30*60)*2.45*10^6 +
            bal2$soilevap[1:dim(bal2)[1]]*2.45*10^6/(30*60)   +
            bal2$et[1:dim(bal2)[1]]*2.45*10^6/(30*60))
lesimMOD <<-c()
for (i in 1:48){
lesimMOD <- c(lesimMOD, mean(LESIMmod[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIMmod[1:336],col=2,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE 1layer mod tort=1.5'),col=c(1,4,2),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesimMOD,col=2,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE 1layer mod tort=1.5'),col=c(1,4,2),lty=1,pch=NA,bty='n')


# effet tortpar = 1.5
setwd(set10)
day <- readdayflux('Dayflx_test.dat')[1:(304*84),]
bal <- readwatbal('watbal_test.dat')[2:(304*48+1),]
wat <- read.table('watlay_test.dat')[1:(304*48),]
tem <- read.table('watsoilt_test.dat')[1:(304*48),]

LESIM <- c(bal$evapstore[1:dim(bal)[1]]/(30*60)*2.45*10^6 +
            bal$soilevap[1:dim(bal)[1]]*2.45*10^6/(30*60)   +
            bal$et[1:dim(bal)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(bal$day[1:(48*304)],bal$hour[1:(48*304)],LESIM[1:(48*304)])
names(SIMDAT) <- c('DATE2','HOUR2','lesim')
HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))

mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31))
lesim <- FH$lesim
hour <- rep(1:48,304)
obsLE <- FH$LE

LES <-c() ; LEO <-c()
for (i in 1:10){
for (j in 1:48){
LES <- c(LES, mean(lesim[mois==i][hour[mois==i]==j],na.rm=T))
LEO <- c(LEO, mean(obsLE[mois==i][hour[mois==i]==j],na.rm=T))
}}

MOI <- c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.')

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(LEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Latent heat flux (W/m�)',cex.lab=1.2,ylim=c(-10,350))
points(LES,type='l',col=4,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=275,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=325,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured latent heat flux','Simulated latent heat flux'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
box()


setwd(set1)
day4 <- readdayflux('10_Dayflx.dat')





# on recup les gscan
H11 <- read.table('4_hrflux.dat')
H12 <- read.table('5_hrflux.dat')
H13 <- read.table('6_hrflux.dat')
names(H11) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H12) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')
names(H13) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')

ID <- 1:(84*365)
ID <- ID*24

GS <- c(H11$Gscan[H11$HOUR==24],H12$Gscan[H12$HOUR==24],H13$Gscan[H13$HOUR==24])
PSIL <- c(H11$PSIL[H11$HOUR==24],H12$PSIL[H12$HOUR==24],H13$PSIL[H13$HOUR==24])
DAY <- c(H11$DOY[H11$HOUR==24],H12$DOY[H12$HOUR==24],H13$DOY[H13$HOUR==24])

GS2<-c();PSIL2<-c()
for (i in 1:1096){
GS2 <- c(GS2, mean(GS[((i-1)*84+1):(i*84)],na.rm=T))
PSIL2 <- c(PSIL2, mean(PSIL[((i-1)*84+1):(i*84)],na.rm=T))}


DA <- 1:1096
lisGS <- loess(GS2~DA,span=0.2,degree=2)
DGS <- predict(lisGS,DA)
PSIL2 <- replace(PSIL2,c(PSIL2 < -10),NA)
lisPSI <- loess(PSIL2~DA,span=0.2,degree=2)
DPSI <- predict(lisPSI,DA)

par(mfrow=c(2,1))
par(mar=c(4.5,4.5,1,1))
plot(1:1096,GS2)
points(DA,DGS,col=2,type='l',lwd=2)
abline(v=365);abline(v=730)
plot(1:1096,PSIL2)
points(DA,DPSI,col=2,type='l',lwd=2)
abline(v=365);abline(v=730)









 par(mfrow=c(1,2))
 par(mar=c(4.5,4.5,1,1))
 plot(-dat211$qh[2:dim(dat211)[1]],ylim=c(-100,600),ylab='Hourly H')
 points(HT22*1000,col=2)
legend('topleft',legend=c('H sol','H tree'),col=1:2,bty='n',pch=1)
 plot(HO11, ylab='Measured H',ylim=c(-100,600))



