require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/MATHIAS_MAESPA")

setwd(set1)

dat11 <- read.table('13_Dayflx (2).dat',skip=27)
dat12 <- read.table('14_Dayflx (2).dat',skip=27)
dat13 <- read.table('15_Dayflx (3).dat',skip=27)
dat21 <- read.table('16_Dayflx (2).dat',skip=27)
dat22 <- read.table('17_Dayflx (2).dat',skip=27)
dat23 <- read.table('18_Dayflx (3).dat',skip=27)
dat31 <- read.table('19_Dayflx (2).dat',skip=27)
dat32 <- read.table('20_Dayflx (2).dat',skip=27)
dat33 <- read.table('21_Dayflx (3).dat',skip=27)
dat41 <- read.table('22_Dayflx (2).dat',skip=27)
dat42 <- read.table('23_Dayflx (2).dat',skip=27)
dat43 <- read.table('24_Dayflx (3).dat',skip=27)
names(dat11) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat12) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat13) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat21) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat22) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat23) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat31) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat32) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat33) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat41) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat42) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dat43) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

setwd(set1)
B <- readmet()

iPAR <- c()
aPAR1 <- c();aPAR2 <-c();aPAR3<-c();aPAR4<-c()
for (i in 1:1096){
iPAR <- c(iPAR, sum(B$PAR[((i-1)*48+1):(i*48)])*30*60/(4.5*10^6))}
for (i in 1:365){
aPAR1 <- c(aPAR1, sum(dat11$absPAR[dat11$DOY==i])/481.790)
aPAR2 <- c(aPAR2, sum(dat21$absPAR[dat21$DOY==i])/502.360)
aPAR3 <- c(aPAR3, sum(dat31$absPAR[dat31$DOY==i])/504.130)
aPAR4 <- c(aPAR4, sum(dat41$absPAR[dat41$DOY==i])/496.05)}
for (i in 1:365){
aPAR1 <- c(aPAR1, sum(dat12$absPAR[dat12$DOY==i])/481.790)
aPAR2 <- c(aPAR2, sum(dat22$absPAR[dat22$DOY==i])/502.360)
aPAR3 <- c(aPAR3, sum(dat32$absPAR[dat32$DOY==i])/504.130)
aPAR4 <- c(aPAR4, sum(dat42$absPAR[dat42$DOY==i])/496.05)}
for (i in 1:366){
aPAR1 <- c(aPAR1, sum(dat13$absPAR[dat13$DOY==i])/481.790)
aPAR2 <- c(aPAR2, sum(dat23$absPAR[dat23$DOY==i])/502.360)
aPAR3 <- c(aPAR3, sum(dat33$absPAR[dat33$DOY==i])/504.130)
aPAR4 <- c(aPAR4, sum(dat43$absPAR[dat43$DOY==i])/496.05)}

#
A1 <- readPAR('0001_trees.dat','values','indivlarea')
A2 <- readPAR('0001_trees.dat','values','indivlarea')
A3 <- readPAR('0001_trees.dat','values','indivlarea')
A4 <- readPAR('0001_trees.dat','values','indivlarea')
dates<-c('11/02/10','09/04/10','01/05/10','05/08/10','12/11/10','04/02/11','25/05/11','10/08/11','05/12/11','18/06/12','04/02/13')
Age <- c(3,5,6,9,12,15,18,21,25,31,44)
AGE <- rep(Age,364); AGE3 <- rep(Age,354)
it <- c(111:124,137:150,163:176,189:202,215:228,241:254)
it3 <- c(111:124,137:150,163:176,189:202,215:228,241:254)-10

LAI1<-c();LAI2<-c();LAI3<-c();LAI4<-c()
for (i in Age){
LAI1 <- c(LAI1, sum(A1[AGE==i][it])/481.79)
LAI2 <- c(LAI2, sum(A2[AGE==i][it])/502.36)
LAI3 <- c(LAI3, sum(A3[AGE==i][it])/504.13)
LAI4 <- c(LAI4, sum(A4[AGE==i][it])/496.05)
}


par(mfrow=c(2,1))
par(mar=c(4.5,4.5,0.5,0.5))
plot(Age,LAI1,type='b',col=1,xlim=c(2,38),lwd=2,xlab='Age (months)',ylab='Leaf Area Index (m�/m�)',cex.lab=1.2)
points(Age,LAI2,type='b',col=2,lwd=2)
points(Age,LAI3,type='b',col=3,lwd=2)
points(Age,LAI4,type='b',col=4,lwd=2)
abline(v=14,lty='dotted')
abline(v=26,lty='dotted')
text(6,5,'2010',font=4)
text(18,5  ,'2011',font=4)
text(30,2,'2012',font=4)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c('Plot 1','Plot 2','Plot 3','Plot 4'),pch=1,lty=1,col=1:4,bg='white',box.col=0,cex=1.2);box()

par(mar=c(4.5,4.5,0.5,0.5))
plot(aPAR1/iPAR,xlab='Day from Jan 2010 to Sept 2012',ylab='Fraction of absorbed PAR)',cex.lab=1.2,col=1,pch=16)
points(aPAR2/iPAR,col=2)
points(aPAR3/iPAR,col=3)
points(aPAR4/iPAR,col=4)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,0.1,'2010',font=4)
text(550,0.1,'2011',font=4)
text(850,0.1,'2012',font=4)
legend('bottomright',legend=c('Plot 1','Plot 2','Plot 3','Plot 4'),pch=1,col=1:4,bg='white',box.col=0,cex=1.2);box()

# efficience
setwd(set3)
CAP <- read.table('CAP.txt',header=T)
setwd(set2)
MOR <- read.table('maestra_morpho124.txt',header=T)
MOR3 <- read.table('maestra_morpho3.txt',header=T)
attach(MOR)
attach(MOR3)
setwd(set1)
DAT <- c("11/02/2010","09/04/2010","01/05/2010","05/08/2010","12/11/2010",
          "04/02/2011","25/05/2011","10/08/2011","05/12/2011","18/06/2012","04/02/2013")
it <- c(111:124,137:150,163:176,189:202,215:228,241:254)
it3 <- c(111:124,137:150,163:176,189:202,215:228,241:254)-10

DAT_P1 <-  data.frame(CAPtot_1[Date124=='11/02/2010'][it],CAPtot_1[Date124=='09/04/2010'][it],CAPtot_1[Date124=='01/05/2010'][it],CAPtot_1[Date124=='05/08/2010'][it])
DAT_P2 <-  data.frame(CAPtot_2[Date124=='11/02/2010'][it],CAPtot_2[Date124=='09/04/2010'][it],CAPtot_2[Date124=='01/05/2010'][it],CAPtot_2[Date124=='05/08/2010'][it])
DAT_P3 <-  data.frame(CAPtot_3[Date3=='11/02/2010'][it3],CAPtot_3[Date3=='09/04/2010'][it3],CAPtot_3[Date3=='01/05/2010'][it3],CAPtot_3[Date3=='05/08/2010'][it3])
DAT_P4 <-  data.frame(CAPtot_4[Date124=='11/02/2010'][it],CAPtot_4[Date124=='09/04/2010'][it],CAPtot_4[Date124=='01/05/2010'][it],CAPtot_4[Date124=='05/08/2010'][it])
for (i in 5:31){
DAT_P1 <- data.frame(DAT_P1,CAP[,i][CAP$Parcela==1])
DAT_P2 <- data.frame(DAT_P2,CAP[,i][CAP$Parcela==2])
DAT_P3 <- data.frame(DAT_P3,CAP[,i][CAP$Parcela==3])
DAT_P4 <- data.frame(DAT_P4,CAP[,i][CAP$Parcela==4])}

DAT2 <- c("11/02/2010","09/04/2010","01/05/2010","05/08/2010","03/09/2010", "01/10/2010", "08/11/2010", "09/12/2010",
"05/01/2011", "03/02/2011,10/03/2011", "05/04/2011", "09/05/2011", "15/06/2011", "06/07/2011",
"05/08/2011", "08/09/2011", "06/10/2011", "04/11/2011", "02/12/2011",
"30/12/2011", "01/02/2012", "15/03/2012", "16/05/2012", "16/06/2012",
"02/08/2012", "10/09/2012", "10/10/2012", "05/11/2012", "10/12/2012", "04/02/2013")
names(DAT_P1) <- DAT2
names(DAT_P2) <- DAT2
names(DAT_P3) <- DAT2
names(DAT_P4) <- DAT2

DY <-c(41,98,120,216,245,273,311,342,369,398,433,459,493,530,551,581,615,643,672,700,728,761,804,866,944,983,1013,1039,1074,1130)
DY2 <- c(31,59,90,120,151,181,212,243,273,304,334,365,
        396,424,455,485,516,546,577,608,638,669,699,730,
        761,790,821,851,882,912,943,974,1004,1035,1065,1096)

X1 <- max(which(c(DY<DY2[2])))
X2 <- min(which(c(DY>DY2[2])))
DI <- DY[X2]-DY[X1]
cap_P1 <- (DAT_P1[,X1]*(DY[X2]-DY2[2]) + DAT_P1[,X2]*(DY2[2]-DY[X1]))/DI
cap_P2 <- (DAT_P2[,X1]*(DY[X2]-DY2[2]) + DAT_P2[,X2]*(DY2[2]-DY[X1]))/DI
cap_P3 <- (DAT_P3[,X1]*(DY[X2]-DY2[2]) + DAT_P3[,X2]*(DY2[2]-DY[X1]))/DI
cap_P4 <- (DAT_P4[,X1]*(DY[X2]-DY2[2]) + DAT_P4[,X2]*(DY2[2]-DY[X1]))/DI
for (i in 3:length(DY2)){
X1 <- max(which(c(DY<DY2[i])))
X2 <- min(which(c(DY>=DY2[i])))
DI <- DY[X2]-DY[X1]
cap_P1 <- data.frame(cap_P1,c((DAT_P1[,X1]*(DY[X2]-DY2[i]) + DAT_P1[,X2]*(DY2[i]-DY[X1]))/DI))
cap_P2 <- data.frame(cap_P2,c((DAT_P2[,X1]*(DY[X2]-DY2[i]) + DAT_P2[,X2]*(DY2[i]-DY[X1]))/DI))
cap_P3 <- data.frame(cap_P3,c((DAT_P3[,X1]*(DY[X2]-DY2[i]) + DAT_P3[,X2]*(DY2[i]-DY[X1]))/DI))
cap_P4 <- data.frame(cap_P4,c((DAT_P4[,X1]*(DY[X2]-DY2[i]) + DAT_P4[,X2]*(DY2[i]-DY[X1]))/DI))
}

setwd(set3)
ALT <- read.table('ALT.txt',header=T)
setwd(set1)
DAT2_P1 <- ALT[,7][ALT$Parcela==1]
DAT2_P2 <- ALT[,7][ALT$Parcela==2]
DAT2_P3 <- ALT[,7][ALT$Parcela==3]
DAT2_P4 <- ALT[,7][ALT$Parcela==4]
for (i in 8:25){
DAT2_P1 <- data.frame(DAT2_P1, ALT[,i][ALT$Parcela==1])
DAT2_P2 <- data.frame(DAT2_P2, ALT[,i][ALT$Parcela==2])
DAT2_P3 <- data.frame(DAT2_P3, ALT[,i][ALT$Parcela==3])
DAT2_P4 <- data.frame(DAT2_P4, ALT[,i][ALT$Parcela==4])}
DAT3 <- c("04/02/2010", "11/02/2010", "03/03/2010", "05/04/2010",
"04/05/2010", "26/05/2010", "08/07/2010", "06/08/2010", "03/09/2010",
"01/10/2010", "08/11/2010", "09/12/2010", "07/01/2011", "05/04/2011",
"21/06/2011", "08/09/2011", "07/02/2012", "02/08/2012", "04/02/2013")
names(DAT2_P1) <- DAT3
names(DAT2_P2) <- DAT3
names(DAT2_P3) <- DAT3
names(DAT2_P4) <- DAT3
DAT2_P1[,2] <- DAT2_P1[,1]*(61-41)/(61-34)+DAT2_P1[,3]*(41-34)/(61-34)
DAT2_P2[,2] <- DAT2_P2[,1]*(61-41)/(61-34)+DAT2_P2[,3]*(41-34)/(61-34)
DAT2_P3[,2] <- DAT2_P3[,1]*(61-41)/(61-34)+DAT2_P3[,3]*(41-34)/(61-34)
DAT2_P4[,2] <- DAT2_P4[,1]*(61-41)/(61-34)+DAT2_P4[,3]*(41-34)/(61-34)

DY <-c(34,41,61,94,123,145,188,217,245,273,311,342,371,459,536,615,767,944,1130)

X1 <- max(which(c(DY<DY2[2])))
X2 <- min(which(c(DY>DY2[2])))
DI <- DY[X2]-DY[X1]
alt_P1 <- (DAT2_P1[,X1]*(DY[X2]-DY2[2]) + DAT2_P1[,X2]*(DY2[2]-DY[X1]))/DI
alt_P2 <- (DAT2_P2[,X1]*(DY[X2]-DY2[2]) + DAT2_P2[,X2]*(DY2[2]-DY[X1]))/DI
alt_P3 <- (DAT2_P3[,X1]*(DY[X2]-DY2[2]) + DAT2_P3[,X2]*(DY2[2]-DY[X1]))/DI
alt_P4 <- (DAT2_P4[,X1]*(DY[X2]-DY2[2]) + DAT2_P4[,X2]*(DY2[2]-DY[X1]))/DI
for (i in 3:length(DY2)){
X1 <- max(which(c(DY<DY2[i])))
X2 <- min(which(c(DY>=DY2[i])))
DI <- DY[X2]-DY[X1]
alt_P1 <- data.frame(alt_P1,c((DAT2_P1[,X1]*(DY[X2]-DY2[i]) + DAT2_P1[,X2]*(DY2[i]-DY[X1]))/DI))
alt_P2 <- data.frame(alt_P2,c((DAT2_P2[,X1]*(DY[X2]-DY2[i]) + DAT2_P2[,X2]*(DY2[i]-DY[X1]))/DI))
alt_P3 <- data.frame(alt_P3,c((DAT2_P3[,X1]*(DY[X2]-DY2[i]) + DAT2_P3[,X2]*(DY2[i]-DY[X1]))/DI))
alt_P4 <- data.frame(alt_P4,c((DAT2_P4[,X1]*(DY[X2]-DY2[i]) + DAT2_P4[,X2]*(DY2[i]-DY[X1]))/DI))
}

# biomasses :
BIOM_P1 <- 194.48*(cap_P1[,1]/pi/100)^2*alt_P1[,1]+0.4583
BIOM_P2 <- 194.48*(cap_P2[,1]/pi/100)^2*alt_P2[,1]+0.4583
BIOM_P3 <- 194.48*(cap_P3[,1]/pi/100)^2*alt_P3[,1]+0.4583
BIOM_P4 <- 194.48*(cap_P4[,1]/pi/100)^2*alt_P4[,1]+0.4583
for (i in 2:11){BIOM_P1 <- data.frame(BIOM_P1,194.48*(cap_P1[,i]/pi/100)^2*alt_P1[,i]+0.4583 )}
for (i in 12:23){
BIOM_P1 <- data.frame(BIOM_P1,161.75*(cap_P1[,i]/pi/100)^2*alt_P1[,i]+1.0226 )}
for (i in 24:35){BIOM_P1 <- data.frame(BIOM_P1,142.7956*(cap_P1[,i]/pi/100)^2*alt_P1[,i]+2.7122)}
# on calcule  l'increament biomass
BIOM2_P1 <- BIOM_P1[,2]-BIOM_P1[,1]
for (i in 3:35){BIOM2_P1 <- data.frame(BIOM2_P1, BIOM_P1[,i]-BIOM_P1[,(i-1)])}
for (i in 2:11){BIOM_P2 <- data.frame(BIOM_P2,194.48*(cap_P2[,i]/pi/100)^2*alt_P2[,i]+0.4583 )}
for (i in 12:23){
BIOM_P2 <- data.frame(BIOM_P2,161.75*(cap_P2[,i]/pi/100)^2*alt_P2[,i]+1.0226 )}
for (i in 24:35){BIOM_P2 <- data.frame(BIOM_P2,142.7956*(cap_P2[,i]/pi/100)^2*alt_P2[,i]+2.7122)}
# on calcule  l'increament biomass
BIOM2_P2 <- BIOM_P2[,2]-BIOM_P2[,1]
for (i in 3:35){BIOM2_P2 <- data.frame(BIOM2_P2, BIOM_P2[,i]-BIOM_P2[,(i-1)])}
for (i in 2:11){BIOM_P3 <- data.frame(BIOM_P3,194.48*(cap_P3[,i]/pi/100)^2*alt_P3[,i]+0.4583 )}
for (i in 12:23){
BIOM_P3 <- data.frame(BIOM_P3,161.75*(cap_P3[,i]/pi/100)^2*alt_P3[,i]+1.0226 )}
for (i in 24:35){BIOM_P3 <- data.frame(BIOM_P3,142.7956*(cap_P3[,i]/pi/100)^2*alt_P3[,i]+2.7122)}
# on calcule  l'increament biomass
BIOM2_P3 <- BIOM_P3[,2]-BIOM_P3[,1]
for (i in 3:35){BIOM2_P3 <- data.frame(BIOM2_P3, BIOM_P3[,i]-BIOM_P3[,(i-1)])}
for (i in 2:11){BIOM_P4 <- data.frame(BIOM_P4,194.48*(cap_P4[,i]/pi/100)^2*alt_P4[,i]+0.4583 )}
for (i in 12:23){
BIOM_P4 <- data.frame(BIOM_P4,161.75*(cap_P4[,i]/pi/100)^2*alt_P4[,i]+1.0226 )}
for (i in 24:35){BIOM_P4 <- data.frame(BIOM_P4,142.7956*(cap_P4[,i]/pi/100)^2*alt_P4[,i]+2.7122)}
# on calcule  l'increament biomass
BIOM2_P4 <- BIOM_P4[,2]-BIOM_P4[,1]
for (i in 3:35){BIOM2_P4 <- data.frame(BIOM2_P4, BIOM_P4[,i]-BIOM_P4[,(i-1)])}



DY2 <- c(31,59,90,120,151,181,212,243,273,304,334,365,
        396,424,455,485,516,546,577,608,638,669,699,730,
        761,790,821,851,882,912,943,974,1004,1035,1065,1096)

Apar_P1 <-c();Apar_P2 <-c();Apar_P3 <-c();Apar_P4 <-c()
for (j in it){
Apar_P1 <- c(Apar_P1, sum(dat11$absPAR[dat11$Tree==j][DY2[2]:DY2[3]]))}
for (i in 4:12){x <- c();for (j in it){x <- c(x, sum(dat11$absPAR[dat11$Tree==j][DY2[i-1]:DY2[i]]))}
Apar_P1 <- data.frame(Apar_P1,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dat12$absPAR[dat12$Tree==j][(DY2[i-1]-365):(DY2[i]-365)]))}
Apar_P1 <- data.frame(Apar_P1,x)}
for (i in 25:36){x <- c();for (j in it){x <- c(x, sum(dat13$absPAR[dat13$Tree==j][(DY2[i-1]-730):(DY2[i]-730)]))}
Apar_P1 <- data.frame(Apar_P1,x)}
for (j in it){
Apar_P2 <- c(Apar_P2, sum(dat21$absPAR[dat21$Tree==j][DY2[2]:DY2[3]]))}
for (i in 4:12){x <- c();for (j in it){x <- c(x, sum(dat21$absPAR[dat21$Tree==j][DY2[i-1]:DY2[i]]))}
Apar_P2 <- data.frame(Apar_P2,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dat22$absPAR[dat22$Tree==j][(DY2[i-1]-365):(DY2[i]-365)]))}
Apar_P2 <- data.frame(Apar_P2,x)}
for (i in 25:36){x <- c();for (j in it){x <- c(x, sum(dat23$absPAR[dat23$Tree==j][(DY2[i-1]-730):(DY2[i]-730)]))}
Apar_P2 <- data.frame(Apar_P2,x)}
for (j in it3){
Apar_P3 <- c(Apar_P3, sum(dat31$absPAR[dat31$Tree==j][DY2[2]:DY2[3]]))}
for (i in 4:12){x <- c();for (j in it3){x <- c(x, sum(dat31$absPAR[dat31$Tree==j][DY2[i-1]:DY2[i]]))}
Apar_P3 <- data.frame(Apar_P3,x)}
for (i in 13:24){x <- c();for (j in it3){x <- c(x, sum(dat32$absPAR[dat32$Tree==j][(DY2[i-1]-365):(DY2[i]-365)]))}
Apar_P3 <- data.frame(Apar_P3,x)}
for (i in 25:36){x <- c();for (j in it3){x <- c(x, sum(dat33$absPAR[dat33$Tree==j][(DY2[i-1]-730):(DY2[i]-730)]))}
Apar_P3 <- data.frame(Apar_P3,x)}
for (j in it){
Apar_P4 <- c(Apar_P4, sum(dat41$absPAR[dat41$Tree==j][DY2[2]:DY2[3]]))}
for (i in 4:12){x <- c();for (j in it){x <- c(x, sum(dat41$absPAR[dat41$Tree==j][DY2[i-1]:DY2[i]]))}
Apar_P4 <- data.frame(Apar_P4,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dat42$absPAR[dat42$Tree==j][(DY2[i-1]-365):(DY2[i]-365)]))}
Apar_P4 <- data.frame(Apar_P4,x)}
for (i in 25:36){x <- c();for (j in it){x <- c(x, sum(dat43$absPAR[dat43$Tree==j][(DY2[i-1]-730):(DY2[i]-730)]))}
Apar_P4 <- data.frame(Apar_P4,x)}


Biom2_P1 <-c();apar_P1 <-c()
Biom2_P2 <-c();apar_P2 <-c()
Biom2_P3 <-c();apar_P3 <-c()
Biom2_P4 <-c();apar_P4 <-c()
for (i in 1:34){
#Biom2_P1 <- c(Biom2_P1, BIOM2_P1[,i]);apar_P1 <-c(apar_P1,Apar_P1[,i])
Biom2_P1 <- c(Biom2_P1,BIOM2_P1[,i][84:71],BIOM2_P1[,i][(5*14):(4*14+1)],BIOM2_P1[,i][(3*14+1):(4*14)],BIOM2_P1[,i][(3*14):(2*14+1)],BIOM2_P1[,i][(1*14+1):(2*14)],BIOM2_P1[,i][(1*14):(0*14+1)])
apar_P1 <-c(apar_P1,Apar_P1[,i])
Biom2_P2 <- c(Biom2_P2, BIOM2_P2[,i]);apar_P2 <-c(apar_P2,Apar_P2[,i])
Biom2_P3 <- c(Biom2_P3, BIOM2_P3[,i]);apar_P3 <-c(apar_P3,Apar_P3[,i])
Biom2_P4 <- c(Biom2_P4, BIOM2_P4[,i]);apar_P4 <-c(apar_P4,Apar_P4[,i])}
cap2_P1<-c();alt2_P1<-c(); Biom_P1 <-c()
cap2_P2<-c();alt2_P2<-c(); Biom_P2 <-c()
cap2_P3<-c();alt2_P3<-c(); Biom_P3 <-c()
cap2_P4<-c();alt2_P4<-c(); Biom_P4 <-c()
for (i in 2:35){
#cap2_P1 <- c(cap2_P1,cap_P1[,i][71:84],cap_P1[,i][(4*14+1):(5*14)],cap_P1[,i][(4*14):(3*14+1)],cap_P1[,i][(2*14+1):(3*14)],cap_P1[,i][(2*14):(1*14+1)],cap_P1[,i][(1):(14)])
#alt2_P1 <- c(alt2_P1,alt_P1[,i][71:84],alt_P1[,i][(4*14+1):(5*14)],alt_P1[,i][(4*14):(3*14+1)],alt_P1[,i][(2*14+1):(3*14)],alt_P1[,i][(2*14):(1*14+1)],alt_P1[,i][(1):(14)])
#cap2_P1 <- c(cap2_P1,cap_P1[,i][14:1],cap_P1[,i][(1*14+1):(2*14)],cap_P1[,i][(3*14):(2*14+1)],cap_P1[,i][(3*14+1):(4*14)],cap_P1[,i][(5*14):(4*14+1)],cap_P1[,i][(5*14+1):(6*14)])
#alt2_P1 <- c(alt2_P1,alt_P1[,i][14:1],alt_P1[,i][(1*14+1):(2*14)],alt_P1[,i][(3*14):(2*14+1)],alt_P1[,i][(3*14+1):(4*14)],alt_P1[,i][(5*14):(4*14+1)],alt_P1[,i][(5*14+1):(6*14)])
#cap2_P1 <- c(cap2_P1,cap_P1[,i][1:14],cap_P1[,i][(2*14):(1*14+1)],cap_P1[,i][(2*14+1):(3*14)],cap_P1[,i][(4*14):(3*14+1)],cap_P1[,i][(4*14+1):(5*14)],cap_P1[,i][(6*14):(5*14+1)])
#alt2_P1 <- c(alt2_P1,alt_P1[,i][1:14],alt_P1[,i][(2*14):(1*14+1)],alt_P1[,i][(2*14+1):(3*14)],alt_P1[,i][(4*14):(3*14+1)],alt_P1[,i][(4*14+1):(5*14)],alt_P1[,i][(6*14):(5*14+1)])
cap2_P1 <- c(cap2_P1,cap_P1[,i][84:71],cap_P1[,i][(5*14):(4*14+1)],cap_P1[,i][(3*14+1):(4*14)],cap_P1[,i][(3*14):(2*14+1)],cap_P1[,i][(1*14+1):(2*14)],cap_P1[,i][(1*14):(0*14+1)])
alt2_P1 <- c(alt2_P1,alt_P1[,i][84:71],alt_P1[,i][(5*14):(4*14+1)],alt_P1[,i][(3*14+1):(4*14)],alt_P1[,i][(3*14):(2*14+1)],alt_P1[,i][(1*14+1):(2*14)],alt_P1[,i][(1*14):(0*14+1)])
Biom_P1 <- c(Biom_P1,BIOM_P1[,i][84:71],BIOM_P1[,i][(5*14):(4*14+1)],BIOM_P1[,i][(3*14+1):(4*14)],BIOM_P1[,i][(3*14):(2*14+1)],BIOM_P1[,i][(1*14+1):(2*14)],BIOM_P1[,i][(1*14):(0*14+1)])
#cap2_P1 <- c(cap2_P1, cap_P1[,i]);alt2_P1 <- c(alt2_P1, alt_P1[,i]); Biom_P1 <- c(Biom_P1, BIOM_P1[,i])
cap2_P2 <- c(cap2_P2, cap_P2[,i]);alt2_P2 <- c(alt2_P2, alt_P2[,i]); Biom_P2 <- c(Biom_P2, BIOM_P2[,i])
cap2_P3 <- c(cap2_P3, cap_P3[,i]);alt2_P3 <- c(alt2_P3, alt_P3[,i]); Biom_P3 <- c(Biom_P3, BIOM_P3[,i])
cap2_P4 <- c(cap2_P4, cap_P4[,i]);alt2_P4 <- c(alt2_P4, alt_P4[,i]); Biom_P4 <- c(Biom_P4, BIOM_P4[,i])}


mn <- c(); for (i in 1:34){mn <- c(mn,rep(i,84))}
isp <- c(rep(dat11$Spec[1:84],10),rep(dat12$Spec[1:84],12),rep(dat13$Spec[1:84],12))
plot(mn,alt2_P1)
points(mn[isp==3],alt2_P1[isp==3])
points(mn[isp==2],alt2_P1[isp==2],col=2)
points(mn[isp==1],alt2_P1[isp==1],col=3)

Biom2_P1 <- replace(Biom2_P1,c(Biom2_P1 <=0),NA)
apar_P1 <- replace(apar_P1,c(apar_P1==0),NA)
Biom2_P2 <- replace(Biom2_P2,c(Biom2_P2 <=0),NA)
apar_P2 <- replace(apar_P2,c(apar_P2==0),NA)
Biom2_P3 <- replace(Biom2_P3,c(Biom2_P3 <=0),NA)
apar_P3 <- replace(apar_P3,c(apar_P3==0),NA)
Biom2_P4 <- replace(Biom2_P4,c(Biom2_P4 <=0),NA)
apar_P4 <- replace(apar_P4,c(apar_P4==0),NA)

MON <- c(); for (i in 3:36){ MON <- c(MON, rep(i,84))}
isp1 <- c(rep(dat11$Spec[1:84],10),rep(dat12$Spec[1:84],12),rep(dat13$Spec[1:84],12))
isp2 <- c(rep(dat21$Spec[1:84],10),rep(dat22$Spec[1:84],12),rep(dat23$Spec[1:84],12))
isp3 <- c(rep(dat31$Spec[1:84],10),rep(dat32$Spec[1:84],12),rep(dat33$Spec[1:84],12))
isp4 <- c(rep(dat41$Spec[1:84],10),rep(dat42$Spec[1:84],12),rep(dat43$Spec[1:84],12))
TT1 <- isp1;TT2 <- isp2;TT3 <- isp3;TT4 <- isp4

MAT <- data.frame(MON,TT1,TT2,TT3,TT4,Biom2_P1,Biom2_P2,Biom2_P3,Biom2_P4,apar_P1,apar_P2,apar_P3,apar_P4)
write.table(MAT,'Out_month.txt',col.names=T,row.names=F)


MSLUE<-c();MMLUE<-c();MBLUE<-c()
sdSLUE<-c();sdMLUE<-c();sdBLUE<-c()
MSBIOM2<-c();MMBIOM2<-c();MBBIOM2<-c()
sdSBIOM2<-c();sdMBIOM2<-c();sdBBIOM2<-c()
MLUE1 <-c(); sdLUE1 <-c();MBIOM1 <-c();sdBIOM1<-c()
MLUE2 <-c(); sdLUE2 <-c();MBIOM2 <-c();sdBIOM2<-c()
MLUE3 <-c(); sdLUE3 <-c();MBIOM3 <-c();sdBIOM3<-c()
MLUE4 <-c(); sdLUE4 <-c();MBIOM4 <-c();sdBIOM4<-c()
  for (i in c(3:12,14:24,26:36)){
MSBIOM2 <- c(MSBIOM2, mean(c(Biom2_P1[MON==i][TT1[MON==i]==1],Biom2_P2[MON==i][TT2[MON==i]==1],Biom2_P3[MON==i][TT3[MON==i]==1],Biom2_P4[MON==i][TT4[MON==i]==1]),na.rm=T))
MMBIOM2 <- c(MMBIOM2, mean(c(Biom2_P1[MON==i][TT1[MON==i]==2],Biom2_P2[MON==i][TT2[MON==i]==2],Biom2_P3[MON==i][TT3[MON==i]==2],Biom2_P4[MON==i][TT4[MON==i]==2]),na.rm=T))
MBBIOM2 <- c(MBBIOM2, mean(c(Biom2_P1[MON==i][TT1[MON==i]==3],Biom2_P2[MON==i][TT2[MON==i]==3],Biom2_P3[MON==i][TT3[MON==i]==3],Biom2_P4[MON==i][TT4[MON==i]==3]),na.rm=T))
sdSBIOM2 <- c(sdSBIOM2, sd(c(Biom2_P1[MON==i][TT1[MON==i]==1],Biom2_P2[MON==i][TT2[MON==i]==1],Biom2_P3[MON==i][TT3[MON==i]==1],Biom2_P4[MON==i][TT4[MON==i]==1]),na.rm=T))
sdMBIOM2 <- c(sdMBIOM2, sd(c(Biom2_P1[MON==i][TT1[MON==i]==2],Biom2_P2[MON==i][TT2[MON==i]==2],Biom2_P3[MON==i][TT3[MON==i]==2],Biom2_P4[MON==i][TT4[MON==i]==2]),na.rm=T))
sdBBIOM2 <- c(sdBBIOM2, sd(c(Biom2_P1[MON==i][TT1[MON==i]==3],Biom2_P2[MON==i][TT2[MON==i]==3],Biom2_P3[MON==i][TT3[MON==i]==3],Biom2_P4[MON==i][TT4[MON==i]==3]),na.rm=T))
MSLUE <- c(MSLUE, mean(c(#Biom2_P1[MON==i][TT1[MON==i]==1]/apar_P1[MON==i][TT1[MON==i]==1],
                         Biom2_P2[MON==i][TT2[MON==i]==1]/apar_P2[MON==i][TT2[MON==i]==1],
                         #Biom2_P3[MON==i][TT3[MON==i]==1]/apar_P3[MON==i][TT3[MON==i]==1]#,
                         Biom2_P4[MON==i][TT4[MON==i]==1]/apar_P4[MON==i][TT4[MON==i]==1]
                         ),na.rm=T))
MMLUE <- c(MMLUE, mean(c(#Biom2_P1[MON==i][TT1[MON==i]==2]/apar_P1[MON==i][TT1[MON==i]==2],
                         Biom2_P2[MON==i][TT2[MON==i]==2]/apar_P2[MON==i][TT2[MON==i]==2],
                         #Biom2_P3[MON==i][TT3[MON==i]==2]/apar_P3[MON==i][TT3[MON==i]==2]#,
                         Biom2_P4[MON==i][TT4[MON==i]==2]/apar_P4[MON==i][TT4[MON==i]==2]
                         ),na.rm=T))
MBLUE <- c(MBLUE, mean(c(#Biom2_P1[MON==i][TT1[MON==i]==3]/apar_P1[MON==i][TT1[MON==i]==3],
                         Biom2_P2[MON==i][TT2[MON==i]==3]/apar_P2[MON==i][TT2[MON==i]==3],
                         #Biom2_P3[MON==i][TT3[MON==i]==3]/apar_P3[MON==i][TT3[MON==i]==3]#,
                         Biom2_P4[MON==i][TT4[MON==i]==3]/apar_P4[MON==i][TT4[MON==i]==3]
                         ),na.rm=T))
sdSLUE <- c(sdSLUE, sd(c(#Biom2_P1[MON==i][TT1[MON==i]==1]/apar_P1[MON==i][TT1[MON==i]==1],
                         Biom2_P2[MON==i][TT2[MON==i]==1]/apar_P2[MON==i][TT2[MON==i]==1],
                         #Biom2_P3[MON==i][TT3[MON==i]==1]/apar_P3[MON==i][TT3[MON==i]==1]#,
                         Biom2_P4[MON==i][TT4[MON==i]==1]/apar_P4[MON==i][TT4[MON==i]==1]
                         ),na.rm=T))
sdMLUE <- c(sdMLUE, sd(c(#Biom2_P1[MON==i][TT1[MON==i]==2]/apar_P1[MON==i][TT1[MON==i]==2],
                         Biom2_P2[MON==i][TT2[MON==i]==2]/apar_P2[MON==i][TT2[MON==i]==2],
                         #Biom2_P3[MON==i][TT3[MON==i]==2]/apar_P3[MON==i][TT3[MON==i]==2]#,
                         Biom2_P4[MON==i][TT4[MON==i]==2]/apar_P4[MON==i][TT4[MON==i]==2]
                         ),na.rm=T))
sdBLUE <- c(sdBLUE, sd(c(#Biom2_P1[MON==i][TT1[MON==i]==3]/apar_P1[MON==i][TT1[MON==i]==3],
                         Biom2_P2[MON==i][TT2[MON==i]==3]/apar_P2[MON==i][TT2[MON==i]==3],
                         #Biom2_P3[MON==i][TT3[MON==i]==3]/apar_P3[MON==i][TT3[MON==i]==3]#,
                         Biom2_P4[MON==i][TT4[MON==i]==3]/apar_P4[MON==i][TT4[MON==i]==3]
                         ),na.rm=T))
MBIOM1 <- c(MBIOM1, mean(c(Biom2_P1[MON==i]),na.rm=T))
MBIOM2 <- c(MBIOM2, mean(c(Biom2_P2[MON==i]),na.rm=T))
MBIOM3 <- c(MBIOM3, mean(c(Biom2_P3[MON==i]),na.rm=T))
MBIOM4 <- c(MBIOM4, mean(c(Biom2_P4[MON==i]),na.rm=T))
sdBIOM1 <- c(sdBIOM1, sd(c(Biom2_P1[MON==i]),na.rm=T))
sdBIOM2 <- c(sdBIOM2, sd(c(Biom2_P2[MON==i]),na.rm=T))
sdBIOM3 <- c(sdBIOM3, sd(c(Biom2_P3[MON==i]),na.rm=T))
sdBIOM4 <- c(sdBIOM4, sd(c(Biom2_P4[MON==i]),na.rm=T))
MLUE1 <- c(MLUE1, mean(c(Biom2_P1[MON==i]/apar_P1[MON==i]),na.rm=T))
MLUE2 <- c(MLUE2, mean(c(Biom2_P2[MON==i]/apar_P2[MON==i]),na.rm=T))
MLUE3 <- c(MLUE3, mean(c(Biom2_P3[MON==i]/apar_P3[MON==i]),na.rm=T))
MLUE4 <- c(MLUE4, mean(c(Biom2_P4[MON==i]/apar_P4[MON==i]),na.rm=T))
sdLUE1 <- c(sdLUE1, sd(c(Biom2_P1[MON==i]/apar_P1[MON==i]),na.rm=T))
sdLUE2 <- c(sdLUE2, sd(c(Biom2_P2[MON==i]/apar_P2[MON==i]),na.rm=T))
sdLUE3 <- c(sdLUE3, sd(c(Biom2_P3[MON==i]/apar_P3[MON==i]),na.rm=T))
sdLUE4 <- c(sdLUE4, sd(c(Biom2_P4[MON==i]/apar_P4[MON==i]),na.rm=T))
}


MON2 <- c(3:12,14:24,26:36)

par(mfrow=c(2,1))
par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(MON2,MBLUE,xlab='Age (months)',ylab='Light use efficiency (kgDM/MJ)',cex.lab=1,lty=1,pch=16,col=2,lwd=2,ylim=c(0,0.003),type='b',xlim=c(0,36),cex.lab=1)
points(MON2,MSLUE,pch=16,lty=1,col=1,lwd=2,type='b')
points(MON2,MMLUE,pch=16,lty=1,col=4,lwd=2,type='b')
for (i in MON2){
  add.errorbars (x=i,y=MMLUE[MON2==i],SE=sdMLUE[MON2==i],direction='vert',barlen=0.04,color=4)}
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Small trees','Medium trees','Big trees'),col=c(1,4,2),pch=16,lty=1,bg='white',box.col=0,cex=1)
box()
abline(v=12+0.5/31,lty=2)
abline(v=24+0.5/31,lty=2)
text(8,0.0028,'2010',font=4)
text(16,0.0028,'2011',font=4)
text(28,0.0028,'2012',font=4)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(MON2,MLUE2,xlab='Age (months)',ylab='Light use efficiency (kgDM/MJ)',cex.lab=1,lty=1,pch=16,col=2,lwd=2,ylim=c(0,0.003),type='b',xlim=c(0,36),cex.lab=1)
#points(MON2,MLUE1,pch=16,lty=1,col=1,lwd=2,type='b')
points(MON2,MLUE3,pch=16,lty=1,col=3,lwd=2,type='b')
points(MON2,MLUE4,pch=16,lty=1,col=4,lwd=2,type='b')
for (i in MON2){
  add.errorbars (x=i,y=MLUE2[MON2==i],SE=sdLUE2[MON2==i],direction='vert',barlen=0.04,color=2)}
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('Plot 2','Plot 3','Plot 4'),col=2:4,pch=16,lty=1,bg='white',box.col=0,cex=1)
box()
abline(v=12+0.5/31,lty=2)
abline(v=24+0.5/31,lty=2)
text(8,0.0028,'2010',font=4)
text(16,0.0028,'2011',font=4)
text(28,0.0028,'2012',font=4)
