require (Maeswrap)

set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use/Simulations")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/runMAESPA_2.r")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX water use")
set8<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test_MAESPA")
set9<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/runMAESPA")
set10<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�/Test2")

setwd(set1)

dat131 <- read.table('4_watbal.dat',skip=37)
names(dat131) <- c('day', 'hour', 'wsoil', 'wsoilroot', 'ppt', 'canopystore', 'evapstore', 'drainstore', 'tfall', 'et', 'etmeas', 'discharge', 'overflow', 'weightedswp', 'totestevap', 'drythick', 'soilevap', 'soilmoist', 'fsoil', 'qh', 'qe', 'qn', 'qc', 'rglobund', 'rglobabv', 'radinterc', 'rnet', 'totlai', 'tair', 'soilt1', 'soilt2','fracw1', 'fracw2')
dat <- read.table("flux2010.txt",header=T)
attach(dat)
DATE2 <- DATE - min(DATE)+1

setwd(set9)
#dat231 <- readwatbal('watbal_NT_2cm.dat')
dat331 <- readwatbal('watbal_ST_4cm.dat')
dat331 <- dat331[2:(48*31+1),]
#dat231 <- dat231[2:(48*31+1),]
setwd(set10)
dat231 <- readwatbal('watbal_ST_2cm.dat')
dat231 <- dat231[2:(48*31+1),]
dat431 <- readwatbal('watbal_ST_6cm.dat')
dat431 <- dat431[2:(48*31+1),]


LESIM1 <- c(dat131$evapstore[1:dim(dat131)[1]]/(30*60)*2.45*10^6 +
            dat131$soilevap[1:dim(dat131)[1]]*2.45*10^6/(30*60)   +
            dat131$et[1:dim(dat131)[1]]*2.45*10^6/(30*60))
LESIM2 <- c(dat231$evapstore[1:dim(dat231)[1]]/(30*60)*2.45*10^6 +
            dat231$soilevap[1:dim(dat231)[1]]*2.45*10^6/(30*60)   +
            dat231$et[1:dim(dat231)[1]]*2.45*10^6/(30*60))
LESIM3 <- c(dat331$evapstore[1:dim(dat331)[1]]/(30*60)*2.45*10^6 +
            dat331$soilevap[1:dim(dat331)[1]]*2.45*10^6/(30*60)   +
            dat331$et[1:dim(dat331)[1]]*2.45*10^6/(30*60))
LESIM4 <- c(dat431$evapstore[1:dim(dat431)[1]]/(30*60)*2.45*10^6 +
            dat431$soilevap[1:dim(dat431)[1]]*2.45*10^6/(30*60)   +
            dat431$et[1:dim(dat431)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(dat131$day[1:(48*304)],dat131$hour[1:(48*304)],LESIM1[1:(48*304)],c(LESIM2,rep(NA,14592-length(LESIM2))),c(LESIM3,rep(NA,14592-length(LESIM3))),c(LESIM4,rep(NA,14592-length(LESIM4))))
names(SIMDAT) <- c('DATE2','HOUR2','lesim1','lesim2','lesim3','lesim4')

HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))


par(mar=c(4.5,4.5,1,1))
par(mfrow=c(1,2))
plot(dat131$soilevap[1:dim(dat231)[1]])
points(dat231$soilevap,col=2)
sum(dat231$soilevap)
sum(dat131$soilevap[1:dim(dat231)[1]])
sum(dat231$soilevap) / sum(dat131$soilevap[1:dim(dat231)[1]])

plot(dat131$soilevap[1:dim(dat331)[1]])
points(dat231$soilevap[1:dim(dat331)[1]],col=2)
points(dat331$soilevap,col=4)
sum(dat331$soilevap)
sum(dat231$soilevap[1:dim(dat331)[1]])
sum(dat131$soilevap[1:dim(dat331)[1]])

lesim1<-c();lesim2<-c();lesim3<-c();Dleobs<-c()
lesim4 <<-c()
for (i in 1:48){
lesim1 <- c(lesim1, mean(LESIM1[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim2 <- c(lesim2, mean(LESIM2[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim3 <- c(lesim3, mean(LESIM3[1:min(c(1488,length(LESIM3)))][FH$HOUR2[1:min(c(1488,length(LESIM3)))]==i],na.rm=T))
lesim4 <- c(lesim4, mean(LESIM4[1:min(c(1488,length(LESIM4)))][FH$HOUR2[1:min(c(1488,length(LESIM4)))]==i],na.rm=T))
Dleobs <- c(Dleobs, mean(FH$LE[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM2[1:336],col=2,type='l',lwd=2)
points(LESIM3[1:336],col=3,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
points(LESIM4[1:336],col=5,type='l',lwd=2)
#legend('topleft',legend=c('LE meas','LE sim ini','LE sim NT 2cm','LE sim T 2cm'),col=c(1,4,2,3),lty=1,pch=NA,bty='n')
#legend('topleft',legend=c('LE meas','LE sim NT 2cm'),col=c(1,2),lty=1,pch=NA,bty='n')
legend('topleft',legend=c('LE meas','LE sim ini','LE sim T 2cm','LE sim T 4cm','LE sim T 6cm'),col=c(1,4,2,3,5),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesim2,col=2,type='l',lwd=2)
points(lesim3,col=3,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
points(lesim4,col=5,type='l',lwd=2)
#legend('topleft',legend=c('LE meas','LE sim NT 2cm'),col=c(1,2),lty=1,pch=NA,bty='n')
#legend('topleft',legend=c('LE meas','LE sim ini','LE sim NT 2cm','LE sim T 2cm'),col=c(1,4,2,3),lty=1,pch=NA,bty='n')
legend('topleft',legend=c('LE meas','LE sim ini','LE sim T 2cm','LE sim T 4cm','LE sim T 6cm'),col=c(1,4,2,3,5),lty=1,pch=NA,bty='n')


# liti�re de 3cm, on fait varier la tortuoisit�

setwd(set9)
dat1 <- readwatbal('watbal_T15mod.dat')
dat1 <- dat1[2:(48*31+1),]
setwd(set10)
dat2 <- readwatbal('watbal_T02.dat')
dat2 <- dat2[2:(48*31+1),]
dat3 <- readwatbal('watbal_T14.dat')
dat3 <- dat3[2:(48*31+1),]


LESIM2 <- c(dat1$evapstore[1:dim(dat1)[1]]/(30*60)*2.45*10^6 +
            dat1$soilevap[1:dim(dat1)[1]]*2.45*10^6/(30*60)   +
            dat1$et[1:dim(dat1)[1]]*2.45*10^6/(30*60))
LESIM3 <- c(dat2$evapstore[1:dim(dat2)[1]]/(30*60)*2.45*10^6 +
            dat2$soilevap[1:dim(dat2)[1]]*2.45*10^6/(30*60)   +
            dat2$et[1:dim(dat2)[1]]*2.45*10^6/(30*60))
LESIM4 <- c(dat3$evapstore[1:dim(dat3)[1]]/(30*60)*2.45*10^6 +
            dat3$soilevap[1:dim(dat3)[1]]*2.45*10^6/(30*60)   +
            dat3$et[1:dim(dat3)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(dat1$day[1:(48*304)],dat1$hour[1:(48*304)],LESIM1[1:(48*304)],c(LESIM2,rep(NA,14592-length(LESIM2))),c(LESIM3,rep(NA,14592-length(LESIM3))),c(LESIM4,rep(NA,14592-length(LESIM4))))
names(SIMDAT) <- c('DATE2','HOUR2','lesim1','lesim2','lesim3','lesim4')

HOUR2 <-c()
for (i in 1:dim(dat)[1]){
if (MIN[i]==15){
HOUR2 <- c(HOUR2, 2*HOUR[i]+1)}
else if (MIN[i]==45){
HOUR2 <- c(HOUR2, 2*HOUR[i]+2)}}
OBSDAT <- data.frame(DATE2,HOUR2,LE,H,Fc)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATE2','HOUR2'))

lesim1<-c();lesim2<-c();lesim3<-c();Dleobs<-c()
lesim4 <<-c()
for (i in 1:48){
lesim1 <- c(lesim1, mean(LESIM1[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim2 <- c(lesim2, mean(LESIM2[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim3 <- c(lesim3, mean(LESIM3[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
lesim4 <- c(lesim4, mean(LESIM4[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
Dleobs <- c(Dleobs, mean(FH$LE[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM2[1:336],col=2,type='l',lwd=2)
points(LESIM3[1:336],col=3,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
points(LESIM4[1:336],col=5,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE sim Tort 1.4','LE sim Tort 2.5','LE sim Tort 0.2'),col=c(1,4,5,2,3),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2,ylim=c(0,250))
points(lesim2,col=2,type='l',lwd=2)
points(lesim3,col=3,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
points(lesim4,col=5,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE sim Tort 1.4','LE sim Tort 2.5','LE sim Tort 0.2'),col=c(1,4,5,2,3),lty=1,pch=NA,bty='n')

# test sans litter
setwd(set9)
dat4 <- readwatbal('watbal_NL_T066.dat')
dat4 <- dat4[2:(48*31+1),]

LESIM5 <- c(dat4$evapstore[1:dim(dat4)[1]]/(30*60)*2.45*10^6 +
            dat4$soilevap[1:dim(dat4)[1]]*2.45*10^6/(30*60)   +
            dat4$et[1:dim(dat4)[1]]*2.45*10^6/(30*60))
lesim5 <-c()
for (i in 1:48){
lesim5 <- c(lesim5, mean(LESIM5[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIM5[1:336],col=2,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE no litter'),col=c(1,4,2),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesim5,col=2,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE no litter'),col=c(1,4,2),lty=1,pch=NA,bty='n')


# test ancienne version, nouvelle version avec 1 seul couche (JU2)
require(Maeswrap)

setwd(set8)
day1 <- readdayflux('Dayflx_JU1.dat') 
bal1 <- readwatbal('watbal_JU1.dat')
wat1 <- read.table('watlay_JU1.dat')
tem1 <- read.table('watsoilt_JU1.dat')
setwd(set9)
day2 <- readdayflux('Dayflx_JU2.dat')
bal2 <- readwatbal('watbal_JU2.dat')
wat2 <- read.table('watlay_JU2.dat')
tem2 <- read.table('watsoilt_JU2.dat')

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(1,3))
plot(day1$totPs/day2$totPs)
abline(h=1,col=2,lwd=2)
plot(day1$totLE1/day2$totLE1)
abline(h=1,col=2,lwd=2)
plot(day1$totH/day2$totH)
abline(h=1,col=2,lwd=2)

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,4))
plot(bal1$soilevap,bal2$soilevap)
abline(0,1,col=2,lwd=2)
plot(-bal1$qh,-bal2$qh)
abline(0,1,col=2,lwd=2)
plot(-bal1$qe,-bal2$qe)
abline(0,1,col=2,lwd=2)
plot(bal1$qn,bal2$qn)
abline(0,1,col=2,lwd=2)
plot(-bal1$qc,-bal2$qc)
abline(0,1,col=2,lwd=2)
plot(bal1$soilt1,bal2$soilt1)
abline(0,1,col=2,lwd=2)
plot(bal1$soilt2,bal2$soilt2)
abline(0,1,col=2,lwd=2)
plot(bal1$fracw1,bal2$fracw1)
abline(0,1,col=2,lwd=2)

LESIMmod <- c(bal2$evapstore[1:dim(bal2)[1]]/(30*60)*2.45*10^6 +
            bal2$soilevap[1:dim(bal2)[1]]*2.45*10^6/(30*60)   +
            bal2$et[1:dim(bal2)[1]]*2.45*10^6/(30*60))
lesimMOD <<-c()
for (i in 1:48){
lesimMOD <- c(lesimMOD, mean(LESIMmod[1:1488][FH$HOUR2[1:1488]==i],na.rm=T))
}

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(FH$LE[1:336],col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(LESIMmod[1:336],col=2,type='l',lwd=2)
points(LESIM1[1:336],col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE 1layer mod tort=1.5'),col=c(1,4,2),lty=1,pch=NA,bty='n')

plot(Dleobs,col=1,type='l',lwd=2,xlab='Half-hour of the day',ylab='LE (W/m�)',cex.lab=1.2)
points(lesimMOD,col=2,type='l',lwd=2)
points(lesim1,col=4,type='l',lwd=2)
legend('topleft',legend=c('LE meas','LE sim ini','LE 1layer mod tort=1.5'),col=c(1,4,2),lty=1,pch=NA,bty='n')


# effet tortpar = 1.5
setwd(set10)
day <- readdayflux('Dayflx_test.dat')[1:(304*84),]
bal <- readwatbal('watbal_test.dat')[2:(304*48+1),]
wat <- read.table('watlay_test.dat')[1:(304*48),]
tem <- read.table('watsoilt_test.dat')[1:(304*48),]
hrf <- read.table('hrflux_test.dat',skip=40)
names(hrf) <- c('DOY','Tree','Spec','HOUR','hrPAR','hrNIR','hrTHM','hrPs','hrRf','hrRmW','hrLE','LECAN','Gscan','Gbhcan','hrH','TCAN','ALMAX','PSIL','PSILMIN','CI','TAIR','VPD','PAR')

setwd(set1)
datLE <- read.table("LEflux_bon.txt",header=T)
datH <- read.table("Hflux_bon.txt",header=T)
#attach(dat)
DATEH <- datH$DATE - min(datH$DATE)+1
DATELE <- datLE$DATE - min(datLE$DATE)+1

HT<-c()
for (i in 1:304){
for (j in 1:48){
HT <- c(HT, sum(hrf$hrH[hrf$DOY==i][hrf$HOUR[hrf$DOY==1]==j],na.rm=T)/496.05)
}}

HSIM <- -bal$qh[1:dim(bal)[1]] +HT*1000

LESIM <- c(bal$evapstore[1:dim(bal)[1]]/(30*60)*2.45*10^6 +
            bal$soilevap[1:dim(bal)[1]]*2.45*10^6/(30*60)   +
            bal$et[1:dim(bal)[1]]*2.45*10^6/(30*60))
SIMDAT <- data.frame(bal$day[1:(48*304)],bal$day[1:(48*304)],bal$hour[1:(48*304)],LESIM[1:(48*304)],HSIM)
names(SIMDAT) <- c('DATEH','DATELE','HOUR2','lesim','hsim')

HOUR2 <-c()
for (i in 1:dim(datLE)[1]){
if (datLE$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+1)}
else if (datLE$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datLE$HOUR[i]+2)}}
OBSDAT <- data.frame(DATELE,HOUR2,datLE$LE)
FLE <- merge(SIMDAT, OBSDAT,all=T,by=c('DATELE','HOUR2'))

HOUR2 <-c()
for (i in 1:dim(datH)[1]){
if (datH$MIN[i]==15){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+1)}
else if (datH$MIN[i]==45){
HOUR2 <- c(HOUR2, 2*datH$HOUR[i]+2)}}
OBSDAT <- data.frame(DATEH,HOUR2,datH$H)
FH <- merge(SIMDAT, OBSDAT,all=T,by=c('DATEH','HOUR2'))

obsLE <- FLE$datLE.LE
obsH <- FH$datH.H
lesim <- FLE$lesim
hsim <- FH$hsim

mois <- c(rep(1,48*31),rep(2,48*28),rep(3,48*31),rep(4,48*30),rep(5,48*31),rep(6,48*30),rep(7,48*31),rep(8,48*31),rep(9,48*30),rep(10,48*31))
hour <- rep(1:48,304)

LES <-c() ; LEO <-c()
HES <-c() ; HEO <-c()
for (i in 1:10){
for (j in 1:48){
LES <- c(LES, mean(lesim[mois==i][hour[mois==i]==j],na.rm=T))
LEO <- c(LEO, mean(obsLE[mois==i][hour[mois==i]==j],na.rm=T))
HES <- c(HES, mean(hsim[mois==i][hour[mois==i]==j],na.rm=T))
HEO <- c(HEO, mean(obsH[mois==i][hour[mois==i]==j],na.rm=T))
}}

MOI <- c('Jan.','Feb.','Mar.','Apr.','May','Jun.','Jul.','Aug.','Sep.','Oct.')

par(mfrow=c(2,1))
par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(LEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Latent heat flux (W/m�)',cex.lab=1.2,ylim=c(-10,350))
points(LES,type='l',col=4,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=275,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=325,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured latent heat flux','Simulated latent heat flux'),col=c(1,4),lty=1,pch=NA,bg='white',box.col=0)
box()

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(HEO,type='l',lwd=2,axes=F,xlab='Hout of the day (2010)',ylab='Sensible heat flux (W/m�)',cex.lab=1.2,ylim=c(-50,350))
points(HES,type='l',col=2,lwd=2)
axis(1,at=c(0:20)*24,labels=c(0,rep(c(12,24),10)))
axis(2,at=c(0:7)*50,labels=c(0:7)*50)
grid(nx=NA,ny=NULL,col=1)
for (i in 48*c(1:9)){ abline(v = i,lty=2)}
for (i in 1:9){ text(48*(i-1)+24,y=275,MOI[i],font=3,cex=1.2)}
text(48*(10-1)+24,y=325,MOI[10],font=3,cex=1.2)
legend('topleft',legend=c('Measured H','Simulated H'),col=c(1,2),lty=1,pch=NA,bg='white',box.col=0)
box()



setwd(set1)
day4 <- readdayflux('10_Dayflx.dat') 

