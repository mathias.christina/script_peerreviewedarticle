set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/racines")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)
dat1 <- read.table('biomasse_g_dm3.txt',header=T)

Dp <- c('0-15','15-30','30-65','65-100','100-200','200-300','300-400','400-500','500-600')

B18 <-  dat1[,2]*c(1.5,1.5,3.5,3.5,rep(10,13))
B30 <- dat1[,3]*c(1.5,1.5,3.5,3.5,rep(10,13))
B42 <- dat1[,4]*c(1.5,1.5,3.5,3.5,rep(10,13))
B71 <- dat1[,5]*c(1.5,1.5,3.5,3.5,rep(10,13))

B18new <- B18/ sum(B18,na.rm=T)
B30new <- B30/ sum(B30,na.rm=T)
B42new <- B42/ sum(B42,na.rm=T)
B71new <- B71/ sum(B71,na.rm=T)

RootMasstot18 <- sum(B18,na.rm=T)/1000 *100
RootMasstot30 <- sum(B30,na.rm=T)/1000 *100
RootMasstot42 <- sum(B42,na.rm=T)/1000 *100
RootMasstot71 <- sum(B71,na.rm=T)/1000 *100

B42n <- c(0.1701618910,0.0579042187,0.0811774916,0.0487854394,0.3605221213,0.1378736104,0.0461679818,0.0343705669,0.0206237877,0.0366026917,0.0021534223,0.0015991652,0.0008589488,0.0006630271,0.0002328645,0.0001,0.0003027717)
B42n <- B42n/sum(B42n)

par(yaxs='i')
par(mar=c(4.5,4.5,2,1))
barplot(B18new[1:9],width=c(15,15,35,35,rep(100,13))[1:9],
        space=0,ylab='Fraction of root biomass',xlab='Depth (cm)',cex.lab=1.2)
axis(1,at=c(0,15,30,65,100,200,300,400,500,600))
title('18 months')

par(yaxs='i')
par(mar=c(4.5,4.5,2,1))
barplot(B30new[1:15],width=c(15,15,35,35,rep(100,13))[1:15],
        space=0,ylab='Fraction of root biomass',xlab='Depth (cm)',cex.lab=1.2)
axis(1,at=c(0,15,30,65,100,200,300,400,500,600,700,800,900,1000,1100,1200))
title('30 months')


# pour 10 mois on r�cup�re

dat2 <- read.table('impact_10mois.txt',header=T)
attach(dat2)

# on va rassembler les mesures sur les m�mes gammes de valeurs que dans celles des TDR
Dp <- c(rep('0-15',3),rep('15-30',3),rep('30-65',7),rep('65-100',7),rep('100-200',20),rep('200-300',20),rep('300-400',20))
Dp2 <- c('0-15','15-30','30-65','65-100','100-200','200-300','300-400')
Dp3 <- c(15,15,35,35,100,100,100)
Nr <- c(3,3,7,7,20,20,20)

M_cm2 <- (Moyenne_i_cm2[1:80] + Moyenne_i_cm2[81:160] + Moyenne_i_cm2[241:320] + Moyenne_i_cm2[321:400])/2

P10m <-c()
for (i in 1:7){
P10m <- c(P10m, sum(M_cm2[Dp==Dp2[i]]*5,na.rm=T))}
P10m <- P10m / sum(P10m)

Fra10m <- c(P10m, rep(NA,8))
Fra18m <- c(B18new[1:9],rep(NA,6))
Fra30m <- c(B30new[1:15])

# graph
Dpgraph <- c(15,15,35,35,rep(100,11))
par(mfrow=c(3,1))
par(yaxs='i')
par(mar=c(4.5,4.5,2,1))
barplot(Fra10m,width=Dpgraph,space=0,ylab='Fraction of root biomass',xlab='Depth (cm)',cex.lab=1.2)
axis(1,at=c(0,15,30,65,100,200,300,400,500,600,700,800,900,1000,1100))
title('10 months')
barplot(Fra18m,width=Dpgraph,
        space=0,ylab='Fraction of root biomass',xlab='Depth (cm)',cex.lab=1.2)
axis(1,at=c(0,15,30,65,100,200,300,400,500,600,700,800,900,1000,1100))
title('18 months')
barplot(Fra30m,width=Dpgraph,
        space=0,ylab='Fraction of root biomass',xlab='Depth (cm)',cex.lab=1.2)
axis(1,at=c(0,15,30,65,100,200,300,400,500,600,700,800,900,1000,1100))
title('30 months')

#test
fit1 <- wilcox.test(Fra10m,Fra18m)
fit2 <- wilcox.test(Fra10m,Fra30m)
fit3 <- wilcox.test(Fra18m,Fra30m)



# densit�, diam�tre,...

dat3 <- read.table('SRL_SRA_Diam.txt',header=T)
attach(dat3)

Rad18 <- mean(Diam_mm[age==18]/1000)/2
Rad30 <- mean(Diam_mm[age==30]/1000)/2
Rad42 <- mean(Diam_mm[age==30]/1000)/2

# on a des longueurs pour 1 g de racine
# en volume : V = L * pi R�  par g de racine

Dens18 <- 1/(SRL_mg[age==18] * pi * Rad18^2)
Dens30 <- 1/(SRL_mg[age==30] * pi * Rad30^2)
detach(dat3)

# root mass total

B18 <-  dat1[,2]*c(1.5,1.5,3.5,3.5,rep(10,13))
B30 <- dat1[,3]*c(1.5,1.5,3.5,3.5,rep(10,13))
B42 <- dat1[,4]*c(1.5,1.5,3.5,3.5,rep(10,13))
B71 <- dat1[,5]*c(1.5,1.5,3.5,3.5,rep(10,13))

Mtot18 <- sum(B18,na.rm=T)/1000 * 100   # passage en kg (/1000) m-2 (*100)
Mtot30 <- sum(B30,na.rm=T)/1000 * 100   
Mtot42 <- sum(B42,na.rm=T)/1000 * 100   
Mtot71 <- sum(B71,na.rm=T)/1000 * 100   

Mtot <- c(Mtot18,Mtot30,Mtot42,Mtot71)
Age <- c(18,30,42,71)

dat4 <- read.table('All_biomass_gdm3.txt',header=T)
attach(dat4)

Mtot18 <-c();Mtot30<-c();Mtot42<-c()
for (i in 1:3){

B18 <-  X18m[Tr==i]*c(1.5,1.5,3.5,3.5,rep(10,13))
B30 <-  X30m[Tr==i]*c(1.5,1.5,3.5,3.5,rep(10,13))
B42 <-  X42m[Tr==i]*c(1.5,1.5,3.5,3.5,rep(10,13))

Mtot18 <- c(Mtot18,sum(B18,na.rm=T)/1000 * 100)   # passage en kg (/1000) m-2 (*100)
Mtot30 <- c(Mtot30,sum(B30,na.rm=T)/1000 * 100)   
Mtot42 <- c(Mtot42,sum(B42,na.rm=T)/1000 * 100)   
}

Mtot <- c(Mtot18,Mtot30,Mtot42)
Age <- c(18,18,18,30,30,30,42,42,42)
fit1 <- lm(Mtot~Age-1)

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(Age,Mtot,pch=16,ylab=expression('Root mass total'~(kg~m^-2)),xlab='Age (months)',
      cex.lab=1.2,ylim=c(0,1.2),xlim=c(0,45))
abline(fit1,lwd=1.4,col=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('10 months :',paste('RMT = ',round(summary(fit1)$coef[1]*10,7),' kg m-2')),pch=c(NA,NA),cex=1.2,bg='white',box.col=0)
box()

