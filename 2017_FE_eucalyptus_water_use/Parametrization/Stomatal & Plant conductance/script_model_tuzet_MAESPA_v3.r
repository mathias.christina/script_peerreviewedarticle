set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/COnductance")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat <- read.table("Cond.txt",header=T)

dat2 <- rep(1,8)
for (i in 1:dim(dat)[1]){
if (is.na(dat$gs[i])==F){
dat2 <- rbind(dat2,dat[i,])}}
dat2<- dat2[2:dim(dat2)[1],]
attach(dat2)

# dry season vs rainy season
DATES <- c("27/11/2010",
           "24/01/2011","05/02/2011","10/03/2011","11/04/2011","21/05/2011","23/06/2011",
           "06/08/2011","05/09/2011" ,"01/10/2011", "05/11/2011","11/12/2011",
           "07/02/2012","13/03/2012","16/04/2012", "19/05/2012","24/06/2012","02/08/2012","31/08/2012","18/09/2012")
Per <- c(rep('R',5),rep('S',5),rep('R',7),rep('S',3))
Per2 <- c(rep('R1',5),rep('S1',5),rep('R2',5),rep('S2',5))
AGE <- c(5,7,8,9,10,11,12,14,15,16,17,18,20,21,22,23,24,26,26,27)

Date2 <- as.vector(Date)
Date3 <- as.vector(Date)
AGE2 <- as.vector(Date)
for (i in 1:length(DATES)){
Date2 <- replace(Date2, c(Date2==DATES[i]),Per[i])
AGE2 <- replace(AGE2, c(AGE2==DATES[i]),AGE[i])
Date3 <- replace(Date3, c(Date3==DATES[i]),Per2[i])}

# on selectionne le C3
Seas <- Date2[Traitement=='C3']
Seas2 <- Date3[Traitement=='C3']
Age <- AGE2[Traitement=='C3']
PotMin2 <- PotMin[Traitement=='C3']
PotBase2 <- PotBase[Traitement=='C3']
gs2 <- gs[Traitement=='C3']
Asat2 <- Asat[Traitement=='C3']


Cond <- function(g0,g1,An,Cs,Sf,Psif,PsiL,GAMMA,alpha, FN) {
if (FN==1){alpha=1}
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
Gs = g0 + g1 * (An /(Cs-GAMMA))^alpha * Fpsi
Output <- data.frame(Gs,rep('all',length(Gs)))}

#minimisation
sce1  <-  function(param,x,  yobs,FN)  {
g0 <- param[1] ; g1 <- param[2]
Psif <- param[3] ;
Sf <- param[4] ;
if (FN==1){GAMMA <- param[5];alpha=1}
else if (FN==2){GAMMA=0;alpha=param[5]}
else if (FN==3){GAMMA=0;alpha=1}
ytheo <- Cond(g0,g1,x[,1],380,Sf,Psif,x[,2],GAMMA,alpha,FN)[,1]
return(sum((yobs  -  ytheo)^2))
}

PAR1 <- c('G0','G1','PSIF','SF','GAMMA')
PAR2 <- c('G0','G1','PSIF','SF','ALPHA')
PAR3 <- c('G0','G1','PSIF','SF')
G0norm <- abs(rnorm(500,mean=0.02,sd=0.01))
G1norm <- abs(rnorm(500,mean=15,sd=7.5))
G1norm2 <- abs(rnorm(500,mean=200,sd=100))
PSIFnorm <- -abs(rnorm(500,mean=-2,sd=1))
SFnorm <- abs(rnorm(500,mean=2,sd=1))
GAMMAnorm <- abs(rnorm(500,mean=0.02,sd=0.01))
Anorm <- abs(rnorm(500,mean=2,sd=1))

Xdatas <- data.frame(Asat2, PotMin2/(-10))
Gsobs <- gs2

for (i in 1:500){
parini <- c(G0norm[i], G1norm[i], PSIFnorm[i],SFnorm[i],GAMMAnorm[i])
Lower <- c(0.01,0,-100,0,0)
Upper <- c(10,500,0,50,2)
fit1 <- optim(par  = parini,fn  =  sce1, x=Xdatas, yobs=Gsobs,FN=1,
               lower=Lower,upper=Upper,method="L-BFGS-B")

parini <- c(G0norm[i], G1norm2[i], PSIFnorm[i],SFnorm[i],Anorm[i])
Lower <- c(0.01,0,-100,0,0)
Upper <- c(10,500,0,50,5)
fit2 <- optim(par  = parini,fn  =  sce1, x=Xdatas, yobs=Gsobs,FN=2,
               lower=Lower,upper=Upper,method="L-BFGS-B")

parini <- c(G0norm[i], G1norm[i], PSIFnorm[i],SFnorm[i])
Lower <- c(0.01,0,-100,0)
Upper <- c(10,500,0,50)
fit3 <- optim(par  = parini,fn  =  sce1, x=Xdatas, yobs=Gsobs,FN=3,
               lower=Lower,upper=Upper,method="L-BFGS-B")

PAR1 <- data.frame(PAR1,fit1$par)
PAR2 <- data.frame(PAR2,fit2$par)
PAR3 <- data.frame(PAR3,fit3$par)
}


RMSE_2 <- function(yobs,ytheo,k,n){
R <- sum((yobs  -  ytheo)^2)/(n-k-1)
print(R)}

# on selectionne avec la m�thode des RMSE
Gsobs1 <- gs2

RMSE1<-c();RMSE2<-c();RMSE3<-c()
for (i in 2:501){
Gs1 <- Cond(PAR1[1,i],PAR1[2,i],Asat2,380,PAR1[4,i],PAR1[3,i],PotMin2/(-10),PAR1[5,i],1,FN=1)[,1]
Gs2 <- Cond(PAR2[1,i],PAR2[2,i],Asat2,380,PAR2[4,i],PAR2[3,i],PotMin2/(-10),0,PAR1[5,i],FN=2)[,1]
Gs3 <- Cond(PAR3[1,i],PAR3[2,i],Asat2,380,PAR3[4,i],PAR3[3,i],PotMin2/(-10),0,1,FN=3)[,1]
RMSE1 <- c(RMSE1, RMSE_2(Gsobs1,Gs1,k=2,n=length(gs2)))
RMSE2 <- c(RMSE2, RMSE_2(Gsobs1,Gs2,k=2,n=length(gs2)))
RMSE3 <- c(RMSE3, RMSE_2(Gsobs1,Gs3,k=2,n=length(gs2)))
}

Val1 <- min(RMSE1)
Val2 <- min(RMSE2)
Val3 <- min(RMSE3)
ID1 <- which(RMSE1==Val1)
ID2 <- which(RMSE2==Val2)
ID3 <- which(RMSE3==Val3)

Gs1 <- Cond(PAR1[1,ID1],PAR1[2,ID1],Asat2,380,PAR1[4,ID1],PAR1[3,ID1],PotMin2/(-10),PAR1[5,ID1],1,FN=1)[,1]
Gs2 <- Cond(PAR2[1,ID2],PAR2[2,ID2],Asat2,380,PAR2[4,ID2],PAR2[3,ID2],PotMin2/(-10),0,PAR2[5,ID2],FN=2)[,1]
Gs3 <- Cond(PAR3[1,ID3],PAR3[2,ID3],Asat2,380,PAR3[4,ID3],PAR3[3,ID3],PotMin2/(-10),0,1,FN=3)[,1]

source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
AIC_1 <- AIC2(78,Gs1,Gsobs1,2,5)
AIC_2 <- AIC2(78,Gs2,Gsobs1,2,5)
AIC_3 <- AIC2(78,Gs3,Gsobs1,2,4)
BIC_1 <- BIC2(78,Gs1,Gsobs1,2,5)
BIC_2 <- BIC2(78,Gs2,Gsobs1,2,5)
BIC_3 <- BIC2(78,Gs3,Gsobs1,2,4)

#avec ou sans gamma

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))

plot(Asat2,gs2,xlab=expression('Photosynthesis'~(�mol~m^-2~s-1)),ylab=expression('Gs'~(mmol~m^-2~s^-1)),cex.lab=1.2)
points(Asat2,Gs1,pch=16,col=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c('Measured conductance','Simulated with gamma'),pch=c(1,16),col=c(1,2),cex=1.2,bg='white',box.col=0)
box()
plot(Gs1,Gs3,ylab='Simulation without Gamma',xlab='Simulation with Gamma',cex.lab=1.2,pch=16,col=4)
abline(0,1)
grid(nx=NULL,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c('With gamma:',paste('  AIC = ',round(AIC_1,1),' and BIC = ',round(BIC_1,1))),cex=1.2,bg='white',box.col=0)
legend('topleft',legend=c('Without gamma:',paste('  AIC = ',round(AIC_3,1),' and BIC = ',round(BIC_3,1))),cex=1.2,bg='white',box.col=0)
box()


par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
plot(Asat2,gs2,xlab=expression('Photosynthesis'~(�mol~m^-2~s-1)),ylab=expression('Gs'~(mmol~m^-2~s^-1)),cex.lab=1.2)
points(Asat2,Gs3,pch=16,col=3)
points(Asat2,Gs2,pch=16,col=4)
grid(nx=NULL,ny=NULL,col = 1,   lty = "dotted", lwd = 1)
legend('bottomright',legend=c('Measured conductance','Simulated Tuzet','Simulated modified'),pch=c(1,16,16),col=c(1,3,4),cex=1.2,bg='white',box.col=0)
legend('topleft',legend=c('Tuzet model:',paste('  AIC = ',round(AIC_3,1)),paste('  BIC = ',round(BIC_3,1)),
                           'Modified model:',paste('  AIC = ',round(AIC_2,1)),paste('  BIC = ',round(BIC_2,1))),
                           pch=NA,cex=1.2,bg='white',box.col=0)
box()

plot(gs2,Gs3,xlab='Measured Gs',ylab='Simulated Gs',cex.lab=1.2,col=3,xlim=c(0,1.5),ylim=c(0.,1.4),pch=16)
points(gs2,Gs2,col=4,pch=16)
abline(0,1, col=2,lwd=2)
grid(nx=NULL,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topleft',legend=c('Tuzet:',paste('  RMSE = ',round(Val2,5)),
                          'Modified',paste('  RMSE = ',round(Val3,5))),pch=c(16,NA,16,NA),col=c(3,0,4,0),bg='white',box.col=0,cex=1);box()



