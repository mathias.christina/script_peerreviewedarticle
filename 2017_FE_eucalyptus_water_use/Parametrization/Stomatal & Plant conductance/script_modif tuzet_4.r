set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/COnductance")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat <- read.table("Cond.txt",header=T)

dat2 <- rep(1,8)
for (i in 1:dim(dat)[1]){
if (is.na(dat$gs[i])==F){
dat2 <- rbind(dat2,dat[i,])}}
dat2<- dat2[2:dim(dat2)[1],]
attach(dat2)

# dry season vs rainy season
DATES <- c("27/11/2010",
           "24/01/2011","05/02/2011","10/03/2011","11/04/2011","21/05/2011","23/06/2011",
           "06/08/2011","05/09/2011" ,"01/10/2011", "05/11/2011","11/12/2011",
           "07/02/2012","13/03/2012","16/04/2012", "19/05/2012","24/06/2012","02/08/2012","31/08/2012","18/09/2012")
Per <- c(rep('R',5),rep('S',5),rep('R',7),rep('S',3))
Per2 <- c(rep('R1',5),rep('S1',5),rep('R2',5),rep('S2',5))

Date2 <- as.vector(Date)
Date3 <- as.vector(Date)
for (i in 1:length(DATES)){
Date2 <- replace(Date2, c(Date2==DATES[i]),Per[i])
Date3 <- replace(Date3, c(Date3==DATES[i]),Per2[i])}

# on selectionne le C3
Seas <- Date2[Traitement=='C3']
Seas2 <- Date3[Traitement=='C3']
PotMin2 <- PotMin[Traitement=='C3']
PotBase2 <- PotBase[Traitement=='C3']
gs2 <- gs[Traitement=='C3']
Asat2 <- Asat[Traitement=='C3']


An1 <- function(g0,g12,GS,Cs,Sf,Psif,PsiL,GAMMA) {
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
An = (GS-g0)*g12*(Cs/(Fpsi))
Output <- data.frame(An,rep('all',length(An)))
}

An2 <- function(g0,g1,GS,Cs,Sf,Psif,PsiL,AMAX,GAMMA) {
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
An = AMAX * (GS-g0)/(g1 +(GS-g0))* (Cs-GAMMA)/Fpsi
Output <- data.frame(An,rep('all',length(An)))
}
An22 <- function(g0,g1,GS,Cs,AMAX,GAMMA) {
An = AMAX * (GS-g0)/(g1 +(GS-g0))* (Cs-GAMMA)
Output <- data.frame(An,rep('all',length(An)))
}

# on commence par fitter juste g0 g1/2 et taumax

ft2 <- nls(Asat2~ TAUMAX * (gs2-g0)/(g1 +(gs2-g0))* 380,
  start=c(g0=0.01,g1=0.3,TAUMAX=0.07),algorithm='port',lower=c(0.001,0,0))
A2 <- An22(summary(ft2)$par[1,1],summary(ft2)$par[2,1],gs2,380,summary(ft2)$par[3,1],0)[,1]



# on fit maintenant PSIF et SF

TAUMAX <- summary(ft2)$coef[3,1]
g0 <- summary(ft2)$coef[1,1]
g1 <- summary(ft2)$coef[2,1]

ft3 <- nls(Asat2~ TAUMAX * (gs2-g0)/(g1 +(gs2-g0))* 380/((1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PotMin2/(-10))))),
  start=c(Psif=-2.5,Sf=2))#,algorithm='port',upper=c(2,5,0,10,1))

ft4 <- nls(Asat~ TAUMAX * (gs-0.001)/(g1 +(gs-0.001))* 380/((1 + exp (Sf * (-2.8) ) ) / ( 1 + exp (Sf * (-2.8 - PotMin/(-10))))),
  start=c(TAUMAX=0.08,g1=0.3,Sf=2))#,algorithm='port',upper=c(2,5,0,10,1))

A3 <- An2(0.001,summary(ft2)$par[2,1],gs2,380,summary(ft3)$par[2,1],-2.8,PotMin2/(-10),summary(ft2)$par[3,1],0)[,1]
A32 <- An2(0.001,summary(ft2)$par[2,1],gs,380,summary(ft3)$par[2,1],-2.8,PotMin/(-10),summary(ft2)$par[3,1],0)[,1]

A3b <- An2(0.001,summary(ft2)$par[2,1],gs2,380,2.44,-2.8,PotMin2/(-10),summary(ft2)$par[3,1],0)[,1]
A32b <- An2(0.001,summary(ft2)$par[2,1],gs,380,2.44,-2.8,PotMin/(-10),summary(ft2)$par[3,1],0)[,1]

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(gs2,Asat2,ylab=expression('Photosynthesis'~(�mol~m^-2~s-1)),xlab=expression('Gs'~(mmol~m^-2~s^-1)),cex.lab=1.2,ylim=c(0,35))
points(gs2,A3,col=4,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topleft',legend=c('Measured conductance','Tuzet model','Modified'),pch=c(1,16,16),col=c(1,2,4),cex=1.2,bg='white',box.col=0)
box()
par(xaxs='i',yaxs='i')
plot(gs,Asat,ylab=expression('Photosynthesis'~(�mol~m^-2~s-1)),xlab=expression('Gs'~(mmol~m^-2~s^-1)),cex.lab=1.2,ylim=c(0,35))
points(gs,A32,col=4,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topleft',legend=c('Measured conductance','Tuzet model','Modified'),pch=c(1,16,16),col=c(1,2,4),cex=1.2,bg='white',box.col=0)
box()

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(Asat2,A3,col=4,pch=16,xlab='A meas',ylab='A sim')
points(Asat2,A3b,col=5,pch=16)
abline(0,1)
plot(Asat,A32,col=4,pch=16,xlab='A meas',ylab='A sim')
points(Asat,A32b,col=5,pch=16)
abline(0,1)

# on fait varier

#An2 <- function(g0,g1,GS,Cs,Sf,Psif,PsiL,AMAX,GAMMA) {

SF <- 0
AD <- An2(0.001,0.276,gs2,380,SF,-2.8,PotMin2/(-10),0.08,0)[,1]

SCR <- (AD - Asat2)^2
RMSE <- c(sqrt(sum(SCR,na.rm=T)/(length(SCR[!is.na(SCR)])-1)))
fit <- lm(AD ~ Asat2-1) ; pente <- c(summary(fit)$coef[1])
for (SF in c(1:22)*0.5){
A3 <- An2(0.001,0.276,gs2,380,SF,-2.8,PotMin2/(-10),0.08,0)[,1]
AD <- data.frame(AD,A3)

SCR <- (A3 - Asat2)^2
RMSE <- c(RMSE,sqrt(sum(SCR,na.rm=T)/(length(SCR[!is.na(SCR)])-1)))

fit <- lm(A3 ~ Asat2-1) ; pente <- c(pente, summary(fit)$coef[1])
}


Asat2~ TAUMAX * (gs2-g0)/(g1 +(gs2-g0))* 380/((1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PotMin2/(-10)))))

YD <- Asat2/(0.08*(gs2-0.001)/(0.276+(gs2-0.001))*380)


# methode dans l'autre sens
Cond1 <- function(g0,g1,An,Cs,Sf,Psif,PsiL,alpha) {
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
Gs = g0 + g1 * An^alpha /(Cs) * Fpsi
Output <- data.frame(Gs,rep('all',length(Gs)))
}
sce1  <-  function(param,x,  yobs,FN)  {
g0 <- param[1] ; g1 <- param[2]
Psif <- param[3] ;
Sf <- param[4] ; alpha <- param[5]
ytheo <- Cond1(g0,g1,x[,1],380,Sf,Psif,x[,2],alpha)[,1]
return(sum((yobs  -  ytheo)^2))
}

ft1 <- nls(gs2~ g0 + g1 * Asat2 /(380)*((1 + exp (Sf * PSIF ) ) / ( 1 + exp (Sf * (PSIF - PotMin2/(-10))))),
  start=c(g0=0.001,g1=5,Sf=2,PSIF=-2.8),algorithm='port',lower=c(0.001,0.1,0,-10))
ft2 <- nls(gs2~ g0 + g1 * Asat2^alpha /(380)*((1 + exp (Sf * PSIF ) ) / ( 1 + exp (Sf * (PSIF - PotMin2/(-10))))),
  start=c(g0=0.001,g1=5,Sf=2,PSIF=-2.8,alpha=1),algorithm='port',lower=c(0.001,0.1,0,-10,0.001))
ft3 <- nls(gs2~ 0.001 + g1 * Asat2^alpha /(380)*((1 + exp (Sf * (-2.8) ) ) / ( 1 + exp (Sf * (-2.8 - PotMin2/(-10))))),
  start=c(g1=5,Sf=2,alpha=1),algorithm='port',lower=c(0.001,0.1,0,-10,0.001))

#GS1 <- Cond1(summary(ft1)$coef[1,1],summary(ft1)$coef[2,1],Asat2,380,summary(ft1)$coef[3,1],summary(ft1)$coef[4,1],PotMin2/(-10))[,1]

plot(Asat2,gs2)
points(Asat2,predict(ft1),col=4,pch=16)
points(Asat2,predict(ft2),col=5,pch=16)
points(Asat2,predict(ft3),col=2,pch=16)


