set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/Conductance")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat <- read.table("Cond.txt",header=T)

dat2 <- rep(1,8)
for (i in 1:dim(dat)[1]){
if (is.na(dat$gs[i])==F){
if (dat$Traitement[i]=='C3'){
dat2 <- rbind(dat2,dat[i,])}
else if(dat$Traitement[i]=='S3'){
dat2 <- rbind(dat2,dat[i,])}
}}
dat2<- dat2[2:dim(dat2)[1],]
attach(dat2)


DATES <- c("27/11/2010",
           "24/01/2011","05/02/2011","10/03/2011","11/04/2011","21/05/2011","23/06/2011",
           "06/08/2011","05/09/2011" ,"01/10/2011", "05/11/2011","11/12/2011",
           "07/02/2012","13/03/2012","16/04/2012", "19/05/2012","24/06/2012","02/08/2012","31/08/2012","18/09/2012")


Cond1 <- function(g0,g1,An,Cs,Sf,Psif,PsiL,GAMMA){
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
Gs = g0 + g1 * An / (Cs-GAMMA) * Fpsi
}
#equation de minimization
sce1  <-  function(param,x,  yobs)  {
Psif <- param[1] ; g0 <- param[2] ; g1 <- param[3] ; Sf <- param[4] ; GAMMA<- param[5]
ytheo <- Cond1(g0,g1,x[,1],380,Sf,Psif,x[,2],GAMMA)
return(sum((yobs  -  ytheo)^2))
}

parini <- c(-2, 0.01, 15, 5,0.01)
Xdatas <- data.frame(Asat, PotMin/(-10))
Lower <- c(-5,0.01,0,0,0)
fit1 <- optim(par  = parini,fn  =  sce1, x=Xdatas, yobs=gs,
               lower=Lower,method="L-BFGS-B")
Gscal1 <- Cond1(fit1$par[2],fit1$par[3],Asat,380,fit1$par[4],fit1$par[1],PotMin/(-10),fit1$par[5])

# diff�rence entre les PER
Per <- c("P1",rep('P2',3),rep('P3',3),rep('P4',2),rep('P5',3),rep('P6',2),rep('P7',3),rep('P8',3))
Per2 <- c("P1",'P2','P3','P4','P5','P6','P7','P8')
PerMonth <- c('Nov 2010','Jan-Mar 2011','Apr-Jun 2011','Jul-Sep 2011','Oct-Dec 2011','Jan-Mar 2012','Apr-Jun 2012','Jul-Sep 2012')
PER <- c();
for (i in 1:20){
PER <- c(PER, rep(Per[i], length(Asat[Date==DATES[i]])))}

Cond2 <- function(An,Cs,Psif,PsiL,g0,g1,Sf,GAMMA){
Gstot<-c()
for (i in 1:8){
Gs = g0[i] + g1[i] * An[PER==Per2[i]] / (Cs-GAMMA[i]) * ( 1 + exp (Sf[i] * Psif ) ) / ( 1 + exp (Sf[i] * (Psif - PsiL[PER==Per2[i]])))
Gstot <- c(Gstot,Gs)
}
Gsout <- Gstot
}
sce2  <-  function(param,x,  yobs)  {
Psif <- param[1] ;
g0 <- param[2:(8+1)] ; g1 <- param[(8+2):(2*8+1)] ; Sf <- param[(2*8+2):(3*8+1)] ;
GAMMA <- param[(3*8+2):(4*8+1)]
ytheo <- Cond2(x[,1],380,Psif,x[,2],g0,g1,Sf,GAMMA)
return(sum((yobs  -  ytheo)^2))
}

parini <- c(-2,rep(0.01,8),rep(15,8), rep(5,8),rep(0.01,8))
Xdatas <- data.frame(Asat, PotMin/(-10))
Lower <- c(-100,rep(0.01,8),rep(0,8), rep(0,8),rep(0,8))
fit2 <- optim(par  = parini,fn  =  sce2, x=Xdatas, yobs=gs,
               lower=Lower,method="L-BFGS-B")

Gscal2 <- Cond2(Asat,380, fit2$par[1], PotMin/(-10),fit2$par[2:9],fit2$par[10:17],fit2$par[18:25],fit2$par[26:33])

#graphiquement
par(mar=c(4.5,4.5,1,1))
plot(Asat,Gscal1,col=1,xlim=c(0,30),ylim=c(0,1.5),type='l',lwd=1.4,
      xlab='Photosynthesis',ylab='Simulated Conductance',cex.lab=1.2)
for (i in 1:8){
points(Asat[PER==Per2[i]],Gscal2[PER==Per2[i]],col=i+1,pch=16)
}
legend('topleft',legend=c('All months',PerMonth),col=c(1:9),pch=c(NA,rep(16,8)),lty=c(1,rep(NA,8)),bty='n')

# on regroupe certains mois
Per <- c(rep('P1',7),rep('P2',2),rep('P3',5),rep('P4',6))
Per2 <- c("P1",'P2','P3','P4')
PerMonth <- c('Nov 2010 - Jun 2011','Jul-Sep 2011','Oct 2011 - Mar 2012','Apr - Sep 2012')
PER <- c();
for (i in 1:20){
PER <- c(PER, rep(Per[i], length(Asat[Date==DATES[i]])))}

Cond3 <- function(An,Cs,Psif,PsiL,g0,g1,Sf,GAMMA){
Gstot<-c()
for (i in 1:4){
Gs = g0[i] + g1[i] * An[PER==Per2[i]] / (Cs-GAMMA[i]) * ( 1 + exp (Sf[i] * Psif ) ) / ( 1 + exp (Sf[i] * (Psif - PsiL[PER==Per2[i]])))
Gstot <- c(Gstot,Gs)
}
Gsout <- Gstot
}
sce3  <-  function(param,x,  yobs)  {
Psif <- param[1] ;
g0 <- param[2:(4+1)] ; g1 <- param[(4+2):(2*4+1)] ; Sf <- param[(2*4+2):(3*4+1)] ;
GAMMA <- param[(3*4+2):(4*4+1)]
ytheo <- Cond3(x[,1],380,Psif,x[,2],g0,g1,Sf,GAMMA)
return(sum((yobs  -  ytheo)^2))
}

parini <- c(-2,rep(0.02,4),rep(15,4), rep(5,4),rep(0.01,4))
Xdatas <- data.frame(Asat, PotMin)
Lower <- c(-3,rep(0.01,4),rep(0,4), rep(0,4),rep(0,4))
fit3 <- optim(par  = parini,fn  =  sce3, x=Xdatas, yobs=gs,
               lower=Lower,method="L-BFGS-B")

Gscal3 <- Cond3(Asat,380, fit3$par[1], PotMin,fit3$par[2:5],fit3$par[6:9],fit3$par[10:13],fit3$par[14:17])

#graphiquement
par(mar=c(4.5,4.5,1,1))
plot(Asat,Gscal1,col=1,xlim=c(0,30),ylim=c(0,1.5),type='l',lwd=1.4,
      xlab='Photosynthesis',ylab='Simulated Conductance',cex.lab=1.2)
for (i in 1:4){
points(Asat[PER==Per2[i]],Gscal3[PER==Per2[i]],col=i+1,pch=16)
}
legend('topleft',legend=c('All months',PerMonth),col=c(1:5),pch=c(NA,rep(16,4)),lty=c(1,rep(NA,4)),bty='n')

# 3 periods
Per <- c(rep('P1',7),rep('P2',2),rep('P3',11))
Per2 <- c("P1",'P2','P3')
PerMonth <- c('Nov 2010 - Jun 2011','Jul-Sep 2011','Oct 2011 - Sep 2012')
PER <- c();
for (i in 1:20){
PER <- c(PER, rep(Per[i], length(Asat[Date==DATES[i]])))}

Cond4 <- function(An,Cs,Psif,PsiL,g0,g1,Sf,GAMMA){
Gstot<-c()
for (i in 1:3){
Gs = g0[i] + g1[i] * An[PER==Per2[i]] / (Cs-GAMMA[i]) * ( 1 + exp (Sf[i] * Psif ) ) / ( 1 + exp (Sf[i] * (Psif - PsiL[PER==Per2[i]])))
Gstot <- c(Gstot,Gs)
}
Gsout <- Gstot
}
sce4  <-  function(param,x,  yobs)  {
Psif <- param[1] ;
g0 <- param[2:(3+1)] ; g1 <- param[(3+2):(2*3+1)] ; Sf <- param[(2*3+2):(3*3+1)] ;
GAMMA <- param[(3*3+2):(4*3+1)]
ytheo <- Cond4(x[,1],380,Psif,x[,2],g0,g1,Sf,GAMMA)
return(sum((yobs  -  ytheo)^2))
}

parini <- c(-2,rep(0.01,3),rep(15,3), rep(5,3),rep(0.01,3))
Xdatas <- data.frame(Asat, PotMin)
Lower <- c(-100,rep(0.01,3),rep(0,3), rep(0,6))
fit4 <- optim(par  = parini,fn  =  sce4, x=Xdatas, yobs=gs,
               lower=Lower,method="L-BFGS-B")

Gscal4 <- Cond4(Asat,380, fit4$par[1], PotMin,fit4$par[2:4],fit4$par[5:7],fit4$par[8:10],fit4$par[11:13])

par(mar=c(4.5,4.5,1,1))
plot(Asat,Gscal1,col=1,xlim=c(0,30),ylim=c(0,1.5),type='l',lwd=1.4,
      xlab='Photosynthesis',ylab='Simulated Conductance',cex.lab=1.2)
for (i in 1:3){
points(Asat[PER==Per2[i]],Gscal4[PER==Per2[i]],col=i+1,pch=16)
}
legend('topleft',legend=c('All months',PerMonth),col=c(1:5),pch=c(NA,rep(16,3)),lty=c(1,rep(NA,3)),bty='n')

# comparons

aic1 <- AIC2(length(gs),Gscal1, gs,2,5)
aic2 <- AIC2(length(gs),Gscal2, gs,2,33)
aic3 <- AIC2(length(gs),Gscal3, gs,2,17)
aic4 <- AIC2(length(gs),Gscal4, gs,2,13)

bic1 <- BIC2(length(gs),Gscal1, gs,2,5)
bic2 <- BIC2(length(gs),Gscal2, gs,2,33)
bic3 <- BIC2(length(gs),Gscal3, gs,2,17)
bic4 <- BIC2(length(gs),Gscal4, gs,2,13)

mse1 <- MSE2(length(gs),Gscal1, gs,2)
mse2 <- MSE2(length(gs),Gscal2, gs,2)
mse3 <- MSE2(length(gs),Gscal3, gs,2)
mse4 <- MSE2(length(gs),Gscal4, gs,2)

par(mfrow=c(2,2))
par(mar=c(4.5,4.5,2,1))

plot(gs, Gscal1, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 1 : All months')
abline(0,1,col=2,lwd=2)
segments(gs, Gscal1, gs, gs,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(aic1,3)),paste('BIC = ',round(bic1,3)),paste('MSE = ',round(mse1,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(gs, Gscal2, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 2 : 8 periods')
abline(0,1,col=2,lwd=2)
segments(gs, Gscal2, gs, gs,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(aic2,3)),paste('BIC = ',round(bic2,3)),paste('MSE = ',round(mse2,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(gs, Gscal3, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 3 : 4 periods')
abline(0,1,col=2,lwd=2)
segments(gs, Gscal3, gs, gs,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(aic3,3)),paste('BIC = ',round(bic3,3)),paste('MSE = ',round(mse3,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(gs, Gscal4, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 4 : 3 periods')
abline(0,1,col=2,lwd=2)
segments(gs, Gscal4, gs, gs,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(aic4,3)),paste('BIC = ',round(bic4,3)),paste('MSE = ',round(mse4,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)


# soit
Per <- c(rep('P1',7),rep('P2',2),rep('P3',5),rep('P4',6))
Per2 <- c("P1",'P2','P3','P4')
PerMonth <- c('Nov 2010 - Jun 2011','Jul-Sep 2011','Oct 2011 - Mar 2012','Apr - Sep 2012')
PER <- c();
for (i in 1:20){
PER <- c(PER, rep(Per[i], length(Asat[Date==DATES[i]])))}

par(mfrow=c(2,4))
par(mar=c(4.5,4.5,2,1))

plot(gs[PER=='P1'], Gscal3[PER=='P1'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title(PerMonth[1])
ft <- lm (Gscal3[PER=='P1']~ gs[PER=='P1'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
plot(gs[PER=='P2'], Gscal3[PER=='P2'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title(PerMonth[2])
ft <- lm (Gscal3[PER=='P2']~ gs[PER=='P2'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
plot(gs[PER=='P3'], Gscal3[PER=='P3'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title(PerMonth[3])
ft <- lm (Gscal3[PER=='P3']~ gs[PER=='P3'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)

plot(gs[PER=='P4'], Gscal3[PER=='P4'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title(PerMonth[4])
ft <- lm (Gscal3[PER=='P4']~ gs[PER=='P4'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)

plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c('Reference water potential : ',expression(~~~Psi[f]~'=  -2.051'),'',
                           PerMonth[1],
                           expression(~~~g[0]~'= 0.01'),
                           expression(~~~g[1]~'= 16.075'),
                           expression(~~~S[f]~'= 4.998'),
                           expression(~~~gamma~'= 0.052')),
                           pch =rep(NA,8),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c('Reference water potential : ',expression(~~~Psi[f]~'=  -2.051'),'',
                           PerMonth[2],
                           expression(~~~g[0]~'= 0.0399'),
                           expression(~~~g[1]~'= 3.173'),
                           expression(~~~S[f]~'= 5.014'),
                           expression(~~~gamma~'= 0.0007')),
                           pch =rep(NA,8),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c('Reference water potential : ',expression(~~~Psi[f]~'=  -2.051'),'',
                           PerMonth[3],
                           expression(~~~g[0]~'= 0.01'),
                           expression(~~~g[1]~'= 13.815'),
                           expression(~~~S[f]~'= 5.002'),
                           expression(~~~gamma~'= 0.074')),
                           pch =rep(NA,8),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c('Reference water potential : ',expression(~~~Psi[f]~'=  -2.051'),'',
                           PerMonth[4],
                           expression(~~~g[0]~'= 0.01'),
                           expression(~~~g[1]~'= 11.069'),
                           expression(~~~S[f]~'= 5.006'),
                           expression(~~~gamma~'= 0.316')),
                           pch =rep(NA,8),cex=1.2,bty='n')


Mat <- data.frame(c('Psif','g0','g1','Sf','Gamma'), fit1$par,
                  c(fit3$par[1],fit3$par[2],fit3$par[6],fit3$par[10],fit3$par[14]),
                  c(fit3$par[1],fit3$par[3],fit3$par[7],fit3$par[11],fit3$par[15]),
                  c(fit3$par[1],fit3$par[4],fit3$par[8],fit3$par[12],fit3$par[16]),
                  c(fit3$par[1],fit3$par[5],fit3$par[9],fit3$par[13],fit3$par[17]))
names(Mat) <- c('Parameters','Allmonth','1110_0611','0711_0911','1011_0312','0412')

write.table(Mat, 'GS_parameters.txt',row.names=F,col.names=T)

