set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/COnductance")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat <- read.table("Cond.txt",header=T)

dat2 <- rep(1,8)
for (i in 1:dim(dat)[1]){
if (is.na(dat$gs[i])==F){
dat2 <- rbind(dat2,dat[i,])}
}
dat2<- dat2[2:dim(dat2)[1],]
attach(dat2)

Cond <- function(g0C1,g1C1,AnC1,Cs,SfC1,Psif,PsiLC1,
                 g0C3,g1C3,AnC3,SfC3,PsiLC3,
                 g0S1,g1S1,AnS1,SfS1,PsiLS1,
                 g0S3,g1S3,AnS3,SfS3,PsiLS3,
                 GAMMA1,GAMMA2,GAMMA3,GAMMA4) {
FpsiC1 = ( 1 + exp (SfC1 * Psif ) ) / ( 1 + exp (SfC1 * (Psif - PsiLC1)))
GsC1 = g0C1 + g1C1 * AnC1 /(Cs-GAMMA1) * FpsiC1
FpsiC3 = ( 1 + exp (SfC3 * Psif ) ) / ( 1 + exp (SfC3 * (Psif - PsiLC3)))
GsC3 = g0C3 + g1C3 * AnC3 /(Cs-GAMMA2) * FpsiC3
FpsiS1 = ( 1 + exp (SfS1 * Psif ) ) / ( 1 + exp (SfS1 * (Psif - PsiLS1)))
GsS1 = g0S1 + g1S1 * AnS1 /(Cs-GAMMA3) * FpsiS1
FpsiS3 = ( 1 + exp (SfS3 * Psif ) ) / ( 1 + exp (SfS3 * (Psif - PsiLS3)))
GsS3 = g0S3 + g1S3 * AnS3 /(Cs-GAMMA4) * FpsiS3

Output <- data.frame(c(GsC1,GsC3,GsS1,GsS3),
                     c(rep('C1',length(GsC1)),rep('C3',length(GsC3)),rep('S1',length(GsS1)),rep('S3',length(GsS3))))
}

#equation de minimization
sce1  <-  function(param,x,  yobs)  {

Psif <- param[1] ;
g0C1 <- param[2] ; g1C1 <- param[3] ; SfC1 <- param[4] ;
g0C3 <- param[5] ; g1C3 <- param[6] ; SfC3 <- param[7] ;
g0S1 <- param[8] ; g1S1 <- param[9] ; SfS1 <- param[10] ;
g0S3 <- param[11] ; g1S3 <- param[12] ; SfS3 <- param[13] ;
GAMMA1 <- param[14];GAMMA2 <-param[15];GAMMA3 <- param[16];GAMMA4 <- param[17]

ytheo <- Cond(g0C1,g1C1,x[,1][Traitement=='C1'],380,SfC1,Psif,x[,2][Traitement=='C1'],
           g0C3,g1C3,x[,1][Traitement=='C3'],SfC3,x[,2][Traitement=='C3'],
           g0S1,g1S1,x[,1][Traitement=='S1'],SfS1,x[,2][Traitement=='S1'],
           g0S3,g1S3,x[,1][Traitement=='S3'],SfS3,x[,2][Traitement=='S3'],
           GAMMA1,GAMMA2,GAMMA3,GAMMA4)[,1]

return(sum((yobs  -  ytheo)^2))
}

# on fit

parini <- c(-2, rep(c(0.01, 15, 5),4),rep(0.01,4))
Xdatas <- data.frame(Asat, PotMin/(-10))
Gsobs <- c(gs[Traitement=='C1'],gs[Traitement=='C3'],gs[Traitement=='S1'],gs[Traitement=='S3'])
Lower <- c(-100,rep(c(0.01,0,0),4),rep(0,4))

fit <- optim(par  = parini,fn  =  sce1, x=Xdatas, yobs=Gsobs,
               lower=Lower,method="L-BFGS-B")

# graphiquement
CondRef <- Cond (fit$par[2],fit$par[3],Xdatas[,1][Traitement=='C1'],380,fit$par[4],fit$par[1],Xdatas[,2][Traitement=='C1'],
                 fit$par[5],fit$par[6],Xdatas[,1][Traitement=='C3'],fit$par[7],Xdatas[,2][Traitement=='C3'],
                 fit$par[8],fit$par[9],Xdatas[,1][Traitement=='S1'],fit$par[10],Xdatas[,2][Traitement=='S1'],
                 fit$par[11],fit$par[12],Xdatas[,1][Traitement=='S3'],fit$par[13],Xdatas[,2][Traitement=='S3'],
                 fit$par[14],fit$par[15],fit$par[16],fit$par[17])

names(CondRef) <- c('Gscal', 'Trait')

par(mfrow=c(3,2))
par(mar=c(4.5,4.5,2,1))

plot(gs[Traitement=='C1'], CondRef[,1][CondRef[,2]=='C1'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('C1 treatment')
ft <- lm (CondRef[,1][CondRef[,2]=='C1']~ gs[Traitement=='C1'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((CondRef[,1][CondRef[,2]=='C1']-gs[Traitement=='C1'])^2)/(length(gs[Traitement=='C1'])-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

plot(gs[Traitement=='C3'], CondRef[,1][CondRef[,2]=='C3'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('C3 treatment')
ft <- lm (CondRef[,1][CondRef[,2]=='C3']~ gs[Traitement=='C3'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((CondRef[,1][CondRef[,2]=='C3']-gs[Traitement=='C3'])^2)/(length(gs[Traitement=='C3'])-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

plot(gs[Traitement=='S1'], CondRef[,1][CondRef[,2]=='S1'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('S1 treatment')
ft <- lm (CondRef[,1][CondRef[,2]=='S1']~ gs[Traitement=='S1'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((CondRef[,1][CondRef[,2]=='S1']-gs[Traitement=='S1'])^2)/(length(gs[Traitement=='S1'])-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

plot(gs[Traitement=='S3'], CondRef[,1][CondRef[,2]=='S3'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('S3 treatment')
ft <- lm (CondRef[,1][CondRef[,2]=='S3']~ gs[Traitement=='S3'])
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((CondRef[,1][CondRef[,2]=='S3']-gs[Traitement=='S3'])^2)/(length(gs[Traitement=='S3'])-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

par(mar=c(0,4.5,0,0))
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(expression('Reference water potential : '~Psi[f]~'=  -2.579'),
                           'C1 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 16.738'),
                           expression(S[f]~'= 1.066'),
                           expression(gamma~' = 0.0731'),
                           'S1 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 14.775'),
                           expression(S[f]~'= 5.0.98'),expression(gamma~'= 0.2363')),pch =rep(NA,11),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(' ',
                           'C3 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 16.679'),
                           expression(S[f]~'= 1.481'),
                           expression(gamma~' = 0.077'),
                           'S3 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 18.719'),
                           expression(S[f]~'= 0.733'),expression(gamma~'= 0.1568')),pch =rep(NA,11),cex=1.2,bty='n')





# comparaison avec un mod�le unique

Cond2 <- function(g0,g1,An,Cs,Sf,Psif,PsiL,GAMMA){
Fpsi = ( 1 + exp (Sf * Psif ) ) / ( 1 + exp (Sf * (Psif - PsiL)))
Gs = g0 + g1 * An / (Cs-GAMMA) * Fpsi
}

#equation de minimization
sce2  <-  function(param,x,  yobs)  {
Psif <- param[1] ; g0 <- param[2] ; g1 <- param[3] ; Sf <- param[4] ; GAMMA <- param[5]
ytheo <- Cond2(g0,g1,x[,1],380,Sf,Psif,x[,2],GAMMA)
return(sum((yobs  -  ytheo)^2))
}

# on fit

parini <- c(-1.9, 0.01, 16, 3.2,0)
Xdatas <- data.frame(Asat, PotMin/(-10))
Lower <- c(-100,0.01,rep(0,3))
fit2 <- optim(par  = parini,fn  =  sce2, x=Xdatas, yobs=gs,
               lower=Lower,method="L-BFGS-B")

# graphiquement
Gscal2 <- Cond2 (fit2$par[2],fit2$par[3],Xdatas[,1],380,fit2$par[4],fit2$par[1],Xdatas[,2],fit2$par[5])

par(mfrow=c(1,2))
par(mar=c(4.5,4.5,2,1))

plot(gs, Gscal2, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('All treatment')
ft <- lm (Gscal2~ gs)
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)

plot(Asat,gs,xlab=expression('Photosynthesis'~(�mol~m^-2~s-1)),ylab=expression('Gs'~(mmol~m^-2~s^-1)),cex.lab=1.2)
points(Asat,Gscal2,pch=16,col=4)
Sf <- sum((Gscal2-gs)^2)/(length(gs)-1)
legend('topleft',legend=c(paste('SCE/(n-1) = ',round(Sf,5)),'Tuzet model parameters',expression(Psi[f]~'=  -1.5501'),
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 19.1825'),
                           expression(S[f]~'= 0.460'),expression(gamma~'= 0.129')),pch =rep(NA,7),cex=1.2,bty='n')
legend('bottomright',legend=c('Measured conductance','Simulated conductance'),pch=c(1,16),col=c(1,4),bty='n',cex=1.2)



# si on fait deux mod�les
Cond3 <- function(g0k,g1k,Ank,Cs,Sfk,Psif,PsiLk,
                 g0K,g1K,AnK,SfK,PsiLK,GAMMAk,GAMMAK) {
Fpsik = ( 1 + exp (Sfk * Psif ) ) / ( 1 + exp (Sfk * (Psif - PsiLk)))
Gsk = g0k + g1k * Ank / (Cs-GAMMAk) * Fpsik
FpsiK = ( 1 + exp (SfK * Psif ) ) / ( 1 + exp (SfK * (Psif - PsiLK)))
GsK = g0K + g1K * AnK / (Cs-GAMMAK) * FpsiK

Output <- data.frame(c(Gsk,GsK),
                     c(rep('k',length(Gsk)),rep('K',length(GsK))))
}

#equation de minimization
sce3  <-  function(param,x,  yobs)  {

Psif <- param[1] ;
g0k <- param[2] ; g1k <- param[3] ; Sfk <- param[4] ;
g0K <- param[5] ; g1K <- param[6] ; SfK <- param[7] ;
GAMMAk <- param[8]; GAMMAK <- param[9]
ytheo <- Cond3(g0k,g1k,c(x[,1][Traitement=='C1'],x[,1][Traitement=='S1']),380,Sfk,Psif,c(x[,2][Traitement=='C1'],x[,2][Traitement=='S1']),
           g0K,g1K,c(x[,1][Traitement=='C3'],x[,1][Traitement=='S3']),SfK,c(x[,2][Traitement=='C3'],x[,2][Traitement=='S3']),
           GAMMAk,GAMMAK)[,1]

return(sum((yobs  -  ytheo)^2))
}

# on fit

parini <- c(-2, rep(c(0.01, 15, 5),2),c(0.01,0.01))
Xdatas <- data.frame(Asat, PotMin/(-10))
Gsobs <- c(gs[Traitement=='C1'],gs[Traitement=='S1'],gs[Traitement=='C3'],gs[Traitement=='S3'])
Lower <- c(-100,rep(c(0.01,0,0),2),rep(0.01,2))

fit3 <- optim(par  = parini,fn  =  sce3, x=Xdatas, yobs=Gsobs,
               lower=Lower,method="L-BFGS-B")

# graphiquement
GscalK <- Cond3 (fit3$par[2],fit3$par[3],c(Xdatas[,1][Traitement=='C1'],Xdatas[,1][Traitement=='S1']),
                 380,fit3$par[4],fit3$par[1],c(Xdatas[,2][Traitement=='C1'],Xdatas[,2][Traitement=='S1']),
                 fit3$par[5],fit3$par[6],c(Xdatas[,1][Traitement=='C3'],Xdatas[,1][Traitement=='S3']),
                 fit3$par[7],c(Xdatas[,2][Traitement=='C3'],Xdatas[,2][Traitement=='S3']),fit3$par[8],fit3$par[9])

names(GscalK) <- c('GscalK', 'Trait')

par(mfrow=c(2,2))
par(mar=c(4.5,4.5,2,1))

plot(c(gs[Traitement=='C1'],gs[Traitement=='S1']), GscalK[,1][GscalK[,2]=='k'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('C1 and S1 treatment')
ft <- lm (GscalK[,1][GscalK[,2]=='k']~ c(gs[Traitement=='C1'],gs[Traitement=='S1']))
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((GscalK[,1][GscalK[,2]=='k']-c(gs[Traitement=='C1'],gs[Traitement=='S1']))^2)/(length(c(gs[Traitement=='C1'],gs[Traitement=='S1']))-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

plot(c(gs[Traitement=='C3'],gs[Traitement=='S3']), GscalK[,1][GscalK[,2]=='K'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('C3 and S3 treatment')
ft <- lm (GscalK[,1][GscalK[,2]=='K']~ c(gs[Traitement=='C3'],gs[Traitement=='S3']))
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((GscalK[,1][GscalK[,2]=='K']-c(gs[Traitement=='C3'],gs[Traitement=='S3']))^2)/(length(c(gs[Traitement=='C3'],gs[Traitement=='S3']))-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)
par(mar=c(0,4.5,0,0))
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(expression('Reference water potential : '~Psi[f]~'=  -1.773'),
                           'C1 and S1 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 17.773'),
                           expression(S[f]~'= 2.834'),
                           expression(gamma~'= 0.1666')),
                           pch =rep(NA,6),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(' ',
                           'C3 and S3 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 19.102'),
                           expression(S[f]~'= 0.454'),
                           expression(gamma~'= 0.184')),
                           pch =rep(NA,6),cex=1.2,bty='n')

#mod�le pluie

Cond4 <- function(g0p,g1p,Anp,Cs,Sfp,Psif,PsiLp,
                 g0P,g1P,AnP,SfP,PsiLP,GAMMAp,GAMMAP) {
Fpsip = ( 1 + exp (Sfp * Psif ) ) / ( 1 + exp (Sfp * (Psif - PsiLp)))
Gsp = g0p + g1p * Anp / (Cs-GAMMAp) * Fpsip
FpsiP = ( 1 + exp (SfP * Psif ) ) / ( 1 + exp (SfP * (Psif - PsiLP)))
GsP = g0P + g1P * AnP / (Cs-GAMMAP) * FpsiP

Output <- data.frame(c(Gsp,GsP),
                     c(rep('p',length(Gsp)),rep('P',length(GsP))))
}

#equation de minimization
sce4  <-  function(param,x,  yobs)  {

Psif <- param[1] ;
g0p <- param[2] ; g1p <- param[3] ; Sfp <- param[4] ;
g0P <- param[5] ; g1P <- param[6] ; SfP <- param[7] ;
GAMMAp <- param[8]; GAMMAP <- param[9]
ytheo <- Cond3(g0p,g1p,c(x[,1][Traitement=='S1'],x[,1][Traitement=='S3']),380,Sfp,Psif,c(x[,2][Traitement=='S1'],x[,2][Traitement=='S3']),
           g0P,g1P,c(x[,1][Traitement=='C1'],x[,1][Traitement=='C3']),SfP,c(x[,2][Traitement=='C1'],x[,2][Traitement=='C3']),
           GAMMAp,GAMMAP)[,1]

return(sum((yobs  -  ytheo)^2))
}

# on fit

parini <- c(-2, rep(c(0.01, 15, 5),2),c(0.01,0.01))
Xdatas <- data.frame(Asat, PotMin/(-10))
Gsobs <- c(gs[Traitement=='S1'],gs[Traitement=='S3'],gs[Traitement=='C1'],gs[Traitement=='C3'])
Lower <- c(-100,rep(c(0.01,0,0),2),rep(0.01,2))

fit4 <- optim(par  = parini,fn  =  sce4, x=Xdatas, yobs=Gsobs,
               lower=Lower,method="L-BFGS-B")

# graphiquement
GscalP <- Cond4 (fit4$par[2],fit4$par[3],c(Xdatas[,1][Traitement=='S1'],Xdatas[,1][Traitement=='S3']),
                 380,fit4$par[4],fit4$par[1],c(Xdatas[,2][Traitement=='S1'],Xdatas[,2][Traitement=='S3']),
                 fit4$par[5],fit4$par[6],c(Xdatas[,1][Traitement=='C1'],Xdatas[,1][Traitement=='C3']),
                 fit4$par[7],c(Xdatas[,2][Traitement=='C1'],Xdatas[,2][Traitement=='C3']),fit4$par[8],fit4$par[9])

names(GscalP) <- c('GscalP', 'Trait')

par(mfrow=c(2,2))
par(mar=c(4.5,4.5,2,1))

plot(c(gs[Traitement=='S1'],gs[Traitement=='S3']), GscalP[,1][GscalP[,2]=='p'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('S1 and S3 treatment')
ft <- lm (GscalP[,1][GscalP[,2]=='p']~ c(gs[Traitement=='S1'],gs[Traitement=='S3']))
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((GscalP[,1][GscalP[,2]=='p']-c(gs[Traitement=='S1'],gs[Traitement=='S3']))^2)/(length(c(gs[Traitement=='S1'],gs[Traitement=='S3']))-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)

plot(c(gs[Traitement=='C1'],gs[Traitement=='C3']), GscalP[,1][GscalP[,2]=='P'], xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2)
title('C1 and C3 treatment')
ft <- lm (GscalP[,1][GscalP[,2]=='P']~ c(gs[Traitement=='C1'],gs[Traitement=='C3']))
abline(0,1)
abline(ft,col=2,lwd=2)
legend('bottomright',legend=c('Identical curve','Fitted curve',paste('r� = ', round(summary(ft)$adj.r.squared,5))),lty=c(1,1,NA),col=c(1,2,1),bty='n',cex=1.2)
Sf <- sum((GscalP[,1][GscalP[,2]=='P']-c(gs[Traitement=='C1'],gs[Traitement=='C3']))^2)/(length(c(gs[Traitement=='C1'],gs[Traitement=='C3']))-1)
legend('topleft',legend=paste('SCE/(n-1) = ',round(Sf,5)),pch=NA,bty='n',cex=1.3)
par(mar=c(0,4.5,0,0))
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(expression('Reference water potential : '~Psi[f]~'=  -2.885'),
                           'S1 and S3 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 17.481'),
                           expression(S[f]~'= 0.926'),
                           expression(gamma~'= 0.102')),
                           pch =rep(NA,6),cex=1.2,bty='n')
plot(1,1,col=0,axes=F,ann=F)
legend('topleft',legend=c(' ',
                           'C1 and C3 treatment :',
                           expression(g[0]~'= 0.01'),
                           expression(g[1]~'= 17.142'),
                           expression(S[f]~'= 1.084'),
                           expression(gamma~'= 0.172')),
                           pch =rep(NA,6),cex=1.2,bty='n')

#comparons les 3 mod�les
Gsref <- c(gs[Traitement=='C1'],gs[Traitement=='S1'],gs[Traitement=='C3'],gs[Traitement=='S3'])
Gscal1 <- c(CondRef[,1][CondRef[,2]=='C1'],CondRef[,1][CondRef[,2]=='S1'],CondRef[,1][CondRef[,2]=='C3'],CondRef[,1][CondRef[,2]=='S3'])
GscalAll <- c(Gscal2[Traitement=='C1'],Gscal2[Traitement=='S1'],Gscal2[Traitement=='C3'],Gscal2[Traitement=='S3'])
Gscalk <- GscalK[,1]
Gscalp <- c(GscalP[,1][156:231],GscalP[,1][1:77],GscalP[,1][232:309],GscalP[,1][78:155])


p1 = 17 ; pall = 5 ; p3 = 9  ;p4=9

#rappel
#AIC = n ln(MSE) + 2p
#o� n est le nombre d'observations, MSE est la mean square error habituel, et p le nombre de param�tres estim�s.
#MSE = SCR / (n � k � 1)
#(k = nombre de variables explicatives) et SCR = sum (Ytheo - Yobs)�
#BIC = AIC(fit) + p*(log10(n)-2)

#soit
AIC1 <- length(gs) * log ( sum((Gscal1 - Gsref)^2) / (length(gs) - 2 - 1) ) + 2 * p1
AICAll <- length(gs) * log ( sum((GscalAll - Gsref)^2) / (length(gs) - 2 - 1) ) + 2 * pall
AICk <- length(gs) * log ( sum((Gscalk - Gsref)^2) / (length(gs) - 2 - 1) ) + 2 * p3
AICp <- length(gs) * log ( sum((Gscalp - Gsref)^2) / (length(gs) - 2 - 1) ) + 2 * p4

BIC1 <- AIC1 + p1 * (log10(length(gs))-2)
BICAll <- AICAll + pall * (log10(length(gs))-2)
BICk <- AICk + p3 * (log10(length(gs))-2)
BICp <- AICp + p4 * (log10(length(gs))-2)

MSE1 <- sum((Gscal1 - Gsref)^2) / (length(gs) - 2 - 1)
MSEAll <- sum((GscalAll - Gsref)^2) / (length(gs) - 2 - 1)
MSE3 <- sum((Gscalk - Gsref)^2) / (length(gs) - 2 - 1)
MSE4 <- sum((Gscalp - Gsref)^2) / (length(gs) - 2 - 1)

par(mfrow=c(2,2))
par(mar=c(4.5,4.5,2,1))

plot(Gsref, GscalAll, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 1 : no treatment effect')
abline(0,1,col=2,lwd=2)
segments(Gsref, GscalAll, Gsref, Gsref,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(AICAll,3)),paste('BIC = ',round(BICAll,3)),paste('MSE = ',round(MSEAll,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(Gsref, Gscal1, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 2 : K and P effect')
abline(0,1,col=2,lwd=2)
segments(Gsref, Gscal1, Gsref, Gsref,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(AIC1,3)),paste('BIC = ',round(BIC1,3)),paste('MSE = ',round(MSE1,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(Gsref, Gscalk, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 3 : only K effect')
abline(0,1,col=2,lwd=2)
segments(Gsref, Gscalk, Gsref, Gsref,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(AICk,3)),paste('BIC = ',round(BICk,3)),paste('MSE = ',round(MSE3,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

plot(Gsref, Gscalp, xlab='Measured conductance',ylab='Fitted Conductance',cex.lab=1.2,pch=16)
title('Method 4 : only P effect')
abline(0,1,col=2,lwd=2)
segments(Gsref, Gscalp, Gsref, Gsref,col=4)
legend('bottomright',legend=c(paste('AIC = ', round(AICp,3)),paste('BIC = ',round(BICp,3)),paste('MSE = ',round(MSE4,5))),pch=c(NA,NA,NA),bty='n',cex=1.4)

