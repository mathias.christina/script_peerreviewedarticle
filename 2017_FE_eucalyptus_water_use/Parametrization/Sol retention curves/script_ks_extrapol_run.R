set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Paramétrisation de MAESPA/Courbes de rétention")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

ksat = c(751.366010241119, 892.600222692457, 559.287481307299, 112.987369961071)
Dpt <- c(0.15,0.5,1.5,2.5)
Kext <-c(100,67,50,29,13)
Dpt2 <- c(3.5,4.5,5.5,6.5,7.5)

fit1 <- nls(ksat~a*Dpt^b, start=c(a=380,b=-0.5))
fit2 <- nls(ksat~a*exp(Dpt*b), start=c(a=1000,b=-0.8))
fit3 <- nls(ksat~a*log(Dpt)+b, start=c(a=-200,b=500))

plot(Dpt,log(ksat),xlim=c(0,10),ylim=c(0,7),ylab='Soil hydraulic conductivity (mol/m/s/MPa)',xlab='Depth (m)',pch=16)
plot(Dpt,ksat,xlim=c(0,10),ylim=c(0,1000),ylab='Soil hydraulic conductivity (mol/m/s/MPa)',xlab='Depth (m)',pch=16)
points(Dpt2,Kext,pch=16)
X <- seq(0.01,10,length.out=100)
points(X,summary(fit1)$coef[1,1]*X^summary(fit1)$coef[2,1],col=2,type='l',lwd=2)
points(X,summary(fit2)$coef[1,1]*exp(X*summary(fit2)$coef[2,1]),col=3,type='l',lwd=2)
points(X,summary(fit3)$coef[1,1]*log(X)+summary(fit3)$coef[2,1],col=4,type='l',lwd=2)

fit <- lm(log(ksat)~Dpt)
fit <- lm(c(log(ksat),log(ksat[4]))~c(Dpt,3.5))
plot(Dpt,log(ksat),ylim=c(0,7),xlim=c(0,10))
abline(v=3.5)
abline(fit)

fit <- lm(log(ksat)~Dpt)

CFORD <- coef(fit)[1]
PMIN <- (CFORD-log(ksat[4]))/(0-2.5)
abline(CFORD,PMIN)
PMAX <- (CFORD-log(ksat[4]))/(0-3.5)
abline(CFORD,PMAX)

PENTE <- seq(PMIN,PMAX,length.out=5)

YNEW <- c(3:9)+0.5

KNEW <- CFORD + PENTE[1]*YNEW
for (i in 2:5){
  KNEW <- data.frame(KNEW, CFORD+PENTE[i]*YNEW)
}
KNEW <- exp(KNEW)

require(Maeswrap)
for (i in 1:5){ # colonne
  
  for (j in 1:8){  # profondeur
    
  From <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Paramétrisation de MAESPA/Courbes de rétention/original")
  To <- c("C:/Users/mathias/Documents/Cours et stage/thesis/Paramétrisation de MAESPA/Courbes de rétention/OUT")
  
  file.copy(paste(From,"/0002_confile.dat",sep=""), To,overwrite=TRUE)
  file.copy(paste(From,"/0002_phy1.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_phy2.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_phy3.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_trees.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_met.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_str1.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_str2.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_str3.dat",sep=""),To,overwrite=TRUE)
  file.copy(paste(From,"/0002_watpars.dat",sep=""),To,overwrite=TRUE)


setwd(To)

KSAT <- c(ksat,KNEW[,i])
KSATNEW <- KSAT[1:3+j]

replacePAR(datfile="0002_watpars.dat", parname = "ksat", namelist = "soilret",KSATNEW)

num <- 1000+(i-1)*8+j

file.rename("0002_confile.dat",paste(num,"_confile.dat",sep=""))
file.rename("0002_trees.dat",paste(num,"_trees.dat",sep=""))
file.rename("0002_met.dat",paste(num,"_met.dat",sep=""))
file.rename("0002_phy1.dat",paste(num,"_phy1.dat",sep=""))
file.rename("0002_phy2.dat",paste(num,"_phy2.dat",sep=""))
file.rename("0002_phy3.dat",paste(num,"_phy3.dat",sep=""))
file.rename("0002_str1.dat",paste(num,"_str1.dat",sep=""))
file.rename("0002_str2.dat",paste(num,"_str2.dat",sep=""))
file.rename("0002_str3.dat",paste(num,"_str3.dat",sep=""))
file.rename("0002_watpars.dat",paste(num,"_watpars.dat",sep=""))

}}



for( i in 1001:1040){
  system('Maespa_cluster_Dec2014',input=i)
}


