set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Paramétrisation de MAESPA/Courbes de rétention")

setwd(set1)

dat <- read.table("Pot_VWC_EUC.txt",header=T)
attach(dat)

# 1 cmH2O = 98,0638 pascals 

Potcal <- function(Psie, Bpar, SWC, Porosity){
Pot <- Psie *(SWC/Porosity)^(-Bpar)
#print(Pot)
}

#equation de minimization
sce1  <-  function(param,x,  yobs)  {
Psie <- param[1] ; Bpar <- param[2] ; Porosity <-  max(x,na.rm=T)
ytheo <- Potcal(Psie,Bpar,x,Porosity)
return(sum((yobs  -  ytheo)^2))
}


VWC <- VWC_15[1:78]
Potobs <- -Pot_15[1:78]/(98.0638*100) 
fit15 <- optim(par  = c(-0.01,4),fn  =  sce1,x=VWC,yobs=Potobs, method="Nelder-Mead")
fit50 <- optim(par  = c(-0.01,4),fn  =  sce1,x=VWC_50,yobs=-Pot_50/(98.0638*100), method="Nelder-Mead")
fit100 <- optim(par  = c(-0.01,4),fn  =  sce1,x=VWC_100,yobs=-Pot_100/(98.0638*100), method="Nelder-Mead")
fit200 <- optim(par  = c(-0.01,4),fn  =  sce1,x=VWC_200,yobs=-Pot_200/(98.0638*100), method="Nelder-Mead")
fit300 <- optim(par  = c(-0.01,4),fn  =  sce1,x=VWC_300,yobs=-Pot_300/(98.0638*100), method="Nelder-Mead")

par (mfrow=c(2,3))
par(mar=c(4.5,4.5,2,1))
plot(VWC_15,-Pot_15/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_15,Potcal(fit15$par[1],fit15$par[2],VWC_15,max(VWC_15,na.rm=T)),type='l',col=2,lwd=2)
legend('bottomright',legend=c(paste('Psie = ',round(fit15$par[1],5)),paste('Bpar = ',round(fit15$par[2],3)),paste('Porosity = ',round(max(VWC_15,na.rm=T),3))),
                    pch=rep(NA,3), bty='n', cex=1.3)
title ('15 cm')
plot(VWC_50,-Pot_50/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_50,Potcal(fit50$par[1],fit50$par[2],VWC_50,max(VWC_50,na.rm=T)),type='l',col=2)
legend('bottomright',legend=c(paste('Psie = ',round(fit50$par[1],5)),paste('Bpar = ',round(fit50$par[2],3)),paste('Porosity = ',round(max(VWC_50,na.rm=T),3))),
                    pch=rep(NA,3), bty='n', cex=1.3)
title ('50 cm')
plot(VWC_100,-Pot_100/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_100,Potcal(fit100$par[1],fit100$par[2],VWC_100,max(VWC_100,na.rm=T)),type='l',col=2)
legend('bottomright',legend=c(paste('Psie = ',round(fit100$par[1],5)),paste('Bpar = ',round(fit100$par[2],3)),paste('Porosity = ',round(max(VWC_100,na.rm=T),3))),
                    pch=rep(NA,3), bty='n', cex=1.3)
title ('100 cm')
plot(VWC_200,-Pot_200/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_200,Potcal(fit200$par[1],fit200$par[2],VWC_200,max(VWC_200,na.rm=T)),type='l',col=2)
legend('bottomright',legend=c(paste('Psie = ',round(fit200$par[1],5)),paste('Bpar = ',round(fit200$par[2],3)),paste('Porosity = ',round(max(VWC_200,na.rm=T),3))),
                    pch=rep(NA,3), bty='n', cex=1.3)
title ('200 cm')
plot(VWC_300,-Pot_300/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_300,Potcal(fit300$par[1],fit300$par[2],VWC_300,max(VWC_300,na.rm=T)),type='l',col=2)
legend('bottomright',legend=c(paste('Psie = ',round(fit300$par[1],5)),paste('Bpar = ',round(fit300$par[2],3)),paste('Porosity = ',round(max(VWC_300,na.rm=T),3))),
                    pch=rep(NA,3), bty='n', cex=1.3)
title ('300 cm')
plot(1,1,col=0,axes=F,ann=F)
legend('top',legend=c('Nelder and Mead (1965) method','Initial parameters :','  Psie = -0.01', '  Bpar = 4', '  Porosity = 0.2'),
        col=rep(0,5),bty='n',cex=1.6)
legend('bottom',legend=expression('SoilWP = Psie * '(SWC~'/'~Porosity)^-Bpar),bty='n',col=0,cex=1.4)


Mat <- rbind(c(fit15$par,max(VWC_15,na.rm=T)),c(fit50$par,max(VWC_50,na.rm=T)),c(fit100$par,max(VWC_100,na.rm=T)),
             c(fit200$par,max(VWC_200,na.rm=T)),c(fit300$par,max(VWC_300,na.rm=T)))
Mat2 <- data.frame(c(15,50,100,200,300),Mat)
names(Mat2) <- c('Depth','Psie','Bpar','Porosity')

write.table(Mat2,file='Retention_parameters_EUC.txt')




par(mar=c(4.5,4.5,2,1))
plot(VWC_50,-Pot_50/(98.0638*100),ylab='Soil Water Potential (MPa)',xlab=expression('SWC'~(m^3~m^-3)),cex.lab=1.2)
points(VWC_50,Potcal(fit50$par[1],fit50$par[2],VWC_50,fit50$par[3]),type='l',col=2)
points(VWC_50,Potcal(fit502$par[1],fit502$par[2],VWC_50,0.4),type='l',col=4)
legend('bottomright',legend=c(paste('Porosity = ',round(fit50$par[3],3)),paste('Porosity = ',0.4)),
                    pch=NA,lty=c(1,1),col=c(2,4), bty='n', cex=1.3)
title ('50 cm')
