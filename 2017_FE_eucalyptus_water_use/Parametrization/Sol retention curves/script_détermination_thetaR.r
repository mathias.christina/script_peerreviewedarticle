set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Paramétrisation de MAESPA/Courbes de rétention")

setwd(set1)

dat <- read.table("Humidite_tot.txt",header=T)
attach(dat)


Pr <- c('0-15cm','15-30cm','30-65cm','65-1m','1-2m','2-3m','3-4m','4-5m','5-6m','6-7m','7-8m','8-9m','9-10m')

par(mar=c(4.5,4.5,3,1))
plot(dat[,3+i],ylim=c(0,0.25),ylab=expression('Measured SWC'~~(m^3~m^-3)),xlab='2010 to 2012',cex.lab=1.2)
title(Pr[i])
Y <-replace(dat[,3+i],c(dat[,3+i]<0.14),NA)
abline(h=min(Y,na.rm=T),col=2)
legend('bottomleft',legend=c(paste('SWCmin = ',round(min(Y,na.rm=T),5))),pch=NA,lty=1,col=2,text.col=2,bty='n',cex=1.2)

