set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/LAI2000")

setwd(set1)

DatAbove <- read.table('above_2octPM_EUC.txt')
DatBelow <- read.table('below_2octPM_EUC.txt')

names(DatAbove) <- c('Loca','numa','Hra','Mina','Seca','an1a','an2a','an3a','an4a','an5a')
names(DatBelow) <- c('locb','Ty','numb','Hrb','Minb','Secb','an1b','an2b','an3b','an4b','an5b')

# on retire les ? dans DatBelow
DB <- DatBelow[1,]
for (i in 2:dim(DatBelow)[1]){
if( c(DatBelow[i,2]=='?')==FALSE){
DB <- rbind(DB, DatBelow[i,])}}

names(DB) <- c('Locb','Ty','numb','Hrb','Minb','Secb','an1b','an2b','an3b','an4b','an5b')

attach(DatAbove) ; attach(DB)


Houra <- Hra *60*60 + Mina*60 + Seca
Hourb <- Hrb *60*60 + Minb*60 + Secb

# Maintenant on extrapole les valeurs d'interception entre les mesures

HRA <- c(min(Houra):max(Houra))

An1A<-c();An2A<-c();An3A<-c();An4A<-c();An5A<-c();
for (j in 1:(length(Houra)-1)){

k = Houra[j+1]-Houra[j]
Extr1<-c();Extr2<-c();Extr3<-c();Extr4<-c();Extr5<-c();

for (i in 1:(k-1)){
Extr1 <- c(Extr1, an1a[j]+ (an1a[j+1]-an1a[j])/k*i)
Extr2 <- c(Extr2, an2a[j]+ (an2a[j+1]-an2a[j])/k*i)
Extr3 <- c(Extr3, an3a[j]+ (an3a[j+1]-an3a[j])/k*i)
Extr4 <- c(Extr4, an4a[j]+ (an4a[j+1]-an4a[j])/k*i)
Extr5 <- c(Extr5, an5a[j]+ (an5a[j+1]-an5a[j])/k*i)
}
An1A <- c(An1A, an1a[j], Extr1)
An2A <- c(An2A, an2a[j], Extr2)
An3A <- c(An3A, an3a[j], Extr3)
An4A <- c(An4A, an4a[j], Extr4)
An5A <- c(An5A, an5a[j], Extr5)
}
HRA <- HRA[1:1901]

par(mar=c(4.5,4.5,1,1))
plot(HRA,An1A,col=2,type='l',lwd=2,ylim=c(0,6),
     ylab='Sensor Above canopy',xlab='Time of the day (s)',cex.lab=1.2)
points(HRA,An2A,col=3,type='l',lwd=2)
points(HRA,An3A,col=4,type='l',lwd=2)
points(HRA,An4A,col=5,type='l',lwd=2)
points(HRA,An5A,col=6,type='l',lwd=2)
legend('topright',legend=c('ring 1: 0-13�','ring 2: 16-28�','ring 3: 32-43�','ring 4: 47-58�','ring 5: 61-74�'),
        col=c(2:6),pch=rep(16,5),bty='n')
for (i in 1:length(Hourb)){ abline(v=Hourb[i])}

# on calibre

CalB <- data.frame(Hourb[Locb=='Above1'],an1b[Locb=='Above1'],an2b[Locb=='Above1'],an3b[Locb=='Above1'],an4b[Locb=='Above1'],an5b[Locb=='Above1'])

CalA <- c(An1A[HRA==Hourb[Locb=='Above1'][1]],An2A[HRA==Hourb[Locb=='Above1'][1]],
                   An3A[HRA==Hourb[Locb=='Above1'][1]],An4A[HRA==Hourb[Locb=='Above1'][1]],
                   An5A[HRA==Hourb[Locb=='Above1'][1]])
for (i in 2:length(Hourb[Locb=='Above1'])){
X <- c(An1A[HRA==Hourb[Locb=='Above1'][i]],An2A[HRA==Hourb[Locb=='Above1'][i]],
                   An3A[HRA==Hourb[Locb=='Above1'][i]],An4A[HRA==Hourb[Locb=='Above1'][i]],
                   An5A[HRA==Hourb[Locb=='Above1'][i]])
CalA <- rbind(CalA,X)}

par(mar=c(4.5,4.5,1.5,1))
plot(CalA[,1],CalB[,2],pch=16,col=2,
                        ylim=c(min(CalB[,2:6]),max(CalB[,2:6])),xlim=c(min(CalA),max(CalA)),
                        ylab='Sensor "below"',xlab='Sensor "above"',cex.lab=1.2)
for (i in 2:5){
points(CalA[,i],CalB[,(i+1)],pch=16,col=i+1)}
legend('bottomright',legend=c('ring 1: 0-13�','ring 2: 16-28�','ring 3: 32-43�','ring 4: 47-58�','ring 5: 61-74�'),
        col=c(2:6),pch=rep(16,5),bty='n')
title('Calibration')
fit1 <- lm(CalB[,2]~CalA[,1])
fit2 <- lm(CalB[,3]~CalA[,2])
fit3 <- lm(CalB[,4]~CalA[,3])
fit4 <- lm(CalB[,5]~CalA[,4])
fit5 <- lm(CalB[,6]~CalA[,5])
abline(fit1,col=2)
abline(fit2,col=3)
abline(fit3,col=4)
abline(fit4,col=5)
abline(fit5,col=6)

#attention coefficient pas bon car Above �tait un peu cach� par appareil donc
# on compare B et A du 3 oct
# on compare B et A du 2 oct les 5 derniers
# on extrapole nouveau A du 2 oct

f1 <- c(0.06256261,1.12893034)
f2 <- c(-0.03191858,1.11604692)   # B3 = A3~f
f3 <- c(-0.0639814,1.0919177)
f4 <- c(-0.03493635,1.00874808)
f5 <- c(0.007823986,0.989418521)

# on fait maintenant A2new = (B2 - f(1))/f(2)
A2_1 <- (an1b[Locb=='Above3'] - f1[1])/f1[2]
A2_2 <- (an2b[Locb=='Above3'] - f2[1])/f2[2]
A2_3 <- (an3b[Locb=='Above3'] - f3[1])/f3[2]
A2_4 <- (an4b[Locb=='Above3'] - f4[1])/f4[2]
A2_5 <- (an5b[Locb=='Above3'] - f5[1])/f5[2]
Anew <- data.frame(Hourb[Locb=='Above3'],A2_1,A2_2,A2_3,A2_4,A2_5)

# on compare maintenant Above_2oct � A2new
CalAnew <- c(An1A[HRA==Hourb[Locb=='Above3'][1]],An2A[HRA==Hourb[Locb=='Above3'][1]],
                   An3A[HRA==Hourb[Locb=='Above3'][1]],An4A[HRA==Hourb[Locb=='Above3'][1]],
                   An5A[HRA==Hourb[Locb=='Above3'][1]])
for (i in 2:length(Hourb[Locb=='Above3'])){
X <- c(An1A[HRA==Hourb[Locb=='Above3'][i]],An2A[HRA==Hourb[Locb=='Above3'][i]],
                   An3A[HRA==Hourb[Locb=='Above3'][i]],An4A[HRA==Hourb[Locb=='Above3'][i]],
                   An5A[HRA==Hourb[Locb=='Above3'][i]])
CalAnew <- rbind(CalAnew,X)}

par(mar=c(4.5,4.5,1.5,1))
plot(CalAnew[,1],Anew[,2],pch=16,col=2,
                        ylim=c(min(Anew[,2:6]),max(Anew[,2:6])),xlim=c(min(CalAnew),max(CalAnew)),
                        ylab='New values of A',xlab='Anterior values of A',cex.lab=1.2)
for (i in 2:5){
points(CalAnew[,i],Anew[,(i+1)],pch=16,col=i+1)}
legend('bottomright',legend=c('ring 1: 0-13�','ring 2: 16-28�','ring 3: 32-43�','ring 4: 47-58�','ring 5: 61-74�'),
        col=c(2:6),pch=rep(16,5),bty='n')
title('Calibration')
ft1 <- lm(Anew[,2]~CalAnew[,1])
ft2 <- lm(Anew[,3]~CalAnew[,2])
ft3 <- lm(Anew[,4]~CalAnew[,3])
ft4 <- lm(Anew[,5]~CalAnew[,4])
ft5 <- lm(Anew[,6]~CalAnew[,5])
abline(ft1,col=2)
abline(ft2,col=3)
abline(ft3,col=4)
abline(ft4,col=5)
abline(ft5,col=6)


# calcul des gap fraction
# new one
Aa1<-c();Aa2<-c();Aa3<-c();Aa4<-c();Aa5<-c()
for (i in 1:length(HrB)){
#Aa1 <- c(Aa1, An1A[HRA==HrB[i]]*summary(ft1)$coef[2,1]+summary(ft1)$coef[1,1])
#Aa2 <- c(Aa2, An2A[HRA==HrB[i]]*summary(ft2)$coef[2,1]+summary(ft2)$coef[1,1])
#Aa3 <- c(Aa3, An3A[HRA==HrB[i]]*summary(ft3)$coef[2,1]+summary(ft3)$coef[1,1])
Aa4 <- c(Aa4, An4A[HRA==HrB[i]]*summary(ft4)$coef[2,1]+summary(ft4)$coef[1,1])
Aa5 <- c(Aa5, An5A[HRA==HrB[i]]*summary(ft5)$coef[2,1]+summary(ft5)$coef[1,1])
}

TyB <- c(rep('P1',54),rep('P2',54),rep('P3',54),rep('P4',55))
HrB <- c(Hourb[Locb=='P1'],Hourb[Locb=='P2'],Hourb[Locb=='P3'],Hourb[Locb=='P4'])
#Ab1 <- c(an1b[Locb=='P1']*f1[2]+f1[1],an1b[Locb=='P2']*f2[2]+f2[1],
#         an1b[Locb=='P3']*f3[2]+f3[1],an1b[Locb=='P4']*f4[2]+f4[1])
#Ab2 <- c(an2b[Locb=='P1']*f1[2]+f1[1],an2b[Locb=='P2']*f2[2]+f2[1],
#         an2b[Locb=='P3']*f3[2]+f3[1],an2b[Locb=='P4']*f4[2]+f4[1])
#Ab3 <- c(an3b[Locb=='P1']*f1[2]+f1[1],an3b[Locb=='P2']*f2[2]+f2[1],
#         an3b[Locb=='P3']*f3[2]+f3[1],an3b[Locb=='P4']*f4[2]+f4[1])
Ab4 <- c(an4b[Locb=='P1']*f1[2]+f1[1],an4b[Locb=='P2']*f2[2]+f2[1],
         an4b[Locb=='P3']*f3[2]+f3[1],an4b[Locb=='P4']*f4[2]+f4[1])
Ab5 <- c(an5b[Locb=='P1']*f1[2]+f1[1],an5b[Locb=='P2']*f2[2]+f2[1],
         an5b[Locb=='P3']*f3[2]+f3[1],an5b[Locb=='P4']*f4[2]+f4[1])


#TyB <- c(rep('P1',54),rep('P2',54),rep('P3',54),rep('P4',55))
#HrB <- c(Hourb[Locb=='P1'],Hourb[Locb=='P2'],Hourb[Locb=='P3'],Hourb[Locb=='P4'])
Ab1 <- c(an1b[Locb=='P1']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],an1b[Locb=='P2']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],
         an1b[Locb=='P3']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],an1b[Locb=='P4']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1])
Ab2 <- c(an2b[Locb=='P1']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],an2b[Locb=='P2']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],
         an2b[Locb=='P3']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],an2b[Locb=='P4']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1])
Ab3 <- c(an3b[Locb=='P1']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],an3b[Locb=='P2']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],
         an3b[Locb=='P3']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],an3b[Locb=='P4']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1])
#Ab4 <- c(an4b[Locb=='P1']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],an4b[Locb=='P2']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],
#         an4b[Locb=='P3']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],an4b[Locb=='P4']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1])
#Ab5 <- c(an5b[Locb=='P1']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],an5b[Locb=='P2']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],
#         an5b[Locb=='P3']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],an5b[Locb=='P4']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1])

#Aa1<-c();Aa2<-c();Aa3<-c();Aa4<-c();Aa5<-c()
for (i in 1:length(HrB)){
Aa1 <- c(Aa1, An1A[HRA==HrB[i]])
Aa2 <- c(Aa2, An2A[HRA==HrB[i]])
Aa3 <- c(Aa3, An3A[HRA==HrB[i]])
#Aa4 <- c(Aa4, An4A[HRA==HrB[i]])
#Aa5 <- c(Aa5, An5A[HRA==HrB[i]])
}


par(mfrow=c(2,3))
par(mar=c(4.5,4.5,2,1))
par(xaxs='i',yaxs='i')
plot(Ab1/Aa1,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.4),cex.lab=1.2)
abline(v=54.5) ; abline(v=54.5+54) ; abline(v=54.5+2*54)
text(x=25,y=0.36,'P1',cex=1.2)
text(x=54+25,y=0.36,'P2',cex=1.2)
text(x=108+25,y=0.36,'P3',cex=1.2)
text(x=162+25,y=0.36,'P4',cex=1.2)
title('7�')
plot(Ab2/Aa2,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.2),cex.lab=1.2)
abline(v=54.5) ; abline(v=54.5+54) ; abline(v=54.5+2*54)
text(x=25,y=0.18,'P1',cex=1.2)
text(x=54+25,y=0.18,'P2',cex=1.2)
text(x=108+25,y=0.18,'P3',cex=1.2)
text(x=162+25,y=0.18,'P4',cex=1.2)
title('23�')
plot(Ab3/Aa3,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.1),cex.lab=1.2)
abline(v=54.5) ; abline(v=54.5+54) ; abline(v=54.5+2*54)
text(x=25,y=0.09,'P1',cex=1.2)
text(x=54+25,y=0.09,'P2',cex=1.2)
text(x=108+25,y=0.09,'P3',cex=1.2)
text(x=162+25,y=0.09,'P4',cex=1.2)
title('38�')
plot(Ab4/Aa4,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.08),cex.lab=1.2)
abline(v=54.5) ; abline(v=54.5+54) ; abline(v=54.5+2*54)
text(x=25,y=0.072,'P1',cex=1.2)
text(x=54+25,y=0.072,'P2',cex=1.2)
text(x=108+25,y=0.072,'P3',cex=1.2)
text(x=162+25,y=0.072,'P4',cex=1.2)
title('53�')
plot(Ab5/Aa5,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.05),cex.lab=1.2)
abline(v=54.5) ; abline(v=54.5+54) ; abline(v=54.5+2*54)
text(x=25,y=0.045,'P1',cex=1.2)
text(x=54+25,y=0.045,'P2',cex=1.2)
text(x=108+25,y=0.045,'P3',cex=1.2)
text(x=162+25,y=0.045,'P4',cex=1.2)
title('68�')

GF <- c(sum(Ab1[TyB=='P1'])/sum(Aa1[TyB=='P1']),sum(Ab2[TyB=='P1'])/sum(Aa2[TyB=='P1']),sum(Ab3[TyB=='P1'])/sum(Aa3[TyB=='P1']),
        sum(Ab4[TyB=='P1'])/sum(Aa4[TyB=='P1']),sum(Ab5[TyB=='P1'])/sum(Aa5[TyB=='P1']),
        sum(Ab1[TyB=='P2'])/sum(Aa1[TyB=='P2']),sum(Ab2[TyB=='P2'])/sum(Aa2[TyB=='P2']),sum(Ab3[TyB=='P2'])/sum(Aa3[TyB=='P2']),
        sum(Ab4[TyB=='P2'])/sum(Aa4[TyB=='P2']),sum(Ab5[TyB=='P2'])/sum(Aa5[TyB=='P2']),
        sum(Ab1[TyB=='P3'])/sum(Aa1[TyB=='P3']),sum(Ab2[TyB=='P3'])/sum(Aa2[TyB=='P3']),sum(Ab3[TyB=='P3'])/sum(Aa3[TyB=='P3']),
        sum(Ab4[TyB=='P3'])/sum(Aa4[TyB=='P3']),sum(Ab5[TyB=='P3'])/sum(Aa5[TyB=='P3']),
        sum(Ab1[TyB=='P4'])/sum(Aa1[TyB=='P4']),sum(Ab2[TyB=='P4'])/sum(Aa2[TyB=='P4']),sum(Ab3[TyB=='P4'])/sum(Aa3[TyB=='P4']),
        sum(Ab4[TyB=='P4'])/sum(Aa4[TyB=='P4']),sum(Ab5[TyB=='P4'])/sum(Aa5[TyB=='P4']))
TyGF <- c(rep('P1',5),rep('P2',5),rep('P3',5),rep('P4',5))
Ang <- rep(c(7,23,38,53,68),4)

par(mar=c(4.5,4.5,2,1))
par(xaxs='i',yaxs='i')
plot(Ang[TyGF=='P1'],GF[TyGF=='P1'],ylab='Mean Gap Fraction',xlab='View angle (�)',
          ylim=c(0,0.1),xlim=c(0,90),cex.lab=1.2,type='b',pch=16,lty=1,col=2)
points(Ang[TyGF=='P2'],GF[TyGF=='P2'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=3)
points(Ang[TyGF=='P3'],GF[TyGF=='P3'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=4)
points(Ang[TyGF=='P4'],GF[TyGF=='P4'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=5)
legend('topright',legend=c('P1','P2','P3','P4'),col=c(2:5),pch=rep(16,4),lty=rep(1,4),bty='n')







