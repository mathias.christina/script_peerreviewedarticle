  set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/Interception")
  source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
  
  setwd(set1)
  
  #
  dat2 <- read.table('datas_ecoulement.txt',header=T)
  attach(dat2)
  
  PP <- rep(PPmm,3)
  Ec <- c(EcmmTr1,EcmmTr2,EcmmTr3)
  
  fit1 <- nls(Ec ~ a0 * PP^b / (a1 + PP^b), start=c(a0=5,a1=2000,b=2.5),upper=c(a0=10,a1=5000,b=10),lower=c(a0=1,a1=1,b=1.1),algorithm='port')
  
  PP2 <- seq(0,130,length.out=100)
  Ecth <- summary(fit1)$coef[1,1] * PP2^summary(fit1)$coef[3,1] / (summary(fit1)$coef[2,1] + PP2^summary(fit1)$coef[3,1])
  
  par(mar=c(4.5,4.5,1,1))
  plot(1,1,col=0,axes=F,ann=F,xlim=c(0,125),ylim=c(0,5))
  rect(19,-1,21,6,col='grey',border=NA)
  rect(39,-1,41,6,col='grey',border=NA)
  par(new=T)
  plot(PP,Ec, xlab='Weekly precipitation (mm)',ylab='Weekly stemflow (mm)',cex.lab=1.2,xlim=c(0,125),ylim=c(0,5))
  points(PP2,Ecth,col=2,type='l',lwd=2)
  grid(nx=NA,ny=NULL,col=1)
  legend('bottomright',legend= c( expression('Y ='~a[0]~'*'~X^b~'/'~(a[1]~'+'~X^b)),
                                  paste('a0 = ',round(summary(fit1)$coef[1,1],3)),
                                  paste('a1 = ',round(summary(fit1)$coef[2,1],1)),
                                  paste('b = ', round(summary(fit1)$coef[3,1],3)),
                                  paste('RMSE = ',round(RMSE(fit1),5))),
                      pch=rep(NA,5),cex=1.2,bg='white',box.col=0)
  

# d�riv�e

derFun <- function(prep,a0,a1,b){
Out <- ( a0*b*prep^(b-1)*(a1+prep^b) - a0*prep^b*b*prep^(b-1)) / (a1+prep^b)^2
}

Yinf <- max( derFun(PP2,summary(fit1)$coef[1,1],summary(fit1)$coef[2,1],summary(fit1)$coef[3,1]))
sce  <-  function(PPI, Y1)  {
Y2 <- derFun(PPI,summary(fit1)$coef[1,1],summary(fit1)$coef[2,1],summary(fit1)$coef[3,1])
return(sum((Y2  -  Y1)^2))
}
ResEq <- optim(par  = 30,fn  =  sce, Y1 = Yinf,
               method="L-BFGS-B")
Xinf <- ResEq$par

plot(PP2,derFun(PP2,summary(fit1)$coef[1,1],summary(fit1)$coef[2,1],summary(fit1)$coef[3,1]),type='l',lwd=2,
          ylab='d Ec / d PP', xlab=' Precipitation (mm)',cex.lab=1.2)
abline(h=Yinf,col=2)
text(labels=paste('Ymax = ',round(Yinf,4)), x=80,y=Yinf,pos=1,cex=1.2)
abline(v=Xinf,col=4)
text(labels=paste('Xmax = ',round(Xinf,4)), x=Xinf,y=0.02,pos=4,cex=1.2)

# du coup on connait l'ordonn�e � l'origine ainsi :
Ecinf <- summary(fit1)$coef[1,1] * Xinf^summary(fit1)$coef[3,1] / (summary(fit1)$coef[2,1] + Xinf^summary(fit1)$coef[3,1])

Ord2 <- Ecinf - (1-0.9096082-0.06) * Xinf
Ord2 <- Ecinf - (1-0.9096082-0.046) * Xinf
Ord2 <- Ecinf - (1-0.9096082-0.047) * Xinf
  
  
par(mar=c(4.5,4.5,1,1))
par(yaxs='i',xaxs='i')
plot(PP2,Ecth, xlab='Weekly precipitation (mm)',ylab='Weekly stemflow (mm)',cex.lab=1.2,ylim=c(-1,5),col=2,type='l',lwd=2)
abline(coef=c(Ord2,1-0.9096082-0.005),col=3,lwd=1.5)
legend('bottomright',legend= c( paste('p1 = ',round(0.9096082,5)),
                                paste('S1 = ',round(-Ord2,5),' mm')),
                    pch=rep(NA,2),,bty='n',cex=1)


# part �vapor�e

#corrEp = (abs(Ord2)-abs(Ord))/abs(Ord2)
corrEp=0.06
# calcule des ruttera et rutterb
# D = RutA + RutB ln (C)
p =  0.9096082


####################  
  
#  Pla�ons nous dans  le premi�re zone
  
StemN <- replace(Ec,PP>20,NA)
PP3 <- replace(PP,PP>20,NA)
PP3 <- replace(PP3,PP3==0,NA)
  
fit <- nls(StemN~ exp(a + b * ((1-p)*PP3-0.06*PP3)),start=c(a=1,b=1))
  
fit <- nls(StemN~ a * log (b * ((1-p)*PP3)),start=c(a=1,b=10))
  
  
#pla�ons nous dans la premi�re zone
PzC <- c()
EzC <- c()
for (i in 1:length(PP)){
if (PP[i] < abs(Ord2)){
PzC <- c(PzC, PP[i])
EzC <- c(EzC, Ec[i])}}

Cont <- ((1-p)*PzC - EzC)/(1 + corrEp*PzC/0.6620706)
Cont2 <- replace(Cont,c(Cont==0),NA)

par(mar=c(4.5,4.5,1,1))
plot(PzC,Cont2,xlab='Precipitation (mm)',ylab=c('Canopy water content (mm)'),cex.lab=1.2)

Drain <- function(CWC, RutA, RutB){
Out <- exp(RutA + RutB * CWC)}
sce  <-  function(param,x,  yobs)  {
RutA <- param[1] ; RutB <- param[2]
ytheo <- Drain(x,RutA,RutB)
return(sum((yobs  -  ytheo)^2,na.rm=T))
}
fitD <- optim(par  = c(-20,3.7),fn  =  sce, x=Cont, yobs=EzC,
               method="L-BFGS-B")

CWC <- seq(0,max(Cont),length.out=100)
drainage <- Drain(CWC,fitD$par[1],fitD$par[2])

RutterD <- exp(fitD$par[1] + fitD$par[2]*abs(Ord2))

par(mar=c(4.5,4.5,3,1))
par(yaxs='i',xaxs='i')
plot(Cont,EzC, xlab= 'Canopy Water Content (mm)',ylab='Drainage (mm)',cex.lab=1.2)
points(CWC, drainage, type='l', col=2, lwd=2)
legend('topleft',legend=c('Excluding points',paste('RutterB = ',round(fitD$par[2],5)),
                          paste('RutterD = ',round(RutterD,5))),
                          pch=c(16,NA,NA),col=c(4,NA,NA),bty='n',cex=1.2)


# si on retire les valeurs ou le drainage est sup�rieure � la pluie
EzC2 <- replace(EzC,c(EzC>Cont),NA)
EzC3 <- replace(EzC,c(EzC<Cont),NA)

fitD <- optim(par  = c(-20,3.7),fn  =  sce, x=Cont, yobs=EzC2,
               method="L-BFGS-B")

CWC <- seq(0,max(Cont),length.out=100)
drainage <- Drain(CWC,fitD$par[1],fitD$par[2])
drainage2 <- Drain(CWC,-8.656608,3.7)
RutterD <- exp(fitD$par[1] + fitD$par[2]*abs(Ord2))

par(mar=c(4.5,4.5,3,1))
par(yaxs='i',xaxs='i')
par(mfrow=c(1,2))
plot(PzC,Cont,xlab='Precipitation (mm)',ylab=c('Canopy water content (mm)'),cex.lab=1.2)
plot(Cont,EzC, xlab= 'Canopy Water Content (mm)',ylab='Drainage (mm)',cex.lab=1.2)
points(CWC, drainage, type='l', col=2, lwd=2)
points(Cont,EzC3,pch=16,col=4)
legend('topleft',legend=c('Excluding points',paste('RutterB = ',round(fitD$par[2],5)),
                          paste('RutterD = ',round(RutterD,5))),
                          pch=c(16,NA,NA),col=c(4,NA,NA),bty='n',cex=1.2)

# on retire encore plus
EzC4 <- replace(EzC,c(EzC>(1/2*Cont)),NA)
EzC5 <- replace(EzC,c(EzC<(1/2*Cont)),NA)

fitD <- optim(par  = c(-20,3.7),fn  =  sce, x=Cont, yobs=EzC4,
               method="L-BFGS-B")

CWC <- seq(0,max(Cont),length.out=100)
drainage <- Drain(CWC,fitD$par[1],fitD$par[2])
RutterD <- exp(fitD$par[1] + fitD$par[2]*abs(Ord2))

par(mar=c(4.5,4.5,3,1))
par(yaxs='i',xaxs='i')
plot(Cont,EzC, xlab= 'Canopy Water Content (mm)',ylab='Drainage (mm)',cex.lab=1.2)
points(CWC, drainage, type='l', col=2, lwd=2)
points(Cont,EzC5,pch=16,col=4)
legend('topleft',legend=c('Excluding points',paste('RutterB = ',round(fitD$par[2],5)),
                          paste('RutterD = ',round(RutterD,5))),
                          pch=c(16,NA,NA),col=c(4,NA,NA),bty='n',cex=1.2)



P <- c(3.9,1.6,2.8,2.2)
Ev <- c(0.13,0.24,0.18,0.03)
