set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/Plant_conductance")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat <- read.table("Cond.txt",header=T)

dat1 <- rep(1,8)
for (i in 1:dim(dat)[1]){
if (is.na(dat$gs[i])==F){
dat1 <- rbind(dat1,dat[i,])}
}
dat1<- dat1[2:dim(dat1)[1],]

# en gros KP = Water use / LAI / (LeafPotmatin - LeafPotmidi)
dati <- read.table('Flux_C1.txt',header=T)
datii <- read.table('Flux_C3.txt',header=T)
datiii <- read.table('Flux_S1.txt',header=T)
dativ <- read.table('Flux_S3.txt',header=T)

A1 <- data.frame(rep('C1',50),dati) ; names(A1) <- c('Trat',names(dati))
A2 <- data.frame(rep('C3',50),datii); names(A2) <- c('Trat',names(dati))
A3 <- data.frame(rep('S1',50),datiii); names(A3) <- c('Trat',names(dati))
A4 <- data.frame(rep('S3',50),dativ); names(A4) <- c('Trat',names(dati))

FLUX <- rbind(A1,A2,A3,A4)


DATES <- c("06/08/2011", "05/09/2011", "01/10/2011",  "05/11/2011", "11/12/2011", "13/03/2012",  "16/04/2012", "15/05/2012", "24/06/2012", "02/08/2012")

WU <- c()            # unit� de d�part dm3 par 30min
for (j in 1:4){
for (i in 1:10){
for (k in 15:24){
WU <- c(WU,mean(FLUX[,k][FLUX$Trat==levels(FLUX$Trat)[j]][FLUX$Data==DATES[i]],na.rm=T))
}}}
WU <- WU /(30*60)# unit� dm3 d'eau par s par m2 de feuille

# a chercher pourquoi valeur aussi hote
WU <- replace(WU,c(WU>0.01),NA)

TRAT <- c(rep('C1',100),rep('C3',100),rep('S1',100),rep('S3',100))
NUM <- c(rep(c(1:10),10),rep(c(11:20),10),rep(c(21:30),10),rep(c(31:40),10))
DATES3 <- c('2011/08','2011/09','2011/10','2011/11','2011/12','2012/03','2012/04','2012/05','2012/06','2012/08')
DATA <-c(); for (i in 1:10){DATA <- c(DATA, rep(DATES3[i],10))}
DATA <- rep(DATA,4)
# plant� en 06/2010
Age <- rep(c(rep(14,10),rep(15,10),rep(16,10),rep(17,10),rep(18,10),rep(21,10),rep(22,10),rep(23,10),rep(24,10),rep(26,10)),4)

FLUX2 <- data.frame(WU,NUM,DATA,TRAT)

library(nlme)
ana1 <- lme(WU~Age*TRAT, random=~1|NUM,na.action=na.omit)

wu <-c() ; AGE <-c(14:18,21:24,26)
for (i in 1:10){
wu <- c(wu,mean(WU[TRAT=='C1'][Age==levels(factor(Age))[i]],na.rm=T),
           mean(WU[TRAT=='C3'][Age==levels(factor(Age))[i]],na.rm=T),
           mean(WU[TRAT=='S1'][Age==levels(factor(Age))[i]],na.rm=T),
           mean(WU[TRAT=='S3'][Age==levels(factor(Age))[i]],na.rm=T))}
TRAT2 <- rep(c('C1','C3','S1','S3'),10)


plot(Age,WU*3600)
points(AGE,wu[TRAT2=='C1']*3600,type='l',col=2,lwd=2)
points(AGE,wu[TRAT2=='C3']*3600,type='l',col=4,lwd=2)
points(AGE,wu[TRAT2=='S1']*3600,type='l',col=3,lwd=2)
points(AGE,wu[TRAT2=='S3']*3600,type='l',col=5,lwd=2)

