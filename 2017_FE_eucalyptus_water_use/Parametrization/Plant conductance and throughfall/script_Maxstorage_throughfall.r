set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/Interception")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")

setwd(set1)

dat1 <- read.table('datas_TH.txt',header=T)

DAT <- dat1$MONTH
for (i in 1:length(DAT)){
if(dat1$YEAR[i]==2006){DAT[i] <- DAT[i]+12}
if(dat1$YEAR[i]==2007){DAT[i] <- DAT[i]+24}
if(dat1$YEAR[i]==2008){DAT[i] <- DAT[i]+36}
if(dat1$YEAR[i]==2009){DAT[i] <- DAT[i]+48}
if(dat1$YEAR[i]==2010){DAT[i] <- DAT[i]+50}}

PP <- rep(dat1$PPT,3)
TH <- c(dat1$TH1,dat1$TH2,dat1$TH3)
AGE <- rep(DAT,3)

plot(AGE,TH/PP,xlab='Month since Jan. 2005')

#
#dat1 <- read.table('data_2003_Th.txt',header=T)
dat2 <- read.table('data_2006_Th.txt',header=T)
attach(dat2)

Th2 <- ThdivPP * PP

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,Th2,xlab='Precipitation (mm)',ylab='Throughfall (mm)',cex.lab=1.2)
fit <- lm(Th2~PP-1)
abline(fit,col=2,lwd=2)
grid(nx=NA,ny=NULL,col=1)
legend('bottomright',legend=c(paste('r1 = ',round(summary(fit)$coef[1],5))),pch=NA,cex=1.2,bg='white',box.col=0)
box()
title('2005 - 2006 after clear-cut')

TH <- sapply(c(2.2,3:12), function(i) mean(ThdivPP[ann�e==2005][mois[ann�e==2005]==i],na.rm=T))
TH <- c(TH, sapply(c(1,2.2,3:9),function(i) mean(ThdivPP[ann�e==2006][mois[ann�e==2006]==i],na.rm=T)))
AGE <- c(2:12,2:9)

par(mfrow=c(1,2))
par(mar=c(4.2,4.2,1,1))
plot(ThdivPP,ylab='Throughfall/precipitation',cex.lab=1.2)
plot(PP,ThdivPP,ylab='Throughfall/precipitation',xlab='precipitation',cex.lab=1.2)


fit2 <- nls(ThdivPP ~ a0 * PP / (a1 + PP), start=c(a0=0.9,a1=1),upper=c(a0=1,a1=50),lower=c(a0=0,a1=0),algorithm='port')
PP2 <- seq(0,140,length.out=100)
Ththeo2 <- summary(fit2)$coef[1,1] * PP2 / (summary(fit2)$coef[2,1] + PP2)

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(dat2$PP,dat2$ThdivPP, ylab='Th/PP',xlab='Precipitation (mm)')
points(PP2,Ththeo2,type='l',col=2,lwd=2)
abline(v=summary(fit2)$coef[2,1],col=4,lwd=2)
title('Exclusion : 20 to 27 months')
legend('bottomright',legend=c(expression('Th/PP'~'='~a[0]~'*'~'PP'~'/'~(a[1]~'+'~'PP')), paste('RMSE = ',round(RMSE(fit2),4)),
                            paste('a1 = ',round(summary(fit2)$coef[2,1],4))),
                  pch=rep(NA,3),cex=1.2,bty='n')

ThinfI <-c() ; ThsupP <-c()
PPinfI <- c() ; PPsupP <- c()
for (i in 1: length(ThdivPP)){
if(PP[i] < summary(fit2)$coef[2,1]){
ThinfI <- c(ThinfI, Th2[i])
PPinfI <- c(PPinfI, PP[i])}
else {
ThsupP <- c(ThsupP, Th2[i])
PPsupP <- c(PPsupP, PP[i])}
}


par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,Th2,xlab='Precipitation (mm)',ylab='Throughfall (mm)',cex.lab=1.2)
points(PPinfI,ThinfI,col=2,pch=1)
ft1 <- lm(ThinfI~PPinfI-1)
abline(ft1,col=2)
# y = (yb-ya)/(xb-xa) * x + ya -(yb - ya)/(xb-xa)*xa
xa = summary(fit2)$coef[2,1]
ya = xa *summary(ft1)$coef[1] 
PPS <- max(ya - xa,na.rm=T)
abline(coef=c(PPS,1),col=4)
legend('toplef',legend=c(paste('p = ',round(summary(ft1)$coef[1],5)),
                        paste ('S = ',round(-PPS,5),' mm')), 
                        bty='n',pch=c(NA,NA), cex=1.2)

# deuxi�me m�thode idem mais sans les valeurs >1

ThdivPP3 <- replace(ThdivPP,c(ThdivPP>1),NA)
Th3 <- ThdivPP3 * PP
fit3 <- nls(ThdivPP3 ~ a0 * PP / (a1 + PP), start=c(a0=0.9,a1=1),upper=c(a0=1,a1=50),lower=c(a0=0,a1=0),algorithm='port')
PP3 <- seq(0,140,length.out=100)
Ththeo3 <- summary(fit3)$coef[1,1] * PP3 / (summary(fit3)$coef[2,1] + PP3)

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,ThdivPP3, ylab='Th/PP',xlab='Precipitation (mm)',ylim=c(0,1.1))
points(PP3,Ththeo3,type='l',col=2,lwd=2)
abline(v=summary(fit3)$coef[2,1],col=4,lwd=2)
title('Exclusion : 20 to 27 months')
legend('bottomright',legend=c(expression('Th/PP'~'='~a[0]~'*'~'PP'~'/'~(a[1]~'+'~'PP')), paste('RMSE = ',round(RMSE(fit3),4)),
                            paste('a1 = ',round(summary(fit3)$coef[2,1],4))),
                  pch=rep(NA,3),cex=1.2,bty='n')

ThinfI <-c() ; ThsupP <-c()
PPinfI <- c() ; PPsupP <- c()
for (i in 1: length(ThdivPP3)){
if(PP[i] < summary(fit3)$coef[2,1]){
ThinfI <- c(ThinfI, Th3[i])
PPinfI <- c(PPinfI, PP[i])}
else {
ThsupP <- c(ThsupP, Th3[i])
PPsupP <- c(PPsupP, PP[i])}
}

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,Th3,xlab='Precipitation (mm)',ylab='Throughfall (mm)',cex.lab=1.2)
points(PPinfI,ThinfI,col=2,pch=1)
ft1 <- lm(ThinfI~PPinfI-1)
abline(ft1,col=2)
# y = (yb-ya)/(xb-xa) * x + ya -(yb - ya)/(xb-xa)*xa
xa = summary(fit3)$coef[2,1]
ya = xa *summary(ft1)$coef[1] 
PPS <- max(ya - xa,na.rm=T)
abline(coef=c(PPS,1),col=4)
legend('toplef',legend=c(paste('p = ',round(summary(ft1)$coef[1],5)),
                        paste ('S = ',round(-PPS,5),' mm')), 
                        bty='n',pch=c(NA,NA), cex=1.2)

# troisi�me m�thode, on utilise l'equation de Val�rier

fit4 <- nls(ThdivPP ~ a0 * PP^b / (a1 + PP^b), start=c(a0=0.9,a1=1,b=3),upper=c(a0=1,a1=50,b=10),lower=c(a0=0,a1=0,b=1),algorithm='port')
PP4 <- seq(0,140,length.out=100)
Ththeo4 <- summary(fit4)$coef[1,1] * PP4^summary(fit4)$coef[3,1] / (summary(fit4)$coef[2,1] + PP4^summary(fit4)$coef[3,1])

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,ThdivPP, ylab='Th/PP',xlab='Precipitation (mm)',cex.lab=1.2)
points(PP4,Ththeo4,type='l',col=2,lwd=2)
title('Exclusion : 20 to 27 months')
legend('bottomright',legend=c(expression('Th/PP'~'='~a[0]~'*'~'PP'^b~'/'~(a[1]~'+'~'PP'^b)), paste('RMSE = ',round(RMSE(fit4),4)),
                                paste('a0 = ',round(summary(fit4)$coef[1,1],3)),
                                paste('a1 = ',round(summary(fit4)$coef[2,1],1)),
                                paste('b = ', round(summary(fit4)$coef[3,1],3))),
                  pch=rep(NA,5),cex=1.2,bty='n')

# on cherche le point d'inflexion

derFun <- function(prep,a0,a1,b){
Out <- ( a0*b*prep^(b-1)*(a1+prep^b) - a0*prep^b*b*prep^(b-1)) / (a1+prep^b)^2
}
Yinf <- max( derFun(PP4,summary(fit4)$coef[1,1],summary(fit4)$coef[2,1],summary(fit4)$coef[3,1]))
sce  <-  function(PPI, Y1)  {
Y2 <- derFun(PPI,summary(fit4)$coef[1,1],summary(fit4)$coef[2,1],summary(fit4)$coef[3,1])
return(sum((Y2  -  Y1)^2))
}
ResEq <- optim(par  = 1,fn  =  sce, Y1 = Yinf,
               method="L-BFGS-B")
Xinf <- ResEq$par

par(mar=c(4.5,4.5,1,1))
plot(PP4,derFun(PP4,summary(fit4)$coef[1,1],summary(fit4)$coef[2,1],summary(fit4)$coef[3,1]),type='l',lwd=2,ylim=c(0,0.3),
          ylab='d Ec / d PP', xlab=' Precipitation (mm)',cex.lab=1.2)
abline(h=Yinf,col=2)
text(labels=paste('Ymax = ',round(Yinf,4)), x=80,y=Yinf,pos=1,cex=1.2)
abline(v=Xinf,col=4)
text(labels=paste('Xmax = ',round(Xinf,4)), x=20,y=0.02,pos=4,cex=1.2)


ThinfI <-c() ; ThsupP <-c()
PPinfI <- c() ; PPsupP <- c()
for (i in 1: length(ThdivPP)){
if(PP[i] < Xinf){
ThinfI <- c(ThinfI, Th2[i])
PPinfI <- c(PPinfI, PP[i])}
else {
ThsupP <- c(ThsupP, Th2[i])
PPsupP <- c(PPsupP, PP[i])}
}


par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
plot(PP,Th2,xlab='Precipitation (mm)',ylab='Throughfall (mm)',cex.lab=1.2)
points(PPinfI,ThinfI,col=2,pch=1)
ft1 <- lm(ThinfI~PPinfI-1)
abline(ft1,col=2)
# y = (yb-ya)/(xb-xa) * x + ya -(yb - ya)/(xb-xa)*xa
xa = summary(fit2)$coef[2,1]
ya = xa *summary(ft1)$coef[1] 
PPS <- max(ya - xa,na.rm=T)
abline(coef=c(PPS,1),col=4)
legend('toplef',legend=c(paste('p = ',round(summary(ft1)$coef[1],5)),
                        paste ('S = ',round(-PPS,5),' mm')), 
                        bty='n',pch=c(NA,NA), cex=1.2)










