 Importance of deep water uptake in tropical eucalypt forest. Christina Mathias, Nouvellon Yann, Laclau Jean-Paul, Stape Jose Luiz, Bouillet Jean-Pierre, Lambais George Rodrigues, Le Maire Guerric. 2017. Functional Ecology, 31 (2) : 509-519.
https://doi.org/10.1111/1365-2435.12727
