R-script of peer-reviewed published articles
=============================

*Christina Mathias, UPR AIDA, Cirad*

*Contact: mathias.christina@cirad.fr*

*Actualized November 2024*

Licence : <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale 4.0 International</a>.


___

# Description
This project gather the R script used in peer-reviewed articles published by Christina Mathias.

References of the articles:

+  Christina Mathias, Heuclin Benjamin, Pilloni Raphael, Mellin Mathilde, Barau Laurent, Hoarau Jean-Yves, Dumont Thomas. 2024. Climate, altitude, yield, and varieties drive lodging in sugarcane: A random forest approach to predict risk levels on a tropical island. European Journal of Agronomy, 161:127381. 
https://doi.org/10.1016/j.eja.2024.127381

+  Effect of crop management and climatic factors on weed control in sugarcane intercropping systems. Soule Mathilde, Mansuy Alizé, Chetty Julien, Auzoux Sandrine, Viaud Pauline, Schwartz Marion, Ripoche Aude, Heuclin Benjamin, Christina Mathias. 2024. Field Crops Research, 306:109234.
https://doi.org/10.1016/j.fcr.2023.109234

+  Modeled impact of climate change on sugarcane yield in Réunion, a tropical island. Christina Mathias, Mézino Mickaël, Le Mézo Lionel, Todoroff Pierre. 2024. Sugar Tech, 26 : 639-646. https://doi.org/10.1007/s12355-024-01372-6

+  Micro-mechanization to manage weed in sugar cane. Chetty Julien, Mansuy Alizé, Christina Mathias. 2024. Sugar Tech, 9 p. https://doi.org/10.1007/s12355-024-01405-0

+  Intercropping and weed cover reduce sugarcane roots colonization in plant crops as a result of spatial root distribution and the co-occurrence of neighboring plant species. Christina Mathias, Chevalier Léa, Viaud Pauline, Schwartz Marion, Chetty Julien, Ripoche Aude, Versini Antoine, Jourdan Christophe, Auzoux Sandrine, Mansuy Alizé. 2023. Plant and Soil, 17 p.
https://doi.org/10.1007/s11104-023-06221-1

+  Weed control under increasing cover crop diversity in tropical summer and winter. Negrier Adrien, Marnotte Pascal, Hoareau Julie Candice, Viaud Pauline, Auzoux Sandrine, Técher Patrick, Schwartz Marion, Ripoche Aude, Christina Mathias. 2023. Biotechnologie, Agronomie, Société et Environnement, 27 (2) : 61-73. https://doi.org/10.25518/1780-4507.20220

+  Sugarcane yield response to legume intercropped: A meta-analysis. Viaud Pauline, Heuclin Benjamin, Letourmy Philippe, Christina Mathias, Versini Antoine, Mansuy Alizé, Chetty Julien, Naudin Krishna. 2023. Field Crops Research, 295:108882, 10 p. https://doi.org/10.1016/j.fcr.2023.108882

+  Native and invasive seedling drought-resistance under elevated temperature in common gorse populations. Christina Mathias, Gire Céline, Bakker Mark, Leckie Alan, Xue Jianming, Clinton Peter W., Negrin-Perez Zaira, Arevalo Sierra Jose Ramon, Domec Jean-Christophe, Gonzalez Maya. 2023. Journal of Plant Ecology, 16 (3):rtac097, 16 p. https://doi.org/10.1093/jpe/rtac097

+ Impact of climate variability and extreme rainfall events on sugarcane yield gap in a tropical Island. Christina Mathias, Jones M.R., Versini Antoine, Mézino Mickaël, Le Mezo Lionel, Auzoux Sandrine, Soulie Jean-Christophe, Poser Christophe, Gérardeaux Edward. 2021. Field Crops Research, 274:108326, 11 p. https://doi.org/10.1016/j.fcr.2021.108326

+ A trait-based analysis to assess the ability of cover crops to control weeds in a tropical island. Christina Mathias, Negrier Adrien, Marnotte Pascal, Viaud Pauline, Mansuy Alizé, Auzoux Sandrine, Técher Patrick, Hoarau Emmanuel, Chabanne André. 2021. European Journal of Agronomy, 128:126316, 10 p. https://doi.org/10.1016/j.eja.2021.126316

+ Optimising non-destructive sampling methods to study nitrogen use efficiency throughout the growth-cycle of giant C4 crops. Poultney Daniel, Christina Mathias, Versini Antoine. 2020. Plant and Soil, 613 : 453-597. https://doi.org/10.1007/s11104-020-04611-3

+ ECOFI: A database of sugar and energy cane field trials. Christina Mathias, Chaput Maxime, Martiné Jean-François, Auzoux Sandrine. 2020. Open Data Journal for Agricultural Research, 6 : 14-18. https://doi.org/10.18174/odjar.v6i0.16322

+ Climatic niche shift of an invasive shrub (Ulex europaeus): A global scale comparison in native and introduced regions. Christina Mathias, Limbada Fawziah, Atlan Anne. 2019. Journal of Plant Ecology, 13 (1) : 42-50. https://doi.org/10.1093/jpe/rtz041

+ Simulating the effects of different potassium and water supply regimes on soil water content and water table depth over a rotation of a tropical Eucalyptus grandis plantation. Christina Mathias, Le Maire Guerric, Nouvellon Yann, Vezy Rémi, Bordon B., Battie Laclau Patricia, Gonçalves José Leonardo M., Delgado-Rojas Juan Sinforiano, Bouillet Jean-Pierre, Laclau Jean-Paul. 2018. Forest Ecology and Management, 418 : 4-14. https://doi.org/10.1016/j.foreco.2017.12.048

+ Importance of deep water uptake in tropical eucalypt forest. Christina Mathias, Nouvellon Yann, Laclau Jean-Paul, Stape Jose Luiz, Bouillet Jean-Pierre, Lambais George Rodrigues, Le Maire Guerric. 2017. Functional Ecology, 31 (2) : 509-519. https://doi.org/10.1111/1365-2435.12727

+ Allelopathic effect of a native species on a major plant invader in Europe. Christina Mathias, Rouifed Soraya, Puijalon Sara, Vallier Félix, Meiffren Guillaume, Bellvert Floriant, Piola Florence. 2015. Naturwissenschaften, 102 (3-4):12, 8 p. https://doi.org/10.1007/s00114-015-1263-x

+ Sensitivity and uncertainty analysis of the carbon and water fluxes at the tree scale in Eucalyptus plantations using a metamodeling approach. Christina Mathias, Nouvellon Yann, Laclau Jean-Paul, Stape Jose Luiz, Campoe Otavio C., Le Maire Guerric. 2016. Canadian Journal of Forest Research, 46 (3) : 297-309. IUFRO World Congress. 24, Salt Lake City, États-Unis, 5 Octobre 2014/11 Octobre 2014. https://doi.org/10.1139/cjfr-2015-0173

+ Measured and modeled interactive effects of potassium deficiency and water deficit on gross primary productivity and light-use efficiency in Eucalyptus grandis plantations. Christina Mathias, Le Maire Guerric, Battie Laclau Patricia, Nouvellon Yann, Bouillet Jean-Pierre, Jourdan Christophe, De Moraes Gonçalves Jose Leonardo, Laclau Jean-Paul. 2015. Global Change Biology, 21 (5) : 2022-2039. https://doi.org/10.1111/gcb.12817
