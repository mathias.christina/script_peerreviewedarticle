###### Maxlike
require(raster)
require(maxlike)
require(rgdal)
require(beanplot)

######################################################################
#################### r?cup?ration donn?es ##### ######################
######################################################################

load("DATA/R_GLOBAL_FIGURE_DATA.RData")

######################################################################
##### FOnction choix des coordonn?es
######################################################################
ChooseCoord <- function(lonmin,lonmax,latmin,latmax,MAT=MATWO, option = 'OBS',THRESHOLD=0.4){
  MATNZ <- MAT
  LATNZ <- replace(MATNZ$LAT2,((MATNZ$LON2 < lonmin)|(MATNZ$LON2>lonmax)) | ((MATNZ$LAT2 > latmax)|(MATNZ$LAT2 < latmin)),NA)
  LONNZ <- replace(MATNZ$LON2,((MATNZ$LON2 < lonmin)|(MATNZ$LON2>lonmax)) | ((MATNZ$LAT2 > latmax)|(MATNZ$LAT2 < latmin)),NA)
  if (option=='OBS'){  
    MATNZ[,2] <- LATNZ
    MATNZ[,1] <- LONNZ
    MATNZ <- na.omit(MATNZ)
    return(MATNZ)}
  else if (option=='SIM'){
    MATNZ[,2] <- LATNZ
    MATNZ[,1] <- LONNZ
    AJCNZ <- replace(MATNZ$AJC,MATNZ$AJC<THRESHOLD,0)
    AJCNZ <- replace(AJCNZ,AJCNZ>=THRESHOLD,1)
    MATNZ[,3] <- AJCNZ
    MATNZ <- na.omit(MATNZ)
    return(MATNZ)}
  else {print('Choose the alternative OBS or SIM')}
}
  

######################################################################
#### R?cup?ration des donn?es locales ####################
######################################################################
MATF <- ChooseCoord(-7,3,44,50,MATWO,'OBS')
MATE <- ChooseCoord(-10,1,39,44,MATWO,'OBS')
MATE <- ChooseCoord(-10,1,40,44,MATWO,'OBS')
MATC <- ChooseCoord(-20,-15,27,30,MATWO,'OBS')
MATR <- ChooseCoord(55,56,-22,-20,MATWO,'OBS')
MATNZ <- ChooseCoord(160,180,-50,-28,MATWO,'OBS')
#MATNZ <- ChooseCoord(110,180,-50,-40,MATWO,'OBS')
MATTOT <-rbind(MATF,MATE,MATC,MATR,MATNZ)
MATLOCAL <- list(MATTOT,MATF,MATE,MATC,MATR,MATNZ)

MATF <- ChooseCoord(-7,3,44,50,MATUL,'OBS') ; MATF <- data.frame(rep('1_France',dim(MATF)[1]),MATF);names(MATF)[1] <- 'Pays'
MATE <- ChooseCoord(-10,1,40,44,MATUL,'OBS'); MATE <- data.frame(rep('2_Spain',dim(MATE)[1]),MATE);names(MATE)[1] <- 'Pays'
MATC <- ChooseCoord(-20,-15,27,30,MATUL,'OBS'); MATC <- data.frame(rep('3_Canary',dim(MATC)[1]),MATC);names(MATC)[1] <- 'Pays'
MATR <- ChooseCoord(55,56,-22,-20,MATUL,'OBS'); MATR <- data.frame(rep('4_Reunion',dim(MATR)[1]),MATR);names(MATR)[1] <- 'Pays'
MATNZ <- ChooseCoord(160,180,-50,-28,MATUL,'OBS'); MATNZ <- data.frame(rep('6_New Zealand',dim(MATNZ)[1]),MATNZ);names(MATNZ)[1] <- 'Pays'
MATTOT <-rbind(MATF,MATE,MATC,MATR,MATNZ)
MATTOT2 <- MATTOT ; MATTOT2[,1] <- rep('0_All countries',dim(MATTOT)[1])
MATTOT3 <- rbind(MATTOT2,MATTOT)
  
boxplot(BIO5~Pays,data=MATTOT3)
abline(h=c(300,240))
boxplot(BIO9~Pays,data=MATTOT3)
abline(h=c(230,170))

x11(width = 7.2,height = 10)


par(mfrow=c(3,1))
par(mar = c(0, 0, 0, 0.5), oma = c(1.8, 3.8, 0.5, 0.5))
par(xaxs='i',yaxs='i')
beanplot(BIO5/10~Pays,data=MATTOT3,axes=F,ylim=c(15,35),ll = 0.04,
         col = c("#CAB2D6", "#33A02C", "#B2DF8A"), border = "#CAB2D6")
axis(side=2,at=c(20,25,30),labels=c(20,25,30),tck=0.01,padj=1);box()
mtext(expression(T['MAX']~'of warmest month (°C)'),side=2,line=1.7,cex=1)
abline(v=c(1.5,3.5))
abline(h=c(30,24),col=2,lty=2,lwd=2)
text(x=1.5,y=34,labels='Native',cex=1.2,pos=4,font=3)
text(x=3.5,y=34,labels='Invasive',cex=1.2,pos=4,font=3)
beanplot(BIO9/10~Pays,data=MATTOT3,axes=F,ylim=c(0,25),ll = 0.04,
         col = c("#CAB2D6", "#33A02C", "#B2DF8A"), border = "#CAB2D6")
axis(side=2,at=c(0:4)*5,labels=c(0:4)*5,tck=0.01,padj=1);box()
mtext(expression(T['MEAN']~'of driest quarter (°C)'),side=2,line=1.7,cex=1)
abline(v=c(1.5,3.5))
abline(h=c(23,17),col=2,lty=2,lwd=2)
beanplot(BIO1/10~Pays,data=MATTOT3,axes=F,ylim=c(5,22),ll = 0.04,
         col = c("#CAB2D6", "#33A02C", "#B2DF8A"), border = "#CAB2D6")
axis(side=2,at=c(0:4)*5,labels=c(0:4)*5,tck=0.01,padj=1);box()
mtext(expression(Annual~T['MEAN']~'(°C)'),side=2,line=1.7,cex=1)
axis(side=1,at=1:6,labels=c('All countries','France','Spain','Canary','Reunion','New Zealand'),tck=0.01,cex=1.2,padj=-0.8,cex.axis=1.2)
abline(v=c(1.5,3.5))

