library(dplyr)
library(ggplot2)
library(ggpubr)
library(nlme)
library(emmeans)
library(readxl)
library(piecewiseSEM)
library(Rmisc)
library(multcomp)
library(car)

M1 <- read.table("DATA/RUE_Delicas.txt",sep=';',header=T)
M1$CYCLE <- NA
M2 <- read.table("DATA/RUE_ICSM.txt",sep=';',header=T)
M2$CYCLE <- NA
M3 <- read.table("DATA/RUE_HENRIQUE.txt",sep=';',header=T)
M3$EXPCODE <- paste0(M3$Site_ID)
M3$CYCLE <- NA
M3$Site_ID <- NULL; M3$Exp_ID <- NULL
head(M3)

M4 <- read_xlsx("DATA/Rafael/Dados tese.xlsx",sheet = 3)
M4 <- M4[,c('EXPCODE','CYCLE','RUEmax','RUEa','Var')]
M5 <- read.table("DATA/RUE_ARGESAEZ2.txt",sep=';',header=T)
M5$Replicate <- NULL
M5 <- M5 %>%
  mutate(EXPCODE = case_when(
    CYCLE == "P0" ~ "ARG-SAEZ0",
    CYCLE == "R1" ~ "ARG-SAEZ1",
    CYCLE == "R2" ~ "ARG-SAEZ2"
  ))
M5$Tmeanmax <- NULL

DATA <- bind_rows(M1,M2,M3,M4,M5)
head(DATA)
#DATA <- DATA %>% filter(!(EXPCODE %in% "DEL2"))
DATA <- DATA %>% filter(!is.na(EXPCODE))
DATA <- DATA %>% filter(!is.na(RUEa))

DATA <- DATA %>%
  mutate(CYCLE = case_when(
    EXPCODE == "DEL1" ~ "P0",
    EXPCODE == "DEL2" ~ "P0",
    EXPCODE == "ICRIR0F1" ~ "P0",
    EXPCODE == "ICRIR1F2" ~ "R1",
    EXPCODE == "ICSAR0F1" ~ "P0",
    EXPCODE == "ICSAR1F2" ~ "R1",
    EXPCODE == "ICUSR1F2" ~ "R1",
    EXPCODE == "ICZMR0F1" ~ "P0",
    EXPCODE == "MGSR" ~ "P0",
    EXPCODE == "PIGL" ~ "P0",
    TRUE ~ CYCLE
  ))
DATA <- DATA %>%
  mutate(EXPCODE = case_when(
    EXPCODE == "DEL1" ~ "RUN-DEL1",
    EXPCODE == "DEL2" ~ "RUN-DEL2",
    EXPCODE == "ICRIR0F1" ~ "RUN-ICSM0",
    EXPCODE == "ICRIR1F2" ~ "RUN-ICSM1",
    EXPCODE == "ICSAR0F1" ~ "SAF-ICSM0",
    EXPCODE == "ICSAR1F2" ~ "SAF-ICSM1",
    EXPCODE == "ICUSR1F2" ~ "USA-ICSM1",
    EXPCODE == "ICZMR0F1" ~ "ZIM-ICSM0",
    EXPCODE == "MGSR" ~ "BRA-MGSR0",
    EXPCODE == "PIGL" ~ "BRA-PIGL0",
    EXPCODE == "BRA-CRU0" ~ "BRA-CRUZ0",
    EXPCODE == "BRA-CRU1" ~ "BRA-CRUZ1",
    TRUE ~ EXPCODE
  ))
DATA <- DATA %>%
  mutate(CYCLE = case_when(
    CYCLE == "R1" ~ "R",
    CYCLE == "R2" ~ "R",
    TRUE ~ CYCLE
  ))

unique(DATA$EXPCODE)
unique(DATA$CYCLE)
head(DATA)
DATA <-DATA %>% filter(!(EXPCODE %in% "RUN-DEL2"))
#DATA <-DATA %>% filter(!(EXPCODE %in% "BRA-MGSR0"))
#DATA <-DATA %>% filter(!(RUEmax > 6))
hist(DATA$RUEmax)


#DATA <-DATA %>% filter(Var!="SP801842")
DATA <-DATA %>% filter(Var!="Vertix 2")
DATA <-DATA %>% filter(Var!="Vertix 3")
#### interaction GxE ------
TAB <- table(DATA$Var,DATA$EXPCODE)
TAB <- replace(TAB, TAB>0,1)
X<-rowSums(TAB)
VARK <- names(X)[which(X>1)]
DATAGE <- DATA %>% filter(Var %in% VARK)
  
fitIx <- lm(RUEmax ~ CYCLE*Var+EXPCODE*Var,data=DATAGE)
summary(px <- powerTransform(fitIx))
DATAGE$RUEmaxnew <- bcPower(DATAGE$RUEmax,coef(px))
fitIx <- lm(RUEmaxnew ~ CYCLE*Var+EXPCODE*Var,data=DATAGE)
anova(fitIx)

fitIa <- lm(RUEa ~CYCLE*Var + EXPCODE*Var,data=DATAGE)
summary(pa <- powerTransform(fitIa))
DATAGE$RUEanew <- log(DATAGE$RUEa)
fitIa <- lm(RUEanew ~ CYCLE*Var+EXPCODE*Var,data=DATAGE)
anova(fitIa)

# fitIx2 <- lm(RUEmaxnew ~ CYCLE+EXPCODE+Var,data=DATAGE)
# AIC(fitIx,fitIx2)
# BIC(fitIx,fitIx2)
# fitIa2 <- lm(RUEanew ~ CYCLE+EXPCODE+Var,data=DATAGE)
# AIC(fitIa,fitIa2)
# BIC(fitIa,fitIa2)

ggplot(DATA %>% filter(Var %in% VARK),
       aes(x=EXPCODE,y=RUEmax))+geom_boxplot()+facet_grid(.~Var)+
  theme_classic()

# mixt model-----------
fitx <- lm(RUEmax ~ CYCLE+Var,data=DATA)
fita <- lm(RUEa ~CYCLE+Var,data=DATA)
summary(px <- powerTransform(fitx))
summary(pa <- powerTransform(fita))
DATA$RUEmaxnew <- bcPower(DATA$RUEmax,coef(px))
DATA$RUEanew <- bcPower(DATA$RUEa,coef(pa))
fitx <- lme(RUEmaxnew ~ CYCLE+Var, random=~1|EXPCODE,data=DATA)
fita <- lme(RUEanew ~CYCLE+Var, random=~1|EXPCODE,data=DATA)
ggqqplot(residuals(fita))
ggqqplot(residuals(fitx))
anova(fita)
anova(fitx)

par(mar=c(7,1,1,1))
boxplot(residuals(fitx)~DATA$EXPCODE,las=2)
abline(h=0)
## figure ---
P0 <- ggplot(DATA, aes(RUEa,RUEmax,colour=EXPCODE))+geom_point()+
  geom_abline(slope=1,intercept=0)+
  # xlim(range(c(DATA$RUEa,DATA$RUEmax)))+
  # ylim(range(c(DATA$RUEa,DATA$RUEmax)))+
  labs(x=expression(RUE[A]~(gC~MJ^-1)),y=expression(RUE[MAX]~(gC~MJ^-1)))+theme_classic()+
  theme(legend.title = element_blank())+coord_fixed()#xlim(0.7,5)+ylim(0.7,5)
P0


EAC <- emmeans(fita, list(pairwise ~ CYCLE), adjust = "tukey")
MMM <- summarySE(DATA,"RUEa",c("CYCLE"),na.rm=T)
CI <- confint(EAC[[1]])
PREDVAL <- (CI$emmean*coef(pa)+1)^(1/coef(pa))
MINVAL <- (CI$lower.CL*coef(pa)+1)^(1/coef(pa))
MAXVAL <- (CI$upper.CL*coef(pa)+1)^(1/coef(pa))
NVAL <- sapply(1:2,function(i) paste0('n = ',MMM$N[i]))
#LET <- c('a','a','b')

PCA <- ggplot(DATA) +
  aes(x = CYCLE, y = RUEa, colour = CYCLE, shape = CYCLE) +
  geom_point(position = position_jitter(width = 0.1,height=0), alpha= 0.3, fill = "#112446", size=1) +
#  scale_color_manual(values = list(TE = "#FC4E07",PDS = "#339966",TP="#003399")) +
  scale_x_discrete(labels=c("Plant", "Ratoon"))+
#  scale_shape_manual(values = c(17, 16,15),labels=c("Low crontrol","Cover crop", "Chemical"))+
  labs(x = "Crop class", y = expression(RUE[A]~(gDM~MJ^-1))) +
  annotate("point", x= 1:2, y=PREDVAL, size =3)+ # 32.7
  annotate ("segment", x=1:2, xend=1:2, y=MINVAL, yend = MAXVAL, size=1)+ #32.7-14.9
#  annotate("text",x=1:2,y=c(2,2)*1.02,label=c('a','b'),fontface=2)+
  theme_classic()+
  theme(axis.title.y = element_text(size = 12),axis.text.y=element_text(size = 10,angle=90,hjust=0.5))+
  theme(axis.text.x = element_text(size= 11),axis.title.x=element_text(size = 12))+
  theme(legend.position = "none")

PCA

library(Rmisc)
EAC <- emmeans(fitx, list(pairwise ~ CYCLE), adjust = "tukey")
MMM <- summarySE(DATA,"RUEmax",c("CYCLE"),na.rm=T)
CI <- confint(EAC[[1]])
PREDVAL <- (CI$emmean*coef(px)+1)^(1/coef(px))
MINVAL <- (CI$lower.CL*coef(px)+1)^(1/coef(px))
MAXVAL <- (CI$upper.CL*coef(px)+1)^(1/coef(px))
NVAL <- sapply(1:2,function(i) paste0('n = ',MMM$N[i]))
#LET <- c('a','a','b')

PCX <- ggplot(DATA) +
  aes(x = CYCLE, y = RUEmax, colour = CYCLE, shape = CYCLE) +
  geom_point(position = position_jitter(width = 0.1,height=0), alpha= 0.3, fill = "#112446", size=1) +
  #  scale_color_manual(values = list(TE = "#FC4E07",PDS = "#339966",TP="#003399")) +
  scale_x_discrete(labels=c("Plant", "Ratoon"))+
  #  scale_shape_manual(values = c(17, 16,15),labels=c("Low crontrol","Cover crop", "Chemical"))+
  labs(x = "Crop class", y = expression(RUE[MAX]~(gDM~MJ^-1))) +
  annotate("point", x= 1:2, y=PREDVAL, size =3)+ # 32.7
  annotate ("segment", x=1:2, xend=1:2, y=MINVAL, yend = MAXVAL, size=1)+ #32.7-14.9
  theme_classic()+
  theme(axis.title.y = element_text(size = 12),axis.text.y=element_text(size = 10,angle=90,hjust=0.5))+
  theme(axis.text.x = element_text(size= 11),axis.title.x=element_text(size = 12))+
  theme(legend.position = "none")

PCX

EAC <- emmeans(fita, list(pairwise ~ Var), adjust = "tukey")
MMM <- summarySE(DATA,"RUEa",c("Var"),na.rm=T)
CI <- confint(EAC[[1]])
PREDVAL <- (CI$emmean*coef(pa)+1)^(1/coef(pa))
MINVAL <- (CI$lower.CL*coef(pa)+1)^(1/coef(pa))
MAXVAL <- (CI$upper.CL*coef(pa)+1)^(1/coef(pa))
NVAL <- sapply(1:2,function(i) paste0('n = ',MMM$N[i]))
LET <- multcomp::cld(EAC)

PVA <- ggplot(DATA) +
  aes(x = Var, y = RUEa) +
  geom_point(position = position_jitter(width = 0.1,height=0), alpha= 0.3, 
             fill = "#112446", size=1,shape=1) +
  labs(x = "", y = expression(RUE[A]~(gDM~MJ^-1))) +
  annotate("point", x= 1:length(PREDVAL), y=PREDVAL, size =2)+ # 32.7
  annotate ("segment", x=1:length(PREDVAL), xend=1:length(PREDVAL), 
            y=MINVAL, yend = MAXVAL, size=0.75)+ #32.7-14.9
  theme_classic()+
  theme(axis.title.y = element_text(size = 12),axis.text.y=element_text(size = 10,angle=90,hjust=0.5))+
  theme(axis.text.x = element_text(size= 11,angle=90,hjust=1,vjust=0.5),axis.title.x=element_text(size = 15))+
  theme(legend.position = "none")

PVA

EAC <- emmeans(fitx, list(pairwise ~ Var), adjust = "tukey")
MMM <- summarySE(DATA,"RUEmax",c("Var"),na.rm=T)
CI <- confint(EAC[[1]])
PREDVAL <- (CI$emmean*coef(px)+1)^(1/coef(px))
MINVAL <- (CI$lower.CL*coef(px)+1)^(1/coef(px))
MAXVAL <- (CI$upper.CL*coef(px)+1)^(1/coef(px))
NVAL <- sapply(1:2,function(i) paste0('n = ',MMM$N[i]))
LET <- multcomp::cld(EAC)
test <- as.data.frame(EAC$`pairwise differences of Var`)
test[grep('F160',test$`1`),]

test <- emmeans(fitx, list(pairwise ~ CYCLE), adjust = "tukey")
testCI <- confint(test[[1]])
testVAL <- (testCI$emmean*coef(px)+1)^(1/coef(px))
MAXtest <- (testCI$upper.CL*coef(px)+1)^(1/coef(px))
# 2.5214 / 0.45 = 5.603
# 3.0 / 0.5 = 5.603

MAXDATA <- DATA %>% dplyr::group_by(Var) %>% dplyr::summarise(RUEmax=max(RUEmax))
MAXDATA$Pred <- PREDVAL
MAXDATA$PredMax <- MAXVAL
MAXDATA %>% dplyr::filter(Var %in% c('R579','R570','LCP85-384','NCO376','RB867515')) %>% mutate(RUEpar=RUEmax/0.45,
                                                                                     RUEparpred=Pred/0.45,
                                                                                     RUEparpredmax=PredMax/0.45)
MAXDATA %>% dplyr::filter(Var %in% c('R579','R570','LCP85-384','NCO376','RB867515')) %>% mutate(RUEpar=RUEmax,
                                                                                     RUEparpred=Pred,
                                                                                     RUEparpredmax=PredMax)


PVX <- ggplot(DATA) +
  aes(x = Var, y = RUEmax) +
  geom_point(position = position_jitter(width = 0.1,height=0), 
             alpha= 0.3, fill = "#112446", size=1,shape=1) +
  labs(x = "Variety", y = expression(RUE[MAX]~(gDM~MJ^-1))) +
  annotate("point", x= 1:length(PREDVAL), y=PREDVAL, size =2)+ # 32.7
  annotate ("segment", x=1:length(PREDVAL), xend=1:length(PREDVAL), 
            y=MINVAL, yend = MAXVAL, size=0.75)+ #32.7-14.9
  theme_classic()+
  theme(axis.title.y = element_text(size = 12),axis.text.y=element_text(size = 10,angle=90,hjust=0.5))+
  theme(axis.text.x = element_text(size= 11,angle=90,hjust=1,vjust=0.5),axis.title.x=element_text(size = 12))+
  theme(legend.position = "none")

PVX

library(cowplot)

THEMESIZE <- theme(
  #  plot.margin = unit(c(1, 1, 1, 1), "cm"),
  plot.title = element_text(size = 6),   # Titre du graphique
  axis.title.x = element_text(size = 8),  # Titre de l'axe X
  axis.title.y = element_text(size = 8),  # Titre de l'axe Y
  axis.text.x = element_text(size = 6),   # Texte des graduations de l'axe X
  axis.text.y = element_text(size = 6),   # Texte des graduations de l'axe Y
  strip.text.x = element_text(size = 7),
  legend.margin=margin(0,0,0,0))

THEME2 <-   theme(legend.spacing.y  = unit(0.01, "cm"),
                  legend.text = element_text(size = 6,
                                             margin = margin(t=0,r=0,b=0,l=0, unit = "pt")),
                  legend.spacing.x = unit(0.02, "cm"),
                  legend.key.height=unit(0.3, "cm"))

P0 <- ggplot(DATA, aes(RUEa,RUEmax,colour=EXPCODE))+geom_point(size=1)+
  geom_abline(slope=1,intercept=0)+
  # xlim(range(c(DATA$RUEa,DATA$RUEmax)))+
  # ylim(range(c(DATA$RUEa,DATA$RUEmax)))+
  labs(x=expression(RUE[A]~(gDM~MJ^-1)),y=expression(RUE[MAX]~(gDM~MJ^-1)))+
  theme_classic()+
  theme(legend.title = element_blank())+coord_fixed()+
  guides(colour = guide_legend(byrow = T))+
  THEME2
P0

x11(width=190*0.0393701,height = 220*0.0393701)

plot_grid(plot_grid(P0+THEMESIZE+THEME2,
                    PCA+THEMESIZE,PCX+THEMESIZE,ncol=3,labels = c("(a)","(b)","(c)"),label_size = 9,
                    rel_widths = c(1.5,1,1)),
          PVA+THEMESIZE,PVX+THEMESIZE,nrow=3, 
          labels = c("","(d)","(e)"), label_size = 9, 
          label_colour = "black")#,align = 'hv')

# plot_grid(P0+THEMESIZE,
#           plot_grid(PCA+THEMESIZE,PCX+THEMESIZE,ncol=2,labels = c("(b)","(c)"),label_size = 9),
#           PVA+THEMESIZE,PVX+THEMESIZE,nrow=4, 
#           labels = c("(a)","","(d)","(e)"), label_size = 9, 
#           label_colour = "black")#,align = 'hv')

# plot_grid(P0,
#           plot_grid(PCA,PCX,ncol=2,labels = c("(b)","(c)"),label_size = 9),
#           PVA,PVX,nrow=4, 
#           labels = c("(a)","","(d)","(e)"), label_size = 9, 
#           label_colour = "black")#,align = 'hv')
# 
# plot_grid(P0,
#           plot_grid(PCA,PCX,ncol=2,labels = c("(b)","(c)"),label_size = 12),
#           nrow=2, 
#           labels = c("(a)",""), label_size = 12, 
#           label_colour = "black")#,align = 'hv')
# 
# plot_grid(PVA,PVX,
#           nrow=2, 
#           labels = c("(d)","(e)"), label_size = 12, 
#           label_colour = "black")#,align = 'hv')

# normal model ------
# fita <- lm(log(RUEa) ~ EXPCODE+CYCLE + Var,data=DATA)
# fitx <- lm(log(RUEmax) ~EXPCODE + CYCLE + Var,data=DATA)
# aa <- anova(fita)
# ax <- anova(fitx)
# ggqqplot(residuals(fitx))
# ggqqplot(residuals(fita))
# aa$`Sum Sq`/sum(aa$`Sum Sq`)*100
# ax$`Sum Sq`/sum(ax$`Sum Sq`)*100
# fita <- lm(log(RUEa) ~ EXPCODE + Var,data=DATA)
# emmeans(fita, ~Var)

P0 <- ggplot(DATA, aes(RUEa,RUEmax,colour=EXPCODE))+geom_point()+
  geom_abline(slope=1,intercept=0)+
  xlim(range(c(DATA$RUEa,DATA$RUEmax)))+
  ylim(range(c(DATA$RUEa,DATA$RUEmax)))+
  labs(x='Apparent RUE (gC/MJ)',y="Maximum RUE (gC/MJ)")+theme_classic()

emmeans(fita, pairwise~CYCLE)

##-- variance per -------------
VAREXP <- data.frame(EXPCODE=unique(DATA$EXPCODE))
VAREXP$RUEmax <- NA
VAREXP$RUEa <- NA
VAREXP$pRUEmax <- NA
VAREXP$pRUEa <- NA
for (i in 1:dim(VAREXP)[1]){
  fita <- lm(RUEa ~ Var,data=DATA %>% filter(EXPCODE==VAREXP$EXPCODE[i]))
  fitx <- lm(RUEmax ~ Var,data=DATA%>% filter(EXPCODE==VAREXP$EXPCODE[i]))
  aa <- anova(fita)
  ax <- anova(fitx)
  VAREXP$RUEa[i] <- (aa$`Sum Sq`/sum(aa$`Sum Sq`))[1]*100
  VAREXP$RUEmax[i] <- (ax$`Sum Sq`/sum(ax$`Sum Sq`))[1]*100
  VAREXP$pRUEa[i] <- aa$`Pr(>F)`[1]
  VAREXP$pRUEmax[i] <- ax$`Pr(>F)`[1]
}
VAREXP <- VAREXP %>%
  mutate(pRUEa = case_when(
    pRUEa <= 0.05 ~ "p<0.05",
    pRUEa > 0.05  ~ "p>0.05"))
VAREXP <- VAREXP %>%
  mutate(pRUEmax = case_when(
    pRUEmax <= 0.05 ~ "p<0.05",
    pRUEmax > 0.05  ~ "p>0.05"))


P1 <- ggplot(VAREXP, aes(RUEa,RUEmax,colour=EXPCODE,shape=pRUEmax))+
  geom_point(size=4)+geom_abline(slope=1,intercept=0)+
  labs(x="RUEa variance explained by variety (%)",
       y="RUEmax variance explained by variety (%)")+
  xlim(range(c(VAREXP$RUEa,VAREXP$RUEmax)))+
  ylim(range(c(VAREXP$RUEa,VAREXP$RUEmax)))+
  theme_classic()

