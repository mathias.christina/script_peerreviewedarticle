 Impact of climate variability and extreme rainfall events on sugarcane yield gap in a tropical Island. Christina Mathias, Jones M.R., Versini Antoine, Mézino Mickaël, Le Mezo Lionel, Auzoux Sandrine, Soulie Jean-Christophe, Poser Christophe, Gérardeaux Edward. 2021. Field Crops Research, 274:108326, 11 p.
https://doi.org/10.1016/j.fcr.2021.108326
