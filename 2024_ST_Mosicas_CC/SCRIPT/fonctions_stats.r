add.errorbars <- function(x,y,SE,direction,barlen=0.04,color=1){

    if (length(color)==1) {COL = rep(color,length(x))}
    if (length(color)>1) {COL = color}
    
    for (i in 1:length(x)){
      if(direction=="up")arrows(x0=x[i], x1=x[i], y0=y[i], y1=y[i] + SE[i], code=2,
      angle=90, length=barlen,col=COL[i])
      if(direction=="down")arrows(x0=x[i], x1=x[i], y0=y[i], y1=y[i] - SE[i], code=1,
      angle=90, length=barlen,col=COL[i])
      if(direction=="left")arrows(x0=x[i], x1=x[i]-SE[i], y0=y[i], y1=y[i], code=1,
      angle=90, length=barlen,col=COL[i])
      if(direction=="right")arrows(x0=x[i], x1=x[i]+SE[i], y0=y[i], y1=y[i], code=2,
      angle=90, length=barlen,col=COL[i])
      if(direction=="vert")arrows(x0=x[i], x1=x[i], y0=y[i]-SE[i], y1=y[i] + SE[i], code=3,
      angle=90, length=barlen,col=COL[i])
      if(direction=="hor")arrows(x0=x[i]-SE, x1=x[i]+SE[i], y0=y[i], y1=y[i], code=3,
      angle=90, length=barlen,col=COL[i])
}}

flat_cor_mat <- function(cor_r, cor_p){
  #This function provides a simple formatting of a correlation matrix
  #into a table with 4 columns containing :
  # Column 1 : row names (variable 1 for the correlation test)
  # Column 2 : column names (variable 2 for the correlation test)
  # Column 3 : the correlation coefficients
  # Column 4 : the p-values of the correlations
  library(tidyr)
  library(tibble)
  library(dplyr)
  cor_r <- rownames_to_column(as.data.frame(cor_r), var = "row")
  cor_r <- gather(cor_r, column, cor, -1)
  cor_p <- rownames_to_column(as.data.frame(cor_p), var = "row")
  cor_p <- gather(cor_p, column, p, -1)
  cor_p_matrix <- left_join(cor_r, cor_p, by = c("row", "column"))
  cor_p_matrix
}  
  
Bic <- function(fit,p,n){
A <- AIC(fit) + p*(log10(n)-2)
print(A)}

RMSE <- function(fit){ A <-summary(fit)$sigma^2 ; print(A)}

RMSE_2 <- function(yobs,ytheo,k,n){
R <- sum((yobs  -  ytheo)^2)/(n-k-1)
return(R)}

meanB <- function(Ysim,Yth){ A <- sum(Ysim-Yth,na.rm=2)/sum(Yth,na.rm=T)*100
; print(A)}

r2 <- function(fit) { A <-summary(fit)$adj.r.squared ; print(A)}


AIC2 <- function(n,ytheo,ymeas,k,p){
aic <- n * log ( sum((ytheo - ymeas)^2,na.rm=T) / (n - k - 1) ) + 2 * p
}
BIC2 <- function(n,ytheo,ymeas,k,p){
bic <- AIC2(n,ytheo,ymeas,k,p) + p *(log10(n)-2)
}
MSE2 <- function(n,ytheo,ymeas,k){
mse <- sum((ytheo - ymeas)^2,na.rm=T) / (n - k - 1)
}


  
  # see http://www.r-bloggers.com/predictnls-part-1-monte-carlo-simulation-confidence-intervals-for-nls-models/
  
  predictNLS <- function(
    object, 
    newdata,
    level = 0.95, 
    nsim = 10000,
    ...
  )
  {
    require(MASS, quietly = TRUE)
    
    ## get right-hand side of formula
    RHS <- as.list(object$call$formula)[[3]]
    EXPR <- as.expression(RHS)
    
    ## all variables in model
    VARS <- all.vars(EXPR)
    
    ## coefficients
    COEF <- coef(object)
    
    ## extract predictor variable    
    predNAME <- setdiff(VARS, names(COEF))  
    
    ## take fitted values, if 'newdata' is missing
    if (missing(newdata)) {
      newdata <- eval(object$data)[predNAME]
      colnames(newdata) <- predNAME
    }
    
    ## check that 'newdata' has same name as predVAR
    if (names(newdata)[1] != predNAME) stop("newdata should have name '", predNAME, "'!")
    
    ## get parameter coefficients
    COEF <- coef(object)
    
    ## get variance-covariance matrix
    VCOV <- vcov(object)
    
    ## augment variance-covariance matrix for 'mvrnorm' 
    ## by adding a column/row for 'error in x'
    NCOL <- ncol(VCOV)
    ADD1 <- c(rep(0, NCOL))
    ADD1 <- matrix(ADD1, ncol = 1)
    colnames(ADD1) <- predNAME
    VCOV <- cbind(VCOV, ADD1)
    ADD2 <- c(rep(0, NCOL + 1))
    ADD2 <- matrix(ADD2, nrow = 1)
    rownames(ADD2) <- predNAME
    VCOV <- rbind(VCOV, ADD2) 
    
    ## iterate over all entries in 'newdata' as in usual 'predict.' functions
    NR <- nrow(newdata)
    respVEC <- numeric(NR)
    seVEC <- numeric(NR)
    varPLACE <- ncol(VCOV)   
    
    ## define counter function
    counter <- function (i) 
    {
      if (i%%10 == 0) 
        cat(i)
      else cat(".")
      if (i%%50 == 0) 
        cat("\n")
      flush.console()
    }
    
    outMAT <- NULL 
    
    for (i in 1:NR) {
      counter(i)
      
      ## get predictor values and optional errors
      predVAL <- newdata[i, 1]
      if (ncol(newdata) == 2) predERROR <- newdata[i, 2] else predERROR <- 0
      names(predVAL) <- predNAME  
      names(predERROR) <- predNAME  
      
      ## create mean vector for 'mvrnorm'
      MU <- c(COEF, predVAL)
      
      ## create variance-covariance matrix for 'mvrnorm'
      ## by putting error^2 in lower-right position of VCOV
      newVCOV <- VCOV
      newVCOV[varPLACE, varPLACE] <- predERROR^2
      
      ## create MC simulation matrix
      simMAT <- mvrnorm(n = nsim, mu = MU, Sigma = newVCOV, empirical = TRUE)
      
      ## evaluate expression on rows of simMAT
      EVAL <- try(eval(EXPR, envir = as.data.frame(simMAT)), silent = TRUE)
      if (inherits(EVAL, "try-error")) stop("There was an error evaluating the simulations!")
      
      ## collect statistics
      PRED <- data.frame(predVAL)
      colnames(PRED) <- predNAME   
      FITTED <- predict(object, newdata = data.frame(PRED))
      MEAN.sim <- mean(EVAL, na.rm = TRUE)
      SD.sim <- sd(EVAL, na.rm = TRUE)
      MEDIAN.sim <- median(EVAL, na.rm = TRUE)
      MAD.sim <- mad(EVAL, na.rm = TRUE)
      QUANT <- quantile(EVAL, c((1 - level)/2, level + (1 - level)/2))
      SD2 <- sd(EVAL[EVAL>=QUANT[1]&EVAL<=QUANT[2]])
      RES <- c(FITTED, MEAN.sim, SD.sim, MEDIAN.sim, MAD.sim, QUANT[1], QUANT[2],SD2)
      outMAT <- rbind(outMAT, RES)
    }
    
    colnames(outMAT) <- c("fit", "mean", "sd", "median", "mad", names(QUANT[1]), names(QUANT[2]),'sd_95')
    rownames(outMAT) <- NULL
    
    cat("\n")
    
    return(outMAT)  
  }
  

counter <- function (i) 
{
  if (i%%10 == 0) 
    cat(i)
  else cat(".")
  if (i%%50 == 0) 
    cat("\n")
  flush.console()
}
