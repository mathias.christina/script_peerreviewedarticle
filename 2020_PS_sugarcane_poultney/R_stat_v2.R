# dans quel dossier sommmes nous ?
set <- c(dirname(rstudioapi::getActiveDocumentContext()$path)) 
setwd(set)

# donn?es
MAT <- read.table(file = 'Stat.txt',header=T)
head(MAT)
# table de fisher
MATFTHEO <- read.table('table_fisher.txt')

# fit avec toutes les donn?es
fit0 <- nls(N~a*Biomass^b,data=MAT,start=list(a=300,b=-0.6),
            na.action=na.omit)

summary(fit0)

X <- seq(400,4000,length.out=100)
Y <- coef(fit0)[1]*X^coef(fit0)[2]

# calcul RMSE
x0rmse <- MAT$Biomass
y0rmsetheo <- coef(fit0)[1]*x0rmse^coef(fit0)[2]
RMSE0 <- sqrt(sum((y0rmsetheo-MAT$N)^2)/length(x0rmse))

# plot
plot(MAT$Biomass,MAT$N,ylab='N content (mg/g)',col=1,xlim=c(300,4000),ylim=c(0,15))
points(X,Y,lty=1,lwd=2,col=2,type='l')
XM <- as.numeric(levels(as.factor(MAT$Biomass)))
YM <- sapply(1:length(XM), function(i) mean(MAT$N[MAT$Biomass==XM[i]],na.rm=T))
points(XM,YM,pch=16,col=2,cex=2)

# classes de biomasse
IDBIOM <- as.numeric(levels(as.factor(MAT$Biomass)))
# nombre de relation allom?trique que l'on cr??e
REP <- 5000

MATFTHEO <- rep(NA,REP)
MATFOBS <- rep(NA,REP)
MATRMSE <- rep(NA,REP)
NBECH <- c()
NREP <- c()
FIT <- list()

for (j in 1:20){

Fobs <- c()
Ftheo <- c()
RMSE <- c()
NBECH <-c(NBECH,j)

for (i in 1:REP){
  print(c(j,i))
  NREP <-c(NREP,i)
  
# ?chantillonnage de j cannes par classe de biomasse
ID <- c(sample(which(MAT$Biomass==IDBIOM[1]),j,replace=F),
        sample(which(MAT$Biomass==IDBIOM[2]),j,replace=F),
        sample(which(MAT$Biomass==IDBIOM[3]),j,replace=F),
        sample(which(MAT$Biomass==IDBIOM[4]),j,replace=F),
        sample(which(MAT$Biomass==IDBIOM[5]),j,replace=F))

#  N et biomasse ?chantillonn?es
Nnew = MAT$N[ID]
Biomnew = MAT$Biomass[ID]

# nouveau fit
fit1 <- nls(Nnew~a*Biomnew^b,start=list(a=300,b=-1),
            na.action=na.omit)

FIT[[(j-1)*5000+i]] <- coef(fit1)
#X1 <- seq(400,4000,length.out=100)
#Y1 <- coef(fit1)[1]*X^coef(fit1)[2]

#plot(Biomnew,Nnew)
#points(X,Y,lty=1,lwd=2,col=2,type='l')
#points(X1,Y1,lty=1,lwd=2,col=4,type='l')

# fit global = donn?es compl?tes + donn?es ?chantillonn?es
NnewG <- c(MAT$N, Nnew)
BiomnewG <- c(MAT$Biomass, Biomnew)

fit2 <- nls(NnewG~a*BiomnewG^b,start=list(a=5000,b=-1),
            na.action=na.omit)

#X1 <- seq(400,4000,length.out=100)
#Y1 <- coef(fit2)[1]*X^coef(fit2)[2]
#plot(BiomnewG,NnewG)
#points(X,Y,lty=1,lwd=2,col=2,type='l')
#points(X1,Y1,lty=1,lwd=2,col=4,type='l')

#summary(fit2)

# SSE fit de r?f?rence
x0 <- MAT$Biomass
y0Meas <- MAT$N
y0theo <- coef(fit0)[1]*x0^coef(fit0)[2]
SCE0 <- sum((y0theo-y0Meas)^2)

# SSE fit ?chantillonn?
x1 <- Biomnew
y1Meas <- Nnew
y1theo <- coef(fit1)[1]*x1^coef(fit1)[2]
SCE1 <- sum((y1theo-y1Meas)^2)

# SSE fit global
x2 <- BiomnewG
y2Meas <- NnewG
y2theo <- coef(fit2)[1]*x2^coef(fit2)[2]
SCE2 <- sum((y2theo-y2Meas)^2)

# calcul du F observ?
FOBS <- ((SCE2 - (SCE1+SCE0) )/ (length(coef(fit0))+length(coef(fit1)) - length(coef(fit2))) ) / 
  ((SCE1+SCE0)/(length(x1)+length(x2) - length(coef(fit0)) - length(coef(fit1))))

# F th?orique de la table de fisher
if (length(BiomnewG)<150){FTHEO <-  3.087}
if ((length(BiomnewG)>=150)&(length(BiomnewG)<200)){FTHEO <-   3.056}
if (length(BiomnewG)>=200){FTHEO <-   3.041}

Ftheo <- c(Ftheo,FTHEO)
Fobs <- c(Fobs,FOBS)

# RMSE du fit ?chantillon?
xrmse <- MAT$Biomass
yrmsetheo <- coef(fit1)[1]*xrmse^coef(fit1)[2]

RMSE <- c(RMSE,sqrt(sum((yrmsetheo-MAT$N)^2,na.rm=T)/length(xrmse)))


}
MATFTHEO <- data.frame(MATFTHEO,Ftheo)
MATFOBS <- data.frame(MATFOBS,Fobs)
MATRMSE <- data.frame(MATRMSE,RMSE)
}
MATFTHEO <- MATFTHEO[,2:21] ; names(MATFTHEO) <- 1:20
MATFOBS <- MATFOBS[,2:21] ; names(MATFOBS) <- 1:20
# on isole les RMSE qui ont rat?, celles qui ont r?ussies = NA
MATRMSE <- MATRMSE[,2:21] ; names(MATRMSE) <- 1:20
MATRMSE <- replace(MATRMSE,MATFOBS<MATFTHEO,NA)


write.table(MATFTHEO,file = 'MATFTHEO.txt',row.names=F,col.names=T)
write.table(MATFOBS,file = 'MATFOBS.txt',row.names=F,col.names=T)
write.table(MATRMSE,file = 'MATRMSE.txt',row.names=F,col.names=T)


######
load("~/R/R_Divers/Daniel/SAUV.RData")



######
# Identification des relation qui ont rat�
LISTFIT <- list()
PROB <-c()

for (j in 1:5){
  
  IDNB <- c(1,2,5,10,12)  
  ID <- which(MATFOBS[,IDNB[j]]> MATFTHEO[,IDNB[j]])
#  ID <- 1:length(MATFOBS[,IDNB[j]])
  PROB <- c(PROB, length(ID)/5000)
  XVAL <- seq(300,4000, length.out=100)
  YVAL <- c()
  xdiff <-c()
  for (i in 1:length(ID)){
#    Y <- FIT[[(5000)+ID[i]]][1] * XVAL ^FIT[[(5000)+ID[i]]][2]
    Y <- FIT[[(5000*(IDNB[j]-1))+ID[i]]][1] * XVAL ^FIT[[(5000*(IDNB[j]-1))+ID[i]]][2]
    YVAL <- c(YVAL,Y)
  }
  LISTFIT[[j]] <- data.frame(rep(XVAL,length(ID)),YVAL)
}

LISTBORN <- list()
for (j in 1:5){
#  YMAX <- sapply(XVAL, function(i) max(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
#  YMIN <- sapply(XVAL, function(i) min(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  YMAX <- sapply(XVAL, function(i) 1.96*sd(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  YMIN <- sapply(XVAL, function(i) 1.96*sd(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  LISTBORN[[j]] <- data.frame(YMIN,YMAX)
}


# fit avec toutes les donn?es
fit0 <- nls(N~a*Biomass^b,data=MAT,start=list(a=333,b=-0.6),
            na.action=na.omit)
X0 <- seq(300,4000,length.out=100)
Y0 <- coef(fit0)[1]*X0^coef(fit0)[2]

IDNB
XM <- as.numeric(levels(as.factor(MAT$Biomass)))
YM <- sapply(1:length(XM), function(i) mean(MAT$N[MAT$Biomass==XM[i]],na.rm=T))

#x11()

par(mfrow=c(1,1),xaxs='i',yaxs='i',mar=c(4,4,1,4))
plot(MAT$Biomass/1000,MAT$N,ylim=c(0,15),xlim=c(300,4000)/1000,col=0,
     mgp=c(2.2,0.5,0),tck=0.02,
     ylab=expression('Aboveground N content'~~(mg~g^-1)),xlab=expression('Aboveground dry mass'~~(kg~m^-2)),cex.lab=1.2)
points(XM/1000,YM,pch=16,cex=1.5)
points(X0/1000,Y0,type='l',col=2,lwd=2)

points(XVAL/1000,Y0-LISTBORN[[2]]$YMIN,type='l',col=1,lty=3)
points(XVAL/1000,Y0+LISTBORN[[2]]$YMAX,type='l',col=1,lty=3)

points(XVAL/1000,Y0+LISTBORN[[3]]$YMIN,type='l',col=1,lty=2)
points(XVAL/1000,Y0-LISTBORN[[3]]$YMAX,type='l',col=1,lty=2)

points(XVAL/1000,Y0+LISTBORN[[4]]$YMIN,type='l',col=1,lty=1)
points(XVAL/1000,Y0-LISTBORN[[4]]$YMAX,type='l',col=1,lty=1)

legend('topright',legend=c('Reference dilution curve: n/date = 20',
                           expression('CI'['95%']~'n/date = 15,'~P[DIFF]~'='~'0 %'),
                           expression('CI'['95%']~'n/date = 10,'~P[DIFF]~'='~'0.9 %'),
                           expression('CI'['95%']~'n/date = 5,'~P[DIFF]~'='~'3.2 %'),
                           expression('CI'['95%']~'n/date = 2,'~P[DIFF]~'='~'4.9 %'),
                           'Average measured N content'),
       pch=c(NA,NA,NA,NA,NA,16),lty=c(1,NA,1,2,3,NA),col=c(2,1,1,1,1,1),cex=1.2,bty='n')



####### avex 3 dates
MATFTHEO2 <- rep(NA,REP)
MATFOBS2 <- rep(NA,REP)
MATRMSE2 <- rep(NA,REP)
NBECH2 <- c()
NREP2 <- c()
FIT2 <- list()

for (j in 1:20){
  
  Fobs <- c()
  Ftheo <- c()
  RMSE <- c()
  NBECH2 <-c(NBECH2,j)
  
  for (i in 1:REP){
    print(c(j,i))
    NREP2 <-c(NREP2,i)
    
    # ?chantillonnage de j cannes par classe de biomasse
    ID <- c(sample(which(MAT$Biomass==IDBIOM[1]),j,replace=F),
#            sample(which(MAT$Biomass==IDBIOM[2]),j,replace=F),
            sample(which(MAT$Biomass==IDBIOM[3]),j,replace=F),
#            sample(which(MAT$Biomass==IDBIOM[4]),j,replace=F),
            sample(which(MAT$Biomass==IDBIOM[5]),j,replace=F))
    
    #  N et biomasse ?chantillonn?es
    Nnew = MAT$N[ID]
    Biomnew = MAT$Biomass[ID]
    
    # nouveau fit
    fit1 <- nls(Nnew~a*Biomnew^b,start=list(a=300,b=-1),
                na.action=na.omit)
    
    FIT2[[(j-1)*5000+i]] <- coef(fit1)
#    X1 <- seq(300,4000,length.out=100)
#    Y1 <- coef(fit1)[1]*X^coef(fit1)[2]
    
#    plot(Biomnew,Nnew,xlim=c(300,4000),ylim=c(0,14))
#    points(X0,Y0,lty=1,lwd=2,col=2,type='l')
#    points(X1,Y1,lty=1,lwd=2,col=4,type='l')
    
    # fit global = donn?es compl?tes + donn?es ?chantillonn?es
    IDref <- c(which(MAT$Biomass==IDBIOM[1]),
               which(MAT$Biomass==IDBIOM[3]),
               which(MAT$Biomass==IDBIOM[5]))
    IDref <- 1:length(MAT$Biomass)
    NnewG <- c(MAT$N[IDref], Nnew)
    BiomnewG <- c(MAT$Biomass[IDref], Biomnew)
    
    fit2 <- nls(NnewG~a*BiomnewG^b,start=list(a=5000,b=-1),
                na.action=na.omit)
    
    #X1 <- seq(400,4000,length.out=100)
#    Y1 <- coef(fit2)[1]*X1^coef(fit2)[2]
    #plot(BiomnewG,NnewG)
    #points(X,Y,lty=1,lwd=2,col=2,type='l')
#    points(X1,Y1,lty=1,lwd=2,col=5,type='l')
    
    #summary(fit2)
    
    # SSE fit de r?f?rence
    x0 <- MAT$Biomass[IDref]
    y0Meas <- MAT$N[IDref]
    y0theo <- coef(fit0)[1]*x0^coef(fit0)[2]
    SCE0 <- sum((y0theo-y0Meas)^2)
    
    # SSE fit ?chantillonn?
    x1 <- Biomnew
    y1Meas <- Nnew
    y1theo <- coef(fit1)[1]*x1^coef(fit1)[2]
    SCE1 <- sum((y1theo-y1Meas)^2)
    
    # SSE fit global
    x2 <- BiomnewG
    y2Meas <- NnewG
    y2theo <- coef(fit2)[1]*x2^coef(fit2)[2]
    SCE2 <- sum((y2theo-y2Meas)^2)
    
    # calcul du F observ?
    FOBS <- ((SCE2 - (SCE1+SCE0) )/ (length(coef(fit0))+length(coef(fit1)) - length(coef(fit2))) ) / 
      ((SCE1+SCE0)/(length(x1)+length(x2) - length(coef(fit0)) - length(coef(fit1))))
    
    # F th?orique de la table de fisher
    if (length(BiomnewG)<150){FTHEO <-  3.087}
    if ((length(BiomnewG)>=150)&(length(BiomnewG)<200)){FTHEO <-   3.056}
    if (length(BiomnewG)>=200){FTHEO <-   3.041}
    
    Ftheo <- c(Ftheo,FTHEO)
    Fobs <- c(Fobs,FOBS)
    
    # RMSE du fit ?chantillon?
    xrmse <- MAT$Biomass
    yrmsetheo <- coef(fit1)[1]*xrmse^coef(fit1)[2]
    
    RMSE <- c(RMSE,sqrt(sum((yrmsetheo-MAT$N)^2,na.rm=T)/length(xrmse)))
    
    
  }
  MATFTHEO2 <- data.frame(MATFTHEO2,Ftheo)
  MATFOBS2 <- data.frame(MATFOBS2,Fobs)
  MATRMSE2 <- data.frame(MATRMSE2,RMSE)
}
MATFTHEO2 <- MATFTHEO2[,2:21] ; names(MATFTHEO2) <- 1:20
MATFOBS2 <- MATFOBS2[,2:21] ; names(MATFOBS2) <- 1:20
# on isole les RMSE qui ont rat?, celles qui ont r?ussies = NA
MATRMSE2 <- MATRMSE2[,2:21] ; names(MATRMSE2) <- 1:20
MATRMSE2 <- replace(MATRMSE2,MATFOBS2<MATFTHEO2,NA)


load("~/R/R_Divers/Daniel/SAUV2.RData")

### figure 3 points

# Identification des relation qui ont rat�
LISTFIT <- list()
PROB <-c()

for (j in 1:5){
  
  IDNB <- c(1,2,5,10,12)  
  ID <- which(MATFOBS[,IDNB[j]]> MATFTHEO[,IDNB[j]])
  #  ID <- 1:length(MATFOBS[,IDNB[j]])
  PROB <- c(PROB, length(ID)/5000)
  XVAL <- seq(300,4000, length.out=100)
  YVAL <- c()
  xdiff <-c()
  for (i in 1:length(ID)){
    #    Y <- FIT[[(5000)+ID[i]]][1] * XVAL ^FIT[[(5000)+ID[i]]][2]
    Y <- FIT[[(5000*(IDNB[j]-1))+ID[i]]][1] * XVAL ^FIT[[(5000*(IDNB[j]-1))+ID[i]]][2]
    YVAL <- c(YVAL,Y)
  }
  LISTFIT[[j]] <- data.frame(rep(XVAL,length(ID)),YVAL)
}

LISTBORN <- list()
for (j in 1:5){
  #  YMAX <- sapply(XVAL, function(i) max(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  #  YMIN <- sapply(XVAL, function(i) min(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  YMAX <- sapply(XVAL, function(i) 1.96*sd(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  YMIN <- sapply(XVAL, function(i) 1.96*sd(LISTFIT[[j]]$YVAL[LISTFIT[[j]][,1]==i]))
  LISTBORN[[j]] <- data.frame(YMIN,YMAX)
}

LISTFIT2 <- list()
PROB2 <-c()

for (j in 1:5){
  
  IDNB <- c(1,2,5,10,15)  
  ID <- which(MATFOBS2[,IDNB[j]]> MATFTHEO2[,IDNB[j]])
  #  ID <- 1:length(MATFOBS[,IDNB[j]])
  PROB2 <- c(PROB2, length(ID)/5000)
  XVAL <- seq(300,4000, length.out=100)
  YVAL <- c()
  xdiff <-c()
  for (i in 1:length(ID)){
#    Y <- FIT2[[(5000)+ID[i]]][1] * XVAL ^FIT2[[(5000)+ID[i]]][2]
    Y <- FIT2[[(5000*(IDNB[j]-1))+ID[i]]][1] * XVAL ^FIT2[[(5000*(IDNB[j]-1))+ID[i]]][2]
    YVAL <- c(YVAL,Y)
  }
  LISTFIT2[[j]] <- data.frame(rep(XVAL,length(ID)),YVAL)
}

LISTBORN2 <- list()
for (j in 1:5){
  YMAX <- sapply(XVAL, function(i) 1.96*sd(LISTFIT2[[j]]$YVAL[LISTFIT2[[j]][,1]==i]))
  YMIN <- sapply(XVAL, function(i) 1.96*sd(LISTFIT2[[j]]$YVAL[LISTFIT2[[j]][,1]==i]))
  LISTBORN2[[j]] <- data.frame(YMIN,YMAX)
}


# fit avec toutes les donn?es
fit0 <- nls(N~a*Biomass^b,data=MAT,start=list(a=300,b=-0.6),
            na.action=na.omit)
X0 <- seq(300,4000,length.out=100)
Y0 <- coef(fit0)[1]*X0^coef(fit0)[2]

IDNB
XM <- as.numeric(levels(as.factor(MAT$Biomass)))[c(1,3,5)]
YM <- sapply(1:length(XM), function(i) mean(MAT$N[MAT$Biomass==XM[i]],na.rm=T))

#x11()

x11()

par(mfrow=c(2,1),xaxs='i',yaxs='i',mar=c(3,4,1.5,1),oma=c(0,0,0,0))
plot(MAT$Biomass/1000,MAT$N,ylim=c(0,15),xlim=c(300,4000)/1000,col=0,
     mgp=c(2.2,0.7,0),tck=-0.02,las=1,
     ylab=expression('Aboveground N content'~~(mg~g^-1)),xlab=expression('Aboveground dry mass'~~(kg~m^-2)),cex.lab=1)
XM <- as.numeric(levels(as.factor(MAT$Biomass)))
YM <- sapply(1:length(XM), function(i) mean(MAT$N[MAT$Biomass==XM[i]],na.rm=T))
points(XM/1000,YM,pch=16,cex=1.5)
points(X0/1000,Y0,type='l',col=2,lwd=2)
points(XVAL/1000,Y0-LISTBORN[[2]]$YMIN,type='l',col=1,lty=4)
points(XVAL/1000,Y0+LISTBORN[[2]]$YMAX,type='l',col=1,lty=4)
points(XVAL/1000,Y0+LISTBORN[[3]]$YMIN,type='l',col=1,lty=3)
points(XVAL/1000,Y0-LISTBORN[[3]]$YMAX,type='l',col=1,lty=3)
points(XVAL/1000,Y0+LISTBORN[[4]]$YMIN,type='l',col=1,lty=2)
points(XVAL/1000,Y0-LISTBORN[[4]]$YMAX,type='l',col=1,lty=2)

legend('topright',legend=c('Reference dilution curve: 20 x 5 dates',
                           'Average measured N content','',
                           'Reduced sampling: 5 dates',
                           expression('CI'['95%']~'n/date = 15,'~P[DIFF]~'='~'0 %'),
                           expression('CI'['95%']~'n/date = 10,'~P[DIFF]~'='~'0.9 %'),
                           expression('CI'['95%']~'n/date = 5,'~P[DIFF]~'='~'3.2 %'),
                           expression('CI'['95%']~'n/date = 2,'~P[DIFF]~'='~'4.9 %')),
       pch=c(NA,16,NA,NA,NA,NA,NA,NA),lty=c(1,NA,NA,NA,NA,2,3,4),col=c(2,1,1,1,1,1,1,1),cex=1,bty='n')
mtext(text = '(a)',side = 3,adj = -0.12,font=1,cex=1.5)

plot(MAT$Biomass/1000,MAT$N,ylim=c(0,15),xlim=c(300,4000)/1000,col=0,
     mgp=c(2.2,0.7,0),tck=-0.02,las=1,
     ylab=expression('Aboveground N content'~~(mg~g^-1)),xlab=expression('Aboveground dry mass'~~(kg~m^-2)),cex.lab=1)
XM <- as.numeric(levels(as.factor(MAT$Biomass)))[c(1,3,5)]
YM <- sapply(1:length(XM), function(i) mean(MAT$N[MAT$Biomass==XM[i]],na.rm=T))
points(XM/1000,YM,pch=16,cex=1.5)
points(X0/1000,Y0,type='l',col=2,lwd=2)
points(XVAL/1000,Y0-LISTBORN2[[2]]$YMIN,type='l',col=1,lty=4)
points(XVAL/1000,Y0+LISTBORN2[[2]]$YMAX,type='l',col=1,lty=4)
points(XVAL/1000,Y0+LISTBORN2[[3]]$YMIN,type='l',col=1,lty=3)
points(XVAL/1000,Y0-LISTBORN2[[3]]$YMAX,type='l',col=1,lty=3)
points(XVAL/1000,Y0+LISTBORN2[[4]]$YMIN,type='l',col=1,lty=2)
points(XVAL/1000,Y0-LISTBORN2[[4]]$YMAX,type='l',col=1,lty=2)
points(XVAL/1000,Y0+LISTBORN2[[5]]$YMIN,type='l',col=1,lty=1)
points(XVAL/1000,Y0-LISTBORN2[[5]]$YMAX,type='l',col=1,lty=1)

legend('topright',legend=c('Reference dilution curve: 20 x 5 dates',
                           'Average measured N content','',
                           'Reduced sampling: 3 dates',
                           expression('CI'['95%']~'n/date = 15,'~P[DIFF]~'='~'0.52 %'),
                           expression('CI'['95%']~'n/date = 10,'~P[DIFF]~'='~'2.96 %'),
                           expression('CI'['95%']~'n/date = 5,'~P[DIFF]~'='~'5.22 %'),
                           expression('CI'['95%']~'n/date = 2,'~P[DIFF]~'='~'5.50 %')),
       pch=c(NA,16,NA,NA,NA,NA,NA,NA),lty=c(1,NA,NA,NA,1,2,3,4),col=c(2,1,1,1,1,1,1,1),cex=1,bty='n')
mtext(text = '(b)',side = 3,adj = -0.12,font=1,cex=1.5)



#####
## tirage d'une courbe

IDGOOD5 <-list()
IDGOOD3 <-list()
IDNB <- c(2,5,20)  

for (j in 1:3){
  IDGOOD3[[j]] <- which(MATFOBS2[,IDNB[j]]<= MATFTHEO2[,IDNB[j]])
  IDGOOD5[[j]] <- which(MATFOBS[,IDNB[j]]<= MATFTHEO[,IDNB[j]])
}

YFIT5 <- list()
YFIT3 <- list()
for (j in 1:3){
  YFIT5[[j]] <- FIT[[(5000*(IDNB[j]-1))+sample(IDGOOD5[[j]],1)]]
  YFIT3[[j]] <- FIT2[[(5000*(IDNB[j]-1))+sample(IDGOOD3[[j]],1)]]
}

fit05 <- nls(N~a*Biomass^b,data=MAT,start=list(a=300,b=-0.6),
            na.action=na.omit)
fit03 <- nls(N~a*Biomass^b,data=subset(MAT,MAT$Jour %in% c(91,147,251)),start=list(a=300,b=-0.6),
             na.action=na.omit)

XVAL <- seq(300,4000, length.out=100)
Y520 <- coef(fit05)[1]*XVAL^coef(fit05)[2]
Y55 <- YFIT5[[2]][1]*XVAL^YFIT5[[2]][2]
Y52 <- YFIT5[[1]][1]*XVAL^YFIT5[[1]][2]
Y320 <- coef(fit03)[1]*XVAL^coef(fit03)[2]
Y35 <- YFIT3[[2]][1]*XVAL^YFIT3[[2]][2]
Y32 <- YFIT3[[1]][1]*XVAL^YFIT3[[1]][2]



plot(XVAL/1000,Y520,type='l',lwd=1,col=1)
points(XVAL/1000,Y55,type='l',lty=2)
points(XVAL/1000,Y52,type='l',lty=3)
points(XVAL/1000,Y320,type='l',lty=1,col=2)
points(XVAL/1000,Y35,type='l',lty=2,col=2)
points(XVAL/1000,Y32,type='l',lty=3,col=2)

legend('topright',legend=c('5 dates x 20 canes','5 dates x 5 canes','5 dates x 2 canes',
                           '3 dates x 20 canes','3 dates x 5 canes','3 dates x 2 canes'),
       pch=NA,lty=c(1:3,1:3),col=c(1,1,1,2,2,2),bty='n',cex=1.2)

