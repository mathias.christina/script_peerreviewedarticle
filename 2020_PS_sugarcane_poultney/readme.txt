 Optimising non-destructive sampling methods to study nitrogen use efficiency throughout the growth-cycle of giant C4 crops. Poultney Daniel, Christina Mathias, Versini Antoine. 2020. Plant and Soil, 613 : 453-597.
https://doi.org/10.1007/s11104-020-04611-3
