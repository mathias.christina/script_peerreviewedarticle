library(readxl)
library(dplyr)
library(ranger)
library(caret)
library(ggplot2)
library(stringr)
library(ggpointdensity)
library(qgraph)
library(psych)
library("FactoMineR")
library("factoextra")
library("corrplot")
library(ggpubr)
library(tibble)
library(RSNNS)
library(mlr3measures)

# load cleaned data
load("DATA_verse/R_save_data_cleaning.RData")

##### performance indicator ----
# lodging classification
# confustion matrix
CM_list <- list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA)
# accuracy
Accuracy_list <- list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA) #(VP+VN)/(VP+VN+FP+FN). 
# balance accuracy mais == accuracy car jeux de donnée dupliqué
bac_list <- list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA) 
# indicateurs via carret: sensibilité, sensitivité, etc
CMind_list <- list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA)

# yield prediction
meanbias_list <-list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA) # 1/n pred-obs
MAPE_list <-list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA) # 1/N abs(pred-obs)/obs
RMSE_list <-list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA)
R2_list <- list(RF=NA,MLP=NA,SVN=NA,KNN=NA,GBM=NA,naive_bayes=NA,multinom=NA)

###### 1) Lodging Variety model -------------------
# select only commercial variety ----
MM <- MATALL %>% group_by(Variete) %>% count()
MM <- MM[order(MM$n,decreasing = T),]
MM[1:20,]
MATALLV <- MATALL %>% filter(Variete %in% MM$Variete[1:9])
table(MATALLV$Variete,MATALLV$Code_station)

levels(MATALLV$VerseC) <- c("D","B","V")

# Data train & validation split ------
used_var <- c('Variete','RU','STMEAN','SRG','SETP','TOTPPT','WD','TOTWIND','ALT')
K=10
# partition with inital data
set.seed(1)
fitPartition  <- createFolds(MATALLV$VerseC, k = K, returnTrain = T)
sapply(fitPartition, length)

set.seed(1)
my_seed = sample(1:1000, 10); my_seed

# partition with duplicate data
table(MATALLV$VerseC)
MATALLVD <- MATALLV %>% filter(MATALLV$VerseC =="D")
MATALLVB <- MATALLV %>% filter(MATALLV$VerseC =="B")
MATALLVV <- MATALLV %>% filter(MATALLV$VerseC =="V")
set.seed(4)
MATALLVB <- rbind(MATALLVB,MATALLVB,MATALLVB,MATALLVB[sample(1:dim(MATALLVB)[1],1361-1152),])
set.seed(4)
MATALLVV <- rbind(MATALLVV,MATALLVV,MATALLVV,MATALLVV,MATALLVV[sample(1:dim(MATALLVV)[1],1361-1100),])
MATALLVNN <- rbind(MATALLVD,MATALLVB,MATALLVV)
set.seed(1)
fitPartitionNN  <- createFolds(MATALLVNN$VerseC, k = K, returnTrain = T)

MATALLVNN <- MATALLVNN %>%
  mutate(Variete = case_when(
    Variete == "R 582" ~ "R582",
    Variete == "R 570" ~ "R570",
    Variete == "R 579" ~ "R579",
    Variete == "R 584" ~ "R584",
    Variete == "R 585" ~ "R585",
    Variete == "R 587" ~ "R587",
    Variete == "R 583" ~ "R584",
    Variete == "R 577" ~ "R577",
    Variete == "R 586" ~ "R586",
    TRUE ~ Variete
  ))


# RF - model  ------
CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
CMind <- matrix(0,3,11)
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  train_control <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  myGrid <- expand.grid(mtry=1:9,min.node.size=c(1,5,10),splitrule='gini') # variance pour rdt
  set.seed(my_seed[k])
  ranger_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
                                 data = datatrain, method = "ranger",tuneGrid=myGrid,
                                 metric="Accuracy",#tuneGrid=rfGrid, 
                                 trControl = train_control, tuneLength = 10)

  fitted <- predict(ranger_model, dataval)
  CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
  CMind <- CMind + CMIND$byClass/K
  CM = CM + table(dataval$VerseC, fitted)
  accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
  bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
  print(k)
}
CM_list[[1]] <- CM
Accuracy_list[[1]] <- accuracy
bac_list[[1]] <- bac
CMind_list[[1]] <- CMind

# MLP - model -----
CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
CMind <- matrix(0,3,11)
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],]
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]

  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  # myGrid <- expand.grid(size=list(c(5,5))) # variance pour rdt
  # myGrid <- expand.grid(layer1=c(3,6,9),layer2=c(3,6,9),layer3=c(3,6,9)) # variance pour rdt
  # myGrid <- data.frame(layer1=c(3,6,9,3,6,9,3,6,9),layer2=c(3,6,9,3,6,9,0,0,0),layer3=c(3,6,9,0,0,0,0,0,0))
  # set.seed(my_seed[k])
  # mlp_model <- caret::train(
  #   formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))),
  #   data = datatrain, method = "mlpML", trControl = fitControl,tuneGrid=myGrid,
  #   metric="Accuracy") #rmse pour rdt / accuracy pour verse
  
  # MLPMON
  myGrid <- expand.grid(hidden1 = c(5,9,16),n.ensemble = 1)
  set.seed(my_seed[k])
  mlp_model <- caret::train(
    formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))),
    data = datatrain, method = "monmlp", trControl = fitControl,tuneGrid=myGrid,hidden2=9,
    metric="Accuracy")

  fitted <- predict(mlp_model, dataval)
  CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
  CMind <- CMind + CMIND$byClass/K
  CM = CM + table(dataval$VerseC, fitted)
  accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
  bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
  print(k)
}
CM_list[[2]] <- CM
Accuracy_list[[2]] <- accuracy
bac_list[[2]] <- bac
CMind_list[[2]] <- CMind


# SVN - model ------
CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
CMind <- matrix(0,3,11)
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]

  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  set.seed(my_seed[k])
  myGrid <- expand.grid(C = 2^(8:12), sigma = 2^(-2:2))
  svm_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
                     data = datatrain, method = "svmRadial",tuneGrid=myGrid,
                     trControl = fitControl,# preProcess = c("center", "scale"),
                     tuneLength = 10)
  
  fitted <- predict(svm_model, dataval)
  CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
  CMind <- CMind + CMIND$byClass/K
  CM = CM + table(dataval$VerseC, fitted)
  accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
  bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
  print(k)
}
CM_list[[3]] <- CM
Accuracy_list[[3]] <- accuracy
bac_list[[3]] <- bac
CMind_list[[3]] <- CMind

# KNN - model ------
CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
CMind <- matrix(0,3,11)
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]

  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  myGrid <- data.frame(k=1:10)
  set.seed(my_seed[k])
  knn_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
                            data = datatrain, method = "knn",tuneGrid=myGrid,
                            trControl = fitControl,# preProcess = c("center", "scale"),
                            tuneLength = 10)
  
  fitted <- predict(knn_model, dataval)
  CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
  CMind <- CMind + CMIND$byClass/K
  CM = CM + table(dataval$VerseC, fitted)
  accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
  bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
  print(k)
}
CM_list[[4]] <- CM
Accuracy_list[[4]] <- accuracy
bac_list[[4]] <- bac
CMind_list[[4]] <- CMind

# GBM - model ------
CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
CMind <- matrix(0,3,11)
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  set.seed(my_seed[k])
  gbm_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
                            data = datatrain, method = "gbm",
                            trControl = fitControl,# preProcess = c("center", "scale"),
                            verbose = FALSE, tuneLength = 10)
  
  fitted <- predict(gbm_model, dataval)
  CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
  CMind <- CMind + CMIND$byClass/K
  CM = CM + table(dataval$VerseC, fitted)
  accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
  bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
  print(k)
}
CM_list[[5]] <- CM
Accuracy_list[[5]] <- accuracy
bac_list[[5]] <- bac
CMind_list[[5]] <- CMind

# # Naive Bayes - model ------
# CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
# CMind <- matrix(0,3,11)
# for (k in 1:K){
#   datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
#   dataval <- MATALLVNN[-fitPartitionNN[[k]],]
#   
#   set.seed(my_seed[k])
#   fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
#   fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
#   set.seed(my_seed[k])
#   naive_bayes_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
#                             data = datatrain, method = "naive_bayes",
#                             trControl = fitControl)
#   
#   fitted <- predict(naive_bayes_model, dataval)
#   CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
#   CMind <- CMind + CMIND$byClass/K
#   CM = CM + table(dataval$VerseC, fitted)
#   accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
#   bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
#   print(k)
# }
# CM_list[[6]] <- CM
# Accuracy_list[[6]] <- accuracy
# bac_list[[6]] <- bac
# CMind_list[[6]] <- CMind

# Logistic Regression - model  ------
# CM = matrix(0, 3, 3); accuracy<-0 ; bac <- 0 ; 
# CMind <- matrix(0,3,11)
# for (k in 1:K){
#   datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
#   dataval <- MATALLVNN[-fitPartitionNN[[k]],]
#   
#   set.seed(my_seed[k])
#   fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
#   fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
#   set.seed(my_seed[k])
#   multinom_model <- caret::train(formula(paste0("VerseC ~ ",paste0(used_var, collapse = " + "))), 
#                                     data = datatrain, method = "multinom",
#                                     trControl = fitControl, metric="Accuracy")
#   
#   fitted <- predict(multinom_model, dataval)
#   CMIND <- caret::confusionMatrix(fitted, dataval$VerseC)
#   CMind <- CMind + CMIND$byClass/K
#   CM = CM + table(dataval$VerseC, fitted)
#   accuracy <- accuracy +1/K * acc(dataval$VerseC,fitted)# sum(dataval$VerseC == fitted) / length(fitted) / K
#   bac <- bac + 1/K * bacc(dataval$VerseC,fitted)
#   print(k)
# }
# CM_list[[7]] <- CM
# Accuracy_list[[7]] <- accuracy
# bac_list[[7]] <- bac
# CMind_list[[7]] <- CMind

# CM = round(CM/rowSums(CM)*100,1)
# CM

# save ----
save.image("DATA_verse/R_model_lodging_comparison.RData")

####### 2) Yield variety model ---------
# RF - model  ------
Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  train_control <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  myGrid <- expand.grid(mtry=1:9,min.node.size=c(1,5,10),splitrule='variance') # variance pour rdt
  set.seed(my_seed[k])
  ranger_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
                               data = datatrain, method = "ranger",tuneGrid=myGrid,
                               metric="RMSE",#tuneGrid=rfGrid, 
                               trControl = train_control, tuneLength = 10)
  
  fitted <- predict(ranger_model, dataval)
  Mape <- Mape + mape(dataval$TCH,fitted)/K
  Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
  Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
  Bias <- Bias + bias(dataval$TCH,fitted)/K
  print(k)
}
meanbias_list[[1]] <- Bias
MAPE_list[[1]] <- Mape
RMSE_list[[1]] <- Rmse
R2_list[[1]] <- Rsq

# MLP - model -----
Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],]
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
#  myGrid <- data.frame(layer1=c(3,6,9,3,6,9,3,6,9),layer2=c(3,6,9,3,6,9,0,0,0),layer3=c(3,6,9,0,0,0,0,0,0))
  set.seed(my_seed[k])

  # MLPMON
  myGrid <- expand.grid(hidden1 = c(5,9,16),n.ensemble = 1)
  set.seed(my_seed[k])
  mlp_model <- caret::train(
    formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),
    data = datatrain, method = "monmlp", trControl = fitControl,tuneGrid=myGrid,hidden2=9,
    metric="RMSE")
  fitted <- predict(mlp_model, dataval)
  plot(dataval$TCH,fitted);abline(0,1)
  rsq(dataval$TCH,fitted)
  cor(dataval$TCH,fitted)^2
  # MLPML

  
  # # MLPML
  # myGrid <- data.frame(layer1=c(5,5,5),layer2=c(5,5,0),layer3=c(5,0,0))
  # mlp_model <- caret::train(
  #   formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),
  #   data = datatrain, method = "mlpML", trControl = fitControl,tuneGrid=myGrid,
  #   linOut=T, hiddenActFunc="Act_Identity",  maxit = 500,
  #   metric="Rsquared")
  # fitted <- predict(mlp_model, dataval)
  # plot(dataval$TCH,fitted);abline(0,1)
  # rsq(dataval$TCH,fitted)
  # cor(dataval$TCH,fitted)^2
  
  # # MLP
  # myGrid <- data.frame(size=c(5,9,16))
  # mlp_model <- caret::train(
  #   formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),
  #   data = datatrain, method = "mlp", trControl = fitControl,tuneGrid=myGrid,
  #   linOut=T,
  #   metric="Rsquared")
  # 
  # fitted <- predict(mlp_model, dataval)
  # plot(dataval$TCH,fitted);abline(0,1)
  # rsq(dataval$TCH,fitted)
  # cor(dataval$TCH,fitted)^2
  # 
  # # mlp only
  # #design.matrix2 = function(factor) contrasts(as.factor(factor), contrasts = FALSE)[factor, ]
  # #xx <- design.matrix2(datatrain$Variete)
  # xx <- model.matrix(formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),datatrain)
  # 
  # mlp_model2 <- mlp(x = xx,y=datatrain$TCH,size = dim(xx)[2],linOut = T)
  # fitted <- predict(mlp_model2, newdata = model.matrix(formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),dataval))
  # plot(dataval$TCH,fitted);abline(0,1)
  # 
  # rsq(dataval$TCH,fitted)
  # cor(dataval$TCH,fitted)^2
  # 
  # # mlpML only
  # #design.matrix2 = function(factor) contrasts(as.factor(factor), contrasts = FALSE)[factor, ]
  # #xx <- design.matrix2(datatrain$Variete)
  # xx <- model.matrix(formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),datatrain)
  # 
  # mlp_model2 <- mlp(x = xx,y=datatrain$TCH,size = dim(xx)[2],linOut = T)
  # fitted <- predict(mlp_model2, newdata = model.matrix(formula(paste0("TCH ~ -1 +",paste0(used_var, collapse = " + "))),dataval))
  # plot(dataval$TCH,fitted);abline(0,1)
  # 
  # rsq(dataval$TCH,fitted)
  # cor(dataval$TCH,fitted)^2
  # 
  # ## brnn
  # myGrid <- data.frame(neurons=3)
  # mlp_model <- caret::train(
  #   formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))),
  #   data = datatrain, method = "brnn", trControl = fitControl,tuneGrid=myGrid,
  #   metric="RMSE") 
  # 
  # fitted <- predict(mlp_model, dataval)
  # plot(dataval$TCH,fitted)
  # 
  # rsq(dataval$TCH,fitted)
  # cor(dataval$TCH,fitted)^2
  
  Mape <- Mape + mape(dataval$TCH,fitted)/K
  Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
  Rsq <- Rsq + rsq(dataval$TCH,fitted)/K ;print(rsq(dataval$TCH,fitted))
  Bias <- Bias + bias(dataval$TCH,fitted)/K
  print(k)
}
meanbias_list[[2]] <- Bias
MAPE_list[[2]] <- Mape
RMSE_list[[2]] <- Rmse
R2_list[[2]] <- Rsq


# SVN - model ------
Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  myGrid <- expand.grid(C = 2^(8:12), sigma = 2^(-2:2))
  set.seed(my_seed[k])
  svm_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
                            data = datatrain, method = "svmRadial",tuneGrid=myGrid,
                            trControl = fitControl,# preProcess = c("center", "scale"),
                            tuneLength = 10)
  
  fitted <- predict(svm_model, dataval)
  Mape <- Mape + mape(dataval$TCH,fitted)/K
  Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
  Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
  Bias <- Bias + bias(dataval$TCH,fitted)/K
  print(k)
}
meanbias_list[[3]] <- Bias
MAPE_list[[3]] <- Mape
RMSE_list[[3]] <- Rmse
R2_list[[3]] <- Rsq

# KNN - model ------
Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  myGrid <- data.frame(k=1:10)
  set.seed(my_seed[k])
  knn_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
                            data = datatrain, method = "knn",tuneGrid=myGrid,
                            trControl = fitControl,# preProcess = c("center", "scale"),
                            tuneLength = 10)
  
  fitted <- predict(knn_model, dataval)
  Mape <- Mape + mape(dataval$TCH,fitted)/K
  Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
  Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
  Bias <- Bias + bias(dataval$TCH,fitted)/K
  print(k)
}
meanbias_list[[4]] <- Bias
MAPE_list[[4]] <- Mape
RMSE_list[[4]] <- Rmse
R2_list[[4]] <- Rsq

# GBM - model ------
Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
for (k in 1:K){
  datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
  dataval <- MATALLVNN[-fitPartitionNN[[k]],]
  
  set.seed(my_seed[k])
  fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
  fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
  set.seed(my_seed[k])
  gbm_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
                            data = datatrain, method = "gbm",
                            trControl = fitControl,# preProcess = c("center", "scale"),
                            verbose = FALSE, tuneLength = 10)
  
  fitted <- predict(gbm_model, dataval)
  Mape <- Mape + mape(dataval$TCH,fitted)/K
  Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
  Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
  Bias <- Bias + bias(dataval$TCH,fitted)/K
  print(k)
}
meanbias_list[[5]] <- Bias
MAPE_list[[5]] <- Mape
RMSE_list[[5]] <- Rmse
R2_list[[5]] <- Rsq

# Naive Bayes - model ------
# Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
# for (k in 1:K){
#   datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
#   dataval <- MATALLVNN[-fitPartitionNN[[k]],]
#   
#   set.seed(my_seed[k])
#   fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
#   fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
#   set.seed(my_seed[k])
#   naive_bayes_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
#                                     data = datatrain, method = "naive_bayes",
#                                     trControl = fitControl)
#   
#   fitted <- predict(naive_bayes_model, dataval)
#   Mape <- Mape + mape(dataval$TCH,fitted)/K
#   Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
#   Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
#   Bias <- Bias + bias(dataval$TCH,fitted)/K
#   print(k)
# }
# meanbias_list[[6]] <- Bias
# MAPE_list[[6]] <- Mape
# RMSE_list[[6]] <- Rmse
# R2_list[[6]] <- Rsq

# Logistic Regression - model  ------
# Bias <- 0; Rmse <- 0;Mape <- 0;Rsq <-0
# for (k in 1:K){
#   datatrain  <- MATALLVNN[fitPartitionNN[[k]],] 
#   dataval <- MATALLVNN[-fitPartitionNN[[k]],]
#   
#   set.seed(my_seed[k])
#   fitSubPartition  <- createFolds(datatrain$VerseC, k = 10, returnTrain = T)
#   fitControl <- trainControl(method = "cv",number = 10,search = "grid",index = fitSubPartition)
#   set.seed(my_seed[k])
#   multinom_model <- caret::train(formula(paste0("TCH ~ ",paste0(used_var, collapse = " + "))), 
#                                  data = datatrain, method = "multinom",
#                                  trControl = fitControl, metric="Accuracy")
#   
#   fitted <- predict(multinom_model, dataval)
#   Mape <- Mape + mape(dataval$TCH,fitted)/K
#   Rmse <- Rmse + rmse(dataval$TCH,fitted)/K
#   Rsq <- Rsq + rsq(dataval$TCH,fitted)/K
#   Bias <- Bias + bias(dataval$TCH,fitted)/K
#   print(k)
# }
# meanbias_list[[7]] <- Bias
# MAPE_list[[7]] <- Mape
# RMSE_list[[7]] <- Rmse
# R2_list[[7]] <- Rsq

# save ----
save.image("DATA_verse/R_model_lodging_comparison.RData")


# load
load("DATA_verse/R_model_lodging_comparison.RData")
Accuracy_list
CMind_list

MAPE_list
RMSE_list
R2_list