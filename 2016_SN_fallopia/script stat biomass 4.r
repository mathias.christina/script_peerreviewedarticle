setwd("/Users/mathias/Documents/R/R_fallopia")

# �tude biomass

datas <- read.csv2("biom_area_tot.csv",header=TRUE)
attach(datas)

# type = type de propagule
# sureau = traitement
# belowbiom_perg = biomass sous terraine

# effet type de propagule
typprop <- type
for (i in 1:length(type)){
if (type[i]=='r2'){
typprop[i] <- 'r1'}  }

shapiro.test(belowbiom_perg[typprop=='r1'])
shapiro.test(belowbiom_perg[typprop=='g'])
bartlett.test(belowbiom_perg[typprop=='r1'],sureau[typprop=='r1'])
bartlett.test(belowbiom_perg[typprop=='g'],sureau[typprop=='g'])
bartlett.test(belowbiom_perg,typprop) # pas bon
shapiro.test(abovebiom_perg[typprop=='r1'])
shapiro.test(abovebiom_perg[typprop=='g'])
bartlett.test(abovebiom_perg[typprop=='r1'],sureau[typprop=='r1'])
bartlett.test(abovebiom_perg[typprop=='g'],sureau[typprop=='g'])
bartlett.test(abovebiom_perg,typprop) # pas bon
shapiro.test(leafbiom_perg[typprop=='r1'])
shapiro.test(leafbiom_perg[typprop=='g'])
bartlett.test(leafbiom_perg[typprop=='r1'],sureau[typprop=='r1'])
bartlett.test(leafbiom_perg[typprop=='g'],sureau[typprop=='g'])
bartlett.test(leafbiom_perg,typprop) # pas bon


ana1 <- aov(belowbiom_perg~ sureau*typprop)

belg <- belowbiom_perg[1:10]
belr <- belowbiom_perg[11:29]
surg <- sureau[1:10]
surr <- sureau[11:29]

t.test(belg[surg=='oui'],belg[surg=='non'])
t.test(belr[surr=='oui'],belg[surr=='non'])


TukeyHSD(ana1,"sureau:typprop"))

ana2 <- aov(abovebiom_perg~ sureau*typprop)
ana3 <- aov(leafbiom_perg~ sureau*typprop)

#posthoc analysis sur belowbiom
t.test(belowbiom_perg[typprop=='r1'][sureau=='oui'],belowbiom_perg[typprop=='r1'][sureau=='non'], var.equal=T) 
#t = -2.8503, df = 17, p-value = 0.01107
t.test(belowbiom_perg[typprop=='g'][sureau=='oui'],belowbiom_perg[typprop=='g'][sureau=='non'], var.equal=T) 
#t = -5.3002, df = 8, p-value = 0.0007282

# effet type de population
typpop <- type[11:29]
ana1 <- aov(belowbiom_perg[11:29]~ sureau[11:29]*typpop)
ana2 <- aov(abovebiom_perg[11:29]~ sureau[11:29]*typpop)
ana3 <- aov(leafbiom_perg[11:29]~ sureau[11:29]*typpop)

shapiro.test(belowbiom_perg[typpop=='r1'])
shapiro.test(belowbiom_perg[typpop=='r2'])
bartlett.test(belowbiom_perg[typpop=='r1'],sureau[typpop=='r1'])
bartlett.test(belowbiom_perg[typpop=='r2'],sureau[typpop=='r2'])
bartlett.test(belowbiom_perg,typpop) # pas bon
shapiro.test(abovebiom_perg[typpop=='r1'])
shapiro.test(abovebiom_perg[typpop=='r2'])
bartlett.test(abovebiom_perg[typpop=='r1'],sureau[typpop=='r1'])
bartlett.test(abovebiom_perg[typpop=='r2'],sureau[typpop=='r2'])
bartlett.test(abovebiom_perg,typpop) # pas bon
shapiro.test(leafbiom_perg[typpop=='r1'])
shapiro.test(leafbiom_perg[typpop=='r2'])
bartlett.test(leafbiom_perg[typpop=='r1'],sureau[typpop=='r1'])
bartlett.test(leafbiom_perg[typpop=='r2'],sureau[typpop=='r2'])
bartlett.test(leafbiom_perg,typpop) # pas bon
