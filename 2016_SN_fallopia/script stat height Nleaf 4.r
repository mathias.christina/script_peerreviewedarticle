setwd("/Users/mathias/Documents/R/R_fallopia")

datas <- read.csv2("croiss_rhiz_germ.csv",header=TRUE)
attach(datas)

#test preliminaire

library(nlme)

datas2 <- datas[58:255,]
detach(datas)
attach(datas2)

# on test si le nombre de germination par pot � un effet sur la hauteur  et le nbr de feuilles

ana1 <- lm(log(Height[age=='50d'])~nbrg[age=='50d'])

ana2 <- glm(nbr_leaves[age=='50d']~nbrg[age=='50d'],family=poisson)
anova(ana2,test='Chisq')

# on test si le num�ro de pot � un effet sur la hauteur  et le nbr de feuilles

datas3 <- datas2[126:198,]
detach(datas2)
attach(datas3)

ana1 <- lm(log(Height[sureau=='oui'])~pot[sureau=='oui'])
ana2 <- lm(log(Height[sureau=='non'])~pot[sureau=='non'])

ana2 <- glm(nbr_leaves[age=='50d']~pot[age=='50d'],family=poisson)
anova(ana2,test='Chisq')


# test hauteur et nbr feuilles entre type de propagule 

detach(datas2)

datas50 <- read.csv2("croiss_rhiz_germ_50.csv",header=TRUE)

# on prend hauteur moyenne par paut et tout

dat <- datas50[1:19,]
dat2 <- datas50[20:92,]
for (i in 1:10){
x <- c(as.vector(dat2$sureau[dat2$pot==i][1]),as.vector(dat2$Type[dat2$pot==i][1]),
        mean(dat2$Height[dat2$pot==i],na.rm=T),mean(dat2$nbr_leaves[dat2$pot==i],na.rm=T),
        as.vector(dat2$age[dat2$pot==i][1]),NA,dat2$nbrg[dat2$pot==i][1],i)
dat <- rbind(dat,x)}
attach(dat)

Typprop <- Type
for (i in 1:length(Type)){
if (Type[i]=='r2'){
Typprop[i] <- 'r1'}  }

shapiro.test(log(Height[typprop=='r1']))
shapiro.test(log(Height[typprop=='g']))
bartlett.test(log(Height[typprop=='r1']),sureau[typprop=='r1'])
bartlett.test(log(Height[typprop=='g']),sureau[typprop=='g'])
bartlett.test(log(Height),typprop) # pas bon

potnew <- c()
for (i in 1:length(pot)){
if (pot[i] >= 11)
  potnew[i] <- 11
else
  potnew[i] <- pot[i]
}

Height <- as.numeric(Height)
ana1 <- lm(log(Height)~sureau*Typprop)



ana1 <- lme(log(Height)~sureau*Typprop,random=~1|potnew)

shapiro.test(nbr_leaves[typprop=='r1'])
shapiro.test(nbr_leaves[typprop=='g'])
bartlett.test(nbr_leaves[typprop=='r1'],sureau[typprop=='r1'])
bartlett.test(nbr_leaves[typprop=='g'],sureau[typprop=='g'])
bartlett.test(nbr_leaves),typprop) # pas bon

nbr_leaves <- as.numeric(nbr_leaves)
ana2 <- lm(nbr_leaves~sureau*Typprop)

ana2 <- lme(nbr_leaves~sureau*Typprop, random=~1|potnew)


# comparaison populations

Typpop <- c()
for (i in 1:length(Type)){
if (Type[i]=='r1')
  Typpop <- c(Typpop, 'r1')
else if (Type[i]=='r2')
  Typpop <- c(Typpop, 'r2')
else
  Typpop <- Typpop  
  }
Typpop <- as.factor(Typpop) 

ana1 <- lm(log(Height[1:19])~sureau[1:19]*Typpop)


ana1 <- lm(log(Height[1:19])~sureau[1:19]*Typpop)

ana2 <- glm(nbr_leaves[1:19]~sureau[1:19]*Typpop,family=poisson)
anova(ana2,test='Chisq')

ana3 <- lm(nbr_leaves[1:19]~sureau[1:19]*Typpop)

mean(Height[1:19][sureau=='oui'], na.rm=T) /mean(Height[10:19][sureau=='non'],na.rm=T)


# 32

detach(datas50)
datas32 <- read.csv2("croiss_rhiz_germ_32.csv",header=TRUE)
attach(datas32)

ana1 <- lme(log(Height)~sureau*Typprop,random=~1|potnew)
ana2 <- lme(nbr_leaves~sureau*Typprop, random=~1|potnew)

ana1 <- lm(log(Height[1:19])~sureau[1:19]*Typpop)
ana3 <- lm(nbr_leaves[1:19]~sureau[1:19]*Typpop)

#14

detach(datas32)
datas14 <- read.csv2("croiss_rhiz_germ_14.csv",header=TRUE)
attach(datas14)

Typprop <- Type
for (i in 1:length(Type)){
if (Type[i]=='r2'){
Typprop[i] <- 'r1'}  }


Typpop <- c()
for (i in 1:length(Type)){
if (Type[i]=='r1')
  Typpop <- c(Typpop, 'r1')
else if (Type[i]=='r2')
  Typpop <- c(Typpop, 'r2')
else
  Typpop <- Typpop  
  }
Typpop <- as.factor(Typpop) 


ana1 <- lme(log(Height)~sureau*Typprop,random=~1|pot)
ana2 <- lme(nbr_leaves~sureau*Typprop, random=~1|pot)

ana1 <- lm(log(Height[1:19])~sureau[1:19]*Typpop)
ana3 <- lm(nbr_leaves[1:19]~sureau[1:19]*Typpop)
