 Allelopathic effect of a native species on a major plant invader in Europe. Christina Mathias, Rouifed Soraya, Puijalon Sara, Vallier Félix, Meiffren Guillaume, Bellvert Floriant, Piola Florence. 2015. Naturwissenschaften, 102 (3-4):12, 8 p.
https://doi.org/10.1007/s00114-015-1263-x
