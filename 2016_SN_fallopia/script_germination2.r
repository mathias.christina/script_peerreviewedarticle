setwd("/Users/mathias/Documents/R/R_fallopia")
add.errorbars <- function(x,y,SE,direction,barlen=0.04){
if(direction=="up")arrows(x0=x, x1=x, y0=y, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="down")arrows(x0=x, x1=x, y0=y, y1=y - SE, code=3,
angle=90, length=barlen)
if(direction=="left")arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="right")arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="vert")arrows(x0=x, x1=x, y0=y-SE, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="hor")arrows(x0=x-SE, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
}
palette(grey(0:10/10))

datas <- read.table("germination.txt",header=TRUE)
attach(datas)

mat <- matrix(c(mean(germ[15:19],na.rm=T),
mean(germ[11:14],na.rm=T),
mean(germ[25:29],na.rm=T),
mean(germ[20:24],na.rm=T),
mean(germ[6:10],na.rm=T),
mean(germ[1:5],na.rm=T)),
nrow=2)

mg <- c(mean(germ[15:19],na.rm=T),
mean(germ[11:14],na.rm=T),
mean(germ[25:29],na.rm=T),
mean(germ[20:24],na.rm=T),
mean(germ[6:10],na.rm=T),
mean(germ[1:5],na.rm=T))

sdg <- c(sd(germ[15:19],na.rm=T),
sd(germ[11:14],na.rm=T),
sd(germ[25:29],na.rm=T),
sd(germ[20:24],na.rm=T),
sd(germ[6:10],na.rm=T),
sd(germ[1:5],na.rm=T))

sdx <- c(1,3,7,9,13,15)

windows(2.79,4.1)

#tiff(filename = "fig2.tif", width = 2.79, height = 4.1,
#     units = "in", pointsize = 7,
#     compression = c("none", "rle", "lzw", "jpeg", "zip"),
#     bg = "white", res = 400,
#     restoreConsole = TRUE)

par(mfrow=c(2,1))
par(font=1)
par(yaxs="i")
par(mar = c(2,3.5,1.5,0.5) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,16),cex.axis=0.9)
for (i in 1:6){
add.errorbars(x=sdx[i], y=mg[i],SE=sdg[i],direction='up')}

par (new=TRUE)
par(yaxs="i")
par(mar = c(2,3.5,1.5,0.5) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,16),yaxp=c(0,16,8),
cex.axis=1, cex.lab=0.9,cex.names=0.7,
ylab= expression("Number of days to emerge"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=0.8)
mtext ('(a)', side=3,line=0.5, font=2, cex=0.9,adj=0)

matt <- matrix(c(
mean(tx[6:10],na.rm=T),
mean(tx[1:5],na.rm=T)),
nrow=2)

mt <- c(
mean(tx[6:10],na.rm=T),
mean(tx[1:5],na.rm=T))

sdt <- c(
sd(tx[6:10],na.rm=T),
sd(tx[1:5],na.rm=T))

sdx <- c(1.5,2.5)

par(font=1)
par(yaxs="i")
par(mar = c(2,3.5,1.5,0.5) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,4), xaxp=c(0,4,4),yaxp=c(0,100,5),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,100),cex.axis=1)
for (i in 1:2){
add.errorbars(x=sdx[i], y=mt[i],SE=sdt[i],direction='up')}

par (new=TRUE)
par(yaxs="i")
par(mar = c(2,3.5,1.5,0.5) +0.1)
barplot(matt, beside=T, names.arg=c('Achenes'),
width=c(1,1),  xlim=c(0,4),
col=c(5,9),
ylim=c(0,100),yaxp=c(0,100,5),
cex.axis=1, cex.lab=0.9,cex.names=0.7,
ylab= expression("Germination rate (%)"),las=1,bty='o')
mtext ('(b)', side=3,line=0.5, font=2, cex=0.9,adj=0)

#dev.off()
#dev.set()
