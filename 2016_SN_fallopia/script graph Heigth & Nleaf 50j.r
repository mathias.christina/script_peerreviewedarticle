setwd("/Users/mathias/Documents/R/R_fallopia")
add.errorbars <- function(x,y,SE,direction,barlen=0.04){
if(direction=="up")arrows(x0=x, x1=x, y0=y, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="down")arrows(x0=x, x1=x, y0=y, y1=y - SE, code=3,
angle=90, length=barlen)
if(direction=="left")arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="right")arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="vert")arrows(x0=x, x1=x, y0=y-SE, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="hor")arrows(x0=x-SE, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
}
palette(grey(0:10/10))

datas <- read.csv2("croiss_rhiz3.csv",header=TRUE)
attach(datas)
datas2 <- read.csv2("croiss_germ2.csv",header=TRUE)
attach(datas2)

dat <- datas2[126:198,]
NAMES <- c("surg","potg","Heigthg","nbr_feuilleg", "ageg","nbr_germg")
Id <- c('f5','f9','f17','f23','f29','f6','f10','f18','f24','f30')
DT <- rep(NA,6)
for (i in 1:10){
x <- c(as.vector(dat$surg[dat$potg==Id[i]][1]),Id[i],
        mean(dat$Heigthg[dat$potg==Id[i]],na.rm=T),mean(dat$nbr_feuilleg[dat$potg==Id[i]],na.rm=T),
        as.vector(dat$ageg[dat$potg==Id[i]][1]),dat$nbr_germg[dat$potg==Id[i]][1])
DT <- rbind(DT,x)}
DT <- DT[2:11,]
DT <- data.frame(DT)
names(DT) <- NAMES
attach(DT)

Hgoui <- as.numeric(as.vector(DT$Heigthg[surg=='oui']))
Hgnon <- as.numeric(as.vector(DT$Heigthg[surg=='non']))
mat <- matrix(c(mean(Heigth[age=='50d'][5:9],na.rm=T),
mean(Heigth[age=='50d'][1:4],na.rm=T),
mean(Heigth[age=='50d'][15:19],na.rm=T),
mean(Heigth[age=='50d'][10:14],na.rm=T),
mean(Hgnon,na.rm=T),
mean(Hgoui,na.rm=T)),
nrow=2)

mH <- c(mean(Heigth[age=='50d'][5:9],na.rm=T),
mean(Heigth[age=='50d'][1:4],na.rm=T),
mean(Heigth[age=='50d'][15:19],na.rm=T),
mean(Heigth[age=='50d'][10:14],na.rm=T),
mean(Hgnon,na.rm=T),
mean(Hgoui,na.rm=T))

sdH <- c(sd(Heigth[age=='50d'][5:9],na.rm=T),
sd(Heigth[age=='50d'][1:4],na.rm=T),
sd(Heigth[age=='50d'][15:19],na.rm=T),
sd(Heigth[age=='50d'][10:14],na.rm=T),
sd(Hgnon,na.rm=T),
sd(Hgoui,na.rm=T))

sdx <- c(1,3,7,9,13,15)

# 5.86 >149mm
#  ?  > 71 mm


windows(2.79,4.1)

tiff(filename = "fig3new.tif", width = 8.4, height = 12.34,
     units = "cm", pointsize = 7,
     compression = c("none", "rle", "lzw", "jpeg", "zip"),
     bg = "white", res = 600,
     restoreConsole = TRUE)

par(mfrow=c(2,1))
par(font=1)
par(yaxs="i")
par(mar = c(3,5,1.5,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,200),cex.axis=1.3)
for (i in 1:16){
add.errorbars(x=sdx[i], y=mH[i],SE=sdH[i],direction='up')}
text(2,180,labels="*",font=2,cex=2.2)
text(8,140,labels="*",font=2,cex=2.2)
text(14,60,labels="*",font=2,cex=2.2)

par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,1.5,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,200),yaxp=c(0,200,5),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Mean height (mm)"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('a', side=3,line=0.5, font=2, cex=1.4,adj=0)


NFgoui <- as.numeric(as.vector(DT$nbr_feuilleg[surg=='oui']))
NFgnon <- as.numeric(as.vector(DT$nbr_feuilleg[surg=='non']))
matN <- matrix(c(mean(nbr_leaves[age=='50d'][5:9],na.rm=T),
mean(nbr_leaves[age=='50d'][1:4],na.rm=T),
mean(nbr_leaves[age=='50d'][15:19],na.rm=T),
mean(nbr_leaves[age=='50d'][10:14],na.rm=T),
mean(NFgnon,na.rm=T),
mean(NFgoui,na.rm=T)),
nrow=2)

mN <- c(mean(nbr_leaves[age=='50d'][5:9],na.rm=T),
mean(nbr_leaves[age=='50d'][1:4],na.rm=T),
mean(nbr_leaves[age=='50d'][15:19],na.rm=T),
mean(nbr_leaves[age=='50d'][10:14],na.rm=T),
mean(NFgnon,na.rm=T),
mean(NFgoui,na.rm=T))

sdN <- c(sd(nbr_leaves[age=='50d'][5:9],na.rm=T),
sd(nbr_leaves[age=='50d'][1:4],na.rm=T),
sd(nbr_leaves[age=='50d'][15:19],na.rm=T),
sd(nbr_leaves[age=='50d'][10:14],na.rm=T),
sd(NFgnon,na.rm=T),
sd(NFgoui,na.rm=T))

sdx <- c(1,3,7,9,13,15)

par(font=1)
par(yaxs="i")
par(mar = c(3,5,1.5,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,10),cex.axis=1.3)
for (i in 1:6){
add.errorbars(x=sdx[i], y=mN[i],SE=sdN[i],direction='up')}

par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,1.5,1) +0.1)
barplot(matN, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,10),yaxp=c(0,10,5),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Mean number of leaves"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('b', side=3,line=0.5, font=2, cex=1.4,adj=0)

dev.off()
dev.set()

