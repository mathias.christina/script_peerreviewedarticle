setwd("/Users/mathias/Documents/R/R_fallopia")
add.errorbars <- function(x,y,SE,direction,barlen=0.04){
if(direction=="up")arrows(x0=x, x1=x, y0=y, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="down")arrows(x0=x, x1=x, y0=y, y1=y - SE, code=3,
angle=90, length=barlen)
if(direction=="left")arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="right")arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="vert")arrows(x0=x, x1=x, y0=y-SE, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="hor")arrows(x0=x-SE, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
}
palette(grey(0:10/10))

datas <- read.csv2("biom_area_tot.csv",header=TRUE)
attach(datas)


mBiom1 <- c(mean(belowbiom_perg[type=='r1'][1:5],na.rm=T),
mean(belowbiom_perg[type=='r1'][6:9],na.rm=T),
mean(belowbiom_perg[type=='r2'][1:5],na.rm=T),
mean(belowbiom_perg[type=='r2'][6:10],na.rm=T),
mean(belowbiom_perg[type=='g'][1:5],na.rm=T),
mean(belowbiom_perg[type=='g'][6:10],na.rm=T))
mBiom2 <- c(mean(abovebiom_perg[type=='r1'][1:5],na.rm=T),
mean(abovebiom_perg[type=='r1'][6:9],na.rm=T),
mean(abovebiom_perg[type=='r2'][1:5],na.rm=T),
mean(abovebiom_perg[type=='r2'][6:10],na.rm=T),
mean(abovebiom_perg[type=='g'][1:5],na.rm=T),
mean(abovebiom_perg[type=='g'][6:10],na.rm=T))
mBiom3 <- c(mean(leafbiom_perg[type=='r1'][1:5],na.rm=T),
mean(leafbiom_perg[type=='r1'][6:9],na.rm=T),
mean(leafbiom_perg[type=='r2'][1:5],na.rm=T),
mean(leafbiom_perg[type=='r2'][6:10],na.rm=T),
mean(leafbiom_perg[type=='g'][1:5],na.rm=T),
mean(leafbiom_perg[type=='g'][6:10],na.rm=T))


sdBiom1 <- c(sd(belowbiom_perg[type=='r1'][1:5],na.rm=T),
sd(belowbiom_perg[type=='r1'][6:9],na.rm=T),
sd(belowbiom_perg[type=='r2'][1:5],na.rm=T),
#sd(belowbiom_perg[type=='r2'][6:10],na.rm=T),
0.09019335,
#sd(belowbiom_perg[type=='g'][1:5],na.rm=T),
0.005710312,
sd(belowbiom_perg[type=='g'][6:10],na.rm=T))
sdBiom2 <- c(
#sd(abovebiom_perg[type=='r1'][1:5],na.rm=T),
0.360571612,
sd(abovebiom_perg[type=='r1'][6:9],na.rm=T),
sd(abovebiom_perg[type=='r2'][1:5],na.rm=T),
sd(abovebiom_perg[type=='r2'][6:10],na.rm=T),
sd(abovebiom_perg[type=='g'][1:5],na.rm=T),
sd(abovebiom_perg[type=='g'][6:10],na.rm=T))
sdBiom3 <- c(sd(leafbiom_perg[type=='r1'][1:5],na.rm=T),
sd(leafbiom_perg[type=='r1'][6:9],na.rm=T),
sd(leafbiom_perg[type=='r2'][1:5],na.rm=T),
sd(leafbiom_perg[type=='r2'][6:10],na.rm=T),
sd(leafbiom_perg[type=='g'][1:5],na.rm=T),
sd(leafbiom_perg[type=='g'][6:10],na.rm=T))

sdx <- c(1,3,7,9,13,15)

windows(5.86,2)
tiff(filename = "fig4new.tif", width = 17.3, height = 5.49,
     units = "cm", pointsize = 7,
     compression = c("none", "rle", "lzw", "jpeg", "zip"),
     bg = "white", res = 600,
     restoreConsole = TRUE)

par(mfrow=c(1,3))
par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,1.4),cex.axis=1.3)
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom1[i],SE=sdBiom1[i],direction='up')}
text(2,1.3,labels="*",font=2,cex=2.2)
text(8,1.3,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom1,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,1.4),yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Belowground plant dry mass (g)"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('A', side=3,line=0.5, font=2, cex=1.4,adj=0)

par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,1.4),cex.axis=1.3)
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom2[i],SE=sdBiom2[i],direction='up')}
text(2,1.3,labels="*",font=2,cex=2.2)
text(8,1.3,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom2,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,1.4),yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Aboveground plant dry mass (g)"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('B', side=3,line=0.5, font=2, cex=1.4,adj=0)

par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0,1.2),cex.axis=1.3)
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom3[i],SE=sdBiom3[i],direction='up')}
text(2,1.12,labels="*",font=2,cex=2.2)
text(8,1,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom3,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0,1.2),yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Leaf plant dry mass (g)"),las=1,bty='o')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('C', side=3,line=0.5, font=2, cex=1.4,adj=0)

dev.off()
dev.set()



# avec echelle logarithmique
sdx <- c(1,3,7,9,13,15)

windows(5.86,2)
tiff(filename = "fig4new2.tif", width = 17.3, height = 5.49,
     units = "cm", pointsize = 7,
     compression = c("none", "rle", "lzw", "jpeg", "zip"),
     bg = "white", res = 600,
     restoreConsole = TRUE)

par(mfrow=c(1,3))
par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0.001,10),cex.axis=1.3,log='y')
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom1[i],SE=sdBiom1[i],direction='up')}
text(2,5,labels="*",font=2,cex=2.2)
text(8,5,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom1,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0.001,10),#yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Belowground plant dry mass (g)"),las=0,bty='o',log='y')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('A', side=3,line=0.5, font=2, cex=1.4,adj=0)

par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0.001,10),cex.axis=1.3,log='y')
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom2[i],SE=sdBiom2[i],direction='up')}
text(2,5,labels="*",font=2,cex=2.2)
text(8,5,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom2,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0.001,10),#,yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Aboveground plant dry mass (g)"),las=0,bty='o',log='y')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('B', side=3,line=0.5, font=2, cex=1.4,adj=0)

par(font=1)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
plot(x=1,y=1,col=0,pch=0,xlim=c(0,16), xaxp=c(0,16,18),
	ann=F,
	xaxt='n',yaxt='n',
	ylim = c(0.001,10),cex.axis=1.3,log='y')
for (i in 1:6){
add.errorbars(x=sdx[i], y=mBiom3[i],SE=sdBiom3[i],direction='up')}
text(2,5,labels="*",font=2,cex=2.2)
text(8,5,labels="*",font=2,cex=2.2)
text(14,0.2,labels="*",font=2,cex=2.2)

mat <- matrix(mBiom3,nrow=2)
par (new=TRUE)
par(yaxs="i")
par(mar = c(3,5,2,1) +0.1)
barplot(mat, beside=T, names.arg=c(c('Rhizomes 1','Rhizomes 2','Achenes')),
col=c(rep(c(5,9),3)),
ylim=c(0.001,10),#,yaxp=c(0,1.2,6),
cex.axis=1.3, cex.lab=1.6,cex.names=1.05,
ylab= expression("Leaf plant dry mass (g)"),las=0,bty='o',log='y')
legend("topright",legend=c("control","treatment"), col=c(5,9), pch=c(15,15),bty="n",cex=1.4)
mtext ('C', side=3,line=0.5, font=2, cex=1.4,adj=0)

dev.off()
dev.set()

