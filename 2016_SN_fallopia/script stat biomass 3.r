setwd("/Users/mathias/Documents/R/R_fallopia")

# �tude biomass

datas <- read.csv2("biom_area_tot.csv",header=TRUE)
attach(datas)

# type = type de propagule
# sureau = traitement
# belowbiom_perg = biomass sous terraine

levels(type)
[1] "g"  "r1" "r2"

# soit : "achenes", "rhizomes 1", "rhizomes"

ana1 <- aov(belowbiom_perg~ sureau*type)

summary(ana1)
            Df Sum Sq Mean Sq  F value    Pr(>F)
sureau       1 0.3182 0.31823  21.2571 0.0001229 ***
type         2 5.5943 2.79714 186.8413 5.995e-15 ***
sureau:type  2 0.1268 0.06338   4.2334 0.0271991 *
Residuals   23 0.3443 0.01497

# Je veux maintenant faire des contrates :
# je vais comparer g * <r1,r2> puis r1 � r2

contrasts(type) <- cbind(c(-2,1,1),c(0,-1,1))
     [,1] [,2]
g     -2    0
r1     1   -1
r2     1    1

ana11 <- aov(belowbiom_perg~ sureau*type)

summary.lm(ana11)

Coefficients:
                Estimate Std. Error t value Pr(>|t|)
(Intercept)      0.72295    0.03159  22.884  < 2e-16 ***
sureauoui       -0.19420    0.04560  -4.259 0.000295 ***   # 1
type1            0.35209    0.02234  15.761 8.07e-14 ***   # 2
type2            0.01506    0.03869   0.389 0.700688       # 3
sureauoui:type1 -0.09193    0.03192  -2.880 0.008451 **    # 4
sureauoui:type2  0.02895    0.05640   0.513 0.612656       # 5

# 1 effet traitement
# 2 belowbiom_perg diff�re entre g et <r1,r2>
# 3 r1 et r2 ne diff�re pas
# 4 l'effet traitement est diff�rent entre g et les rhizomes
# 5 l'effet traitement est identique pour les deux rhizomes

contrasts(type) <- cbind(c(0,1,0),c(0,0,1))
ana2 <- aov(abovebiom_perg~ sureau*type)
contrasts(type) <- cbind(c(-2,1,1),c(0,-1,1))
ana21 <- aov(abovebiom_perg~ sureau*type)

summary(ana2)
            Df Sum Sq Mean Sq F value    Pr(>F)
sureau       1 0.5607 0.56068  8.7263  0.007117 **
type         2 3.5215 1.76074 27.4040 8.186e-07 ***
sureau:type  2 0.2477 0.12384  1.9275  0.168300
Residuals   23 1.4778 0.06425

summary.lm(ana21)
                Estimate Std. Error t value Pr(>|t|)
(Intercept)      0.64294    0.06545   9.824 1.07e-09 ***
sureauoui       -0.26700    0.09447  -2.826  0.00957 **
type1            0.30397    0.04628   6.568 1.06e-06 ***
type2           -0.05829    0.08016  -0.727  0.47446
sureauoui:type1 -0.12283    0.06613  -1.858  0.07608 .
sureauoui:type2  0.08179    0.11685   0.700  0.49097

contrasts(type) <- cbind(c(0,1,0),c(0,0,1))
ana3 <- aov(leafbiom_perg~ sureau*type)
contrasts(type) <- cbind(c(-2,1,1),c(0,-1,1))
ana31 <- aov(leafbiom_perg~ sureau*type)

summary(ana3)
            Df  Sum Sq Mean Sq F value    Pr(>F)
sureau       1 0.31204 0.31204  8.6181  0.007429 **
type         2 2.14326 1.07163 29.5966 4.358e-07 ***
sureau:type  2 0.12734 0.06367  1.7584  0.194697
Residuals   23 0.83278 0.03621

summary.lm(ana31)
                Estimate Std. Error t value Pr(>|t|)
(Intercept)      0.49226    0.04913  10.019 7.37e-10 ***
sureauoui       -0.19836    0.07091  -2.797   0.0102 *
type1            0.23538    0.03474   6.775 6.55e-07 ***
type2           -0.02032    0.06017  -0.338   0.7387
sureauoui:type1 -0.09160    0.04964  -1.845   0.0779 .
sureauoui:type2  0.03492    0.08772   0.398   0.6942

