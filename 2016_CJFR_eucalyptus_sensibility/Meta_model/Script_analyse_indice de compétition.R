# script pour estimer l'indice de compétition

set<-c("C:/Users/mathias/Documents/R/R_Maespa_thèse/Script Maespa EUCFLUX sensibilité/Maespa_cluster")
setwd(set)

require(Maeswrap)

#plot1
POS1 <- readPAR('0001_trees.dat','xycoords',namelist='xy')
POS2 <- readPAR('0004_trees.dat','xycoords',namelist='xy')
POS3 <- readPAR('0007_trees.dat','xycoords',namelist='xy')
POS4 <- readPAR('0010_trees.dat','xycoords',namelist='xy')

CAP1 <- readPAR('0001_trees.dat','values',namelist='indivdiam')*pi*100
CAP2 <- readPAR('0004_trees.dat','values',namelist='indivdiam')*pi*100
CAP3 <- readPAR('0007_trees.dat','values',namelist='indivdiam')*pi*100
CAP4 <- readPAR('0010_trees.dat','values',namelist='indivdiam')*pi*100

X1 <- POS1[((0:363)*2+1)]
Y1 <- POS1[((0:363)*2+2)]
X2 <- POS2[((0:363)*2+1)]
Y2 <- POS2[((0:363)*2+2)]
X3 <- POS3[((0:353)*2+1)]
Y3 <- POS3[((0:353)*2+2)]
X4 <- POS4[((0:363)*2+1)]
Y4 <- POS4[((0:363)*2+2)]

cap1 <- CAP1[((0:363)*11+1)]
cap2 <- CAP2[((0:363)*11+1)]
cap3 <- CAP3[((0:353)*11+1)]
cap4 <- CAP4[((0:363)*11+1)]
for (i in 2:11){
cap1 <- data.frame(cap1,CAP1[((0:363)*11+i)])
cap2 <- data.frame(cap2,CAP2[((0:363)*11+i)])
cap3 <- data.frame(cap3,CAP3[((0:353)*11+i)])
cap4 <- data.frame(cap4,CAP4[((0:363)*11+i)])
}
cap1 <- replace(cap1,cap1==0,NA)
cap2 <- replace(cap2,cap4==0,NA)
cap3 <- replace(cap3,cap3==0,NA)
cap4 <- replace(cap4,cap2==0,NA)

# maintenant calculons les indice de compétitions

DIST1 <- sqrt((X1[1] - X1)^2 + (Y1[1] - Y1)^2)
DIST1 <- replace(DIST1,c(DIST1==0),NA)
IDTREE1 <- c() ; DISTTABLE1 <- c()
DIST2 <- sqrt((X2[1] - X2)^2 + (Y2[1] - Y2)^2)
DIST2 <- replace(DIST2,c(DIST2==0),NA)
IDTREE2 <- c() ; DISTTABLE2 <- c()
DIST3 <- sqrt((X3[1] - X3)^2 + (Y3[1] - Y3)^2)
DIST3 <- replace(DIST3,c(DIST3==0),NA)
IDTREE3 <- c() ; DISTTABLE3 <- c()
DIST4 <- sqrt((X4[1] - X4)^2 + (Y4[1] - Y4)^2)
DIST4 <- replace(DIST4,c(DIST4==0),NA)
IDTREE4 <- c() ; DISTTABLE4 <- c()

for (i in 1:8){
IDTREE1 <- c(IDTREE1, which(DIST1==min(DIST1,na.rm=T)))
DISTTABLE1 <- c(DISTTABLE1, DIST1[which(DIST1==min(DIST1,na.rm=T))])
DIST1[IDTREE1] <- NA
IDTREE2 <- c(IDTREE2, which(DIST2==min(DIST2,na.rm=T)))
DISTTABLE2 <- c(DISTTABLE2, DIST2[which(DIST2==min(DIST2,na.rm=T))])
DIST2[IDTREE2] <- NA
IDTREE3 <- c(IDTREE3, which(DIST3==min(DIST3,na.rm=T)))
DISTTABLE3 <- c(DISTTABLE3, DIST3[which(DIST3==min(DIST3,na.rm=T))])
DIST3[IDTREE3] <- NA
IDTREE4 <- c(IDTREE4, which(DIST4==min(DIST4,na.rm=T)))
DISTTABLE4 <- c(DISTTABLE4, DIST4[which(DIST4==min(DIST4,na.rm=T))])
DIST4[IDTREE4] <- NA
}
IDTREE1 <- IDTREE1[1:8]
DISTTABLE1 <- DISTTABLE1[1:8]
IDTREE2 <- IDTREE2[1:8]
DISTTABLE2 <- DISTTABLE2[1:8]
IDTREE3 <- IDTREE3[1:8]
DISTTABLE3 <- DISTTABLE3[1:8]
IDTREE4 <- IDTREE4[1:8]
DISTTABLE4 <- DISTTABLE4[1:8]


for (i in 2:364){
DIST12 <- sqrt((X1[i] - X1)^2 + (Y1[i] - Y1)^2)
DIST12 <- replace(DIST12,c(DIST12==0),NA)
IDTREE12 <- c();DISTTABLE12 <- c()
DIST22 <- sqrt((X2[i] - X2)^2 + (Y2[i] - Y2)^2)
DIST22 <- replace(DIST22,c(DIST22==0),NA)
IDTREE22 <- c();DISTTABLE22 <- c()
DIST42 <- sqrt((X2[i] - X2)^2 + (Y2[i] - Y2)^2)
DIST42 <- replace(DIST42,c(DIST42==0),NA)
IDTREE42 <- c();DISTTABLE42 <- c()
for (i in 1:8){
IDTREE12 <- c(IDTREE12, which(DIST12==min(DIST12,na.rm=T)))
DISTTABLE12 <- c(DISTTABLE12, DIST12[which(DIST12==min(DIST12,na.rm=T))])
DIST12[IDTREE12] <- NA
IDTREE22 <- c(IDTREE22, which(DIST22==min(DIST22,na.rm=T)))
DISTTABLE22 <- c(DISTTABLE22, DIST22[which(DIST22==min(DIST22,na.rm=T))])
DIST22[IDTREE22] <- NA
IDTREE42 <- c(IDTREE42, which(DIST42==min(DIST42,na.rm=T)))
DISTTABLE42 <- c(DISTTABLE42, DIST42[which(DIST42==min(DIST42,na.rm=T))])
DIST42[IDTREE42] <- NA
}
IDTREE1 <- data.frame(IDTREE1,IDTREE12[1:8])
DISTTABLE1 <- data.frame(DISTTABLE1,DISTTABLE12[1:8])
IDTREE2 <- data.frame(IDTREE2,IDTREE22[1:8])
DISTTABLE2 <- data.frame(DISTTABLE2,DISTTABLE22[1:8])
IDTREE4 <- data.frame(IDTREE4,IDTREE42[1:8])
DISTTABLE4 <- data.frame(DISTTABLE4,DISTTABLE42[1:8])}

for (i in 2:354){
DIST32 <- sqrt((X2[i] - X2)^2 + (Y2[i] - Y2)^2)
DIST32 <- replace(DIST32,c(DIST32==0),NA)
IDTREE32 <- c();DISTTABLE32 <- c()
for (i in 1:8){
IDTREE32 <- c(IDTREE32, which(DIST32==min(DIST32,na.rm=T)))
DISTTABLE32 <- c(DISTTABLE32, DIST32[which(DIST32==min(DIST32,na.rm=T))])
DIST32[IDTREE32] <- NA
}
IDTREE3 <- data.frame(IDTREE3,IDTREE32[1:8])
DISTTABLE3 <- data.frame(DISTTABLE3,DISTTABLE32[1:8])}

# on calcule l'indice de comptétion
cap1 <- replace(cap1,is.na(cap1),0)
cap2 <- replace(cap2,is.na(cap2),0)
cap3 <- replace(cap3,is.na(cap3),0)
cap4 <- replace(cap4,is.na(cap4),0)
dia1 <- cap1/pi
dia2 <- cap2/pi
dia3 <- cap3/pi
dia4 <- cap4/pi


IC1 <- sum(dia1[IDTREE1[,1],1]/(dia1[1,1]*DISTTABLE1[,1]))
IC2 <- sum(dia2[IDTREE2[,1],1]/(dia2[1,1]*DISTTABLE2[,1]))
IC3 <- sum(dia3[IDTREE3[,1],1]/(dia3[1,1]*DISTTABLE3[,1]))
IC4 <- sum(dia4[IDTREE4[,1],1]/(dia4[1,1]*DISTTABLE4[,1]))
for (i in 2:364){
IC1 <- c(IC1,sum(dia1[IDTREE1[,i],1]/(dia1[i,1]*DISTTABLE1[,i])))
IC2 <- c(IC2,sum(dia2[IDTREE2[,i],1]/(dia2[i,1]*DISTTABLE2[,i])))
IC4 <- c(IC4,sum(dia4[IDTREE4[,i],1]/(dia4[i,1]*DISTTABLE4[,i])))
}
for (i in 2:354){
IC3 <- c(IC3,sum(dia3[IDTREE3[,i],1]/(dia3[i,1]*DISTTABLE3[,i])))
}

for (j in 2:11){
IC12 <- sum(dia1[IDTREE1[,1],j]/(dia1[1,j]*DISTTABLE1[,1]))
IC22 <- sum(dia2[IDTREE2[,1],j]/(dia2[1,j]*DISTTABLE2[,1]))
IC32 <- sum(dia3[IDTREE3[,1],j]/(dia3[1,j]*DISTTABLE3[,1]))
IC42 <- sum(dia4[IDTREE4[,1],j]/(dia4[1,j]*DISTTABLE4[,1]))
for (i in 2:364){
IC12 <- c(IC12,sum(dia1[IDTREE1[,i],j]/(dia1[i,j]*DISTTABLE1[,i])))
IC22 <- c(IC22,sum(dia2[IDTREE2[,i],j]/(dia2[i,j]*DISTTABLE2[,i])))
IC42 <- c(IC42,sum(dia4[IDTREE4[,i],j]/(dia4[i,j]*DISTTABLE4[,i])))
}
for (i in 2:354){
IC32 <- c(IC32,sum(dia3[IDTREE3[,i],j]/(dia3[i,j]*DISTTABLE3[,i])))
}

IC1 <- data.frame(IC1,IC12)
IC2 <- data.frame(IC2,IC22)
IC3 <- data.frame(IC3,IC32)
IC4 <- data.frame(IC4,IC42)}

k=1
ICb1 <- IC1[,k] ;diab1 <- dia1[,k]
ICb2 <- IC2[,k] ;diab2 <- dia2[,k]
ICb3 <- IC3[,k] ;diab3 <- dia3[,k]
ICb4 <- IC4[,k] ;diab4 <- dia4[,k]
for (i in (k+1):11){
ICb1 <- c(ICb1,IC1[,i]) ; diab1 <- c(diab1,dia1[,i])
ICb2 <- c(ICb2,IC2[,i]) ; diab2 <- c(diab2,dia2[,i])
ICb3 <- c(ICb3,IC3[,i]) ; diab3 <- c(diab3,dia3[,i])
ICb4 <- c(ICb4,IC4[,i]) ; diab4 <- c(diab4,dia4[,i])
}

plot(diab1,ICb1)
points(diab2,ICb2,col=2)
points(diab3,ICb3,col=3)
points(diab4,ICb4,col=4)

IC <- c(ICb2,ICb3,ICb4)
DIA <- c(diab2,diab3,diab4)
# infini regarder pourquoi ?
IC <- replace(IC,IC>20,NA)
DIA <- replace(DIA,DIA==0,NA)


fit1 <- nls(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=2))
fit2 <- nls(IC~exp(-b*DIA+d),start=c(b=1,d=2))
fit3 <- nls(IC~exp(-b*DIA),start=c(b=1))
fit4 <- nls(IC~a+exp(-b*DIA),start=c(a=2,b=2),lower=c(a=0,b=0.0001),algorithm='port')

X <- seq(0,50,length.out=100)
y1 <- summary(fit1)$coef[1,1] +exp(-summary(fit1)$coef[2,1]*X + summary(fit1)$coef[3,1] )
y2 <- exp(-summary(fit2)$coef[1,1]*X + summary(fit2)$coef[2,1] )
y3 <- exp(-summary(fit3)$coef[1]*X  )
y4 <- summary(fit4)$coef[1,1] +exp(-summary(fit4)$coef[2,1]*X )

plot(DIA,IC)
points(X,y1,type='l',lwd=2,col=2)
points(X,y2,type='l',lwd=2,col=3)
points(X,y3,type='l',lwd=2,col=4)
points(X,y4,type='l',lwd=2,col=5)


require(quantreg)
IC <- c(ICb2,ICb3,ICb4)
DIA <- c(diab2,diab3,diab4)
# infini regarder pourquoi ?
IC <- replace(IC,IC>20,NA)
DIA <- replace(DIA,DIA==0,NA)
fit1 <- nls(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=2))
fit11 <- nlrq(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=2),tau=0.99)
fit12 <- nlrq(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=2),tau=0.01)

X <- seq(0,50,length.out=1000)
y1 <- summary(fit1)$coef[1,1] +exp(-summary(fit1)$coef[2,1]*X + summary(fit1)$coef[3,1] )
y11 <- summary(fit11)$coef[1,1] +exp(-summary(fit11)$coef[2,1]*X + summary(fit11)$coef[3,1] )
y12 <- summary(fit12)$coef[1,1] +exp(-summary(fit12)$coef[2,1]*X + summary(fit12)$coef[3,1] )

par(mar=c(4,4,1,1))
plot(DIA,IC,mgp=c(2.5,1,0),cex=1.2,ylab='Competition Index',xlab='DBH (cm)',cex.lab=1.2)
points(X,y1,type='l',lwd=2,col=2)
points(X,y11,type='l',lwd=2,col=2,lty=2)
points(X,y12,type='l',lwd=2,col=2,lty=2)
legend('topright',legend=c('Y = a*exp(-b*X+c)','99% quantile interval'),lty=1:2,col=2,pch=NA,bty='n',cex=1.2)


par(mfrow=c(2,3))
for (i in 1:6){
IC <- c(ICb2[((i-1)*364+1):(i*364)],ICb3[((i-1)*354+1):(i*354)],ICb4[((i-1)*364+1):(i*364)])
DIA <- c(diab2[((i-1)*364+1):(i*364)],diab3[((i-1)*354+1):(i*354)],diab4[((i-1)*364+1):(i*364)])
IC <- replace(IC,IC>20,NA)
DIA <- replace(DIA,DIA==0,NA)
fit1 <- nls(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=3))
fit11 <- nlrq(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=3),tau=0.99)
fit12 <- nlrq(IC~a+exp(-b*DIA+d),start=c(a=0,b=1,d=3),tau=0.01)
X <- seq(0,50,length.out=1000)
y1 <- summary(fit1)$coef[1,1] +exp(-summary(fit1)$coef[2,1]*X + summary(fit1)$coef[3,1] )
y11 <- summary(fit11)$coef[1,1] +exp(-summary(fit11)$coef[2,1]*X + summary(fit11)$coef[3,1] )
y12 <- summary(fit12)$coef[1,1] +exp(-summary(fit12)$coef[2,1]*X + summary(fit12)$coef[3,1] )

par(mar=c(4,4,1,1))
plot(DIA,IC,mgp=c(2.5,1,0),cex=1.2,ylab='Competition Index',xlab='DBH (cm)')
points(X,y1,type='l',lwd=2,col=2)
points(X,y11,type='l',lwd=2,col=2,lty=2)
points(X,y12,type='l',lwd=2,col=2,lty=2)
legend('topright',legend=c('Y = a*exp(-b*X+c)','99% quantile interval'),lty=1:2,col=2,pch=NA,bty='n',cex=1.2)
}

# on normalise CAP pour avoir une bonne répartition quelque soir l'age
IC <- c(ICb1,ICb2,ICb3,ICb4)
diab1 <- replace(diab1,c(diab1==0), NA)
diab2 <- replace(diab2,c(diab2==0), NA)
diab3 <- replace(diab3,c(diab3==0), NA)
diab4 <- replace(diab4,c(diab4==0), NA)

DIAnorm <-c()
for (i in 1:11){
CA1 <- (diab1[((i-1)*364+1):(i*364)] - min(diab1[((i-1)*364+1):(i*364)],na.rm=T))/(max(diab1[((i-1)*364+1):(i*364)],na.rm=T)-min(diab1[((i-1)*364+1):(i*364)],na.rm=T))
DIAnorm <- c(DIAnorm,CA1)}
for (i in 1:11){
CA2 <- (diab2[((i-1)*364+1):(i*364)] - min(diab2[((i-1)*364+1):(i*364)],na.rm=T))/(max(diab2[((i-1)*364+1):(i*364)],na.rm=T)-min(diab2[((i-1)*364+1):(i*364)],na.rm=T))
DIAnorm <- c(DIAnorm,CA2)}
for (i in 1:11){
CA3 <- (diab3[((i-1)*354+1):(i*354)] - min(diab3[((i-1)*354+1):(i*354)],na.rm=T))/(max(diab3[((i-1)*354+1):(i*354)],na.rm=T)-min(diab1[((i-1)*354+1):(i*354)],na.rm=T))
DIAnorm <- c(DIAnorm,CA3)}
for (i in 1:11){
CA4 <- (diab4[((i-1)*364+1):(i*364)] - min(diab4[((i-1)*364+1):(i*364)],na.rm=T))/(max(diab4[((i-1)*364+1):(i*364)],na.rm=T)-min(diab1[((i-1)*364+1):(i*364)],na.rm=T))
DIAnorm <- c(DIAnorm,CA4)}

IC <- replace(IC,IC>20,NA)
DIAnorm <- replace(DIAnorm,DIAnorm==0,NA)



fit1 <- nls(IC~a+exp(-b*DIAnorm+d),start=c(a=0,b=1,d=2))
fit11 <- nlrq(IC~a+exp(-b*DIAnorm+d),start=c(a=0,b=1,d=2),tau=0.99)
fit12 <- nlrq(IC~a+exp(-b*DIAnorm+d),start=c(a=0,b=1,d=2),tau=0.01)
X <- seq(0,1,length.out=1000)
y1 <- summary(fit1)$coef[1,1] +exp(-summary(fit1)$coef[2,1]*X + summary(fit1)$coef[3,1] )
y11 <- summary(fit11)$coef[1,1] +exp(-summary(fit11)$coef[2,1]*X + summary(fit11)$coef[3,1] )
y12 <- summary(fit12)$coef[1,1] +exp(-summary(fit12)$coef[2,1]*X + summary(fit12)$coef[3,1] )

par(mar=c(4,4,1,1))
plot(DIAnorm,IC,mgp=c(2.5,1,0),cex=0.8,ylab='Competition Index',xlab='DBH normalisé',cex.lab=1.2)
points(X,y1,type='l',lwd=2,col=2)
points(X,y11,type='l',lwd=2,col=2,lty=2)
points(X,y12,type='l',lwd=2,col=2,lty=2)
legend('topright',legend=c('Y = a*exp(-b*X+c)','99% quantile interval'),lty=1:2,col=2,pch=NA,bty='n',cex=1.2)




