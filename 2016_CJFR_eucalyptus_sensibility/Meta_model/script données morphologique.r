set<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
setwd(set)


dat <- read.table('Donn�es_compl�tes.txt',header=T)
datb <- read.table('datas_completa_bordadura.txt',header=T)

# parcelle 1

dat01 <- dat$dat_Date[dat$dat_Parcelle==1]
cap01 <- dat$dat_CAP[dat$dat_Parcelle==1]
alt01 <- dat$dat_Alt[dat$dat_Parcelle==1]
alc01 <- dat$dat_Alcopa[dat$dat_Parcelle==1]
R01 <- dat$dat_R[dat$dat_Parcelle==1]
Rel01 <- dat$dat_Rel[dat$dat_Parcelle==1]
area01 <- dat$dat_Area[dat$dat_Parcelle==1]

dtb01 <- datb$datb[datb$Parcelle==1]
ligne01 <- datb$ligne[datb$Parcelle==1]
interligne01 <- datb$interligne[datb$Parcelle==1]
altb01 <- datb$Alb[datb$Parcelle==1]
  alcb01 <- datb$Alcopb[datb$Parcelle==1]
capb01 <- datb$Capb[datb$Parcelle==1]
Rb01 <- datb$Rb[datb$Parcelle==1]
Relb01 <- datb$Relb[datb$Parcelle==1]
areab01 <- datb$Areab[datb$Parcelle==1]

# on retire les deux premi�res lignes et les deux derni�res (sur 18) et la premi�re et derni�re interligne (sur 28)

Alb01 <- c() ; Alcb01<-c();Capb01<-c(); rb01<-c();relb01<-c(); Areab01<-c()
for (i in 1:length(altb01)){
if (ligne01[i] == '4b'|ligne01[i]=='3b'|ligne01[i]==14|ligne01[i]==15|interligne01[i]==4|interligne01[i]==31){
Alb01 <- Alb01 ; Alcb01 <- Alcb01 ; Capb01 <- Capb01; rb01<-rb01; relb01 <- relb01; Areab01 <- Areab01}
else{
Alb01 <- c(Alb01,altb01[i])
Alcb01 <- c(Alcb01,alcb01[i])
Capb01 <- c(Capb01,capb01[i])
rb01 <- c(rb01,Rb01[i])
relb01 <- c(relb01,Relb01[i])
Areab01 <- c(Areab01,areab01[i])
}}

# on doit maintenant combiner
Areatot_1 <- c() ; Altot_1 <-c(); Alctot_1<-c();CAPtot_1<-c();Rtot_1<-c();Reltot_1<-c()

for (j in 1:14){

Alb1 <- Alb01[((j-1)*280+1) : (j*280)]
Alcb1 <- Alcb01[((j-1)*280+1) : (j*280)]
Capb1 <- Capb01[((j-1)*280+1) : (j*280)]
rb1 <- rb01[((j-1)*280+1) : (j*280)]
relb1 <- relb01[((j-1)*280+1) : (j*280)]
Areab1 <- Areab01[((j-1)*280+1) : (j*280)]
cap1 <- cap01[((j-1)*84+1) : (j*84)]
alt1 <- alt01[((j-1)*84+1) : (j*84)]
alc1 <- alc01[((j-1)*84+1) : (j*84)]
R1 <- R01[((j-1)*84+1) : (j*84)]
Rel1 <- Rel01[((j-1)*84+1) : (j*84)]
area1 <- area01[((j-1)*84+1) : (j*84)]


B =26*4
Alini <- c(Alb1[1:B])
Alent <- c(
            Alb1[(B+1): (B+6)], alt1[13:1],alt1[84], Alb1[(B+6+1):(B+2*6)],
            Alb1[(B+2*6+1): (B+3*6)], alt1[14:(2*14-1)], Alb1[(B+3*6+1):(B+4*6)],
            Alb1[(B+4*6+1): (B+5*6)], alt1[(3*14-1):(2*14)], Alb1[(B+5*6+1):(B+6*6)],
            Alb1[(B+6*6+1): (B+7*6)], alt1[(3*14):(4*14-1)], Alb1[(B+7*6+1):(B+8*6)],
            Alb1[(B+8*6+1): (B+9*6)], alt1[(5*14-1):(4*14)], Alb1[(B+9*6+1):(B+10*6)],
            Alb1[(B+10*6+1): (B+11*6)], alt1[(5*14):(6*14-1)], Alb1[(B+11*6+1):(B+12*6)])
Alfin <- Alb1[(B+12*6+1):280]
Altot1 <- c(Alini, Alent,Alfin)
Acini <- c(Alcb1[1:B])
Acent <- c(
            Alcb1[(B+1): (B+6)], alc1[13:1],alc1[84], Alcb1[(B+6+1):(B+2*6)],
            Alcb1[(B+2*6+1): (B+3*6)], alc1[14:(2*14-1)], Alcb1[(B+3*6+1):(B+4*6)],
            Alcb1[(B+4*6+1): (B+5*6)], alc1[(3*14-1):(2*14)], Alcb1[(B+5*6+1):(B+6*6)],
            Alcb1[(B+6*6+1): (B+7*6)], alc1[(3*14):(4*14-1)], Alcb1[(B+7*6+1):(B+8*6)],
            Alcb1[(B+8*6+1): (B+9*6)], alc1[(5*14-1):(4*14)], Alcb1[(B+9*6+1):(B+10*6)],
            Alcb1[(B+10*6+1): (B+11*6)], alc1[(5*14):(6*14-1)], Alcb1[(B+11*6+1):(B+12*6)])
Acfin <- Alcb1[(B+12*6+1):280]
Actot1 <- c(Acini, Acent,Acfin)
capini <- c(Capb1[1:B])
capent <- c(
            Capb1[(B+1): (B+6)], cap1[13:1],cap1[84], Capb1[(B+6+1):(B+2*6)],
            Capb1[(B+2*6+1): (B+3*6)], cap1[14:(2*14-1)], Capb1[(B+3*6+1):(B+4*6)],
            Capb1[(B+4*6+1): (B+5*6)], cap1[(3*14-1):(2*14)], Capb1[(B+5*6+1):(B+6*6)],
            Capb1[(B+6*6+1): (B+7*6)], cap1[(3*14):(4*14-1)], Capb1[(B+7*6+1):(B+8*6)],
            Capb1[(B+8*6+1): (B+9*6)], cap1[(5*14-1):(4*14)], Capb1[(B+9*6+1):(B+10*6)],
            Capb1[(B+10*6+1): (B+11*6)], cap1[(5*14):(6*14-1)], Capb1[(B+11*6+1):(B+12*6)])
capfin <- Capb1[(B+12*6+1):280]
captot1 <- c(capini, capent,capfin)
Rini <- c(rb1[1:B])
Rent <- c(
            rb1[(B+1): (B+6)], R1[13:1],R1[84], rb1[(B+6+1):(B+2*6)],
            rb1[(B+2*6+1): (B+3*6)], R1[14:(2*14-1)], rb1[(B+3*6+1):(B+4*6)],
            rb1[(B+4*6+1): (B+5*6)], R1[(3*14-1):(2*14)], rb1[(B+5*6+1):(B+6*6)],
            rb1[(B+6*6+1): (B+7*6)], R1[(3*14):(4*14-1)], rb1[(B+7*6+1):(B+8*6)],
            rb1[(B+8*6+1): (B+9*6)], R1[(5*14-1):(4*14)], rb1[(B+9*6+1):(B+10*6)],
            rb1[(B+10*6+1): (B+11*6)], R1[(5*14):(6*14-1)], rb1[(B+11*6+1):(B+12*6)])
Rfin <- rb1[(B+12*6+1):280]
Rtot1 <- c(Rini, Rent,Rfin)
Relini <- c(relb1[1:B])
Relent <- c(
            relb1[(B+1): (B+6)], Rel1[13:1],Rel1[84], relb1[(B+6+1):(B+2*6)],
            relb1[(B+2*6+1): (B+3*6)], Rel1[14:(2*14-1)], relb1[(B+3*6+1):(B+4*6)],
            relb1[(B+4*6+1): (B+5*6)], Rel1[(3*14-1):(2*14)], relb1[(B+5*6+1):(B+6*6)],
            relb1[(B+6*6+1): (B+7*6)], Rel1[(3*14):(4*14-1)], relb1[(B+7*6+1):(B+8*6)],
            relb1[(B+8*6+1): (B+9*6)], Rel1[(5*14-1):(4*14)], relb1[(B+9*6+1):(B+10*6)],
            relb1[(B+10*6+1): (B+11*6)], Rel1[(5*14):(6*14-1)], relb1[(B+11*6+1):(B+12*6)])
Relfin <- relb1[(B+12*6+1):280]
Reltot1 <- c(Relini, Relent,Relfin)
Areaini <- c(Areab1[1:B])
Areaent <- c(
            Areab1[(B+1): (B+6)], area1[13:1],area1[84], Areab1[(B+6+1):(B+2*6)],
            Areab1[(B+2*6+1): (B+3*6)], area1[14:(2*14-1)], Areab1[(B+3*6+1):(B+4*6)],
            Areab1[(B+4*6+1): (B+5*6)], area1[(3*14-1):(2*14)], Areab1[(B+5*6+1):(B+6*6)],
            Areab1[(B+6*6+1): (B+7*6)], area1[(3*14):(4*14-1)], Areab1[(B+7*6+1):(B+8*6)],
            Areab1[(B+8*6+1): (B+9*6)], area1[(5*14-1):(4*14)], Areab1[(B+9*6+1):(B+10*6)],
            Areab1[(B+10*6+1): (B+11*6)], area1[(5*14):(6*14-1)], Areab1[(B+11*6+1):(B+12*6)])
Areafin <- Areab1[(B+12*6+1):280]
Areatot1 <- c(Areaini, Areaent,Areafin)

# on remet dans le bon sens maintenant (en zig zag)

Ar1 <- c(); Ca1 <- c() ; Al1 <-c(); Ac1<-c(); Ra1<-c(); Rae1 <- c()
for (i in 1:7){
Ar1 <- c(Ar1, Areatot1[(26*(2*i-1)): (26*(2*i-2)+1)], Areatot1[(26*(2*i-1)+1):(26*(2*i))])
Ca1 <- c(Ca1, captot1[(26*(2*i-1)): (26*(2*i-2)+1)], captot1[(26*(2*i-1)+1):(26*(2*i))])
Al1 <- c(Al1, Altot1[(26*(2*i-1)): (26*(2*i-2)+1)], Altot1[(26*(2*i-1)+1):(26*(2*i))])
Ac1 <- c(Ac1, Actot1[(26*(2*i-1)): (26*(2*i-2)+1)], Actot1[(26*(2*i-1)+1):(26*(2*i))])
Ra1 <- c(Ra1, Rtot1[(26*(2*i-1)): (26*(2*i-2)+1)], Rtot1[(26*(2*i-1)+1):(26*(2*i))])
Rae1 <- c(Rae1, Reltot1[(26*(2*i-1)): (26*(2*i-2)+1)], Reltot1[(26*(2*i-1)+1):(26*(2*i))])
}
 
Areatot_1 <- c(Areatot_1, Ar1)
Altot_1 <-c(Altot_1, Al1)
Alctot_1<-c(Alctot_1,Ac1)
CAPtot_1<-c(CAPtot_1, Ca1)
Rtot_1<-c(Rtot_1, Ra1)
Reltot_1<-c(Reltot_1, Rae1)
}


# parcelle 2

dat02 <- dat$dat_Date[dat$dat_Parcelle==2]
cap02 <- dat$dat_CAP[dat$dat_Parcelle==2]
alt02 <- dat$dat_Alt[dat$dat_Parcelle==2]
alc02 <- dat$dat_Alcopa[dat$dat_Parcelle==2]
R02 <- dat$dat_R[dat$dat_Parcelle==2]
Rel02 <- dat$dat_Rel[dat$dat_Parcelle==2]
area02 <- dat$dat_Area[dat$dat_Parcelle==2]

dtb02 <- datb$datb[datb$Parcelle==2]
ligne02 <- datb$ligne[datb$Parcelle==2]
interligne02 <- datb$interligne[datb$Parcelle==2]
altb02 <- datb$Alb[datb$Parcelle==2]
alcb02 <- datb$Alcopb[datb$Parcelle==2]
capb02 <- datb$Capb[datb$Parcelle==2]
Rb02 <- datb$Rb[datb$Parcelle==2]
Relb02 <- datb$Relb[datb$Parcelle==2]
areab02 <- datb$Areab[datb$Parcelle==2]

# on retire les deux premi�res lignes et les deux derni�res (sur 18) et 2 interlignes de chaque cot� (sur 30)

Alb02 <- c() ; Alcb02<-c();Capb02<-c(); rb02<-c();relb02<-c(); Areab02<-c()
for (i in 1:length(altb02)){
if (ligne02[i] == 8|ligne02[i]==9|ligne02[i]==24|ligne02[i]==25|interligne02[i]==3|interligne02[i]==4|interligne02[i]==31|interligne02[i]==32){
Alb02 <- Alb02 ; Alcb02 <- Alcb02 ; Capb02 <- Capb02; rb02<-rb02; relb02 <- relb02; Areab02 <- Areab02}
else{
Alb02 <- c(Alb02,altb02[i])
Alcb02 <- c(Alcb02,alcb02[i])
Capb02 <- c(Capb02,capb02[i])
rb02 <- c(rb02,Rb02[i])
relb02 <- c(relb02,Relb02[i])
Areab02 <- c(Areab02,areab02[i])
}}

# on doit maintenant combiner
Areatot_2 <- c() ; Altot_2 <-c(); Alctot_2<-c();CAPtot_2<-c();Rtot_2<-c();Reltot_2<-c()

for (j in 1:14){

Alb2 <- Alb02[((j-1)*280+1) : (j*280)]
Alcb2 <- Alcb02[((j-1)*280+1) : (j*280)]
Capb2 <- Capb02[((j-1)*280+1) : (j*280)]
rb2 <- rb02[((j-1)*280+1) : (j*280)]
relb2 <- relb02[((j-1)*280+1) : (j*280)]
Areab2 <- Areab02[((j-1)*280+1) : (j*280)]
cap2 <- cap02[((j-1)*84+1) : (j*84)]
alt2 <- alt02[((j-1)*84+1) : (j*84)]
alc2 <- alc02[((j-1)*84+1) : (j*84)]
R2 <- R02[((j-1)*84+1) : (j*84)]
Rel2 <- Rel02[((j-1)*84+1) : (j*84)]
area2 <- area02[((j-1)*84+1) : (j*84)]


B =26*4
Alini <- c(Alb2[1:B])
Alent <- c(
            Alb2[(B+1): (B+6)], alt2[1:14], Alb2[(B+6+1):(B+2*6)],
            Alb2[(B+2*6+1): (B+3*6)], alt2[(2*14):(1*14+1)], Alb2[(B+3*6+1):(B+4*6)],
            Alb2[(B+4*6+1): (B+5*6)], alt2[(2*14+1):(3*14)], Alb2[(B+5*6+1):(B+6*6)],
            Alb2[(B+6*6+1): (B+7*6)], alt2[(4*14):(3*14+1)], Alb2[(B+7*6+1):(B+8*6)],
            Alb2[(B+8*6+1): (B+9*6)], alt2[(4*14+1):(5*14)], Alb2[(B+9*6+1):(B+10*6)],
            Alb2[(B+10*6+1): (B+11*6)], alt2[(6*14):(6*14+1)], Alb2[(B+11*6+1):(B+12*6)])
Alfin <- Alb2[(B+12*6+1):280]
Altot2 <- c(Alini, Alent,Alfin)
Acini <- c(Alcb2[1:B])
Acent <- c(
            Alcb2[(B+1): (B+6)], alc2[1:14], Alcb2[(B+6+1):(B+2*6)],
            Alcb2[(B+2*6+1): (B+3*6)], alc2[(2*14):(1*14+1)], Alcb2[(B+3*6+1):(B+4*6)],
            Alcb2[(B+4*6+1): (B+5*6)], alc2[(2*14+1):(3*14)], Alcb2[(B+5*6+1):(B+6*6)],
            Alcb2[(B+6*6+1): (B+7*6)], alc2[(4*14):(3*14+1)], Alcb2[(B+7*6+1):(B+8*6)],
            Alcb2[(B+8*6+1): (B+9*6)], alc2[(4*14+1):(5*14)], Alcb2[(B+9*6+1):(B+10*6)],
            Alcb2[(B+10*6+1): (B+11*6)], alc2[(6*14):(6*14+1)], Alcb2[(B+11*6+1):(B+12*6)])
Acfin <- Alcb2[(B+12*6+1):280]
Actot2 <- c(Acini, Acent,Acfin)
capini <- c(Capb2[1:B])
capent <- c(
            Capb2[(B+1): (B+6)], cap2[1:14], Capb2[(B+6+1):(B+2*6)],
            Capb2[(B+2*6+1): (B+3*6)], cap2[(2*14):(1*14+1)], Capb2[(B+3*6+1):(B+4*6)],
            Capb2[(B+4*6+1): (B+5*6)], cap2[(2*14+1):(3*14)], Capb2[(B+5*6+1):(B+6*6)],
            Capb2[(B+6*6+1): (B+7*6)], cap2[(4*14):(3*14+1)], Capb2[(B+7*6+1):(B+8*6)],
            Capb2[(B+8*6+1): (B+9*6)], cap2[(4*14+1):(5*14)], Capb2[(B+9*6+1):(B+10*6)],
            Capb2[(B+10*6+1): (B+11*6)], cap2[(6*14):(6*14+1)], Capb2[(B+11*6+1):(B+12*6)])
capfin <- Capb2[(B+12*6+1):280]
captot2 <- c(capini, capent,capfin)
Rini <- c(rb2[1:B])
Rent <- c(
            rb2[(B+1): (B+6)], R2[1:14], rb2[(B+6+1):(B+2*6)],
            rb2[(B+2*6+1): (B+3*6)], R2[(2*14):(1*14+1)], rb2[(B+3*6+1):(B+4*6)],
            rb2[(B+4*6+1): (B+5*6)], R2[(2*14+1):(3*14)], rb2[(B+5*6+1):(B+6*6)],
            rb2[(B+6*6+1): (B+7*6)], R2[(4*14):(3*14+1)], rb2[(B+7*6+1):(B+8*6)],
            rb2[(B+8*6+1): (B+9*6)], R2[(4*14+1):(5*14)], rb2[(B+9*6+1):(B+10*6)],
            rb2[(B+10*6+1): (B+11*6)], R2[(6*14):(6*14+1)], rb2[(B+11*6+1):(B+12*6)])
Rfin <- rb2[(B+12*6+1):280]
Rtot2 <- c(Rini, Rent,Rfin)
Relini <- c(relb2[1:B])
Relent <- c(
            relb2[(B+1): (B+6)], Rel2[1:14], relb2[(B+6+1):(B+2*6)],
            relb2[(B+2*6+1): (B+3*6)], Rel2[(2*14):(1*14+1)], relb2[(B+3*6+1):(B+4*6)],
            relb2[(B+4*6+1): (B+5*6)], Rel2[(2*14+1):(3*14)], relb2[(B+5*6+1):(B+6*6)],
            relb2[(B+6*6+1): (B+7*6)], Rel2[(4*14):(3*14+1)], relb2[(B+7*6+1):(B+8*6)],
            relb2[(B+8*6+1): (B+9*6)], Rel2[(4*14+1):(5*14)], relb2[(B+9*6+1):(B+10*6)],
            relb2[(B+10*6+1): (B+11*6)], Rel2[(6*14):(6*14+1)], relb2[(B+11*6+1):(B+12*6)])
Relfin <- relb2[(B+12*6+1):280]
Reltot2 <- c(Relini, Relent,Relfin)
Areaini <- c(Areab2[1:B])
Areaent <- c(
            Areab2[(B+1): (B+6)], area2[1:14], Areab2[(B+6+1):(B+2*6)],
            Areab2[(B+2*6+1): (B+3*6)], area2[(2*14):(1*14+1)], Areab2[(B+3*6+1):(B+4*6)],
            Areab2[(B+4*6+1): (B+5*6)], area2[(2*14+1):(3*14)], Areab2[(B+5*6+1):(B+6*6)],
            Areab2[(B+6*6+1): (B+7*6)], area2[(4*14):(3*14+1)], Areab2[(B+7*6+1):(B+8*6)],
            Areab2[(B+8*6+1): (B+9*6)], area2[(4*14+1):(5*14)], Areab2[(B+9*6+1):(B+10*6)],
            Areab2[(B+10*6+1): (B+11*6)], area2[(6*14):(6*14+1)], Areab2[(B+11*6+1):(B+12*6)])
Areafin <- Areab2[(B+12*6+1):280]
Areatot2 <- c(Areaini, Areaent,Areafin)

# on remet dans le bon sens maintenant (en zig zag)
# les positions commencent en haut � droite

Ar2 <- c(); Ca2 <- c() ; Al2 <-c(); Ac2<-c(); Ra2<-c(); Rae2 <- c()
for (i in 1:7){
Ar2 <- c(Ar2, Areatot2[(26*(2*i-1)): (26*(2*i-2)+1)], Areatot2[(26*(2*i-1)+1):(26*(2*i))])
Ca2 <- c(Ca2, captot2[(26*(2*i-1)): (26*(2*i-2)+1)], captot2[(26*(2*i-1)+1):(26*(2*i))])
Al2 <- c(Al2, Altot2[(26*(2*i-1)): (26*(2*i-2)+1)], Altot2[(26*(2*i-1)+1):(26*(2*i))])
Ac2 <- c(Ac2, Actot2[(26*(2*i-1)): (26*(2*i-2)+1)], Actot2[(26*(2*i-1)+1):(26*(2*i))])
Ra2 <- c(Ra2, Rtot2[(26*(2*i-1)): (26*(2*i-2)+1)], Rtot2[(26*(2*i-1)+1):(26*(2*i))])
Rae2 <- c(Rae2, Reltot2[(26*(2*i-1)): (26*(2*i-2)+1)], Reltot2[(26*(2*i-1)+1):(26*(2*i))])
}


Areatot_2 <- c(Areatot_2, Ar2)
Altot_2 <-c(Altot_2, Al2)
Alctot_2<-c(Alctot_2,Ac2)
CAPtot_2<-c(CAPtot_2, Ca2)
Rtot_2<-c(Rtot_2, Ra2)
Reltot_2<-c(Reltot_2, Rae2)
}


# parcelle 4

dat04 <- dat$dat_Date[dat$dat_Parcelle==4]
cap04 <- dat$dat_CAP[dat$dat_Parcelle==4]
alt04 <- dat$dat_Alt[dat$dat_Parcelle==4]
alc04 <- dat$dat_Alcopa[dat$dat_Parcelle==4]
R04 <- dat$dat_R[dat$dat_Parcelle==4]
Rel04 <- dat$dat_Rel[dat$dat_Parcelle==4]
area04 <- dat$dat_Area[dat$dat_Parcelle==4]

dtb04 <- datb$datb[datb$Parcelle==4]
ligne04 <- datb$ligne[datb$Parcelle==4]
interligne04 <- datb$interligne[datb$Parcelle==4]
altb04 <- datb$Alb[datb$Parcelle==4]
alcb04 <- datb$Alcopb[datb$Parcelle==4]
capb04 <- datb$Capb[datb$Parcelle==4]
Rb04 <- datb$Rb[datb$Parcelle==4]
Relb04 <- datb$Relb[datb$Parcelle==4]
areab04 <- datb$Areab[datb$Parcelle==4]

# on retire les deux premi�res lignes et les deux derni�res (sur 18) et 3 interlignes de chaque cot� (sur 32)

Alb04 <- c() ; Alcb04<-c();Capb04<-c(); rb04<-c();relb04<-c(); Areab04<-c()
for (i in 1:length(altb04)){
if (ligne04[i] == 28|ligne04[i]==29|ligne04[i]==44|ligne04[i]==45|
    interligne04[i]==4|interligne04[i]==5|interligne04[i]==6|interligne04[i]==33|interligne04[i]==34|interligne04[i]==35){
Alb04 <- Alb04 ; Alcb04 <- Alcb04 ; Capb04 <- Capb04; rb04<-rb04; relb04 <- relb04; Areab04 <- Areab04}
else{
Alb04 <- c(Alb04,altb04[i])
Alcb04 <- c(Alcb04,alcb04[i])
Capb04 <- c(Capb04,capb04[i])
rb04 <- c(rb04,Rb04[i])
relb04 <- c(relb04,Relb04[i])
Areab04 <- c(Areab04,areab04[i])
}}

# on doit maintenant combiner
Areatot_4 <- c() ; Altot_4 <-c(); Alctot_4<-c();CAPtot_4<-c();Rtot_4<-c();Reltot_4<-c()

for (j in 1:14){

Alb4 <- Alb04[((j-1)*280+1) : (j*280)]
Alcb4 <- Alcb04[((j-1)*280+1) : (j*280)]
Capb4 <- Capb04[((j-1)*280+1) : (j*280)]
rb4 <- rb04[((j-1)*280+1) : (j*280)]
relb4 <- relb04[((j-1)*280+1) : (j*280)]
Areab4 <- Areab04[((j-1)*280+1) : (j*280)]
cap4 <- cap04[((j-1)*84+1) : (j*84)]
alt4 <- alt04[((j-1)*84+1) : (j*84)]
alc4 <- alc04[((j-1)*84+1) : (j*84)]
R4 <- R04[((j-1)*84+1) : (j*84)]
Rel4 <- Rel04[((j-1)*84+1) : (j*84)]
area4 <- area04[((j-1)*84+1) : (j*84)]


B =26*4
Alini <- c(Alb4[1:B])
Alent <- c(
            Alb4[(B+1): (B+6)], alt4[14:1], Alb4[(B+6+1):(B+2*6)],
            Alb4[(B+2*6+1): (B+3*6)], alt4[(1*14+1):(2*14)], Alb4[(B+3*6+1):(B+4*6)],
            Alb4[(B+4*6+1): (B+5*6)], alt4[(3*14):(2*14+1)], Alb4[(B+5*6+1):(B+6*6)],
            Alb4[(B+6*6+1): (B+7*6)], alt4[(3*14+1):(4*14)], Alb4[(B+7*6+1):(B+8*6)],
            Alb4[(B+8*6+1): (B+9*6)], alt4[(5*14):(4*14+1)], Alb4[(B+9*6+1):(B+10*6)],
            Alb4[(B+10*6+1): (B+11*6)], alt4[(6*14+1):(6*14)], Alb4[(B+11*6+1):(B+12*6)])
Alfin <- Alb4[(B+12*6+1):280]
Altot4 <- c(Alini, Alent,Alfin)
Acini <- c(Alcb4[1:B])
Acent <- c(
            Alcb4[(B+1): (B+6)], alc4[14:1], Alcb4[(B+6+1):(B+2*6)],
            Alcb4[(B+2*6+1): (B+3*6)], alc4[(1*14+1):(2*14)], Alcb4[(B+3*6+1):(B+4*6)],
            Alcb4[(B+4*6+1): (B+5*6)], alc4[(3*14):(2*14+1)], Alcb4[(B+5*6+1):(B+6*6)],
            Alcb4[(B+6*6+1): (B+7*6)], alc4[(3*14+1):(4*14)], Alcb4[(B+7*6+1):(B+8*6)],
            Alcb4[(B+8*6+1): (B+9*6)], alc4[(5*14):(4*14+1)], Alcb4[(B+9*6+1):(B+10*6)],
            Alcb4[(B+10*6+1): (B+11*6)], alc4[(6*14+1):(6*14)], Alcb4[(B+11*6+1):(B+12*6)])
Acfin <- Alcb4[(B+12*6+1):280]
Actot4 <- c(Acini, Acent,Acfin)
capini <- c(Capb4[1:B])
capent <- c(
            Capb4[(B+1): (B+6)], cap4[14:1], Capb4[(B+6+1):(B+2*6)],
            Capb4[(B+2*6+1): (B+3*6)], cap4[(1*14+1):(2*14)], Capb4[(B+3*6+1):(B+4*6)],
            Capb4[(B+4*6+1): (B+5*6)], cap4[(3*14):(2*14+1)], Capb4[(B+5*6+1):(B+6*6)],
            Capb4[(B+6*6+1): (B+7*6)], cap4[(3*14+1):(4*14)], Capb4[(B+7*6+1):(B+8*6)],
            Capb4[(B+8*6+1): (B+9*6)], cap4[(5*14):(4*14+1)], Capb4[(B+9*6+1):(B+10*6)],
            Capb4[(B+10*6+1): (B+11*6)], cap4[(6*14+1):(6*14)], Capb4[(B+11*6+1):(B+12*6)])
capfin <- Capb4[(B+12*6+1):280]
captot4 <- c(capini, capent,capfin)
Rini <- c(rb4[1:B])
Rent <- c(
            rb4[(B+1): (B+6)], R4[14:1], rb4[(B+6+1):(B+2*6)],
            rb4[(B+2*6+1): (B+3*6)], R4[(1*14+1):(2*14)], rb4[(B+3*6+1):(B+4*6)],
            rb4[(B+4*6+1): (B+5*6)], R4[(3*14):(2*14+1)], rb4[(B+5*6+1):(B+6*6)],
            rb4[(B+6*6+1): (B+7*6)], R4[(3*14+1):(4*14)], rb4[(B+7*6+1):(B+8*6)],
            rb4[(B+8*6+1): (B+9*6)], R4[(5*14):(4*14+1)], rb4[(B+9*6+1):(B+10*6)],
            rb4[(B+10*6+1): (B+11*6)], R4[(6*14+1):(6*14)], rb4[(B+11*6+1):(B+12*6)])
Rfin <- rb4[(B+12*6+1):280]
Rtot4 <- c(Rini, Rent,Rfin)
Relini <- c(relb4[1:B])
Relent <- c(
            relb4[(B+1): (B+6)], Rel4[14:1], relb4[(B+6+1):(B+2*6)],
            relb4[(B+2*6+1): (B+3*6)], Rel4[(1*14+1):(2*14)], relb4[(B+3*6+1):(B+4*6)],
            relb4[(B+4*6+1): (B+5*6)], Rel4[(3*14):(2*14+1)], relb4[(B+5*6+1):(B+6*6)],
            relb4[(B+6*6+1): (B+7*6)], Rel4[(3*14+1):(4*14)], relb4[(B+7*6+1):(B+8*6)],
            relb4[(B+8*6+1): (B+9*6)], Rel4[(5*14):(4*14+1)], relb4[(B+9*6+1):(B+10*6)],
            relb4[(B+10*6+1): (B+11*6)], Rel4[(6*14+1):(6*14)], relb4[(B+11*6+1):(B+12*6)])
Relfin <- relb4[(B+12*6+1):280]
Reltot4 <- c(Relini, Relent,Relfin)
Areaini <- c(Areab4[1:B])
Areaent <- c(
            Areab4[(B+1): (B+6)], area4[14:1], Areab4[(B+6+1):(B+2*6)],
            Areab4[(B+2*6+1): (B+3*6)], area4[(1*14+1):(2*14)], Areab4[(B+3*6+1):(B+4*6)],
            Areab4[(B+4*6+1): (B+5*6)], area4[(3*14):(2*14+1)], Areab4[(B+5*6+1):(B+6*6)],
            Areab4[(B+6*6+1): (B+7*6)], area4[(3*14+1):(4*14)], Areab4[(B+7*6+1):(B+8*6)],
            Areab4[(B+8*6+1): (B+9*6)], area4[(5*14):(4*14+1)], Areab4[(B+9*6+1):(B+10*6)],
            Areab4[(B+10*6+1): (B+11*6)], area4[(6*14+1):(6*14)], Areab4[(B+11*6+1):(B+12*6)])
Areafin <- Areab4[(B+12*6+1):280]
Areatot4 <- c(Areaini, Areaent,Areafin)

# on remet dans le bon sens maintenant (en zig zag)

Ar4 <- c(); Ca4 <- c() ; Al4 <-c(); Ac4<-c(); Ra4<-c(); Rae4 <- c()
for (i in 1:7){
Ar4 <- c(Ar4, Areatot4[(26*(2*i-1)): (26*(2*i-2)+1)], Areatot4[(26*(2*i-1)+1):(26*(2*i))])
Ca4 <- c(Ca4, captot4[(26*(2*i-1)): (26*(2*i-2)+1)], captot4[(26*(2*i-1)+1):(26*(2*i))])
Al4 <- c(Al4, Altot4[(26*(2*i-1)): (26*(2*i-2)+1)], Altot4[(26*(2*i-1)+1):(26*(2*i))])
Ac4 <- c(Ac4, Actot4[(26*(2*i-1)): (26*(2*i-2)+1)], Actot4[(26*(2*i-1)+1):(26*(2*i))])
Ra4 <- c(Ra4, Rtot4[(26*(2*i-1)): (26*(2*i-2)+1)], Rtot4[(26*(2*i-1)+1):(26*(2*i))])
Rae4 <- c(Rae4, Reltot4[(26*(2*i-1)): (26*(2*i-2)+1)], Reltot4[(26*(2*i-1)+1):(26*(2*i))])
}


Areatot_4 <- c(Areatot_4, Ar4)
Altot_4 <-c(Altot_4, Al4)
Alctot_4<-c(Alctot_4,Ac4)
CAPtot_4<-c(CAPtot_4, Ca4)
Rtot_4<-c(Rtot_4, Ra4)
Reltot_4<-c(Reltot_4, Rae4)
}

# parcelle 3

dat03 <- dat$dat_Date[dat$dat_Parcelle==3]
cap03 <- dat$dat_CAP[dat$dat_Parcelle==3]
alt03 <- dat$dat_Alt[dat$dat_Parcelle==3]
alc03 <- dat$dat_Alcopa[dat$dat_Parcelle==3]
R03 <- dat$dat_R[dat$dat_Parcelle==3]
Rel03 <- dat$dat_Rel[dat$dat_Parcelle==3]
area03 <- dat$dat_Area[dat$dat_Parcelle==3]

dtb03 <- datb$datb[datb$Parcelle==3]
ligne03 <- datb$ligne[datb$Parcelle==3]
interligne03 <- datb$interligne[datb$Parcelle==3]
altb03 <- datb$Alb[datb$Parcelle==3]
alcb03 <- datb$Alcopb[datb$Parcelle==3]
capb03 <- datb$Capb[datb$Parcelle==3]
Rb03 <- datb$Rb[datb$Parcelle==3]
Relb03 <- datb$Relb[datb$Parcelle==3]
areab03 <- datb$Areab[datb$Parcelle==3]

# on retire les deux premi�res lignes et les deux derni�res (sur 18) et 3 interlignes de chaque cot� (sur 32)

Alb03 <- c() ; Alcb03<-c();Capb03<-c(); rb03<-c();relb03<-c(); Areab03<-c()
for (i in 1:length(altb03)){
if (ligne03[i] == 15|ligne03[i]==16|ligne03[i]==31|ligne03[i]==32|interligne03[i]==3|interligne03[i]==28|interligne03[i]==29|interligne03[i]==30|interligne03[i]==57|interligne03[i]==58|interligne03[i]==59){
Alb03 <- Alb03 ; Alcb03 <- Alcb03 ; Capb03 <- Capb03; rb03<-rb03; relb03 <- relb03; Areab03 <- Areab03}
else{
Alb03 <- c(Alb03,altb03[i])
Alcb03 <- c(Alcb03,alcb03[i])
Capb03 <- c(Capb03,capb03[i])
rb03 <- c(rb03,Rb03[i])
relb03 <- c(relb03,Relb03[i])
Areab03 <- c(Areab03,areab03[i])
}}

# retirons les 10 premiers arbres � chaque fois
Alb003 <- c() ; Alcb003<-c();Capb003<-c(); rb003<-c();relb003<-c(); Areab003<-c()
for (i in 1:14){
Alb003 <- c(Alb003,Alb03[(280*(i-1)+11):(280*i)])
Alcb003 <- c(Alcb003,Alcb03[(280*(i-1)+11):(280*i)])
Capb003 <- c(Capb003,Capb03[(280*(i-1)+11):(280*i)])
rb003 <- c(rb003,rb03[(280*(i-1)+11):(280*i)])
relb003 <- c(relb003,relb03[(280*(i-1)+11):(280*i)])
Areab003 <- c(Areab003,Areab03[(280*(i-1)+11):(280*i)])
}

# on doit maintenant combiner
Areatot_3 <- c() ; Altot_3 <-c(); Alctot_3<-c();CAPtot_3<-c();Rtot_3<-c();Reltot_3<-c()

for (j in 1:14){

Alb3 <- Alb003[((j-1)*270+1) : (j*270)]
Alcb3 <- Alcb003[((j-1)*270+1) : (j*270)]
Capb3 <- Capb003[((j-1)*270+1) : (j*270)]
rb3 <- rb003[((j-1)*270+1) : (j*270)]
relb3 <- relb003[((j-1)*270+1) : (j*270)]
Areab3 <- Areab003[((j-1)*270+1) : (j*270)]
cap3 <- cap03[((j-1)*84+1) : (j*84)]
alt3 <- alt03[((j-1)*84+1) : (j*84)]
alc3 <- alc03[((j-1)*84+1) : (j*84)]
R3 <- R03[((j-1)*84+1) : (j*84)]
Rel3 <- Rel03[((j-1)*84+1) : (j*84)]
area3 <- area03[((j-1)*84+1) : (j*84)]


B =26*4-10
Alini <- c(Alb3[1:B])
Alent <- c(
            Alb3[(B+1): (B+6)], alt3[1:14], Alb3[(B+6+1):(B+2*6)],
            Alb3[(B+2*6+1): (B+3*6)], alt3[(2*14):(1*14+1)], Alb3[(B+3*6+1):(B+4*6)],
            Alb3[(B+4*6+1): (B+5*6)], alt3[(2*14+1):(3*14)], Alb3[(B+5*6+1):(B+6*6)],
            Alb3[(B+6*6+1): (B+7*6)], alt3[(4*14):(3*14+1)], Alb3[(B+7*6+1):(B+8*6)],
            Alb3[(B+8*6+1): (B+9*6)], alt3[(4*14+1):(5*14)], Alb3[(B+9*6+1):(B+10*6)],
            Alb3[(B+10*6+1): (B+11*6)], alt3[(6*14):(6*14+1)], Alb3[(B+11*6+1):(B+12*6)])
Alfin <- Alb3[(B+12*6+1):270]
Altot3 <- c(Alini, Alent,Alfin)
Acini <- c(Alcb3[1:B])
Acent <- c(
            Alcb3[(B+1): (B+6)], alc3[1:14], Alcb3[(B+6+1):(B+2*6)],
            Alcb3[(B+2*6+1): (B+3*6)], alc3[(2*14):(1*14+1)], Alcb3[(B+3*6+1):(B+4*6)],
            Alcb3[(B+4*6+1): (B+5*6)], alc3[(2*14+1):(3*14)], Alcb3[(B+5*6+1):(B+6*6)],
            Alcb3[(B+6*6+1): (B+7*6)], alc3[(4*14):(3*14+1)], Alcb3[(B+7*6+1):(B+8*6)],
            Alcb3[(B+8*6+1): (B+9*6)], alc3[(4*14+1):(5*14)], Alcb3[(B+9*6+1):(B+10*6)],
            Alcb3[(B+10*6+1): (B+11*6)], alc3[(6*14):(6*14+1)], Alcb3[(B+11*6+1):(B+12*6)])
Acfin <- Alcb3[(B+12*6+1):270]
Actot3 <- c(Acini, Acent,Acfin)
capini <- c(Capb3[1:B])
capent <- c(
            Capb3[(B+1): (B+6)], cap3[1:14], Capb3[(B+6+1):(B+2*6)],
            Capb3[(B+2*6+1): (B+3*6)], cap3[(2*14):(1*14+1)], Capb3[(B+3*6+1):(B+4*6)],
            Capb3[(B+4*6+1): (B+5*6)], cap3[(2*14+1):(3*14)], Capb3[(B+5*6+1):(B+6*6)],
            Capb3[(B+6*6+1): (B+7*6)], cap3[(4*14):(3*14+1)], Capb3[(B+7*6+1):(B+8*6)],
            Capb3[(B+8*6+1): (B+9*6)], cap3[(4*14+1):(5*14)], Capb3[(B+9*6+1):(B+10*6)],
            Capb3[(B+10*6+1): (B+11*6)], cap3[(6*14):(6*14+1)], Capb3[(B+11*6+1):(B+12*6)])
capfin <- Capb3[(B+12*6+1):270]
captot3 <- c(capini, capent,capfin)
Rini <- c(rb3[1:B])
Rent <- c(
            rb3[(B+1): (B+6)], R3[1:14], rb3[(B+6+1):(B+2*6)],
            rb3[(B+2*6+1): (B+3*6)], R3[(2*14):(1*14+1)], rb3[(B+3*6+1):(B+4*6)],
            rb3[(B+4*6+1): (B+5*6)], R3[(2*14+1):(3*14)], rb3[(B+5*6+1):(B+6*6)],
            rb3[(B+6*6+1): (B+7*6)], R3[(4*14):(3*14+1)], rb3[(B+7*6+1):(B+8*6)],
            rb3[(B+8*6+1): (B+9*6)], R3[(4*14+1):(5*14)], rb3[(B+9*6+1):(B+10*6)],
            rb3[(B+10*6+1): (B+11*6)], R3[(6*14):(6*14+1)], rb3[(B+11*6+1):(B+12*6)])
Rfin <- rb3[(B+12*6+1):270]
Rtot3 <- c(Rini, Rent,Rfin)
Relini <- c(relb3[1:B])
Relent <- c(
            relb3[(B+1): (B+6)], Rel3[1:14], relb3[(B+6+1):(B+2*6)],
            relb3[(B+2*6+1): (B+3*6)], Rel3[(2*14):(1*14+1)], relb3[(B+3*6+1):(B+4*6)],
            relb3[(B+4*6+1): (B+5*6)], Rel3[(2*14+1):(3*14)], relb3[(B+5*6+1):(B+6*6)],
            relb3[(B+6*6+1): (B+7*6)], Rel3[(4*14):(3*14+1)], relb3[(B+7*6+1):(B+8*6)],
            relb3[(B+8*6+1): (B+9*6)], Rel3[(4*14+1):(5*14)], relb3[(B+9*6+1):(B+10*6)],
            relb3[(B+10*6+1): (B+11*6)], Rel3[(6*14):(6*14+1)], relb3[(B+11*6+1):(B+12*6)])
Relfin <- relb3[(B+12*6+1):270]
Reltot3 <- c(Relini, Relent,Relfin)
Areaini <- c(Areab3[1:B])
Areaent <- c(
            Areab3[(B+1): (B+6)], area3[1:14], Areab3[(B+6+1):(B+2*6)],
            Areab3[(B+2*6+1): (B+3*6)], area3[(2*14):(1*14+1)], Areab3[(B+3*6+1):(B+4*6)],
            Areab3[(B+4*6+1): (B+5*6)], area3[(2*14+1):(3*14)], Areab3[(B+5*6+1):(B+6*6)],
            Areab3[(B+6*6+1): (B+7*6)], area3[(4*14):(3*14+1)], Areab3[(B+7*6+1):(B+8*6)],
            Areab3[(B+8*6+1): (B+9*6)], area3[(4*14+1):(5*14)], Areab3[(B+9*6+1):(B+10*6)],
            Areab3[(B+10*6+1): (B+11*6)], area3[(6*14):(6*14+1)], Areab3[(B+11*6+1):(B+12*6)])
Areafin <- Areab3[(B+12*6+1):270]
Areatot3 <- c(Areaini, Areaent,Areafin)

# on remet dans le bon sens maintenant (en zig zag)

Ar3 <- c(Areatot3[16: 1], Areatot3[17:42])
Ca3 <- c(captot3[16: 1], captot3[17:42])
Al3 <- c(Altot3[16: 1], Altot3[17:42])
Ac3 <- c(Actot3[16: 1], Actot3[17:42])
Ra3 <- c(Rtot3[16: 1], Rtot3[17:42])
Rae3 <- c(Reltot3[16: 1], Reltot3[17:42])

for (i in 1:6){
Ar3 <- c(Ar3, Areatot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], Areatot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
Ca3 <- c(Ca3, captot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], captot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
Al3 <- c(Al3, Altot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], Altot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
Ac3 <- c(Ac3, Actot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], Actot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
Ra3 <- c(Ra3, Rtot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], Rtot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
Rae3 <- c(Rae3, Reltot3[(42+26*(2*i-1)): (42+26*(2*i-2)+1)], Reltot3[(42+26*(2*i-1)+1):(42+26*(2*i))])
}

Areatot_3 <- c(Areatot_3, Ar3)
Altot_3 <-c(Altot_3, Al3)
Alctot_3<-c(Alctot_3,Ac3)
CAPtot_3<-c(CAPtot_3, Ca3)
Rtot_3<-c(Rtot_3, Ra3)
Reltot_3<-c(Reltot_3, Rae3)
}


# les copa ne sont pas bien
barplot(Altot_1,ylim=c(0,25))
par(new=T)
barplot(Alctot_1,col='gray',border=NA,ylim=c(0,25))

barplot(Altot_2,ylim=c(0,25))
par(new=T)
barplot(Alctot_2,col='gray',border=NA,ylim=c(0,25))
barplot(Altot_3,ylim=c(0,25))
par(new=T)
barplot(Alctot_3,col='gray',border=NA,ylim=c(0,25))
barplot(Altot_4,ylim=c(0,25))
par(new=T)
barplot(Alctot_4,col='gray',border=NA,ylim=c(0,25))

# Alctot = hauteur de couronne

#Copa1<-c();Copa2 <- c(); Copa3 <- c(); Copa4<-c()
#for (i in 1:length(Alctot_1)){
#if (Alctot_1[i]==0|is.na(Alctot_1[i])==T){
#Copa1 <- c(Copa1,0)}
#else{ Copa1 <- c(Copa1, Altot_1[i] - Alctot_1[i])}
#if (Alctot_2[i]==0|is.na(Alctot_2[i])==T){
#Copa2 <- c(Copa2,0)}
#else{ Copa2 <- c(Copa2, Altot_2[i] - Alctot_2[i])}
#if (Alctot_4[i]==0|is.na(Alctot_4[i])==T){
#Copa4 <- c(Copa4,0)}                                    
#else{ Copa4 <- c(Copa4, Altot_4[i] - Alctot_4[i])}
#}
#for (i in 1:length(Alctot_3)){
#if (Alctot_3[i]==0|is.na(Alctot_3[i])==T){
#Copa3 <- c(Copa3,0)}
#else{ Copa3 <- c(Copa3, Altot_3[i] - Alctot_3[i])}
#}


# autre possibilit� les deux derni�res dates altctot = hauteur de tronc
#Copa1<-c();Copa2 <- c(); Copa3 <- c(); Copa4<-c()
#for (i in 1:3276){
#if (Alctot_1[i]==0|is.na(Alctot_1[i])==T){
#Copa1 <- c(Copa1,0)}
#else{ Copa1 <- c(Copa1, Altot_1[i] - Alctot_1[i])}
#if (Alctot_2[i]==0|is.na(Alctot_2[i])==T){
#Copa2 <- c(Copa2,0)}
#else{ Copa2 <- c(Copa2, Altot_2[i] - Alctot_2[i])}
#if (Alctot_4[i]==0|is.na(Alctot_4[i])==T){
#Copa4 <- c(Copa4,0)}                                    
#else{ Copa4 <- c(Copa4, Altot_4[i] - Alctot_4[i])}
#}
#for (i in 3277:length(Alctot_1)){
#if (Alctot_1[i]==0|is.na(Alctot_1[i])==T){
#Copa1 <- c(Copa1,0)}
#else{ Copa1 <- c(Copa1, Alctot_1[i])}
#if (Alctot_2[i]==0|is.na(Alctot_2[i])==T){
#Copa2 <- c(Copa2,0)}
#else{ Copa2 <- c(Copa2, Alctot_2[i])}
#if (Alctot_4[i]==0|is.na(Alctot_4[i])==T){
#Copa4 <- c(Copa4,0)}                                    
#else{ Copa4 <- c(Copa4, Alctot_4[i])}
#}
#for (i in 1:3186){
#if (Alctot_3[i]==0|is.na(Alctot_3[i])==T){
#Copa3 <- c(Copa3,0)}
#else{ Copa3 <- c(Copa3, Altot_3[i] - Alctot_3[i])}
#}
#for (i in 3187:length(Alctot_3)){
#if (Alctot_3[i]==0|is.na(Alctot_3[i])==T){
#Copa3 <- c(Copa3,0)}
#else{ Copa3 <- c(Copa3, Alctot_3[i])}
#}


#for (i in 1:length(Copa1)){
#if (Copa1[i] <=0){Copa1[i] <- 0}else{Copa1[i] <- Copa1[i]}
#if (Copa2[i] <=0){Copa2[i] <- 0}else{Copa2[i] <- Copa2[i]}
#if (Copa4[i] <=0){Copa4[i] <- 0}else{Copa4[i] <- Copa4[i]}}
#for (i in 1:length(Copa3)){
#if (Copa3[i] <=0|is.na(Copa3[i])==T){Copa3[i] <- 0}else{Copa3[i] <- Copa3[i]}}

Copa1 <- Alctot_1
Copa2 <- Alctot_2
Copa3 <- Alctot_3
Copa4 <- Alctot_4



Date124 <- c( rep("11/02/2010",364),rep("09/04/2010",364),rep("01/05/2010",364), rep("05/08/2010",364),
              rep("12/11/2010",364),rep("04/02/2011",364),rep("25/05/2011",364), rep("10/08/2011",364), 
              rep("05/12/2011",364),rep("18/06/2012",364),rep('04/02/2013',364),
              rep("08/07/2013",364),rep("10/02/2014",364),rep("10/08/2014",364))
Date3 <- c( rep("11/02/2010",354),rep("09/04/2010",354),rep("01/05/2010",354), rep("05/08/2010",354),
            rep("12/11/2010",354),rep("04/02/2011",354),rep("25/05/2011",354), rep("10/08/2011",354), rep("05/12/2011",354),
            rep("18/06/2012",354),rep("04/02/2013",354),rep("08/07/2013",354),rep("10/02/2014",354),rep("10/08/2014",354))

Rtot_1 <- Rtot_1 * 0.9
Rtot_2 <- Rtot_2 * 0.9
Rtot_3 <- Rtot_3 * 0.9
Rtot_4 <- Rtot_4 * 0.9
Reltot_1 <- Reltot_1 * 0.9
Reltot_2 <- Reltot_2 * 0.9
Reltot_3 <- Reltot_3 * 0.9
Reltot_4 <- Reltot_4 * 0.9


Datas_maestra124 <- data.frame (Date124,
  Areatot_1,Areatot_2,Areatot_4,
  Altot_1,Altot_2,Altot_4,
  Copa1,Copa2,Copa4,
  CAPtot_1,CAPtot_2,CAPtot_4,
  Rtot_1,Rtot_2,Rtot_4,
  Reltot_1,Reltot_2,Reltot_4)
Datas_maestra3 <- data.frame ( Date3,
  Areatot_3, Altot_3, Copa3,CAPtot_3,Rtot_3,Reltot_3)

barplot(Altot_1,ylim=c(0,25))
par(new=T)
barplot(Copa1,col='gray',border=NA,ylim=c(0,25))


Datas_maestra124 <- replace(Datas_maestra124,is.na(Datas_maestra124),0)
Datas_maestra3 <- replace(Datas_maestra3,is.na(Datas_maestra3),0)

write.table(Datas_maestra124,'maestra_morpho124.txt',col.names=T,row.names=F)
write.table(Datas_maestra3,'maestra_morpho3.txt',col.names=T,row.names=F)








