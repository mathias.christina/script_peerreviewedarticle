set<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EUCFLUX sensibilit�")
setwd(set)

D1 <- read.table('angles_maestra.txt',header=T)

ANG <- c(0:8)*10+5


D2 <- read.table('dist_vert_maestra.txt',header=T)
D3 <- read.table('dist_horiz_maestra.txt',header=T)

X <- seq(0,1,length.out=100)
Y1 <- D2$ini_2010[1] * X ^ D2$ini_2010[2] * (1-X)^D2$ini_2010[3]/3
Y2 <- D2$X11_2010[1] * X ^ D2$X11_2010[2] * (1-X)^D2$X11_2010[3]/3
Y3 <- D2$ini_2011[1] * X ^ D2$ini_2011[2] * (1-X)^D2$ini_2011[3]/3
Y4 <- D2$X12_2011_2012[1] * X ^ D2$X12_2011_2012[2] * (1-X)^D2$X12_2011_2012[3]/3

Z1 <- D3$X18_P_tot[1] * X ^ D3$X18_P_tot[2] * (1-X)^D3$X18_P_tot[3]/(2*pi)
Z2 <- D3$X30_P_tot[1] * X ^ D3$X30_P_tot[2] * (1-X)^D3$X30_P_tot[3]/(2*pi)
Z3 <- D3$X18_MG_tot[1] * X ^ D3$X18_MG_tot[2] * (1-X)^D3$X18_MG_tot[3]/(2*pi)
Z4 <- D3$X30_MG_tot[1] * X ^ D3$X30_MG_tot[2] * (1-X)^D3$X30_MG_tot[3]/(2*pi)

# 3 ages
par(mfrow=c(1,3))
par(mar=c(4,4,3,1))
par(xaxs='i',yaxs='i')
plot(ANG, D1$X10P, type='b',lty=1, pch=16,lwd=2,ylim=c(0,0.4),xlim=c(0,90),
      xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,axes=T,ann=T,mgp=c(2.5,1,0))
points(ANG,D1$X10M,type='b',lty=2,pch=16,lwd=2)
points(ANG,D1$X10G,type='b',lty=3,pch=16,lwd=2)
legend('topright',legend=c(expression('Small tree:'~theta[I]~'='~'24�'),
                           expression('Medium tree:'~theta[I]~'='~'21�'),
                           expression('Big tree:'~theta[I]~'='~'24�')),pch=4,lty=1:3,bg='white',box.col=0,cex=1)
box()
title('12 months')
plot(ANG, D1$X11P, type='b',lty=1, pch=16,lwd=2,ylim=c(0,0.4),xlim=c(0,90),
      xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,axes=T,ann=T,mgp=c(2.5,1,0))
points(ANG,D1$X11M,type='b',lty=2,pch=16,lwd=2)
points(ANG,D1$X11G,type='b',lty=3,pch=16,lwd=2)
legend('topright',legend=c(expression('Small tree:'~theta[I]~'='~'27�'),
                           expression('Medium tree:'~theta[I]~'='~'27�'),
                           expression('Big tree:'~theta[I]~'='~'26�')),pch=4,lty=1:3,bg='white',box.col=0,cex=1)
box()
title('24 months')
plot(ANG, D1$X12P, type='b',lty=1, pch=16,lwd=2,ylim=c(0,0.4),xlim=c(0,90),
      xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,axes=T,ann=T,mgp=c(2.5,1,0))
points(ANG,D1$X12M,type='b',lty=2,pch=16,lwd=2)
points(ANG,D1$X12G,type='b',lty=3,pch=16,lwd=2)
legend('topright',legend=c(expression('Small tree:'~theta[I]~'='~'34�'),
                           expression('Medium tree:'~theta[I]~'='~'36�'),
                           expression('Big tree:'~theta[I]~'='~'37�')),pch=4,lty=1:3,bg='white',box.col=0,cex=1)
box()
title('36 months')

# autre
par(mfrow=c(1,2))
par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(ANG, D1$ang2010+0.3, type='b',lty=1, pch=16,lwd=2,ylim=c(0,0.6),xlim=c(0,90),
      xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1,axes=F,ann=F)
points(ANG,D1$ang2011+0.3,type='b',lty=2,pch=16,lwd=2)
points(ANG,D1$ang2012+0.3,type='b',lty=3,pch=16,lwd=2)
abline(h=0.3)
points(ANG,D1$P,type='b',pch=4,lty=1,lwd=2)
points(ANG,D1$M,type='b',pch=4,lty=2,lwd=2)
points(ANG,D1$G,type='b',pch=4,lty=3,lwd=2)
axis(1,at=c(0:4)*20,labels=c(0:4)*20)
axis(2,at=c(0:6)/10,labels=c(0,0.1,0.2,0,0.1,0.2,0.3))
mtext('Probability',side=2,line=3)
mtext('Leaf inclination angle (�)',side=1,line=2.5)
legend('topright',legend=c(expression('1 year:'~theta[I]~'='~'23�'),
                           expression('2 year:'~theta[I]~'='~'27�'),
                           expression('3 year:'~theta[I]~'='~'36�')),pch=16,lty=1:3,bg='white',box.col=0,cex=1)
legend(x=40,y=0.28,legend=c(expression('Small tree:'~theta[I]~'='~'24�'),
                           expression('Medium tree:'~theta[I]~'='~'29�'),
                           expression('Big tree:'~theta[I]~'='~'38�')),pch=4,lty=1:3,bg='white',box.col=0,cex=1)
box()


par(mar=c(16.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(Y1, X, type='l',lty=1,lwd=2,ylim=c(0,1),xlim=c(0,1),
      xlab='Relative leaf area',ylab='Relative height within the crown',cex.lab=1,ann=F)
points(Y2,X,type='l',lty=2,lwd=2)
points(Y3,X,type='l',lty=3,lwd=2)
points(Y4,X,type='l',lty=4,lwd=2)
legend('topright',legend=c('2 to 9 months','10 to 13 months','14 to 23 months','24 to 36 months'),pch=NA,lty=1:4,bg='white',box.col=0,cex=1)
box()
mtext('Relative leaf area',side=1,line=2.5)
mtext('Relative height within the crown',side=2,line=3)
par(new=T)
par(mar=c(4,4.5,19,1))
par(xaxs='i',yaxs='i')
plot(X, Z1, type='l',lty=3,lwd=2,ylim=c(0,0.7),xlim=c(0,1),
      xlab='Relative distance to trunk',ylab='Relative leaf area',cex.lab=1,ann=F)
points(X,Z2,type='l',lty=4,lwd=2)
points(X,Z3,type='l',lty=2,lwd=2)
points(X,Z4,type='l',lty=1,lwd=2)
legend('topright',legend=c('Small trees, 18 months','Small trees, 30 months','Medium & big trees, 18 months','Medium & big trees, 30 months'),
      pch=NA,lty=c(3,4,2,1),bg='white',box.col=0,cex=1)
mtext('Relative leaf area',side=2,line=3)
mtext('Relative distance to trunk',side=1,line=2.5)
box()




# figures 2
set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Eucflux/analyse_photosynthetical_parameter")
setwd(set)
D1 <- read.table('JmVcmRdALL.txt',header=T)
set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Param�trisation de MAESPA/Plant_conductance")
setwd(set1)
D2 <- read.table('KPC3.txt',header=T)
set1<-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Largeur feuille")
setwd(set1)
D3 <- read.table('leafwidth.txt',header=T)

MLW<-c();sdLW<-c();P<-c()
for (i in c(5,9,12,15,18,25,31)){
MLW <- c(MLW, mean(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='S']),mean(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='M']),mean(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='B']))
sdLW <- c(sdLW, sd(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='S']),sd(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='M']),sd(D3$LW[D3$AGE==i][D3$TT[D3$AGE==i]=='B']))
P <- c(P,'S','M','B')
}

MJ <- c(mean(D1$Jmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),mean(D1$Jmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        mean(D1$Jmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),mean(D1$Jmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        mean(D1$Jmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),mean(D1$Jmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))
MV <- c(mean(D1$Vcmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),mean(D1$Vcmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        mean(D1$Vcmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),mean(D1$Vcmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        mean(D1$Vcmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),mean(D1$Vcmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))
MR <- c(mean(D1$Rd[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),mean(D1$Rd[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        mean(D1$Rd[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),mean(D1$Rd[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        mean(D1$Rd[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),mean(D1$Rd[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))
sdJ <- c(sd(D1$Jmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),sd(D1$Jmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        sd(D1$Jmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),sd(D1$Jmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        sd(D1$Jmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),sd(D1$Jmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))
sdV <- c(sd(D1$Vcmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),sd(D1$Vcmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        sd(D1$Vcmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),sd(D1$Vcmax[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        sd(D1$Vcmax[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),sd(D1$Vcmax[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))
sdR <- c(sd(D1$Rd[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='I']),sd(D1$Rd[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='I']),
        sd(D1$Rd[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='I']),sd(D1$Rd[D1$Pv=='I'][D1$Ph[D1$Pv=='I']=='E']),
        sd(D1$Rd[D1$Pv=='M'][D1$Ph[D1$Pv=='M']=='E']),sd(D1$Rd[D1$Pv=='S'][D1$Ph[D1$Pv=='S']=='E']))

KPC3<-c();sdKPC3<-c();for (i in levels(as.factor(D2$AGEC3))){
KPC3 <- c(KPC3, mean(D2$KPC3[D2$AGEC3==i],na.rm=T))
sdKPC3 <- c(sdKPC3, sd(D2$KPC3[D2$AGEC3==i],na.rm=T))}

source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
H <-c(1:3)

split.screen(c(2,1))
split.screen(c(1,3),screen=1)#3 4 5
split.screen(c(1,2),screen=2)#6 7

screen(3)
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(c(MJ[1:3]),H,ylim=c(0.5,3.5),xlim=c(0,250),lty=2,lwd=2,type='b',pch=15,axes=F,ann=F,cex.lab=1)
points(c(MJ[4:6]),H,lty=1,lwd=2,type='b',pch=16)
grid(nx=NULL,ny=NA,col = 1, lty = "dotted", lwd = 1)
legend('topleft',legend=c('Old leaves','Young leaves'),pch=c(15,16),lty=c(2,1),cex=0.8,bg='white',box.col=0)
for (i in 1:3){
add.errorbars (MJ[i],H[i],SE=sdJ[i],direction='hor',barlen=0.04,color=1)
}
axis(2,at=c(1,2,3),labels=c('Inf','Med','Sup'))
axis(1,at=c(0:5)*50,labels=c(0:5)*50)
mtext(expression (J[max]~at~'25�C'~(�mol~m^-2~s^-1)),side=1,line=2.5,cex=1)
mtext('Vertical position within the crown',cex=1,line=2.5,side=2)
box()
screen(4)
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(c(MV[1:3]),H,ylim=c(0.5,3.5),xlim=c(0,180),lty=2,lwd=2,type='b',pch=15,axes=F,ann=F,cex.lab=1)
points(c(MV[4:6]),H,lty=1,lwd=2,type='b',pch=16)
for (i in 1:3){
add.errorbars (MV[i],H[i],SE=sdV[i],direction='hor',barlen=0.04)
}
axis(2,at=c(1,2,3),labels=c('Inf','Med','Sup'))
axis(1,at=c(0:3)*50,labels=c(0:3)*50)
mtext(expression (V[cmax]~at~'25�C'~(�mol~m^-2~s^-1)),side=1,line=2.5,cex=1)
mtext('Vertical position within the crown',cex=1,line=2.5,side=2)
grid(nx=NULL,ny=NA,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c('Old leaves','Young leaves'),pch=c(15,16),lty=c(2,1),cex=0.8,bg='white',box.col=0)
box()
screen(5)
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(c(MR[1:3]),H,ylim=c(0.5,3.5),xlim=c(0.5,4),lty=2,lwd=2,type='b',pch=15,axes=F,ann=F,cex.lab=1)
points(c(MR[4:6]),H,lty=1,lwd=2,type='b',pch=16)
for (i in 1:3){
add.errorbars (MR[i],H[i],SE=sdR[i],direction='hor',barlen=0.04)
}
axis(2,at=c(1,2,3),labels=c('Inf','Med','Sup'))
axis(1,at=c(0:4),labels=c(0:4))
mtext(expression ('Rd'~at~'25�C'~(�mol~m^-2~s^-1)),side=1,line=2.5,cex=1)
mtext('Vertical position within the crown',cex=1,line=2.5,side=2)
grid(nx=NULL,ny=NA,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c('Old leaves','Young leaves'),pch=c(15,16),lty=c(2,1),cex=0.8,bg='white',box.col=0)
box()

screen(6)
AGE2 <- as.numeric(levels(as.factor(D2$AGEC3)))
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(AGE2, KPC3,col=0,an=F,axes=F,
          cex.lab=1.2,ylim=c(0,2),xlim=c(13.5,26.5))
rect(13,0,15.5,2,border=NA,col=palette(gray(seq(0,.8,len = 10)))[10])
rect(25.5,0,26.5,2,border=NA,col=palette(gray(seq(0,.8,len = 10)))[10])
par(new=T)
plot(AGE2, KPC3,type='b',col=1,lwd=2,axes=F,ann=F,pch=16,
          cex.lab=1.2,ylim=c(0,2),xlim=c(13.5,26.5))
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
axis(2,labels=c(0,0.5,1,1.5,2),at=c(0,0.5,1,1.5,2))
mtext(expression('Leaf-specific conductivity'~(mmol~m^-2~s^-1~MPa^-1)),side=2,line=2.5,cex=1)
abline(v=18.5,lty='dotted')
mtext('Age (month)',side=1,line=2.5,cex=1)
axis(1,labels=c(7:13)*2,at=c(7:13)*2)
for (i in 1:10){
add.errorbars(AGE2[i],KPC3[i],SE=sdKPC3[i],direction='vert',color=1)}
box()

screen(7)
A <- as.numeric(levels(as.factor(D3$AGE)))
par(mar=c(4,4,1,1))
par(yaxs='i',xaxs='i')
plot(A, MLW[P=='S'],type='b',lty=3,pch=16,lwd=2,cex.lab=1,xlab='Age (months)',ylab='Leaf width (cm)',ylim=c(1,6),xlim=c(0,32),ann=F)
points(A,MLW[P=='M'],type='b',lty=2,pch=15,lwd=2)
points(A,MLW[P=='B'],type='b',lty=1,pch=17,lwd=2)
grid(nx=NA,ny=NULL,col=1)
text(A,c(4.5,rep(5.5,6)),labels=c('a','b','c',rep('d',4)),cex=1,font=3)
legend('bottomright',legend=c('Small trees,','Medium trees','Big trees'), pch=c(16,15,17),lty=c(3,2,1),cex=0.8,bg='white',box.col=0)
mtext('Age (month)',side=1,line=2.5,cex=1)
mtext('Leaf width (m)',side=2,line=2.5,cex=1)
for (i in 1:7){
add.errorbars(A[i],MLW[P=='S'][i],SE=sdLW[P=='S'][i],direction='vert')}
box()

close.screen()











