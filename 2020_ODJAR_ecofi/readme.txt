ECOFI: A database of sugar and energy cane field trials. Christina Mathias, Chaput Maxime, Martiné Jean-François, Auzoux Sandrine. 2020. Open Data Journal for Agricultural Research, 6 : 14-18.
https://doi.org/10.18174/odjar.v6i0.16322
