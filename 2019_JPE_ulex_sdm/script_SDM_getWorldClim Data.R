# To get worldclim Data
require(raster)

dir.create('WORLDCLIM/CURRENT_2,5min')

getData(name='worldclim',download = T,path='WORLDCLIM/CURRENT_2,5min/',var='bio',res=2.5,lat=-40,lon=170)

getData(name='alt',download = T,path='WORLDCLIM/Elevation/',res=2.5,country='NZL')









#getData(name='worldclim',download = T,path='WORLDCLIM',var='bio',res=0.5,lat=-40,lon=170)

Model <- c( "AC", "BC", "CC", "CE", "CN", "GF", "GD", "GS", "HD", "HG", "HE", "IN", "IP", "MI", "MR", "MC", "MP", "MG", "NO")
RCP <- c(45, 60, 85)
YEAR <- c(50,70)

#for (i in 1:length(Model)){
i=2
  for (j in 1:length(RCP)){
    for (k in 1:length(YEAR)){
      getData(name='CMIP5',download = T,path='WORLDCLIM/FUTURE_2,5min/',var='bio',res=2.5,lat=-40,lon=170,model=Model[i],rcp=RCP[j],year=YEAR[k])
    }
  }
}
