Climatic niche shift of an invasive shrub (Ulex europaeus): A global scale comparison in native and introduced regions. Christina Mathias, Limbada Fawziah, Atlan Anne. 2019. Journal of Plant Ecology, 13 (1) : 42-50.
https://doi.org/10.1093/jpe/rtz041
