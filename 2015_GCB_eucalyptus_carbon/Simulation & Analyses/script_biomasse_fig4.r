set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set2)

D1 <- read.table('All_datas.txt',header=T)
attach(D1)
AGE <- as.numeric(levels(as.factor(MONb)))

D2 <- read.table('Biomass2.txt',header=T)


D2HC1_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='C1']*(CAPb2[MONb==12][TRATb[MONb==12]=='C1']/pi/100)^2
D2HC3_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='C3']*(CAPb2[MONb==12][TRATb[MONb==12]=='C3']/pi/100)^2
D2HS1_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='S1']*(CAPb2[MONb==12][TRATb[MONb==12]=='S1']/pi/100)^2
D2HS3_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='S3']*(CAPb2[MONb==12][TRATb[MONb==12]=='S3']/pi/100)^2
D2HC1_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='C1']*(CAPb2[MONb==22][TRATb[MONb==22]=='C1']/pi/100)^2
D2HC3_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='C3']*(CAPb2[MONb==22][TRATb[MONb==22]=='C3']/pi/100)^2
D2HS1_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='S1']*(CAPb2[MONb==22][TRATb[MONb==22]=='S1']/pi/100)^2
D2HS3_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='S3']*(CAPb2[MONb==22][TRATb[MONb==22]=='S3']/pi/100)^2
D2HC1_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='C1']*(CAPb2[MONb==36][TRATb[MONb==36]=='C1']/pi/100)^2
D2HC3_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='C3']*(CAPb2[MONb==36][TRATb[MONb==36]=='C3']/pi/100)^2
D2HS1_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='S1']*(CAPb2[MONb==36][TRATb[MONb==36]=='S1']/pi/100)^2
D2HS3_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='S3']*(CAPb2[MONb==36][TRATb[MONb==36]=='S3']/pi/100)^2

HC1_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='C1']
HC3_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='C3']
HS1_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='S1']
HS3_1 <- ALTb2[MONb==12][TRATb[MONb==12]=='S3']
HC1_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='C1']
HC3_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='C3']
HS1_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='S1']
HS3_2 <- ALTb2[MONb==22][TRATb[MONb==22]=='S3']
HC1_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='C1']
HC3_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='C3']
HS1_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='S1']
HS3_3 <- ALTb2[MONb==36][TRATb[MONb==36]=='S3']

LC1_1 <-c() ; SC1_1 <-c() ; BC1_1 <- c()
LC3_1 <-c() ; SC3_1 <-c() ; BC3_1 <- c()
LS1_1 <-c() ; SS1_1 <-c() ; BS1_1 <- c()
LS3_1 <-c() ; SS3_1 <-c() ; BS3_1 <- c()
LC1_2 <-c() ; SC1_2 <-c() ; BC1_2 <- c()
LC3_2 <-c() ; SC3_2 <-c() ; BC3_2 <- c()
LS1_2 <-c() ; SS1_2 <-c() ; BS1_2 <- c()
LS3_2 <-c() ; SS3_2 <-c() ; BS3_2 <- c()
LC1_3 <-c() ; SC1_3 <-c() ; BC1_3 <- c()
LC3_3 <-c() ; SC3_3 <-c() ; BC3_3 <- c()
LS1_3 <-c() ; SS1_3 <-c() ; BS1_3 <- c()
LS3_3 <-c() ; SS3_3 <-c() ; BS3_3 <- c()

MC1_1 <- data.frame(HC1_1,D2HC1_1)[order(data.frame(HC1_1,D2HC1_1)[,1]),]
MC1_2 <- data.frame(HC1_2,D2HC1_2)[order(data.frame(HC1_2,D2HC1_2)[,1]),]
MC1_3 <- data.frame(HC1_3,D2HC1_3)[order(data.frame(HC1_3,D2HC1_3)[,1]),]
MC3_1 <- data.frame(HC3_1,D2HC3_1)[order(data.frame(HC3_1,D2HC3_1)[,1]),]
MC3_2 <- data.frame(HC3_2,D2HC3_2)[order(data.frame(HC3_2,D2HC3_2)[,1]),]
MC3_3 <- data.frame(HC3_3,D2HC3_3)[order(data.frame(HC3_3,D2HC3_3)[,1]),]
MS1_1 <- data.frame(HS1_1,D2HS1_1)[order(data.frame(HS1_1,D2HS1_1)[,1]),]
MS1_2 <- data.frame(HS1_2,D2HS1_2)[order(data.frame(HS1_2,D2HS1_2)[,1]),]
MS1_3 <- data.frame(HS1_3,D2HS1_3)[order(data.frame(HS1_3,D2HS1_3)[,1]),]
MS3_1 <- data.frame(HS3_1,D2HS3_1)[order(data.frame(HS3_1,D2HS3_1)[,1]),]
MS3_2 <- data.frame(HS3_2,D2HS3_2)[order(data.frame(HS3_2,D2HS3_2)[,1]),]
MS3_3 <- data.frame(HS3_3,D2HS3_3)[order(data.frame(HS3_3,D2HS3_3)[,1]),]





for (i in MC1_1[,2]){
LC1_1 <- c(LC1_1,0.622287+402.756429*i)
SC1_1 <- c(SC1_1,0.017028+7.370808*i^ 0.646703 + 27.931636*i^ 0.624991)
BC1_1 <- c(BC1_1,0.302777+420.817853*i)}
for (i in MC3_1[,2]){
LC3_1 <- c(LC3_1,0.685409+155.456263*i)
SC3_1 <- c(SC3_1,0.017028+7.370808*i^ 0.646703 + 27.931636*i^ 0.624991)
BC3_1 <- c(BC3_1,0.257321+171.356865*i)}
for (i in MS1_1[,2]){
LS1_1 <- c(LS1_1,0.607331+ 269.363395*i)
SS1_1 <- c(SS1_1,0.017028+7.370808*i^ 0.646703 + 27.931636*i^ 0.624991)
BS1_1 <- c(BS1_1,0.514545+209.104341*i)}
for (i in MS3_1[,2]){
LS3_1 <- c(LS3_1,0.370913+163.006382*i)
SS3_1 <- c(SS3_1,0.017028+7.370808*i^ 0.646703 + 27.931636*i^ 0.624991)
BS3_1 <- c(BS3_1,157.338325*i)}

for (i in MC1_2[,2]){
LC1_2 <- c(LC1_2,83.079070*i)
SC1_2 <- c(SC1_2,15.124793*i^ 0.807512 + 138.814684*i)
BC1_2 <- c(BC1_2,84.439470*i)}
for (i in MC3_2[,2]){
LC3_2 <- c(LC3_2,43.341547*i)
SC3_2 <- c(SC3_2,15.124793*i^ 0.807512  + 125.680463*i)
BC3_2 <- c(BC3_2,38.364966*i)}
for (i in MS1_2[,2]){
LS1_2 <- c(LS1_2,75.964807*i)
SS1_2 <- c(SS1_2,15.124793*i^ 0.807512  + 140.656578*i)
BS1_2 <- c(BS1_2,68.367801*i)}
for (i in MS3_2[,2]){
LS3_2 <- c(LS3_2,0.751047+33.103940*i)
SS3_2 <- c(SS3_2,15.124793*i^ 0.807512  + 133.830768*i)
BS3_2 <- c(BS3_2,34.648531*i)}

for (i in MC1_3[,2]){
LC1_3 <- c(LC1_3,25.180985*i)
SC1_3 <- c(SC1_3,27.152379*i + 143.440537*i)
BC1_3 <- c(BC1_3,28.726482*i)}
for (i in MC3_3[,2]){
LC3_3 <- c(LC3_3,20.750271*i)
SC3_3 <- c(SC3_3,21.673414*i  + 136.735546*i)
BC3_3 <- c(BC3_3,21.618625*i)}
for (i in MS1_3[,2]){
LS1_3 <- c(LS1_3,23.320648*i)
SS1_3 <- c(SS1_3,27.170910*i  + 149.525659*i)
BS1_3 <- c(BS1_3,25.644491*i)}
for (i in MS3_3[,2]){
LS3_3 <- c(LS3_3,19.168842*i)
SS3_3 <- c(SS3_3,22.871782*i + 139.768852*i)
BS3_3 <- c(BS3_3,19.743665*i)}


PLC1_1 <- LC1_1 / (LC1_1+BC1_1+SC1_1) ;PBC1_1 <- BC1_1 / (LC1_1+BC1_1+SC1_1) ;PSC1_1 <- SC1_1 / (LC1_1+BC1_1+SC1_1)
PLC3_1 <- LC3_1 / (LC3_1+BC3_1+SC3_1) ;PBC3_1 <- BC3_1 / (LC3_1+BC3_1+SC3_1) ;PSC3_1 <- SC3_1 / (LC3_1+BC3_1+SC3_1)
PLS1_1 <- LS1_1 / (LS1_1+BS1_1+SS1_1) ;PBS1_1 <- BS1_1 / (LS1_1+BS1_1+SS1_1) ;PSS1_1 <- SS1_1 / (LS1_1+BS1_1+SS1_1)
PLS3_1 <- LS3_1 / (LS3_1+BS3_1+SS3_1) ;PBS3_1 <- BS3_1 / (LS3_1+BS3_1+SS3_1) ;PSS3_1 <- SS3_1 / (LS3_1+BS3_1+SS3_1)
PLC1_2 <- LC1_2 / (LC1_2+BC1_2+SC1_2) ;PBC1_2 <- BC1_2 / (LC1_2+BC1_2+SC1_2) ;PSC1_2 <- SC1_2 / (LC1_2+BC1_2+SC1_2)
PLC3_2 <- LC3_2 / (LC3_2+BC3_2+SC3_2) ;PBC3_2 <- BC3_2 / (LC3_2+BC3_2+SC3_2) ;PSC3_2 <- SC3_2 / (LC3_2+BC3_2+SC3_2)
PLS1_2 <- LS1_2 / (LS1_2+BS1_2+SS1_2) ;PBS1_2 <- BS1_2 / (LS1_2+BS1_2+SS1_2) ;PSS1_2 <- SS1_2 / (LS1_2+BS1_2+SS1_2)
PLS3_2 <- LS3_2 / (LS3_2+BS3_2+SS3_2) ;PBS3_2 <- BS3_2 / (LS3_2+BS3_2+SS3_2) ;PSS3_2 <- SS3_2 / (LS3_2+BS3_2+SS3_2)
PLC1_3 <- LC1_3 / (LC1_3+BC1_3+SC1_3) ;PBC1_3 <- BC1_3 / (LC1_3+BC1_3+SC1_3) ;PSC1_3 <- SC1_3 / (LC1_3+BC1_3+SC1_3)
PLC3_3 <- LC3_3 / (LC3_3+BC3_3+SC3_3) ;PBC3_3 <- BC3_3 / (LC3_3+BC3_3+SC3_3) ;PSC3_3 <- SC3_3 / (LC3_3+BC3_3+SC3_3)
PLS1_3 <- LS1_3 / (LS1_3+BS1_3+SS1_3) ;PBS1_3 <- BS1_3 / (LS1_3+BS1_3+SS1_3) ;PSS1_3 <- SS1_3 / (LS1_3+BS1_3+SS1_3)
PLS3_3 <- LS3_3 / (LS3_3+BS3_3+SS3_3) ;PBS3_3 <- BS3_3 / (LS3_3+BS3_3+SS3_3) ;PSS3_3 <- SS3_3 / (LS3_3+BS3_3+SS3_3)

TRC1_1 <- (MC1_1[,1]-min(MC1_1[,1],na.rm=T))/(max(MC1_1[,1],na.rm=T)-min(MC1_1[,1],na.rm=T))
TRC1_2 <- (MC1_2[,1]-min(MC1_2[,1],na.rm=T))/(max(MC1_2[,1],na.rm=T)-min(MC1_2[,1],na.rm=T))
TRC1_3 <- (MC1_3[,1]-min(MC1_3[,1],na.rm=T))/(max(MC1_3[,1],na.rm=T)-min(MC1_3[,1],na.rm=T))
TRC3_1 <- (MC3_1[,1]-min(MC3_1[,1],na.rm=T))/(max(MC3_1[,1],na.rm=T)-min(MC3_1[,1],na.rm=T))
TRC3_2 <- (MC3_2[,1]-min(MC3_2[,1],na.rm=T))/(max(MC3_2[,1],na.rm=T)-min(MC3_2[,1],na.rm=T))
TRC3_3 <- (MC3_3[,1]-min(MC3_3[,1],na.rm=T))/(max(MC3_3[,1],na.rm=T)-min(MC3_3[,1],na.rm=T))
TRS1_1 <- (MS1_1[,1]-min(MS1_1[,1],na.rm=T))/(max(MS1_1[,1],na.rm=T)-min(MS1_1[,1],na.rm=T))
TRS1_2 <- (MS1_2[,1]-min(MS1_2[,1],na.rm=T))/(max(MS1_2[,1],na.rm=T)-min(MS1_2[,1],na.rm=T))
TRS1_3 <- (MS1_3[,1]-min(MS1_3[,1],na.rm=T))/(max(MS1_3[,1],na.rm=T)-min(MS1_3[,1],na.rm=T))
TRS3_1 <- (MS3_1[,1]-min(MS3_1[,1],na.rm=T))/(max(MS3_1[,1],na.rm=T)-min(MS3_1[,1],na.rm=T))
TRS3_2 <- (MS3_2[,1]-min(MS3_2[,1],na.rm=T))/(max(MS3_2[,1],na.rm=T)-min(MS3_2[,1],na.rm=T))
TRS3_3 <- (MS3_3[,1]-min(MS3_3[,1],na.rm=T))/(max(MS3_3[,1],na.rm=T)-min(MS3_3[,1],na.rm=T))
TRC1_1 <- TRC1_1[is.na(TRC1_1)==F] ; PSC1_1 <- PSC1_1[is.na(PSC1_1)==F] ; PBC1_1 <- PBC1_1[is.na(PBC1_1)==F];PLC1_1 <- PLC1_1[is.na(PLC1_1)==F]
TRC1_2 <- TRC1_2[is.na(TRC1_2)==F] ; PSC1_2 <- PSC1_2[is.na(PSC1_2)==F] ; PBC1_2 <- PBC1_2[is.na(PBC1_2)==F];PLC1_2 <- PLC1_2[is.na(PLC1_2)==F]
TRC1_3 <- TRC1_3[is.na(TRC1_3)==F] ; PSC1_3 <- PSC1_3[is.na(PSC1_3)==F] ; PBC1_3 <- PBC1_3[is.na(PBC1_3)==F];PLC1_3 <- PLC1_3[is.na(PLC1_3)==F]
TRC3_1 <- TRC3_1[is.na(TRC3_1)==F] ; PSC3_1 <- PSC3_1[is.na(PSC3_1)==F] ; PBC3_1 <- PBC3_1[is.na(PBC3_1)==F];PLC3_1 <- PLC3_1[is.na(PLC3_1)==F]
TRC3_2 <- TRC3_2[is.na(TRC3_2)==F] ; PSC3_2 <- PSC3_2[is.na(PSC3_2)==F] ; PBC3_2 <- PBC3_2[is.na(PBC3_2)==F];PLC3_2 <- PLC3_2[is.na(PLC3_2)==F]
TRC3_3 <- TRC3_3[is.na(TRC3_3)==F] ; PSC3_3 <- PSC3_3[is.na(PSC3_3)==F] ; PBC3_3 <- PBC3_3[is.na(PBC3_3)==F];PLC3_3 <- PLC3_3[is.na(PLC3_3)==F]
TRS1_1 <- TRS1_1[is.na(TRS1_1)==F] ; PSS1_1 <- PSS1_1[is.na(PSS1_1)==F] ; PBS1_1 <- PBS1_1[is.na(PBS1_1)==F];PLS1_1 <- PLS1_1[is.na(PLS1_1)==F]
TRS1_2 <- TRS1_2[is.na(TRS1_2)==F] ; PSS1_2 <- PSS1_2[is.na(PSS1_2)==F] ; PBS1_2 <- PBS1_2[is.na(PBS1_2)==F];PLS1_2 <- PLS1_2[is.na(PLS1_2)==F]
TRS1_3 <- TRS1_3[is.na(TRS1_3)==F] ; PSS1_3 <- PSS1_3[is.na(PSS1_3)==F] ; PBS1_3 <- PBS1_3[is.na(PBS1_3)==F];PLS1_3 <- PLS1_3[is.na(PLS1_3)==F]
TRS3_1 <- TRS3_1[is.na(TRS3_1)==F] ; PSS3_1 <- PSS3_1[is.na(PSS3_1)==F] ; PBS3_1 <- PBS3_1[is.na(PBS3_1)==F];PLS3_1 <- PLS3_1[is.na(PLS3_1)==F]
TRS3_2 <- TRS3_2[is.na(TRS3_2)==F] ; PSS3_2 <- PSS3_2[is.na(PSS3_2)==F] ; PBS3_2 <- PBS3_2[is.na(PBS3_2)==F];PLS3_2 <- PLS3_2[is.na(PLS3_2)==F]
TRS3_3 <- TRS3_3[is.na(TRS3_3)==F] ; PSS3_3 <- PSS3_3[is.na(PSS3_3)==F] ; PBS3_3 <- PBS3_3[is.na(PBS3_3)==F];PLS3_3 <- PLS3_3[is.na(PLS3_3)==F]



mycols <- adjustcolor(palette(), alpha.f = 0.2)
mysalmon <- adjustcolor('salmon', alpha.f = 0.4)

par(mfrow=c(4,3))
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(TRC1_1,PSC1_1,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC1_1,PSC1_1+PBC1_1,type='l')
polygon(c(TRC1_1[1:694],rev(TRC1_1[1:694])),c(rep(0,length(TRC1_1[1:694])),rev(PSC1_1[1:694])),col='brown',border=NULL)
polygon(c(TRC1_1,rev(TRC1_1)),c(PSC1_1,rev(PSC1_1+PBC1_1)),col='dark green',border=NULL)
polygon(c(TRC1_1,rev(TRC1_1)),c(PSC1_1+PBC1_1,rev(rep(1,length(TRC1_1)))),col='light green',border=NULL)

plot(TRC1_2,PSC1_2,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC1_2,PSC1_2+PBC1_2,type='l')
polygon(c(TRC1_2,rev(TRC1_2)),c(rep(0,length(TRC1_2)),rev(PSC1_2)),col='brown',border=NULL)
polygon(c(TRC1_2,rev(TRC1_2)),c(PSC1_2,rev(PSC1_2+PBC1_2)),col='dark green',border=NULL)
polygon(c(TRC1_2,rev(TRC1_2)),c(PSC1_2+PBC1_2,rev(rep(1,length(TRC1_2)))),col='light green',border=NULL)

plot(TRC1_3,PSC1_3,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC1_3,PSC1_3+PBC1_3,type='l')
polygon(c(TRC1_3,rev(TRC1_3)),c(rep(0,length(TRC1_3)),rev(PSC1_3)),col='brown',border=NULL)
polygon(c(TRC1_3,rev(TRC1_3)),c(PSC1_3,rev(PSC1_3+PBC1_3)),col='dark green',border=NULL)
polygon(c(TRC1_3,rev(TRC1_3)),c(PSC1_3+PBC1_3,rev(rep(1,length(TRC1_3)))),col='light green',border=NULL)

plot(TRC3_1,PSC3_1,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC3_1,PSC3_1+PBC3_1,type='l')
polygon(c(TRC3_1[1:694],rev(TRC3_1[1:694])),c(rep(0,length(TRC3_1[1:694])),rev(PSC3_1[1:694])),col='brown',border=NULL)
polygon(c(TRC3_1,rev(TRC3_1)),c(PSC3_1,rev(PSC3_1+PBC3_1)),col='dark green',border=NULL)
polygon(c(TRC3_1,rev(TRC3_1)),c(PSC3_1+PBC3_1,rev(rep(1,length(TRC3_1)))),col='light green',border=NULL)

plot(TRC3_2,PSC3_2,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC3_2,PSC3_2+PBC3_2,type='l')
polygon(c(TRC3_2,rev(TRC3_2)),c(rep(0,length(TRC3_2)),rev(PSC3_2)),col='brown',border=NULL)
polygon(c(TRC3_2,rev(TRC3_2)),c(PSC3_2,rev(PSC3_2+PBC3_2)),col='dark green',border=NULL)
polygon(c(TRC3_2,rev(TRC3_2)),c(PSC3_2+PBC3_2,rev(rep(1,length(TRC3_2)))),col='light green',border=NULL)

plot(TRC3_3,PSC3_3,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRC3_3,PSC3_3+PBC3_3,type='l')
polygon(c(TRC3_3,rev(TRC3_3)),c(rep(0,length(TRC3_3)),rev(PSC3_3)),col='brown',border=NULL)
polygon(c(TRC3_3,rev(TRC3_3)),c(PSC3_3,rev(PSC3_3+PBC3_3)),col='dark green',border=NULL)
polygon(c(TRC3_3,rev(TRC3_3)),c(PSC3_3+PBC3_3,rev(rep(1,length(TRC3_3)))),col='light green',border=NULL)

plot(TRS1_1,PSS1_1,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS1_1,PSS1_1+PBS1_1,type='l')
polygon(c(TRS1_1[1:694],rev(TRS1_1[1:694])),c(rep(0,length(TRS1_1[1:694])),rev(PSS1_1[1:694])),col='brown',border=NULL)
polygon(c(TRS1_1,rev(TRS1_1)),c(PSS1_1,rev(PSS1_1+PBS1_1)),col='dark green',border=NULL)
polygon(c(TRS1_1,rev(TRS1_1)),c(PSS1_1+PBS1_1,rev(rep(1,length(TRS1_1)))),col='light green',border=NULL)

plot(TRS1_2,PSS1_2,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS1_2,PSS1_2+PBS1_2,type='l')
polygon(c(TRS1_2,rev(TRS1_2)),c(rep(0,length(TRS1_2)),rev(PSS1_2)),col='brown',border=NULL)
polygon(c(TRS1_2,rev(TRS1_2)),c(PSS1_2,rev(PSS1_2+PBS1_2)),col='dark green',border=NULL)
polygon(c(TRS1_2,rev(TRS1_2)),c(PSS1_2+PBS1_2,rev(rep(1,length(TRS1_2)))),col='light green',border=NULL)

plot(TRS1_3,PSS1_3,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS1_3,PSS1_3+PBS1_3,type='l')
polygon(c(TRS1_3,rev(TRS1_3)),c(rep(0,length(TRS1_3)),rev(PSS1_3)),col='brown',border=NULL)
polygon(c(TRS1_3,rev(TRS1_3)),c(PSS1_3,rev(PSS1_3+PBS1_3)),col='dark green',border=NULL)
polygon(c(TRS1_3,rev(TRS1_3)),c(PSS1_3+PBS1_3,rev(rep(1,length(TRS1_3)))),col='light green',border=NULL)

plot(TRS3_1,PSS3_1,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS3_1,PSS3_1+PBS3_1,type='l')
polygon(c(TRS3_1[1:694],rev(TRS3_1[1:694])),c(rep(0,length(TRS3_1[1:694])),rev(PSS3_1[1:694])),col='brown',border=NULL)
polygon(c(TRS3_1,rev(TRS3_1)),c(PSS3_1,rev(PSS3_1+PBS3_1)),col='dark green',border=NULL)
polygon(c(TRS3_1,rev(TRS3_1)),c(PSS3_1+PBS3_1,rev(rep(1,length(TRS3_1)))),col='light green',border=NULL)

plot(TRS3_2,PSS3_2,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS3_2,PSS3_2+PBS3_2,type='l')
polygon(c(TRS3_2,rev(TRS3_2)),c(rep(0,length(TRS3_2)),rev(PSS3_2)),col='brown',border=NULL)
polygon(c(TRS3_2,rev(TRS3_2)),c(PSS3_2,rev(PSS3_2+PBS3_2)),col='dark green',border=NULL)
polygon(c(TRS3_2,rev(TRS3_2)),c(PSS3_2+PBS3_2,rev(rep(1,length(TRS3_2)))),col='light green',border=NULL)

plot(TRS3_3,PSS3_3,type='l',ylim=c(0,1),xlim=c(0,1),mgp=c(2.5,1,0),xlab='Tree rank',ylab='Proportion of AG biomass',axes=T,cex.lab=1.2)
points(TRS3_3,PSS3_3+PBS3_3,type='l')
polygon(c(TRS3_3,rev(TRS3_3)),c(rep(0,length(TRS3_3)),rev(PSS3_3)),col='brown',border=NULL)
polygon(c(TRS3_3,rev(TRS3_3)),c(PSS3_3,rev(PSS3_3+PBS3_3)),col='dark green',border=NULL)
polygon(c(TRS3_3,rev(TRS3_3)),c(PSS3_3+PBS3_3,rev(rep(1,length(TRS3_3)))),col='light green',border=NULL)

