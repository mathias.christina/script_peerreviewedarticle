set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
set6<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF")
set7<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF_3ans")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set6)
require(Maeswrap)

setwd(set7)
dC110 <- readdayflux('1_Dayflx.dat')
setwd(set6)
dC111 <- read.table('1_Dayflx.dat')
dC112 <- read.table('2_Dayflx.dat')
dC113 <- read.table('3_Dayflx.dat')
setwd(set7)
dC120 <- readdayflux('5_Dayflx.dat')
setwd(set6)
dC121 <- read.table('4_Dayflx.dat')
dC122 <- read.table('5_Dayflx.dat')
dC123 <- read.table('6_Dayflx.dat')
setwd(set7)
dC130 <- readdayflux('9_Dayflx.dat')
setwd(set6)
dC131 <- read.table('7_Dayflx.dat')
dC132 <- read.table('8_Dayflx.dat')
dC133 <- read.table('9_Dayflx.dat')

setwd(set7)
dC310 <- readdayflux('13_Dayflx.dat')
setwd(set6)
dC311 <- read.table('10_Dayflx.dat')
dC312 <- read.table('11_Dayflx.dat')
dC313 <- read.table('12_Dayflx.dat')
setwd(set7)
dC320 <- readdayflux('17_Dayflx.dat')
setwd(set6)
dC321 <- read.table('13_Dayflx.dat')
dC322 <- read.table('14_Dayflx.dat')
dC323 <- read.table('15_Dayflx.dat')
setwd(set7)
dC330 <- readdayflux('21_Dayflx.dat')
setwd(set6)
dC331 <- read.table('16_Dayflx.dat')
dC332 <- read.table('17_Dayflx.dat')
dC333 <- read.table('18_Dayflx.dat')

setwd(set7)
dS110 <- readdayflux('25_Dayflx.dat')
setwd(set6)
dS111 <- read.table('19_Dayflx.dat')
dS112 <- read.table('20_Dayflx.dat') # vide
dS113 <- read.table('21_Dayflx.dat')
setwd(set7)
dS120 <- readdayflux('29_Dayflx.dat')
setwd(set6)
dS121 <- read.table('22_Dayflx.dat') # jour 305 arret au milieu
dS122 <- read.table('23_Dayflx.dat')
dS123 <- read.table('24_Dayflx.dat')
setwd(set7)
dS130 <- readdayflux('33_Dayflx.dat')
setwd(set6)
dS131 <- read.table('25_Dayflx.dat')
dS132 <- read.table('26_Dayflx.dat')
dS133 <- read.table('27_Dayflx.dat')

setwd(set7)
dS310 <- readdayflux('37_Dayflx.dat')
setwd(set6)
dS311 <- read.table('28_Dayflx.dat')
dS312 <- read.table('29_Dayflx.dat')
dS313 <- read.table('30_Dayflx.dat')
setwd(set7)
dS320 <- readdayflux('41_Dayflx.dat')
setwd(set6)
dS321 <- read.table('31_Dayflx.dat')
dS322 <- read.table('32_Dayflx.dat')
dS323 <- read.table('33_Dayflx.dat')
setwd(set7)
dS330 <- readdayflux('45_Dayflx.dat')
setwd(set6)
dS331 <- read.table('34_Dayflx.dat')
dS332 <- read.table('35_Dayflx.dat')
dS333 <- read.table('36_Dayflx.dat')
setwd(set4)
names(dC111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dC311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

setwd(set3)
require (Maeswrap)
B11 <- read.table('met2011_C.dat',skip=25)
B12 <- read.table('met2012_C.dat',skip=25)
B13 <- read.table('met2013_C.dat',skip=26)

PPT <- c()

for (i in 1:365){
PPT <- c(PPT, sum(B11[,10][((i-1)*48+1):(i*48)]))}
for (i in 1:366){
PPT <- c(PPT, sum(B12[,10][((i-1)*48+1):(i*48)]))}
for (i in 1:168){
PPT <- c(PPT, sum(B13[,8][((i-1)*48+1):(i*48)]))}

LEC11 <- c();LEC12 <-c();LEC13<-c()
LEC31 <- c();LEC32 <-c();LEC33<-c()
LES11 <- c();LES12 <-c();LES13<-c()
LES31 <- c();LES32 <-c();LES33<-c()

for (i in 1:197){
LEC11 <- c(LEC11, sum(dC110$totLE1[dC110$DOY==i],na.rm=T)*18*10^-3/(length(dC110$Tree[dC110$DOY==i])*6))
LEC12 <- c(LEC12, sum(dC120$totLE1[dC120$DOY==i],na.rm=T)*18*10^-3/(length(dC120$Tree[dC120$DOY==i])*6))
LEC13 <- c(LEC13, sum(dC130$totLE1[dC130$DOY==i],na.rm=T)*18*10^-3/(length(dC130$Tree[dC130$DOY==i])*6))
LEC31 <- c(LEC31, sum(dC310$totLE1[dC310$DOY==i],na.rm=T)*18*10^-3/(length(dC310$Tree[dC310$DOY==i])*6))
LEC32 <- c(LEC32, sum(dC310$totLE1[dC310$DOY==i],na.rm=T)*18*10^-3/(length(dC310$Tree[dC310$DOY==i])*6))
LEC33 <- c(LEC33, sum(dC330$totLE1[dC330$DOY==i],na.rm=T)*18*10^-3/(length(dC330$Tree[dC330$DOY==i])*6))
LES11 <- c(LES11, sum(dS110$totLE1[dS110$DOY==i],na.rm=T)*18*10^-3/(length(dS110$Tree[dS110$DOY==i])*6))
LES12 <- c(LES12, sum(dS120$totLE1[dS120$DOY==i],na.rm=T)*18*10^-3/(length(dS120$Tree[dS120$DOY==i])*6))
LES13 <- c(LES13, sum(dS130$totLE1[dS130$DOY==i],na.rm=T)*18*10^-3/(length(dS130$Tree[dS130$DOY==i])*6))
LES31 <- c(LES31, sum(dS310$totLE1[dS310$DOY==i],na.rm=T)*18*10^-3/(length(dS310$Tree[dS310$DOY==i])*6))
LES32 <- c(LES32, sum(dS320$totLE1[dS320$DOY==i],na.rm=T)*18*10^-3/(length(dS320$Tree[dS320$DOY==i])*6))
LES33 <- c(LES33, sum(dS330$totLE1[dS330$DOY==i],na.rm=T)*18*10^-3/(length(dS330$Tree[dS330$DOY==i])*6))}
for (i in 1:365){
LEC11 <- c(LEC11, sum(dC111$totLE1[dC111$DOY==i],na.rm=T)*18*10^-3/(length(dC111$Tree[dC111$DOY==i])*6))
LEC12 <- c(LEC12, sum(dC121$totLE1[dC121$DOY==i],na.rm=T)*18*10^-3/(length(dC121$Tree[dC121$DOY==i])*6))
LEC13 <- c(LEC13, sum(dC131$totLE1[dC131$DOY==i],na.rm=T)*18*10^-3/(length(dC131$Tree[dC131$DOY==i])*6))
LEC31 <- c(LEC31, sum(dC311$totLE1[dC311$DOY==i],na.rm=T)*18*10^-3/(length(dC311$Tree[dC311$DOY==i])*6))
LEC32 <- c(LEC32, sum(dC321$totLE1[dC321$DOY==i],na.rm=T)*18*10^-3/(length(dC321$Tree[dC321$DOY==i])*6))
LEC33 <- c(LEC33, sum(dC331$totLE1[dC331$DOY==i],na.rm=T)*18*10^-3/(length(dC331$Tree[dC331$DOY==i])*6))
LES11 <- c(LES11, sum(dS111$totLE1[dS111$DOY==i],na.rm=T)*18*10^-3/(length(dS111$Tree[dS111$DOY==i])*6))
LES12 <- c(LES12, sum(dS121$totLE1[dS121$DOY==i],na.rm=T)*18*10^-3/(length(dS121$Tree[dS121$DOY==i])*6))
LES13 <- c(LES13, sum(dS131$totLE1[dS131$DOY==i],na.rm=T)*18*10^-3/(length(dS131$Tree[dS131$DOY==i])*6))
LES31 <- c(LES31, sum(dS311$totLE1[dS311$DOY==i],na.rm=T)*18*10^-3/(length(dS311$Tree[dS311$DOY==i])*6))
LES32 <- c(LES32, sum(dS321$totLE1[dS321$DOY==i],na.rm=T)*18*10^-3/(length(dS321$Tree[dS321$DOY==i])*6))
LES33 <- c(LES33, sum(dS331$totLE1[dS331$DOY==i],na.rm=T)*18*10^-3/(length(dS331$Tree[dS331$DOY==i])*6))}
for (i in 1:366){
LEC11 <- c(LEC11, sum(dC112$totLE1[dC112$DOY==i],na.rm=T)*18*10^-3/(length(dC112$Tree[dC112$DOY==i])*6))
LEC12 <- c(LEC12, sum(dC122$totLE1[dC122$DOY==i],na.rm=T)*18*10^-3/(length(dC122$Tree[dC122$DOY==i])*6))
LEC13 <- c(LEC13, sum(dC132$totLE1[dC132$DOY==i],na.rm=T)*18*10^-3/(length(dC132$Tree[dC132$DOY==i])*6))
LEC31 <- c(LEC31, sum(dC312$totLE1[dC312$DOY==i],na.rm=T)*18*10^-3/(length(dC312$Tree[dC312$DOY==i])*6))
LEC32 <- c(LEC32, sum(dC322$totLE1[dC322$DOY==i],na.rm=T)*18*10^-3/(length(dC322$Tree[dC322$DOY==i])*6))
LEC33 <- c(LEC33, sum(dC332$totLE1[dC332$DOY==i],na.rm=T)*18*10^-3/(length(dC332$Tree[dC332$DOY==i])*6))
LES11 <- c(LES11, sum(dS112$totLE1[dS112$DOY==i],na.rm=T)*18*10^-3/(length(dS112$Tree[dS112$DOY==i])*6))
LES12 <- c(LES12, sum(dS122$totLE1[dS122$DOY==i],na.rm=T)*18*10^-3/(length(dS122$Tree[dS122$DOY==i])*6))
LES13 <- c(LES13, sum(dS132$totLE1[dS132$DOY==i],na.rm=T)*18*10^-3/(length(dS132$Tree[dS132$DOY==i])*6))
LES31 <- c(LES31, sum(dS312$totLE1[dS312$DOY==i],na.rm=T)*18*10^-3/(length(dS312$Tree[dS312$DOY==i])*6))
LES32 <- c(LES32, sum(dS322$totLE1[dS322$DOY==i],na.rm=T)*18*10^-3/(length(dS322$Tree[dS322$DOY==i])*6))
LES33 <- c(LES33, sum(dS332$totLE1[dS332$DOY==i],na.rm=T)*18*10^-3/(length(dS332$Tree[dS332$DOY==i])*6))}
for (i in 1:168){
LEC11 <- c(LEC11, sum(dC113$totLE1[dC113$DOY==i],na.rm=T)*18*10^-3/(length(dC113$Tree[dC113$DOY==i])*6))
LEC12 <- c(LEC12, sum(dC123$totLE1[dC123$DOY==i],na.rm=T)*18*10^-3/(length(dC123$Tree[dC123$DOY==i])*6))
LEC13 <- c(LEC13, sum(dC133$totLE1[dC133$DOY==i],na.rm=T)*18*10^-3/(length(dC133$Tree[dC133$DOY==i])*6))
LEC31 <- c(LEC31, sum(dC313$totLE1[dC313$DOY==i],na.rm=T)*18*10^-3/(length(dC313$Tree[dC313$DOY==i])*6))
LEC32 <- c(LEC32, sum(dC323$totLE1[dC323$DOY==i],na.rm=T)*18*10^-3/(length(dC323$Tree[dC323$DOY==i])*6))
LEC33 <- c(LEC33, sum(dC333$totLE1[dC333$DOY==i],na.rm=T)*18*10^-3/(length(dC333$Tree[dC333$DOY==i])*6))
LES11 <- c(LES11, sum(dS113$totLE1[dS113$DOY==i],na.rm=T)*18*10^-3/(length(dS113$Tree[dS113$DOY==i])*6))
LES12 <- c(LES12, sum(dS123$totLE1[dS123$DOY==i],na.rm=T)*18*10^-3/(length(dS123$Tree[dS123$DOY==i])*6))
LES13 <- c(LES13, sum(dS133$totLE1[dS133$DOY==i],na.rm=T)*18*10^-3/(length(dS133$Tree[dS133$DOY==i])*6))
LES31 <- c(LES31, sum(dS313$totLE1[dS313$DOY==i],na.rm=T)*18*10^-3/(length(dS313$Tree[dS313$DOY==i])*6))
LES32 <- c(LES32, sum(dS323$totLE1[dS323$DOY==i],na.rm=T)*18*10^-3/(length(dS323$Tree[dS323$DOY==i])*6))
LES33 <- c(LES33, sum(dS333$totLE1[dS333$DOY==i],na.rm=T)*18*10^-3/(length(dS333$Tree[dS333$DOY==i])*6))}

#LEC11 <- replace(LEC11,c(LEC11==NA),0)
#LEC13 <- replace(LEC13,c(LEC13==NA),0)
#LEC31 <- replace(LEC31,c(LEC31==NA),0)
#LEC32 <- replace(LEC32,c(LEC32==NA),0)
#LEC33 <- replace(LEC33,c(LEC33==NA),0)
#LES31 <- replace(LES31,c(LES31==NA),0)
#LES32 <- replace(LES32,c(LES32==NA),0)
#LES33 <- replace(LES33,c(LES33==NA),0)

LEC1 <- LEC11
LEC3 <- LEC31
LES3 <- LES31
LES1 <- LES12

LEC1 <- c(LEC11+LEC13)/2
LEC3 <- c(LEC31+LEC32+LEC33)/3
LES3 <- c(LES31+LES32+LES33)/3
LES1 <- c(LES12+LES13)/2


par(mar=c(4.5,4.5,0.2,4.5))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(LEC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily transpiration'~(L[H20]~m^-2~d^-1)),
    cex.lab=1.2,col=4,pch=16,ylim=c(0,8))
points(LES3,col=5,pch=16)
points(LEC1,col=2,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,7.5,'2011',font=4)
text(550,7.5,'2012',font=4)
text(850,7.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()




# efficience
setwd(set2)
BIOM <- read.table('datas_biomass.txt',header=T)
HEIGHT <- read.table('datas_height.txt',header=T)
names(BIOM) <- c('TRAT','BLOC','ID',1:29)
names(BIOM) <- c('TRAT','BLOC','ID',7:35)
attach(BIOM)


# annuellement
BC11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'],BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'],BIOM[,32][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
BC31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'],BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'],BIOM[,32][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
BS11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'],BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'],BIOM[,32][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
BS31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'],BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'],BIOM[,32][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,21][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
BC12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'],BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'],BIOM[,32][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
BC32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'],BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'],BIOM[,32][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
BS12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'],BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'],BIOM[,32][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
BS32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'],BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'],BIOM[,32][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,21][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
BC13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'],BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'],BIOM[,32][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
BC33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'],BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'],BIOM[,32][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
BS13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'],BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'],BIOM[,32][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
BS33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'],BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'],BIOM[,32][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,21][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])

HC11 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C1'],HEIGHT[,21][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C1'],HEIGHT[,31][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C1'])
HC31 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C3'],HEIGHT[,21][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C3'],HEIGHT[,31][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C3'])
HS11 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S1'],HEIGHT[,21][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S1'],HEIGHT[,31][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S1'])
HS31 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S3'],HEIGHT[,21][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S3'],HEIGHT[,31][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S3'])
HC12 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C1'],HEIGHT[,21][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C1'],HEIGHT[,31][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C1'])
HC32 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C3'],HEIGHT[,21][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C3'],HEIGHT[,31][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C3'])
HS12 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S1'],HEIGHT[,21][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S1'],HEIGHT[,31][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S1'])
HS32 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S3'],HEIGHT[,21][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S3'],HEIGHT[,31][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S3'])
HC13 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C1'],HEIGHT[,21][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C1'],HEIGHT[,31][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C1'])
HC33 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C3'],HEIGHT[,21][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C3'],HEIGHT[,31][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C3'])
HS13 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S1'],HEIGHT[,21][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S1'],HEIGHT[,31][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S1'])
HS33 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S3'],HEIGHT[,21][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S3'],HEIGHT[,31][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S3'])

PS_S31 <-c();PS_S32 <-c();PS_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
PS_S31 <- c(PS_S31, sum(dS310$totPs[dS310$Tree==j][1:197],na.rm=T)*12+sum(dS311$totPs[dS311$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS312$totPs[dS312$Tree==j][1:168],na.rm=T)*12+sum(dS311$totPs[dS312$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS313$totPs[dS313$Tree==j][1:167],na.rm=T)*12+sum(dS312$totPs[dS312$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
PS_S32 <- c(PS_S32, sum(dS320$totPs[dS320$Tree==j][1:197],na.rm=T)*12+sum(dS321$totPs[dS321$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS322$totPs[dS322$Tree==j][1:168],na.rm=T)*12+sum(dS321$totPs[dS322$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS323$totPs[dS323$Tree==j][1:167],na.rm=T)*12+sum(dS322$totPs[dS322$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
PS_S33 <- c(PS_S33, sum(dS330$totPs[dS330$Tree==j][1:197],na.rm=T)*12+sum(dS331$totPs[dS331$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS332$totPs[dS332$Tree==j][1:168],na.rm=T)*12+sum(dS331$totPs[dS332$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS333$totPs[dS333$Tree==j][1:167],na.rm=T)*12+sum(dS332$totPs[dS332$Tree==j][169:365],na.rm=T)*12)}

PS_C31 <-c();PS_C32 <-c();PS_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
PS_C31 <- c(PS_C31, sum(dC310$totPs[dC310$Tree==j][1:197],na.rm=T)*12+sum(dC311$totPs[dC311$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC312$totPs[dC312$Tree==j][1:168],na.rm=T)*12+sum(dC311$totPs[dC312$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC313$totPs[dC313$Tree==j][1:167],na.rm=T)*12+sum(dC312$totPs[dC312$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
PS_C32 <- c(PS_C32, sum(dC310$totPs[dC310$Tree==j][1:197],na.rm=T)*12+sum(dC321$totPs[dC321$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC322$totPs[dC322$Tree==j][1:168],na.rm=T)*12+sum(dC321$totPs[dC322$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC323$totPs[dC323$Tree==j][1:167],na.rm=T)*12+sum(dC322$totPs[dC322$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
PS_C33 <- c(PS_C33, sum(dC330$totPs[dC330$Tree==j][1:197],na.rm=T)*12+sum(dC331$totPs[dC331$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC332$totPs[dC332$Tree==j][1:168],na.rm=T)*12+sum(dC331$totPs[dC332$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC333$totPs[dC333$Tree==j][1:167],na.rm=T)*12+sum(dC332$totPs[dC332$Tree==j][169:365],na.rm=T)*12)}

PS_C11 <-c();PS_C12 <-c();PS_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
PS_C11 <- c(PS_C11, sum(dC110$totPs[dC110$Tree==j][1:197],na.rm=T)*12+sum(dC111$totPs[dC111$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC112$totPs[dC112$Tree==j][1:168],na.rm=T)*12+sum(dC111$totPs[dC112$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC113$totPs[dC113$Tree==j][1:167],na.rm=T)*12+sum(dC112$totPs[dC112$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
for (j in it){
PS_C12 <- c(PS_C12, sum(dC120$totPs[dC120$Tree==j][1:197],na.rm=T)*12+sum(dC121$totPs[dC121$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC122$totPs[dC122$Tree==j][1:168],na.rm=T)*12+sum(dC121$totPs[dC122$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC123$totPs[dC123$Tree==j][1:167],na.rm=T)*12+sum(dC122$totPs[dC122$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
PS_C13 <- c(PS_C13, sum(dC130$totPs[dC130$Tree==j][1:197],na.rm=T)*12+sum(dC131$totPs[dC131$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC132$totPs[dC132$Tree==j][1:168],na.rm=T)*12+sum(dC131$totPs[dC132$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC133$totPs[dC133$Tree==j][1:167],na.rm=T)*12+sum(dC132$totPs[dC132$Tree==j][169:365],na.rm=T)*12)}

PS_S11 <-c();PS_S12 <-c();PS_S13 <-c()
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==1]
for (j in it){
PS_S11 <- c(PS_S11, sum(dS110$totPs[dS110$Tree==j][1:197],na.rm=T)*12+sum(dS111$totPs[dS111$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS112$totPs[dS112$Tree==j][1:168],na.rm=T)*12+sum(dS111$totPs[dS112$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS113$totPs[dS113$Tree==j][1:167],na.rm=T)*12+sum(dS112$totPs[dS112$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==2]
for (j in it){
PS_S12 <- c(PS_S12, sum(dS120$totPs[dS120$Tree==j][1:197],na.rm=T)*12+sum(dS121$totPs[dS121$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS122$totPs[dS122$Tree==j][1:168],na.rm=T)*12+sum(dS121$totPs[dS122$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS123$totPs[dS123$Tree==j][1:167],na.rm=T)*12+sum(dS122$totPs[dS122$Tree==j][169:365],na.rm=T)*12)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==3]
for (j in it){
PS_S13 <- c(PS_S13, sum(dS130$totPs[dS130$Tree==j][1:197],na.rm=T)*12+sum(dS131$totPs[dS131$Tree==j][1:168],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS132$totPs[dS132$Tree==j][1:168],na.rm=T)*12+sum(dS131$totPs[dS132$Tree==j][169:364],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS133$totPs[dS133$Tree==j][1:167],na.rm=T)*12+sum(dS132$totPs[dS132$Tree==j][169:365],na.rm=T)*12)}

LE_S31 <-c();LE_S32 <-c();LE_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
LE_S31 <- c(LE_S31, sum(dS310$totLE1[dS310$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS311$totLE1[dS311$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S31 <- c(LE_S31, sum(dS312$totLE1[dS312$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS311$totLE1[dS312$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S31 <- c(LE_S31, sum(dS313$totLE1[dS313$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS312$totLE1[dS312$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
LE_S32 <- c(LE_S32, sum(dS320$totLE1[dS320$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS321$totLE1[dS321$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S32 <- c(LE_S32, sum(dS322$totLE1[dS322$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS321$totLE1[dS322$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S32 <- c(LE_S32, sum(dS323$totLE1[dS323$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS322$totLE1[dS322$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
LE_S33 <- c(LE_S33, sum(dS330$totLE1[dS330$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS331$totLE1[dS331$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S33 <- c(LE_S33, sum(dS332$totLE1[dS332$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS331$totLE1[dS332$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S33 <- c(LE_S33, sum(dS333$totLE1[dS333$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS332$totLE1[dS332$Tree==j][169:365],na.rm=T)*18*10^-3)}

LE_C31 <-c();LE_C32 <-c();LE_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
LE_C31 <- c(LE_C31, sum(dC310$totLE1[dC310$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC311$totLE1[dC311$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C31 <- c(LE_C31, sum(dC312$totLE1[dC312$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC311$totLE1[dC312$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C31 <- c(LE_C31, sum(dC313$totLE1[dC313$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC312$totLE1[dC312$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
LE_C32 <- c(LE_C32, sum(dC310$totLE1[dC310$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC321$totLE1[dC321$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C32 <- c(LE_C32, sum(dC322$totLE1[dC322$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC321$totLE1[dC322$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C32 <- c(LE_C32, sum(dC323$totLE1[dC323$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC322$totLE1[dC322$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
LE_C33 <- c(LE_C33, sum(dC330$totLE1[dC330$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC331$totLE1[dC331$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C33 <- c(LE_C33, sum(dC332$totLE1[dC332$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC331$totLE1[dC332$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C33 <- c(LE_C33, sum(dC333$totLE1[dC333$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC332$totLE1[dC332$Tree==j][169:365],na.rm=T)*18*10^-3)}

LE_C11 <-c();LE_C12 <-c();LE_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
LE_C11 <- c(LE_C11, sum(dC110$totLE1[dC110$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC111$totLE1[dC111$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C11 <- c(LE_C11, sum(dC112$totLE1[dC112$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC111$totLE1[dC112$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C11 <- c(LE_C11, sum(dC113$totLE1[dC113$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC112$totLE1[dC112$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
for (j in it){
LE_C12 <- c(LE_C12, sum(dC120$totLE1[dC120$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC121$totLE1[dC121$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C12 <- c(LE_C12, sum(dC122$totLE1[dC122$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC121$totLE1[dC122$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C12 <- c(LE_C12, sum(dC123$totLE1[dC123$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC122$totLE1[dC122$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
LE_C13 <- c(LE_C13, sum(dC130$totLE1[dC130$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dC131$totLE1[dC131$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_C13 <- c(LE_C13, sum(dC132$totLE1[dC132$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dC131$totLE1[dC132$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_C13 <- c(LE_C13, sum(dC133$totLE1[dC133$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dC132$totLE1[dC132$Tree==j][169:365],na.rm=T)*18*10^-3)}

LE_S11 <-c();LE_S12 <-c();LE_S13 <-c()
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==1]
for (j in it){
LE_S11 <- c(LE_S11, sum(dS110$totLE1[dS110$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS111$totLE1[dS111$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S11 <- c(LE_S11, sum(dS112$totLE1[dS112$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS111$totLE1[dS112$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S11 <- c(LE_S11, sum(dS113$totLE1[dS113$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS112$totLE1[dS112$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==2]
for (j in it){
LE_S12 <- c(LE_S12, sum(dS120$totLE1[dS120$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS121$totLE1[dS121$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S12 <- c(LE_S12, sum(dS122$totLE1[dS122$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS121$totLE1[dS122$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S12 <- c(LE_S12, sum(dS123$totLE1[dS123$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS122$totLE1[dS122$Tree==j][169:365],na.rm=T)*18*10^-3)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==3]
for (j in it){
LE_S13 <- c(LE_S13, sum(dS130$totLE1[dS130$Tree==j][1:197],na.rm=T)*18*10^-3+sum(dS131$totLE1[dS131$Tree==j][1:168],na.rm=T)*18*10^-3)}
for (j in it){
LE_S13 <- c(LE_S13, sum(dS132$totLE1[dS132$Tree==j][1:168],na.rm=T)*18*10^-3+sum(dS131$totLE1[dS132$Tree==j][169:364],na.rm=T)*18*10^-3)}
for (j in it){
LE_S13 <- c(LE_S13, sum(dS133$totLE1[dS133$Tree==j][1:167],na.rm=T)*18*10^-3+sum(dS132$totLE1[dS132$Tree==j][169:365],na.rm=T)*18*10^-3)}

APAR_S31 <-c();APAR_S32 <-c();APAR_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS310$absPAR[dS310$Tree==j][1:197],na.rm=T)+sum(dS311$absPAR[dS311$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS312$absPAR[dS312$Tree==j][1:168],na.rm=T)+sum(dS311$absPAR[dS312$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS313$absPAR[dS313$Tree==j][1:167],na.rm=T)+sum(dS312$absPAR[dS312$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS320$absPAR[dS320$Tree==j][1:197],na.rm=T)+sum(dS321$absPAR[dS321$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS322$absPAR[dS322$Tree==j][1:168],na.rm=T)+sum(dS321$absPAR[dS322$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS323$absPAR[dS323$Tree==j][1:167],na.rm=T)+sum(dS322$absPAR[dS322$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS330$absPAR[dS330$Tree==j][1:197],na.rm=T)+sum(dS331$absPAR[dS331$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS332$absPAR[dS332$Tree==j][1:168],na.rm=T)+sum(dS331$absPAR[dS332$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS333$absPAR[dS333$Tree==j][1:167],na.rm=T)+sum(dS332$absPAR[dS332$Tree==j][169:365],na.rm=T))}

APAR_C31 <-c();APAR_C32 <-c();APAR_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC310$absPAR[dC310$Tree==j][1:197],na.rm=T)+sum(dC311$absPAR[dC311$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC312$absPAR[dC312$Tree==j][1:168],na.rm=T)+sum(dC311$absPAR[dC312$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC313$absPAR[dC313$Tree==j][1:167],na.rm=T)+sum(dC312$absPAR[dC312$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC310$absPAR[dC310$Tree==j][1:197],na.rm=T)+sum(dC321$absPAR[dC321$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC322$absPAR[dC322$Tree==j][1:168],na.rm=T)+sum(dC321$absPAR[dC322$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC323$absPAR[dC323$Tree==j][1:167],na.rm=T)+sum(dC322$absPAR[dC322$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC330$absPAR[dC330$Tree==j][1:197],na.rm=T)+sum(dC331$absPAR[dC331$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC332$absPAR[dC332$Tree==j][1:168],na.rm=T)+sum(dC331$absPAR[dC332$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC333$absPAR[dC333$Tree==j][1:167],na.rm=T)+sum(dC332$absPAR[dC332$Tree==j][169:365],na.rm=T))}

APAR_C11 <-c();APAR_C12 <-c();APAR_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC110$absPAR[dC110$Tree==j][1:197],na.rm=T)+sum(dC111$absPAR[dC111$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC112$absPAR[dC112$Tree==j][1:168],na.rm=T)+sum(dC111$absPAR[dC112$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC113$absPAR[dC113$Tree==j][1:167],na.rm=T)+sum(dC112$absPAR[dC112$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC120$absPAR[dC120$Tree==j][1:197],na.rm=T)+sum(dC121$absPAR[dC121$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC122$absPAR[dC122$Tree==j][1:168],na.rm=T)+sum(dC121$absPAR[dC122$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC123$absPAR[dC123$Tree==j][1:167],na.rm=T)+sum(dC122$absPAR[dC122$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC130$absPAR[dC130$Tree==j][1:197],na.rm=T)+sum(dC131$absPAR[dC131$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC132$absPAR[dC132$Tree==j][1:168],na.rm=T)+sum(dC131$absPAR[dC132$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC133$absPAR[dC133$Tree==j][1:167],na.rm=T)+sum(dC132$absPAR[dC132$Tree==j][169:365],na.rm=T))}

APAR_S11 <-c();APAR_S12 <-c();APAR_S13 <-c()
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==1]
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS110$absPAR[dS110$Tree==j][1:197],na.rm=T)+sum(dS111$absPAR[dS111$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS112$absPAR[dS112$Tree==j][1:168],na.rm=T)+sum(dS111$absPAR[dS112$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS113$absPAR[dS113$Tree==j][1:167],na.rm=T)+sum(dS112$absPAR[dS112$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==2]
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS120$absPAR[dS120$Tree==j][1:197],na.rm=T)+sum(dS121$absPAR[dS121$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS122$absPAR[dS122$Tree==j][1:168],na.rm=T)+sum(dS121$absPAR[dS122$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS123$absPAR[dS123$Tree==j][1:167],na.rm=T)+sum(dS122$absPAR[dS122$Tree==j][169:365],na.rm=T))}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==3]
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS130$absPAR[dS130$Tree==j][1:197],na.rm=T)+sum(dS131$absPAR[dS131$Tree==j][1:168],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS132$absPAR[dS132$Tree==j][1:168],na.rm=T)+sum(dS131$absPAR[dS132$Tree==j][169:364],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS133$absPAR[dS133$Tree==j][1:167],na.rm=T)+sum(dS132$absPAR[dS132$Tree==j][169:365],na.rm=T))}

LEC1_A1 <- c(LE_C11[1:36],LE_C12[1:48],LE_C13[1:36])
LEC1_A2 <- c(LE_C11[37:72],LE_C12[49:96],LE_C13[37:72])
LEC1_A3 <- c(LE_C11[73:108],LE_C12[97:144],LE_C13[73:108])
APARC1_A1 <- c(APAR_C11[1:36],APAR_C12[1:48],APAR_C13[1:36])
APARC1_A2 <- c(APAR_C11[37:72],APAR_C12[49:96],APAR_C13[37:72])
APARC1_A3 <- c(APAR_C11[73:108],APAR_C12[97:144],APAR_C13[73:108])
PSC1_A1 <- c(PS_C11[1:36],PS_C12[1:48],PS_C13[1:36])
PSC1_A2 <- c(PS_C11[37:72],PS_C12[49:96],PS_C13[37:72])
PSC1_A3 <- c(PS_C11[73:108],PS_C12[97:144],PS_C13[73:108])
BC1_A1 <- c(BC11[,1],BC12[,1],BC13[,1])
BC1_A2 <- c(BC11[,2],BC12[,2],BC13[,2])
BC1_A3 <- c(BC11[,3],BC12[,3],BC13[,3])
HC1_A1 <- c(HC11[,1],HC12[,1],HC13[,1])
HC1_A2 <- c(HC11[,2],HC12[,2],HC13[,2])
HC1_A3 <- c(HC11[,3],HC12[,3],HC13[,3])

LEC1_A1 <- c(LE_C11[1:36],LE_C13[1:36])
LEC1_A2 <- c(LE_C11[37:72],LE_C13[37:72])
LEC1_A3 <- c(LE_C11[73:108],LE_C13[73:108])
APARC1_A1 <- c(APAR_C11[1:36],APAR_C13[1:36])
APARC1_A2 <- c(APAR_C11[37:72],APAR_C13[37:72])
APARC1_A3 <- c(APAR_C11[73:108],APAR_C13[73:108])
PSC1_A1 <- c(PS_C11[1:36],PS_C13[1:36])
PSC1_A2 <- c(PS_C11[37:72],PS_C13[37:72])
PSC1_A3 <- c(PS_C11[73:108],PS_C13[73:108])
BC1_A1 <- c(BC11[,1],BC13[,1])
BC1_A2 <- c(BC11[,2],BC13[,2])
BC1_A3 <- c(BC11[,3],BC13[,3])
HC1_A1 <- c(HC11[,1],HC13[,1])
HC1_A2 <- c(HC11[,2],HC13[,2])
HC1_A3 <- c(HC11[,3],HC13[,3])

LEC3_A1 <- c(LE_C31[1:36],LE_C32[1:54],LE_C33[1:36]) 
LEC3_A2 <- c(LE_C31[37:72],LE_C32[55:108],LE_C33[37:72])
LEC3_A3 <- c(LE_C31[73:108],LE_C32[109:162],LE_C33[73:108])
APARC3_A1 <- c(APAR_C31[1:36],APAR_C32[1:54],APAR_C33[1:36]) 
APARC3_A2 <- c(APAR_C31[37:72],APAR_C32[55:108],APAR_C33[37:72])
APARC3_A3 <- c(APAR_C31[73:108],APAR_C32[109:162],APAR_C33[73:108])
PSC3_A1 <- c(PS_C31[1:36],PS_C32[1:54],PS_C33[1:36]) 
PSC3_A2 <- c(PS_C31[37:72],PS_C32[55:108],PS_C33[37:72])
PSC3_A3 <- c(PS_C31[73:108],PS_C32[109:162],PS_C33[73:108])
BC3_A1 <- c(BC31[,1],BC32[,1],BC33[,1]) 
BC3_A2 <- c(BC31[,2],BC32[,2],BC33[,2])
BC3_A3 <- c(BC31[,3],BC32[,3],BC33[,3])
HC3_A1 <- c(HC31[,1],HC32[,1],HC33[,1]) 
HC3_A2 <- c(HC31[,2],HC32[,2],HC33[,2])
HC3_A3 <- c(HC31[,3],HC32[,3],HC33[,3])
LES3_A1 <- c(LE_S31[1:36],LE_S32[1:36],LE_S33[1:36])
LES3_A2 <- c(LE_S31[37:72],LE_S32[37:72],LE_S33[37:72])
LES3_A3 <- c(LE_S31[73:108],LE_S32[73:108],LE_S33[73:108])
PSS3_A1 <- c(PS_S31[1:36],PS_S32[1:36],PS_S33[1:36])
PSS3_A2 <- c(PS_S31[37:72],PS_S32[37:72],PS_S33[37:72])
PSS3_A3 <- c(PS_S31[73:108],PS_S32[73:108],PS_S33[73:108])
APARS3_A1 <- c(APAR_S31[1:36],APAR_S32[1:36],APAR_S33[1:36])
APARS3_A2 <- c(APAR_S31[37:72],APAR_S32[37:72],APAR_S33[37:72])
APARS3_A3 <- c(APAR_S31[73:108],APAR_S32[73:108],APAR_S33[73:108])
BS3_A1 <- c(BS31[,1],BS32[,1],BS33[,1])
BS3_A2 <- c(BS31[,2],BS32[,2],BS33[,2])
BS3_A3 <- c(BS31[,3],BS32[,3],BS33[,3])
HS3_A1 <- c(HS31[,1],HS32[,1],HS33[,1])
HS3_A2 <- c(HS31[,2],HS32[,2],HS33[,2])
HS3_A3 <- c(HS31[,3],HS32[,3],HS33[,3])

LES1_A1 <- c(LE_S12[1:36],LE_S13[1:36])
LES1_A2 <- c(LE_S12[37:72],LE_S13[37:72])
LES1_A3 <- c(LE_S12[73:108],LE_S13[73:108])
PSS1_A1 <- c(PS_S12[1:36],PS_S13[1:36])
PSS1_A2 <- c(PS_S12[37:72],PS_S13[37:72])
PSS1_A3 <- c(PS_S12[73:108],PS_S13[73:108])
APARS1_A1 <- c(APAR_S12[1:36],APAR_S13[1:36])
APARS1_A2 <- c(APAR_S12[37:72],APAR_S13[37:72])
APARS1_A3 <- c(APAR_S12[73:108],APAR_S13[73:108])
BS1_A1 <- c(BS12[,1],BS13[,1])
BS1_A2 <- c(BS12[,2],BS13[,2])
BS1_A3 <- c(BS12[,3],BS13[,3])
HS1_A1 <- c(HS12[,1],HS13[,1])
HS1_A2 <- c(HS12[,2],HS13[,2])
HS1_A3 <- c(HS12[,3],HS13[,3])

PS <-  c(PSC1_A1,PSC1_A2,PSC1_A3,PSC3_A1,PSC3_A2,PSC3_A3,PSS3_A1,PSS3_A2,PSS3_A3,PSS1_A1,PSS1_A2,PSS1_A3)
APAR <-  c(APARC1_A1,APARC1_A2,APARC1_A3,APARC3_A1,APARC3_A2,APARC3_A3,APARS3_A1,APARS3_A2,APARS3_A3,APARS1_A1,APARS1_A2,APARS1_A3)
LE <-  c(LEC1_A1,LEC1_A2,LEC1_A3,LEC3_A1,LEC3_A2,LEC3_A3,LES3_A1,LES3_A2,LES3_A3,LES1_A1,LES1_A2,LES1_A3)
BLOC <- c(rep(c(rep(1,36),rep(2,36),rep(3,36)),3),
          rep(c(rep(1,36),rep(2,54),rep(3,36)),3),
          rep(c(rep(1,36),rep(2,36),rep(3,36)),3),rep(2,36*3))
#NUM <- c(rep(1:72,3),rep(73:198,3),rep(199:306,3))
WEFF <- c(rep('+W',630),rep('-W',540))
KEFF <- c(rep('-K',252),rep('+K',378+324),rep('-K',216))
AGE <- c(rep(1.5,108),rep(2.5,108),rep(3,108),rep(1.5,126),rep(2.5,126),rep(3,126),rep(1.5,108),rep(2.5,108),rep(3,108),rep(1.5,36),rep(2.5,36),rep(3,36))

data.frame(PS,APAR,LE,BLOC,WEFF,KEFF,AGE)
require(nlme)

LUE <- PS/APAR
WUE <- PS/LE

ana1 <- lme(LUE~WEFF*KEFF*AGE,random=~1+1|BLOC/WEFF,na.action=na.omit)

AGE <- as.factor(AGE)
contrasts(AGE)

for (i in levels(AGE)){
lue <- LUE[AGE==i]
weff <- WEFF[AGE==i]
keff <- KEFF[AGE==i]
bloc <- BLOC[AGE==i]
ana11 <- lme(lue~weff*keff,random=~1+1|bloc/keff,na.action=na.omit)
print(anova(ana11))}



ana13 <-  lme(ATOT~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|AGE)) # bon





#BC1_A2 <- replace(BC1_A2,c(BC1_A2/APARC1_A2*1000>0.75),NA)
#BC1_A2 <- replace(BC1_A2,c(BC1_A2/APARC1_A2*1000<0.2),NA)
#BC1_A3 <- replace(BC1_A3,c(BC1_A3/APARC1_A3*1000>0.6),NA)
#BC1_A3 <- replace(BC1_A3,c(HC1_A3<8),NA)
#BC3_A3 <- replace(BC3_A3,c(BC3_A3/APARC3_A3*1000>100),NA)
#BS3_A2 <- replace(BS3_A2,c(HS3_A2<11),NA)

ftC11 <- lm(BC1_A1/APARC1_A1~HC1_A1)
ftC12 <- lm(BC1_A2/APARC1_A2~HC1_A2)
ftC13 <- lm(BC1_A3/APARC1_A3~HC1_A3)
ftC31 <- lm(BC3_A1/APARC3_A1~HC3_A1)
ftC32 <- lm(BC3_A2/APARC3_A2~HC3_A2)
ftC33 <- lm(BC3_A3/APARC3_A3~HC3_A3)
ftS31 <- lm(BS3_A1/APARS3_A1~HS3_A1)
ftS32 <- lm(BS3_A2/APARS3_A2~HS3_A2)
ftS33 <- lm(BS3_A3/APARS3_A3~HS3_A3)
ftS11 <- lm(BS1_A1/APARS1_A1~HS1_A1)
ftS12 <- lm(BS1_A2/APARS1_A2~HS1_A2)
ftS13 <- lm(BS1_A3/APARS1_A3~HS1_A3)

par(mfrow=c(2,1))
par(mar=c(2.5,4,1,1))
par(xaxs='i',yaxs='i')
plot(HC3_A1,BC3_A1/APARC3_A1*1000,col=4,xlab='Tree height (m)',ylab='LUE (gDM/MJ)',cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,1.6),mgp=c(2.5,1,0),axes=F)
points(HS3_A1,BS3_A1/APARS3_A1*1000,col=2)
points(HC1_A1,BC1_A1/APARC1_A1*1000,col=5)
points(HS1_A1,BS1_A1/APARS1_A1*1000,col='salmon')
points(HC3_A2+4,BC3_A2/APARC3_A2*1000,col=4)
points(HS3_A2+4,BS3_A2/APARS3_A2*1000,col=2)
points(HC1_A2+4,BC1_A2/APARC1_A2*1000,col=5)
points(HS1_A2+4,BS1_A2/APARS1_A2*1000,col='salmon')
points(HC3_A3+12,BC3_A3/APARC3_A3*1000,col=4)
points(HS3_A3+12,BS3_A3/APARS3_A3*1000,col=2)
points(HC1_A3+12,BC1_A3/APARC1_A3*1000,col=5)
points(HS1_A3+12,BS1_A3/APARS1_A3*1000,col='salmon')
grid(nx=15,ny=4)                                                           
legend('left',legend=c(paste('+K +R, r� = ',round(summary(ftC31)$r.squared,3)),
                          paste('+K -R, r� = ',round(summary(ftS31)$r.squared,3)),
                          paste('-K +R, r� = ',round(summary(ftC11)$r.squared,3)),
                          paste('-K -R, r� = ',round(summary(ftS11)$r.squared,3))),
                          text.col=c(4,2,5,'salmon'),pch=1,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1)
legend(x=16,y=0.65,legend=c(paste('r� = ',round(summary(ftC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftS12)$r.squared,3))),
                          text.col=c(4,2,5,'salmon'),pch=1,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1)
legend(x=26,y=0.65,legend=c(paste('r� = ',round(summary(ftC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftS13)$r.squared,3))),
                          text.col=c(4,2,5,'salmon'),pch=1,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1)
legend(x=0,y=1.5,legend='1 year-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=10,y=1.5,legend='2 years-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=20,y=1.5,legend='2.5 years-old',pch=NA,cex=1,bg='white',box.col=0)
abline(v=10)
abline(v=20)
axis(1,at=c(0:15)*2,labels=c(0,2,4,6,8,  6,8,10,12,14,   8,10,12,14,16,18))
axis(2,at=c(0:4)*0.4,labels=c(0:4)*0.4)
box()


BC1_A2 <- replace(BC1_A2,c(BC1_A2/LEC1_A2*1000>2),NA)
BC1_A2 <- replace(BC1_A2,c(BC1_A2/LEC1_A2*1000<0.5),NA)
BC1_A3 <- replace(BC1_A3,c(BC1_A3/LEC1_A3*1000>2),NA)
BC1_A3 <- replace(BC1_A3,c(HC1_A3<8),NA)
BC3_A3 <- replace(BC3_A3,c(BC3_A3/LEC3_A3*1000>3),NA)
ftC11 <- lm(BC1_A1/LEC1_A1~HC1_A1)
ftC12 <- lm(BC1_A2/LEC1_A2~HC1_A2)
ftC13 <- lm(BC1_A3/LEC1_A3~HC1_A3)
ftC31 <- lm(BC3_A1/LEC3_A1~HC3_A1)
ftC32 <- lm(BC3_A2/LEC3_A2~HC3_A2)
ftC33 <- lm(BC3_A3/LEC3_A3~HC3_A3)
ftS31 <- lm(BS3_A1/LES3_A1~HS3_A1)
ftS32 <- lm(BS3_A2/LES3_A2~HS3_A2)
ftS33 <- lm(BS3_A3/LES3_A3~HS3_A3)
ftS11 <- lm(BS1_A1/LES1_A1~HS1_A1)
ftS12 <- lm(BS1_A2/LES1_A2~HS1_A2)
ftS13 <- lm(BS1_A3/LES1_A3~HS1_A3)


#
mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols2 <- adjustcolor(palette(), alpha.f = 0.1)
mycols3 <- adjustcolor(palette(), alpha.f = 0.3)
mysalmon <- adjustcolor('salmon',alpha.f = 0.6)

par(mar=c(4,4,0,1))
par(xaxs='i',yaxs='i')
plot(HC3_A1,BC3_A1/LEC3_A1*1000,col=mycols[4],xlab='Tree height (m)',ylab='WUE (gDM/LH2O)',cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,3),mgp=c(2.5,1,0),axes=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1/LES3_A1*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1/LEC1_A1*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1/LES1_A1*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2/LEC3_A2*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2/LES3_A2*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2/LEC1_A2*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2/LES1_A2*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3/LEC3_A3*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3/LES3_A3*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3/LEC1_A3*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3/LES1_A3*1000,col=mysalmon,pch=16,cex=0.8)
grid(nx=15,ny=NULL)                                                           
legend(x=10,y=3,legend=c(paste('+K +R, r� = ',round(summary(ftC31)$r.squared,3)),
                          paste('+K -R, r� = ',round(summary(ftS31)$r.squared,3)),
                          paste('-K +R, r� = ',round(summary(ftC11)$r.squared,3)),
                          paste('-K -R, r� = ',round(summary(ftS11)$r.squared,3))),
                          pch=16,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1,xjust=1)
legend(x=20,y=0,legend=c(paste('r� = ',round(summary(ftC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftS12)$r.squared,3))),
                          pch=16,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1,xjust=1,yjust=0)
legend(x=30,y=0,legend=c(paste('r� = ',round(summary(ftC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftS13)$r.squared,3))),
                          pch=16,col=c(4,2,5,'salmon'),bg='white',box.col=0,cex=1,xjust=1,yjust=0)
legend(x=0,y=3,legend='1 year-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=10,y=3,legend='2 years-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=20,y=3,legend='2.5 years-old',pch=NA,cex=1,bg='white',box.col=0)
abline(v=10)
abline(v=20)
axis(1,at=c(0:15)*2,labels=c(0,2,4,6,8,  4,6,8,10,12,   8,10,12,14,16,18))
axis(2,at=c(1:5)*0.5,labels=c(1:5)*0.5)
box()





MATWUE_trunk <- matrix(c(mean(1000*BC3_A1/LEC3_A1,na.rm=T),mean(1000*BC1_A1/LEC1_A1,na.rm=T),mean(1000*BS3_A1/LES3_A1,na.rm=T),
                   mean(1000*BC3_A2/LEC3_A2,na.rm=T),mean(1000*BC1_A2/LEC1_A2,na.rm=T),mean(1000*BS3_A2/LES3_A2,na.rm=T),
                   mean(1000*BC3_A3/LEC3_A3,na.rm=T),mean(1000*BC1_A3/LEC1_A3,na.rm=T),mean(1000*BS3_A3/LES3_A3,na.rm=T)),nrow=3)
MATLUE_trunk <- matrix(c(mean(1000*BC3_A1/APARC3_A1,na.rm=T),mean(1000*BC1_A1/APARC1_A1,na.rm=T),mean(1000*BS3_A1/APARS3_A1,na.rm=T),
                   mean(1000*BC3_A2/APARC3_A2,na.rm=T),mean(1000*BC1_A2/APARC1_A2,na.rm=T),mean(1000*BS3_A2/APARS3_A2,na.rm=T),
                   mean(1000*BC3_A3/APARC3_A3,na.rm=T),mean(1000*BC1_A3/APARC1_A3,na.rm=T),mean(1000*BS3_A3/APARS3_A3,na.rm=T)),nrow=3)
sdMATWUE_trunk <- c(sd(1000*BC3_A1/LEC3_A1,na.rm=T),sd(1000*BC1_A1/LEC1_A1,na.rm=T),sd(1000*BS3_A1/LES3_A1,na.rm=T),
                   sd(1000*BC3_A2/LEC3_A2,na.rm=T),sd(1000*BC1_A2/LEC1_A2,na.rm=T),sd(1000*BS3_A2/LES3_A2,na.rm=T),
                   sd(1000*BC3_A3/LEC3_A3,na.rm=T),sd(1000*BC1_A3/LEC1_A3,na.rm=T),sd(1000*BS3_A3/LES3_A3,na.rm=T))
sdMATLUE_trunk <- c(sd(1000*BC3_A1/APARC3_A1,na.rm=T),sd(1000*BC1_A1/APARC1_A1,na.rm=T),sd(1000*BS3_A1/APARS3_A1,na.rm=T),
                   sd(1000*BC3_A2/APARC3_A2,na.rm=T),sd(1000*BC1_A2/APARC1_A2,na.rm=T),sd(1000*BS3_A2/APARS3_A2,na.rm=T),
                   sd(1000*BC3_A3/APARC3_A3,na.rm=T),sd(1000*BC1_A3/APARC1_A3,na.rm=T),sd(1000*BS3_A3/APARS3_A3,na.rm=T))

MATWUE <- matrix(c(mean(PSC3_A1/LEC3_A1,na.rm=T),mean(PSC1_A1/LEC1_A1,na.rm=T),mean(PSS3_A1/LES3_A1,na.rm=T),
                   mean(PSC3_A2/LEC3_A2,na.rm=T),mean(PSC1_A2/LEC1_A2,na.rm=T),mean(PSS3_A2/LES3_A2,na.rm=T),
                   mean(PSC3_A3/LEC3_A3,na.rm=T),mean(PSC1_A3/LEC1_A3,na.rm=T),mean(PSS3_A3/LES3_A3,na.rm=T)),nrow=3)
MATLUE <- matrix(c(mean(PSC3_A1/APARC3_A1,na.rm=T),mean(PSC1_A1/APARC1_A1,na.rm=T),mean(PSS3_A1/APARS3_A1,na.rm=T),
                   mean(PSC3_A2/APARC3_A2,na.rm=T),mean(PSC1_A2/APARC1_A2,na.rm=T),mean(PSS3_A2/APARS3_A2,na.rm=T),
                   mean(PSC3_A3/APARC3_A3,na.rm=T),mean(PSC1_A3/APARC1_A3,na.rm=T),mean(PSS3_A3/APARS3_A3,na.rm=T)),nrow=3)
sdMATWUE <- c(sd(PSC3_A1/LEC3_A1,na.rm=T),sd(PSC1_A1/LEC1_A1,na.rm=T),sd(PSS3_A1/LES3_A1,na.rm=T),
                   sd(PSC3_A2/LEC3_A2,na.rm=T),sd(PSC1_A2/LEC1_A2,na.rm=T),sd(PSS3_A2/LES3_A2,na.rm=T),
                   sd(PSC3_A3/LEC3_A3,na.rm=T),sd(PSC1_A3/LEC1_A3,na.rm=T),sd(PSS3_A3/LES3_A3,na.rm=T))
sdMATLUE <- c(sd(PSC3_A1/APARC3_A1,na.rm=T),sd(PSC1_A1/APARC1_A1,na.rm=T),sd(PSS3_A1/APARS3_A1,na.rm=T),
                   sd(PSC3_A2/APARC3_A2,na.rm=T),sd(PSC1_A2/APARC1_A2,na.rm=T),sd(PSS3_A2/APARS3_A2,na.rm=T),
                   sd(PSC3_A3/APARC3_A3,na.rm=T),sd(PSC1_A3/APARC1_A3,na.rm=T),sd(PSS3_A3/APARS3_A3,na.rm=T))
                                  
YEA <-c('1.5 year','2.5 years','3 years')
ARG <-c('+K +R','- K +R','+K - R')
                   
mycols <- adjustcolor(palette(), alpha.f = 0.6)

par(mfrow=c(2,2))                  
par(mar=c(3,4,1,1))
par(yaxs='i')
barplot(MATLUE,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='GPP LUE (gC/MJ)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,2.5),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              

barplot(MATWUE,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='GPP WUE (gC/LH2O)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,5),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              

barplot(MATLUE_trunk,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Stem LUE (gDM/MJ)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,1.5),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              

barplot(MATWUE_trunk,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Stem WUE (gDM/LH2O)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,3),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              


                                                         



















                                 

# figue A et E, figure 4

mycols <- adjustcolor(palette(), alpha.f = 0.6)

MAT <- matrix(c(2611,4275,2639,2782,3529,1866,1935,2713,1256),nrow=3,byrow=T)
MATE <- matrix(c(968,1519,697,920,1167,560,559,768,277),nrow=3,byrow=T)
YEA <-c('2011','2012','ini 2013')
ARG <-c('+K +R','+K -R','-K +R')

par(mfrow=c(2,1))
par(mar=c(3.5,4,1.5,30))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(PSC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily gross photosynthesis'~~(gC~m^-2~d^-1)),
      cex.lab=1.2,col=mycols[4],pch=16,mgp=c(2.5,1,0),ylim=c(0,25))
points(PSS3,col=mycols[5],pch=16)
points(PSC1,col=mycols[2],pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,22.5,'2011',font=4)
text(550,22.5,'2012',font=4)
text(850,22.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()

par(new=T)
par(mar=c(4,40,1,1))
par(yaxs='i')
barplot(MAT,col=rep(c(mycols[4],mycols[5],mycols[2]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Annual A (gC/m�/y)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,4500))
grid(nx=NA,ny=NULL)
box()

par(mar=c(4,4,1,30))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(LEC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily transpiration'~~(L~m^-2~d^-1)),
      cex.lab=1.2,col=mycols[4],pch=16,mgp=c(2.5,1,0),ylim=c(0,10))
points(LES3,col=mycols[5],pch=16)
points(LEC1,col=mycols[2],pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,22.5,'2011',font=4)
text(550,22.5,'2012',font=4)
text(850,22.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()

par(new=T)
par(mar=c(4,40,1,1))
par(yaxs='i')
barplot(MATE,col=rep(c(mycols[4],mycols[5],mycols[2]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Annual E (L/m�/y)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,1800))
grid(nx=NA,ny=NULL)
box()





# efficacit�

PS <-  c(PSC1_A1,PSC1_A2,PSC1_A3,PSC3_A1,PSC3_A2,PSC3_A3,PSS3_A1,PSS3_A2,PSS3_A3)
APAR <-  c(APARC1_A1,APARC1_A2,APARC1_A3,APARC3_A1,APARC3_A2,APARC3_A3,APARS3_A1,APARS3_A2,APARS3_A3)
LE <-  c(LEC1_A1,LEC1_A2,LEC1_A3,LEC3_A1,LEC3_A2,LEC3_A3,LES3_A1,LES3_A2,LES3_A3)
BLOC <- c(rep(c(rep(1,36),rep(3,36)),3),rep(c(rep(1,36),rep(2,54),rep(3,36)),3),rep(c(rep(1,36),rep(2,36),rep(3,36)),3))
NUM <- c(rep(1:72,3),rep(73:198,3),rep(199:306,3))
WEFF <- c(rep('+W',198*3),rep('-W',108*3))
KEFF <- c(rep('-K',72*3),rep('+K',234*3))
AGE <- c(rep(1.5,72),rep(2.5,72),rep(3,72),rep(1.5,126),rep(2.5,126),rep(3,126),rep(1.5,108),rep(2.5,108),rep(3,108))

MAT <- data.frame(PS,APAR,LE,BLOC,NUM,WEFF,KEFF,AGE)
AGE <- as.factor(AGE)

LUE <- PS/APAR

require (nlme)

ana1 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit)

ana1 <- gls(LUE~(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit)

ana5 <- gls(LUE~(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF*KEFF),  correlation=corAR1(form= ~ 1))
                
anova(ana1,ana5)





ana2 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC))
                
ana3 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF))
ana31 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF*KEFF))
                
ana4 <- gls(PS~APAR*(WEFF+KEFF)*AGE,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF*KEFF),  correlation=corAR1(form= ~ 1))
                
ana5 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF*KEFF),  correlation=corAR1(form= ~ 1))

ana6 <- gls(PS~APAR*(WEFF+KEFF)*AGE*BLOC,
                na.action=na.omit,  weights=varIdent(form= ~ 1|BLOC*WEFF*KEFF),  correlation=corAR1(form= ~ 1|BLOC))


hist(residuals(ana5),breaks=50)


ana1 <- lme(PS~APAR+(WEFF+KEFF)*AGE)

ana1 <- lme(PS~APAR*(WEFF+KEFF)*AGE,
                random=~1+1|BLOC/WEFF,na.action=na.omit)





# autre mani�re de voire le LUE et WUE
require(gss)
DAY <- 1:length(PSS3)
GEC3 <- ssanova(PSC3/LEC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGEC3 <- predict(GEC3,newC3,se=T)
GEC1 <- ssanova(PSC1/LEC1~DAY,alpha=1.1)
newC1 <- data.frame(DAY)
estGEC1 <- predict(GEC1,newC1,se=T)
GES3 <- ssanova(PSS3/LES3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGES3 <- predict(GES3,newS3,se=T)

DAY <- 1:length(PSS3)
GFC3 <- ssanova(PSC3/aPARC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGFC3 <- predict(GFC3,newC3,se=T)
GFC1 <- ssanova(PSC1/aPARC1~DAY,alpha=1.1)
newC1 <- data.frame(DAY)
estGFC1 <- predict(GFC1,newC1,se=T)
GFS3 <- ssanova(PSS3/aPARS3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGFS3 <- predict(GFS3,newS3,se=T)

mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols2 <- adjustcolor(palette(), alpha.f = 0.1)
YEA <- c('1.5 year','2.5 years','3 years')
ARG <-c('+K +R','+K -R','-K +R')

par(mfrow=c(2,1))
par(mar=c(4,4,1,25))
par(yaxs='i',xaxs='i')
plot(PSC3/aPARC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('LUE (gC/MJ)'),
      cex.lab=1.2,col=1,pch=1,mgp=c(2.5,1,0),ylim=c(0,4))
points(PSC1/aPARC1)
points(PSS3/aPARS3)
rect( 182,0,273,10,border=NA,col=mycols2[1])
rect(548,0,639,10,border=NA,col=mycols2[1])
lines(DAY,estGFC3$fit,col=4,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFC3$fit,rev(estGFC3$fit+estGFC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFC3$fit,rev(estGFC3$fit-estGFC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGFC1$fit,col=2,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFC1$fit,rev(estGFC1$fit+estGFC1$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFC1$fit,rev(estGFC1$fit-estGFC1$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGFS3$fit,col=5,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFS3$fit,rev(estGFS3$fit+estGFS3$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFS3$fit,rev(estGFS3$fit-estGFS3$se.fit)),col=mycols[5],border=NA)
grid(nx=NA,ny=NULL, lty = "dotted", lwd = 1)
box()

par(new=T)                  
par(mar=c(4,35,1,1))
par(yaxs='i')
barplot(MATLUE,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='LUE (gC/MJ)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,2.5),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              

par(mar=c(4,4,1,25))
par(yaxs='i',xaxs='i')
plot(PSC3/LEC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('WUE (gC/LH2O)'),
      cex.lab=1.2,col=1,pch=1,mgp=c(2.5,1,0),ylim=c(0,10))
points(PSC1/LEC1)
points(PSS3/LES3)
rect( 182,0,273,10,border=NA,col=mycols2[1])
rect(548,0,639,10,border=NA,col=mycols2[1])
lines(DAY,estGEC3$fit,col=4,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGEC3$fit,rev(estGEC3$fit+estGEC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGEC3$fit,rev(estGEC3$fit-estGEC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGEC1$fit,col=2,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGEC1$fit,rev(estGEC1$fit+estGEC1$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGEC1$fit,rev(estGEC1$fit-estGEC1$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGES3$fit,col=5,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGES3$fit,rev(estGES3$fit+estGES3$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGES3$fit,rev(estGES3$fit-estGES3$se.fit)),col=mycols[5],border=NA)
grid(nx=NA,ny=NULL, lty = "dotted", lwd = 1)
box()

par(new=T)
par(mar=c(4,35,1,1))
par(yaxs='i')
barplot(MATWUE,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='GPP WUE (gC/LH2O)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,5),args.legend=c(x='topleft',bg='white',box.col=0))
grid(nx=NA,ny= NULL)
box()                                                              


# trunk

WUETC1 <-c();MONC1<-c()
WUETC3 <-c();MONC3 <- c()
WUETS1 <-c()
WUETS3 <-c();MONS3 <-c()
for (i in 1:28){
WUETC1 <- c(WUETC1, c(dB_C11[,i]/dLE_C11[,i],dB_C13[,i]/dLE_C13[,i])*1000)
WUETC3 <- c(WUETC3, c(dB_C31[,i]/dLE_C31[,i],dB_C32[,i]/dLE_C32[,i],dB_C33[,i]/dLE_C33[,i])*1000)
WUETS3 <- c(WUETS3, c(dB_S31[,i]/dLE_S31[,i],dB_S32[,i]/dLE_S32[,i],dB_S33[,i]/dLE_S33[,i])*1000)
MONC1 <- c(MONC1, rep(i,length(c(dB_C11[,i]/dLE_C11[,i],dB_C13[,i]/dLE_C13[,i]))))
MONC3 <- c(MONC3, rep(i,length(c(dB_C31[,i]/dLE_C31[,i],dB_C32[,i]/dLE_C32[,i],dB_C33[,i]/dLE_C33[,i]))))
MONS3 <- c(MONS3, rep(i,length(c(dB_S31[,i]/dLE_S31[,i],dB_S32[,i]/dLE_S32[,i],dB_S33[,i]/dLE_S33[,i]))))
}

WUETC1 <- replace(WUETC1,c(WUETC1<=0),NA)
WUETC3 <- replace(WUETC3,c(WUETC3<=0),NA)
WUETS1 <- replace(WUETS1,c(WUETS1<=0),NA)
WUETS3 <- replace(WUETS3,c(WUETS3<=0),NA)


MON2 <- seq(1,28,length.out=1000)
GETC1 <- ssanova(WUETC1~MONC1,alpha=1.4)
newC1 <- data.frame(MON2)
estGETC1 <- predict(GETC1,newC1,se=T)
GETC3 <- ssanova(WUETC3~MONC3,alpha=1.1)
newC3 <- data.frame(MON2)
estGETC3 <- predict(GETC3,newC3,se=T)
GETS3 <- ssanova(WUETS3~MONS3,alpha=1.1)
newS3 <- data.frame(MON2)
estGETS3 <- predict(GETS3,newS3,se=T)

par(mfrow=c(1,2))
par(xaxs='i',yaxs='i')
par(mar=c(4,4,1,1))
plot(MONC3,WUETC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('WUE'),
      cex.lab=1.2,col=1,pch=1,mgp=c(2.5,1,0),ylim=c(0,10),xlim=c(0,28))
points(MONC1,WUETC1)
points(MONS3,WUETS3)
rect(6.5,0,9.5,10,border=NA,col=mycols2[1])
rect(18.6,0,21.5,10,border=NA,col=mycols2[1])
lines(MONC3,estGETC3$fit,col=4,lwd=2)
polygon(c(MONC3,rev(MONC3)),c(estGETC3$fit,rev(estGETC3$fit+estGETC3$se.fit)),col=mycols[4],border=NA)
polygon(c(MONC3,rev(MONC3)),c(estGETC3$fit,rev(estGETC3$fit-estGETC3$se.fit)),col=mycols[4],border=NA)
lines(MON2,estGETC1$fit,col=2,lwd=2)
polygon(c(MON2,rev(MON2)),c(estGETC1$fit,rev(estGETC1$fit+estGETC1$se.fit)),col=mycols[2],border=NA)
polygon(c(MON2,rev(MON2)),c(estGETC1$fit,rev(estGETC1$fit-estGETC1$se.fit)),col=mycols[2],border=NA)
lines(MON2,estGETS3$fit,col=5,lwd=2)
polygon(c(MON2,rev(MON2)),c(estGETS3$fit,rev(estGETS3$fit+estGETS3$se.fit)),col=mycols[5],border=NA)
polygon(c(MON2,rev(MON2)),c(estGETS3$fit,rev(estGETS3$fit-estGETS3$se.fit)),col=mycols[5],border=NA)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)


shell(
