set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
set6<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF")
set7<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF_3ans")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set6)


setwd(set6)
require(Maeswrap)

setwd(set7)
dC110 <- readdayflux('1_Dayflx.dat')
setwd(set6)
dC111 <- read.table('1_Dayflx.dat')
dC112 <- read.table('2_Dayflx.dat')
dC113 <- read.table('3_Dayflx.dat')
setwd(set7)
dC120 <- readdayflux('5_Dayflx.dat')
setwd(set6)
dC121 <- read.table('4_Dayflx.dat')
dC122 <- read.table('5_Dayflx.dat')
dC123 <- read.table('6_Dayflx.dat')
setwd(set7)
dC130 <- readdayflux('9_Dayflx.dat')
setwd(set6)
dC131 <- read.table('7_Dayflx.dat')
dC132 <- read.table('8_Dayflx.dat')
dC133 <- read.table('9_Dayflx.dat')

setwd(set7)
dC310 <- readdayflux('13_Dayflx.dat')
setwd(set6)
dC311 <- read.table('10_Dayflx.dat')
dC312 <- read.table('11_Dayflx.dat')
dC313 <- read.table('12_Dayflx.dat')
setwd(set7)
dC320 <- readdayflux('17_Dayflx.dat')
setwd(set6)
dC321 <- read.table('13_Dayflx.dat')
dC322 <- read.table('14_Dayflx.dat')
dC323 <- read.table('15_Dayflx.dat')
setwd(set7)
dC330 <- readdayflux('21_Dayflx.dat')
setwd(set6)
dC331 <- read.table('16_Dayflx.dat')
dC332 <- read.table('17_Dayflx.dat')
dC333 <- read.table('18_Dayflx.dat')

setwd(set7)
dS110 <- readdayflux('25_Dayflx.dat')
setwd(set6)
dS111 <- read.table('19_Dayflx.dat')
dS112 <- read.table('20_Dayflx.dat') # vide
dS113 <- read.table('21_Dayflx.dat')
setwd(set7)
dS120 <- readdayflux('29_Dayflx.dat')
setwd(set6)
dS121 <- read.table('22_Dayflx.dat') # jour 305 arret au milieu
dS122 <- read.table('23_Dayflx.dat')
dS123 <- read.table('24_Dayflx.dat')
setwd(set7)
dS130 <- readdayflux('33_Dayflx.dat')
setwd(set6)
dS131 <- read.table('25_Dayflx.dat')
dS132 <- read.table('26_Dayflx.dat')
dS133 <- read.table('27_Dayflx.dat')

setwd(set7)
dS310 <- readdayflux('37_Dayflx.dat')
setwd(set6)
dS311 <- read.table('28_Dayflx.dat')
dS312 <- read.table('29_Dayflx.dat')
dS313 <- read.table('30_Dayflx.dat')
setwd(set7)
dS320 <- readdayflux('41_Dayflx.dat')
setwd(set6)
dS321 <- read.table('31_Dayflx.dat')
dS322 <- read.table('32_Dayflx.dat')
dS323 <- read.table('33_Dayflx.dat')
setwd(set7)
dS330 <- readdayflux('45_Dayflx.dat')
setwd(set6)
dS331 <- read.table('34_Dayflx.dat')
dS332 <- read.table('35_Dayflx.dat')
dS333 <- read.table('36_Dayflx.dat')

names(dC111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dC311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

setwd(set3)
require (Maeswrap)
B11 <- read.table('met2011_C.dat',skip=25)
B12 <- read.table('met2012_C.dat',skip=25)
B13 <- read.table('met2013_C.dat',skip=26)

PPT <- c()

for (i in 1:365){
PPT <- c(PPT, sum(B11[,10][((i-1)*48+1):(i*48)]))}
for (i in 1:366){
PPT <- c(PPT, sum(B12[,10][((i-1)*48+1):(i*48)]))}
for (i in 1:168){
PPT <- c(PPT, sum(B13[,8][((i-1)*48+1):(i*48)]))}

PSC11 <- c();PSC12 <-c();PSC13<-c()
PSC31 <- c();PSC32 <-c();PSC33<-c()
PSS11 <- c();PSS12 <-c();PSS13<-c()
PSS31 <- c();PSS32 <-c();PSS33<-c()

for (i in 1:197){
PSC11 <- c(PSC11, sum(dC110$totPs[dC110$DOY==i],na.rm=T)*12/(length(dC110$Tree[dC110$DOY==i])*6))
PSC12 <- c(PSC12, sum(dC120$totPs[dC120$DOY==i],na.rm=T)*12/(length(dC120$Tree[dC120$DOY==i])*6))
PSC13 <- c(PSC13, sum(dC130$totPs[dC130$DOY==i],na.rm=T)*12/(length(dC130$Tree[dC130$DOY==i])*6))
PSC31 <- c(PSC31, sum(dC310$totPs[dC310$DOY==i],na.rm=T)*12/(length(dC310$Tree[dC310$DOY==i])*6))
PSC32 <- c(PSC32, sum(dC310$totPs[dC310$DOY==i],na.rm=T)*12/(length(dC310$Tree[dC310$DOY==i])*6))
PSC33 <- c(PSC33, sum(dC330$totPs[dC330$DOY==i],na.rm=T)*12/(length(dC330$Tree[dC330$DOY==i])*6))
PSS11 <- c(PSS11, sum(dS110$totPs[dS110$DOY==i],na.rm=T)*12/(length(dS110$Tree[dS110$DOY==i])*6))
PSS12 <- c(PSS12, sum(dS120$totPs[dS120$DOY==i],na.rm=T)*12/(length(dS120$Tree[dS120$DOY==i])*6))
PSS13 <- c(PSS13, sum(dS130$totPs[dS130$DOY==i],na.rm=T)*12/(length(dS130$Tree[dS130$DOY==i])*6))
PSS31 <- c(PSS31, sum(dS310$totPs[dS310$DOY==i],na.rm=T)*12/(length(dS310$Tree[dS310$DOY==i])*6))
PSS32 <- c(PSS32, sum(dS320$totPs[dS320$DOY==i],na.rm=T)*12/(length(dS320$Tree[dS320$DOY==i])*6))
PSS33 <- c(PSS33, sum(dS330$totPs[dS330$DOY==i],na.rm=T)*12/(length(dS330$Tree[dS330$DOY==i])*6))}
for (i in 1:365){
PSC11 <- c(PSC11, sum(dC111$totPs[dC111$DOY==i],na.rm=T)*12/(length(dC111$Tree[dC111$DOY==i])*6))
PSC12 <- c(PSC12, sum(dC121$totPs[dC121$DOY==i],na.rm=T)*12/(length(dC121$Tree[dC121$DOY==i])*6))
PSC13 <- c(PSC13, sum(dC131$totPs[dC131$DOY==i],na.rm=T)*12/(length(dC131$Tree[dC131$DOY==i])*6))
PSC31 <- c(PSC31, sum(dC311$totPs[dC311$DOY==i],na.rm=T)*12/(length(dC311$Tree[dC311$DOY==i])*6))
PSC32 <- c(PSC32, sum(dC321$totPs[dC321$DOY==i],na.rm=T)*12/(length(dC321$Tree[dC321$DOY==i])*6))
PSC33 <- c(PSC33, sum(dC331$totPs[dC331$DOY==i],na.rm=T)*12/(length(dC331$Tree[dC331$DOY==i])*6))
PSS11 <- c(PSS11, sum(dS111$totPs[dS111$DOY==i],na.rm=T)*12/(length(dS111$Tree[dS111$DOY==i])*6))
PSS12 <- c(PSS12, sum(dS121$totPs[dS121$DOY==i],na.rm=T)*12/(length(dS121$Tree[dS121$DOY==i])*6))
PSS13 <- c(PSS13, sum(dS131$totPs[dS131$DOY==i],na.rm=T)*12/(length(dS131$Tree[dS131$DOY==i])*6))
PSS31 <- c(PSS31, sum(dS311$totPs[dS311$DOY==i],na.rm=T)*12/(length(dS311$Tree[dS311$DOY==i])*6))
PSS32 <- c(PSS32, sum(dS321$totPs[dS321$DOY==i],na.rm=T)*12/(length(dS321$Tree[dS321$DOY==i])*6))
PSS33 <- c(PSS33, sum(dS331$totPs[dS331$DOY==i],na.rm=T)*12/(length(dS331$Tree[dS331$DOY==i])*6))}
for (i in 1:366){
PSC11 <- c(PSC11, sum(dC112$totPs[dC112$DOY==i],na.rm=T)*12/(length(dC112$Tree[dC112$DOY==i])*6))
PSC12 <- c(PSC12, sum(dC122$totPs[dC122$DOY==i],na.rm=T)*12/(length(dC122$Tree[dC122$DOY==i])*6))
PSC13 <- c(PSC13, sum(dC132$totPs[dC132$DOY==i],na.rm=T)*12/(length(dC132$Tree[dC132$DOY==i])*6))
PSC31 <- c(PSC31, sum(dC312$totPs[dC312$DOY==i],na.rm=T)*12/(length(dC312$Tree[dC312$DOY==i])*6))
PSC32 <- c(PSC32, sum(dC322$totPs[dC322$DOY==i],na.rm=T)*12/(length(dC322$Tree[dC322$DOY==i])*6))
PSC33 <- c(PSC33, sum(dC332$totPs[dC332$DOY==i],na.rm=T)*12/(length(dC332$Tree[dC332$DOY==i])*6))
PSS11 <- c(PSS11, sum(dS112$totPs[dS112$DOY==i],na.rm=T)*12/(length(dS112$Tree[dS112$DOY==i])*6))
PSS12 <- c(PSS12, sum(dS122$totPs[dS122$DOY==i],na.rm=T)*12/(length(dS122$Tree[dS122$DOY==i])*6))
PSS13 <- c(PSS13, sum(dS132$totPs[dS132$DOY==i],na.rm=T)*12/(length(dS132$Tree[dS132$DOY==i])*6))
PSS31 <- c(PSS31, sum(dS312$totPs[dS312$DOY==i],na.rm=T)*12/(length(dS312$Tree[dS312$DOY==i])*6))
PSS32 <- c(PSS32, sum(dS322$totPs[dS322$DOY==i],na.rm=T)*12/(length(dS322$Tree[dS322$DOY==i])*6))
PSS33 <- c(PSS33, sum(dS332$totPs[dS332$DOY==i],na.rm=T)*12/(length(dS332$Tree[dS332$DOY==i])*6))}
for (i in 1:168){
PSC11 <- c(PSC11, sum(dC113$totPs[dC113$DOY==i],na.rm=T)*12/(length(dC113$Tree[dC113$DOY==i])*6))
PSC12 <- c(PSC12, sum(dC123$totPs[dC123$DOY==i],na.rm=T)*12/(length(dC123$Tree[dC123$DOY==i])*6))
PSC13 <- c(PSC13, sum(dC133$totPs[dC133$DOY==i],na.rm=T)*12/(length(dC133$Tree[dC133$DOY==i])*6))
PSC31 <- c(PSC31, sum(dC313$totPs[dC313$DOY==i],na.rm=T)*12/(length(dC313$Tree[dC313$DOY==i])*6))
PSC32 <- c(PSC32, sum(dC323$totPs[dC323$DOY==i],na.rm=T)*12/(length(dC323$Tree[dC323$DOY==i])*6))
PSC33 <- c(PSC33, sum(dC333$totPs[dC333$DOY==i],na.rm=T)*12/(length(dC333$Tree[dC333$DOY==i])*6))
PSS11 <- c(PSS11, sum(dS113$totPs[dS113$DOY==i],na.rm=T)*12/(length(dS113$Tree[dS113$DOY==i])*6))
PSS12 <- c(PSS12, sum(dS123$totPs[dS123$DOY==i],na.rm=T)*12/(length(dS123$Tree[dS123$DOY==i])*6))
PSS13 <- c(PSS13, sum(dS133$totPs[dS133$DOY==i],na.rm=T)*12/(length(dS133$Tree[dS133$DOY==i])*6))
PSS31 <- c(PSS31, sum(dS313$totPs[dS313$DOY==i],na.rm=T)*12/(length(dS313$Tree[dS313$DOY==i])*6))
PSS32 <- c(PSS32, sum(dS323$totPs[dS323$DOY==i],na.rm=T)*12/(length(dS323$Tree[dS323$DOY==i])*6))
PSS33 <- c(PSS33, sum(dS333$totPs[dS333$DOY==i],na.rm=T)*12/(length(dS333$Tree[dS333$DOY==i])*6))}


#PSC11 <- replace(PSC11,c(PSC11==NA),0)
#PSC13 <- replace(PSC13,c(PSC13==NA),0)
  #PSC31 <- replace(PSC31,c(PSC31==NA),0)
#PSC32 <- replace(PSC32,c(PSC32==NA),0)
#PSC33 <- replace(PSC33,c(PSC33==NA),0)
#PSS31 <- replace(PSS31,c(PSS31==NA),0)
#PSS32 <- replace(PSS32,c(PSS32==NA),0)
#PSS33 <- replace(PSS33,c(PSS33==NA),0)

PSC1 <- c(PSC11+PSC12+PSC13)/3
PSC3 <- c(PSC31+PSC32+PSC33)/3
PSS1 <- c(PSS11+PSS12+PSS13)/3
PSS3 <- c(PSS31+PSS32+PSS33)/3
               

PSC3/12*10^6/24/60/60

LAIC1 <- c(1.3,2.9,1.6,2.7,2.8,2)
LAIC3 <- c(2.2,3.4,2.8,4.7,4.2,5.2)
AGE <- c(c(8,11,16,23,27,35)-6)*30.5
DY <- c(61,152,305,518,640,884)

laiC1 <- rep(1.3,61)
laiC3 <- rep(2.2,61)
for (i in 62:884){
X1 <- max(which(c(DY<i)))
X2 <- min(which(c(DY>=i)))
DI <- DY[X2]-DY[X1]
laiC1 <- c(laiC1,c((LAIC1[X1]*(DY[X2]-i) + LAIC1[X2]*(i-DY[X1]))/DI))
laiC3 <- c(laiC3,c((LAIC3[X1]*(DY[X2]-i) + LAIC3[X2]*(i-DY[X1]))/DI))
}
laiC1 <- c(laiC1,rep(2,15))
laiC3 <- c(laiC3,rep(5.2,15))


MAT<- matrix(c(sum(PSC1[1:168],na.rm=T),sum(PSC3[1:168],na.rm=T),sum(PSS1[1:168],na.rm=T),sum(PSS3[1:168],na.rm=T),
           sum(PSC1[169:535],na.rm=T),sum(PSC3[169:535],na.rm=T),sum(PSS1[169:535],na.rm=T),sum(PSS3[169:535],na.rm=T),
           sum(PSC1[536:899],na.rm=T),sum(PSC3[536:899],na.rm=T),sum(PSS1[536:899],na.rm=T),sum(PSS3[536:899],na.rm=T)),nrow=3,byrow=T)

MAT<- matrix(c(sum(PSC1[1:168],na.rm=T),sum(PSC3[1:168],na.rm=T),sum(PSS1[1:168],na.rm=T),sum(PSS3[1:168],na.rm=T),
           sum(PSC1[1:535],na.rm=T),sum(PSC3[1:535],na.rm=T),sum(PSS1[1:535],na.rm=T),sum(PSS3[1:535],na.rm=T),
           sum(PSC1[1:899],na.rm=T),sum(PSC3[1:899],na.rm=T),sum(PSS1[1:899],na.rm=T),sum(PSS3[1:899],na.rm=T)),nrow=3,byrow=T)
MAT <- data.frame(MAT)
names(MAT) <-c('C1','C3','S1','S1')
MAT <- MAT*10000/10^6              

MAT2 <- data.frame(c(6.796,16.928,25.704),c(9.814,36.777,68.722),c(6.510,17.513,26.243),c(10.902,32.296,58.686))
names(MAT2) <-c('C1','C3','S1','S1')

MAT <- matrix(as.numeric(c(MAT[1,],MAT[2,],MAT[3,])),byrow=F,nrow=4)
MAT[,1] <- MAT[,1]
MAT2 <- matrix(as.numeric(c(MAT2[1,],MAT2[2,],MAT2[3,])),byrow=F,nrow=4)

MASSTOT12 <- c(253.9329, 388.6382, 278.4325, 427.0762)                    
MASSTOT13 <- c(695.779496689973,898.4795068, 597.7056876, 960.419722602016)
MAT3 <- matrix(c(MASSTOT12/2*0.01,MASSTOT12*0.01,MASSTOT13*0.01),nrow=4,byrow=F)


palette('default')

par(mfrow=c(1,3))
par(mar=c(4,4,1,1))
barplot(MAT,beside=T,ylab='Predicted total GPP (Mg/ha)',names.arg=c('1 year','2 years','3 years'),cex.lab=1.2,
        ylim=c(0,100),legend.text=c('C1','C3','S1','S3'),args.legend=c(x='topleft',bty='n'))
box()
barplot(MAT2,beside=T,ylab='Measured ANPP (Mg/ha)',names.arg=c('1 year','2 years','3 years'),cex.lab=1.2,
        ylim=c(0,100),legend.text=c('C1','C3','S1','S3'),args.legend=c(x='topleft',bty='n'))
box()
barplot(MAT2+MAT3,beside=T,ylab='Estimated NPP (Mg/ha)',names.arg=c('1 year','2 years','3 years'),cex.lab=1.2,
        ylim=c(0,100),legend.text=c('C1','C3','S1','S3'),args.legend=c(x='topleft',bty='n'))
box()




mycols <- adjustcolor(palette(), alpha.f = 0.6)

MAT <- matrix(c(2611,4275,2639,1935,2713,1256,2782,3529,1866),nrow=3,byrow=T)
YEA <-c('2011','2012','ini 2013')
ARG <-c('+K +R','-K +R','+K -R')

par(mar=c(4,4,1,30))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(PSC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily gross photosynthesis'~~(gC~m^-2~d^-1)),
      cex.lab=1.2,col=mycols[4],pch=16,mgp=c(2.5,1,0),ylim=c(0,25))
points(PSS3,col=mycols[5],pch=16)
points(PSC1,col=mycols[2],pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,22.5,'2011',font=4)
text(550,22.5,'2012',font=4)
text(850,22.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()

par(new=T)
par(mar=c(4,40,1,1))
par(yaxs='i')
barplot(MAT,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Annual A (gC/m�/y)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,4500))
grid(nx=NA,ny=NULL)
box()


setwd(set2)
BIOM <- read.table('datas_biomass.txt',header=T)
names(BIOM) <- c('TRAT','BLOC','ID',1:28)
attach(BIOM)

DY2 <- c(31,60,91,121,152,182,213,244,274,305,335,366,
        397,425,456,486,517,547,578,609,639,670,700,731,
        762,791,822,852)




PS_C11 <-c();PS_C12 <-c();PS_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
PS_C11 <- c(PS_C11, sum(dC111$totPs[dC111$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dC111$totPs[dC111$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_C11 <- data.frame(PS_C11,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dC112$totPs[dC112$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_C11 <- data.frame(PS_C11,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dC113$totPs[dC113$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_C11 <- data.frame(PS_C11,x)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
PS_C13 <- c(PS_C13, sum(dC131$totPs[dC131$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dC131$totPs[dC131$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_C13 <- data.frame(PS_C13,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dC132$totPs[dC132$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_C13 <- data.frame(PS_C13,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dC133$totPs[dC133$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_C13 <- data.frame(PS_C13,x)}

PS_C31 <-c();PS_C32 <-c();PS_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
PS_C31 <- c(PS_C31, sum(dC311$totPs[dC311$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dC311$totPs[dC311$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_C31 <- data.frame(PS_C31,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dC312$totPs[dC312$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_C31 <- data.frame(PS_C31,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dC313$totPs[dC313$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_C31 <- data.frame(PS_C31,x)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
PS_C32 <- c(PS_C32, sum(dC321$totPs[dC321$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dC321$totPs[dC321$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_C32 <- data.frame(PS_C32,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dC322$totPs[dC322$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_C32 <- data.frame(PS_C32,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dC323$totPs[dC323$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_C32 <- data.frame(PS_C32,x)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
PS_C33 <- c(PS_C33, sum(dC331$totPs[dC331$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dC331$totPs[dC331$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_C33 <- data.frame(PS_C33,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dC332$totPs[dC332$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_C33 <- data.frame(PS_C33,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dC333$totPs[dC333$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_C33 <- data.frame(PS_C33,x)}

PS_S31 <-c();PS_S32 <-c();PS_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
PS_S31 <- c(PS_S31, sum(dS311$totPs[dS311$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dS311$totPs[dS311$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_S31 <- data.frame(PS_S31,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dS312$totPs[dS312$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_S31 <- data.frame(PS_S31,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dS313$totPs[dS313$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_S31 <- data.frame(PS_S31,x)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
PS_S32 <- c(PS_S32, sum(dS321$totPs[dS321$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dS321$totPs[dS321$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_S32 <- data.frame(PS_S32,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dS322$totPs[dS322$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_S32 <- data.frame(PS_S32,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dS323$totPs[dS323$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_S32 <- data.frame(PS_S32,x)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
PS_S33 <- c(PS_S33, sum(dS331$totPs[dS331$Tree==j][1:DY2[1]],na.rm=T)*12)}
for (i in 2:12){x <- c();for (j in it){x <- c(x, sum(dS331$totPs[dS331$Tree==j][DY2[i-1]:DY2[i]],na.rm=T)*12)}
PS_S33 <- data.frame(PS_S33,x)}
for (i in 13:24){x <- c();for (j in it){x <- c(x, sum(dS332$totPs[dS332$Tree==j][(DY2[i-1]-365):(DY2[i]-365)],na.rm=T)*12)}
PS_S33 <- data.frame(PS_S33,x)}
for (i in 25:28){x <- c();for (j in it){x <- c(x, sum(dS333$totPs[dS333$Tree==j][(DY2[i-1]-730):(DY2[i]-730)],na.rm=T)*12)}
PS_S33 <- data.frame(PS_S33,x)}

B1 <- BIOM[,4][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==1]
B2 <- BIOM[,5][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==1]
B_C11 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==1]
B2 <- BIOM[,i][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==1]
B_C11 <- data.frame(B_C11, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==2]
B2 <- BIOM[,5][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==2]
B_C12 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==2]
B2 <- BIOM[,i][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==2]
B_C12 <- data.frame(B_C12, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==3]
B2 <- BIOM[,5][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==3]
B_C13 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==3]
B2 <- BIOM[,i][BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==3]
B_C13 <- data.frame(B_C13, B2 - B1)}

B1 <- BIOM[,4][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==1]
B2 <- BIOM[,5][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==1]
B_C31 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==1]
B2 <- BIOM[,i][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==1]
B_C31 <- data.frame(B_C31, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==2]
B2 <- BIOM[,5][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==2]
B_C32 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==2]
B2 <- BIOM[,i][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==2]
B_C32 <- data.frame(B_C32, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==3]
B2 <- BIOM[,5][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==3]
B_C33 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==3]
B2 <- BIOM[,i][BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==3]
B_C33 <- data.frame(B_C33, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==1]
B2 <- BIOM[,5][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==1]
B_S31 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==1]
B2 <- BIOM[,i][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==1]
B_S31 <- data.frame(B_S31, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==2]
B2 <- BIOM[,5][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==2]
B_S32 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==2]
B2 <- BIOM[,i][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==2]
B_S32 <- data.frame(B_S32, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==3]
B2 <- BIOM[,5][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==3]
B_S33 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==3]
B2 <- BIOM[,i][BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==3]
B_S33 <- data.frame(B_S33, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==1]
B2 <- BIOM[,5][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==1]
B_S11 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==1]
B2 <- BIOM[,i][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==1]
B_S11 <- data.frame(B_S11, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==2]
B2 <- BIOM[,5][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==2]
B_S12 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==2]
B2 <- BIOM[,i][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==2]
B_S12 <- data.frame(B_S12, B2 - B1)}
B1 <- BIOM[,4][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==3]
B2 <- BIOM[,5][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==3]
B_S13 <-B2 - B1
for (i in 6:32){
B1 <- BIOM[,i-1][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==3]
B2 <- BIOM[,i][BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==3]
B_S13 <- data.frame(B_S13, B2 - B1)}

DY2 <- c(31,60,91,121,152,182,213,244,274,305,335,366,
        397,425,456,486,517,547,578,609,639,670,700,731,
        762,791,822,852)

BC1 <- data.frame(BIOM[,15][BIOM$TRAT=='C1']-BIOM[,4][BIOM$TRAT=='C1'],BIOM[,27][BIOM$TRAT=='C1']-BIOM[,15][BIOM$TRAT=='C1'],BIOM[,31][BIOM$TRAT=='C1']-BIOM[,27][BIOM$TRAT=='C1'])
BC3 <- data.frame(BIOM[,15][BIOM$TRAT=='C3']-BIOM[,4][BIOM$TRAT=='C3'],BIOM[,27][BIOM$TRAT=='C3']-BIOM[,15][BIOM$TRAT=='C3'],BIOM[,31][BIOM$TRAT=='C3']-BIOM[,27][BIOM$TRAT=='C3'])
BS1 <- data.frame(BIOM[,15][BIOM$TRAT=='S1']-BIOM[,4][BIOM$TRAT=='S1'],BIOM[,27][BIOM$TRAT=='S1']-BIOM[,15][BIOM$TRAT=='S1'],BIOM[,31][BIOM$TRAT=='S1']-BIOM[,27][BIOM$TRAT=='S1'])
BS3 <- data.frame(BIOM[,15][BIOM$TRAT=='S3']-BIOM[,4][BIOM$TRAT=='S3'],BIOM[,27][BIOM$TRAT=='S3']-BIOM[,15][BIOM$TRAT=='S3'],BIOM[,31][BIOM$TRAT=='S3']-BIOM[,27][BIOM$TRAT=='S3'])

MBC1 <-c() ; MBC3 <-c() ; MBS1 <-c() ; MBS3 <-c()
sdMBC1 <-c() ; sdMBC3 <-c() ; sdMBS1 <-c() ; sdMBS3 <-c()
for (i in 1:28){
MBC1 <- c(MBC1, mean(c(B_C11[,i],B_C12[,i],B_C13[,i]),na.rm=T))
MBC3 <- c(MBC3, mean(c(B_C31[,i],B_C32[,i],B_C33[,i]),na.rm=T))
MBS1 <- c(MBS1, mean(c(B_S11[,i],B_S12[,i],B_S13[,i]),na.rm=T))
MBS3 <- c(MBS3, mean(c(B_S31[,i],B_S32[,i],B_S33[,i]),na.rm=T))
sdMBC1 <- c(sdMBC1, sd(c(B_C11[,i],B_C12[,i],B_C13[,i]),na.rm=T))
sdMBC3 <- c(sdMBC3, sd(c(B_C31[,i],B_C32[,i],B_C33[,i]),na.rm=T))
sdMBS1 <- c(sdMBS1, sd(c(B_S11[,i],B_S12[,i],B_S13[,i]),na.rm=T))
sdMBS3 <- c(sdMBS3, sd(c(B_S31[,i],B_S32[,i],B_S33[,i]),na.rm=T))
}

MAT <- c(mean(BC1[,1],na.rm=T),mean(BC3[,1],na.rm=T),mean(BS1[,1],na.rm=T),mean(BS3[,1],na.rm=T),
         mean(BC1[,2],na.rm=T),mean(BC3[,2],na.rm=T),mean(BS1[,2],na.rm=T),mean(BS3[,2],na.rm=T),
         mean(BC1[,3],na.rm=T),mean(BC3[,3],na.rm=T),mean(BS1[,3],na.rm=T),mean(BS3[,3],na.rm=T))
sdMAT <- c(sd(BC1[,1],na.rm=T),sd(BC3[,1],na.rm=T),sd(BS1[,1],na.rm=T),sd(BS3[,1],na.rm=T),
         sd(BC1[,2],na.rm=T),sd(BC3[,2],na.rm=T),sd(BS1[,2],na.rm=T),sd(BS3[,2],na.rm=T),
         sd(BC1[,3],na.rm=T),sd(BC3[,3],na.rm=T),sd(BS1[,3],na.rm=T),sd(BS3[,3],na.rm=T))

par(mar=c(3.5,4.5,1.5,1))
par(yaxs='i')
 barplot(MAT,space=c(0.2,0.1,0.1,0.1,0.5,0.1,0.1,0.1,0.5,0.1,0.1,0.1),col=rep(c(2,4,2,4),3),density=rep(c(1000,1000,50,50),3),
          ylab='Annual stem biomass increament (kgDM/tree)',cex.lab=1.2,names.arg=c('','2011','','','','2012','','','','Ini 2013','',''),
          ylim=c(0,20),cex.names=1)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),
        col=c(2,4,2,4),pch=c(15,15,12,12),bg='white',box.col=0,cex=1.2)
IT <- c(0.5,1.6,2.7,3.7,5.2,6.3,7.4,8.5,10,11.1,12.2,13.3,14.4) +0.2
for (i in 1:12){
  add.errorbars (x=IT[i],y=MAT[i],SE=sdMAT[i],direction='up',barlen=0.04,color=1)}
box()


MON2 <- c(1:28)
par(mar=c(4.5,4.5,0.2,4.5))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(MON2,MBC3,xlab='Month from Jan 2011 to Jun 2013',ylab='Stem biomass increament (kgDM/month)',
    cex.lab=1.2,col=4,lty=1,pch=16,type='b',lwd=2,xlim=c(0,29),ylim=c(0,2))
points(MON2,MBS3,col=4,lty=2,pch=4,type='b',lwd=2)
points(MON2,MBC1,col=2,lty=1,pch=16,type='b',lwd=2)
points(MON2,MBS1,col=2,lty=2,pch=4,type='b',lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
for (i in 1:28){
  add.errorbars (x=MON2[i],y=MBC1[i],SE=sdMBC1[i],direction='vert',barlen=0.04,color=2)}
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=12.5,lty='dotted')
abline(v=24.5,lty='dotted')
text(8,1.75,'2011',font=4)
text(18,1.75,'2012',font=4)
#text(850,1.75,'2013',font=4)
legend('topleft',legend=c('+K +R','-K +R','+K -R','-K -R'),pch=c(16,16,4,4),col=c(4,2,4,2),bg='white',,box.col=0,cex=1.2)
box()

# annuellement
BC11 <- data.frame(BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'],BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'],BIOM[,31][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
BC31 <- data.frame(BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'],BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'],BIOM[,31][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
BS11 <- data.frame(BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'],BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'],BIOM[,31][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
BS31 <- data.frame(BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'],BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,15][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'],BIOM[,31][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,27][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
BC12 <- data.frame(BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'],BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'],BIOM[,31][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
BC32 <- data.frame(BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'],BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'],BIOM[,31][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
BS12 <- data.frame(BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'],BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'],BIOM[,31][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
BS32 <- data.frame(BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'],BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,15][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'],BIOM[,31][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,27][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
BC13 <- data.frame(BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'],BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'],BIOM[,31][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
BC33 <- data.frame(BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'],BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'],BIOM[,31][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
BS13 <- data.frame(BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'],BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'],BIOM[,31][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
BS33 <- data.frame(BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'],BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,15][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'],BIOM[,31][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,27][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])

PS_S31 <-c();PS_S32 <-c();PS_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
PS_S31 <- c(PS_S31, sum(dS311$totPs[dS311$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS312$totPs[dS312$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS313$totPs[dS313$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
PS_S32 <- c(PS_S32, sum(dS321$totPs[dS321$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS322$totPs[dS322$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS323$totPs[dS323$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
PS_S33 <- c(PS_S33, sum(dS331$totPs[dS331$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS332$totPs[dS332$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS333$totPs[dS333$Tree==j][1:167],na.rm=T)*12)}
PS_C31 <-c();PS_C32 <-c();PS_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
PS_C31 <- c(PS_C31, sum(dC311$totPs[dC311$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC312$totPs[dC312$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC313$totPs[dC313$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
PS_C32 <- c(PS_C32, sum(dC321$totPs[dC321$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC322$totPs[dC322$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC323$totPs[dC323$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
PS_C33 <- c(PS_C33, sum(dC331$totPs[dC331$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC332$totPs[dC332$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC333$totPs[dC333$Tree==j][1:167],na.rm=T)*12)}
PS_C11 <-c();PS_C12 <-c();PS_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
PS_C11 <- c(PS_C11, sum(dC111$totPs[dC111$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC112$totPs[dC112$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC113$totPs[dC113$Tree==j][1:167],na.rm=T)*12)}
#it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
#for (j in it){
#PS_C12 <- c(PS_C12, sum(dC121$totPs[dC121$Tree==j][32:364],na.rm=T)*12)}
#for (j in it){
#PS_C12 <- c(PS_C12, sum(dC122$totPs[dC122$Tree==j][1:365],na.rm=T)*12)}
#for (j in it){
#PS_C12 <- c(PS_C12, sum(dC123$totPs[dC123$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
PS_C13 <- c(PS_C13, sum(dC131$totPs[dC131$Tree==j][32:364],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC132$totPs[dC132$Tree==j][1:365],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC133$totPs[dC133$Tree==j][1:167],na.rm=T)*12)}

psC1_A1 <- c(PS_C11[1:36],PS_C13[1:36])/1000
psC1_A2 <- c(PS_C11[37:72],PS_C13[37:72])/1000
psC1_A3 <- c(PS_C11[73:108],PS_C13[73:108])/1000
BC1_A1 <- c(BC11[,1],BC13[,1])
BC1_A2 <- c(BC11[,2],BC13[,2])
BC1_A3 <- c(BC11[,3],BC13[,3])
psC3_A1 <- c(PS_C31[1:36],PS_C32[1:54],PS_C33[1:36]) /1000
psC3_A2 <- c(PS_C31[37:72],PS_C32[55:108],PS_C33[37:72])/1000
psC3_A3 <- c(PS_C31[73:108],PS_C32[109:162],PS_C33[73:108])/1000
BC3_A1 <- c(BC31[,1],BC32[,1],BC33[,1]) 
BC3_A2 <- c(BC31[,2],BC32[,2],BC33[,2])
BC3_A3 <- c(BC31[,3],BC32[,3],BC33[,3])
psS3_A1 <- c(PS_S31[1:36],PS_S32[1:36],PS_S33[1:36])/1000
psS3_A2 <- c(PS_S31[37:72],PS_S32[37:72],PS_S33[37:72])/1000
psS3_A3 <- c(PS_S31[73:108],PS_S32[73:108],PS_S33[73:108])/1000
BS3_A1 <- c(BS31[,1],BS32[,1],BS33[,1])
BS3_A2 <- c(BS31[,2],BS32[,2],BS33[,2])
BS3_A3 <- c(BS31[,3],BS32[,3],BS33[,3])

ftC11 <- lm(BC1_A1~psC1_A1-1)
ftC12 <- lm(BC1_A2~psC1_A2-1)
ftC13 <- lm(BC1_A3~psC1_A3-1)
ftC31 <- lm(BC3_A1~psC3_A1-1)
ftC32 <- lm(BC3_A2~psC3_A2-1)
ftC33 <- lm(BC3_A3~psC3_A3-1)
ftS31 <- lm(BS3_A1~psS3_A1-1)
ftS32 <- lm(BS3_A2~psS3_A2-1)
ftS33 <- lm(BS3_A3~psS3_A3-1)

par(mar=c(4.5,4.5,3,1))
par(xaxs='i',yaxs='i')
par(mfrow=c(1,3))
plot(psC3_A1,BC3_A1,col=4,xlab='Annual totPs (kgC/tree/y)',ylab='Stem biomass increament (kgDM/y)',cex.lab=1.2)
points(psS3_A1,BS3_A1,col=5)
points(psC1_A1,BC1_A1,col=2)
abline(ftC11,col=2,lwd=2)
abline(ftC31,col=4,lwd=2)
abline(ftS31,col=5,lwd=2)
grid(col=1)
legend('topleft',legend=c(paste('+K +R, CUE = ',round(summary(ftC31)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('+K -R, CUE = ',round(summary(ftS31)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('-K +R, CUE = ',round(summary(ftC11)$coef[1],5),' kgDM/kgC',sep='')),
                          text.col=c(4,5,2),pch=NA,lty=1,col=c(4,5,2),bg='white',box.col=0,cex=1.4)
box()
title('2011')
plot(psC3_A2,BC3_A2,col=4,xlab='Annual totPs (kgC/tree/y)',ylab='Stem biomass increament (kgDM/y)',cex.lab=1.2)
points(psS3_A2,BS3_A2,col=5)
points(psC1_A2,BC1_A2,col=2)
abline(ftC12,col=2,lwd=2)
abline(ftC32,col=4,lwd=2)
abline(ftS32,col=5,lwd=2)
grid(col=1)
legend('topleft',legend=c(paste('+K +R, CUE = ',round(summary(ftC32)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('+K -R, CUE = ',round(summary(ftS32)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('-K +R, CUE = ',round(summary(ftC12)$coef[1],5),' kgDM/kgC',sep='')),
                          text.col=c(4,5,2),pch=NA,lty=1,col=c(4,5,2),bg='white',box.col=0,cex=1.4)
box()
title('2012')
plot(psC3_A3,BC3_A3,col=4,xlab='Annual totPs (kgC/tree/y)',ylab='Stem biomass increament (kgDM/y)',cex.lab=1.2)
points(psS3_A3,BS3_A3,col=5)
points(psC1_A3,BC1_A3,col=2)
abline(ftC13,col=2,lwd=2)
abline(ftC33,col=4,lwd=2)
abline(ftS33,col=5,lwd=2)
grid(col=1)
legend('topleft',legend=c(paste('+K +R, CUE = ',round(summary(ftC33)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('+K -R, CUE = ',round(summary(ftS33)$coef[1],5),' kgDM/kgC',sep=''),
                          paste('-K +R, CUE = ',round(summary(ftC13)$coef[1],5),' kgDM/kgC',sep='')),
                          text.col=c(4,5,2),pch=NA,lty=1,col=c(4,5,2),bg='white',box.col=0,cex=1.4)
box()
title('Ini 2013')




# par mois
C1CUE <- c() ; C3CUE <-c() ; S3CUE <-c()
sdC1CUE <- c() ; sdC3CUE <-c() ; sdS3CUE <-c()

for (i in 1:28){
C1CUE <- c(C1CUE, mean(c(B_C11[,i]/PS_C11[,i],B_C13[,i]/PS_C13[,i]),na.rm=T))
C3CUE <- c(C3CUE, mean(c(B_C31[,i]/PS_C31[,i],B_C32[,i]/PS_C32[,i],B_C33[,i]/PS_C33[,i]),na.rm=T))
S3CUE <- c(S3CUE, mean(c(B_S31[,i]/PS_S31[,i],B_S32[,i]/PS_S32[,i],B_S33[,i]/PS_S33[,i]),na.rm=T))
sdC1CUE <- c(sdC1CUE, sd(c(B_C11[,i]/PS_C11[,i],B_C13[,i]/PS_C13[,i]),na.rm=T))
sdC3CUE <- c(sdC3CUE, sd(c(B_C31[,i]/PS_C31[,i],B_C32[,i]/PS_C32[,i],B_C33[,i]/PS_C33[,i]),na.rm=T))
sdS3CUE <- c(sdS3CUE, sd(c(B_S31[,i]/PS_S31[,i],B_S32[,i]/PS_S32[,i],B_S33[,i]/PS_S33[,i]),na.rm=T))
}

MON2 <- c(1:28)

par(mar=c(4.5,4.5,0.2,4.5))
par(yaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(MON2,C3CUE,xlab='Month from Jan 2011 to Jun 2013',ylab='Carbon use efficiency (kgDM/gC)',
    cex.lab=1.2,col=2,pch=16,type='b',lwd=2,xlim=c(0,29),ylim=c(0,0.0015))
points(S3CUE,col=4,pch=16,type='b',lwd=2)
points(C1CUE,col=3,pch=16,type='b',lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=12.5,lty='dotted')
abline(v=24.5,lty='dotted')
text(300,7.5,'2011',font=4)
text(550,7.5,'2012',font=4)
text(850,7.5,'2013',font=4)
for (i in 1:28){
  add.errorbars (x=MON2[i],y=C3CUE[i],SE=sdC3CUE[i],direction='vert',barlen=0.04,color=2)}
legend('topleft',legend=c('+K +R','-K +R','-K -R'),pch=16,col=c(2,3,4),bg='white',,box.col=0,cex=1.2)
box()




# A et E
require(gss)
DAY <- 1:length(PSC3)
GFC3 <- ssanova(PSC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGFC3 <- predict(GFC3,newC3,se=T)
GFC1 <- ssanova(PSC1~DAY)
newC1 <- data.frame(DAY,alpha=1.1)
estGFC1 <- predict(GFC1,newC1,se=T)
GFS3 <- ssanova(PSS3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGFS3 <- predict(GFS3,newS3,se=T)
GEC3 <- ssanova(LEC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGEC3 <- predict(GEC3,newC3,se=T)
GEC1 <- ssanova(LEC1~DAY,alpha=1.1)
newC1 <- data.frame(DAY)
estGEC1 <- predict(GEC1,newC1,se=T)
GES3 <- ssanova(LES3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGES3 <- predict(GES3,newS3,se=T)


MAT <- matrix(c(2611,4275,2639,1935,2713,1256,2782,3529,1866),nrow=3,byrow=T)
MATE <- matrix(c(968,1519,697,920,1167,560,559,768,277),nrow=3,byrow=T)
YEA <-c('2011','2012','ini 2013')
ARG <-c('+K +R','-K +R','+K -R')

par(mfrow=c(2,1))
par(mar=c(4,4,1,30))
par(yaxs='i',xaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(PSC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily gross photosynthesis'~~(gC~m^-2~d^-1)),
      cex.lab=1.2,col=1,pch=1,mgp=c(2.5,1,0),ylim=c(0,25))
points(PSC1)
points(PSS3)
lines(DAY,estGFC3$fit,col=4,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFC3$fit,rev(estGFC3$fit+estGFC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFC3$fit,rev(estGFC3$fit-estGFC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGFC1$fit,col=2,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFC1$fit,rev(estGFC1$fit+estGFC1$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFC1$fit,rev(estGFC1$fit-estGFC1$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGFS3$fit,col=5,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGFS3$fit,rev(estGFS3$fit+estGFS3$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGFS3$fit,rev(estGFS3$fit-estGFS3$se.fit)),col=mycols[5],border=NA)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,22.5,'2011',font=4)
text(550,22.5,'2012',font=4)
text(850,22.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()

par(new=T)
par(mar=c(4,40,1,1))
par(yaxs='i')
barplot(MAT,col=rep(c(mycols[4],mycols[2],mycols[5]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Annual A (gC/m�/y)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,4500))
grid(nx=NA,ny=NULL)
box()


par(mar=c(4,4,1,30))
par(yaxs='i',xaxs='i')
barplot(PPT,border=NA,ann=F,axes=F,ylim=c(0,120))
axis(4,at=c(0:6)*20,labels=c(0:6)*20)
par(new=T)
plot(LEC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('Daily transpiration'~~(L~m^-2~d^-1)),
      cex.lab=1.2,col=1,pch=1,mgp=c(2.5,1,0),ylim=c(0,10))
points(LES3)
points(LEC1)
lines(DAY,estGEC3$fit,col=4,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGEC3$fit,rev(estGEC3$fit+estGEC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGEC3$fit,rev(estGEC3$fit-estGEC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGEC1$fit,col=2,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGEC1$fit,rev(estGEC1$fit+estGEC1$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGEC1$fit,rev(estGEC1$fit-estGEC1$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGES3$fit,col=5,lwd=2)
polygon(c(DAY,rev(DAY)),c(estGES3$fit,rev(estGES3$fit+estGES3$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGES3$fit,rev(estGES3$fit-estGES3$se.fit)),col=mycols[5],border=NA)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
mtext(expression('Daily precipitation'~(mm~day^-1)),side=4,line=2.5,cex=1.2 )
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,22.5,'2011',font=4)
text(550,22.5,'2012',font=4)
text(850,22.5,'2013',font=4)
legend('topleft',legend=c('+K +R','+K -R','-K +R'),pch=16,col=c(4,5,2),bg='white',,box.col=0,cex=1.2)
box()

par(new=T)
par(mar=c(4,40,1,1))
par(yaxs='i')
barplot(MATE,col=rep(c(mycols[4],mycols[5],mycols[2]),3),beside=T,border=NA,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=ARG,space=c(0.2,0,0,0.4,0,0,0.4,0,0),
        ylab='Annual E (L/m�/y)',xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,1800))
grid(nx=NA,ny=NULL)
box()























