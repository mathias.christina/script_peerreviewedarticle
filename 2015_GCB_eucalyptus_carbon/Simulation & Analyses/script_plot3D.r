set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")            
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Cluster")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/MAESTRA_input.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_MAESTRA_3D_EXC.r")

setwd(set2)
require(Maeswrap)
dat2 <- read.table('Position.txt',header=T)
setwd(set1)

file.copy(paste(set4,"/0001_confile.dat",sep=""), set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_phyC1.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_phyC3.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_phyS1.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_phyS3.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_trees.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_met.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_strC1.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_strC3.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_strS1.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_strS3.dat",sep=""),set1,overwrite=TRUE)
file.copy(paste(set4,"/0001_watpars.dat",sep=""),set1,overwrite=TRUE)

file.rename("0001_confile.dat","confile.dat")
file.rename("0001_trees.dat","trees.dat")
file.rename("0001_met.dat","met.dat")
file.rename("0001_phyC1.dat","phyC1.dat")
file.rename("0001_phyC3.dat","phyC3.dat")
file.rename("0001_phyS1.dat","phyS1.dat")
file.rename("0001_phyS3.dat","phyS3.dat")
file.rename("0001_strC1.dat","strC1.dat")
file.rename("0001_strC3.dat","strC3.dat")
file.rename("0001_strS1.dat","strS1.dat")
file.rename("0001_strS3.dat","strS3.dat")
file.rename("0001_watpars.dat","watpars.dat")


NUM1 <- c(1:4254)
TYP1 <- c(dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==1],dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==2],dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==3],
       dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==1],dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==2],dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==3],
       dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==1],dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==2],dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==3],
       dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==1],dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==2],dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==3],
       dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==1],dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==2],dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==3],
       dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==1],dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==2],dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==3],
       dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==1],dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==2],dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==3],
       dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==1],dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==2],dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==3])
TYP2 <- c(dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==1],dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==2],dat2$Typ[dat2$Trat=='C1'][dat2$Bloc[dat2$Trat=='C1']==3],
       rep(1,length(c(dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==1],dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==2],dat2$Typ[dat2$Trat=='C2'][dat2$Bloc[dat2$Trat=='C2']==3]))),
       dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==1],dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==2],dat2$Typ[dat2$Trat=='C3'][dat2$Bloc[dat2$Trat=='C3']==3],
       dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==1],dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==2],dat2$Typ[dat2$Trat=='S1'][dat2$Bloc[dat2$Trat=='S1']==3],
       rep(1,length(c(dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==1],dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==2],dat2$Typ[dat2$Trat=='S2'][dat2$Bloc[dat2$Trat=='S2']==3]))),
       dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==1],dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==2],dat2$Typ[dat2$Trat=='S3'][dat2$Bloc[dat2$Trat=='S3']==3],
       dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==1],dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==2],dat2$Typ[dat2$Trat=='P1'][dat2$Bloc[dat2$Trat=='P1']==3],
       dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==1],dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==2],dat2$Typ[dat2$Trat=='P2'][dat2$Bloc[dat2$Trat=='P2']==3])

NUM <- NUM1[TYP2==2]


replacePAR(datfile="confile.dat", parname = "itargets", namelist = "treescon", NUM)

replacePAR(datfile="trees.dat", parname = "XMAX", namelist = "plot", max(dat2$Xpos)+6)
replacePAR(datfile="trees.dat", parname = "YMAX", namelist = "plot", max(dat2$Ypos)+6)


#  Plotstand(readstrfiles=F,treesfile='trees.dat',crownshape="halfellipsoid")
DATES <- c("20/12/10","28/01/11","24/02/11","28/03/11","10/05/11","26/05/11",
           "30/06/11","29/07/11","25/08/11","27/09/11","31/10/11","23/11/11", "23/04/12","20/09/12",'28/01/13', '02/05/13')   
require(rgl)
  
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_MAESTRA_3D_EXC.r")
  
#windows(16.9/2.54,8.05/2.54)

  Plotstand2 (treesfile = "trees.dat", strfile = c("strC1.dat","strC3.dat","strS1.dat","strS3.dat"), crownshape = "halfellipsoid", 
    readstrfiles = TRUE, targethighlight = TRUE, addNarrow = TRUE,
    xyaxes = TRUE, labcex = 1, axiscex = 1, verbose = FALSE, MulticolorS=T, Title,color=c(5,4,'salmon',2),Date =14)


#rgl.postscript("FIG1.pdf","pdf") 

par(mar=c(0,0,0,0))
plot(1,1,col=0,ann=F,axes=F)
legend('left',legend=c('+K +W','- K +W','+K - W','- K - W'),pch=15,col=c(4,5,2,'salmon'),
       bty='n',cex=3.0)



treesfile = "trees.dat"; strfile = c("strC1.dat","strC3.dat","strS1.dat","strS3.dat"); crownshape = "halfellipsoid"; 
    readstrfiles = TRUE; targethighlight = TRUE; addNarrow = TRUE;
    xyaxes = TRUE; labcex = 1; axiscex = 1; verbose = FALSE; MulticolorS=T; Title='main';color=c(2,4,'salmon',5);Date =14
