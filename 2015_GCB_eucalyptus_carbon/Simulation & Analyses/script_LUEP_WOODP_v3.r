set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
set6<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set6)


dC111 <- read.table('1_Dayflx.dat')
dC112 <- read.table('2_Dayflx.dat')
dC113 <- read.table('3_Dayflx.dat')
dC121 <- read.table('4_Dayflx.dat')
dC122 <- read.table('5_Dayflx.dat')
dC123 <- read.table('6_Dayflx.dat')
dC131 <- read.table('7_Dayflx.dat')
dC132 <- read.table('8_Dayflx.dat')
dC133 <- read.table('9_Dayflx.dat')

dC311 <- read.table('10_Dayflx.dat')
dC312 <- read.table('11_Dayflx.dat')
dC313 <- read.table('12_Dayflx.dat')
dC321 <- read.table('13_Dayflx.dat')
dC322 <- read.table('14_Dayflx.dat')
dC323 <- read.table('15_Dayflx.dat')
dC331 <- read.table('16_Dayflx.dat')
dC332 <- read.table('17_Dayflx.dat')
dC333 <- read.table('18_Dayflx.dat')

dS111 <- read.table('19_Dayflx.dat')
dS112 <- read.table('20_Dayflx.dat') # vide
dS113 <- read.table('21_Dayflx.dat')
dS121 <- read.table('22_Dayflx.dat') # jour 305 arret au milieu
dS122 <- read.table('23_Dayflx.dat')
dS123 <- read.table('24_Dayflx.dat')
dS131 <- read.table('25_Dayflx.dat')
dS132 <- read.table('26_Dayflx.dat')
dS133 <- read.table('27_Dayflx.dat')

dS311 <- read.table('28_Dayflx.dat')
dS312 <- read.table('29_Dayflx.dat')
dS313 <- read.table('30_Dayflx.dat')
dS321 <- read.table('31_Dayflx.dat')
dS322 <- read.table('32_Dayflx.dat')
dS323 <- read.table('33_Dayflx.dat')
dS331 <- read.table('34_Dayflx.dat')
dS332 <- read.table('35_Dayflx.dat')
dS333 <- read.table('36_Dayflx.dat')
setwd(set4)
names(dC111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dC311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dC333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS111) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS112) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS113) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS121) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS122) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS123) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS131) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS132) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS133) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")

names(dS311) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS312) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS313) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS321) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS322) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS323) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS331) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS332) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
names(dS333) <- c("DOY", "Tree", "Spec", "absPAR", "absNIR", "absTherm", "totPs", "totRf", "netPs", "totLE1", "totLE2", "totH")
setwd(set6)

setwd(set2)
BIOM <- read.table('datas_biomass.txt',header=T)
HEIGHT <- read.table('datas_height.txt',header=T)
DIAM <- read.table('datas_Diam.txt',header=T)
CROWN <- read.table('datas_RadCrown.txt',header=T)
names(BIOM) <- c('TRAT','BLOC','ID',1:29)
names(BIOM) <- c('TRAT','BLOC','ID',7:35)
attach(BIOM)

setwd(set5)
require(Maeswrap)
POS <- readPAR('0001_trees.dat',parname='xycoords',namelist='xy')
YPOS <- POS[(1:4254)*2]
XPOS <- POS[(1:4254)*2-1]
setwd(set2)

BC11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
BC12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
BC13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
BC31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
BC32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
BC33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
BS11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
BS12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
BS13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
BS31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,4][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
BS32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,4][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
BS33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,4][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])
TBC11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
TBC12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
TBC13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
TBC31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
TBC32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
TBC33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
TBS11 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
TBS12 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
TBS13 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
TBS31 <- data.frame(BIOM[,9][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
TBS32 <- data.frame(BIOM[,9][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
TBS33 <- data.frame(BIOM[,9][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])
HC11 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C1'])
HC12 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C1'])
HC13 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C1'])
HC31 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C3'])
HC32 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C3'])
HC33 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C3'])
HS11 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S1'])
HS12 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S1'])
HS13 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S1'])
HS31 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S3'])
HS32 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S3'])
HS33 <- data.frame(HEIGHT[,9][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S3'])
DC11 <- data.frame(DIAM[,9][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='C1'])
DC12 <- data.frame(DIAM[,9][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='C1'])
DC13 <- data.frame(DIAM[,9][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='C1'])
DC31 <- data.frame(DIAM[,9][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='C3'])
DC32 <- data.frame(DIAM[,9][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='C3'])
DC33 <- data.frame(DIAM[,9][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='C3'])
DS11 <- data.frame(DIAM[,9][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='S1'])
DS12 <- data.frame(DIAM[,9][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='S1'])
DS13 <- data.frame(DIAM[,9][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='S1'])
DS31 <- data.frame(DIAM[,9][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='S3'])
DS32 <- data.frame(DIAM[,9][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='S3'])
DS33 <- data.frame(DIAM[,9][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='S3'])
CC11 <- data.frame(CROWN[,9][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='C1'])
CC12 <- data.frame(CROWN[,9][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='C1'])
CC13 <- data.frame(CROWN[,9][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='C1'])
CC31 <- data.frame(CROWN[,9][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='C3'])
CC32 <- data.frame(CROWN[,9][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='C3'])
CC33 <- data.frame(CROWN[,9][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='C3'])
CS11 <- data.frame(CROWN[,9][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='S1'])
CS12 <- data.frame(CROWN[,9][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='S1'])
CS13 <- data.frame(CROWN[,9][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='S1'])
CS31 <- data.frame(CROWN[,9][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='S3'])
CS32 <- data.frame(CROWN[,9][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='S3'])
CS33 <- data.frame(CROWN[,9][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='S3'])
for (i in c(15,21,27,32)){
BC11 <- data.frame(BC11,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1']-BIOM[,i-6][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
BC12 <- data.frame(BC12,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1']-BIOM[,i-6][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
BC13 <- data.frame(BC13,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1']-BIOM[,i-6][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
BC31 <- data.frame(BC31,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3']-BIOM[,i-6][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
BC32 <- data.frame(BC32,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3']-BIOM[,i-6][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
BC33 <- data.frame(BC33,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3']-BIOM[,i-6][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
BS11 <- data.frame(BS11,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1']-BIOM[,i-6][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
BS12 <- data.frame(BS12,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1']-BIOM[,i-6][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
BS13 <- data.frame(BS13,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1']-BIOM[,i-6][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
BS31 <- data.frame(BS31,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3']-BIOM[,i-6][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
BS32 <- data.frame(BS32,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3']-BIOM[,i-6][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
BS33 <- data.frame(BS33,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3']-BIOM[,i-6][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])
TBC11 <- data.frame(TBC11,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C1'])
TBC12 <- data.frame(TBC12,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C1'])
TBC13 <- data.frame(TBC13,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C1'])
TBC31 <- data.frame(TBC31,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='C3'])
TBC32 <- data.frame(TBC32,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='C3'])
TBC33 <- data.frame(TBC33,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='C3'])
TBS11 <- data.frame(TBS11,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S1'])
TBS12 <- data.frame(TBS12,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S1'])
TBS13 <- data.frame(TBS13,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S1'])
TBS31 <- data.frame(TBS31,BIOM[,i][BIOM$BLOC==1][BIOM$TRAT[BIOM$BLOC==1]=='S3'])
TBS32 <- data.frame(TBS32,BIOM[,i][BIOM$BLOC==2][BIOM$TRAT[BIOM$BLOC==2]=='S3'])
TBS33 <- data.frame(TBS33,BIOM[,i][BIOM$BLOC==3][BIOM$TRAT[BIOM$BLOC==3]=='S3'])
HC11 <- data.frame(HC11,HEIGHT[,i][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C1'])
HC12 <- data.frame(HC12,HEIGHT[,i][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C1'])
HC13 <- data.frame(HC13,HEIGHT[,i][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C1'])
HC31 <- data.frame(HC31,HEIGHT[,i][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='C3'])
HC32 <- data.frame(HC32,HEIGHT[,i][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='C3'])
HC33 <- data.frame(HC33,HEIGHT[,i][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='C3'])
HS11 <- data.frame(HS11,HEIGHT[,i][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S1'])
HS12 <- data.frame(HS12,HEIGHT[,i][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S1'])
HS13 <- data.frame(HS13,HEIGHT[,i][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S1'])
HS31 <- data.frame(HS31,HEIGHT[,i][HEIGHT$BLOC==1][HEIGHT$TRAT[HEIGHT$BLOC==1]=='S3'])
HS32 <- data.frame(HS32,HEIGHT[,i][HEIGHT$BLOC==2][HEIGHT$TRAT[HEIGHT$BLOC==2]=='S3'])
HS33 <- data.frame(HS33,HEIGHT[,i][HEIGHT$BLOC==3][HEIGHT$TRAT[HEIGHT$BLOC==3]=='S3'])
DC11 <- data.frame(DC11,DIAM[,i][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='C1'])
DC12 <- data.frame(DC12,DIAM[,i][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='C1'])
DC13 <- data.frame(DC13,DIAM[,i][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='C1'])
DC31 <- data.frame(DC31,DIAM[,i][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='C3'])
DC32 <- data.frame(DC32,DIAM[,i][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='C3'])
DC33 <- data.frame(DC33,DIAM[,i][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='C3'])
DS11 <- data.frame(DS11,DIAM[,i][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='S1'])
DS12 <- data.frame(DS12,DIAM[,i][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='S1'])
DS13 <- data.frame(DS13,DIAM[,i][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='S1'])
DS31 <- data.frame(DS31,DIAM[,i][DIAM$BLOC==1][DIAM$TRAT[DIAM$BLOC==1]=='S3'])
DS32 <- data.frame(DS32,DIAM[,i][DIAM$BLOC==2][DIAM$TRAT[DIAM$BLOC==2]=='S3'])
DS33 <- data.frame(DS33,DIAM[,i][DIAM$BLOC==3][DIAM$TRAT[DIAM$BLOC==3]=='S3'])
CC11 <- data.frame(CC11,CROWN[,i][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='C1'])
CC12 <- data.frame(CC12,CROWN[,i][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='C1'])
CC13 <- data.frame(CC13,CROWN[,i][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='C1'])
CC31 <- data.frame(CC31,CROWN[,i][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='C3'])
CC32 <- data.frame(CC32,CROWN[,i][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='C3'])
CC33 <- data.frame(CC33,CROWN[,i][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='C3'])
CS11 <- data.frame(CS11,CROWN[,i][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='S1'])
CS12 <- data.frame(CS12,CROWN[,i][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='S1'])
CS13 <- data.frame(CS13,CROWN[,i][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='S1'])
CS31 <- data.frame(CS31,CROWN[,i][CROWN$BLOC==1][CROWN$TRAT[CROWN$BLOC==1]=='S3'])
CS32 <- data.frame(CS32,CROWN[,i][CROWN$BLOC==2][CROWN$TRAT[CROWN$BLOC==2]=='S3'])
CS33 <- data.frame(CS33,CROWN[,i][CROWN$BLOC==3][CROWN$TRAT[CROWN$BLOC==3]=='S3'])
}


PS_C31 <-c();PS_C32 <-c();PS_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
PS_C31 <- c(PS_C31, sum(dC311$totPs[dC311$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC311$totPs[dC311$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC312$totPs[dC312$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC312$totPs[dC312$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C31 <- c(PS_C31, sum(dC313$totPs[dC313$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
PS_C32 <- c(PS_C32, sum(dC321$totPs[dC321$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC321$totPs[dC321$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC322$totPs[dC322$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC322$totPs[dC322$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C32 <- c(PS_C32, sum(dC323$totPs[dC323$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
PS_C33 <- c(PS_C33, sum(dC331$totPs[dC331$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC331$totPs[dC331$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC332$totPs[dC332$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC332$totPs[dC332$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C33 <- c(PS_C33, sum(dC333$totPs[dC333$Tree==j][1:167],na.rm=T)*12)}

PS_C11 <-c();PS_C12 <-c();PS_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
PS_C11 <- c(PS_C11, sum(dC111$totPs[dC111$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC111$totPs[dC111$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC112$totPs[dC112$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC112$totPs[dC112$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C11 <- c(PS_C11, sum(dC113$totPs[dC113$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
for (j in it){
PS_C12 <- c(PS_C12, sum(dC121$totPs[dC121$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC121$totPs[dC121$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC122$totPs[dC122$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC122$totPs[dC122$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C12 <- c(PS_C12, sum(dC123$totPs[dC123$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
PS_C13 <- c(PS_C13, sum(dC131$totPs[dC131$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC131$totPs[dC131$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC132$totPs[dC132$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC132$totPs[dC132$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_C13 <- c(PS_C13, sum(dC133$totPs[dC133$Tree==j][1:167],na.rm=T)*12)}

PS_S31 <-c();PS_S32 <-c();PS_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
PS_S31 <- c(PS_S31, sum(dS311$totPs[dS311$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS311$totPs[dS311$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS312$totPs[dS312$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS312$totPs[dS312$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S31 <- c(PS_S31, sum(dS313$totPs[dS313$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
PS_S32 <- c(PS_S32, sum(dS321$totPs[dS321$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS321$totPs[dS321$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS322$totPs[dS322$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS322$totPs[dS322$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S32 <- c(PS_S32, sum(dS323$totPs[dS323$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
PS_S33 <- c(PS_S33, sum(dS331$totPs[dS331$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS331$totPs[dS331$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS332$totPs[dS332$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS332$totPs[dS332$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S33 <- c(PS_S33, sum(dS333$totPs[dS333$Tree==j][1:167],na.rm=T)*12)}
PS_S11 <-c();PS_S12 <-c();PS_S13 <-c()
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==1]
for (j in it){
PS_S11 <- c(PS_S11, sum(dS111$totPs[dS111$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS111$totPs[dS111$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS112$totPs[dS112$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS112$totPs[dS112$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S11 <- c(PS_S11, sum(dS113$totPs[dS113$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==2]
for (j in it){
PS_S12 <- c(PS_S12, sum(dS121$totPs[dS121$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS121$totPs[dS121$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS122$totPs[dS122$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS122$totPs[dS122$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S12 <- c(PS_S12, sum(dS123$totPs[dS123$Tree==j][1:167],na.rm=T)*12)}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==3]
for (j in it){
PS_S13 <- c(PS_S13, sum(dS131$totPs[dS131$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS131$totPs[dS131$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS132$totPs[dS132$Tree==j][1:181],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS132$totPs[dS132$Tree==j][182:365],na.rm=T)*12)}
for (j in it){
PS_S13 <- c(PS_S13, sum(dS133$totPs[dS133$Tree==j][1:167],na.rm=T)*12)}

APAR_C31 <-c();APAR_C32 <-c();APAR_C33 <-c()
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==1]
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC311$absPAR[dC311$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC311$absPAR[dC311$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC312$absPAR[dC312$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC312$absPAR[dC312$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C31 <- c(APAR_C31, sum(dC313$absPAR[dC313$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==2]
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC321$absPAR[dC321$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC321$absPAR[dC321$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC322$absPAR[dC322$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC322$absPAR[dC322$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C32 <- c(APAR_C32, sum(dC323$absPAR[dC323$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='C3'][BLOC[TRAT=='C3']==3]
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC331$absPAR[dC331$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC331$absPAR[dC331$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC332$absPAR[dC332$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC332$absPAR[dC332$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C33 <- c(APAR_C33, sum(dC333$absPAR[dC333$Tree==j][1:167],na.rm=T))}

APAR_C11 <-c();APAR_C12 <-c();APAR_C13 <-c()
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==1]
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC111$absPAR[dC111$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC111$absPAR[dC111$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC112$absPAR[dC112$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC112$absPAR[dC112$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C11 <- c(APAR_C11, sum(dC113$absPAR[dC113$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==2]
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC121$absPAR[dC121$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC121$absPAR[dC121$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC122$absPAR[dC122$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC122$absPAR[dC122$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C12 <- c(APAR_C12, sum(dC123$absPAR[dC123$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='C1'][BLOC[TRAT=='C1']==3]
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC131$absPAR[dC131$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC131$absPAR[dC131$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC132$absPAR[dC132$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC132$absPAR[dC132$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_C13 <- c(APAR_C13, sum(dC133$absPAR[dC133$Tree==j][1:167],na.rm=T))}

APAR_S31 <-c();APAR_S32 <-c();APAR_S33 <-c()
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==1]
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS311$absPAR[dS311$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS311$absPAR[dS311$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS312$absPAR[dS312$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS312$absPAR[dS312$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S31 <- c(APAR_S31, sum(dS313$absPAR[dS313$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==2]
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS321$absPAR[dS321$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS321$absPAR[dS321$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS322$absPAR[dS322$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS322$absPAR[dS322$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S32 <- c(APAR_S32, sum(dS323$absPAR[dS323$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='S3'][BLOC[TRAT=='S3']==3]
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS331$absPAR[dS331$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS331$absPAR[dS331$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS332$absPAR[dS332$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS332$absPAR[dS332$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S33 <- c(APAR_S33, sum(dS333$absPAR[dS333$Tree==j][1:167],na.rm=T))}
APAR_S11 <-c();APAR_S12 <-c();APAR_S13 <-c()
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==1]
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS111$absPAR[dS111$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS111$absPAR[dS111$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS112$absPAR[dS112$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS112$absPAR[dS112$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S11 <- c(APAR_S11, sum(dS113$absPAR[dS113$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==2]
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS121$absPAR[dS121$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS121$absPAR[dS121$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS122$absPAR[dS122$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS122$absPAR[dS122$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S12 <- c(APAR_S12, sum(dS123$absPAR[dS123$Tree==j][1:167],na.rm=T))}
it <- ID[TRAT=='S1'][BLOC[TRAT=='S1']==3]
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS131$absPAR[dS131$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS131$absPAR[dS131$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS132$absPAR[dS132$Tree==j][1:181],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS132$absPAR[dS132$Tree==j][182:365],na.rm=T))}
for (j in it){
APAR_S13 <- c(APAR_S13, sum(dS133$absPAR[dS133$Tree==j][1:167],na.rm=T))}


AGEC3 <- c(rep(12,126),rep(18,126),rep(24,126),rep(30,126),rep(35,126))
NUMC3 <- rep(1:126,5)
BC3 <- c(BC31[,1],BC32[,1],BC33[,1],BC31[,2],BC32[,2],BC33[,2],BC31[,3],BC32[,3],BC33[,3],BC31[,4],BC32[,4],BC33[,4],BC31[,5],BC32[,5],BC33[,5])
BC3 <- c(BC31[,1],BC32[,1],BC33[,1],BC31[,2],BC32[,2],BC33[,2],BC31[,3],BC32[,3],BC33[,3],BC31[,4],BC32[,4],BC33[,4],BC31[,5],BC32[,5],BC33[,5])
TBC3 <- c(TBC31[,1],TBC32[,1],TBC33[,1],TBC31[,2],TBC32[,2],TBC33[,2],TBC31[,3],TBC32[,3],TBC33[,3],TBC31[,4],TBC32[,4],TBC33[,4],TBC31[,5],TBC32[,5],TBC33[,5])
HC3 <- c(HC31[,1],HC32[,1],HC33[,1],HC31[,2],HC32[,2],HC33[,2],HC31[,3],HC32[,3],HC33[,3],HC31[,4],HC32[,4],HC33[,4],HC31[,5],HC32[,5],HC33[,5])
DC3 <- c(DC31[,1],DC32[,1],DC33[,1],DC31[,2],DC32[,2],DC33[,2],DC31[,3],DC32[,3],DC33[,3],DC31[,4],DC32[,4],DC33[,4],DC31[,5],DC32[,5],DC33[,5])
CC3 <- c(CC31[,1],CC32[,1],CC33[,1],CC31[,2],CC32[,2],CC33[,2],CC31[,3],CC32[,3],CC33[,3],CC31[,4],CC32[,4],CC33[,4],CC31[,5],CC32[,5],CC33[,5])
PSC3 <- c(PS_C31[1:36],PS_C32[1:54],PS_C33[1:36],
          PS_C31[37:72],PS_C32[55:108],PS_C33[37:72],
          PS_C31[73:108],PS_C32[109:162],PS_C33[73:108],
          PS_C31[109:144],PS_C32[163:216],PS_C33[109:144],
          PS_C31[145:180],PS_C32[217:270],PS_C33[145:180])
APARC3 <- c(APAR_C31[1:36],APAR_C32[1:54],APAR_C33[1:36],
          APAR_C31[37:72],APAR_C32[55:108],APAR_C33[37:72],
          APAR_C31[73:108],APAR_C32[109:162],APAR_C33[73:108],
          APAR_C31[109:144],APAR_C32[163:216],APAR_C33[109:144],
          APAR_C31[145:180],APAR_C32[217:270],APAR_C33[145:180])

AGEC1 <- c(rep(12,72),rep(18,72),rep(24,72),rep(30,72),rep(35,72))
NUMC1 <- rep(1:72,5)
BC1 <- c(BC11[,1],BC13[,1],BC11[,2],BC13[,2],BC11[,3],BC13[,3],BC11[,4],BC13[,4],BC11[,5],BC13[,5])
TBC1 <- c(TBC11[,1],TBC13[,1],TBC11[,2],TBC13[,2],TBC11[,3],TBC13[,3],TBC11[,4],TBC13[,4],TBC11[,5],TBC13[,5])
HC1 <- c(HC11[,1],HC13[,1],HC11[,2],HC13[,2],HC11[,3],HC13[,3],HC11[,4],HC13[,4],HC11[,5],HC13[,5])
DC1 <- c(DC11[,1],DC13[,1],DC11[,2],DC13[,2],DC11[,3],DC13[,3],DC11[,4],DC13[,4],DC11[,5],DC13[,5])
CC1 <- c(CC11[,1],CC13[,1],CC11[,2],CC13[,2],CC11[,3],CC13[,3],CC11[,4],CC13[,4],CC11[,5],CC13[,5])
PSC1 <- c(PS_C11[1:36],PS_C13[1:36],
          PS_C11[37:72],PS_C13[37:72],
          PS_C11[73:108],PS_C13[73:108],
          PS_C11[109:144],PS_C13[109:144],
          PS_C11[145:180],PS_C13[145:180])
APARC1 <- c(APAR_C11[1:36],APAR_C13[1:36],
          APAR_C11[37:72],APAR_C13[37:72],
          APAR_C11[73:108],APAR_C13[73:108],
          APAR_C11[109:144],APAR_C13[109:144],
          APAR_C11[145:180],APAR_C13[145:180])


AGES3 <- c(rep(12,108),rep(18,108),rep(24,108),rep(30,108),rep(35,108))
NUMS3 <- rep(1:108,5)
BS3 <- c(BS31[,1],BS32[,1],BS33[,1],BS31[,2],BS32[,2],BS33[,2],BS31[,3],BS32[,3],BS33[,3],BS31[,4],BS32[,4],BS33[,4],BS31[,5],BS32[,5],BS33[,5])
TBS3 <- c(TBS31[,1],TBS32[,1],TBS33[,1],TBS31[,2],TBS32[,2],TBS33[,2],TBS31[,3],TBS32[,3],TBS33[,3],TBS31[,4],TBS32[,4],TBS33[,4],TBS31[,5],TBS32[,5],TBS33[,5])
HS3 <- c(HS31[,1],HS32[,1],HS33[,1],HS31[,2],HS32[,2],HS33[,2],HS31[,3],HS32[,3],HS33[,3],HS31[,4],HS32[,4],HS33[,4],HS31[,5],HS32[,5],HS33[,5])
DS3 <- c(DS31[,1],DS32[,1],DS33[,1],DS31[,2],DS32[,2],DS33[,2],DS31[,3],DS32[,3],DS33[,3],DS31[,4],DS32[,4],DS33[,4],DS31[,5],DS32[,5],DS33[,5])
CS3 <- c(CS31[,1],CS32[,1],CS33[,1],CS31[,2],CS32[,2],CS33[,2],CS31[,3],CS32[,3],CS33[,3],CS31[,4],CS32[,4],CS33[,4],CS31[,5],CS32[,5],CS33[,5])
PSS3 <- c(PS_S31[1:36],PS_S32[1:36],PS_S33[1:36],
          PS_S31[37:72],PS_S32[37:72],PS_S33[37:72],
          PS_S31[73:108],PS_S32[73:108],PS_S33[73:108],
          PS_S31[109:144],PS_S32[109:144],PS_S33[109:144],
          PS_S31[145:180],PS_S32[145:180],PS_S33[145:180])
APARS3 <- c(APAR_S31[1:36],APAR_S32[1:36],APAR_S33[1:36],
          APAR_S31[37:72],APAR_S32[37:72],APAR_S33[37:72],
          APAR_S31[73:108],APAR_S32[73:108],APAR_S33[73:108],
          APAR_S31[109:144],APAR_S32[109:144],APAR_S33[109:144],
          APAR_S31[145:180],APAR_S32[145:180],APAR_S33[145:180])

AGES1 <- c(rep(12,72),rep(18,72),rep(24,72),rep(30,72),rep(35,72))
NUMS1 <- rep(1:72,5)
BS1 <- c(BS12[,1],BS13[,1],BS12[,2],BS13[,2],BS12[,3],BS13[,3],BS12[,4],BS13[,4],BS12[,5],BS13[,5])
TBS1 <- c(TBS12[,1],TBS13[,1],TBS12[,2],TBS13[,2],TBS12[,3],TBS13[,3],TBS12[,4],TBS13[,4],TBS12[,5],TBS13[,5])
HS1 <- c(HS12[,1],HS13[,1],HS12[,2],HS13[,2],HS12[,3],HS13[,3],HS12[,4],HS13[,4],HS12[,5],HS13[,5])
DS1 <- c(DS12[,1],DS13[,1],DS12[,2],DS13[,2],DS12[,3],DS13[,3],DS12[,4],DS13[,4],DS12[,5],DS13[,5])
CS1 <- c(CS12[,1],CS13[,1],CS12[,2],CS13[,2],CS12[,3],CS13[,3],CS12[,4],CS13[,4],CS12[,5],CS13[,5])
PSS1 <- c(PS_S12[1:36],PS_S13[1:36],
          PS_S12[37:72],PS_S13[37:72],
          PS_S12[73:108],PS_S13[73:108],
          PS_S12[109:144],PS_S13[109:144],
          PS_S12[145:180],PS_S13[145:180])
APARS1 <- c(APAR_S12[1:36],APAR_S13[1:36],
          APAR_S12[37:72],APAR_S13[37:72],
          APAR_S12[73:108],APAR_S13[73:108],
          APAR_S12[109:144],APAR_S13[109:144],
          APAR_S12[145:180],APAR_S13[145:180])

# indice de competition
setwd(set5)
require(Maeswrap)

isp <- readPAR('0001_trees.dat',parname='ispecies',namelist='speclist')
DIAM <- readPAR('0001_trees.dat',parname='value',namelist='indivdiam')
diam <- DIAM[((1:4254)*16)]
xypos <- readPAR('0001_trees.dat',parname='xycoords',namelist='xy')
XPOS <- xypos[((1:4254)*2-1)]
YPOS <- xypos[((1:4254)*2)]

# on a XPOS et YPOS, positionnons les 
IDC1 <- c(BIOM$ID[BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==1],
          BIOM$ID[BIOM$TRAT=='C1'][BIOM$BLOC[BIOM$TRAT=='C1']==3])
IDC3 <- c(BIOM$ID[BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==1],
          BIOM$ID[BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==2],
          BIOM$ID[BIOM$TRAT=='C3'][BIOM$BLOC[BIOM$TRAT=='C3']==3])
IDS3 <- c(BIOM$ID[BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==1],
          BIOM$ID[BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==2],
          BIOM$ID[BIOM$TRAT=='S3'][BIOM$BLOC[BIOM$TRAT=='S3']==3])
IDS1 <- c(BIOM$ID[BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==2],
          BIOM$ID[BIOM$TRAT=='S1'][BIOM$BLOC[BIOM$TRAT=='S1']==3])
XPC1 <- XPOS[IDC1] ; XPC3 <- XPOS[IDC3] ; XPS1 <- XPOS[IDS1] ; XPS3 <- XPOS[IDS3] ; 
YPC1 <- YPOS[IDC1] ; YPC3 <- YPOS[IDC3] ; YPS1 <- YPOS[IDS1] ; YPS3 <- YPOS[IDS3] ; 



N=22

ICC1 <-c()
for (i in 1:length(IDC1)){
DIST <- sqrt((XPOS-XPC1[i])^2 + (YPOS-YPC1[i])^2)
DIST <- replace(DIST,DIST==min(DIST,na.rm=T),NA)
id <-c() ; Li <- c();Di<-c()
for (j in 1:N){
id <- c(id,which(DIST==min(DIST,na.rm=T)))
Li <- c(Li,DIST[which(DIST==min(DIST,na.rm=T))])
Di <- c(Di,diam[which(DIST==min(DIST,na.rm=T))])
DIST[id]=NA
}
id <- id[1:N]
ICC1 <- c(ICC1, sum(Di[1:N]/(diam[IDC1[i]]*Li[1:N])))
}

ICC3 <-c()
for (i in 1:length(IDC3)){
DIST <- sqrt((XPOS-XPC3[i])^2 + (YPOS-YPC3[i])^2)
DIST <- replace(DIST,DIST==min(DIST,na.rm=T),NA)
id <-c() ; Li <- c();Di<-c()
for (j in 1:N){
id <- c(id,which(DIST==min(DIST,na.rm=T)))
Li <- c(Li,DIST[which(DIST==min(DIST,na.rm=T))])
Di <- c(Di,diam[which(DIST==min(DIST,na.rm=T))])
DIST[id]=NA
}
id <- id[1:N]
ICC3 <- c(ICC3, sum(Di[1:N]/(diam[IDC3[i]]*Li[1:N])))
}
ICS1 <-c()
for (i in 1:length(IDS1)){
DIST <- sqrt((XPOS-XPS1[i])^2 + (YPOS-YPS1[i])^2)
DIST <- replace(DIST,DIST==min(DIST,na.rm=T),NA)
id <-c() ; Li <- c();Di<-c()
for (j in 1:N){
id <- c(id,which(DIST==min(DIST,na.rm=T)))
Li <- c(Li,DIST[which(DIST==min(DIST,na.rm=T))])
Di <- c(Di,diam[which(DIST==min(DIST,na.rm=T))])
DIST[id]=NA
}
id <- id[1:N]
ICS1 <- c(ICS1, sum(Di[1:N]/(diam[IDS1[i]]*Li[1:N])))
}
ICS3 <-c()
for (i in 1:length(IDS3)){
DIST <- sqrt((XPOS-XPS3[i])^2 + (YPOS-YPS3[i])^2)
DIST <- replace(DIST,DIST==min(DIST,na.rm=T),NA)
id <-c() ; Li <- c();Di<-c()
for (j in 1:N){
id <- c(id,which(DIST==min(DIST,na.rm=T)))
Li <- c(Li,DIST[which(DIST==min(DIST,na.rm=T))])
Di <- c(Di,diam[which(DIST==min(DIST,na.rm=T))])
DIST[id]=NA
}
id <- id[1:N]
ICS3 <- c(ICS3, sum(Di[1:N]/(diam[IDS3[i]]*Li[1:N])))
}

X <- seq(0,40,length.out=100)
Y <- 1/ (1+ exp(-0.4*X))
Y2 <- 1/ (1+ exp(-0.3*(X-20)))
Y3 <- X^5.5/ (X^5.5+ 20^5.5)

plot(X,Y,type='l',ylim=c(0,1))
points(X,Y2,col=2,type='l')
points(X,Y3,col=3,type='l')


fitC1 <- nls(BC1*0.47/PSC1*1000~a/(1+exp(-b*(AGEC1-T0))), start=c(a=0.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitC3 <- nls(BC3*0.47/PSC3*1000~a/(1+exp(-b*(AGEC3-T0))), start=c(a=0.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitS1 <- nls(BS1*0.47/PSS1*1000~a/(1+exp(-b*(AGES1-T0))), start=c(a=0.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitS3 <- nls(BS3*0.47/PSS3*1000~a/(1+exp(-b*(AGES3-T0))), start=c(a=0.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitC1 <- nls(BC1*0.47/PSC1*1000~a*AGEC1^b/(AGEC1^b+T0^b), start=c(a=0.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitC3 <- nls(BC3*0.47/PSC3*1000~a*AGEC3^b/(AGEC3^b+T0^b), start=c(a=0.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitS1 <- nls(BS1*0.47/PSS1*1000~a*AGES1^b/(AGES1^b+T0^b), start=c(a=0.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitS3 <- nls(BS3*0.47/PSS3*1000~a*AGES3^b/(AGES3^b+T0^b), start=c(a=0.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')

require(nls2)

fitLC1 <- nls(BC1/APARC1*1000~a/(1+exp(-b*(AGEC1-T0))), start=c(a=1.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLC3 <- nls(BC3/APARC3*1000~a/(1+exp(-b*(AGEC3-T0))), start=c(a=1.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLS1 <- nls(BS1/APARS1*1000~a/(1+exp(-b*(AGES1-T0))), start=c(a=1.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLS3 <- nls(BS3/APARS3*1000~a/(1+exp(-b*(AGES3-T0))), start=c(a=1.3,b=0.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLC1 <- nls(BC1/APARC1*1000~a*AGEC1^b/(AGEC1^b+T0^b), start=c(a=1.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLC3 <- nls(BC3/APARC3*1000~a*AGEC3^b/(AGEC3^b+T0^b), start=c(a=1.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
fitLS1 <- nls(BS1/APARS1*1000~a*AGES1^b/(AGES1^b+T0^b), start=c(a=1.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')
BS32 <- BS3
BS32[325:432] <- BS3[325:432]*1.1
fitLS3 <- nls(BS32/APARS3*1000~a*AGES3^b/(AGES3^b+T0^b), start=c(a=2.3,b=5.5,T0=20))#,lower=c(0,0,0),algorithm='port')

X <- seq(0,40,length.out=100)

YC3 <- summary(fitC3)$coef[1,1]  / (1+exp(-summary(fitC3)$coef[2,1] *( X-summary(fitC3)$coef[3,1])))# + summary(fitC3)$coef[3,1]
YC1 <- summary(fitC1)$coef[1,1]  / (1+exp(-summary(fitC1)$coef[2,1] *( X-summary(fitC1)$coef[3,1])))# + summary(fitC3)$coef[3,1]
YS3 <- summary(fitS3)$coef[1,1]  / (1+exp(-summary(fitS3)$coef[2,1] *( X-summary(fitS3)$coef[3,1])))# + summary(fitC3)$coef[3,1]
YS1 <- summary(fitS1)$coef[1,1]  / (1+exp(-summary(fitS1)$coef[2,1] *( X-summary(fitS1)$coef[3,1])))# + summary(fitC3)$coef[3,1]
YLC3 <- summary(fitLC3)$coef[1,1]  / (1+exp(-summary(fitLC3)$coef[2,1] *( X-summary(fitLC3)$coef[3,1])))# + summary(fitLC3)$coef[3,1]
YLC1 <- summary(fitLC1)$coef[1,1]  / (1+exp(-summary(fitLC1)$coef[2,1] *( X-summary(fitLC1)$coef[3,1])))# + summary(fitLC3)$coef[3,1]
YLS3 <- summary(fitLS3)$coef[1,1]  / (1+exp(-summary(fitLS3)$coef[2,1] *( X-summary(fitLS3)$coef[3,1])))# + summary(fitLC3)$coef[3,1]
YLS1 <- summary(fitLS1)$coef[1,1]  / (1+exp(-summary(fitLS1)$coef[2,1] *( X-summary(fitLS1)$coef[3,1])))# + summary(fitLC3)$coef[3,1]

YC3 <- summary(fitC3)$coef[1,1]*X^summary(fitC3)$coef[2,1]  / (
      (X^summary(fitC3)$coef[2,1]+summary(fitC3)$coef[3,1] ^summary(fitC3)$coef[2,1]))
YC1 <- summary(fitC1)$coef[1,1]*X^summary(fitC1)$coef[2,1]  / (
      (X^summary(fitC1)$coef[2,1]+summary(fitC1)$coef[3,1] ^summary(fitC1)$coef[2,1]))
YS3 <- summary(fitS3)$coef[1,1]*X^summary(fitS3)$coef[2,1]  / (
      (X^summary(fitS3)$coef[2,1]+summary(fitS3)$coef[3,1] ^summary(fitS3)$coef[2,1]))
YS1 <- summary(fitS1)$coef[1,1]*X^summary(fitS1)$coef[2,1]  / (
      (X^summary(fitS1)$coef[2,1]+summary(fitS1)$coef[3,1] ^summary(fitS1)$coef[2,1]))
YLC3 <- summary(fitLC3)$coef[1,1]*X^summary(fitLC3)$coef[2,1]  / (
      (X^summary(fitLC3)$coef[2,1]+summary(fitLC3)$coef[3,1] ^summary(fitLC3)$coef[2,1]))
YLC1 <- summary(fitLC1)$coef[1,1]*X^summary(fitLC1)$coef[2,1]  / (
      (X^summary(fitLC1)$coef[2,1]+summary(fitLC1)$coef[3,1] ^summary(fitLC1)$coef[2,1]))
YLS3 <- summary(fitLS3)$coef[1,1]*X^summary(fitLS3)$coef[2,1]  / (
      (X^summary(fitLS3)$coef[2,1]+summary(fitLS3)$coef[3,1] ^summary(fitLS3)$coef[2,1]))
YLS1 <- summary(fitLS1)$coef[1,1]*X^summary(fitLS1)$coef[2,1]  / (
      (X^summary(fitLS1)$coef[2,1]+summary(fitLS1)$coef[3,1] ^summary(fitLS1)$coef[2,1]))

mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols3 <- adjustcolor(palette(), alpha.f = 0.3)
mysalmon <- adjustcolor('salmon',alpha.f = 0.3)

par(mfrow=c(1,2))
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(AGEC3, BC3*0.47/PSC3*1000,xlim=c(0,36),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression('%GPP'[stem]),cex.lab=1.2,ylim=c(0,0.5))
points(AGEC1, BC1*0.47/PSC1*1000,xlim=c(0,36),col=mycols[5])
points(AGES3, BS3*0.47/PSS3*1000,xlim=c(0,36),col=mycols[2])
points(AGES1, BS1*0.47/PSS1*1000,xlim=c(0,36),col=mysalmon)
lines(X,YC3,col=4,lwd=2)
lines(X,YC1,col=5,lwd=2)
lines(X,YS3,col=2,lwd=2)
lines(X,YS1,col='salmon',lwd=2)
legend('topleft',legend=c('+K+W','-K+W','+K-W','-K-W'),pch=16,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1.2)
box()

plot(AGEC3, BC3*1000/APARC3,xlim=c(0,36),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUE[p]~('gDM/MJ')),cex.lab=1.2,ylim=c(0,2))
points(AGEC1, BC1*1000/APARC1,xlim=c(0,36),col=mycols[5])
points(AGES3, BS3*1000/APARS3,xlim=c(0,36),col=mycols[2])
points(AGES1, BS1*1000/APARS1,xlim=c(0,36),col=mysalmon)
lines(X,YLC3,col=4,lwd=2)
lines(X,YLC1,col=5,lwd=2)
lines(X,YLS3,col=2,lwd=2)
lines(X,YLS1,col='salmon',lwd=2)
legend('topleft',legend=c('+K+W','-K+W','+K-W','-K-W'),pch=16,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1.2)
box()


# bIOMASS

fitC1 <- nls(BC1*0.47/PSC1*1000~a*TBC1/(b+TBC1)+T0, start=c(a=0.3,b=0.5,T0=20),lower=c(0,0,0),algorithm='port')
fitC3 <- nls(BC3*0.47/PSC3*1000~a*TBC3/(b+TBC3)+T0, start=c(a=0.3,b=0.5,T0=20),lower=c(0,0,0),algorithm='port')
fitS1 <- nls(BS1*0.47/PSS1*1000~a*TBS1/(b+TBS1)+T0, start=c(a=0.3,b=0.5,T0=20),lower=c(0,0,0),algorithm='port')
fitS3 <- nls(BS3*0.47/PSS3*1000~a*TBS3/(b+TBS3)+T0, start=c(a=0.3,b=0.5,T0=20),lower=c(0,0,0),algorithm='port')

require(nls2)

fitLC1 <- nls(BC1/APARC1*1000~a*TBC1/(b+TBC1)+T0, start=c(a=1.3,b=5,T0=2),lower=c(0,0,0),algorithm='port')
fitLC3 <- nls(BC3/APARC3*1000~a*TBC3/(b+TBC3)+T0, start=c(a=1.3,b=5,T0=2),lower=c(0,0,0),algorithm='port')
fitLS1 <- nls(BS1/APARS1*1000~a*TBS1/(b+TBS1)+T0, start=c(a=1.3,b=5,T0=2),lower=c(0,0,0),algorithm='port')
fitLS3 <- nls(BS3/APARS3*1000~a*TBS3/(b+TBS3)+T0, start=c(a=1.3,b=5,T0=2),lower=c(0,0,0),algorithm='port')

X2 <- seq(0,50,length.out=100)

Y2C3 <- summary(fitC3)$coef[1,1]*X2  / (summary(fitC3)$coef[2,1] + X2)+summary(fitC3)$coef[3,1]# + summary(fitC3)$coef[3,1]
Y2C1 <- summary(fitC1)$coef[1,1]*X2  / (summary(fitC1)$coef[2,1] + X2)+summary(fitC1)$coef[3,1]# + summary(fitC3)$coef[3,1]
Y2S3 <- summary(fitS3)$coef[1,1]*X2  / (summary(fitS3)$coef[2,1] + X2)+summary(fitS3)$coef[3,1]# + summary(fitC3)$coef[3,1]
Y2S1 <- summary(fitS1)$coef[1,1]*X2  / (summary(fitS1)$coef[2,1] + X2)+summary(fitS1)$coef[3,1]# + summary(fitC3)$coef[3,1]
Y2LC3 <- summary(fitLC3)$coef[1,1]*X2  / (summary(fitLC3)$coef[2,1] + X2)+summary(fitLC3)$coef[3,1]# + summary(fitLC3)$coef[3,1]
Y2LC1 <- summary(fitLC1)$coef[1,1]*X2  / (summary(fitLC1)$coef[2,1] + X2)+summary(fitLC1)$coef[3,1]# + summary(fitLC3)$coef[3,1]
Y2LS3 <- summary(fitLS3)$coef[1,1]*X2  / (summary(fitLS3)$coef[2,1] + X2)+summary(fitLS3)$coef[3,1]# + summary(fitLC3)$coef[3,1]
Y2LS1 <- summary(fitLS1)$coef[1,1]*X2 / (summary(fitLS1)$coef[2,1] + X2)+summary(fitLS1)$coef[3,1]# + summary(fitLC3)$coef[3,1]


mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols3 <- adjustcolor(palette(), alpha.f = 0.3)
mysalmon <- adjustcolor('salmon',alpha.f = 0.3)

par(mfrow=c(2,2))
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(AGEC3, BC3*0.47/PSC3*1000,xlim=c(0,36),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=1)
points(AGEC1, BC1*0.47/PSC1*1000,xlim=c(0,36),col=mycols[5],pch=1)
points(AGES3, BS3*0.47/PSS3*1000,xlim=c(0,36),col=mycols[2],pch=1)
points(AGES1, BS1*0.47/PSS1*1000,xlim=c(0,36),col=mysalmon,pch=1)
lines(X,YC3,col=4,lwd=2)
lines(X,YC1,col=5,lwd=2)
lines(X,YS3,col=2,lwd=2)
lines(X,YS1,col='salmon',lwd=2)
legend('topleft',legend=c('+K+W','-K+W','+K-W','-K-W'),pch=1,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1.2)
box()

plot(AGEC3, BC3*1000/APARC3,xlim=c(0,36),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUE[p]~('gDM/MJ')),cex.lab=1.2,ylim=c(0,2),pch=1)
points(AGEC1, BC1*1000/APARC1,xlim=c(0,36),col=mycols[5],pch=1)
points(AGES3, BS3*1000/APARS3,xlim=c(0,36),col=mycols[2],pch=1)
points(AGES1, BS1*1000/APARS1,xlim=c(0,36),col=mysalmon,pch=1)
lines(X,YLC3,col=4,lwd=2)
lines(X,YLC1,col=5,lwd=2)
lines(X,YLS3,col=2,lwd=2)
lines(X,YLS1,col='salmon',lwd=2)
legend('topleft',legend=c('+K+W','-K+W','+K-W','-K-W'),pch=1,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1.2)
box()

par(mfrow=c(1,2))
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(TBC3, BC3*0.47/PSC3*1000,xlim=c(0,20),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Trunk biomass (kgDM)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=1)
points(TBC1, BC1*0.47/PSC1*1000,xlim=c(0,20),col=mycols[5],pch=1)
points(TBS3, BS3*0.47/PSS3*1000,xlim=c(0,20),col=mycols[2],pch=1)
points(TBS1, BS1*0.47/PSS1*1000,xlim=c(0,20),col=mysalmon,pch=1)
lines(X2,Y2C3,col=4,lwd=2)
lines(X2,Y2C1,col=5,lwd=2)
lines(X2,Y2S3,col=2,lwd=2)
lines(X2,Y2S1,col='salmon',lwd=2)
legend('topleft',legend=c('+K +W','- K +W','+K - W','- K - W'),pch=1,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
legend('bottomright',legend=c(expression(tau[max]~'='~0.39~';'~TB['1/2']~'='~3.25~';'~tau[0]~'='~0),
                              expression(tau[max]~'='~'0.20'~';'~TB['1/2']~'='~1.38~';'~tau[0]~'='~0),
                              expression(tau[max]~'='~0.48~';'~TB['1/2']~'='~10.5~';'~tau[0]~'='~0.04),
                              expression(tau[max]~'='~0.25~';'~TB['1/2']~'='~3.04~';'~tau[0]~'='~0.01)),
                              pch=NA,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
box()

plot(TBC3, BC3*1000/APARC3,xlim=c(0,20),col=mycols[4],mgp=c(2.5,1,0),
     xlab='Trunk biomass (kgDM)',ylab=expression(LUE[p]~('gDM/MJ')),cex.lab=1.2,ylim=c(0,2),pch=1)
points(TBC1, BC1*1000/APARC1,xlim=c(0,20),col=mycols[5],pch=1)
points(TBS3, BS3*1000/APARS3,xlim=c(0,20),col=mycols[2],pch=1)
points(TBS1, BS1*1000/APARS1,xlim=c(0,20),col=mysalmon,pch=1)
lines(X2,Y2LC3,col=4,lwd=2)
lines(X2,Y2LC1,col=5,lwd=2)
lines(X2,Y2LS3,col=2,lwd=2)
lines(X2,Y2LS1,col='salmon',lwd=2)
#legend('topleft',legend=c('+K+W','-K+W','+K-W','-K-W'),pch=1,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(expression('+K +W '~tau[max]~'='~1.79~';'~TB['1/2']~'='~10.4~';'~tau[0]~'='~0.02),
                              expression('- K +W '~tau[max]~'='~0.61~';'~TB['1/2']~'='~2.22~';'~tau[0]~'='~0),
                              expression('+K - W '~tau[max]~'='~2.08~';'~TB['1/2']~'='~24.3~';'~tau[0]~'='~0.17),
                              expression('- K - W '~tau[max]~'='~0.87~';'~TB['1/2']~'='~4.62~';'~tau[0]~'='~0.03)),
                              pch=1,lty=1,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
box()



require(nlme)

NUM <- c(NUMC3,NUMC1+126,NUMS3+126+72,NUMS1+126+72+108)
AGE <- c(AGEC3,AGEC1,AGES3,AGES1)
TB <- c(TBC3,TBC1,TBS3,TBS1)
BIO <- c(BC3,BC1,BS3,BS1)
PS <- c(PSC3,PSC1,PSS3,PSS1)
aPAR <- c(APARC3,APARC1,APARS3,APARS1)


X <- AGE[NUM==1]
X2 <- TB[NUM==1]
Y <- BIO[NUM==1]*0.47/PS[NUM==1]*1000
modele <-  nls(Y~a*X^b/(X^b+T0^b), start=c(a=0.3,b=5.5,T0=20),
                control=list(maxiter=1000))
COEF <- as.numeric(coef(modele))
modele2 <-  nls(Y~a*X2/(b+X2)+T0, start=c(a=0.3,b=1,T0=0),
                control=list(maxiter=1000),lower=c(0,0,0),algorithm='port')
COEF2 <- as.numeric(coef(modele2))
for (i in 2:378){
X <- AGE[NUM==i]
X2 <- TB[NUM==i]
Y <- BIO[NUM==i]*0.47/PS[NUM==i]*1000
modele <-  try(nls(Y~a*X^b/(X^b+T0^b), start=c(a=0.3,b=5.5,T0=20),
                control=list(maxiter=1000)))
  if (inherits(modele,"try-error")) { COEF <- data.frame(COEF,c(NA,NA,NA))}
  else {
COEF <- data.frame(COEF,as.numeric(coef(modele)))}
modele2 <-  try(nls(Y~a*X2/(b+X2)+T0, start=c(a=0.3,b=1,T0=0),
                control=list(maxiter=1000),lower=c(0,0,0),algorithm='port'))
  if (inherits(modele2,"try-error")) { COEF2 <- data.frame(COEF2,c(NA,NA,NA))}
  else {
COEF2 <- data.frame(COEF2,as.numeric(coef(modele2)))}
}

X <- AGE[NUM==1]
X2 <- TB[NUM==1]
Y <- BIO[NUM==1]/aPAR[NUM==1]*1000
modele3 <-  nls(Y~a*X^b/(X^b+T0^b), start=c(a=1.3,b=5.5,T0=20),
                control=list(maxiter=1000))
COEF3 <- as.numeric(coef(modele3))
modele4 <-  nls(Y~a*X2/(b+X2)+T0, start=c(a=1.3,b=0.5,T0=0),
                control=list(maxiter=1000),lower=c(0,0,0),algorithm='port')
COEF4 <- as.numeric(coef(modele4))
for (i in 2:378){
X <- AGE[NUM==i]
X2 <- TB[NUM==i]
Y <- BIO[NUM==i]/aPAR[NUM==i]*1000
modele3 <-  try(nls(Y~a*X^b/(X^b+T0^b), start=c(a=1.3,b=2.5,T0=20),
                control=list(maxiter=1000)))
  if (inherits(modele3,"try-error")) { COEF3 <- data.frame(COEF3,c(NA,NA,NA))}
  else {COEF3 <- data.frame(COEF3,as.numeric(coef(modele3)))}
modele4 <-  try(nls(Y~a*X2/(b+X2)+T0, start=c(a=1.3,b=0.5,T0=0),
                control=list(maxiter=1000),lower=c(0,0,0),algorithm='port'))
  if (inherits(modele4,"try-error")) { COEF4 <- data.frame(COEF4,c(NA,NA,NA))}
  else {COEF4 <- data.frame(COEF4,as.numeric(coef(modele4)))
}}


A1 <- as.numeric(COEF[1,])
B1 <- as.numeric(COEF[2,])
T01 <- as.numeric(COEF[3,])
A2 <- as.numeric(COEF2[1,])
B2 <- as.numeric(COEF2[2,])
T02 <- as.numeric(COEF2[3,])
A3 <- as.numeric(COEF3[1,])
B3 <- as.numeric(COEF3[2,])
T03 <- as.numeric(COEF3[3,])
A4 <- as.numeric(COEF4[1,])
B4 <- as.numeric(COEF4[2,])
T04 <- as.numeric(COEF4[3,])


IC <- c(ICC3, ICC1, ICS3,ICS1)

KEFF <- c(rep('+K',126),rep('-K',72),rep('+K',108),rep('-K',72))
WEFF <- c(rep('+W',126+72),rep('-W',72+108))
BLOC <- c(rep(1,36),rep(2,54),rep(3,36),rep(1,36),rep(3,36),rep(1,36),rep(2,36),rep(3,36),rep(2,36),rep(3,36))


A3 <- replace(A3,c(A3>6),NA)
B3 <- replace(B3,c(A3>6),NA)
T03 <- replace(T03,c(A3>6),NA)
T03 <- replace(T03,c(T03>80),NA)
A1 <- replace(A1,c(A1>2),NA)
B1 <- replace(B1,c(A1>2),NA)
T01 <- replace(T01,c(A1>2),NA)
T01 <- replace(T01,c(T01>80),NA)

ana11 <- lme(A1~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana12 <- lme(B1~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana13 <- lme(T01~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana31 <- lme(A3~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana32 <- lme(B3~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana33 <- lme(T03~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)



A4 <- replace(A4,c(A4>30),NA)
B4 <- replace(B4,c(A4>30),NA)
T04 <- replace(T04,c(A4>30),NA)

ana21 <- lme(A2~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana22 <- lme(B2~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana23 <- lme(T02~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana41 <- lme(A4~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana42 <- lme(B4~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana43 <- lme(T04~KEFF*WEFF*IC, random=~1+1|BLOC/WEFF,na.action=na.omit)


boxplot(T02~IC)

A <- replace(A,c(T0>50),NA)
B <- replace(B,c(T0>50),NA)
T0 <- replace(T0,c(T0>50),NA)
B <- replace(B,c(A>3),NA)
T0 <- replace(T0,c(A>3),NA)
A <- replace(A,c(A>3),NA)

HEFF <- c(HC3[AGEC3==35],HC1[AGEC1==35],HS3[AGES3==35],HS1[AGES1==35])

IDC3 <- c(which(HC3[AGEC3==35]==min(HC3[AGEC3==35],na.rm=T)),80,which(HC3[AGEC3==35]==max(HC3[AGEC3==35],na.rm=T)))
IDC1 <- c(which(HC1[AGEC1==35]==min(HC1[AGEC1==35],na.rm=T)),50,71)#which(HC1[AGEC1==35]==max(HC1[AGEC1==35],na.rm=T)))
IDS3 <- c(57,70,which(HS3[AGES3==35]==max(HS3[AGES3==35],na.rm=T)))
IDS1 <- c(which(HS1[AGES1==35]==min(HS1[AGES1==35],na.rm=T)),65,which(HS1[AGES1==35]==max(HS1[AGES1==35],na.rm=T)))

X <- seq(0,40,length.out=100)

par(mar=c(4,4,1,1))
par(mfrow=c(2,2))
par(xaxs='i',yaxs='i')
plot(AGEC3[NUMC3==IDC3[1]], BC3[NUMC3==IDC3[1]]*0.47/PSC3[NUMC3==IDC3[1]]*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC3[NUMC3==IDC3[2]], BC3[NUMC3==IDC3[2]]*0.47/PSC3[NUMC3==IDC3[2]]*1000,col=mycols[1],pch=16)
points(AGEC3[NUMC3==IDC3[3]], BC3[NUMC3==IDC3[3]]*0.47/PSC3[NUMC3==IDC3[3]]*1000,col=mycols[1],pch=16)
Y <-  A[1:126][IDC3[1]] / (1+ exp( -B[1:126][IDC3[1]] * (X - T0[1:126][IDC3[1]])))
lines(X, Y,lwd=2,col=4,lty=1)
Y <-  A[1:126][IDC3[2]] / (1+ exp( -B[1:126][IDC3[2]] * (X - T0[1:126][IDC3[2]])))
lines(X, Y,lwd=2,col=4,lty=2)
Y <-  A[1:126][IDC3[3]] / (1+ exp( -B[1:126][IDC3[3]] * (X - T0[1:126][IDC3[3]])))
lines(X, Y,lwd=2,col=4,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=4,bty='n',cex=1.2)
legend('bottomright',legend='+K+W',pch=NA,cex=1.6,bty='n')
plot(AGEC1[NUMC1==IDC1[1]], BC1[NUMC1==IDC1[1]]*0.47/PSC1[NUMC1==IDC1[1]]*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC1[NUMC1==IDC1[2]], BC1[NUMC1==IDC1[2]]*0.47/PSC1[NUMC1==IDC1[2]]*1000,col=mycols[1],pch=16)
points(AGEC1[NUMC1==IDC1[3]], BC1[NUMC1==IDC1[3]]*0.47/PSC1[NUMC1==IDC1[3]]*1000,col=mycols[1],pch=16)
Y <-  A[127:198][IDC1[1]] / (1+ exp( -B[127:198][IDC1[1]] * (X - T0[127:198][IDC1[1]])))
lines(X, Y,lwd=2,col=5,lty=1)
Y <-  A[127:198][IDC1[2]] / (1+ exp( -B[127:198][IDC1[2]] * (X - T0[127:198][IDC1[2]])))
lines(X, Y,lwd=2,col=5,lty=2)
Y <-  A[127:198][IDC1[3]] / (1+ exp( -B[127:198][IDC1[3]] * (X - T0[127:198][IDC1[3]])))
lines(X, Y,lwd=2,col=5,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=5,bty='n',cex=1.2)
legend('bottomright',legend='-K+W',pch=NA,cex=1.6,bty='n')
plot(AGES3[NUMS3==IDS3[1]], BS3[NUMS3==IDS3[1]]*0.47/PSS3[NUMS3==IDS3[1]]*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES3[NUMS3==IDS3[2]], BS3[NUMS3==IDS3[2]]*0.47/PSS3[NUMS3==IDS3[2]]*1000,col=mycols[1],pch=16)
points(AGES3[NUMS3==IDS3[3]], BS3[NUMS3==IDS3[3]]*0.47/PSS3[NUMS3==IDS3[3]]*1000,col=mycols[1],pch=16)
Y <-  A[199:306][IDS3[1]] / (1+ exp( -B[199:306][IDS3[1]] * (X - T0[199:306][IDS3[1]])))
lines(X, Y,lwd=2,col=2,lty=1)
Y <-  A[199:306][IDS3[2]] / (1+ exp( -B[199:306][IDS3[2]] * (X - T0[199:306][IDS3[2]])))
lines(X, Y,lwd=2,col=2,lty=2)
Y <-  A[199:306][IDS3[3]] / (1+ exp( -B[199:306][IDS3[3]] * (X - T0[199:306][IDS3[3]])))
lines(X, Y,lwd=2,col=2,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=2,bty='n',cex=1.2)
legend('bottomright',legend='+K-W',pch=NA,cex=1.6,bty='n')
plot(AGES1[NUMS1==IDS1[1]], BS1[NUMS1==IDS1[1]]*0.47/PSS1[NUMS1==IDS1[1]]*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES1[NUMS1==IDS1[2]], BS1[NUMS1==IDS1[2]]*0.47/PSS1[NUMS1==IDS1[2]]*1000,col=mycols[1],pch=16)
points(AGES1[NUMS1==IDS1[3]], BS1[NUMS1==IDS1[3]]*0.47/PSS1[NUMS1==IDS1[3]]*1000,col=mycols[1],pch=16)
Y <-  A[307:378][IDS1[1]] / (1+ exp( -B[307:378][IDS1[1]] * (X - T0[307:378][IDS1[1]])))
lines(X, Y,lwd=2,col='salmon',lty=1)
Y <-  A[307:378][IDS1[2]] / (1+ exp( -B[307:378][IDS1[2]] * (X - T0[307:378][IDS1[2]])))
lines(X, Y,lwd=2,col='salmon',lty=2)
Y <-  A[307:378][IDS1[3]] / (1+ exp( -B[307:378][IDS1[3]] * (X - T0[307:378][IDS1[3]])))
lines(X, Y,lwd=2,col='salmon',lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col='salmon',bty='n',cex=1.2)
legend('bottomright',legend='-K-W',pch=NA,cex=1.6,bty='n')



NUM <- c(NUMC3,NUMC1+126,NUMS3+126+72,NUMS1+126+72+108)
AGE <- c(AGEC3,AGEC1,AGES3,AGES1)
BIO <- c(BC3,BC1,BS3,BS1)
PS <- c(PSC3,PSC1,PSS3,PSS1)
aPAR <- c(APARC3,APARC1,APARS3,APARS1)


HEFF <- c(HC3[AGEC3==35],HC1[AGEC1==35],HS3[AGES3==35],HS1[AGES1==35])
DEFF1 <- c(DC3[AGEC3==35]/mean(DC3[AGEC3==35],na.rm=T),DC1[AGEC1==35]/mean(DC1[AGEC1==35],na.rm=T),
           DS3[AGES3==35]/mean(DC3[AGES3==35],na.rm=T),DS1[AGES1==35]/mean(DS1[AGES1==35],na.rm=T))
DEFF2 <- c(DC3[AGEC3==35]/max(DC3[AGEC3==35],na.rm=T),DC1[AGEC1==35]/max(DC1[AGEC1==35],na.rm=T),
           DS3[AGES3==35]/max(DC3[AGES3==35],na.rm=T),DS1[AGES1==35]/max(DS1[AGES1==35],na.rm=T))
CEFF1 <- c(CC3[AGEC3==35]/mean(CC3[AGEC3==35],na.rm=T),CC1[AGEC1==35]/mean(CC1[AGEC1==35],na.rm=T),
           CS3[AGES3==35]/mean(CC3[AGES3==35],na.rm=T),CS1[AGES1==35]/mean(CS1[AGES1==35],na.rm=T))
CEFF2 <- c(CC3[AGEC3==35]/max(CC3[AGEC3==35],na.rm=T),CC1[AGEC1==35]/max(CC1[AGEC1==35],na.rm=T),
           CS3[AGES3==35]/max(CC3[AGES3==35],na.rm=T),CS1[AGES1==35]/max(CS1[AGES1==35],na.rm=T))

KEFF <- c(rep('+K',126),rep('-K',72),rep('+K',108),rep('-K',72))
WEFF <- c(rep('+W',126+72),rep('-W',72+108))
BLOC <- c(rep(1,36),rep(2,54),rep(3,36),rep(1,36),rep(3,36),rep(1,36),rep(2,36),rep(3,36),rep(2,36),rep(3,36))

require(nlme)

ana11 <- lme(A~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana12 <- lme(A~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana13 <- lme(A~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana14 <- lme(A~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana15 <- lme(A~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana21 <- lme(B~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana22 <- lme(B~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana23 <- lme(B~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana24 <- lme(B~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana25 <- lme(B~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana31 <- lme(T0~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana32 <- lme(T0~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana33 <- lme(T0~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana34 <- lme(T0~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana35 <- lme(T0~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)


ana41 <- lme(A2~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana42 <- lme(A2~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana43 <- lme(A2~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana44 <- lme(A2~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana45 <- lme(A2~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana51 <- lme(B2~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana52 <- lme(B2~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana53 <- lme(B2~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana54 <- lme(B2~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana55 <- lme(B2~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)

ana61 <- lme(T02~KEFF*WEFF*DEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana62 <- lme(T02~KEFF*WEFF*DEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana63 <- lme(T02~KEFF*WEFF*CEFF1, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana64 <- lme(T02~KEFF*WEFF*CEFF2, random=~1+1|BLOC/WEFF,na.action=na.omit)
ana65 <- lme(T02~KEFF*WEFF*HEFF, random=~1+1|BLOC/WEFF,na.action=na.omit)

boxplot(A~WEFF*KEFF)


HEFF <- c(HC3[AGEC3==35],HC1[AGEC1==35],HS3[AGES3==35],HS1[AGES1==35])
CEFF1 <- c(CC3[AGEC3==35]/mean(CC3[AGEC3==35],na.rm=T),CC1[AGEC1==35]/mean(CC1[AGEC1==35],na.rm=T),
           CS3[AGES3==35]/mean(CC3[AGES3==35],na.rm=T),CS1[AGES1==35]/mean(CS1[AGES1==35],na.rm=T))

MATC32 <- data.frame(CC3[AGEC3==35]/mean(CC3[AGEC3==35],na.rm=T),1:126)
MATC32 <- MATC32[order(MATC32[,1]),]
MATC32 <- data.frame(MATC32,c(rep('s',25),rep('m',76),rep('b',25)))
MATC32 <- MATC32[order(MATC32[,2]),]
MATC12 <- data.frame(CC1[AGEC1==35]/mean(CC1[AGEC1==35],na.rm=T),1:72)
MATC12 <- MATC12[order(MATC12[,1]),]
MATC12 <- data.frame(MATC12,c(rep('s',14),rep('m',44),rep('b',14)))
MATC12 <- MATC12[order(MATC12[,2]),]
MATS32 <- data.frame(CS3[AGES3==35]/mean(CS3[AGES3==35],na.rm=T),1:108)
MATS32 <- MATS32[order(MATS32[,1]),]
MATS32 <- data.frame(MATS32,c(rep('s',21),rep('m',66),rep('b',21)))
MATS32 <- MATS32[order(MATS32[,2]),]
MATS12 <- data.frame(CS1[AGES1==35]/mean(CS1[AGES1==35],na.rm=T),1:72)
MATS12 <- MATS12[order(MATS12[,1]),]
MATS12 <- data.frame(MATS12,c(rep('s',14),rep('m',44),rep('b',14)))
MATS12 <- MATS12[order(MATS12[,2]),]

MATC3 <- data.frame(HC3[AGEC3==35],1:126)
MATC3 <- MATC3[order(MATC3[,1]),]
MATC3 <- data.frame(MATC3,c(rep('S',25),rep('M',76),rep('B',25)))
MATC3 <- MATC3[order(MATC3[,2]),]
MATC1 <- data.frame(HC1[AGEC1==35],1:72)
MATC1 <- MATC1[order(MATC1[,1]),]
MATC1 <- data.frame(MATC1,c(rep('S',14),rep('M',44),rep('B',14)))
MATC1 <- MATC1[order(MATC1[,2]),]
MATS3 <- data.frame(HS3[AGES3==35],1:108)
MATS3 <- MATS3[order(MATS3[,1]),]
MATS3 <- data.frame(MATS3,c(rep('S',21),rep('M',66),rep('B',21)))
MATS3 <- MATS3[order(MATS3[,2]),]
MATS1 <- data.frame(HS1[AGES1==35],1:72)
MATS1 <- MATS1[order(MATS1[,1]),]
MATS1 <- data.frame(MATS1,c(rep('S',14),rep('M',44),rep('B',14)))
MATS1 <- MATS1[order(MATS1[,2]),]

ICHC1 <- replace(NUMC1,c(NUMC1==MATC1[,2]),as.vector(MATC1[,3]))
ICHC3 <- replace(NUMC3,c(NUMC3==MATC3[,2]),as.vector(MATC3[,3]))
ICHS1 <- replace(NUMS1,c(NUMS1==MATS1[,2]),as.vector(MATS1[,3]))
ICHS3 <- replace(NUMS3,c(NUMS3==MATS3[,2]),as.vector(MATS3[,3]))

ICCC1 <- replace(NUMC1,c(NUMC1==MATC12[,2]),as.vector(MATC12[,3]))
ICCC3 <- replace(NUMC3,c(NUMC3==MATC32[,2]),as.vector(MATC32[,3]))
ICCS1 <- replace(NUMS1,c(NUMS1==MATS12[,2]),as.vector(MATS12[,3]))
ICCS3 <- replace(NUMS3,c(NUMS3==MATS32[,2]),as.vector(MATS32[,3]))


Y <-BC1[ICHC1=='S']*0.47/PSC1[ICHC1=='S']*1000
X <- AGEC1[ICHC1=='S']
WHSC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICHC1=='M']*0.47/PSC1[ICHC1=='M']*1000
X <- AGEC1[ICHC1=='M']
WHMC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICHC1=='B']*0.47/PSC1[ICHC1=='B']*1000
X <- AGEC1[ICHC1=='B']
WHBC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BC1[ICHC1=='S']/APARC1[ICHC1=='S']*1000
X <- AGEC1[ICHC1=='S']
LHSC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICHC1=='M']/APARC1[ICHC1=='M']*1000
X <- AGEC1[ICHC1=='M']
LHMC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICHC1=='B']/APARC1[ICHC1=='B']*1000
X <- AGEC1[ICHC1=='B']
LHBC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BC1[ICCC1=='s']*0.47/PSC1[ICCC1=='s']*1000
X <- AGEC1[ICCC1=='s']
WCSC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICCC1=='m']*0.47/PSC1[ICCC1=='m']*1000
X <- AGEC1[ICCC1=='m']
WCMC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICCC1=='b']*0.47/PSC1[ICCC1=='b']*1000
X <- AGEC1[ICCC1=='b']
WCBC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BC1[ICCC1=='s']/APARC1[ICCC1=='s']*1000
X <- AGEC1[ICCC1=='s']
LCSC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICCC1=='m']/APARC1[ICCC1=='m']*1000
X <- AGEC1[ICCC1=='m']
LCMC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC1[ICCC1=='b']/APARC1[ICCC1=='b']*1000
X <- AGEC1[ICCC1=='b']
LCBC1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

#C3
Y <-BC3[ICHC3=='S']*0.47/PSC3[ICHC3=='S']*1000
X <- AGEC3[ICHC3=='S']
WHSC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICHC3=='M']*0.47/PSC3[ICHC3=='M']*1000
X <- AGEC3[ICHC3=='M']
WHMC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICHC3=='B']*0.47/PSC3[ICHC3=='B']*1000
X <- AGEC3[ICHC3=='B']
WHBC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BC3[ICHC3=='S']/APARC3[ICHC3=='S']*1000
X <- AGEC3[ICHC3=='S']
LHSC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICHC3=='M']/APARC3[ICHC3=='M']*1000
X <- AGEC3[ICHC3=='M']
LHMC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICHC3=='B']/APARC3[ICHC3=='B']*1000
X <- AGEC3[ICHC3=='B']
LHBC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=1.3,b=0.5,T0=20))

Y <-BC3[ICCC3=='s']*0.47/PSC3[ICCC3=='s']*1000
X <- AGEC3[ICCC3=='s']
WCSC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICCC3=='m']*0.47/PSC3[ICCC3=='m']*1000
X <- AGEC3[ICCC3=='m']
WCMC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICCC3=='b']*0.47/PSC3[ICCC3=='b']*1000
X <- AGEC3[ICCC3=='b']
WCBC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BC3[ICCC3=='s']/APARC3[ICCC3=='s']*1000
X <- AGEC3[ICCC3=='s']
LCSC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICCC3=='m']/APARC3[ICCC3=='m']*1000
X <- AGEC3[ICCC3=='m']
LCMC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BC3[ICCC3=='b']/APARC3[ICCC3=='b']*1000
X <- AGEC3[ICCC3=='b']
LCBC3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=1.3,b=0.5,T0=20))

Y <-BS1[ICHS1=='S']*0.47/PSS1[ICHS1=='S']*1000
X <- AGES1[ICHS1=='S']
WHSS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICHS1=='M']*0.47/PSS1[ICHS1=='M']*1000
X <- AGES1[ICHS1=='M']
WHMS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICHS1=='B']*0.47/PSS1[ICHS1=='B']*1000
X <- AGES1[ICHS1=='B']
WHBS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS1[ICHS1=='S']/APARS1[ICHS1=='S']*1000
X <- AGES1[ICHS1=='S']
LHSS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICHS1=='M']/APARS1[ICHS1=='M']*1000
X <- AGES1[ICHS1=='M']
LHMS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICHS1=='B']/APARS1[ICHS1=='B']*1000
X <- AGES1[ICHS1=='B']
LHBS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS1[ICCS1=='s']*0.47/PSS1[ICCS1=='s']*1000
X <- AGES1[ICCS1=='s']
WCSS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICCS1=='m']*0.47/PSS1[ICCS1=='m']*1000
X <- AGES1[ICCS1=='m']
WCMS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICCS1=='b']*0.47/PSS1[ICCS1=='b']*1000
X <- AGES1[ICCS1=='b']
WCBS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS1[ICCS1=='s']/APARS1[ICCS1=='s']*1000
X <- AGES1[ICCS1=='s']
LCSS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICCS1=='m']/APARS1[ICCS1=='m']*1000
X <- AGES1[ICCS1=='m']
LCMS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS1[ICCS1=='b']/APARS1[ICCS1=='b']*1000
X <- AGES1[ICCS1=='b']
LCBS1 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS3[ICHS3=='S']*0.47/PSS3[ICHS3=='S']*1000
X <- AGES3[ICHS3=='S']
WHSS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICHS3=='M']*0.47/PSS3[ICHS3=='M']*1000
X <- AGES3[ICHS3=='M']
WHMS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICHS3=='B']*0.47/PSS3[ICHS3=='B']*1000
X <- AGES3[ICHS3=='B']
WHBS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS3[ICHS3=='S']/APARS3[ICHS3=='S']*1000
X <- AGES3[ICHS3=='S']
LHSS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICHS3=='M']/APARS3[ICHS3=='M']*1000
X <- AGES3[ICHS3=='M']
LHMS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICHS3=='B']/APARS3[ICHS3=='B']*1000
X <- AGES3[ICHS3=='B']
LHBS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS3[ICCS3=='s']*0.47/PSS3[ICCS3=='s']*1000
X <- AGES3[ICCS3=='s']
WCSS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICCS3=='m']*0.47/PSS3[ICCS3=='m']*1000
X <- AGES3[ICCS3=='m']
WCMS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICCS3=='b']*0.47/PSS3[ICCS3=='b']*1000
X <- AGES3[ICCS3=='b']
WCBS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))

Y <-BS3[ICCS3=='s']/APARS3[ICCS3=='s']*1000
X <- AGES3[ICCS3=='s']
LCSS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICCS3=='m']/APARS3[ICCS3=='m']*1000
X <- AGES3[ICCS3=='m']
LCMS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))
Y <-BS3[ICCS3=='b']/APARS3[ICCS3=='b']*1000
X <- AGES3[ICCS3=='b']
LCBS3 <- nls(Y~a/(1+exp(-b*(X-T0))), start=c(a=0.3,b=0.5,T0=20))



X <- seq(0,40,length.out=100)

par(mar=c(4,4,1,1))
par(mfrow=c(2,2))
par(xaxs='i',yaxs='i')
plot(AGEC3[ICHC3=='S'], BC3[ICHC3=='S']*0.47/PSC3[ICHC3=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC3[ICHC3=='M'], BC3[ICHC3=='M']*0.47/PSC3[ICHC3=='M']*1000,col=mycols[1],pch=16)
points(AGEC3[ICHC3=='M'], BC3[ICHC3=='M']*0.47/PSC3[ICHC3=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(WHSC3)$coef[1,1] / (1+ exp( -summary(WHSC3)$coef[2,1] * (X - summary(WHSC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=1)
Y <-  summary(WHMC3)$coef[1,1] / (1+ exp( -summary(WHMC3)$coef[2,1] * (X - summary(WHMC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=2)
Y <-  summary(WHBC3)$coef[1,1] / (1+ exp( -summary(WHBC3)$coef[2,1] * (X - summary(WHBC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=4,bty='n',cex=1.2)
legend('bottomright',legend='+K+W',pch=NA,cex=1.6,bty='n')

plot(AGEC1[ICHC1=='S'], BC1[ICHC1=='S']*0.47/PSC1[ICHC1=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC1[ICHC1=='M'], BC1[ICHC1=='M']*0.47/PSC1[ICHC1=='M']*1000,col=mycols[1],pch=16)
points(AGEC1[ICHC1=='M'], BC1[ICHC1=='M']*0.47/PSC1[ICHC1=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(WHSC1)$coef[1,1] / (1+ exp( -summary(WHSC1)$coef[2,1] * (X - summary(WHSC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=1)
Y <-  summary(WHMC1)$coef[1,1] / (1+ exp( -summary(WHMC1)$coef[2,1] * (X - summary(WHMC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=2)
Y <-  summary(WHBC1)$coef[1,1] / (1+ exp( -summary(WHBC1)$coef[2,1] * (X - summary(WHBC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=5,bty='n',cex=1.2)
legend('bottomright',legend='-K+W',pch=NA,cex=1.6,bty='n')
plot(AGES3[ICHS3=='S'], BS3[ICHS3=='S']*0.47/PSS3[ICHS3=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES3[ICHS3=='M'], BS3[ICHS3=='M']*0.47/PSS3[ICHS3=='M']*1000,col=mycols[1],pch=16)
points(AGES3[ICHS3=='M'], BS3[ICHS3=='M']*0.47/PSS3[ICHS3=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(WHSS3)$coef[1,1] / (1+ exp( -summary(WHSS3)$coef[2,1] * (X - summary(WHSS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=1)
Y <-  summary(WHMS3)$coef[1,1] / (1+ exp( -summary(WHMS3)$coef[2,1] * (X - summary(WHMS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=2)
Y <-  summary(WHBS3)$coef[1,1] / (1+ exp( -summary(WHBS3)$coef[2,1] * (X - summary(WHBS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=2,bty='n',cex=1.2)
legend('bottomright',legend='+K-W',pch=NA,cex=1.6,bty='n')
plot(AGES1[ICHS1=='S'], BS1[ICHS1=='S']*0.47/PSS1[ICHS1=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES1[ICHS1=='M'], BS1[ICHS1=='M']*0.47/PSS1[ICHS1=='M']*1000,col=mycols[1],pch=16)
points(AGES1[ICHS1=='M'], BS1[ICHS1=='M']*0.47/PSS1[ICHS1=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(WHSS1)$coef[1,1] / (1+ exp( -summary(WHSS1)$coef[2,1] * (X - summary(WHSS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=1)
Y <-  summary(WHMS1)$coef[1,1] / (1+ exp( -summary(WHMS1)$coef[2,1] * (X - summary(WHMS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=2)
Y <-  summary(WHBS1)$coef[1,1] / (1+ exp( -summary(WHBS1)$coef[2,1] * (X - summary(WHBS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col='salmon',bty='n',cex=1.2)
legend('bottomright',legend='-K-W',pch=NA,cex=1.6,bty='n')

par(mar=c(4,4,1,1))
par(mfrow=c(2,2))
par(xaxs='i',yaxs='i')
plot(AGEC3[ICCC3=='s'], BC3[ICCC3=='s']*0.47/PSC3[ICCC3=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC3[ICCC3=='m'], BC3[ICCC3=='m']*0.47/PSC3[ICCC3=='m']*1000,col=mycols[1],pch=16)
points(AGEC3[ICCC3=='m'], BC3[ICCC3=='m']*0.47/PSC3[ICCC3=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(WCSC3)$coef[1,1] / (1+ exp( -summary(WCSC3)$coef[2,1] * (X - summary(WCSC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=1)
Y <-  summary(WCMC3)$coef[1,1] / (1+ exp( -summary(WCMC3)$coef[2,1] * (X - summary(WCMC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=2)
Y <-  summary(WCBC3)$coef[1,1] / (1+ exp( -summary(WCBC3)$coef[2,1] * (X - summary(WCBC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=4,bty='n',cex=1.2)
legend('bottomright',legend='+K+W',pch=NA,cex=1.6,bty='n')

plot(AGEC1[ICCC1=='s'], BC1[ICCC1=='s']*0.47/PSC1[ICCC1=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGEC1[ICCC1=='m'], BC1[ICCC1=='m']*0.47/PSC1[ICCC1=='m']*1000,col=mycols[1],pch=16)
points(AGEC1[ICCC1=='m'], BC1[ICCC1=='m']*0.47/PSC1[ICCC1=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(WCSC1)$coef[1,1] / (1+ exp( -summary(WCSC1)$coef[2,1] * (X - summary(WCSC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=1)
Y <-  summary(WCMC1)$coef[1,1] / (1+ exp( -summary(WCMC1)$coef[2,1] * (X - summary(WCMC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=2)
Y <-  summary(WCBC1)$coef[1,1] / (1+ exp( -summary(WCBC1)$coef[2,1] * (X - summary(WCBC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=5,bty='n',cex=1.2)
legend('bottomright',legend='-K+W',pch=NA,cex=1.6,bty='n')
plot(AGES3[ICCS3=='s'], BS3[ICCS3=='s']*0.47/PSS3[ICCS3=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES3[ICCS3=='m'], BS3[ICCS3=='m']*0.47/PSS3[ICCS3=='m']*1000,col=mycols[1],pch=16)
points(AGES3[ICCS3=='m'], BS3[ICCS3=='m']*0.47/PSS3[ICCS3=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(WCSS3)$coef[1,1] / (1+ exp( -summary(WCSS3)$coef[2,1] * (X - summary(WCSS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=1)
Y <-  summary(WCMS3)$coef[1,1] / (1+ exp( -summary(WCMS3)$coef[2,1] * (X - summary(WCMS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=2)
Y <-  summary(WCBS3)$coef[1,1] / (1+ exp( -summary(WCBS3)$coef[2,1] * (X - summary(WCBS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=2,bty='n',cex=1.2)
legend('bottomright',legend='+K-W',pch=NA,cex=1.6,bty='n')
plot(AGES1[ICCS1=='s'], BS1[ICCS1=='s']*0.47/PSS1[ICCS1=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(Delta[wood]~('%GPP')),cex.lab=1.2,ylim=c(0,0.5),pch=16)
points(AGES1[ICCS1=='m'], BS1[ICCS1=='m']*0.47/PSS1[ICCS1=='m']*1000,col=mycols[1],pch=16)
points(AGES1[ICCS1=='m'], BS1[ICCS1=='m']*0.47/PSS1[ICCS1=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(WCSS1)$coef[1,1] / (1+ exp( -summary(WCSS1)$coef[2,1] * (X - summary(WCSS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=1)
Y <-  summary(WCMS1)$coef[1,1] / (1+ exp( -summary(WCMS1)$coef[2,1] * (X - summary(WCMS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=2)
Y <-  summary(WCBS1)$coef[1,1] / (1+ exp( -summary(WCBS1)$coef[2,1] * (X - summary(WCBS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col='salmon',bty='n',cex=1.2)
legend('bottomright',legend='-K-W',pch=NA,cex=1.6,bty='n')




# LUE

par(mar=c(4,4,1,1))
par(mfrow=c(2,2))
par(xaxs='i',yaxs='i')
plot(AGEC3[ICHC3=='S'], BC3[ICHC3=='S']/APARC3[ICHC3=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGEC3[ICHC3=='M'], BC3[ICHC3=='M']/APARC3[ICHC3=='M']*1000,col=mycols[1],pch=16)
points(AGEC3[ICHC3=='M'], BC3[ICHC3=='M']/APARC3[ICHC3=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(LHSC3)$coef[1,1] / (1+ exp( -summary(LHSC3)$coef[2,1] * (X - summary(LHSC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=1)
Y <-  summary(LHMC3)$coef[1,1] / (1+ exp( -summary(LHMC3)$coef[2,1] * (X - summary(LHMC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=2)
Y <-  summary(LHBC3)$coef[1,1] / (1+ exp( -summary(LHBC3)$coef[2,1] * (X - summary(LHBC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=4,bty='n',cex=1.2)
legend('bottomright',legend='+K+W',pch=NA,cex=1.6,bty='n')

plot(AGEC1[ICHC1=='S'], BC1[ICHC1=='S']/APARC1[ICHC1=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGEC1[ICHC1=='M'], BC1[ICHC1=='M']/APARC1[ICHC1=='M']*1000,col=mycols[1],pch=16)
points(AGEC1[ICHC1=='M'], BC1[ICHC1=='M']/APARC1[ICHC1=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(LHSC1)$coef[1,1] / (1+ exp( -summary(LHSC1)$coef[2,1] * (X - summary(LHSC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=1)
Y <-  summary(LHMC1)$coef[1,1] / (1+ exp( -summary(LHMC1)$coef[2,1] * (X - summary(LHMC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=2)
Y <-  summary(LHBC1)$coef[1,1] / (1+ exp( -summary(LHBC1)$coef[2,1] * (X - summary(LHBC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=5,bty='n',cex=1.2)
legend('bottomright',legend='-K+W',pch=NA,cex=1.6,bty='n')
plot(AGES3[ICHS3=='S'], BS3[ICHS3=='S']/APARS3[ICHS3=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGES3[ICHS3=='M'], BS3[ICHS3=='M']/APARS3[ICHS3=='M']*1000,col=mycols[1],pch=16)
points(AGES3[ICHS3=='M'], BS3[ICHS3=='M']/APARS3[ICHS3=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(LHSS3)$coef[1,1] / (1+ exp( -summary(LHSS3)$coef[2,1] * (X - summary(LHSS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=1)
Y <-  summary(LHMS3)$coef[1,1] / (1+ exp( -summary(LHMS3)$coef[2,1] * (X - summary(LHMS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=2)
Y <-  summary(LHBS3)$coef[1,1] / (1+ exp( -summary(LHBS3)$coef[2,1] * (X - summary(LHBS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col=2,bty='n',cex=1.2)
legend('bottomright',legend='+K-W',pch=NA,cex=1.6,bty='n')
plot(AGES1[ICHS1=='S'], BS1[ICHS1=='S']/APARS1[ICHS1=='S']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='Tree age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGES1[ICHS1=='M'], BS1[ICHS1=='M']/APARS1[ICHS1=='M']*1000,col=mycols[1],pch=16)
points(AGES1[ICHS1=='M'], BS1[ICHS1=='M']/APARS1[ICHS1=='M']*1000,col=mycols[1],pch=16)
Y <-  summary(LHSS1)$coef[1,1] / (1+ exp( -summary(LHSS1)$coef[2,1] * (X - summary(LHSS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=1)
Y <-  summary(LHMS1)$coef[1,1] / (1+ exp( -summary(LHMS1)$coef[2,1] * (X - summary(LHMS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=2)
Y <-  summary(LHBS1)$coef[1,1] / (1+ exp( -summary(LHBS1)$coef[2,1] * (X - summary(LHBS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=3)
legend('topleft',legend=c('Small tree','Medium tree','Big tree'),pch=NA,lty=1:3,col='salmon',bty='n',cex=1.2)
legend('bottomright',legend='-K-W',pch=NA,cex=1.6,bty='n')

par(mar=c(4,4,1,1))
par(mfrow=c(2,2))
par(xaxs='i',yaxs='i')
plot(AGEC3[ICCC3=='s'], BC3[ICCC3=='s']/APARC3[ICCC3=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGEC3[ICCC3=='m'], BC3[ICCC3=='m']/APARC3[ICCC3=='m']*1000,col=mycols[1],pch=16)
points(AGEC3[ICCC3=='m'], BC3[ICCC3=='m']/APARC3[ICCC3=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(LCSC3)$coef[1,1] / (1+ exp( -summary(LCSC3)$coef[2,1] * (X - summary(LCSC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=1)
Y <-  summary(LCMC3)$coef[1,1] / (1+ exp( -summary(LCMC3)$coef[2,1] * (X - summary(LCMC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=2)
Y <-  summary(LCBC3)$coef[1,1] / (1+ exp( -summary(LCBC3)$coef[2,1] * (X - summary(LCBC3)$coef[3,1])))
lines(X, Y,lwd=2,col=4,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=4,bty='n',cex=1.2)
legend('bottomright',legend='+K+W',pch=NA,cex=1.6,bty='n')

plot(AGEC1[ICCC1=='s'], BC1[ICCC1=='s']/APARC1[ICCC1=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGEC1[ICCC1=='m'], BC1[ICCC1=='m']/APARC1[ICCC1=='m']*1000,col=mycols[1],pch=16)
points(AGEC1[ICCC1=='m'], BC1[ICCC1=='m']/APARC1[ICCC1=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(LCSC1)$coef[1,1] / (1+ exp( -summary(LCSC1)$coef[2,1] * (X - summary(LCSC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=1)
Y <-  summary(LCMC1)$coef[1,1] / (1+ exp( -summary(LCMC1)$coef[2,1] * (X - summary(LCMC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=2)
Y <-  summary(LCBC1)$coef[1,1] / (1+ exp( -summary(LCBC1)$coef[2,1] * (X - summary(LCBC1)$coef[3,1])))
lines(X, Y,lwd=2,col=5,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=5,bty='n',cex=1.2)
legend('bottomright',legend='-K+W',pch=NA,cex=1.6,bty='n')
plot(AGES3[ICCS3=='s'], BS3[ICCS3=='s']/APARS3[ICCS3=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGES3[ICCS3=='m'], BS3[ICCS3=='m']/APARS3[ICCS3=='m']*1000,col=mycols[1],pch=16)
points(AGES3[ICCS3=='m'], BS3[ICCS3=='m']/APARS3[ICCS3=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(LCSS3)$coef[1,1] / (1+ exp( -summary(LCSS3)$coef[2,1] * (X - summary(LCSS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=1)
Y <-  summary(LCMS3)$coef[1,1] / (1+ exp( -summary(LCMS3)$coef[2,1] * (X - summary(LCMS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=2)
Y <-  summary(LCBS3)$coef[1,1] / (1+ exp( -summary(LCBS3)$coef[2,1] * (X - summary(LCBS3)$coef[3,1])))
lines(X, Y,lwd=2,col=2,lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col=2,bty='n',cex=1.2)
legend('bottomright',legend='+K-W',pch=NA,cex=1.6,bty='n')
plot(AGES1[ICCS1=='s'], BS1[ICCS1=='s']/APARS1[ICCS1=='s']*1000,xlim=c(0,36),col=mycols[1],mgp=c(2.5,1,0),
     xlab='crown age (months)',ylab=expression(LUEp~('gDM/MJ')),cex.lab=1.2,ylim=c(0,1.5),pch=16)
points(AGES1[ICCS1=='m'], BS1[ICCS1=='m']/APARS1[ICCS1=='m']*1000,col=mycols[1],pch=16)
points(AGES1[ICCS1=='m'], BS1[ICCS1=='m']/APARS1[ICCS1=='m']*1000,col=mycols[1],pch=16)
Y <-  summary(LCSS1)$coef[1,1] / (1+ exp( -summary(LCSS1)$coef[2,1] * (X - summary(LCSS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=1)
Y <-  summary(LCMS1)$coef[1,1] / (1+ exp( -summary(LCMS1)$coef[2,1] * (X - summary(LCMS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=2)
Y <-  summary(LCBS1)$coef[1,1] / (1+ exp( -summary(LCBS1)$coef[2,1] * (X - summary(LCBS1)$coef[3,1])))
lines(X, Y,lwd=2,col='salmon',lty=3)
legend('topleft',legend=c('Small crown','Medium crown','Big crown'),pch=NA,lty=1:3,col='salmon',bty='n',cex=1.2)
legend('bottomright',legend='-K-W',pch=NA,cex=1.6,bty='n')













# graph
HEFF <- c(HC3[AGEC3==35],HC1[AGEC1==35],HS3[AGES3==35],HS1[AGES1==35])

plot(HEFF[1:126]/mean(HEFF[1:126]),A[1:126],col=4,xlim=c(0.5,1.5),ylim=c(0,1))
points(HEFF[127:198]/mean(HEFF[127:198]),A[127:198],col=5)
points(HEFF[199:306]/mean(HEFF[199:306]),A[199:306],col=2)
points(HEFF[307:378]/mean(HEFF[307:378]),A[307:378],col='salmon')

plot(HEFF[1:126]/mean(HEFF[1:126]),B[1:126],col=4,xlim=c(0.5,1.5),ylim=c(0,1))
points(HEFF[127:198]/mean(HEFF[127:198]),B[127:198],col=5)
points(HEFF[199:306]/mean(HEFF[199:306]),B[199:306],col=2)
points(HEFF[307:378]/mean(HEFF[307:378]),B[307:378],col='salmon')

plot(HEFF[1:126]/mean(HEFF[1:126]),T0[1:126],col=4,xlim=c(0.5,1.5),ylim=c(0,40))
points(HEFF[127:198]/mean(HEFF[127:198]),T0[127:198],col=5)
points(HEFF[199:306]/mean(HEFF[199:306]),T0[199:306],col=2)
points(HEFF[307:378]/mean(HEFF[307:378]),T0[307:378],col='salmon')

require(gss)


THC3 <- seq(min(HEFF[1:126]),max(HEFF[1:126]),length.out=126)
newC3 <- data.frame(THC3)
GC3 <- lm(T0[1:126]~HEFF[1:126])
estGC3 <- predict(GC3,newC3,se.fit=T)

TRAT <- c(rep('C3',126),rep('C1',72),rep('S3',108),rep('S1',72))
FAC3 <- lm(A[TRAT=='C3']~HEFF[TRAT=='C3'])
FBC3 <- lm(B[TRAT=='C3']~HEFF[TRAT=='C3'])
FTC3 <- lm(T0[TRAT=='C3']~HEFF[TRAT=='C3'])
FAC1 <- lm(A[TRAT=='C1']~HEFF[TRAT=='C1'])
FBC1 <- lm(B[TRAT=='C1']~HEFF[TRAT=='C1'])
FTC1 <- lm(T0[TRAT=='C1']~HEFF[TRAT=='C1'])
FAS3 <- lm(A[TRAT=='S3']~HEFF[TRAT=='S3'])
FBS3 <- lm(B[TRAT=='S3']~HEFF[TRAT=='S3'])
FTS3 <- lm(T0[TRAT=='S3']~HEFF[TRAT=='S3'])
FAS1 <- lm(A[TRAT=='S1']~HEFF[TRAT=='S1'])
FBS1 <- lm(B[TRAT=='S1']~HEFF[TRAT=='S1'])
FTS1 <- lm(T0[TRAT=='S1']~HEFF[TRAT=='S1'])

mycols <- adjustcolor(palette(), alpha.f = 0.6)

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(HEFF[TRAT=='C3'],T0[TRAT=='C3'], col=4,xlim=c(0,20),ylim=c(0,40),mgp=c(2.5,1,0),
      xlab='Tree height at 35 months (m)',ylab='T0',cex.lab=1.2)
points(HEFF[127:198],T0[127:198],col=5)
points(HEFF[199:306],T0[199:306],col=2)
points(HEFF[307:378],T0[307:378],col='salmon')


