source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
# autre mani�re de voire le LUE et WUE
require(gss)


#figure plot

DAY <- 1:length(PSC3)
GAC3 <- ssanova(PSC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGAC3 <- predict(GAC3,newC3,se=T)
GAC1 <- ssanova(PSC1~DAY)
newC1 <- data.frame(DAY,alpha=1.1)
estGAC1 <- predict(GAC1,newC1,se=T)
GAS1 <- ssanova(PSS1~DAY,alpha=1.1)
newS1 <- data.frame(DAY)
estGAS1 <- predict(GAS1,newS1,se=T)
GAS3 <- ssanova(PSS3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGAS3 <- predict(GAS3,newS3,se=T)

MATA <- matrix(c(sum(PSC3[1:167],na.rm=T)/167,sum(PSC3[168:534],na.rm=T)/364,sum(PSC3[535:899],na.rm=T)/365,
                sum(PSC1[1:167],na.rm=T)/167,sum(PSC1[168:534],na.rm=T)/364,sum(PSC1[535:899],na.rm=T)/365,
                sum(PSS3[1:167],na.rm=T)/167,sum(PSS3[168:534],na.rm=T)/364,sum(PSS3[535:899],na.rm=T)/365,
                sum(PSS1[1:167],na.rm=T)/167,sum(PSS1[168:534],na.rm=T)/364,sum(PSS1[535:899],na.rm=T)/365),
                nrow=4,byrow=T)

DAY <- 1:length(aPARC3)
GaPARC3 <- ssanova(aPARC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGaPARC3 <- predict(GaPARC3,newC3,se=T)
GaPARC1 <- ssanova(aPARC1~DAY)
newC1 <- data.frame(DAY,alpha=1.1)
estGaPARC1 <- predict(GaPARC1,newC1,se=T)
GaPARS1 <- ssanova(aPARS1~DAY,alpha=1.1)
newS1 <- data.frame(DAY)
estGaPARS1 <- predict(GaPARS1,newS1,se=T)
GaPARS3 <- ssanova(aPARS3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGaPARS3 <- predict(GaPARS3,newS3,se=T)
MATAPAR <- matrix(c(sum(aPARC3[1:167],na.rm=T)/167,sum(aPARC3[168:534],na.rm=T)/364,sum(aPARC3[535:899],na.rm=T)/365,
                sum(aPARC1[1:167],na.rm=T)/167,sum(aPARC1[168:534],na.rm=T)/364,sum(aPARC1[535:899],na.rm=T)/365,
                sum(aPARS3[1:167],na.rm=T)/167,sum(aPARS3[168:534],na.rm=T)/364,sum(aPARS3[535:899],na.rm=T)/365,
                sum(aPARS1[1:167],na.rm=T)/167,sum(aPARS1[168:534],na.rm=T)/364,sum(aPARS1[535:899],na.rm=T)/365),
                nrow=4,byrow=T)

DAY <- 1:length(PSS3)
GLUEC3 <- ssanova(PSC3/aPARC3~DAY,alpha=1.1)
newC3 <- data.frame(DAY)
estGLUEC3 <- predict(GLUEC3,newC3,se=T)
GLUEC1 <- ssanova(PSC1/aPARC1~DAY,alpha=1.1)
newC1 <- data.frame(DAY)
estGLUEC1 <- predict(GLUEC1,newC1,se=T)
GLUES3 <- ssanova(PSS3/aPARS3~DAY,alpha=1.1)
newS3 <- data.frame(DAY)
estGLUES3 <- predict(GLUES3,newS3,se=T)
GLUES1 <- ssanova(PSS1/aPARS1~DAY,alpha=1.1)
newS1 <- data.frame(DAY)
estGLUES1 <- predict(GLUES1,newS1,se=T)

MATLUE <- matrix(c(sum(PSC3[1:168],na.rm=T)/sum(aPARC3[1:168],na.rm=T),sum(PSC3[169:534],na.rm=T)/sum(aPARC3[169:534],na.rm=T),sum(PSC3[535:899],na.rm=T)/sum(aPARC3[535:899],na.rm=T),
                   sum(PSC1[1:168],na.rm=T)/sum(aPARC1[1:168],na.rm=T),sum(PSC1[169:534],na.rm=T)/sum(aPARC1[169:534],na.rm=T),sum(PSC1[535:899],na.rm=T)/sum(aPARC1[535:899],na.rm=T),
                   sum(PSS3[1:168],na.rm=T)/sum(aPARS3[1:168],na.rm=T),sum(PSS3[169:534],na.rm=T)/sum(aPARS3[169:534],na.rm=T),sum(PSS3[535:899],na.rm=T)/sum(aPARS3[535:899],na.rm=T),
                   sum(PSS1[1:168],na.rm=T)/sum(aPARS1[1:168],na.rm=T),sum(PSS1[169:534],na.rm=T)/sum(aPARS1[169:534],na.rm=T),sum(PSS1[535:899],na.rm=T)/sum(aPARS1[535:899],na.rm=T)),
                   nrow=4,byrow=T)

# deuxi�me essai en collant

mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols2 <- adjustcolor(palette(), alpha.f = 0.1)
mycols3 <- adjustcolor(palette(), alpha.f = 0.3)
mysalmon <- adjustcolor('salmon',alpha.f = 0.6)
YEA <- c('0.5-1y','1-2y','2-3y')
ARG <-c('+K +W','- K +W','+K - W','- K - W')

par(mar=c(4,4.1,1,16))
par(yaxs='i',xaxs='i')
plot(aPARC3+24,cex.lab=1.2,col=mycols3[1],pch=1,mgp=c(2.5,1,0),ylim=c(0,12*3),axes=F,ann=F,cex=0.6)
points(aPARC1+24,col=mycols3[1],cex=0.6)
points(aPARS3+24,col=mycols3[1],cex=0.6)
points(aPARS1+24,col=mycols3[1],cex=0.6)
lines(DAY,estGaPARC3$fit+24,col=4,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGaPARC3$fit+24,rev(estGaPARC3$fit+24+estGaPARC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGaPARC3$fit+24,rev(estGaPARC3$fit+24-estGaPARC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGaPARC1$fit+24,col=5,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGaPARC1$fit+24,rev(estGaPARC1$fit+24+estGaPARC1$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGaPARC1$fit+24,rev(estGaPARC1$fit+24-estGaPARC1$se.fit)),col=mycols[5],border=NA)
lines(DAY,estGaPARS3$fit+24,col=2,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGaPARS3$fit+24,rev(estGaPARS3$fit+24+estGaPARS3$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGaPARS3$fit+24,rev(estGaPARS3$fit+24-estGaPARS3$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGaPARS1$fit+24,col='salmon',lwd=1)
polygon(c(DAY,rev(DAY)),c(estGaPARS1$fit+24,rev(estGaPARS1$fit+24+estGaPARS1$se.fit)),col=mysalmon,border=NA)
polygon(c(DAY,rev(DAY)),c(estGaPARS1$fit+24,rev(estGaPARS1$fit+24-estGaPARS1$se.fit)),col=mysalmon,border=NA)

par(new=T)
plot(PSC3+25,cex.lab=1.2,col=mycols3[1],pch=1,mgp=c(2.5,1,0),ylim=c(0,25*3),axes=F,ann=F,cex=0.6)
points(PSC1+25,col=mycols3[1],cex=0.6)
points(PSS3+25,col=mycols3[1],cex=0.6)
points(PSS1+25,col=mycols3[1],cex=0.6)
lines(DAY,estGAC3$fit+25,col=4,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGAC3$fit+25,rev(estGAC3$fit+25+estGAC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGAC3$fit+25,rev(estGAC3$fit+25-estGAC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGAC1$fit+25,col=5,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGAC1$fit+25,rev(estGAC1$fit+25+estGAC1$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGAC1$fit+25,rev(estGAC1$fit+25-estGAC1$se.fit)),col=mycols[5],border=NA)
lines(DAY,estGAS3$fit+25,col=2,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGAS3$fit+25,rev(estGAS3$fit+25+estGAS3$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGAS3$fit+25,rev(estGAS3$fit+25-estGAS3$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGAS1$fit+25,col='salmon',lwd=1)
polygon(c(DAY,rev(DAY)),c(estGAS1$fit+25,rev(estGAS1$fit+25+estGAS1$se.fit)),col=mysalmon,border=NA)
polygon(c(DAY,rev(DAY)),c(estGAS1$fit+25,rev(estGAS1$fit+25-estGAS1$se.fit)),col=mysalmon,border=NA)

par(new=T)
plot(PSC3/aPARC3,xlab='Day from Jan 2011 to Jun 2013',ylab=expression('LUE'~(gC~MJ^-1)),
      cex.lab=1.2,col=mycols3[1],pch=1,mgp=c(2.5,1,0),ylim=c(0,4*3),axes=F,ann=F,cex=0.6)
points(PSC1/aPARC1,col=mycols3[1],cex=0.6)
points(PSS3/aPARS3,col=mycols3[1],cex=0.6)
points(PSS1/aPARS1,col=mycols3[1],cex=0.6)
lines(DAY,estGLUEC3$fit,col=4,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGLUEC3$fit,rev(estGLUEC3$fit+estGLUEC3$se.fit)),col=mycols[4],border=NA)
polygon(c(DAY,rev(DAY)),c(estGLUEC3$fit,rev(estGLUEC3$fit-estGLUEC3$se.fit)),col=mycols[4],border=NA)
lines(DAY,estGLUEC1$fit,col=5,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGLUEC1$fit,rev(estGLUEC1$fit+estGLUEC1$se.fit)),col=mycols[5],border=NA)
polygon(c(DAY,rev(DAY)),c(estGLUEC1$fit,rev(estGLUEC1$fit-estGLUEC1$se.fit)),col=mycols[5],border=NA)
lines(DAY,estGLUES3$fit,col=2,lwd=1)
polygon(c(DAY,rev(DAY)),c(estGLUES3$fit,rev(estGLUES3$fit+estGLUES3$se.fit)),col=mycols[2],border=NA)
polygon(c(DAY,rev(DAY)),c(estGLUES3$fit,rev(estGLUES3$fit-estGLUES3$se.fit)),col=mycols[2],border=NA)
lines(DAY,estGLUES1$fit,col='salmon',lwd=1)
polygon(c(DAY,rev(DAY)),c(estGLUES1$fit,rev(estGLUES1$fit+estGLUES1$se.fit)),col=mysalmon,border=NA)
polygon(c(DAY,rev(DAY)),c(estGLUES1$fit,rev(estGLUES1$fit-estGLUES1$se.fit)),col=mysalmon,border=NA)


rect(182,0,273,40,border=NA,col=mycols2[1])
rect(548,0,639,40,border=NA,col=mycols2[1])
legend(x=0,y=8,legend=c('+K +W','- K +W','+K - W','- K - W'),pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.8)
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
abline(h=4)
abline(h=8)
text(260,7.5,'2011',font=4)
text(550,7.5,'2012',font=4)
text(850,7.5,'2013',font=4)
box()

axis(1,at=c(8,208,408,608,808),labels=c(0,200,400,600,800)+200,cex.axis=0.8)
axis(2, at=c(0,1,2,3,  4,4.8,5.6,6.4,7.2,  8:12),
    labels=c(0,1,2,3,  0,5,10,15,20,      0,3,6,9,12),cex.axis=0.8)

mtext('Stand age (day)',side=1,line=2.5,cex=1.1)
mtext(expression('LUE'~(gC~MJ^-1)~'        '~'GPP'~~(gC~m^-2~d^-1)~'            '~aPAR~(MJ~m^-2~d^-1)),side=2,line=2.5,cex=1.1)

par(new=T)
par(mar=c(4,30,1,1))
par(yaxs='i')
barplot(MATAPAR,col=0,beside=T,border=0,names.arg=YEA,mgp=c(2.5,1,0),
        legend.text=F,space=c(0.2,0,0,0,0.4,0,0,0,0.4,0,0,0),
        ylab=NA,xlab=NA,cex.lab=1.2,cex.names=1,ylim=c(0,10*3),axes=F)
rect(0,20,1.2,MATAPAR[1,1]+20,col=mycols[4])
rect(1.2,20,2.2,MATAPAR[2,1]+20,col=mycols[5])
rect(2.2,20,3.2,MATAPAR[3,1]+20,col=mycols[2])
rect(3.2,20,4.2,MATAPAR[4,1]+20,col=mysalmon)
rect(4.6,20,5.6,MATAPAR[1,2]+20,col=mycols[4])
rect(5.6,20,6.6,MATAPAR[2,2]+20,col=mycols[5])
rect(6.6,20,7.6,MATAPAR[3,2]+20,col=mycols[2])
rect(7.6,20,8.6,MATAPAR[4,2]+20,col=mysalmon)
rect(9,20,10,MATAPAR[1,3]+20,col=mycols[4])
rect(10,20,11,MATAPAR[2,3]+20,col=mycols[5])
rect(11,20,12,MATAPAR[3,3]+20,col=mycols[2])
rect(12,20,13,MATAPAR[4,3]+20,col=mysalmon)
abline(h=20)


par(new=T)
barplot(MATA+15,col=0,beside=T,border=0,names.arg=NA,mgp=c(2.5,1,0),
        legend.text=F,space=c(0.2,0,0,0,0.4,0,0,0,0.4,0,0,0),
        ylab=NA,xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,15*3),axes=F)
rect(0,15,1.2,MATA[1,1]+15,col=mycols[4])
rect(1.2,15,2.2,MATA[2,1]+15,col=mycols[5])
rect(2.2,15,3.2,MATA[3,1]+15,col=mycols[2])
rect(3.2,15,4.2,MATA[4,1]+15,col=mysalmon)
rect(4.6,15,5.6,MATA[1,2]+15,col=mycols[4])
rect(5.6,15,6.6,MATA[2,2]+15,col=mycols[5])
rect(6.6,15,7.6,MATA[3,2]+15,col=mycols[2])
rect(7.6,15,8.6,MATA[4,2]+15,col=mysalmon)
rect(9,15,10,MATA[1,3]+15,col=mycols[4])
rect(10,15,11,MATA[2,3]+15,col=mycols[5])
rect(11,15,12,MATA[3,3]+15,col=mycols[2])
rect(12,15,13,MATA[4,3]+15,col=mysalmon)

par(new=T)
barplot(MATLUE,col=0,beside=T,border=0,names.arg=NA,mgp=c(2.5,1,0),
        legend.text=F,space=c(0.2,0,0,0,0.4,0,0,0,0.4,0,0,0),
        ylab=NA,xlab=NA,cex.lab=1.2,cex.names=1.2,ylim=c(0,2.5*3),args.legend=c(x='topleft',bg='white',box.col=0))
rect(0,0,1.2,MATLUE[1,1]+0,col=mycols[4])
rect(1.2,0,2.2,MATLUE[2,1]+0,col=mycols[5])
rect(2.2,0,3.2,MATLUE[3,1]+0,col=mycols[2])
rect(3.2,0,4.2,MATLUE[4,1]+0,col=mysalmon)
rect(4.6,0,5.6,MATLUE[1,2]+0,col=mycols[4])
rect(5.6,0,6.6,MATLUE[2,2]+0,col=mycols[5])
rect(6.6,0,7.6,MATLUE[3,2]+0,col=mycols[2])
rect(7.6,0,8.6,MATLUE[4,2]+0,col=mysalmon)
rect(9,0,10,MATLUE[1,3]+0,col=mycols[4])
rect(10,0,11,MATLUE[2,3]+0,col=mycols[5])
rect(11,0,12,MATLUE[3,3]+0,col=mycols[2])
rect(12,0,13,MATLUE[4,3]+0,col=mysalmon)

legend(x=0,y=5,legend=ARG,pch=15,col=c(mycols[4],mycols[5],mycols[2],mysalmon),bg='white',box.col=0,cex=0.8)
abline(h=2.5)
abline(h=5)
axis(2, at=c(0,0.5,1,1.5,2,  2.5,3,3.5,4,4.5,   5,5.5,6,6.5,7,7.5),
    labels=c(0,0.5,1,1.5,2,  0,3,6,9,12,       0,2,4,6,8,10       ),cex.axis=0.8)
box()
mtext(expression('LUE'~(gC~MJ^-1)~'        '~'GPP'~~(gC~m^-2~d^-1)~'            '~aPAR~(MJ~m^-2~d^-1)),side=2,line=2.5,cex=1.1)







# figure en prenant en compta taille rabre

ftLUEC11 <- lm(BC1_A1/APARC1_A1~HC1_A1)
ftLUEC12 <- lm(BC1_A2/APARC1_A2~HC1_A2)
ftLUEC13 <- lm(BC1_A3/APARC1_A3~HC1_A3)
ftLUEC31 <- lm(BC3_A1/APARC3_A1~HC3_A1)
ftLUEC32 <- lm(BC3_A2/APARC3_A2~HC3_A2)
ftLUEC33 <- lm(BC3_A3/APARC3_A3~HC3_A3)
ftLUES31 <- lm(BS3_A1/APARS3_A1~HS3_A1)
ftLUES32 <- lm(BS3_A2/APARS3_A2~HS3_A2)
ftLUES33 <- lm(BS3_A3/APARS3_A3~HS3_A3)
ftLUES11 <- lm(BS1_A1/APARS1_A1~HS1_A1)
ftLUES12 <- lm(BS1_A2/APARS1_A2~HS1_A2)
ftLUES13 <- lm(BS1_A3/APARS1_A3~HS1_A3)



ftBC11 <- lm(BC1_A1~HC1_A1)
ftBC12 <- lm(BC1_A2~HC1_A2)
ftBC13 <- lm(BC1_A3~HC1_A3)
ftBC31 <- lm(BC3_A1~HC3_A1)
ftBC32 <- lm(BC3_A2~HC3_A2)
ftBC33 <- lm(BC3_A3~HC3_A3)
ftBS31 <- lm(BS3_A1~HS3_A1)
ftBS32 <- lm(BS3_A2~HS3_A2)
ftBS33 <- lm(BS3_A3~HS3_A3)
ftBS11 <- lm(BS1_A1~HS1_A1)
ftBS12 <- lm(BS1_A2~HS1_A2)
ftBS13 <- lm(BS1_A3~HS1_A3)

ftDBC11 <- lm(BC1_A1*0.47/(PSC1_A1/1000)~HC1_A1)
ftDBC12 <- lm(BC1_A2*0.47/(PSC1_A2/1000)~HC1_A2)
ftDBC13 <- lm(BC1_A3*0.47/(PSC1_A3/1000)~HC1_A3)
ftDBC31 <- lm(BC3_A1*0.47/(PSC3_A1/1000)~HC3_A1)
ftDBC32 <- lm(BC3_A2*0.47/(PSC3_A2/1000)~HC3_A2)
ftDBC33 <- lm(BC3_A3*0.47/(PSC3_A3/1000)~HC3_A3)
ftDBS11 <- lm(BS1_A1*0.47/(PSS1_A1/1000)~HS1_A1)
ftDBS12 <- lm(BS1_A2*0.47/(PSS1_A2/1000)~HS1_A2)
ftDBS13 <- lm(BS1_A3*0.47/(PSS1_A3/1000)~HS1_A3)
ftDBS31 <- lm(BS3_A1*0.47/(PSS3_A1/1000)~HS3_A1)
ftDBS32 <- lm(BS3_A2*0.47/(PSS3_A2/1000)~HS3_A2)
ftDBS33 <- lm(BS3_A3*0.47/(PSS3_A3/1000)~HS3_A3)

ftBC31 <- lm(BC3_A1~HC3_A1)
ftBC32 <- lm(BC3_A2~HC3_A2)
ftBC33 <- lm(BC3_A3~HC3_A3)
ftBS31 <- lm(BS3_A1~HS3_A1)
ftBS32 <- lm(BS3_A2~HS3_A2)
ftBS33 <- lm(BS3_A3~HS3_A3)
ftBS11 <- lm(BS1_A1~HS1_A1)
ftBS12 <- lm(BS1_A2~HS1_A2)
ftBS13 <- lm(BS1_A3~HS1_A3)

ftAPARC11 <- lm(APARC1_A1~HC1_A1)
ftAPARC12 <- lm(APARC1_A2~HC1_A2)
ftAPARC13 <- lm(APARC1_A3~HC1_A3)
ftAPARC31 <- lm(APARC3_A1~HC3_A1)
ftAPARC32 <- lm(APARC3_A2~HC3_A2)
ftAPARC33 <- lm(APARC3_A3~HC3_A3)
ftAPARS31 <- lm(APARS3_A1~HS3_A1)
ftAPARS32 <- lm(APARS3_A2~HS3_A2)
ftAPARS33 <- lm(APARS3_A3~HS3_A3)
ftAPARS11 <- lm(APARS1_A1~HS1_A1)
ftAPARS12 <- lm(APARS1_A2~HS1_A2)
ftAPARS13 <- lm(APARS1_A3~HS1_A3)
ftPSC11 <- lm(PSC1_A1~HC1_A1)
ftPSC12 <- lm(PSC1_A2~HC1_A2)
ftPSC13 <- lm(PSC1_A3~HC1_A3)
ftPSC31 <- lm(PSC3_A1~HC3_A1)
ftPSC32 <- lm(PSC3_A2~HC3_A2)
ftPSC33 <- lm(PSC3_A3~HC3_A3)
ftPSS31 <- lm(PSS3_A1~HS3_A1)
ftPSS32 <- lm(PSS3_A2~HS3_A2)
ftPSS33 <- lm(PSS3_A3~HS3_A3)
ftPSS11 <- lm(PSS1_A1~HS1_A1)
ftPSS12 <- lm(PSS1_A2~HS1_A2)
ftPSS13 <- lm(PSS1_A3~HS1_A3)


mycols <- adjustcolor(palette(), alpha.f = 0.6)
mycols2 <- adjustcolor(palette(), alpha.f = 0.1)
mycols3 <- adjustcolor(palette(), alpha.f = 0.3)
mysalmon <- adjustcolor('salmon',alpha.f = 0.6)

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')

plot(HC3_A1,BC3_A1+32*3,col=mycols[4],xlab='Tree height (m)',ylab=expression('B'~(GJ~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,32*4),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1+32*3,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1+32*3,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2+32*3,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2+32*3,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2+32*3,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3+32*3,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3+32*3,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3+32*3,col=mysalmon,pch=16,cex=0.8)
grid(nx=15,ny=16)                                                           
legend(x=10,y=128,legend=c('+K +W','- K +W','+K - W','- K - W'),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=1,cex=0.8,xjus=1)
legend(x=0,y=120,legend=c(paste('r� = ',round(summary(ftBC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftBC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=120,legend=c(paste('r� = ',round(summary(ftBC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=120,legend=c(paste('r� = ',round(summary(ftBC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftBC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bty='n',cex=0.6)#bg='white',box.col=0,cex=0.6)
legend(x=0,y=128,legend='1 year-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=10,y=128,legend='2 years-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=20,y=128,legend='3 years-old',pch=NA,cex=1,bg='white',box.col=0)
box()

par(new=T)
plot(HC3_A1,APARC3_A1/1000+32*2,col=mycols[4],xlab='Tree height (m)',ylab=expression('APAR'~(GJ~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,32*4),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,APARS3_A1/1000+32*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,APARC1_A1/1000+32*2,col=mycols[4],pch=16,cex=0.8)
points(HS1_A1,APARS1_A1/1000+32*2,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,APARC3_A2/1000+32*2,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,APARS3_A2/1000+32*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,APARC1_A2/1000+32*2,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,APARS1_A2/1000+32*2,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,APARC3_A3/1000+32*2,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,APARS3_A3/1000+32*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,APARC1_A3/1000+32*2,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,APARS1_A3/1000+32*2,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=96,legend=c(paste('r� = ',round(summary(ftAPARC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=96,legend=c(paste('r� = ',round(summary(ftAPARC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=96,legend=c(paste('r� = ',round(summary(ftAPARC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),cex=0.6,bg='white',box.col=0)
box()

par(new=T)
plot(HC3_A1,PSC3_A1/1000+40,col=mycols[4],xlab='Tree height (m)',ylab=expression('GPP'~(gC~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,40*4),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,PSS3_A1/1000+40,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,PSC1_A1/1000+40,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,PSS1_A1/1000+40,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,PSC3_A2/1000+40,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,PSS3_A2/1000+40,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,PSC1_A2/1000+40,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,PSS1_A2/1000+40,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,PSC3_A3/1000+40,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,PSS3_A3/1000+40,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,PSC1_A3/1000+40,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,PSS1_A3/1000+40,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=80,legend=c(paste('r� = ',round(summary(ftPSC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=80,legend=c(paste('r� = ',round(summary(ftPSC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=40,y=80,legend=c(paste('r� = ',round(summary(ftPSC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
box()



par(new=T)
plot(HC3_A1,BC3_A1/APARC3_A1*1000,col=mycols[4],xlab='Tree height (m)',ylab='LUE (gDM/MJ)',cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,1.6*4),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1/APARS3_A1*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1/APARC1_A1*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1/APARS1_A1*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2/APARC3_A2*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2/APARS3_A2*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2/APARC1_A2*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2/APARS1_A2*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3/APARC3_A3*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3/APARS3_A3*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3/APARC1_A3*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3/APARS1_A3*1000,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES31)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES32)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES33)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
abline(h=1.6)
abline(h=3.2)
abline(h=4.8)
abline(v=10)
abline(v=20)
axis(1,at=c(0:15)*2,labels=c(0,2,4,6,8,  4,6,8,10,12,   8,10,12,14,16,18),cex.axis=0.8)
axis(2,at=c(0,0.4,0.8,1.2,       1.6,2,2.4,2.8,     3.2,3.6,4,4.4,     4.8,5.2,5.6,6,6.4),
      labels=c(0,0.4,0.8,1.2,    0,10,20,30,     0,8,16,24, 0,8,16,24,32),cex.axis=0.8)

mtext(expression('LUE'~(gDM~MJ^-1)~'   '~'GPP'~~(kgC~tree^-1~y^-1)~'   '~aPAR~(GJ~tree^-1~y^-1)~'   '~delta[wood]~(kg~tree^-1~y^-1)),side=2,line=2.5,cex=0.9)
mtext('Tree height (m)',side=1,line=2.5,cex=1)


# graph supp

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(HC3_A1,BC3_A1*0.47/(PSC3_A1/1000),col=mycols[4],xlab='Tree height (m)',ylab=expression(DELTA['wood']~('%GPP')),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,0.5),mgp=c(2.5,1,0),axes=F,ann=T,pch=16,cex=0.8)
points(HS3_A1,BS3_A1*0.47/(PSS3_A1/1000),col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1*0.47/(PSC1_A1/1000),col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1*0.47/(PSS1_A1/1000),col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2*0.47/(PSC3_A2/1000),col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2*0.47/(PSS3_A2/1000),col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2*0.47/(PSC1_A2/1000),col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2*0.47/(PSS1_A2/1000),col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3*0.47/(PSC3_A3/1000),col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3*0.47/(PSS3_A3/1000),col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3*0.47/(PSC1_A3/1000),col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3*0.47/(PSS1_A3/1000),col=mysalmon,pch=16,cex=0.8)
grid(nx=15,ny=5)
legend(x=0,y=0.5,legend=c(paste('r� = ',round(summary(ftDBC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
legend(x=10,y=0.5,legend=c(paste('r� = ',round(summary(ftDBC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
legend(x=20,y=0.5,legend=c(paste('r� = ',round(summary(ftDBC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=1)
abline(v=10)
abline(v=20)
axis(1,at=c(0:15)*2,labels=c(0,2,4,6,8,  4,6,8,10,12,   8,10,12,14,16,18),cex.axis=0.8)
axis(2,at=c(0:5)*0.1,labels=c(0:5)*0.1)
box()









# graph ou on rajoute Delta wood % GPP

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')

plot(HC3_A1,BC3_A1+32*4,col=mycols[4],xlab='Tree height (m)',ylab=expression('B'~(GJ~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,32*5),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1+32*4,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1+32*4,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1+32*4,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2+32*4,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2+32*4,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2+32*4,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2+32*4,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3+32*4,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3+32*4,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3+32*4,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3+32*4,col=mysalmon,pch=16,cex=0.8)
grid(nx=15,ny=20)                                                           
legend(x=10,y=160,legend=c('+K +W','- K +W','+K - W','- K - W'),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=1,cex=0.8,xjus=1)
legend(x=0,y=150,legend=c(paste('r� = ',round(summary(ftBC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftBC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=150,legend=c(paste('r� = ',round(summary(ftBC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=150,legend=c(paste('r� = ',round(summary(ftBC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftBC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftBS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bty='n',cex=0.6)#bg='white',box.col=0,cex=0.6)
legend(x=0,y=160,legend='1 year-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=10,y=160,legend='2 years-old',pch=NA,cex=1,bg='white',box.col=0)
legend(x=20,y=160,legend='3 years-old',pch=NA,cex=1,bg='white',box.col=0)
box()

par(new=T)
plot(HC3_A1,APARC3_A1/1000+32*3,col=mycols[4],xlab='Tree height (m)',ylab=expression('APAR'~(GJ~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,32*5),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,APARS3_A1/1000+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,APARC1_A1/1000+32*3,col=mycols[4],pch=16,cex=0.8)
points(HS1_A1,APARS1_A1/1000+32*3,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,APARC3_A2/1000+32*3,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,APARS3_A2/1000+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,APARC1_A2/1000+32*3,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,APARS1_A2/1000+32*3,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,APARC3_A3/1000+32*3,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,APARS3_A3/1000+32*3,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,APARC1_A3/1000+32*3,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,APARS1_A3/1000+32*3,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=128,legend=c(paste('r� = ',round(summary(ftAPARC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=128,legend=c(paste('r� = ',round(summary(ftAPARC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=128,legend=c(paste('r� = ',round(summary(ftAPARC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftAPARS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),cex=0.6,bg='white',box.col=0)
box()

par(new=T)
plot(HC3_A1,PSC3_A1/1000+40*2,col=mycols[4],xlab='Tree height (m)',ylab=expression('GPP'~(gC~d^-1~tree-1)),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,40*5),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,PSS3_A1/1000+40*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,PSC1_A1/1000+40*2,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,PSS1_A1/1000+40*2,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,PSC3_A2/1000+40*2,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,PSS3_A2/1000+40*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,PSC1_A2/1000+40*2,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,PSS1_A2/1000+40*2,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,PSC3_A3/1000+40*2,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,PSS3_A3/1000+40*2,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,PSC1_A3/1000+40*2,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,PSS1_A3/1000+40*2,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=120,legend=c(paste('r� = ',round(summary(ftPSC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=120,legend=c(paste('r� = ',round(summary(ftPSC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS12)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=120,legend=c(paste('r� = ',round(summary(ftPSC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftPSS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
box()

par(new=T)
plot(HC3_A1,BC3_A1*0.47/(PSC3_A1/1000)+0.5,col=mycols[4],xlab='Tree height (m)',ylab=expression(DELTA['wood']~('%GPP')),cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,0.5*5),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1*0.47/(PSS3_A1/1000)+0.5,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1*0.47/(PSC1_A1/1000)+0.5,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1*0.47/(PSS1_A1/1000)+0.5,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2*0.47/(PSC3_A2/1000)+0.5,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2*0.47/(PSS3_A2/1000)+0.5,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2*0.47/(PSC1_A2/1000)+0.5,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2*0.47/(PSS1_A2/1000)+0.5,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3*0.47/(PSC3_A3/1000)+0.5,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3*0.47/(PSS3_A3/1000)+0.5,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3*0.47/(PSC1_A3/1000)+0.5,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3*0.47/(PSS1_A3/1000)+0.5,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=1,legend=c(paste('r� = ',round(summary(ftDBC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS31)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=1,legend=c(paste('r� = ',round(summary(ftDBC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS32)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=1,legend=c(paste('r� = ',round(summary(ftDBC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS33)$r.squared,3)),
                          paste('r� = ',round(summary(ftDBS13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)


par(new=T)
plot(HC3_A1,BC3_A1/APARC3_A1*1000,col=mycols[4],xlab='Tree height (m)',ylab='LUE (gDM/MJ)',cex.lab=1.2,
      xlim=c(0,30),ylim=c(0,1.6*5),mgp=c(2.5,1,0),axes=F,ann=F,pch=16,cex=0.8)
points(HS3_A1,BS3_A1/APARS3_A1*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A1,BC1_A1/APARC1_A1*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A1,BS1_A1/APARS1_A1*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A2+6,BC3_A2/APARC3_A2*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A2+6,BS3_A2/APARS3_A2*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A2+6,BC1_A2/APARC1_A2*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A2+6,BS1_A2/APARS1_A2*1000,col=mysalmon,pch=16,cex=0.8)
points(HC3_A3+12,BC3_A3/APARC3_A3*1000,col=mycols[4],pch=16,cex=0.8)
points(HS3_A3+12,BS3_A3/APARS3_A3*1000,col=mycols[2],pch=16,cex=0.8)
points(HC1_A3+12,BC1_A3/APARC1_A3*1000,col=mycols[5],pch=16,cex=0.8)
points(HS1_A3+12,BS1_A3/APARS1_A3*1000,col=mysalmon,pch=16,cex=0.8)
legend(x=0,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC31)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC11)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES31)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES11)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=10,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC32)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC12)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES32)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES12)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
legend(x=20,y=1.6,legend=c(paste('r� = ',round(summary(ftLUEC33)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUEC13)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES33)$r.squared,3)),
                          paste('r� = ',round(summary(ftLUES13)$r.squared,3))),
                          pch=16,col=c(4,5,2,'salmon'),bg='white',box.col=0,cex=0.6)
abline(h=1.6)
abline(h=3.2)
abline(h=4.8)
abline(h=6.4)
abline(v=10)
abline(v=20)
axis(1,at=c(0:15)*2,labels=c(0,2,4,6,8,  4,6,8,10,12,   8,10,12,14,16,18),cex.axis=0.8)
axis(2,at=c(0,0.4,0.8,1.2,       1.6,1.92,2.24,2.56,2.88,     3.2,3.6,4,4.4,     4.8,5.2,5.6,6,    6.4,6.8,7.2,7.6,8),
      labels=c(0,0.4,0.8,1.2,    0,0.1,0.2,0.3,0.4, 0,10,20,30,     0,8,16,24,  0,8,16,24,32),cex.axis=0.8)

mtext(expression('LUEp'~(gDM~MJ^-1)~'   '~Delta[wood]~('%GPP')~'   '~'GPP'~~(kgC~tree^-1~y^-1)~'   '~aPAR~(GJ~tree^-1~y^-1)~'   '~Delta[wood]~(kg~tree^-1~y^-1)),side=2,line=2.5,cex=0.8)
mtext('Tree height (m)',side=1,line=2.5,cex=1)


























  