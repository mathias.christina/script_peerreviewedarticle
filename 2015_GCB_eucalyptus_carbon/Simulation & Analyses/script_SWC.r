set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
set6<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations/REF")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set6)

dC111 <- read.table('1_watlay.dat')
dC112 <- read.table('2_watlay.dat')
dC113 <- read.table('3_watlay.dat')

dC311 <- read.table('10_watlay.dat')
dC312 <- read.table('11_watlay.dat')
dC313 <- read.table('12_watlay.dat')

dS111 <- read.table('25_watlay.dat')
dS112 <- read.table('26_watlay.dat')
dS113 <- read.table('27_watlay.dat')

dS311 <- read.table('28_watlay.dat')
dS312 <- read.table('29_watlay.dat')
dS313 <- read.table('30_watlay.dat')

setwd(set2)
DAT <- read.table('Hum.txt',header=T)

c(-94,-86,-80,-73,-66,-58,-50,-45,-40,-34,-27,-15,-10,-4,3,10,17,24,31,38,48,52,60,68,73,80,87,94,101,108,115,122,129,137,143,149,157,164,171,177,185,192,200,206,213,220,227,234,241,248,256,262,269,276,283,290,297,304,311,318,325,332,339,346,353,360,367,374,378,388,395,402,409,416,423,428,437,445,451,456,467,472,480,488,495,500,504,514,521,528,537,544,549,557,563,573,577,584,591,599,605,613,619,626,633,640,648)

HUM <- data.frame(DAT[,1:3],DAT[,18:110])

names(HUM) <- c('Num','Trat','Prof',3,10,17,24,31,38,48,52,60,68,73,80,87,94,101,108,115,122,129,137,143,149,157,164,171,177,185,192,200,206,213,220,227,234,241,248,256,262,269,276,283,290,297,304,311,318,325,332,339,346,353,360,367,374,378,388,395,402,409,416,423,428,437,445,451,456,467,472,480,488,495,500,504,514,521,528,537,544,549,557,563,573,577,584,591,599,605,613,619,626,633,640,648)

DAY <- c(3,10,17,24,31,38,48,52,60,68,73,80,87,94,101,108,115,122,129,137,143,149,157,164,171,177,185,192,200,206,213,220,227,234,241,248,256,262,269,276,283,290,297,304,311,318,325,332,339,346,353,360,367,374,378,388,395,402,409,416,423,428,437,445,451,456,467,472,480,488,495,500,504,514,521,528,537,544,549,557,563,573,577,584,591,599,605,613,619,626,633,640,648)
# on passe les SWC en daily

watDayC1 <- rep(NA,24)
watDayC3 <- rep(NA,24)
watDayS1 <- rep(NA,24)
watDayS3 <- rep(NA,24)
for (i in 1:365){
x4 <-c();x1 <-c() ; x2 <-c() ;x3 <-c() ;for (j in 1:24){
x1 <- c(x1,mean(dC111[((i-1)*48+1):(i*48),j],na.rm=T))
x2 <- c(x2,mean(dC311[((i-1)*48+1):(i*48),j],na.rm=T))
x3 <- c(x3,mean(dS111[((i-1)*48+1):(i*48),j],na.rm=T))
x4 <- c(x4,mean(dS311[((i-1)*48+1):(i*48),j],na.rm=T))}
watDayC1 <- rbind(watDayC1,x1)
watDayC3 <- rbind(watDayC3,x2)
watDayS1 <- rbind(watDayS1,x3)
watDayS3 <- rbind(watDayS3,x4)}

for (i in 1:366){
x4 <-c();x1 <-c() ; x2 <-c() ;x3 <-c() ;for (j in 1:24){
x1 <- c(x1,mean(dC112[((i-1)*48+1):(i*48),j],na.rm=T))
x2 <- c(x2,mean(dC312[((i-1)*48+1):(i*48),j],na.rm=T))
x3 <- c(x3,mean(dS112[((i-1)*48+1):(i*48),j],na.rm=T))
x4 <- c(x4,mean(dS312[((i-1)*48+1):(i*48),j],na.rm=T))}
watDayC1 <- rbind(watDayC1,x1)
watDayC3 <- rbind(watDayC3,x2)
watDayS1 <- rbind(watDayS1,x3)
watDayS3 <- rbind(watDayS3,x4)}
watDayC11 <- watDayC1[2:732,1:24]
watDayC31 <- watDayC3[2:732,1:24]
watDayS11 <- watDayS1[2:732,1:24]
watDayS31 <- watDayS3[2:732,1:24]

# faison les moyennes

MC1 <- c()
MC3 <- c()
MS1 <- c()
MS3 <- c()
for(i in levels(as.factor(HUM$Prof))){
MC1 <- c(MC1, mean(HUM[,4][HUM$Trat=='C1'][HUM$Prof[HUM$Trat=='C1']==i],na.rm=T)/100)
MC3 <- c(MC3, mean(HUM[,4][HUM$Trat=='C3'][HUM$Prof[HUM$Trat=='C3']==i],na.rm=T)/100)
MS1 <- c(MS1, mean(HUM[,4][HUM$Trat=='S1'][HUM$Prof[HUM$Trat=='S1']==i],na.rm=T)/100)
MS3 <- c(MS3, mean(HUM[,4][HUM$Trat=='S3'][HUM$Prof[HUM$Trat=='S3']==i],na.rm=T)/100)
}
for (j in 5:96){
x4 <-c();x1 <-c() ; x2 <-c() ;x3 <-c()
for(i in levels(as.factor(HUM$Prof))){
x1 <- c(x1, mean(HUM[,j][HUM$Trat=='C1'][HUM$Prof[HUM$Trat=='C1']==i],na.rm=T)/100)
x2 <- c(x2, mean(HUM[,j][HUM$Trat=='C3'][HUM$Prof[HUM$Trat=='C3']==i],na.rm=T)/100)
x3 <- c(x3, mean(HUM[,j][HUM$Trat=='S1'][HUM$Prof[HUM$Trat=='S1']==i],na.rm=T)/100)
x4 <- c(x4, mean(HUM[,j][HUM$Trat=='S3'][HUM$Prof[HUM$Trat=='S3']==i],na.rm=T)/100)
}
MC1 <- rbind(MC1,x1)
MC3 <- rbind(MC3,x2)
MS1 <- rbind(MS1,x3)
MS3 <- rbind(MS3,x4)}

MC1 <- data.frame(MC1);MC3 <- data.frame(MC3);MS1 <- data.frame(MS1);MS3 <- data.frame(MS3)
names(MC1) <- levels(as.factor(HUM$Prof))
names(MC3) <- levels(as.factor(HUM$Prof))
names(MS1) <- levels(as.factor(HUM$Prof))
names(MS3) <- levels(as.factor(HUM$Prof))


ALLSC1 <- c(watDayC1[,3][DAY],(watDayC1[,4][DAY]+watDayC1[,5][DAY])/2,(watDayC1[,6][DAY]+watDayC1[,7][DAY])/2 ,watDayC1[,9][DAY],(watDayC1[,10][DAY]+watDayC1[,11][DAY])/2,
            watDayC1[,12][DAY],watDayC1[,14][DAY], watDayC1[,16][DAY], watDayC1[,18][DAY],watDayC1[,20][DAY],watDayC1[,23][DAY])
ALLMC1 <- c(MC1[,1],MC1[,2],MC1[,3], MC1[,4], MC1[,5],MC1[,6],MC1[,7],MC1[,8],MC1[,9], MC1[,10],MC1[,11])
ALLSC3 <- c(watDayC3[,3][DAY],(watDayC3[,4][DAY]+watDayC3[,5][DAY])/2,(watDayC3[,6][DAY]+watDayC3[,7][DAY])/2 ,watDayC3[,9][DAY],(watDayC3[,10][DAY]+watDayC3[,11][DAY])/2,
            watDayC3[,12][DAY],watDayC3[,14][DAY], watDayC3[,16][DAY], watDayC3[,18][DAY],watDayC3[,20][DAY],watDayC3[,23][DAY])
ALLMC3 <- c(MC3[,1],MC3[,2],MC3[,3], MC3[,4], MC3[,5],MC3[,6],MC3[,7],MC3[,8],MC3[,9], MC3[,10],MC3[,11])
ALLSS1 <- c(watDayS1[,3][DAY],(watDayS1[,4][DAY]+watDayS1[,5][DAY])/2,(watDayS1[,6][DAY]+watDayS1[,7][DAY])/2 ,watDayS1[,9][DAY],(watDayS1[,10][DAY]+watDayS1[,11][DAY])/2,
            watDayS1[,12][DAY],watDayS1[,14][DAY], watDayS1[,16][DAY], watDayS1[,18][DAY],watDayS1[,20][DAY],watDayS1[,23][DAY])
ALLMS1 <- c(MS1[,1],MS1[,2],MS1[,3], MS1[,4], MS1[,5],MS1[,6],MS1[,7],MS1[,8],MS1[,9], MS1[,10],MS1[,11])
ALLSS3 <- c(watDayS3[,3][DAY],(watDayS3[,4][DAY]+watDayS3[,5][DAY])/2,(watDayS3[,6][DAY]+watDayS3[,7][DAY])/2 ,watDayS3[,9][DAY],(watDayS3[,10][DAY]+watDayS3[,11][DAY])/2,
            watDayS3[,12][DAY],watDayS3[,14][DAY], watDayS3[,16][DAY], watDayS3[,18][DAY],watDayS3[,20][DAY],watDayS3[,23][DAY])
ALLMS3 <- c(MS3[,1],MS3[,2],MS3[,3], MS3[,4], MS3[,5],MS3[,6],MS3[,7],MS3[,8],MS3[,9], MS3[,10],MS3[,11])

FC1 <- lm(ALLSC1~ALLMC1-1)
FC3 <- lm(ALLSC3~ALLMC3-1)
FS1 <- lm(ALLSS1~ALLMS1-1)
FS3 <- lm(ALLSS3~ALLMS3-1)


par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(c(ALLMC1,ALLMC3,ALLMS1,ALLMS3),c(ALLSC1,ALLSC3,ALLSS1,ALLSS3),xlab='Measured SWC',ylab='Predicted SWC',mgp=c(2.5,1,0),cex.lab=1.2)
abline(FC1,col=5,lwd=2)
abline(FC3,col=4,lwd=2)
abline(FS1,col='salmon',lwd=2)
abline(FS3,col=2,lwd=2)
grid(nx=NULL,ny=NULL)
legend('bottomright',legend=c(paste('- K +W: SWCpred = SWCmeas * ',round(summary(FC1)$coef[1],3),sep=''),
                              paste('        R� = ',round(summary(FC1)$r.squared,5),sep=''),
                              paste('+K +W: SWCpred = SWCmeas * ',round(summary(FC3)$coef[1],3),sep=''),
                              paste('        R� = ',round(summary(FC3)$r.squared,5),sep=''),
                              paste('-K -W: SWCpred = SWCmeas * ',round(summary(FS1)$coef[1],3),sep=''),
                              paste('        R� = ',round(summary(FS1)$r.squared,5),sep=''),
                              paste('+K - W: SWCpred = SWCmeas * ',round(summary(FS3)$coef[1],3),sep=''),
                              paste('        R� = ',round(summary(FS3)$r.squared,5),sep='')),                              
                              pch=NA,lty=c(1,NA,1,NA,1,NA),col=c(5,5,4,4,'salmon','salmon',2,2),bg='white',box.col=0,cex=1)


# 15 <- L2 ; 50 L3/L4; 150 L5/L6; 300  L8  ;450, L9/L10 ; 600 L11, 800 L12, 1000 L14, 1200 L16, 1400, L18, 1700 L21

# pour le moment tout +1 car double litter

#laythick = c(0.02,0.02,0.2,0.3,0.5,0.5,0.5,0.5,1,1,1,1,1,1,1,1,1,rep(1,6))
#         = c(0,0,  0.2,0.5,1  ,1.5,2  ,2.5,3.5,4.5,5.5,6.5
#"15"   "50"   "150"  "300"  "450"  "600"  "800"  "1000" "1200" "1400" "1700"


YC1 <- watDayC1[,3] ; mC1 <- MC1[,1]
YC3 <- watDayC3[,3] ; mC3 <- MC3[,1]
YS3 <- watDayS3[,3] ; mS3 <- MS3[,1]
PROF <- '0-20cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- (watDayC1[,4]+watDayC1[,5])/2 ; mC1 <- MC1[,2]
YC3 <- (watDayC3[,4]+watDayC3[,5])/2 ; mC3 <- MC3[,2]
YS3 <- (watDayS3[,4]+watDayS3[,5])/2 ; mS3 <- MS3[,2]
PROF <- '50cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- (watDayC1[,6]+watDayC1[,7])/2 ; mC1 <- MC1[,3]
YC3 <- (watDayC3[,6]+watDayC3[,7])/2 ; mC3 <- MC3[,3]
YS3 <- (watDayS3[,6]+watDayS3[,7])/2 ; mS3 <- MS3[,3]
PROF <- '150cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- watDayC1[,9] ; mC1 <- MC1[,4]
YC3 <- watDayC3[,9] ; mC3 <- MC3[,4]
YS3 <- watDayS3[,9] ; mS3 <- MS3[,4]
PROF <- '300cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

# 15 <- L2 ; 50 L3/L4; 150 L5/L6; 300  L8  ;450, L9/L10 ; 600 L11, 800 L12, 1000 L14, 1200 L16, 1400, L18, 1700 L21

# pour le moment tout +1 car double litter

#laythick = c(0.02,0.02,0.2,0.3,0.5,0.5,0.5,0.5,1,1,1,1,1,1,1,1,1,rep(1,6))
#         = c(0,0,  0.2,0.5,1  ,1.5,2  ,2.5,3.5,4.5,5.5,6.5
#"15"   "50"   "150"  "300"  "450"  "600"  "800"  "1000" "1200" "1400" "1700"

YC1 <- (watDayC1[,10]+watDayC1[,11])/2 ; mC1 <- MC1[,5]
YC3 <- (watDayC3[,10]+watDayC3[,11])/2 ; mC3 <- MC3[,5]
YS3 <- (watDayS3[,10]+watDayS3[,11])/2 ; mS3 <- MS3[,5]
PROF <- '450cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)
YC1 <- watDayC1[,12] ; mC1 <- MC1[,6]
YC3 <- watDayC3[,12] ; mC3 <- MC3[,6]
YS3 <- watDayS3[,12] ; mS3 <- MS3[,6]
PROF <- '600cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- watDayC1[,14] ; mC1 <- MC1[,7]
YC3 <- watDayC3[,14] ; mC3 <- MC3[,7]
YS3 <- watDayS3[,14] ; mS3 <- MS3[,7]
PROF <- '800cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)
YC1 <- watDayC1[,16] ; mC1 <- MC1[,8]
YC3 <- watDayC3[,16] ; mC3 <- MC3[,8]
YS3 <- watDayS3[,16] ; mS3 <- MS3[,8]
PROF <- '1000cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- watDayC1[,18] ; mC1 <- MC1[,9]
YC3 <- watDayC3[,18] ; mC3 <- MC3[,9]
YS3 <- watDayS3[,18] ; mS3 <- MS3[,9]
PROF <- '1200cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- watDayC1[,20] ; mC1 <- MC1[,10]
YC3 <- watDayC3[,20] ; mC3 <- MC3[,10]
YS3 <- watDayS3[,20] ; mS3 <- MS3[,10]
PROF <- '1400cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

YC1 <- watDayC1[,23] ; mC1 <- MC1[,11]
YC3 <- watDayC3[,23] ; mC3 <- MC3[,11]
YS3 <- watDayS3[,23] ; mS3 <- MS3[,11]
PROF <- '1700cm'
PLOT(YC1,mC1,YC3,mC3,YS3,mS3,PROF)

plot((watDayC3[,5]+watDayC3[,4])/2,type='l',ylim=c(0,0.3))
points(DAY, MC3[,2],pch=16,col=2)

plot((watDayC3[,6]+watDayC3[,7])/2,type='l',ylim=c(0,0.3))
points(DAY, MC3[,3],pch=16,col=2)

plot(watDayC3[,9],type='l',ylim=c(0,0.3))
points(DAY, MC3[,4],pch=16,col=2)

plot((watDayC3[,9]+watDayC3[,10])/2,type='l',ylim=c(0,0.3))
points(DAY, MC3[,5],pch=16,col=2)

plot(watDayC3[,22],type='l',ylim=c(0,0.3))
points(DAY, MC3[,11],pch=16,col=2)






PLOT <- function(YC1,mC1,YC3,mC3,YS3,mS3,PROF){

if(c(sum(mC1,na.rm=T)==0)==F){fitC1 <-lm(YC1[DAY]~mC1-1)}
if(c(sum(mC1,na.rm=T)==0)==T){fitC1 <-NA}
if(c(sum(mC3,na.rm=T)==0)==F){fitC3 <-lm(YC3[DAY]~mC3-1)}
if(c(sum(mC3,na.rm=T)==0)==T){fitC3 <-NA}
if(c(sum(mS3,na.rm=T)==0)==F){fitS3 <-lm(YS3[DAY]~mS3-1)}
if(c(sum(mS3,na.rm=T)==0)==T){fitS3 <-NA}

par(mfrow=c(3,2))
par(mar=c(4,4,1,1))
plot(YC1,type='l',ylim=c(0,0.3),xlab=c('Day since 1st Jan 2011'),ylab='SWC',mgp=c(2.5,1,0),cex.lab=1.2)
points(DAY, mC1,pch=16,col=2)
legend('topleft',legend='C1',pch=NA,cex=1.2,bty='n',text.font=2)
legend('topright',legend=PROF,pch=NA,cex=1.2,bty='n',text.font=2)
legend('bottomleft',legend=c('Predicted SWC','Measured SWC'),pch=c(NA,16),lty=c(1,NA),col=1:2,cex=1.2,text.font=2,bty='n')
plot(mC1,YC1[DAY],xlim=c(0,0.3),ylim=c(0,0.3),xlab='Measured SWC',ylab='Predicted SWC',cex.lab=1.2,mgp=c(2.5,1,0))
abline(0,1)
if(c(sum(mC1,na.rm=T)==0)==F){abline(fitC1,col=2,lwd=2)}
legend('topleft',legend=paste('C1, ',PROF,sep=''),pch=NA,cex=1.2,bty='n',text.font=2)
if(c(sum(mC1,na.rm=T)==0)==F){legend('bottomright',legend=paste('SWCpred = SWCmeas * ',round(summary(fitC1)$coef[1],3),' ; R� = ',round(summary(fitC1)$r.squared,3),sep=''),
                    pch=NA,lty=1,col=2,text.font=2,cex=1.2,bty='n')}
plot(YC3,type='l',ylim=c(0,0.3),xlab=c('Day since 1st Jan 2011'),ylab='SWC',mgp=c(2.5,1,0),cex.lab=1.2)
points(DAY, mC3,pch=16,col=2)
legend('topleft',legend='C3',pch=NA,cex=1.2,bty='n',text.font=2)
legend('topright',legend=PROF,pch=NA,cex=1.2,bty='n',text.font=2)
plot(mC3,YC3[DAY],xlim=c(0,0.3),ylim=c(0,0.3),xlab='Measured SWC',ylab='Predicted SWC',cex.lab=1.2,mgp=c(2.5,1,0))
abline(0,1)
if(c(sum(mC3,na.rm=T)==0)==F){abline(fitC3,col=2,lwd=2)}
legend('topleft',legend=paste('C3, ',PROF,sep=''),pch=NA,cex=1.2,bty='n',text.font=2)
if(c(sum(mC3,na.rm=T)==0)==F){legend('bottomright',legend=paste('SWCpred = SWCmeas * ',round(summary(fitC3)$coef[1],3),' ; R� = ',round(summary(fitC3)$r.squared,3),sep=''),
                    pch=NA,lty=1,col=2,text.font=2,cex=1.2,bty='n')}

plot(YS3,type='l',ylim=c(0,0.3),xlab=c('Day since 1st Jan 2011'),ylab='SWC',mgp=c(2.5,1,0),cex.lab=1.2)
points(DAY, mS3,pch=16,col=2)
legend('topleft',legend='S3',pch=NA,cex=1.2,bty='n',text.font=2)
legend('topright',legend=PROF,pch=NA,cex=1.2,bty='n',text.font=2)
plot(mS3,YS3[DAY],xlim=c(0,0.3),ylim=c(0,0.3),xlab='Measured SWC',ylab='Predicted SWC',cex.lab=1.2,mgp=c(2.5,1,0))
abline(0,1)
if(c(sum(mS3,na.rm=T)==0)==F){abline(fitS3,col=2,lwd=2)}
legend('topleft',legend=paste('S3, ',PROF,sep=''),pch=NA,cex=1.2,bty='n',text.font=2)
if(c(sum(mS3,na.rm=T)==0)==F){legend('bottomright',legend=paste('SWCpred = SWCmeas * ',round(summary(fitS3)$coef[1],3),' ; R� = ',round(summary(fitS3)$r.squared,3),sep=''),
                    pch=NA,lty=1,col=2,text.font=2,cex=1.2,bty='n')}

}





#estimons les minimum de SWC

MINC1 <- c()
MINS1 <- c()
MINC3 <- c()
MINS3 <- c()
for (i in 1:11){
MINC1 <- c(MINC1,min(MC1[,i],na.rm=T))
MINC3 <- c(MINC3,min(MC3[,i],na.rm=T))
MINS1 <- c(MINS1,min(MS1[,i],na.rm=T))
MINS3 <- c(MINS3,min(MS3[,i],na.rm=T))}
