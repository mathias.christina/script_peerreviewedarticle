set1<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/runMaespa")
set2<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION")
set3<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Maespa_original")
set4<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Simulations")
set5<-c("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/cluster")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_phy.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_watpar.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_trees.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_confile.r")
source("C:/Users/mathias/Documents/R/R_Maespa_th�se/Script Maespa EXCLUSION/Fonction_input/run_str.r")

setwd(set4)


dC111 <- read.table('1_watupt.dat')
dC112 <- read.table('2_watupt.dat')
dC113 <- read.table('3_watupt.dat')
dC121 <- read.table('4_watupt.dat')
dC122 <- read.table('5_watupt.dat')
dC123 <- read.table('6_watupt.dat')
dC131 <- read.table('7_watupt.dat')
dC132 <- read.table('8_watupt.dat')
dC133 <- read.table('9_watupt.dat')

dC311 <- read.table('10_watupt.dat')
dC312 <- read.table('11_watupt.dat')
dC313 <- read.table('12_watupt.dat')
dC321 <- read.table('13_watupt.dat')
dC322 <- read.table('14_watupt.dat')
dC323 <- read.table('15_watupt.dat')
dC331 <- read.table('16_watupt.dat')
dC332 <- read.table('17_watupt.dat')
dC333 <- read.table('18_watupt.dat')

dS111 <- read.table('19_watupt.dat')
dS112 <- read.table('20_watupt.dat')
dS113 <- read.table('21_watupt.dat')
dS121 <- read.table('22_watupt.dat')
dS122 <- read.table('23_watupt.dat')
dS123 <- read.table('24_watupt.dat')
dS131 <- read.table('25_watupt.dat')
dS132 <- read.table('26_watupt.dat')
dS133 <- read.table('27_watupt.dat')

dS311 <- read.table('28_watupt.dat')
dS312 <- read.table('29_watupt.dat')
dS313 <- read.table('30_watupt.dat')
dS321 <- read.table('31_watupt.dat')
dS322 <- read.table('32_watupt.dat')
dS323 <- read.table('33_watupt.dat')
dS331 <- read.table('34_watupt.dat')
dS332 <- read.table('35_watupt.dat')
dS333 <- read.table('36_watupt.dat')




dC11 <- rbind(dC111,dC112,dC113)
dC12 <- rbind(dC121,dC122,dC123)
dC13 <- rbind(dC131,dC132,dC133)
dC31 <- rbind(dC311,dC312,dC313)
dC32 <- rbind(dC321,dC322,dC323)
dC33 <- rbind(dC331,dC332,dC333)
dS11 <- rbind(dS111,dS112,dS113)
dS12 <- rbind(dS121,dS122,dS123)
dS13 <- rbind(dS131,dS132,dS133)
dS31 <- rbind(dS311,dS312,dS313)
dS32 <- rbind(dS321,dS322,dS323)
dS33 <- rbind(dS331,dS332,dS333)

uptDayC1 <- rep(NA,23)
uptDayC3 <- rep(NA,23)
uptDayS3 <- rep(NA,23)
for (i in 1:899){
xC1 <-c()
xC3 <-c()
xS1 <-c()
xS3 <-c()
for (j in 1:23){
xC1 <- c(xC1,mean(c(dC11[((i-1)*48+1):(i*48),j],dC13[((i-1)*48+1):(i*48),j]),na.rm=T))
xC3 <- c(xC3,mean(c(dC31[((i-1)*48+1):(i*48),j],dC32[((i-1)*48+1):(i*48),j],dC33[((i-1)*48+1):(i*48),j]),na.rm=T))
xS3 <- c(xS3,mean(c(dS31[((i-1)*48+1):(i*48),j],dS32[((i-1)*48+1):(i*48),j],dS33[((i-1)*48+1):(i*48),j]),na.rm=T))
}
uptDayC1 <- rbind(uptDayC1,xC1)
uptDayC3 <- rbind(uptDayC3,xC3)
uptDayS3 <- rbind(uptDayS3,xS3)
}
uptDayC1 <- uptDayC1[2:dim(uptDayC1)[1],3:dim(uptDayC1)[2]]*LEC1
uptDayC3 <- uptDayC3[2:dim(uptDayC3)[1],3:dim(uptDayC3)[2]]*LEC3
uptDayS3 <- uptDayS3[2:dim(uptDayS3)[1],3:dim(uptDayS3)[2]]*LES3


N=7
uptC1 <- rep(NA,21)
uptC3 <- rep(NA,21)
uptS3 <- rep(NA,21)
Day <-c()
for (i in 1:floor(899/N)){
xC1 <-c()
xC3 <-c()
xS3 <-c()
for (j in 1:21){
xC1 <- c(xC1,mean(uptDayC1[((i-1)*N+1):(i*N),j]))
xC3 <- c(xC3,mean(uptDayC3[((i-1)*N+1):(i*N),j]))
xS3 <- c(xS3,mean(uptDayS3[((i-1)*N+1):(i*N),j]))
}
uptC1 <- rbind(uptC1,xC1)
uptC3 <- rbind(uptC3,xC3)
uptS3 <- rbind(uptS3,xS3)
Day <- c(Day,(i-1)*N+1)}
uptC1 <- uptC1[2:dim(uptC1)[1],]
uptC3 <- uptC3[2:dim(uptC3)[1],]
uptS3 <- uptS3[2:dim(uptS3)[1],]

Pr <- c('0-1m','1-2.5m','2.5-5.5m','5.5-8.5m','8.5-12.5m','>12.5m')

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
barplot(PPT,ann=F,axes=F,border=NA)
par(new=T)
plot(Day,uptC1[,1]+uptC1[,2]+uptC1[,3],ylab=expression('Simulated Soil Water uptake'~(mm~d^-1)),
    xlab='Day from Jan 2011 to Jun 2013',cex.lab=1.2,type='l',lwd=2,ylim=c(0,3))
points(Day,uptC1[,4]+uptC1[,5]+uptC1[,6],col=2,type='l',lwd=2)
points(Day,uptC1[,7]+uptC1[,8]+uptC1[,9],col=3,type='l',lwd=2)
points(Day,uptC1[,10]+uptC1[,11]+uptC1[,12],col=4,type='l',lwd=2)
points(Day,uptC1[,13]+uptC1[,14]+uptC1[,15],col=5,type='l',lwd=2)
points(Day,uptC1[,16]+uptC1[,17]+uptC1[,18]+uptC1[,19]+uptC1[,20]+uptC1[,21],col=6,type='l',lwd=2)
grid(nx=NA,ny=NULL,col=1)
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,2.75,'2010',font=4)
text(550,2.75,'2011',font=4)
text(850,2.75,'2012',font=4)
legend('topleft',legend=Pr,pch=NA,lty=1,col=1:6,cex=1.2,bg='white',box.col=0)
legend('topright',legend='-K +R',pch=NA,cex=1.2,,bg='white',box.col=0)
box()

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
barplot(PPT,ann=F,axes=F,border=NA)
par(new=T)
plot(Day,uptC3[,1]+uptC3[,2]+uptC3[,3],ylab=expression('Simulated Soil Water uptake'~(mm~d^-1)),
    xlab='Day from Jan 2011 to Jun 2013',cex.lab=1.2,type='l',lwd=2,ylim=c(0,6))
points(Day,uptC3[,4]+uptC3[,5]+uptC3[,6],col=2,type='l',lwd=2)
points(Day,uptC3[,7]+uptC3[,8]+uptC3[,9],col=3,type='l',lwd=2)
points(Day,uptC3[,10]+uptC3[,11]+uptC3[,12],col=4,type='l',lwd=2)
points(Day,uptC3[,13]+uptC3[,14]+uptC3[,15],col=5,type='l',lwd=2)
points(Day,uptC3[,16]+uptC3[,17]+uptC3[,18]+uptC3[,19]+uptC3[,20]+uptC3[,21],col=6,type='l',lwd=2)
grid(nx=NA,ny=NULL,col=1)
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,5.5,'2010',font=4)
text(550,5.5,'2011',font=4)
text(850,5.5,'2012',font=4)
legend('topleft',legend=Pr,pch=NA,lty=1,col=1:6,cex=1.2,bg='white',box.col=0)
legend('topright',legend='+K +R',pch=NA,cex=1.2,,bg='white',box.col=0)
box()


par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
barplot(PPT,ann=F,axes=F,border=NA)
par(new=T)
plot(Day,uptS3[,1]+uptS3[,2]+uptS3[,3],ylab=expression('Simulated Soil Water uptake'~(mm~d^-1)),
    xlab='Day from Jan 2011 to Jun 2013',cex.lab=1.2,type='l',lwd=2,ylim=c(0,5))
points(Day,uptS3[,4]+uptS3[,5]+uptS3[,6],col=2,type='l',lwd=2)
points(Day,uptS3[,7]+uptS3[,8]+uptS3[,9],col=3,type='l',lwd=2)
points(Day,uptS3[,10]+uptS3[,11]+uptS3[,12],col=4,type='l',lwd=2)
points(Day,uptS3[,13]+uptS3[,14]+uptS3[,15],col=5,type='l',lwd=2)
points(Day,uptS3[,16]+uptS3[,17]+uptS3[,18]+uptS3[,19]+uptS3[,20]+uptS3[,21],col=6,type='l',lwd=2)
grid(nx=NA,ny=NULL,col=1)
abline(v=365.5,lty='dotted')
abline(v=730.5,lty='dotted')
text(300,4.5,'2011',font=4)
text(550,4.5,'2012',font=4)
text(850,4.5,'2013',font=4)
legend('topleft',legend=Pr,pch=NA,lty=1,col=1:6,cex=1.2,bg='white',box.col=0)
legend('topright',legend='+K -R',pch=NA,cex=1.2,,bg='white',box.col=0)
box()

UC1_1 <- c(sum(uptDayC1[1:366,1:3],na.rm=T),sum(uptDayC1[1:366,4:6],na.rm=T),sum(uptDayC1[1:366,7:9],na.rm=T),
          sum(uptDayC1[1:366,10:12],na.rm=T),sum(uptDayC1[1:366,13:15],na.rm=T),sum(uptDayC1[1:366,16:21],na.rm=T))
UC1_2 <- c(sum(uptDayC1[367:731,1:3],na.rm=T),sum(uptDayC1[367:731,4:6],na.rm=T),sum(uptDayC1[367:731,7:9],na.rm=T),
          sum(uptDayC1[367:731,10:12],na.rm=T),sum(uptDayC1[367:731,13:15],na.rm=T),sum(uptDayC1[367:731,16:21],na.rm=T))
UC1_3 <- c(sum(uptDayC1[732:899,1:3],na.rm=T),sum(uptDayC1[732:899,4:6],na.rm=T),sum(uptDayC1[732:899,7:9],na.rm=T),
          sum(uptDayC1[732:899,10:12],na.rm=T),sum(uptDayC1[732:899,13:15],na.rm=T),sum(uptDayC1[732:899,16:21],na.rm=T))
UC3_1 <- c(sum(uptDayC3[1:366,1:3],na.rm=T),sum(uptDayC3[1:366,4:6],na.rm=T),sum(uptDayC3[1:366,7:9],na.rm=T),
          sum(uptDayC3[1:366,10:12],na.rm=T),sum(uptDayC3[1:366,13:15],na.rm=T),sum(uptDayC3[1:366,16:21],na.rm=T))
UC3_2 <- c(sum(uptDayC3[367:731,1:3],na.rm=T),sum(uptDayC3[367:731,4:6],na.rm=T),sum(uptDayC3[367:731,7:9],na.rm=T),
          sum(uptDayC3[367:731,10:12],na.rm=T),sum(uptDayC3[367:731,13:15],na.rm=T),sum(uptDayC3[367:731,16:21],na.rm=T))
UC3_3 <- c(sum(uptDayC3[732:899,1:3],na.rm=T),sum(uptDayC3[732:899,4:6],na.rm=T),sum(uptDayC3[732:899,7:9],na.rm=T),
          sum(uptDayC3[732:899,10:12],na.rm=T),sum(uptDayC3[732:899,13:15],na.rm=T),sum(uptDayC3[732:899,16:21],na.rm=T))
US3_1 <- c(sum(uptDayS3[1:366,1:3],na.rm=T),sum(uptDayS3[1:366,4:6],na.rm=T),sum(uptDayS3[1:366,7:9],na.rm=T),
          sum(uptDayS3[1:366,10:12],na.rm=T),sum(uptDayS3[1:366,13:15],na.rm=T),sum(uptDayS3[1:366,16:21],na.rm=T))
US3_2 <- c(sum(uptDayS3[367:731,1:3],na.rm=T),sum(uptDayS3[367:731,4:6],na.rm=T),sum(uptDayS3[367:731,7:9],na.rm=T),
          sum(uptDayS3[367:731,10:12],na.rm=T),sum(uptDayS3[367:731,13:15],na.rm=T),sum(uptDayS3[367:731,16:21],na.rm=T))
US3_3 <- c(sum(uptDayS3[732:899,1:3],na.rm=T),sum(uptDayS3[732:899,4:6],na.rm=T),sum(uptDayS3[732:899,7:9],na.rm=T),
          sum(uptDayS3[732:899,10:12],na.rm=T),sum(uptDayS3[732:899,13:15],na.rm=T),sum(uptDayS3[732:899,16:21],na.rm=T))

MATU <- data.frame(UC1_1/sum(UC1_1),UC1_2/sum(UC1_2),UC1_3/sum(UC1_3),UC3_1/sum(UC3_1),UC3_2/sum(UC3_2),UC3_3/sum(UC3_3),US3_1/sum(US3_1),US3_2/sum(US3_2),US3_3/sum(US3_3))
MATU2 <- as.matrix(MATU)

palette('default')


par(mar=c(4.5,4.5,2,2))
par(yaxs='i')
barplot(MATU2*100,col=1:6,border=NA,xlim=c(0,12),names.arg=c('','C1 : -K +R','','','C3 : +K +R','','','S3 : +K -R',''),
        legend.text=Pr,space=c(0.2,0.05,0.05,0.5,0.05,0.05,0.5,0.05,0.05),
        ylab='Proportion of water uptake (%)',xlab='Treatment',cex.lab=1.2,cex.names=1.2)
text(0.7,75,'2011',font=2)
text(1.75,75,'2012',font=2)
text(2.8,75,'2013',font=2)
box()



