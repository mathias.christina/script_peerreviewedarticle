
set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Effet des traitements/racines")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

dat1 <- read.table('Alldatas.txt',header=T)
dat2 <- read.table('Alldatas2013.txt',header=T)

# Figure :

MASSTOT12 <- c(253.9329, 388.6382, 278.4325, 427.0762)                    
MASSTOT13 <- c(695.779496689973,898.4795068, 597.7056876, 960.419722602016)

MAT <- matrix(c(MASSTOT12,MASSTOT13),nrow=2,byrow=T)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(0,0,col=0,ann=F,axes=F,ylim=c(0,1200))
grid(nx=NA,ny=NULL,col=1)
par(new=T)
barplot(MAT,beside=T,ylim=c(0,1200),xlab='Treatment',ylab='Fine root mass (g/m�)',
        cex.lab=1.2,names.arg=c('-K +R','+K +R','-K -R','+K -R'),legend.text=c('2 year-old','3 year-old'))
box()

FRC112 <- c(26.027486125,15.282872979,12.460634184,25.551647456,12.256413017,3.957368487,1.864433161,1.217028413,0.387909403,0.904637807,0.058839767,0.011763980,0.010192618,0.008772605,0.000000000,rep(0,6))/100
FRC312 <- c(16.658181251,10.257005549,17.795310300,19.610320527,13.263842699,3.502450612,6.376735795,3.460023280,1.972843358,3.445307029,2.296571007,0.803650681,0.005215453,0.552542459,0.000000000,rep(0,6))/100
FRS112 <- c(0.2071010423,0.1147600750,0.1984348080,0.1867085296,0.0771970148,0.0909475770,0.0373678845,0.0224689724,0.0085480506,0.0322670671,0.0229807041,0.0001180273,0.0009145471,0.0001857002,0.0000000000,rep(0,6))
FRS312 <- c(0.1936437754,0.1048010809,0.1514097021,0.1402826726,0.1081047937,0.0645770221,0.0639355589,0.0861709468,0.0266928473,0.0343745516,0.0160729734,0.0087373933,0.0007462074,0.0004504744,0.0000000000,rep(0,6))
FRC113 <- c(0.209125888,0.116032370,0.123261730,0.137855760,0.112009499,0.044055855,0.072504439,0.061012604,0.030512723,0.041723273,0.014157679,0.001688073,0.010334043,0.009853194,0.006015768,0.005201666,0.002363239,0.001099431,0.001192763,0.000000000,0.000000000)  
FRC313 <- c(0.1402496352,0.0735211654,0.0922107588,0.0940957530,0.1158167609,0.0694345859,0.1023746674,0.0833781767,0.0971260528,0.0399519289,0.0295047075,0.0162488299,0.0291670119,0.0057605125,0.0041285115,0.0004843772,0.0040115119,0.0010440548,0.0014909977,0.0000000000,0.0000000000)
FRS113 <- c(23.021657069,13.725470070,13.937860050,14.265831973,8.427951346,3.328462568,10.798638700,7.181051835,1.656915835,2.592980207,0.750395913,0.244419752,0.035859030,0.006466238,0.005031799,0.006736613,0.003184858,0.011086144,0.000000000,0.000000000,0.000000000)/100
FRS313 <- c(20.813903068,8.134487674,12.600627762,13.357210809,10.008404446,6.621087804,7.850223525,2.682185749,4.467349240,4.961089225,3.581780263,2.772393587,0.852944035,0.882178814,0.359058772,0.006648326,0.030256361,0.003794868,0.003474171,0.004918545,0.005982956)/100

PC112 <-FRC112[1]/sum(FRC112) ; PC312<- FRC312[1]/sum(FRC312) ; PS112<-FRS112[1]/sum(FRS112);PS312<-FRS312[1]/sum(FRS312)
PC113 <-FRC113[1]/sum(FRC113) ; PC313<- FRC313[1]/sum(FRC313) ; PS113<-FRS113[1]/sum(FRS113);PS313<-FRS313[1]/sum(FRS313)
for (i in 2:21){
PC112 <- c(PC112,  sum(FRC112[1:i])/sum(FRC112))
PC312 <- c(PC312,  sum(FRC312[1:i])/sum(FRC312))
PS112 <- c(PS112,  sum(FRS112[1:i])/sum(FRS112))
PS312 <- c(PS312,  sum(FRS312[1:i])/sum(FRS312))
PC113 <- c(PC113,  sum(FRC113[1:i])/sum(FRC113))
PC313 <- c(PC313,  sum(FRC313[1:i])/sum(FRC313))
PS113 <- c(PS113,  sum(FRS113[1:i])/sum(FRS113))
PS313 <- c(PS313,  sum(FRS313[1:i])/sum(FRS313))
}

FRC312N <- c(FRC312[1]+FRC312[2],FRC312[3],FRC312[4],FRC312[5]+FRC312[6],FRC312[7:21])
FRC112N <- c(FRC112[1]+FRC112[2],FRC112[3],FRC112[4],FRC112[5]+FRC112[6],FRC112[7:21])
FRS112N <- c(FRS112[1]+FRS112[2],FRS112[3],FRS112[4],FRS112[5]+FRS112[6],FRS112[7:21])
FRS312N <- c(FRS312[1]+FRS312[2],FRS312[3],FRS312[4],FRS312[5]+FRS312[6],FRS312[7:21])
FRC313N <- c(FRC313[1]+FRC313[2],FRC313[3],FRC313[4],FRC313[5]+FRC313[6],FRC313[7:21])
FRC113N <- c(FRC113[1]+FRC113[2],FRC113[3],FRC113[4],FRC113[5]+FRC113[6],FRC113[7:21])
FRS113N <- c(FRS113[1]+FRS113[2],FRS113[3],FRS113[4],FRS113[5]+FRS113[6],FRS113[7:21])
FRS313N <- c(FRS313[1]+FRS313[2],FRS313[3],FRS313[4],FRS313[5]+FRS313[6],FRS313[7:21])
DEPN <- c(DEP[1]+DEP[2],DEP[3],DEP[4],DEP[5]+DEP[6],DEP[7:21])

MAT <- data.frame(DEPN,FRC312N,FRC112N,FRS112N,FRS312N,FRC313N,FRC113N,FRS113N,FRS313N)
write.table(MAT,'DIST.txt',col.names=T,row.names=F)

DEP <- c(0.1,0.35,0.75,1.25, 1.75,2.25,3:17)

par(mfrow=c(1,2))
par(mar=c(1,4.5,4.5,2))
par(xaxs='i',yaxs='i')
plot(PC112,-DEP,type='b',pch=1,lwd=2,col=1,xlim=c(0,1),ylim=c(-18,0),axes=F,ann=F)
points(PC312, -DEP,type='b',pch=1,col=2,lwd=2)
points(PS112, -DEP,type='b',pch=4,col=3,lwd=2)
points(PS312, -DEP,type='b',pch=4,col=4,lwd=2)
grid(nx=NULL,ny=NA,col=1)
axis(2,at=c(-15,-10,-5,0),labels=c(15,10,5,0))
axis(3,at=c(0:5)*0.2,labels=c(0:5)*20)
mtext('Depth (m)',side=2,line=2.5,cex=1.2)
mtext('Proportion of total fine root mass (%)',side=3,line=2.5,cex=1.2)
legend('bottomleft',legend=c('2 year-old','C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=c(1,1:4),pch=c(NA,1,1,4,4),lty=c(NA,1,1,1,1),bg='white',box.col=0,cex=1.2)
box()
par(mar=c(1,4.5,4.5,2))
par(xaxs='i',yaxs='i')
plot(PC113,-DEP,type='b',pch=1,lwd=2,col=1,xlim=c(0,1),ylim=c(-18,0),axes=F,ann=F)
points(PC313, -DEP,type='b',pch=1,col=2,lwd=2)
points(PS113, -DEP,type='b',pch=4,col=3,lwd=2)
points(PS313, -DEP,type='b',pch=4,col=4,lwd=2)
grid(nx=NULL,ny=NA,col=1)
axis(2,at=c(-15,-10,-5,0),labels=c(15,10,5,0))
axis(3,at=c(0:5)*0.2,labels=c(0:5)*20)
mtext('Depth (m)',side=2,line=2.5,cex=1.2)
mtext('Proportion of total fine root mass (%)',side=3,line=2.5,cex=1.2)
legend('bottomleft',legend=c('3 year-old','C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=c(1,1:4),pch=c(NA,1,1,4,4),lty=c(NA,1,1,1,1),bg='white',box.col=0,cex=1.2)
box()




RMD <- dat2$RMD_.g.kg.

require(gss)

X <- seq(min(PROF_MOYENNE),max(PROF_MOYENNE),length.out=100)

x <- runif(100); y <- 5 + 3*sin(2*pi*x) + rnorm(x)
cubic.fit <- ssanova(y~x)
new <- data.frame(x=seq(min(x),max(x),len=50))
est <- predict(cubic.fit,new,se=TRUE)


GF1 <- ssanova(RMD~PROF_MOYENNE)
new <- data.frame(PROF_MOYENNE=seq(min(PROF_MOYENNE),max(PROF_MOYENNE),len=100))
estGF1 <- predict(GF1,new,se=T)

par(mar=c(4,4,1,1))
plot(PROF_MOYENNE,RMD,ylim=c(0,1),log='x')
lines(new$PROF_MOYENNE,estGF1$fit,col=2,lwd=2)
lines(new$PROF_MOYENNE,estGF1$fit+estGF1$se.fit,col=4,lwd=2)
lines(new$PROF_MOYENNE,estGF1$fit-estGF1$se.fit,col=4,lwd=2)
 
rmd <- RMD[Pluie==100]
PROF <- PROF_MOYENNE[Pluie==100]
GF11 <- ssanova(rmd~PROF)
new1 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGF11 <- predict(GF11,new1,se=T)
rmd <- RMD[Pluie==66]
PROF <- PROF_MOYENNE[Pluie==66]
GF12 <- ssanova(rmd~PROF)
new2 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGF12 <- predict(GF12,new2,se=T)

par(mar=c(4,4,1,1))
plot(PROF_MOYENNE,RMD,ylim=c(0,1),log='x')
lines(new$PROF,estGF11$fit,col=2,lwd=2)
lines(new$PROF,estGF11$fit+estGF11$se.fit,col=4,lwd=2)
lines(new$PROF,estGF11$fit-estGF11$se.fit,col=4,lwd=2)
lines(new$PROF,estGF12$fit,col=2,lwd=2,lty=2)
lines(new$PROF,estGF12$fit+estGF12$se.fit,col=4,lwd=2,lty=2)
lines(new$PROF,estGF12$fit-estGF12$se.fit,col=4,lwd=2,lty=2)
legend('topright',legend=c('+W','-W'),pch=NA,lty=1:2,col=2,bty='n',cex=1.2)

rmd <- RMD[Potassium==4.5]
PROF <- PROF_MOYENNE[Potassium==4.5]
GF11 <- ssanova(rmd~PROF)
new1 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGF11 <- predict(GF11,new1,se=T)
rmd <- RMD[Potassium==0]
PROF <- PROF_MOYENNE[Potassium==0]

GF12 <- ssanova(rmd~PROF)

new2 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGF12 <- predict(GF12,new2,se=T)

par(mar=c(4,4,1,1))
plot(PROF_MOYENNE,RMD,ylim=c(0,1),log='x')
lines(new$PROF,estGF11$fit,col=2,lwd=2)
lines(new$PROF,estGF11$fit+estGF11$se.fit,col=4,lwd=2)
lines(new$PROF,estGF11$fit-estGF11$se.fit,col=4,lwd=2)
lines(new$PROF,estGF12$fit,col=2,lwd=2,lty=2)
lines(new$PROF,estGF12$fit+estGF12$se.fit,col=4,lwd=2,lty=2)
lines(new$PROF,estGF12$fit-estGF12$se.fit,col=4,lwd=2,lty=2)
legend('topright',legend=c('+K','-K'),pch=NA,lty=1:2,col=2,bty='n',cex=1.2)



# on regarde tout
PROF1 <- dat1$PROFONDEUR_MOYENNE
PROF2 <- dat2$PROF_MOYENNE
RMD1 <- dat1$DRF_masse_g.kg
RMD2 <- dat2$RMD_.g.kg.
TRAT1 <- dat1$TRAITEMENT
TRAT2 <- dat2$TRAITEMENT



rmd <- RMD1[TRAT1=='C1']
PROF <- PROF1[TRAT1=='C1']
GRMDC1 <- ssanova(rmd~PROF,alpha=0.8)
newC11 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDC11 <- predict(GRMDC1,newC11,se=T)
rmd <- RMD1[TRAT1=='C3']
PROF <- PROF1[TRAT1=='C3']
GRMDC3 <- ssanova(rmd~PROF,alpha=0.8)
newC31 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDC31 <- predict(GRMDC3,newC31,se=T)
rmd <- RMD1[TRAT1=='S1']
PROF <- PROF1[TRAT1=='S1']
GRMDS1 <- ssanova(rmd~PROF,alpha=0.8)
newS11 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDS11 <- predict(GRMDS1,newS11,se=T)
rmd <- RMD1[TRAT1=='S3']
PROF <- PROF1[TRAT1=='S3']
GRMDS3 <- ssanova(rmd~PROF,alpha=0.8)
newS31 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDS31 <- predict(GRMDS3,newS31,se=T)

rmd <- RMD2[TRAT2=='C1']
PROF <- PROF2[TRAT2=='C1']
GRMDC1 <- ssanova(rmd~PROF,alpha=0.8)
newC12 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDC12 <- predict(GRMDC1,newC12,se=T)
rmd <- RMD2[TRAT2=='C3']
PROF <- PROF2[TRAT2=='C3']
GRMDC3 <- ssanova(rmd~PROF,alpha=0.8)
newC32 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDC32 <- predict(GRMDC3,newC32,se=T)
rmd <- RMD2[TRAT2=='S1']
PROF <- PROF2[TRAT2=='S1']
GRMDS1 <- ssanova(rmd~PROF,alpha=0.8)
newS12 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDS12 <- predict(GRMDS1,newS12,se=T)
rmd <- RMD2[TRAT2=='S3']
PROF <- PROF2[TRAT2=='S3']
GRMDS3 <- ssanova(rmd~PROF,alpha=0.8)
newS32 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDS32 <- predict(GRMDS3,newS32,se=T)


rmd <- replace(RMD1[TRAT1=='C1'],c(RMD1[TRAT1=='C1']>0.48),NA)
PROF <- PROF1[TRAT1=='C1']
GRMDC1 <- ssanova(rmd~PROF,alpha=0.8)
newC11 <- data.frame(PROF=seq(min(PROF),max(PROF),len=100))
estGRMDC11 <- predict(GRMDC1,newC11,se=T)

mycols <- adjustcolor(palette(), alpha.f = 0.2)
mysalmon <- adjustcolor('salmon', alpha.f = 0.2)

par(mfrow=c(1,2))
par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(RMD1,-log(PROF1/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRMDC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGRMDC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGRMDS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGRMDS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGRMDC11$fit,rev(estGRMDC11$fit+estGRMDC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC11$fit,rev(estGRMDC11$fit-estGRMDC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC31$fit,rev(estGRMDC31$fit+estGRMDC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDC31$fit,rev(estGRMDC31$fit-estGRMDC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDS11$fit,rev(estGRMDS11$fit+estGRMDS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS11$fit,rev(estGRMDS11$fit-estGRMDS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS31$fit,rev(estGRMDS31$fit+estGRMDS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRMDS31$fit,rev(estGRMDS31$fit-estGRMDS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root mass density (g/kg)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('bottomright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1.2,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()


plot(RMD2,-log(PROF2/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRMDC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGRMDC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGRMDS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGRMDS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGRMDC12$fit,rev(estGRMDC12$fit+estGRMDC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC12$fit,rev(estGRMDC12$fit-estGRMDC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC32$fit,rev(estGRMDC32$fit+estGRMDC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDC32$fit,rev(estGRMDC32$fit-estGRMDC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDS12$fit,rev(estGRMDS12$fit+estGRMDS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS12$fit,rev(estGRMDS12$fit-estGRMDS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS32$fit,rev(estGRMDS32$fit+estGRMDS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRMDS32$fit,rev(estGRMDS32$fit-estGRMDS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root mass density (g/kg)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('bottomright',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1.2,bg='white',box.col=0)
box()

# maintenant rayon

PROF1 <- dat1$PROFONDEUR_MOYENNE
PROF2 <- dat2$PROF_MOYENNE
RAD1 <- dat1$DIAMETRE_MOYEN_mm
RAD2 <- dat2$AvgDiam.mm.
TRAT1 <- dat1$TRAITEMENT
TRAT2 <- dat2$TRAITEMENT

mRAD1 <- c()
mRAD2 <- c()
j = 'S3'
for (i in as.numeric(levels(as.factor(PROF1)))){
mRAD1 <- c(mRAD1,mean(RAD1[TRAT1==j][PROF1[TRAT1==j]==i],na.rm=T))
mRAD2 <- c(mRAD2,mean(RAD2[TRAT2==j][PROF2[TRAT2==j]==i],na.rm=T))
}




RAD <- RAD1[TRAT1=='C1']
PROF <- PROF1[TRAT1=='C1']
GRADC1 <- ssanova(RAD~PROF)
newC11 <- data.frame(PROF=seq(10,700,len=100))
estGRADC11 <- predict(GRADC1,newC11,se=T)
RAD <- RAD1[TRAT1=='C3']
PROF <- PROF1[TRAT1=='C3']
GRADC3 <- ssanova(RAD~PROF,alpha=0.1)
newC31 <- data.frame(PROF=seq(10,800,len=100))
estGRADC31 <- predict(GRADC3,newC31,se=T)
RAD <- RAD1[TRAT1=='S1']
PROF <- PROF1[TRAT1=='S1']
GRADS1 <- ssanova(RAD~PROF)
newS11 <- data.frame(PROF=seq(10,900,len=100))
estGRADS11 <- predict(GRADS1,newS11,se=T)
RAD <- RAD1[TRAT1=='S3']
PROF <- PROF1[TRAT1=='S3']
GRADS3 <- ssanova(RAD~PROF)
newS31 <- data.frame(PROF=seq(10,700,len=100))
estGRADS31 <- predict(GRADS3,newS31,se=T)

RAD <- RAD2[TRAT2=='C1']
PROF <- PROF2[TRAT2=='C1']
GRADC1 <- ssanova(RAD~PROF)
newC12 <- data.frame(PROF=seq(10,1100,len=100))
estGRADC12 <- predict(GRADC1,newC12,se=T)
RAD <- RAD2[TRAT2=='C3']
PROF <- PROF2[TRAT2=='C3']
GRADC3 <- ssanova(RAD~PROF)
newC32 <- data.frame(PROF=seq(10,1100,len=100))
estGRADC32 <- predict(GRADC3,newC32,se=T)
RAD <- RAD2[TRAT2=='S1']
PROF <- PROF2[TRAT2=='S1']
GRADS1 <- ssanova(RAD~PROF)
newS12 <- data.frame(PROF=seq(10,1100,len=100))
estGRADS12 <- predict(GRADS1,newS12,se=T)
RAD <- RAD2[TRAT2=='S3']
PROF <- PROF2[TRAT2=='S3']
GRADS3 <- ssanova(RAD~PROF)
newS32 <- data.frame(PROF=seq(10,1100,len=100))
estGRADS32 <- predict(GRADS3,newS32,se=T)



mycols <- adjustcolor(palette(), alpha.f = 0.2)
mysalmon <- adjustcolor('salmon', alpha.f = 0.2)

par(mfrow=c(1,2))
par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(RAD1,-log(PROF1/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRADC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGRADC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGRADS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGRADS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGRADC11$fit,rev(estGRADC11$fit+estGRADC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC11$fit,rev(estGRADC11$fit-estGRADC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC31$fit,rev(estGRADC31$fit+estGRADC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADC31$fit,rev(estGRADC31$fit-estGRADC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADS11$fit,rev(estGRADS11$fit+estGRADS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS11$fit,rev(estGRADS11$fit-estGRADS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS31$fit,rev(estGRADS31$fit+estGRADS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRADS31$fit,rev(estGRADS31$fit-estGRADS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root radius (mm)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('topright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()


plot(RAD2,-log(PROF2/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRADC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGRADC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGRADS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGRADS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGRADC12$fit,rev(estGRADC12$fit+estGRADC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC12$fit,rev(estGRADC12$fit-estGRADC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC32$fit,rev(estGRADC32$fit+estGRADC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADC32$fit,rev(estGRADC32$fit-estGRADC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADS12$fit,rev(estGRADS12$fit+estGRADS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS12$fit,rev(estGRADS12$fit-estGRADS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS32$fit,rev(estGRADS32$fit+estGRADS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRADS32$fit,rev(estGRADS32$fit-estGRADS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root radius (mm)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('topright',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()




# maintenant SRL

PROF1 <- dat1$PROFONDEUR_MOYENNE
PROF2 <- dat2$PROF_MOYENNE
SRL1 <- dat1$SRL_m.g
SRL2 <- dat2$SRL_mg.1
TRAT1 <- dat1$TRAITEMENT
TRAT2 <- dat2$TRAITEMENT

SRL1 <- replace(SRL1,c(PROF1>1100),NA)
SRL2 <- replace(SRL2,c(PROF2>1100),NA)

SRL <- SRL1[TRAT1=='C1']
PROF <- PROF1[TRAT1=='C1']
GSRLC1 <- ssanova(SRL~PROF)
newC11 <- data.frame(PROF=seq(10,1100,len=100))
estGSRLC11 <- predict(GSRLC1,newC11,se=T)
SRL <- SRL1[TRAT1=='C3']
PROF <- PROF1[TRAT1=='C3']
GSRLC3 <- ssanova(SRL~PROF,alpha=0.1)
newC31 <- data.frame(PROF=seq(10,800,len=100))
estGSRLC31 <- predict(GSRLC3,newC31,se=T)
SRL <- SRL1[TRAT1=='S1']
PROF <- PROF1[TRAT1=='S1']
GSRLS1 <- ssanova(SRL~PROF)
newS11 <- data.frame(PROF=seq(10,900,len=100))
estGSRLS11 <- predict(GSRLS1,newS11,se=T)
SRL <- SRL1[TRAT1=='S3']
PROF <- PROF1[TRAT1=='S3']
GSRLS3 <- ssanova(SRL~PROF)
newS31 <- data.frame(PROF=seq(10,1000,len=100))
estGSRLS31 <- predict(GSRLS3,newS31,se=T)

SRL <- SRL2[TRAT2=='C1']
PROF <- PROF2[TRAT2=='C1']
GSRLC1 <- ssanova(SRL~PROF)
newC12 <- data.frame(PROF=seq(10,1100,len=100))
estGSRLC12 <- predict(GSRLC1,newC12,se=T)
SRL <- SRL2[TRAT2=='C3']
PROF <- PROF2[TRAT2=='C3']
GSRLC3 <- ssanova(SRL~PROF)
newC32 <- data.frame(PROF=seq(10,1100,len=100))
estGSRLC32 <- predict(GSRLC3,newC32,se=T)
SRL <- SRL2[TRAT2=='S1']
PROF <- PROF2[TRAT2=='S1']
GSRLS1 <- ssanova(SRL~PROF)
newS12 <- data.frame(PROF=seq(10,1100,len=100))
estGSRLS12 <- predict(GSRLS1,newS12,se=T)
SRL <- SRL2[TRAT2=='S3']
PROF <- PROF2[TRAT2=='S3']
newS32 <- data.frame(PROF=seq(10,1000,len=100))
estGSRLS32 <- predict(GSRLS3,newS32,se=T)


mycols <- adjustcolor(palette(), alpha.f = 0.2)
mysalmon <- adjustcolor('salmon', alpha.f = 0.2)

par(mfrow=c(1,2))
par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(SRL1,-log(PROF1/100+1),xlim=c(0,80),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRLC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGSRLC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGSRLS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGSRLS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRLC11$fit,rev(estGSRLC11$fit+estGSRLC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC11$fit,rev(estGSRLC11$fit-estGSRLC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC31$fit,rev(estGSRLC31$fit+estGSRLC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLC31$fit,rev(estGSRLC31$fit-estGSRLC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLS11$fit,rev(estGSRLS11$fit+estGSRLS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS11$fit,rev(estGSRLS11$fit-estGSRLS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS31$fit,rev(estGSRLS31$fit+estGSRLS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRLS31$fit,rev(estGSRLS31$fit-estGSRLS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:4)*20,labels=c(0:4)*20,cex.axis=0.8)
mtext('SRL (m/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('topright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()


plot(SRL2,-log(PROF2/100+1),xlim=c(0,80),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRLC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGSRLC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGSRLS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGSRLS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRLC12$fit,rev(estGSRLC12$fit+estGSRLC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC12$fit,rev(estGSRLC12$fit-estGSRLC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC32$fit,rev(estGSRLC32$fit+estGSRLC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLC32$fit,rev(estGSRLC32$fit-estGSRLC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLS12$fit,rev(estGSRLS12$fit+estGSRLS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS12$fit,rev(estGSRLS12$fit-estGSRLS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS32$fit,rev(estGSRLS32$fit+estGSRLS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRLS32$fit,rev(estGSRLS32$fit-estGSRLS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:4)*20,labels=c(0:4)*20,cex.axis=0.8)
mtext('SRL (m/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('right',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()


#SRA
PROF1 <- dat1$PROFONDEUR_MOYENNE
PROF2 <- dat2$PROF_MOYENNE
SRA1 <- dat1$SRA_cm2.g
SRA2 <- dat2$SRA_cm2g.1
TRAT1 <- dat1$TRAITEMENT
TRAT2 <- dat2$TRAITEMENT

SRA1 <- replace(SRA1,c(PROF1>1100),NA)
SRA2 <- replace(SRA2,c(PROF2>1100),NA)

SRA <- SRA1[TRAT1=='C1']
PROF <- PROF1[TRAT1=='C1']
GSRAC1 <- ssanova(SRA~PROF)
newC11 <- data.frame(PROF=seq(10,1100,len=100))
estGSRAC11 <- predict(GSRAC1,newC11,se=T)
SRA <- SRA1[TRAT1=='C3']
PROF <- PROF1[TRAT1=='C3']
GSRAC3 <- ssanova(SRA~PROF,alpha=0.1)
newC31 <- data.frame(PROF=seq(10,800,len=100))
estGSRAC31 <- predict(GSRAC3,newC31,se=T)
SRA <- SRA1[TRAT1=='S1']
PROF <- PROF1[TRAT1=='S1']
GSRAS1 <- ssanova(SRA~PROF)
newS11 <- data.frame(PROF=seq(10,900,len=100))
estGSRAS11 <- predict(GSRAS1,newS11,se=T)
SRA <- SRA1[TRAT1=='S3']
PROF <- PROF1[TRAT1=='S3']
GSRAS3 <- ssanova(SRA~PROF)
newS31 <- data.frame(PROF=seq(10,1000,len=100))
estGSRAS31 <- predict(GSRAS3,newS31,se=T)

SRA <- SRA2[TRAT2=='C1']
PROF <- PROF2[TRAT2=='C1']
GSRAC1 <- ssanova(SRA~PROF)
newC12 <- data.frame(PROF=seq(10,1100,len=100))
estGSRAC12 <- predict(GSRAC1,newC12,se=T)
SRA <- SRA2[TRAT2=='C3']
PROF <- PROF2[TRAT2=='C3']
GSRAC3 <- ssanova(SRA~PROF)
newC32 <- data.frame(PROF=seq(10,1100,len=100))
estGSRAC32 <- predict(GSRAC3,newC32,se=T)
SRA <- SRA2[TRAT2=='S1']
PROF <- PROF2[TRAT2=='S1']
GSRAS1 <- ssanova(SRA~PROF)
newS12 <- data.frame(PROF=seq(10,1100,len=100))
estGSRAS12 <- predict(GSRAS1,newS12,se=T)
SRA <- SRA2[TRAT2=='S3']
PROF <- PROF2[TRAT2=='S3']
newS32 <- data.frame(PROF=seq(10,1000,len=100))
estGSRAS32 <- predict(GSRAS3,newS32,se=T)


mycols <- adjustcolor(palette(), alpha.f = 0.2)
mysalmon <- adjustcolor('salmon', alpha.f = 0.2)

par(mfrow=c(1,2))
par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(SRA1,-log(PROF1/100+1),xlim=c(0,1000),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRAC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGSRAC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGSRAS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGSRAS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRAC11$fit,rev(estGSRAC11$fit+estGSRAC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC11$fit,rev(estGSRAC11$fit-estGSRAC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC31$fit,rev(estGSRAC31$fit+estGSRAC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAC31$fit,rev(estGSRAC31$fit-estGSRAC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAS11$fit,rev(estGSRAS11$fit+estGSRAS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS11$fit,rev(estGSRAS11$fit-estGSRAS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS31$fit,rev(estGSRAS31$fit+estGSRAS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRAS31$fit,rev(estGSRAS31$fit-estGSRAS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*200,labels=c(0:5)*200,cex.axis=0.8)
mtext('SRA (cm�/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('right',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()


plot(SRA2,-log(PROF2/100+1),xlim=c(0,1000),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRAC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGSRAC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGSRAS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGSRAS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRAC12$fit,rev(estGSRAC12$fit+estGSRAC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC12$fit,rev(estGSRAC12$fit-estGSRAC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC32$fit,rev(estGSRAC32$fit+estGSRAC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAC32$fit,rev(estGSRAC32$fit-estGSRAC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAS12$fit,rev(estGSRAS12$fit+estGSRAS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS12$fit,rev(estGSRAS12$fit-estGSRAS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS32$fit,rev(estGSRAS32$fit+estGSRAS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRAS32$fit,rev(estGSRAS32$fit-estGSRAS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*200,labels=c(0:5)*200,cex.axis=0.8)
mtext('SRA (cm�/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('right',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()





RADC112 <- c(0.4004541,0.4713182,0.4267480,0.4261333,0.4113667,0.4244583,0.5129556,0.4617056,0.3474500,0.4590500,0.7393000)/1000
RADC113 <- c(0.3451750,0.3877500,0.4521250,0.3885000,0.4161000,0.5359333,0.5866667,0.5205333,0.5698000,0.5181333,0.4909000,0.3598500,0.6034000,0.5151500,0.6005000)/1000
RADC312 <- c(0.3436368,0.3744491,0.3924873,0.4151333,0.4125583,0.5552500,0.5154000,0.6928100,0.4394000,0.5051250,0.2442875,0.5161500)/1000
RADC313 <- c(0.3524000,0.3281250,0.3771250,0.4053500,0.4419750,0.6448333,0.6533333,0.6633333,0.7277000,0.6557667,0.5315333,0.4722000,0.5308000,0.6906000,0.2433000)/1000
RADS112 <- c(0.3211966,0.3510717,0.3518047,0.3486167,0.3765917,0.4232000,0.5207500,0.3559500,0.3798000,0.4893500,0.5368000,0.58,0.6302000)/1000
RADS113 <- c(0.3972000,0.3762333,0.3774833,0.3602167,0.4086667,0.5609000,0.6641667,0.6139333,0.4337000,0.4979333,0.5444000)/1000
RADS312 <- c(0.3043685,0.3639766,0.3575162,0.3347633,0.4016900,0.3259333,0.2403000,0.4036000,0.5772000,0.4668000,0.4382000)/1000
RADS313 <- c(0.3164500,0.3818250,0.3812000,0.4134750,0.4574000,0.7009667,0.8183333,0.5616667,0.5765667,0.6297667,0.5597667,0.5947667,0.5649000,0.5450000,0.8388000)/1000


SRLC112 <- c() ; SRLC113 <- c() ; SRLC312 <- c() ; SRLC313 <- c() ; SRLS112 <- c() ; SRLS113 <- c() ; SRLS312 <- c() ; SRLS313 <- c() ; 
j = 'C1'
for (i in as.numeric(levels(as.factor(PROF1)))){
SRLC112 <- c(SRLC112,mean(SRL1[TRAT1=='C1'][PROF1[TRAT1=='C1']==i],na.rm=T))
SRLC113 <- c(SRLC113,mean(SRL2[TRAT2=='C1'][PROF2[TRAT2=='C1']==i],na.rm=T))
SRLC312 <- c(SRLC312,mean(SRL1[TRAT1=='C3'][PROF1[TRAT1=='C3']==i],na.rm=T))
SRLC313 <- c(SRLC313,mean(SRL2[TRAT2=='C3'][PROF2[TRAT2=='C3']==i],na.rm=T))
SRLS112 <- c(SRLS112,mean(SRL1[TRAT1=='S1'][PROF1[TRAT1=='S1']==i],na.rm=T))
SRLS113 <- c(SRLS113,mean(SRL2[TRAT2=='S1'][PROF2[TRAT2=='S1']==i],na.rm=T))
SRLS312 <- c(SRLS312,mean(SRL1[TRAT1=='S3'][PROF1[TRAT1=='S3']==i],na.rm=T))
SRLS313 <- c(SRLS313,mean(SRL2[TRAT2=='S3'][PROF2[TRAT2=='S3']==i],na.rm=T))
}

Dens18 <- 1/(SRL_mg[age==18] * pi * Rad18^2)

DENSC112 <- 1/(SRLC112 * pi * (c(RADC112,rep(RADC112[11],4)))^2)
DENSC113 <- 1/(SRLC113 * pi * (c(RADC113))^2)
DENSC312 <- 1/(SRLC312 * pi * (c(RADC312,rep(RADC312[12],3)))^2)
DENSC313 <- 1/(SRLC313 * pi * (c(RADC313))^2)
DENSS112 <- 1/(SRLS112 * pi * (c(RADS112,rep(RADS112[13],2)))^2)
DENSS113 <- 1/(SRLS113 * pi * (c(RADS113,rep(RADS113[11],4)))^2)
DENSS312 <- 1/(SRLS312 * pi * (c(RADS312,rep(RADS312[11],4)))^2)
DENSS313 <- 1/(SRLS313 * pi * (c(RADS313))^2)
 














# alll figure
par(mfrow=c(2,4))
par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(RMD1,-log(PROF1/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRMDC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGRMDC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGRMDS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGRMDS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGRMDC11$fit,rev(estGRMDC11$fit+estGRMDC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC11$fit,rev(estGRMDC11$fit-estGRMDC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC31$fit,rev(estGRMDC31$fit+estGRMDC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDC31$fit,rev(estGRMDC31$fit-estGRMDC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDS11$fit,rev(estGRMDS11$fit+estGRMDS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS11$fit,rev(estGRMDS11$fit-estGRMDS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS31$fit,rev(estGRMDS31$fit+estGRMDS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRMDS31$fit,rev(estGRMDS31$fit-estGRMDS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root mass density (g/kg)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('bottomright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1.2,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()

par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(RAD1,-log(PROF1/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRADC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGRADC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGRADS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGRADS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGRADC11$fit,rev(estGRADC11$fit+estGRADC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC11$fit,rev(estGRADC11$fit-estGRADC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC31$fit,rev(estGRADC31$fit+estGRADC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADC31$fit,rev(estGRADC31$fit-estGRADC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADS11$fit,rev(estGRADS11$fit+estGRADS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS11$fit,rev(estGRADS11$fit-estGRADS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS31$fit,rev(estGRADS31$fit+estGRADS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRADS31$fit,rev(estGRADS31$fit-estGRADS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root radius (mm)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('topright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()

par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(SRL1,-log(PROF1/100+1),xlim=c(0,80),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRLC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGSRLC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGSRLS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGSRLS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRLC11$fit,rev(estGSRLC11$fit+estGSRLC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC11$fit,rev(estGSRLC11$fit-estGSRLC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC31$fit,rev(estGSRLC31$fit+estGSRLC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLC31$fit,rev(estGSRLC31$fit-estGSRLC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLS11$fit,rev(estGSRLS11$fit+estGSRLS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS11$fit,rev(estGSRLS11$fit-estGSRLS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS31$fit,rev(estGSRLS31$fit+estGSRLS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRLS31$fit,rev(estGSRLS31$fit-estGSRLS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:4)*20,labels=c(0:4)*20,cex.axis=0.8)
mtext('SRL (m/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('topright',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()

par(mar=c(1,4,4,1))
par(xaxs='i',yaxs='i')
plot(SRA1,-log(PROF1/100+1),xlim=c(0,1000),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRAC11$fit,-log(newC11$PROF/100+1),col=5,lwd=2)
lines(estGSRAC31$fit,-log(newC31$PROF/100+1),col=4,lwd=2)
lines(estGSRAS11$fit,-log(newS11$PROF/100+1),col='salmon',lwd=2)
lines(estGSRAS31$fit,-log(newS31$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRAC11$fit,rev(estGSRAC11$fit+estGSRAC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC11$fit,rev(estGSRAC11$fit-estGSRAC11$se.fit)),-log(c(newC11$PROF,rev(newC11$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC31$fit,rev(estGSRAC31$fit+estGSRAC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAC31$fit,rev(estGSRAC31$fit-estGSRAC31$se.fit)),-log(c(newC31$PROF,rev(newC31$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAS11$fit,rev(estGSRAS11$fit+estGSRAS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS11$fit,rev(estGSRAS11$fit-estGSRAS11$se.fit)),-log(c(newS11$PROF,rev(newS11$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS31$fit,rev(estGSRAS31$fit+estGSRAS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRAS31$fit,rev(estGSRAS31$fit-estGSRAS31$se.fit)),-log(c(newS31$PROF,rev(newS31$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*200,labels=c(0:5)*200,cex.axis=0.8)
mtext('SRA (cm�/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
legend('right',legend=c('2 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
grid(nx=NULL,ny=NA)
box()


plot(RMD2,-log(PROF2/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRMDC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGRMDC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGRMDS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGRMDS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGRMDC12$fit,rev(estGRMDC12$fit+estGRMDC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC12$fit,rev(estGRMDC12$fit-estGRMDC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRMDC32$fit,rev(estGRMDC32$fit+estGRMDC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDC32$fit,rev(estGRMDC32$fit-estGRMDC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRMDS12$fit,rev(estGRMDS12$fit+estGRMDS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS12$fit,rev(estGRMDS12$fit-estGRMDS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRMDS32$fit,rev(estGRMDS32$fit+estGRMDS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRMDS32$fit,rev(estGRMDS32$fit-estGRMDS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root mass density (g/kg)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('bottomright',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1.2,bg='white',box.col=0)
box()



plot(RAD2,-log(PROF2/100+1),xlim=c(0,1),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGRADC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGRADC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGRADS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGRADS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGRADC12$fit,rev(estGRADC12$fit+estGRADC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC12$fit,rev(estGRADC12$fit-estGRADC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGRADC32$fit,rev(estGRADC32$fit+estGRADC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADC32$fit,rev(estGRADC32$fit-estGRADC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGRADS12$fit,rev(estGRADS12$fit+estGRADS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS12$fit,rev(estGRADS12$fit-estGRADS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGRADS32$fit,rev(estGRADS32$fit+estGRADS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGRADS32$fit,rev(estGRADS32$fit-estGRADS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*0.2,labels=c(0:5)*0.2,cex.axis=0.8)
mtext('Root radius (mm)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('topright',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()

plot(SRL2,-log(PROF2/100+1),xlim=c(0,80),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRLC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGSRLC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGSRLS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGSRLS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRLC12$fit,rev(estGSRLC12$fit+estGSRLC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC12$fit,rev(estGSRLC12$fit-estGSRLC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRLC32$fit,rev(estGSRLC32$fit+estGSRLC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLC32$fit,rev(estGSRLC32$fit-estGSRLC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRLS12$fit,rev(estGSRLS12$fit+estGSRLS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS12$fit,rev(estGSRLS12$fit-estGSRLS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRLS32$fit,rev(estGSRLS32$fit+estGSRLS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRLS32$fit,rev(estGSRLS32$fit-estGSRLS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:4)*20,labels=c(0:4)*20,cex.axis=0.8)
mtext('SRL (m/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('right',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()

plot(SRA2,-log(PROF2/100+1),xlim=c(0,1000),ylim=c(-log(12),0),axes=F,ann=F)
lines(estGSRAC12$fit,-log(newC12$PROF/100+1),col=5,lwd=2)
lines(estGSRAC32$fit,-log(newC32$PROF/100+1),col=4,lwd=2)
lines(estGSRAS12$fit,-log(newS12$PROF/100+1),col='salmon',lwd=2)
lines(estGSRAS32$fit,-log(newS32$PROF/100+1),col=2,lwd=2)
polygon(c(estGSRAC12$fit,rev(estGSRAC12$fit+estGSRAC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC12$fit,rev(estGSRAC12$fit-estGSRAC12$se.fit)),-log(c(newC12$PROF,rev(newC12$PROF))/100+1),col=mycols[5],border=NA)
polygon(c(estGSRAC32$fit,rev(estGSRAC32$fit+estGSRAC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAC32$fit,rev(estGSRAC32$fit-estGSRAC32$se.fit)),-log(c(newC32$PROF,rev(newC32$PROF))/100+1),col=mycols[4],border=NA)
polygon(c(estGSRAS12$fit,rev(estGSRAS12$fit+estGSRAS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS12$fit,rev(estGSRAS12$fit-estGSRAS12$se.fit)),-log(c(newS12$PROF,rev(newS12$PROF))/100+1),col=mysalmon,border=NA)
polygon(c(estGSRAS32$fit,rev(estGSRAS32$fit+estGSRAS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
polygon(c(estGSRAS32$fit,rev(estGSRAS32$fit-estGSRAS32$se.fit)),-log(c(newS32$PROF,rev(newS32$PROF))/100+1),col=mycols[2],border=NA)
prof <- as.numeric(levels(as.factor(PROF1)))/100
axis(side=2,at=-log(prof+1),labels=prof,cex.axis=0.8)
axis(side=3,at=c(0:5)*200,labels=c(0:5)*200,cex.axis=0.8)
mtext('SRA (cm�/g)',side=3,line=2.5)
mtext('Depth(m)',side=2,line=2.5)
grid(nx=NULL,ny=NA)
legend('right',legend=c('3 years-old','+K +W','- K +W','+K - W','- K - W'),pch=NA,lty=c(NA,1,1,1,1),col=c(1,4,5,2,'salmon'),cex=1,bg='white',box.col=0)
box()



# analyse sta

require(nlme)

AGE <- c( rep('2012',384),rep('2013',409))
RMD <- c(RMD1,RMD2)
RAD <- c(RAD1,RAD2)
SRL <- c(SRL1,SRL2)
SRA <- c(SRA1,SRA2)
POS <- c(dat1$POSITION,dat2$Position)
POS1 <- dat1$POSITION; POS2 <- dat2$Position
WEFF <- c(dat1$Pluie, dat2$Pluie)
KEFF <- c(dat1$Potassium, dat2$Potassium)
AGE <- c(rep(2,length(RMD1)),rep(3,length(RMD2)))
BLOC1 <- dat1$BLOC
BLOC2 <- dat2$BLOC
BLOC <- c(BLOC1, BLOC2)

ana1 <- lme(RMD~WEFF*KEFF*AGE,random=~1+1|BLOC/WEFF,na.action=na.omit)
ana2 <- lme(RAD~WEFF*KEFF*AGE,random=~1+1|BLOC/WEFF,na.action=na.omit)
ana3 <- lme(SRL~WEFF*KEFF*AGE,random=~1+1|BLOC/WEFF,na.action=na.omit)


WEFF1 <-c();WEFF2 <-c(); KEFF1 <-c(); KEFF2 <-c()
for (i in 1:length(TRAT1)){
if (TRAT1[i]=='C3'|TRAT1[i]=='C1'){ WEFF1 <- c(WEFF1, '+W')}
if (TRAT1[i]=='S3'|TRAT1[i]=='S1'){ WEFF1 <- c(WEFF1, '-W')}
if (TRAT1[i]=='C3'|TRAT1[i]=='S3'){ KEFF1 <- c(KEFF1, '+K')}
if (TRAT1[i]=='C1'|TRAT1[i]=='S1'){ KEFF1 <- c(KEFF1, '-W')}}
for (i in 1:length(TRAT2)){
if (TRAT2[i]=='C3'|TRAT2[i]=='C1'){ WEFF2 <- c(WEFF2, '+W')}
if (TRAT2[i]=='S3'|TRAT2[i]=='S1'){ WEFF2 <- c(WEFF2, '-W')}
if (TRAT2[i]=='C3'|TRAT2[i]=='S3'){ KEFF2 <- c(KEFF2, '+K')}
if (TRAT2[i]=='C1'|TRAT2[i]=='S1'){ KEFF2 <- c(KEFF2, '-W')}}






ana11 <- lme(RMD1~WEFF1*KEFF1*PROF1,random=~1+1|BLOC1/WEFF1,na.action=na.omit)
ana12 <- lme(RMD2~WEFF2*KEFF2*PROF2,random=~1+1|BLOC2/WEFF2,na.action=na.omit)

ana21 <- lme(RAD1~WEFF1*KEFF1*PROF1,random=~1+1|BLOC1/WEFF1,na.action=na.omit)
ana22 <- lme(RAD2~WEFF2*KEFF2*PROF2,random=~1+1|BLOC2/WEFF2,na.action=na.omit)

ana31 <- lme(SRL1~WEFF1*KEFF1*PROF1,random=~1+1|BLOC1/WEFF1,na.action=na.omit)
ana32 <- lme(SRL2~WEFF2*KEFF2*PROF2,random=~1+1|BLOC2/WEFF2,na.action=na.omit)

ana41 <- lme(SRA1~WEFF1*KEFF1*PROF1,random=~1+1|BLOC1/WEFF1,na.action=na.omit)
ana42 <- lme(SRA2~WEFF2*KEFF2*PROF2,random=~1+1|BLOC2/WEFF2,na.action=na.omit)


ana12 <-  lme(ATOT~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit)

ana1 <- gls(RMD1~WEFF1*KEFF1*PROF1,na.action=na.omit)

ana21 <-  gls(Vcmax~WEFF*KEFF*YEA,method='ML',na.action=na.omit,  weights=varIdent(form= ~ 1|trat) )







