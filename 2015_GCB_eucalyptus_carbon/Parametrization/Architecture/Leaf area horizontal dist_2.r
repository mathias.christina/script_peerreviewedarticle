set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Effet des traitements/Leaf area distribution")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

DAT <- read.table('DIST_HOR.txt',header=T)
DAT2 <- read.table('DIST_HORIZ_2013.txt',header=T)

DIST1<-rep(NA,357*3)
for (i in 1:12){
DIST1 <- data.frame(DIST1, c(DAT[,(3*(i-1)+1)]/max(DAT[,(3*(i-1)+1)],na.rm=T),
                           DAT[,(3*(i-1)+2)]/max(DAT[,(3*(i-1)+2)],na.rm=T),
                           DAT[,(3*(i))]/max(DAT[,(3*i)],na.rm=T)))}  ; DIST1 <- DIST1[,2:13]
DIST2<-rep(NA,352*3)
for (i in 1:12){
DIST2 <- data.frame(DIST2, c(DAT2[,(3*(i-1)+1)]/max(DAT2[,(3*(i-1)+1)],na.rm=T),
                           DAT2[,(3*(i-1)+2)]/max(DAT2[,(3*(i-1)+2)],na.rm=T),
                           DAT2[,(3*(i))]/max(DAT2[,(3*i)],na.rm=T)))}  ; DIST2 <- DIST2[,2:13]


Dall1 <- c()
for (i in 1:12){
Dall1 <- c(Dall1, hist(DIST1[,i],breaks=seq(0,1,0.1))$density)}
TRAT1 <- c(rep('C1',30),rep('S3',30),rep('C3',30),rep('S1',30))
TT1 <- rep(c(rep('S',10),rep('M',10),rep('B',10)),4)

Dall2 <- c()
for (i in 1:12){
Dall2 <- c(Dall2, hist(DIST2[,i],breaks=seq(0,1,0.1))$density)}
TRAT2 <- c(rep('C1',30),rep('C3',30),rep('S1',30),rep('S3',30))
TT2 <- rep(c(rep('S',10),rep('M',10),rep('B',10)),4)

Dall <- c(Dall1,Dall2)
TRAT <- c(TRAT1,TRAT2)
TT <- c(TT1,TT2)
AGE <- c(rep('2012',120),rep('2013',120))

# effet traitement

R1 <- rep(seq(0.05,0.95,0.1),24)
fit0 <- nls(Dall/(2*pi) ~  a*R1^(b+1)*(1-R1)^d, start=c(a=1,b=1.2,d=1.1))

DC1_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C1']
DC3_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C3']
DS1_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='S1']
DS3_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='S3']
R2 <- rep(seq(0.05,0.95,0.1),3)
fit1 <- nls(DC1_12/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit2 <- nls(DC3_12/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit3 <- nls(DS1_12/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit4 <- nls(DS3_12/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
DC1_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C1']
DC3_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C3']
DS1_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='S1']
DS3_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='S3']
R2 <- rep(seq(0.05,0.95,0.1),3)
fit5 <- nls(DC1_13/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit6 <- nls(DC3_13/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit7 <- nls(DS1_13/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit8 <- nls(DS3_13/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))

COEFC1 <- c(summary(fit1)$coef[,1],summary(fit5)$coef[,1])
COEFC3 <- c(summary(fit2)$coef[,1],summary(fit6)$coef[,1])
COEFS1 <- c(summary(fit3)$coef[,1],summary(fit7)$coef[,1])
COEFS3 <- c(summary(fit4)$coef[,1],summary(fit8)$coef[,1])
DATE <- c('2012','2012','2012','2013','2013','2013')
MAT <- data.frame(DATE,COEFC1,COEFC3,COEFS1,COEFS3)
write.table(MAT,'Horiz_LAD.txt',col.names=T,row.names=F)

COEF12 <- summary(fit01)$coef[,1]
COEF13 <- summary(fit02)$coef[,1]
COEFAGE <- c(COEF12,COEF13)
MAT <- data.frame(DATE,COEFC1,COEFC3,COEFS1,COEFS3,COEFAGE)
write.table(MAT,'Horiz_LAD.txt',col.names=T,row.names=F)

LADfun <- function (H,a,b,d){a*H^(b+1) *(1-H)^d}
MAT2 <- MAT
for (i in 2:6){
MAT2[1,i] <- MAT[1,i]*(1/(2*pi))/integrate(LADfun,lower=0,upper=1,a=MAT[1,i],b=MAT[2,i],d=MAT[3,i])$value
MAT2[4,i] <- MAT[4,i]*(1/(2*pi))/integrate(LADfun,lower=0,upper=1,a=MAT[4,i],b=MAT[5,i],d=MAT[6,i])$value
}

write.table(MAT2,'Horiz_LAD.txt',col.names=T,row.names=F)


par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(0.5,0.5, xlab='Relative distance to trunk',ylab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,0.4),cex.lab=1.2)
points(R2,DC1_12/(2*pi),col=1)
X <- seq(0,1,0.01)
points(X,summary(fit1)$coef[1,1] * X ^(summary(fit1)$coef[2,1]+1) * (1-X) ^summary(fit1)$coef[3,1],type='l',lwd=2,col=1)
points(R2,DC3_12/(2*pi),col=1)
X <- seq(0,1,0.01)
points(X,summary(fit2)$coef[1,1] * X ^(summary(fit2)$coef[2,1]+1) * (1-X) ^summary(fit2)$coef[3,1],type='l',lwd=2,col=2)
points(R2,DS1_12/(2*pi),col=1)
X <- seq(0,1,0.01)
points(X,summary(fit3)$coef[1,1] * X ^(summary(fit3)$coef[2,1]+1) * (1-X) ^summary(fit3)$coef[3,1],type='l',lwd=2,col=3)
points(R2,DS3_12/(2*pi),col=1)
X <- seq(0,1,0.01)
points(X,summary(fit4)$coef[1,1] * X ^(summary(fit4)$coef[2,1]+1) * (1-X) ^summary(fit4)$coef[3,1],type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1','C3','S1','S3'),pch=1,lty=1,col=1:4,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(paste('AIC = ',round(aic3,1)),paste('BIC = ',round(bic3,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()

A0 <- summary(fit0)$coef[1,1] * R1 ^(summary(fit0)$coef[2,1]+1) * (1-R1) ^summary(fit0)$coef[3,1]*2*pi
AC1_12 <- summary(fit1)$coef[1,1] * R2 ^(summary(fit1)$coef[2,1]+1) * (1-R2) ^summary(fit1)$coef[3,1]*2*pi
AC3_12 <- summary(fit2)$coef[1,1] * R2 ^(summary(fit2)$coef[2,1]+1) * (1-R2) ^summary(fit2)$coef[3,1]*2*pi
AS1_12 <- summary(fit3)$coef[1,1] * R2 ^(summary(fit3)$coef[2,1]+1) * (1-R2) ^summary(fit3)$coef[3,1]*2*pi
AS3_12 <- summary(fit4)$coef[1,1] * R2 ^(summary(fit4)$coef[2,1]+1) * (1-R2) ^summary(fit4)$coef[3,1]*2*pi
AC1_13 <- summary(fit5)$coef[1,1] * R2 ^(summary(fit5)$coef[2,1]+1) * (1-R2) ^summary(fit5)$coef[3,1]*2*pi
AC3_13 <- summary(fit6)$coef[1,1] * R2 ^(summary(fit6)$coef[2,1]+1) * (1-R2) ^summary(fit6)$coef[3,1]*2*pi
AS1_13 <- summary(fit7)$coef[1,1] * R2 ^(summary(fit7)$coef[2,1]+1) * (1-R2) ^summary(fit7)$coef[3,1]*2*pi
AS3_13 <- summary(fit8)$coef[1,1] * R2 ^(summary(fit8)$coef[2,1]+1) * (1-R2) ^summary(fit8)$coef[3,1]*2*pi

aic0 <- AIC2(240,A0,Dall,1,3)
aic1 <- AIC2(240,c(AC1_12,AC3_12,AS1_12,AS3_12,AC1_13,AC3_13,AS1_13,AS3_13),c(DC1_12,DC3_12,DS1_12,DS3_12,DC1_13,DC3_13,DS1_13,DS3_13),1,24)
bic0 <- BIC2(240,A0,Dall,1,3)
bic1 <- BIC2(240,c(AC1_12,AC3_12,AS1_12,AS3_12,AC1_13,AC3_13,AS1_13,AS3_13),c(DC1_12,DC3_12,DS1_12,DS3_12,DC1_13,DC3_13,DS1_13,DS3_13),1,24)

D12 <- Dall[AGE=='2012']
D13 <- Dall[AGE=='2013']
RI <- rep(seq(0.05,0.95,0.1),12)
fit01 <- nls(D12/(2*pi) ~  a*RI^(b+1)*(1-RI)^d, start=c(a=1,b=1.2,d=1.1))
fit02 <- nls(D13/(2*pi) ~  a*RI^(b+1)*(1-RI)^d, start=c(a=1,b=1.2,d=1.1))
A01 <- summary(fit01)$coef[1,1] * RI ^(summary(fit01)$coef[2,1]+1) * (1-RI) ^summary(fit01)$coef[3,1]*2*pi
A02 <- summary(fit02)$coef[1,1] * RI ^(summary(fit02)$coef[2,1]+1) * (1-RI) ^summary(fit02)$coef[3,1]*2*pi
aic01 <- AIC2(240,c(A01,A02),c(D12,D13),1,6)
bic01 <- BIC2(240,c(A01,A02),c(D12,D13),1,6)


DC1 <- Dall[TRAT=='C1']
DC3 <- Dall[TRAT=='C3']
DS1 <- Dall[TRAT=='S1']
DS3 <- Dall[TRAT=='S3']
RA <- rep(seq(0.05,0.95,0.1),6)
fit11 <- nls(DC1/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit21 <- nls(DC3/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit31 <- nls(DS1/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit41 <- nls(DS3/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
AC1 <- summary(fit11)$coef[1,1] * RA ^(summary(fit11)$coef[2,1]+1) * (1-RA) ^summary(fit11)$coef[3,1]*2*pi
AC3 <- summary(fit21)$coef[1,1] * RA ^(summary(fit21)$coef[2,1]+1) * (1-RA) ^summary(fit21)$coef[3,1]*2*pi
AS1 <- summary(fit31)$coef[1,1] * RA ^(summary(fit31)$coef[2,1]+1) * (1-RA) ^summary(fit31)$coef[3,1]*2*pi
AS3 <- summary(fit41)$coef[1,1] * RA ^(summary(fit41)$coef[2,1]+1) * (1-RA) ^summary(fit41)$coef[3,1]*2*pi
aic11 <- AIC2(240,c(AC1,AC3,AS1,AS3),c(DC1,DC3,DS1,DS3),1,12)
bic11 <- BIC2(240,c(AC1,AC3,AS1,AS3),c(DC1,DC3,DS1,DS3),1,12)

DC1C3 <- Dall[TRAT=='C1'|TRAT=='C3']
DS1S3 <- Dall[TRAT=='S1'|TRAT=='S3']
DC1S1 <- Dall[TRAT=='C1'|TRAT=='S1']
DC3S3 <- Dall[TRAT=='C3'|TRAT=='S3']
RA <- rep(seq(0.05,0.95,0.1),12)
fit21 <- nls(DC1C3/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit22 <- nls(DS1S3/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit23 <- nls(DC1S1/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
fit24 <- nls(DC3S3/(2*pi) ~  a*RA^(b+1)*(1-RA)^d, start=c(a=1,b=1.2,d=1.1))
AC1C3 <- summary(fit21)$coef[1,1] * RA ^(summary(fit21)$coef[2,1]+1) * (1-RA) ^summary(fit21)$coef[3,1]*2*pi
AS1S3 <- summary(fit22)$coef[1,1] * RA ^(summary(fit22)$coef[2,1]+1) * (1-RA) ^summary(fit22)$coef[3,1]*2*pi
AC1S1 <- summary(fit23)$coef[1,1] * RA ^(summary(fit23)$coef[2,1]+1) * (1-RA) ^summary(fit23)$coef[3,1]*2*pi
AC3S3 <- summary(fit24)$coef[1,1] * RA ^(summary(fit24)$coef[2,1]+1) * (1-RA) ^summary(fit24)$coef[3,1]*2*pi
aic21 <- AIC2(240,c(AC1C3,AS1S3),c(DC1C3,DS1S3),1,6)
bic21 <- BIC2(240,c(AC1C3,AS1S3),c(DC1C3,DS1S3),1,6)
aic22 <- AIC2(240,c(AC1S1,AC3S3),c(DC1S1,DC3S3),1,6)
bic22 <- BIC2(240,c(AC1S1,AC3S3),c(DC1S1,DC3S3),1,6)

# gardons les ages s�par�s

DC1C3_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C1'|TRAT[AGE=='2012']=='C3']
DC1C3_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C1'|TRAT[AGE=='2013']=='C3']
R3 <- rep(seq(0.05,0.95,0.1),6)
fit51 <- nls(DC1C3_12/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
fit52 <- nls(DC1C3_13/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
AC1C3_12 <- summary(fit51)$coef[1,1] * R3 ^(summary(fit51)$coef[2,1]+1) * (1-R3) ^summary(fit51)$coef[3,1]*2*pi
AC1C3_13 <- summary(fit52)$coef[1,1] * R3 ^(summary(fit52)$coef[2,1]+1) * (1-R3) ^summary(fit52)$coef[3,1]*2*pi
aic2 <- AIC2(240,c(AC1C3_12,AS1_12,AS3_12,AC1C3_13,AS1_13,AS3_13),c(DC1C3_12,DS1_12,DS3_12,DC1C3_13,DS1_13,DS3_13),1,18)
bic2 <- BIC2(240,c(AC1C3_12,AS1_12,AS3_12,AC1C3_13,AS1_13,AS3_13),c(DC1C3_12,DS1_12,DS3_12,DC1C3_13,DS1_13,DS3_13),1,18)

DS1S3_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='S1'|TRAT[AGE=='2012']=='S3']
DS1S3_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='S1'|TRAT[AGE=='2013']=='S3']
R3 <- rep(seq(0.05,0.95,0.1),6)
fit61 <- nls(DS1S3_12/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
fit62 <- nls(DS1S3_13/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
AS1S3_12 <- summary(fit61)$coef[1,1] * R3 ^(summary(fit61)$coef[2,1]+1) * (1-R3) ^summary(fit61)$coef[3,1]*2*pi
AS1S3_13 <- summary(fit62)$coef[1,1] * R3 ^(summary(fit62)$coef[2,1]+1) * (1-R3) ^summary(fit62)$coef[3,1]*2*pi
aic3 <- AIC2(240,c(AC1C3_12,AS1S3_12,AC1C3_13,AS1S3_13),c(DC1C3_12,DS1S3_12,DC1C3_13,DS1S3_13),1,12)
bic3 <- BIC2(240,c(AC1C3_12,AS1S3_12,AC1C3_13,AS1S3_13),c(DC1C3_12,DS1S3_12,DC1C3_13,DS1S3_13),1,12)

R4 <- rep(seq(0.05,0.95,0.1),6)
DC3S3_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C3'|TRAT[AGE=='2012']=='S3']
DC3S3_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C3'|TRAT[AGE=='2013']=='S3']
DC1S1_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C1'|TRAT[AGE=='2012']=='S1']
DC1S1_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C1'|TRAT[AGE=='2013']=='S1']
fit71 <- nls(DC3S3_12/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
fit72 <- nls(DC3S3_13/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
fit73 <- nls(DC1S1_12/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
fit74 <- nls(DC1S1_13/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
AC3S3_12 <- summary(fit71)$coef[1,1] * R4 ^(summary(fit71)$coef[2,1]+1) * (1-R4) ^summary(fit71)$coef[3,1]*2*pi
AC3S3_13 <- summary(fit72)$coef[1,1] * R4 ^(summary(fit72)$coef[2,1]+1) * (1-R4) ^summary(fit72)$coef[3,1]*2*pi
AC1S1_12 <- summary(fit73)$coef[1,1] * R4 ^(summary(fit73)$coef[2,1]+1) * (1-R4) ^summary(fit73)$coef[3,1]*2*pi
AC1S1_13 <- summary(fit74)$coef[1,1] * R4 ^(summary(fit74)$coef[2,1]+1) * (1-R4) ^summary(fit74)$coef[3,1]*2*pi
aic4 <- AIC2(240,c(AC1S1_12,AC3S3_12,AC1S1_13,AC3S3_13),c(DC1S1_12,DC3S3_12,DC1S1_13,DC3S3_13),1,12)
bic4 <- BIC2(240,c(AC1S1_12,AC3S3_12,AC1S1_13,AC3S3_13),c(DC1S1_12,DC3S3_12,DC1S1_13,DC3S3_13),1,12)

R4 <- rep(seq(0.05,0.95,0.1),9)
DC3S3C1_12 <- Dall[AGE=='2012'][TRAT[AGE=='2012']=='C3'|TRAT[AGE=='2012']=='S3'|TRAT[AGE=='2012']=='C1']
DC3S3C1_13 <- Dall[AGE=='2013'][TRAT[AGE=='2013']=='C3'|TRAT[AGE=='2013']=='S3'|TRAT[AGE=='2013']=='C1']
fit81 <- nls(DC3S3C1_12/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
fit82 <- nls(DC3S3C1_13/(2*pi) ~  a*R4^(b+1)*(1-R4)^d, start=c(a=1,b=1.2,d=1.1))
AC3S3C1_12 <- summary(fit81)$coef[1,1] * R4 ^(summary(fit81)$coef[2,1]+1) * (1-R4) ^summary(fit81)$coef[3,1]*2*pi
AC3S3C1_13 <- summary(fit82)$coef[1,1] * R4 ^(summary(fit82)$coef[2,1]+1) * (1-R4) ^summary(fit82)$coef[3,1]*2*pi
aic5 <- AIC2(240,c(AC3S3C1_12,AS1_12,AC3S3C1_13,AS1_13),c(DC3S3C1_12,DC1_12,DC3S3C1_13,DC1_13),1,12)
bic5 <- BIC2(240,c(AC3S3C1_12,AS1_12,AC3S3C1_13,AS1_13),c(DC3S3C1_12,DC1_12,DC3S3C1_13,DC1_13),1,12)

aic6 <- AIC2(240,c(AC3S3C1_12,AS1_12,A02),c(DC3S3C1_12,DC1_12,D13),1,9)
bic6 <- BIC2(240,c(AC3S3C1_12,AS1_12,A02),c(DC3S3C1_12,DC1_12,D13),1,9)
aic7 <- AIC2(240,c(AC3S3C1_12,AS1_12,AC3S3_13,AC1S1_13),c(DC3S3C1_12,DC1_12,DC3S3_13,DC1S1_13),1,12)
bic7 <- BIC2(240,c(AC3S3C1_12,AS1_12,AC3S3_13,AC1S1_13),c(DC3S3C1_12,DC1_12,DC3S3_13,DC1S1_13),1,12)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
par(mfrow=c(1,2))
plot(0.5,0.5, xlab='Relative distance to trunk',ylab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,0.4),cex.lab=1.2)
points(R4,DC3S3C1_12/(2*pi),col=2)
X <- seq(0,1,0.01)
points(X,summary(fit81)$coef[1,1] * X ^(summary(fit81)$coef[2,1]+1) * (1-X) ^summary(fit81)$coef[3,1],type='l',lwd=2,col=2)
points(R2,DS1_12/(2*pi),col=3)
X <- seq(0,1,0.01)
points(X,summary(fit3)$coef[1,1] * X ^(summary(fit3)$coef[2,1]+1) * (1-X) ^summary(fit3)$coef[3,1],type='l',lwd=2,col=3)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1 & C3 & S3','S1'),pch=1,lty=1,col=2:3,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(paste('AIC = ',round(aic5,1)),paste('BIC = ',round(bic5,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
plot(0.5,0.5, xlab='Relative distance to trunk',ylab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,0.4),cex.lab=1.2)
points(R4,DC3S3C1_13/(2*pi),col=2)
X <- seq(0,1,0.01)
points(X,summary(fit82)$coef[1,1] * X ^(summary(fit82)$coef[2,1]+1) * (1-X) ^summary(fit82)$coef[3,1],type='l',lwd=2,col=2)
points(R2,DS1_13/(2*pi),col=3)
X <- seq(0,1,0.01)
points(X,summary(fit7)$coef[1,1] * X ^(summary(fit7)$coef[2,1]+1) * (1-X) ^summary(fit7)$coef[3,1],type='l',lwd=2,col=3)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1 & C3 & S3','S1'),pch=1,lty=1,col=2:3,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(paste('AIC = ',round(aic5,1)),paste('BIC = ',round(bic5,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()


# type d'arbre

DC1C3
R1 <- rep(seq(0.05,0.95,0.1),6)
fit0 <- nls(DC1C3/(2*pi) ~  a*R1^(b+1)*(1-R1)^d, start=c(a=1,b=1.2,d=1.1))

DP <- Dall[TRAT=='C1'|TRAT=='C3'][TT[TRAT=='C1'|TRAT=='C3']=='S']
DM <- Dall[TRAT=='C1'|TRAT=='C3'][TT[TRAT=='C1'|TRAT=='C3']=='M']
DG <- Dall[TRAT=='C1'|TRAT=='C3'][TT[TRAT=='C1'|TRAT=='C3']=='B']

R2 <- rep(seq(0.05,0.95,0.1),2)
fit1 <- nls(DP/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit2 <- nls(DM/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit3 <- nls(DG/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))

A0 <- summary(fit0)$coef[1,1] * R1 ^(summary(fit0)$coef[2,1]+1) * (1-R1) ^summary(fit0)$coef[3,1]*2*pi
AP <- summary(fit1)$coef[1,1] * R2 ^(summary(fit1)$coef[2,1]+1) * (1-R2) ^summary(fit1)$coef[3,1]*2*pi
AM <- summary(fit2)$coef[1,1] * R2 ^(summary(fit2)$coef[2,1]+1) * (1-R2) ^summary(fit2)$coef[3,1]*2*pi
AG <- summary(fit3)$coef[1,1] * R2 ^(summary(fit3)$coef[2,1]+1) * (1-R2) ^summary(fit3)$coef[3,1]*2*pi

aic0 <- AIC2(60,A0,DC1C3,1,3)
aic1 <- AIC2(60,c(AP,AM,AG),c(DP,DM,DG),1,9)
bic0 <- BIC2(60,A0,DC1C3,1,3)
bic1 <- BIC2(60,c(AP,AM,AG),c(DP,DM,DG),1,9)

DMG <- c(DM,DG)
R3 <- rep(seq(0.05,0.95,0.1),4)
fit4 <- nls(DMG/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
AMG <- summary(fit4)$coef[1,1] * R3 ^(summary(fit4)$coef[2,1]+1) * (1-R3) ^summary(fit4)$coef[3,1]*2*pi
aic2 <- AIC2(60,c(AP,AMG),c(DP,DMG),1,6)
bic2 <- BIC2(60,c(AP,AMG),c(DP,DMG),1,6)

par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, xlab='Relative distance to trunk',ylab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,0.4),cex.lab=1.2)
points(R2,DP/(2*pi),col=2)
X <- seq(0,1,0.01)
points(X,summary(fit1)$coef[1,1] * X ^(summary(fit1)$coef[2,1]+1) * (1-X) ^summary(fit1)$coef[3,1],type='l',lwd=2,col=2)
points(R2,DM/(2*pi),col=3)
X <- seq(0,1,0.01)
points(X,summary(fit2)$coef[1,1] * X ^(summary(fit2)$coef[2,1]+1) * (1-X) ^summary(fit2)$coef[3,1],type='l',lwd=2,col=3)
points(R2,DG/(2*pi),col=4)
X <- seq(0,1,0.01)
points(X,summary(fit3)$coef[1,1] * X ^(summary(fit3)$coef[2,1]+1) * (1-X) ^summary(fit3)$coef[3,1],type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('small trees','medium trees','big trees'),pch=1,lty=1,col=2:4,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(paste('AIC = ',round(aic1,1)),paste('BIC = ',round(bic1,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('treatment C1 & C3')


# S1 et S3
DS1S3
R1 <- rep(seq(0.05,0.95,0.1),6)
fit0 <- nls(DS1S3/(2*pi) ~  a*R1^(b+1)*(1-R1)^d, start=c(a=1,b=1.2,d=1.1))

DP <- Dall[TRAT=='S1'|TRAT=='S3'][TT[TRAT=='S1'|TRAT=='S3']=='S']
DM <- Dall[TRAT=='S1'|TRAT=='S3'][TT[TRAT=='S1'|TRAT=='S3']=='M']
DG <- Dall[TRAT=='S1'|TRAT=='S3'][TT[TRAT=='S1'|TRAT=='S3']=='B']

R2 <- rep(seq(0.05,0.95,0.1),2)
fit1 <- nls(DP/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit2 <- nls(DM/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))
fit3 <- nls(DG/(2*pi) ~  a*R2^(b+1)*(1-R2)^d, start=c(a=1,b=1.2,d=1.1))

A0 <- summary(fit0)$coef[1,1] * R1 ^(summary(fit0)$coef[2,1]+1) * (1-R1) ^summary(fit0)$coef[3,1]*2*pi
AP <- summary(fit1)$coef[1,1] * R2 ^(summary(fit1)$coef[2,1]+1) * (1-R2) ^summary(fit1)$coef[3,1]*2*pi
AM <- summary(fit2)$coef[1,1] * R2 ^(summary(fit2)$coef[2,1]+1) * (1-R2) ^summary(fit2)$coef[3,1]*2*pi
AG <- summary(fit3)$coef[1,1] * R2 ^(summary(fit3)$coef[2,1]+1) * (1-R2) ^summary(fit3)$coef[3,1]*2*pi

aic0 <- AIC2(60,A0,DS1S3,1,3)
aic1 <- AIC2(60,c(AP,AM,AG),c(DP,DM,DG),1,9)
bic0 <- BIC2(60,A0,DS1S3,1,3)
bic1 <- BIC2(60,c(AP,AM,AG),c(DP,DM,DG),1,9)

DPM <- c(DP,DM)
R3 <- rep(seq(0.05,0.95,0.1),4)
fit4 <- nls(DPM/(2*pi) ~  a*R3^(b+1)*(1-R3)^d, start=c(a=1,b=1.2,d=1.1))
APM <- summary(fit4)$coef[1,1] * R3 ^(summary(fit4)$coef[2,1]+1) * (1-R3) ^summary(fit4)$coef[3,1]*2*pi
aic2 <- AIC2(60,c(APM,AG),c(DPM,DG),1,6)
bic2 <- BIC2(60,c(APM,AG),c(DPM,DG),1,6)

par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, xlab='Relative distance to trunk',ylab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,0.4),cex.lab=1.2)
points(R1,DS1S3/(2*pi),col=1)
X <- seq(0,1,0.01)
points(X,summary(fit0)$coef[1,1] * X ^(summary(fit0)$coef[2,1]+1) * (1-X) ^summary(fit0)$coef[3,1],type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('no tree effect'),pch=1,lty=1,col=4,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c(paste('AIC = ',round(aic0,1)),paste('BIC = ',round(bic0,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('treatment S1 & S3')





















