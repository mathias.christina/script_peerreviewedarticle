set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Effet des traitements/Leaf angle distribution")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

dat1 <- read.table('ANG_MAIO2011.txt',header=T)
dat2 <- read.table('ANG_MAIO2012.txt',header=T)
dat3 <- read.table('ANG_MAIO2013.txt',header=T)
dat4 <- read.table('ANG_MAIO2014.txt',header=T)

dat11 <- dat1[1,]
for (i in 2:588){
if(dat1$Tratamento[i]=='C2'|dat1$Tratamento[i]=='S2'){ dat11 <- dat11}
else { dat11 <- rbind(dat11,dat1[i,])}}
dat22 <- dat2[121,]
for (i in 122:960){
if (dat2$TRAT[i]=='C2'|dat2$TRAT[i]=='S2'){ dat22 <- dat22}
else {dat22 <- rbind(dat22,dat2[i,])}}
dat32 <- dat3[1,]
for (i in 2:576){
if(dat3$Trat[i]=='C2'|dat3$Trat[i]=='S2'){ dat32 <- dat32}
else { dat32 <- rbind(dat32,dat3[i,])}}
dat42 <- dat4[1,]
for (i in 2:576){
  if(dat4$Tratamento[i]=='C2'|dat4$Tratamento[i]=='S2'){ dat42 <- dat42}
  else { dat42 <- rbind(dat42,dat4[i,])}}


NUM1 <- c(); for (i in 1:33){ NUM1 <- c(NUM1, rep(i,12))}
NUM1 <- rep(NUM1, 6)
TRAT1 <- rep(dat11$Tratamento,6)
dates1 <- rep('Maio 2011',396*6)
arbre1 <- rep(dat11$Arvore,6)
Niveau1 <- rep(dat11$Nivel,6)
Bloc1 <- rep(dat11$Bloco,6)
angles1 <- c(abs(dat11$Folha1),abs(dat11$Folha2),abs(dat11$Folha3),abs(dat11$Folha4),abs(dat11$Folha5),abs(dat11$Folha6))

NUM2 <- c(); for (i in 34:65){ NUM2 <- c(NUM2, rep(i,20))}
NUM2 <- rep(NUM2,3)
TRAT2 <- rep(dat22$TRAT,3)
dates2 <- rep('Maio 2012',640*3)
arbre2 <- rep(dat22$ARBRE,3)
Niveau2 <- c(rep('Inferior',640),rep('Medio',640),rep('Superior',640))
angles2 <- c(abs(dat22[,5]),abs(dat22[,6]),abs(dat22[,7]))
Bloc2 <- rep(dat22$BLOCO,3)

NUM3 <- c(); for (i in 66:113){ NUM2 <- c(NUM2, rep(i,12))}
NUM3 <- rep(NUM2,6)                   
TRAT3 <- rep(dat32$Trat,6)
dates3 <- rep('Maio 2013',384*6)
arbre3 <- rep(dat32$Arv,6)
Niveau3 <- rep(dat32$Nivel,6)
angles3 <- c(abs(dat32$X1),abs(dat32$X2),abs(dat32$X3),abs(dat32$X4),abs(dat32$X5),abs(dat32$X6))
Bloc3 <- rep(dat32$Bloco,6)

TRAT4 <- rep(dat42$Tratamento,6)
dates4 <- rep('Maio 2014',384*6)
arbre4 <- rep(dat42$Arvore,6)
angles4 <- c(abs(dat42$X1),abs(dat42$X2),abs(dat42$X3),abs(dat42$X4),abs(dat42$X5),abs(dat42$X6))

arbre <- c(arbre1,arbre2,arbre3)
Arbre <- rep(NA, length(arbre))
for (i in 1:length(arbre)){
if(arbre[i]==1|arbre[i]==2){
Arbre[i] <- c('p')}
else if(arbre[i]==3|arbre[i]==4|arbre[i]==5|arbre[i]==6){
Arbre[i] <- c('m')}
else
Arbre[i] <- c('g')
}

NUM <- c(NUM1,NUM2,NUM3)
TRAT <- c(as.vector(TRAT1),as.vector(TRAT2),as.vector(TRAT3),as.vector(TRAT4))
dates <- c(as.vector(dates1),as.vector(dates2),as.vector(dates3),as.vector(dates4))
Niveau <- c(as.vector(Niveau1), as.vector(Niveau2), as.vector(Niveau3))
angles <- c(angles1,angles2,angles3,angles4)
BLOC <- c(Bloc1, Bloc2, Bloc3)

WEFF<-c() ; KEFF<-c()
for (i in 1:length(angles)){
if(TRAT[i]=='C1'){WEFF <- c(WEFF,'+W');KEFF <- c(KEFF,'-K')}
else if(TRAT[i]=='C3'){WEFF <- c(WEFF,'+W');KEFF <- c(KEFF,'+K')}
else if(TRAT[i]=='S1'){WEFF <- c(WEFF,'-W');KEFF <- c(KEFF,'-K')}
else if(TRAT[i]=='S3'){WEFF <- c(WEFF,'-W');KEFF <- c(KEFF,'+K')}}

#test statistic
angles <- replace(angles,c(angles >90),NA)

library(nlme)

ana1 <-  gls(angles~WEFF*KEFF+dates+Arbre,method='ML',na.action=na.omit)
ana2 <-  gls(angles~WEFF*KEFF*dates*Arbre,method='ML',na.action=na.omit)
ana3 <-  gls(angles~WEFF*KEFF+(WEFF+KEFF)*dates+(WEFF+KEFF)*Arbre,method='ML',na.action=na.omit)


ana11 <-  lme(angles~WEFF*KEFF+dates+Arbre,random=~1|BLOC,na.action=na.omit)
ana12 <-  lme(angles~WEFF*KEFF+dates+Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit)
ana13 <-  lme(angles~WEFF*KEFF+dates+Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana14 <-  lme(angles~WEFF*KEFF+dates+Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|Arbre))

ana43 <-  lme(angles~WEFF*KEFF,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana33 <-  lme(angles~WEFF*KEFF+(WEFF+KEFF)*dates+(WEFF+KEFF)*Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana23 <-  lme(angles~WEFF*KEFF*dates*Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana53 <-  lme(angles~WEFF*KEFF+(WEFF+KEFF)*dates*Arbre,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana63 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana63 <-  lme(angles~WEFF*KEFF+(WEFF+KEFF)*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))

ana6 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit)
ana61 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates))
ana62 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|TRAT))
ana62 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|arbre))
ana63 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates/arbre))

ana62 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates),  correlation=corAR1(form= ~ 1))
ana63 <-  lme(angles~WEFF*KEFF*dates,random=~1+1|BLOC/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|dates),  correlation=corAR1(form= ~ 1|dates))


TRAT <- as.factor(TRAT)
contrasts(TRAT) <- matrix(c(3,-1,-1,-1,0,1,-2,1,0,1,0,-1),nrow=4)
Arbre <- as.factor(Arbre)
contrasts(Arbre) <- matrix(c(2,-1,-1,0,1,-1),nrow=3)
dates <- as.factor(dates)
contrasts(dates) <- matrix(c(2,-1,-1,0,1,-1),nrow=3)

ana131 <-  gls(angles~TRAT*dates,method='ML',  weights=varIdent(form= ~ 1|dates),na.action=na.omit)

angles <- replace(angles, angles>90,90)

# on sort les distribution
AnC1_11 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2011']), breaks=seq(0,90,10))$dens*10
AnC3_11 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2011']), breaks=seq(0,90,10))$dens*10
AnS1_11 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2011']), breaks=seq(0,90,10))$dens*10
AnS3_11 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2011']), breaks=seq(0,90,10))$dens*10
AnC1_12 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2012']), breaks=seq(0,90,10))$dens*10
AnC3_12 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2012']), breaks=seq(0,90,10))$dens*10
AnS1_12 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2012']), breaks=seq(0,90,10))$dens*10
AnS3_12 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2012']), breaks=seq(0,90,10))$dens*10
AnC1_13 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2013']), breaks=seq(0,90,10))$dens*10
AnC3_13 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2013']), breaks=seq(0,90,10))$dens*10
AnS1_13 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2013']), breaks=seq(0,90,10))$dens*10
AnS3_13 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2013']), breaks=seq(0,90,10))$dens*10
AnC1_14 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2014']), breaks=seq(0,90,10))$dens*10
AnC3_14 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2014']), breaks=seq(0,90,10))$dens*10
AnS1_14 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2014']), breaks=seq(0,90,10))$dens*10
AnS3_14 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2014']), breaks=seq(0,90,10))$dens*10

MAT <- data.frame(AnC1_11,AnC3_11,AnS1_11,AnS3_11,AnC1_12,AnC3_12,AnS1_12,AnS3_12,AnC1_13,AnC3_13,AnS1_13,AnS3_13,
                  AnC1_14,AnC3_14,AnS1_14,AnS3_14)
write.table(MAT,'Angles_E140.txt',col.names=T,row.names=F)

contrasts(TRAT) <- matrix(c(1,-1,1,-1,1,0,-1,0,0,1,0,-1),nrow=4)
angles13 <- angles[dates=='Maio 2013']
TRAT13 <- TRAT[dates=='Maio 2013']
ana1313 <-  lm(angles13~TRAT13)#*dates,method='ML',  weights=varIdent(form= ~ 1|dates),na.action=na.omit)

ANG <- c(5,15,25,35,45,55,65,75,85)
sum(ANG*AnC1_11)

par(mar=c(4.5,4.5,3,1))
par(mfrow=c(1,3))
plot(ANG,AnC1_11,type='b',pch=16,lty=1,lwd=2,col=2,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,xlim=c(0,90),ylim=c(0,0.25))
points(ANG,AnC3_11,type='b',col=4,pch=16,lty=1,lwd=2)
points(ANG,AnS1_11,type='b',col=2,pch=4,lty=2,lwd=2)
points(ANG,AnS3_11,type='b',col=4,pch=4,lty=2,lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('C1:'~theta[I]~'='~'24�'),expression('C3:'~theta[I]~'='~'28�'),
                           expression('S1&S3:'~theta[I]~'='~'31�')),
                           pch=NA,cex=1.2,bg='white',box.col=0);box()
legend('bottomleft',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),
        col=c(2,4,2,4),pch=c(16,16,4,4),lty=c(1,1,2,2),bg='white',box.col=0,cex=1.2)
box()
title('1 year-old')
plot(ANG,AnC1_12,type='b',pch=16,lty=1,col=2,lwd=2,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,xlim=c(0,90),ylim=c(0,0.25))
points(ANG,AnC3_12,type='b',col=4,pch=16,lty=1,lwd=2)
points(ANG,AnS1_12,type='b',col=2,pch=4,lty=2,lwd=2)
points(ANG,AnS3_12,type='b',col=4,pch=4,lty=2,lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('C1:'~theta[I]~'='~'35�'),expression('S1:'~theta[I]~'='~'42�'),
                           expression('C3&S3:'~theta[I]~'='~'47�')),
                           pch=NA,cex=1.2,bg='white',box.col=0);box()
title('2 year-old')
plot(ANG,AnC1_13,type='b',pch=16,lty=1,col=2,lwd=2,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2,xlim=c(0,90),ylim=c(0,0.25))
points(ANG,AnC3_13,type='b',col=4,pch=16,lty=1,lwd=2)
points(ANG,AnS1_13,type='b',col=2,pch=4,lty=2,lwd=2)
points(ANG,AnS3_13,type='b',col=4,pch=4,lty=2,lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('C1&S1:'~theta[I]~'='~'42�'),
                           expression('C3&S3:'~theta[I]~'='~'49�')),
                           pch=NA,cex=1.2,bg='white',box.col=0);box()
title('3 year-old')

# traitement
AnC1 <- hist(c(angles[TRAT=='C1']), breaks=seq(0,90,10))$dens
AnC3 <- hist(c(angles[TRAT=='C3']), breaks=seq(0,90,10))$dens
AnS1 <- hist(c(angles[TRAT=='S1']), breaks=seq(0,90,10))$dens
AnS3 <- hist(c(angles[TRAT=='S3']), breaks=seq(0,90,10))$dens
AnC3S1S3 <- hist(c(angles[TRAT=='C3'|TRAT=='S1'|TRAT=='S3']), breaks=seq(0,90,10))$dens

ANG <- c(5,15,25,35,45,55,65,75,85)
palette('Default')
sum(ANG*AnC1*10)
sum(ANG*AnC3S1S3*10)

par(mar=c(4.5,4.5,1,1))
#par(mfrow=c(2,1))
plot(ANG,AnC1*10,type='b',pch=16,col=1,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnC3*10,type='b',col=2,pch=16)
points(ANG,AnS1*10,type='b',col=3,pch=16)
points(ANG,AnS3*10,type='b',col=4,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('C1:'~theta[I]~'='~'28�'),
                           expression('C3&S1&S3:'~theta[I]~'='~'37�')),
                           pch=c(16,16,16),lty=c(1,1,1),col=1:2,cex=1.2,bg='white',box.col=0,text.col=1:2);box()

AnC1_11 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2011']), breaks=seq(0,90,10))$dens
AnC3_11 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2011']), breaks=seq(0,90,10))$dens
AnS1_11 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2011']), breaks=seq(0,90,10))$dens
AnS3_11 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2011']), breaks=seq(0,90,10))$dens
AnC1_12 <- hist(c(angles[TRAT=='C1'][dates[TRAT=='C1']=='Maio 2012']), breaks=seq(0,90,10))$dens
AnC3_12 <- hist(c(angles[TRAT=='C3'][dates[TRAT=='C3']=='Maio 2012']), breaks=seq(0,90,10))$dens
AnS1_12 <- hist(c(angles[TRAT=='S1'][dates[TRAT=='S1']=='Maio 2012']), breaks=seq(0,90,10))$dens
AnS3_12 <- hist(c(angles[TRAT=='S3'][dates[TRAT=='S3']=='Maio 2012']), breaks=seq(0,90,10))$dens
sum(ANG*AnC1_11*10)

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,2))
plot(ANG,AnC1_11*10,type='b',pch=16,col=1,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnC1_12*10,type='b',col=1,pch=16,lty=2)
legend('bottomleft',legend=c('C1'),pch=NA,bty='n')
legend('topright',legend=c(expression('1y,'~theta[I]~'=  24�'),expression('2y,'~theta[I]~'=  35�')),pch=16,lty=1:2,bty='n')
plot(ANG,AnC3_11*10,type='b',col=1,pch=16,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnC3_12*10,type='b',col=1,pch=16,lty=2)
legend('bottomleft',legend=c('C3'),pch=NA,bty='n')
legend('topright',legend=c(expression('1y,'~theta[I]~'=  28�'),expression('2y,'~theta[I]~'=  45�')),pch=16,lty=1:2,bty='n')
plot(ANG,AnS1_11*10,type='b',col=1,pch=16,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnS1_12*10,type='b',col=1,pch=16,lty=2)
legend('bottomleft',legend=c('S1'),pch=NA,bty='n')
legend('topright',legend=c(expression('1y,'~theta[I]~'=  30�'),expression('2y,'~theta[I]~'=  41�')),pch=16,lty=1:2,bty='n')
plot(ANG,AnS3_11*10,type='b',col=1,pch=16,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnS3_12*10,type='b',col=1,pch=16,lty=2)
legend('bottomleft',legend=c('S3'),pch=NA,bty='n')
legend('topright',legend=c(expression('1y,'~theta[I]~'=  32�'),expression('2y,'~theta[I]~'=  47�')),pch=16,lty=1:2,bty='n')



An2011 <- hist(c(angles[dates=='Maio 2011']),breaks=seq(0,90,10))$dens
An2012 <- hist(c(angles[dates=='Maio 2012']),breaks=seq(0,90,10))$dens
sum(ANG*An2011*10)
sum(ANG*An2012*10)

par(mar=c(4.5,4.5,1,1))
#par(mfrow=c(2,1))
plot(ANG,An2011*10,type='b',pch=16,col=1,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,An2012*10,type='b',col=2,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('2011:'~theta[I]~'='~'28�'),
                           expression('2012:'~theta[I]~'='~'42�')),
                           pch=c(16,16,16),lty=c(1,1,1),col=1:2,cex=1.2,bg='white',box.col=0,text.col=1:2);box()

AnS <- hist(c(angles[Arbre=='p']),breaks=seq(0,90,10))$dens
AnM <- hist(c(angles[Arbre=='m']),breaks=seq(0,90,10))$dens
AnG <- hist(c(angles[Arbre=='g']),breaks=seq(0,90,10))$dens
sum(AnS*ANG*10)
sum(AnM*ANG*10)
sum(AnG*ANG*10)

par(mar=c(4.5,4.5,1,1))
#par(mfrow=c(2,1))
plot(ANG,AnS*10,type='b',pch=16,col=1,xlab='Leaf inclination angle (�)',ylab='Probability',cex.lab=1.2)
points(ANG,AnM*10,type='b',col=2,pch=16)
points(ANG,AnG*10,type='b',col=4,pch=16)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('topright',legend=c(expression('Small trees:'~theta[I]~'='~'31�'),
                           expression('Medium trees:'~theta[I]~'='~'34�'),
                           expression('Big trees:'~theta[I]~'='~'38�')),
                           pch=c(16,16,16),lty=c(1,1,1),col=c(1,2,4),cex=1.2,bg='white',box.col=0,text.col=c(1,2,4));box()






