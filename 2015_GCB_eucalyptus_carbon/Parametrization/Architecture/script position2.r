set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Extrapolation bordure et architecture copa")
setwd(set)
DI <- read.table('Datas_intern.txt',header=T)
DB <- read.table('Datas_bordure.txt',header=T)


# on construit le C1
XB1C1 <- rep(c(1:22)*2,18)
y <- c(25:43)*3
YB1C1 <- c()
for (i in 1:18){
YB1C1 <- c(YB1C1, rep(y[18-i+1],22))}
TB1C1 <- c(rep('B',22*7+13),rep('PI',6),rep('B',16),rep('PI',6),rep('B',16),rep('PI',6),rep('B',16),rep('PI',6),rep('B',16),rep('PI',6),rep('B',16),rep('PI',6),rep('B',3+22*5))
plot(XB1C1,YB1C1)
points(XB1C1[1:30],YB1C1[1:30],col=3)
points(XB1C1[TB1C1=='PI'],YB1C1[TB1C1=='PI'],col=2)

#B2
xini <- c(22+12+16+18+1)
xfin <- c(22+12+16+18+14)
XB2C1 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2C1<-c()
for (i in 1:12){ YB2C1 <- c(YB2C1, rep(y[12-i+1],14))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C1'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C1']==2])
TB2C1 <- c(rep('B',14*3+3),rep('PI',8),rep('B',6),rep('PI',8),rep('B',6),rep('PI',8),rep('B',6),rep('PI',8),rep('B',6),rep('PI',8),rep('B',6),rep('PI',8),rep('B',3+14*3))
plot(XB2C1,YB2C1)
points(XB2C1[1:30],YB2C1[1:30],col=3)
points(XB2C1[TB2C1=='PI'],YB2C1[TB2C1=='PI'],col=2)

#B3
xini <- c(22+12+16+18+4+1)
xfin <- c(22+12+16+18+4+12)
XB3C1 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3C1<-c()
for (i in 1:12){ YB3C1 <- c(YB3C1, rep(y[12-i+1],12))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C1'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C1']==3])
TB3C1 <- c(rep('B',12*3+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*3))
plot(XB3C1,YB3C1)
points(XB3C1[1:30],YB3C1[1:30],col=3)
points(XB3C1[TB3C1=='PI'],YB3C1[TB3C1=='PI'],col=2)

#C2 B1
xini <- c(22+8+12+1)
xfin <- c(22+8+12+14)
XB1C2 <- c(c(xini:(xini+6))*2,rep(c(xini:xfin)*2,16))
y <- c(25:41)*3
YB1C2 <- c()
for (i in 1:17){
YB1C2 <- c(YB1C2, rep(y[17-i+1],14))}
YB1C2 <- c(YB1C2[1:7],YB1C2[15:238])
TB1C2 <- c(rep('B',7+14*5+5),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',3+14*5))
plot(XB1C2,YB1C2)
points(XB1C2[1:30],YB1C2[1:30],col=3)
points(XB1C2[TB1C2=='PI'],YB1C2[TB1C2=='PI'],col=2)
##B2
xini <- c(22+12+16+18+14+10+1)
xfin <- c(22+12+16+18+14+10+13)
XB2C2 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2C2<-c()
for (i in 1:12){ YB2C2 <- c(YB2C2, rep(y[12-i+1],13))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2']==2])
TB2C2 <- c(rep('B',13*3+3),rep('PI',7),rep('B',6),rep('PI',7),rep('B',6),rep('PI',7),rep('B',6),rep('PI',7),rep('B',6),rep('PI',7),rep('B',6),rep('PI',7),rep('B',3+13*3))
plot(XB2C2,YB2C2)
points(XB2C2[1:30],YB2C2[1:30],col=3)
points(XB2C2[TB2C2=='PI'],YB2C2[TB2C2=='PI'],col=2)
#B3
xini <- c(22+12+16+18+4+12+1)
xfin <- c(22+12+16+18+4+12+21)
XB3C2 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3C2<-c()
for (i in 1:12){ YB3C2 <- c(YB3C2, rep(y[12-i+1],21))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2']==3])
TB3C2 <- c(rep('B',21*3+12),rep('PI',6),rep('B',15),rep('PI',6),rep('B',15),rep('PI',6),rep('B',15),rep('PI',6),rep('B',15),rep('PI',6),rep('B',15),rep('PI',6),rep('B',3+21*3))
plot(XB3C2,YB3C2)
points(XB3C2[1:30],YB3C2[1:30],col=3)
points(XB3C2[TB3C2=='PI'],YB3C2[TB3C2=='PI'],col=2)

#C3B1
xini <- c(22+8+1)
xfin <- c(22+8+12)
XB1C3 <- rep(c(xini:xfin)*2,17)
y <- c(25:42)*3
YB1C3 <- c()
for (i in 1:17){
YB1C3 <- c(YB1C3, rep(y[17-i+1],12))}
TB1C3 <- c(rep('B',12*6+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*5))
plot(XB1C3,YB1C3)
points(XB1C3[1:30],YB1C3[1:30],col=3)
points(XB1C3[TB1C3=='PI'],YB1C3[TB1C3=='PI'],col=2)
#B2C3
xini <- c(22+12+16+1)
xfin <- c(22+12+16+18)
XB2C3 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2C3<-c()
for (i in 1:12){ YB2C3 <- c(YB2C3, rep(y[12-i+1],18))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3']==2])
TB2C3 <- c(rep('B',18*3+4),rep('PI',9),rep('B',9),rep('PI',9),rep('B',9),rep('PI',9),rep('B',9),rep('PI',9),rep('B',9),rep('PI',9),rep('B',9),rep('PI',9),rep('B',5+18*3))
plot(XB2C3,YB2C3)
points(XB2C3[1:30],YB2C3[1:30],col=3)
points(XB2C3[TB2C3=='PI'],YB2C3[TB2C3=='PI'],col=2)
#B3C3
xini <- c(14+12+16+9+1)
xfin <- c(14+12+16+9+12)
XB3C3 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3C3<-c()
for (i in 1:12){ YB3C3 <- c(YB3C3, rep(y[12-i+1],12))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3']==3])
TB3C3 <- c(rep('B',12*3+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*3))
plot(XB3C3,YB3C3)
points(XB3C3[1:30],YB3C3[1:30],col=3)
points(XB3C3[TB3C3=='PI'],YB3C3[TB3C3=='PI'],col=2)

#B1S1
xini <- c(22+8+12+14+14+12+10+1)
xfin <- c(22+8+12+14+14+12+10+13)
XB1S1 <- rep(c(xini:xfin)*2,15)
y <- c(25:39)*3
YB1S1 <- c()
for (i in 1:15){
YB1S1 <- c(YB1S1, rep(y[15-i+1],13))}
TB1S1 <- c(rep('B',13*4+4),rep('PI',6),rep('B',7),rep('PI',6),rep('B',7),rep('PI',6),rep('B',7),rep('PI',6),rep('B',7),rep('PI',6),rep('B',7),rep('PI',6),rep('B',3+13*5))
plot(XB1S1,YB1S1)
points(XB1S1[1:30],YB1S1[1:30],col=3)
points(XB1S1[TB1S1=='PI'],YB1S1[TB1S1=='PI'],col=2)
#B1S2
xini <- c(22+8+12+14+14+1)
xfin <- c(22+8+12+14+14+12)
XB1S2 <- rep(c(xini:xfin)*2,16)
y <- c(25:40)*3
YB1S2 <- c()
for (i in 1:16){
YB1S2 <- c(YB1S2, rep(y[16-i+1],12))}
TB1S2 <- c(rep('B',12*5+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*5))
plot(XB1S2,YB1S2)
points(XB1S2[1:30],YB1S2[1:30],col=3,pch=16)
points(XB1S2[TB1S2=='PI'],YB1S2[TB1S2=='PI'],col=2,pch=16)
#B1S3
xini <- c(22+8+12+14+1)
xfin <- c(22+8+12+14+14)
XB1S3 <- rep(c(xini:xfin)*2,16)
y <- c(25:40)*3
YB1S3 <- c()
for (i in 1:16){
YB1S3 <- c(YB1S3, rep(y[16-i+1],14))}
TB1S3 <- c(rep('B',14*5+5),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',3+14*5))
plot(XB1S3,YB1S3)
points(XB1S3[1:30],YB1S3[1:30],col=3,pch=16)
points(XB1S3[TB1S3=='PI'],YB1S3[TB1S3=='PI'],col=2,pch=16)
#B1P1
xini <- c(22+1)
xfin <- c(22+8)
XB1P1 <- c(c(23:27)*2,rep(c(xini:xfin)*2,17))
y <- c(25:42)*3
YB1P1 <- c()
for (i in 1:18){
YB1P1 <- c(YB1P1, rep(y[18-i+1],8))}
YB1P1 <- c(YB1P1[1:5],YB1P1[9:144])
TB1P1 <- rep('B',17*8+5)
plot(XB1P1,YB1P1)
points(XB1P1[1:30],YB1P1[1:30],col=3,pch=16)
#points(XB1P1[TB1S3=='PI'],YB1P1[TB1P1=='PI'],col=2,pch=16)
#B1P2
xini <- c(22+8+12+14+14+12+1)
xfin <- c(22+8+12+14+14+12+10)
XB1P2 <- c((xini+1)*2,rep(c(xini:xfin)*2,15))
y <- c(25:40)*3
YB1P2 <- c()
for (i in 1:16){
YB1P2 <- c(YB1P2, rep(y[16-i+1],10))}
YB1P2 <- c(YB1P2[2],YB1P2[11:160])
TB1P2 <- c(rep('B',10*15+1))
plot(XB1P2,YB1P2)
points(XB1P2[1:30],YB1P2[1:30],col=3,pch=16)
#points(XB1P2[TB1S2=='PI'],YB1P2[TB1P2=='PI'],col=2,pch=16)
#B2P1
xini <- c(1)
xfin <- c(10)
XB2P1 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2P1<-c()
for (i in 1:12){ YB2P1 <- c(YB2P1, rep(y[12-i+1],10))}
#length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C2']==2])
TB2P1 <- c(rep('B',10*12))
plot(XB2P1,YB2P1)
points(XB2P1[1:30],YB2P1[1:30],col=3)
#points(XB2P1[TB2C2=='PI'],YB2P1[TB2P1=='PI'],col=2)
#B2S3
xini <- c(10+1)
xfin <- c(10+12)
XB2S3 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2S3<-c()
for (i in 1:12){ YB2S3 <- c(YB2S3, rep(y[12-i+1],12))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3']==2])
TB2S3 <- c(rep('B',12*3+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*3))
plot(XB2S3,YB2S3)
points(XB2S3[1:30],YB2S3[1:30],col=3)
points(XB2S3[TB2S3=='PI'],YB2S3[TB2S3=='PI'],col=2)
#B2S2
xini <- c(10+12+1)
xfin <- c(10+12+12)
XB2S2 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2S2<-c()
for (i in 1:12){ YB2S2 <- c(YB2S2, rep(y[12-i+1],12))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3']==2])
TB2S2 <- c(rep('B',12*3+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*3))
plot(XB2S2,YB2S2)
points(XB2S2[1:30],YB2S2[1:30],col=3)
points(XB2S2[TB2S2=='PI'],YB2S2[TB2S2=='PI'],col=2)
#B2S1
xini <- c(10+12+12+1)
xfin <- c(10+12+12+16)
XB2S1 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2S1<-c()
for (i in 1:12){ YB2S1 <- c(YB2S1, rep(y[12-i+1],16))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3']==2])
TB2S1 <- c(rep('B',16*3+7),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',3+16*3))
plot(XB2S1,YB2S1)
points(XB2S1[1:30],YB2S1[1:30],col=3)
points(XB2S1[TB2S1=='PI'],YB2S1[TB2S1=='PI'],col=2)
#B2P2
xini <- c(10+12+12+16+18+14+1)
xfin <- c(10+12+12+16+18+14+10)
XB2P2 <- rep(c(xini:xfin)*2,12)
y <- c(13:24)*3 ; YB2P2<-c()
for (i in 1:12){ YB2P2 <- c(YB2P2, rep(y[12-i+1],10))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='S3']==2])
TB2P2 <- c(rep('B',12*10))
plot(XB2P2,YB2P2)
points(XB2P2[1:30],YB2P2[1:30],col=3)
points(XB2P2[TB2P2=='PI'],YB2P2[TB2P2=='PI'],col=2)

#B3S3
xini <- c(1)
xfin <- c(14)
XB3S3 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3S3<-c()
for (i in 1:12){ YB3S3 <- c(YB3S3, rep(y[12-i+1],14))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3']==3])
TB3S3 <- c(rep('B',14*3+5),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',8),rep('PI',6),rep('B',3+14*3))
plot(XB3S3,YB3S3)
points(XB3S3[1:30],YB3S3[1:30],col=3)
points(XB3S3[TB3S3=='PI'],YB3S3[TB3S3=='PI'],col=2)
#B3S1
xini <- c(14+1)
xfin <- c(14+12)
XB3S1 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3S1<-c()
for (i in 1:12){ YB3S1 <- c(YB3S1, rep(y[12-i+1],12))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3']==3])
TB3S1 <- c(rep('B',12*3+3),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',6),rep('PI',6),rep('B',3+12*3))
plot(XB3S1,YB3S1)
points(XB3S1[1:30],YB3S1[1:30],col=3)
points(XB3S1[TB3S1=='PI'],YB3S1[TB3S1=='PI'],col=2)
#B3S2
xini <- c(14+12+1)
xfin <- c(14+12+16)
XB3S2 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3S2<-c()
for (i in 1:12){ YB3S2 <- c(YB3S2, rep(y[12-i+1],16))}
length(DI$ALTTOT[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3'][DI$BLOCO[DI$AGE==6][DI$TRAT[DI$AGE==6]=='C3']==3])
TB3S2 <- c(rep('B',16*3+7),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',10),rep('PI',6),rep('B',3+16*3))
plot(XB3S2,YB3S2)
points(XB3S2[1:30],YB3S2[1:30],col=3)
points(XB3S2[TB3S2=='PI'],YB3S2[TB3S2=='PI'],col=2)
#B3P1
xini <- c(14+12+16+1)
xfin <- c(14+12+16+9)
XB3P1 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3P1<-c()
for (i in 1:12){ YB3P1 <- c(YB3P1, rep(y[12-i+1],9))}
TB3P1 <- c(rep('B',9*12))
plot(XB3P1,YB3P1)
points(XB3P1[1:30],YB3P1[1:30],col=3)
points(XB3P1[TB3P1=='PI'],YB3P1[TB3P1=='PI'],col=2)
#B3P2
xini <- c(14+12+16+9+12+1)
xfin <- c(14+12+16+9+12+9)
XB3P2 <- rep(c(xini:xfin)*2,12)
y <- c(1:12)*3 ; YB3P2<-c()
for (i in 1:12){ YB3P2 <- c(YB3P2, rep(y[12-i+1],9))}
TB3P2 <- c(rep('B',9*12))
plot(XB3P2,YB3P2)
points(XB3P2[1:30],YB3P2[1:30],col=3)
points(XB3P2[TB3P1=='PI'],YB3P2[TB3P1=='PI'],col=2)

# representons

X <- c(XB1C1,XB1C2,XB1C3,XB1S1,XB1S2,XB1S3,XB1P1,XB1P2,
       XB2C1,XB2C2,XB2C3,XB2S1,XB2S2,XB2S3,XB2P1,XB2P2,
       XB3C1,XB3C2,XB3C3,XB3S1,XB3S2,XB3S3,XB3P1,XB3P2)
Y <- c(YB1C1,YB1C2,YB1C3,YB1S1,YB1S2,YB1S3,YB1P1,YB1P2,
       YB2C1,YB2C2,YB2C3,YB2S1,YB2S2,YB2S3,YB2P1,YB2P2,
       YB3C1,YB3C2,YB3C3,YB3S1,YB3S2,YB3S3,YB3P1,YB3P2)

plot(X,Y)
points(XB1C1[TB1C1=='PI'],YB1C1[TB1C1=='PI'],pch=16,col=2)
points(XB2C1[TB2C1=='PI'],YB2C1[TB2C1=='PI'],pch=16,col=2)
points(XB3C1[TB3C1=='PI'],YB3C1[TB3C1=='PI'],pch=16,col=2)
points(XB1C2[TB1C2=='PI'],YB1C2[TB1C2=='PI'],pch=16,col=3)
points(XB2C2[TB2C2=='PI'],YB2C2[TB2C2=='PI'],pch=16,col=3)
points(XB3C2[TB3C2=='PI'],YB3C2[TB3C2=='PI'],pch=16,col=3)
points(XB1C3[TB1C3=='PI'],YB1C3[TB1C3=='PI'],pch=16,col=4)
points(XB2C3[TB2C3=='PI'],YB2C3[TB2C3=='PI'],pch=16,col=4)
points(XB3C3[TB3C3=='PI'],YB3C3[TB3C3=='PI'],pch=16,col=4)
points(XB1S1[TB1S1=='PI'],YB1S1[TB1S1=='PI'],pch=16,col=5)
points(XB2S1[TB2S1=='PI'],YB2S1[TB2S1=='PI'],pch=16,col=5)
points(XB3S1[TB3S1=='PI'],YB3S1[TB3S1=='PI'],pch=16,col=5)
points(XB1S2[TB1S2=='PI'],YB1S2[TB1S2=='PI'],pch=16,col=6)
points(XB2S2[TB2S2=='PI'],YB2S2[TB2S2=='PI'],pch=16,col=6)
points(XB3S2[TB3S2=='PI'],YB3S2[TB3S2=='PI'],pch=16,col=6)
points(XB1S3[TB1S3=='PI'],YB1S3[TB1S3=='PI'],pch=16,col=7)
points(XB2S3[TB2S3=='PI'],YB2S3[TB2S3=='PI'],pch=16,col=7)
points(XB3S3[TB3S3=='PI'],YB3S3[TB3S3=='PI'],pch=16,col=7)

Ty <- c(TB1C1,TB1C2,TB1C3,TB1S1,TB1S2,TB1S3,TB1P1,TB1P2,
        TB2C1,TB2C2,TB2C3,TB2S1,TB2S2,TB2S3,TB2P1,TB2P2,
        TB3C1,TB3C2,TB3C3,TB3S1,TB3S2,TB3S3,TB3P1,TB3P2)
Bl <- c(rep(1,1734),rep(2,1260),rep(3,1260))
Tr <- c(rep('C1',length(TB1C1)),rep('C2',length(TB1C2)),rep('C3',length(TB1C3)),rep('S1',length(TB1S1)),rep('S2',length(TB1S2)),rep('S3',length(TB1S3)),rep('P1',length(TB1P1)),rep('P2',length(TB1P2)),
        rep('C1',length(TB2C1)),rep('C2',length(TB2C2)),rep('C3',length(TB2C3)),rep('S1',length(TB2S1)),rep('S2',length(TB2S2)),rep('S3',length(TB2S3)),rep('P1',length(TB2P1)),rep('P2',length(TB2P2)),
        rep('C1',length(TB3C1)),rep('C2',length(TB3C2)),rep('C3',length(TB3C3)),rep('S1',length(TB3S1)),rep('S2',length(TB3S2)),rep('S3',length(TB3S3)),rep('P1',length(TB3P1)),rep('P2',length(TB3P2)))


# on rassemble les donn�es
detach(DI);detach(DB)
DI <- read.table('Datas_intern.txt',header=T)
DB <- read.table('Datas_bordure.txt',header=T)
attach(DB)
attach(DI)

# mesures dans l'itnerligne en r�alit� !!!  
Areab2 <- Areab ;RELb2 <- RELb ; CAPb2 <- CAPb
ALTb2 <- ALTb ;COPAb2 <- COPAb; Rb2 <- Rb
for (i in c(6:17,22,27,31,36)){
for (j in 1:3){
for (k in c('C1','C2','C3','S1','S2','S3')){
AI <- ATOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(AI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, AI[6*2*(w-1)+1+(l-1)],AI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, AI[6*(N-1)+l])}}
Areab2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI
               
RELI <- RELTOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(RELI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, RELI[6*2*(w-1)+1+(l-1)],RELI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, RELI[6*(N-1)+l])}}
RELb2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI

CAPI <- CAPTOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(CAPI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, CAPI[6*2*(w-1)+1+(l-1)],CAPI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, CAPI[6*(N-1)+l])}}
CAPb2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI

ALTI <- ALTTOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(ALTI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, ALTI[6*2*(w-1)+1+(l-1)],ALTI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, ALTI[6*(N-1)+l])}}
ALTb2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI

COPAI <- COPATOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(COPAI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, COPAI[6*2*(w-1)+1+(l-1)],COPAI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, COPAI[6*(N-1)+l])}}
COPAb2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI

RI <- RTOT[AGE==i][BLOCO[AGE==i]==j][TRAT[AGE==i][BLOCO[AGE==i]==j]==k]
N <- length(RI)/6
XI<-c();
for (l in 1:6){
for (w in 1:(N/2)){XI <- c(XI, RI[6*2*(w-1)+1+(l-1)],RI[6*2*w-(l-1)])}
if (N==9|N==7){XI <- c(XI, RI[6*(N-1)+l])}}
Rb2[MONb==i][BLOC[MONb==i]==j][TRATb[MONb==i][BLOC[MONb==i]==j]==k][Ty[Bl==j][Tr[Bl==j]==k]=='PI'] <- XI

}}}

  MAT <- data.frame(MONb,DATb,BLOC,TRATb,Areab2,ALTb2,COPAb2,CAPb2,Rb2,RELb2)

  MAT2 <- data.frame(Bl,Tr,Ty,X,Y)
  names(MAT2) <- c('Bloc','Trat','Typ','Xpos','Ypos')

  write.table(MAT,'All_datas.txt',row.names=F,col.names=T)
  write.table(MAT2,'Position.txt',row.names=F,col.names=T)









N=9

X<-c();
for (l in 1:6){
for (w in 1:(N/2)){X <- c(X, 6*2*(w-1)+1+(l-1),6*2*w-(l-1))}
}

