set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Effet des traitements/Architecture de base")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

DAT <- read.table('SURF_SYMP.txt',header=T)

SYMC1 <- DAT$Surf_Symp_tot[DAT$Trat=='C1']
SYMS1 <- DAT$Surf_Symp_tot[DAT$Trat=='S1']
LAC1 <- DAT$Surf_feuil_tot[DAT$Trat=='C1']
LAS1 <- DAT$Surf_feuil_tot[DAT$Trat=='S1']
AGEC1 <- DAT$Idade[DAT$Trat=='C1']
AGES1 <- DAT$Idade[DAT$Trat=='S1']




DAT <- read.table('Datas_intern.txt',header=T)

DAT2 <- DAT[1,]
for (i in 2:dim(DAT)[1]){
if (DAT$TRAT[i]=='C2'| DAT$TRAT[i]=='S2'){
DAT2 <- DAT2}
else {
DAT2 <- rbind(DAT2,DAT[i,])}}

attach(DAT2)


TRAT <- as.factor(as.vector(TRAT))

WEFF<-c() ; KEFF<-c()
for (i in 1:length(TRAT)){
if(TRAT[i]=='C1'){WEFF <- c(WEFF,'+W');KEFF <- c(KEFF,'-K')}
else if(TRAT[i]=='C3'){WEFF <- c(WEFF,'+W');KEFF <- c(KEFF,'+K')}
else if(TRAT[i]=='S1'){WEFF <- c(WEFF,'-W');KEFF <- c(KEFF,'-K')}
else if(TRAT[i]=='S3'){WEFF <- c(WEFF,'-W');KEFF <- c(KEFF,'+K')}}

require(nlme)

COP <- COPATOT/ALTTOT
RC <- (RTOT+RELTOT)/2

ana12 <-  lme(ATOT~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit)
ana13 <-  lme(ATOT~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit,  weights=varIdent(form= ~ 1|AGE)) # bon
ana22 <-  lme(ALTTOT~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit)
ana32 <-  lme(RC~WEFF*KEFF*AGE,random=~1+1|BLOCO/WEFF,na.action=na.omit)


contrasts(Trat2) <- matrix(c(1,-1,0,0,1,0,-1,0,1,0,0,-1),nrow=4)

ana1 <- lm(COP~Trat2)
ana2 <- lm(RC~AGE*Trat2)
ana3 <- lm(ATOT~AGE*Trat2)
ana4 <- lm(BIOMTOT~AGE*Trat2)
ana5 <- lm(NLTOT~AGE*Trat2)
ana6 <- lm(ATOT~BIOMTOT+AGE*Trat2)

library(nlme)
#hauteur
ana1 <- gls(ALTTOT~AGE*Trat2,method='ML',na.action=na.omit)
ana2 <- gls(ALTTOT~AGE+Trat2,method='ML',na.action=na.omit)
ana11 <- gls(ALTTOT~AGE*Trat2,  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)
ana12 <- gls(ALTTOT~AGE*Trat2,  weights=varIdent(form= ~ 1|Trat2),method='ML',na.action=na.omit)
ana13 <- gls(ALTTOT~AGE*Trat2,  weights=varIdent(form= ~ 1|AGE),method='ML',na.action=na.omit)
ana14 <- gls(ALTTOT~AGE*Trat2,  correlation=corAR1(form= ~ 1 ),method='ML',na.action=na.omit)
ana15 <- gls(ALTTOT~AGE*Trat2,  correlation=corAR1(form= ~ 1|AGE),  weights=varIdent(form= ~ 1|AGE),method='ML',na.action=na.omit)
plot(ana15)
qqplot(ana15)
contrasts(Trat2) <- matrix(c(1,-1,1,-1,1,0,-1,0,0,1,0,-1),nrow=4)
ana15 <- gls(ALTTOT~AGE*Trat2,  correlation=corAR1(form= ~ 1|AGE),  weights=varIdent(form= ~ 1|AGE),method='REML',na.action=na.omit)
contrasts(Trat2) <- matrix(c(1,-1,0,0,1,0,-1,0,1,0,0,-1),nrow=4)

ana1 <- gls(RC~AGE*Trat2,method='ML',na.action=na.omit)
ana2 <- gls(RC~AGE+Trat2,method='ML',na.action=na.omit)
ana11 <- gls(RC~AGE*Trat2,  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)
ana12 <- gls(RC~AGE*Trat2,  weights=varIdent(form= ~ 1|Trat2),method='ML',na.action=na.omit)
ana13 <- gls(RC~AGE*Trat2,  weights=varIdent(form= ~ 1|AGE),method='ML',na.action=na.omit)
ana14 <- gls(RC~AGE*Trat2,  correlation=corAR1(form= ~ 1 ),method='ML',na.action=na.omit)
ana15 <- gls(RC~AGE*Trat2,  correlation=corAR1(form= ~ 1|AGE),  weights=varIdent(form= ~ 1|AGE),method='ML',na.action=na.omit)
contrasts(Trat2) <- matrix(c(1,-1,1,-1,1,0,-1,0,0,1,0,-1),nrow=4)
ana15 <- gls(RC~AGE*Trat2,  correlation=corAR1(form= ~ 1|AGE),  weights=varIdent(form= ~ 1|AGE),method='REML',na.action=na.omit)
contrasts(Trat2) <- matrix(c(1,-1,0,0,1,0,-1,0,1,0,0,-1),nrow=4)

Cop <-c()
for (i in levels(Trat2)){
for (j in as.numeric(levels(as.factor(AGE)))){
Cop <- c(Cop,COP[Trat2==i][AGE[Trat2==i]==j][1])}}

ana1 <- gls(COP~Trat2,method='ML',na.action=na.omit)
ana11 <- gls(COP~Trat2,  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)
ana12 <- gls(COP~Trat2,  correlation=corAR1(form= ~ 1 ),method='ML',na.action=na.omit)
ana13 <- gls(COP~Trat2,  correlation=corAR1(form= ~ 1|BLOCO),  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)



MALT<-rep(NA,4);SdALT<-rep(NA,4)
MR <- rep(NA,4);SdR<-rep(NA,4)
MA<-rep(NA,4);SdA<-rep(NA,4)
MB<-rep(NA,4);SdB<-rep(NA,4)
MNL<-rep(NA,4);SdNL<-rep(NA,4)
MSLA<-rep(NA,4);SdSLA<-rep(NA,4)
for (i in levels(as.factor(AGE))){
MALT <- data.frame(MALT,c(mean(ALTTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(ALTTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(ALTTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(ALTTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdALT <- data.frame(SdALT,c(sd(ALTTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(ALTTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(ALTTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(ALTTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
MR <- data.frame(MR,c(mean(RC[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(RC[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(RC[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(RC[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdR <- data.frame(SdR,c(sd(RC[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(RC[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(RC[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(RC[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
MA <- data.frame(MA,c(mean(ATOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(ATOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(ATOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(ATOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdA <- data.frame(SdA,c(sd(ATOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(ATOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(ATOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(ATOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
MB <- data.frame(MB,c(mean(BIOMTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(BIOMTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(BIOMTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(BIOMTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdB <- data.frame(SdB,c(sd(BIOMTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(BIOMTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(BIOMTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(BIOMTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
MNL <- data.frame(MNL,c(mean(NLTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(NLTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(NLTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(NLTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdNL <- data.frame(SdNL,c(sd(NLTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(NLTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(NLTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(NLTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
MSLA <- data.frame(MSLA,c(mean(ATOT[AGE==i][Trat2[AGE==i]=='C1']/BIOMTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),mean(ATOT[AGE==i][Trat2[AGE==i]=='C3']/BIOMTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      mean(ATOT[AGE==i][Trat2[AGE==i]=='S1']/BIOMTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),mean(ATOT[AGE==i][Trat2[AGE==i]=='S3']/BIOMTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
SdSLA <- data.frame(SdSLA,c(sd(ATOT[AGE==i][Trat2[AGE==i]=='C1']/BIOMTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T),sd(ATOT[AGE==i][Trat2[AGE==i]=='C3']/BIOMTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T),
                      sd(ATOT[AGE==i][Trat2[AGE==i]=='S1']/BIOMTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T),sd(ATOT[AGE==i][Trat2[AGE==i]=='S3']/BIOMTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)))
}

LAIC1<-c();LAIC3<-c();LAIS1<-c();LAIS3<-c()
SLAC1<-c();SLAC3<-c();SLAS1<-c();SLAS3<-c()
for (i in levels(as.factor(AGE))){
  LAIC1 <- c(LAIC1, sum(ATOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T)/(length(ATOT[AGE==i][Trat2[AGE==i]=='C1'])*6))
  LAIC3 <- c(LAIC3, sum(ATOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T)/(length(ATOT[AGE==i][Trat2[AGE==i]=='C3'])*6))
  LAIS1 <- c(LAIS1, sum(ATOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T)/(length(ATOT[AGE==i][Trat2[AGE==i]=='S1'])*6))
  LAIS3 <- c(LAIS3, sum(ATOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)/(length(ATOT[AGE==i][Trat2[AGE==i]=='S3'])*6))

  SLAC1 <- c(SLAC1, sum(ATOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T)/sum(BIOMTOT[AGE==i][Trat2[AGE==i]=='C1'],na.rm=T))
  SLAC3 <- c(SLAC3, sum(ATOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T)/sum(BIOMTOT[AGE==i][Trat2[AGE==i]=='C3'],na.rm=T))
  SLAS1 <- c(SLAS1, sum(ATOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T)/sum(BIOMTOT[AGE==i][Trat2[AGE==i]=='S1'],na.rm=T))
  SLAS3 <- c(SLAS3, sum(ATOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T)/sum(BIOMTOT[AGE==i][Trat2[AGE==i]=='S3'],na.rm=T))
}
  
  
Age <- as.numeric(levels(as.factor(AGE)))

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(Age,MALT[1,2:17],type='b',pch=16,col=2,lwd=2,xlab='Age (months)',ylab='Tree height (m)',cex.lab=1.2,ylim=c(0,18),xlim=c(0,38))
points(Age,MALT[2,2:17],type='b',pch=16,lwd=2,col=4)
points(Age,MALT[3,2:17],type='b',pch=4,lty=2,lwd=2,col=2)
points(Age,MALT[4,2:17],type='b',pch=4,lty=2,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
for (i in 1:16){for (j in 1){
add.errorbars(Age[i],MALT[j,i+1],SE=SdALT[j,i+1],color=2,direction='vert')}}  
legend('bottomright',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=c(2,4,2,4),pch=c(16,16,4,4),lty=c(1,1,2,2),bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('Variance analysis:','  - Treatment: F = 21.4***',
                          '      C1&S1 vs C3&S3: t=-4.39***','      C1 vs S1: t=31.10***','      C3 vs S3: t=5.53***'),pch=NA,col=1,bg='white',box.col=0,cex=1.2)
box()

ana1 <- gls(ATOT~AGE*Trat2,method='ML',na.action=na.omit)
ana2 <- gls(ATOT~AGE+Trat2,method='ML',na.action=na.omit)
ana11 <- gls(ATOT~AGE*Trat2,  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)
ana12 <- gls(ATOT~AGE*Trat2,  correlation=corAR1(form= ~ 1 ),method='ML',na.action=na.omit)
ana13 <- gls(ATOT~AGE*Trat2,  correlation=corAR1(form= ~ 1|BLOCO),  weights=varIdent(form= ~ 1|BLOCO),method='ML',na.action=na.omit)
                      
par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(Age,LAIC1,type='b',pch=16,lwd=2,col=2,xlab='Age (months)',ylab='LAI (m�/m�)',cex.lab=1.2,ylim=c(0,8),xlim=c(0,38))
points(Age,LAIC3,type='b',pch=16,lwd=2,col=4)
points(Age,LAIS1,type='b',pch=4,lwd=2,col=2,lty=2)
points(Age,LAIS3,type='b',pch=4,lwd=2,col=4,lty=2)
grid(nx=NA,ny=NULL,col=1)
legend('topright',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=c(2,4,2,4),lty=c(1,1,2,2),pch=c(16,16,4,4),bg='white',box.col=0,cex=1)
legend('topleft',legend=c('Variance analysis on leaf area per tree:','  - Age: F = 4282***','  - Treatment: F = 506***',
                          '      C1&S1 vs C3&S3: t=-5.2***','      C1 vs S1: t=-0.03','      C3 vs S3: t=-11.2***','  - Interaction: F = 84***'),
                          pch=NA,col=1,bg='white',box.col=0,cex=1)
box()


plot(Age,MR[1,2:15],type='b',pch=16,lwd=2,xlab='Age (months)',ylab='Mean Crown diameter (m)',cex.lab=1.2,ylim=c(1,5),xlim=c(0,30))
points(Age,MR[2,2:15],type='b',pch=16,lwd=2,col=2)
points(Age,MR[3,2:15],type='b',pch=4,lwd=2,col=3)
points(Age,MR[4,2:15],type='b',pch=4,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
legend('bottomright',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=1:4,pch=c(1,1,4,4),lty=1,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('Variance analysis:','  - Age: F = 13360***','  - Treatment: F = 459***',
'      C1&S1 vs C3&S3: t=-21.5***','      C1 vs S1: t=-1.5','      C3 vs S3: t=-2.9**','  - Interaction: F = 2.9***'),pch=NA,col=1,bg='white',
        box.col=0,cex=1.1)
box()
for (i in 1:14){for (j in 1){
add.errorbars(Age[i],MR[j,i+1],SE=SdR[j,i+1],color=j,direction='vert')}}  

plot(Age,MB[1,2:15],type='b',pch=16,lwd=2,xlab='Age (months)',ylab='Leaf dry weight (kg/tree)',cex.lab=1.2,ylim=c(0,5),xlim=c(0,30))
points(Age,MB[2,2:15],type='b',pch=16,lwd=2,col=2)
points(Age,MB[3,2:15],type='b',pch=16,lwd=2,col=3)
points(Age,MB[4,2:15],type='b',pch=16,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
legend('bottomright',legend=c('C1 : -K +R','C3 : +K +R','S1 : -K -R','S3 : +K -R'),col=1:4,pch=c(1,1,4,4),lty=1,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('Variance analysis:','  - Age: F = 11697***','  - Treatment: F = 764***',
'      C1&S1 vs C3&S3: t=6.1***','      C1 vs S1: t=0.5','      C3 vs S3: t=-7.9***','  - Interaction: F = 273***'),pch=NA,col=1,bg='white',box.col=0,cex=1.2)
box()
for (i in 1:14){for (j in 3){
add.errorbars(Age[i],MB[j,i+1],SE=SdB[j,i+1],color=j,direction='vert')}}  



plot(Age,SLAC1,type='b',pch=16,lwd=2,xlab='Age (months)',ylab='SLA',cex.lab=1.2,ylim=c(5,20))
points(Age,SLAC3,type='b',pch=16,lwd=2,col=2)
points(Age,SLAS1,type='b',pch=16,lwd=2,col=3)
points(Age,SLAS3,type='b',pch=16,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
legend('topright',legend=levels(Trat2),col=1:4,pch=16,lty=1,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('Variance analysis:','  - Age: F = 225.3***','  - Treatment: F = 4.2**',
                          '      C1 vs C3: t=2.3*','      C1 vs S1: t=-3.0**','      C1 vs S3: t=1.46'),pch=NA,col=1,bg='white',box.col=0,cex=1.2)
box()

for (i in 1:5){for (j in 1){
add.errorbars(AGE[i],MSLA[j,i+1],SE=SdSLA[j,i+1],color=j,direction='up')}}  

plot(Age,MSLA[1,2:15],type='b',pch=16,lwd=2,xlab='Age (months)',ylab='SLA (m�/kg)',cex.lab=1.2,ylim=c(5,25))
points(Age,MSLA[2,2:15],type='b',pch=16,lwd=2,col=2)
points(Age,MSLA[3,2:15],type='b',pch=16,lwd=2,col=3)
points(Age,MSLA[4,2:15],type='b',pch=16,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
legend('bottomleft',legend=levels(Trat2),col=1:4,pch=16,lty=1,bg='white',box.col=0,cex=1.2)
legend('topright',legend=c('Variance analysis with leaf dry weight as covariable:','  - Age: F = 4307***','  - Treatment: F = 103***',
'      C1&S1 vs C3&S3: t=-36.3***','      C1 vs S1: t=-1.7','      C3 vs S3: t=-11.9***','  - Interaction: F = 718***'),pch=NA,col=1,bg='white',box.col=0,cex=1.2)
box()
for (i in 1:14){for (j in 3){
add.errorbars(Age[i],MSLA[j,i+1],SE=SdSLA[j,i+1],color=j,direction='vert')}}  

                      
                      
plot(Age,MNL[1,2:15],type='b',pch=16,lwd=2,xlab='Age (months)',ylab='Number of leaves per tree',cex.lab=1.2,ylim=c(0,15000))
points(Age,MNL[2,2:15],type='b',pch=16,lwd=2,col=2)
points(Age,MNL[3,2:15],type='b',pch=16,lwd=2,col=3)
points(Age,MNL[4,2:15],type='b',pch=16,lwd=2,col=4)
grid(nx=NA,ny=NULL,col=1)
legend('bottomright',legend=levels(Trat2),col=1:4,pch=16,lty=1,bg='white',box.col=0,cex=1.2)
legend('topleft',legend=c('Variance analysis with leaf dry weight as covariable:','  - Age: F = 887***','  - Treatment: F = 234***',
      '      C1&S1 vs C3&S3: t=-10.3***','      C1 vs S1: t=1.1','      C3 vs S3: t=-4.4***','  - Interaction: F = 14***'),
      pch=NA,col=1,bg='white',box.col=0,cex=1)
box()
for (i in 1:14){for (j in 2){
add.errorbars(Age[i],MNL[j,i+1],SE=SdNL[j,i+1],color=j,direction='vert')}}  
                      
                      
                      
                      
# LAI moyen R et alt

age <- as.numeric(levels(as.factor(AGE)))
LAI <- c(sum(ATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]],na.rm=T)/(length(ATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]])*6),
         sum(ATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]],na.rm=T)/(length(ATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]])*6),
         sum(ATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]],na.rm=T)/(length(ATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]])*6),
         sum(ATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]],na.rm=T)/(length(ATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]])*6))

ALT <- c(mean(ALTTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]],na.rm=T),
         mean(ALTTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]],na.rm=T),
         mean(ALTTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]],na.rm=T),
         mean(ALTTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]],na.rm=T))
R <- c(mean(RTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]],na.rm=T),
         mean(RTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]],na.rm=T),
         mean(RTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]],na.rm=T),
         mean(RTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]],na.rm=T))
REL <- c(mean(RELTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]],na.rm=T),
         mean(RELTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]],na.rm=T),
         mean(RELTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]],na.rm=T),
         mean(RELTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]],na.rm=T))
COPA <- c(mean(COPATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[1]],na.rm=T),
         mean(COPATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[1]],na.rm=T),
         mean(COPATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[1]],na.rm=T),
         mean(COPATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[1]],na.rm=T))

for (i in 2:16){
LAI <- data.frame(LAI,
       c(sum(ATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]],na.rm=T)/(length(ATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]])*6),
         sum(ATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]],na.rm=T)/(length(ATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]])*6),
         sum(ATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]],na.rm=T)/(length(ATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]])*6),
         sum(ATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]],na.rm=T)/(length(ATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]])*6)))
ALT <- data.frame(ALT,c(mean(ALTTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]],na.rm=T),
         mean(ALTTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]],na.rm=T),
         mean(ALTTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]],na.rm=T),
         mean(ALTTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]],na.rm=T)))
R <- data.frame(R,c(mean(RTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]],na.rm=T),
         mean(RTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]],na.rm=T),
         mean(RTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]],na.rm=T),
         mean(RTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]],na.rm=T)))
REL <- data.frame(REL,c(mean(RELTOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]],na.rm=T),
         mean(RELTOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]],na.rm=T),
         mean(RELTOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]],na.rm=T),
         mean(RELTOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]],na.rm=T)))
COPA <- data.frame(COPA,c(mean(COPATOT[TRAT=='C3'][AGE[TRAT=='C3']==age[i]],na.rm=T),
         mean(COPATOT[TRAT=='C1'][AGE[TRAT=='C1']==age[i]],na.rm=T),
         mean(COPATOT[TRAT=='S1'][AGE[TRAT=='S1']==age[i]],na.rm=T),
         mean(COPATOT[TRAT=='S3'][AGE[TRAT=='S3']==age[i]],na.rm=T)))
}

names(LAI) <- age
names(ALT) <- age
names(R) <- age
names(REL) <- age
names(COPA) <- age

LAI[1,]/LAI[2,]
ALT[1,]/ALT[2,]
R[1,]/R[2,]
REL[1,]/REL[2,]
COPA[1,]/COPA[2,]

LAI[4,]/LAI[3,]
ALT[4,]/ALT[3,]
R[4,]/R[3,]
REL[4,]/REL[3,]
COPA[4,]/COPA[3,]





