set1<-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Largeur feuille")
RMSE <- function(fit){ A <-summary(fit)$sigma^2 ; print(A)}
add.errorbars <- function(x,y,SE,direction,barlen=0.04){
if(direction=="up")arrows(x0=x, x1=x, y0=y, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="down")arrows(x0=x, x1=x, y0=y, y1=y - SE, code=3,
angle=90, length=barlen)
if(direction=="left")arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="right")arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="vert")arrows(x0=x, x1=x, y0=y-SE, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="hor")arrows(x0=x-SE, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
}


setwd(set1)

dat <- read.table('datas.txt',header=T)
attach(dat)

LA <- largeur_cm[traitement=='C3']
A <- Area[traitement=='C3']

fit <- nls(LA~a * A^b,start=c(a=0.3,b=1))
X <- seq(min(A),max(A),length.out=100)

par(mar=c(4.5,4.5,1,1))
par(xaxs='i',yaxs='i')
plot(A,LA,xlab='Leaf Area (cm�)',ylab='Leaf width (cm)',cex.lab=1.2,pch=16,col=4)
points(X, summary(fit)$coef[1,1]*X^summary(fit)$coef[2,1],type='l',lwd=2)
grid(nx=NA,ny=NULL,col = 1, lty = "dotted", lwd = 1)
legend('bottomright',legend=c( expression(W[leaf]~'= 0.0454 * LA'^0.542), paste('RMSE = ',round(RMSE(fit) ,3))),pch=NA,cex=1.2,bg='white',box.col=0)


Y1 <- c(largeur_cm[traitement=='C1'|traitement=='S1'])
Y2 <- c(largeur_cm[traitement=='C3'|traitement=='S3'])
X1 <- c(Area[traitement=='C1'|traitement=='S1'])
X2 <- c(Area[traitement=='C3'|traitement=='S3'])
# on suppose que m�me puissance juste le coefficient de devant qui change
fit2 <- nls((Y1-Y2)~(a1*X1^b)-(a2*X2^b),start=c(a1=0.3,a2=0.3,b=1))

FACT <- summary(fit2)$coef[2,1]/summary(fit2)$coef[1,1]#a2/a1


detach(dat)






dat <- read.table('All_LW_EUCFLUX.txt',header=T)
attach(dat)

LW <- summary(fit)$coef[1,1]*AREA^summary(fit)$coef[2,1]

AGE <- replace(as.vector(DATE),c(as.vector(DATE)=='04_10'),5)
AGE <- replace(AGE,c(DATE=='08_10'),9)
AGE <- replace(AGE,c(DATE=='11_10'),12)
AGE <- replace(AGE,c(DATE=='02_11'),15)
AGE <- replace(AGE,c(DATE=='05_11'),18)
AGE <- replace(AGE,c(DATE=='12_11'),25)
AGE <- replace(AGE,c(DATE=='06_12'),31)

TT <- c()
for (i in 1:length(NUM)){
if (NUM[i] <= 3){TT <- c(TT,'S')}
else if (NUM[i] >= 8){ TT <- c(TT,'B')}
else { TT <- c(TT,'M')}
}



Di <- c('08_10','11_10','02_11','05_11','12_11','06_12')
NUM2<-NUM
for (i in 1:6){
NUM2 <- replace(NUM2,c(as.vector(DATE)==Di[i]),NUM[as.vector(DATE)==Di[i]]+i*10)}

AGE <- as.numeric(AGE)

library(nlme)
fit1 <- lme(LW~DATE+TT+POS, random=~1|NUM2)
fit2 <- lme(LW~AGE+TT, random=~1|NUM2)

contrasts(POS) <- matrix(c(0,1,0,0,0,1),nrow=3)
contrasts(POS) <- matrix(c(1,1,-2,1,-1,0),nrow=3)

contrasts(DATE) <- matrix(c(-1,6,-1,-1,-1,-1,-1,
                            -1,0,-1,-1,5,-1,-1,
                            -1,0,-1,-1,0,4,-1,
                            1,0,1,-1,0,0,-1,
                            1,0,-1,0,0,0,0,
                            0,0,0,1,0,0,-1),nrow=7)


fit1 <- lm(LW~AGE+POS)

MAT <- data.frame(AGE,TT,LW)
write.table(MAT,'leafwidth.txt',row.names=F,col.names=T) 

MLW<-c();sdLW<-c()
for (i in c(5,9,12,15,18,25,31)){
MLW <- c(MLW, mean(LW[AGE==i][TT[AGE==i]=='S']),
              mean(LW[AGE==i][TT[AGE==i]=='M']),
              mean(LW[AGE==i][TT[AGE==i]=='B']))
sdLW <- c(sdLW, sd(LW[AGE==i][TT[AGE==i]=='S']),
              sd(LW[AGE==i][TT[AGE==i]=='M']),
              sd(LW[AGE==i][TT[AGE==i]=='B']))
}

P <- rep(c('S','M','B'),7)
A <- c(5,9,12,15,18,25,31)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i',xaxs='i')
plot(A, MLW[P=='S'],type='b',lty=3,pch=16,lwd=2,cex.lab=1.2,xlab='Age (months)',ylab='Leaf width (cm)',ylim=c(1,6),xlim=c(0,32))
points(A,MLW[P=='M'],type='b',lty=2,pch=15,lwd=2)
points(A,MLW[P=='B'],type='b',lty=1,pch=17,lwd=2)
grid(nx=NA,ny=NULL,col=1)
text(A,c(4.5,rep(5.5,6)),labels=c('a','b','c',rep('d',4)),cex=1.2,font=3)
legend('bottomright',legend=c('Variance analysis :','  - Age of tree F = 7.546**','  - Type of tree F = 2.095'),
                    pch=NA,cex=1.2,bg='white',box.col=0)
legend('topleft',legend=c('Small trees,','Medium trees','Big trees'), pch=c(16,15,17),lty=c(3,2,1),cex=1.2,bg='white',box.col=0)
for (i in 1:7){
add.errorbars(A[i],MLW[P=='S'][i],SE=sdLW[P=='S'][i],direction='vert',barlen=0.04)}
box()
































