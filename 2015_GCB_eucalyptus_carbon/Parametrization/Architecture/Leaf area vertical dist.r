set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Effet des traitements/Leaf area distribution")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

DAT <- read.table('Biomass.txt',header=T)
attach(DAT)

Bic <- function(fit,p,n){
A <- AIC(fit) + p*(log10(n)-2)
print(A)}
RMSE <- function(fit){ A <-summary(fit)$sigma^2 ; print(A)}

An <- c(Surf_feuil_Inf,Surf_feuil_Moy,Surf_feuil_Sup) / rep(Surf_feuil_tot,3)
H <-  c(rep(1/6,276),rep(1/2,276),rep(5/6,276))
  trat <- rep(Trat,3)

An2 <- c(Surf_feuil_Inf[37:276],Surf_feuil_Moy[37:276],Surf_feuil_Sup[37:276])/ rep(Surf_feuil_tot[37:276],3)
TT2 <- rep(Arvore[37:276],3)
trat2 <- rep(Trat[37:276],3)
age <- rep(Idade[37:276],3)
H2 <-  c(rep(1/6,240),rep(1/2,240),rep(5/6,240))

tt<-c()
for (i in 1:length(An2)){
if(TT2[i]<=2){tt<-c(tt,'p')}
else if (TT2[i] >= 6){tt<-c(tt,'g')}
else {tt<-c(tt,'m')}
}

# on sort les distributions
A11 <- An2[trat2=='C1'][age[trat2=='C1']==11]
H11 <- H2[trat2=='C1'][age[trat2=='C1']==11]
A16 <- An2[trat2=='C1'][age[trat2=='C1']==16]
H16 <- H2[trat2=='C1'][age[trat2=='C1']==16]
A23 <- An2[trat2=='C1'][age[trat2=='C1']==23]
H23 <- H2[trat2=='C1'][age[trat2=='C1']==23]
A28 <- An2[trat2=='C1'][age[trat2=='C1']==28]
H28 <- H2[trat2=='C1'][age[trat2=='C1']==28]
A36 <- An2[trat2=='C1'][age[trat2=='C1']==36]
H36 <- H2[trat2=='C1'][age[trat2=='C1']==36]
fit1 <- nls(3*A11~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
COEFC1 <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
LADfun <- function (H,a,b,d){a*H^b *(1-H)^d}
COEFC1[1] <- COEFC1[1]/integrate(LADfun,lower=0,upper=1,a=COEFC1[1],b=COEFC1[2],d=COEFC1[3])$value
COEFC1[4] <- COEFC1[4]/integrate(LADfun,lower=0,upper=1,a=COEFC1[4],b=COEFC1[5],d=COEFC1[6])$value
COEFC1[7] <- COEFC1[7]/integrate(LADfun,lower=0,upper=1,a=COEFC1[7],b=COEFC1[8],d=COEFC1[9])$value
COEFC1[10] <- COEFC1[10]/integrate(LADfun,lower=0,upper=1,a=COEFC1[10],b=COEFC1[11],d=COEFC1[12])$value
COEFC1[13] <- COEFC1[13]/integrate(LADfun,lower=0,upper=1,a=COEFC1[13],b=COEFC1[14],d=COEFC1[15])$value

A11 <- An2[trat2=='C3'][age[trat2=='C3']==11]
H11 <- H2[trat2=='C3'][age[trat2=='C3']==11]
A16 <- An2[trat2=='C3'][age[trat2=='C3']==16]
H16 <- H2[trat2=='C3'][age[trat2=='C3']==16]
A23 <- An2[trat2=='C3'][age[trat2=='C3']==23]
H23 <- H2[trat2=='C3'][age[trat2=='C3']==23]
A28 <- An2[trat2=='C3'][age[trat2=='C3']==28]
H28 <- H2[trat2=='C3'][age[trat2=='C3']==28]
A36 <- An2[trat2=='C3'][age[trat2=='C3']==36]
H36 <- H2[trat2=='C3'][age[trat2=='C3']==36]
fit1 <- nls(3*A11~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
COEFC3 <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
COEFC3[1] <- COEFC3[1]/integrate(LADfun,lower=0,upper=1,a=COEFC3[1],b=COEFC3[2],d=COEFC3[3])$value
COEFC3[4] <- COEFC3[4]/integrate(LADfun,lower=0,upper=1,a=COEFC3[4],b=COEFC3[5],d=COEFC3[6])$value
COEFC3[7] <- COEFC3[7]/integrate(LADfun,lower=0,upper=1,a=COEFC3[7],b=COEFC3[8],d=COEFC3[9])$value
COEFC3[10] <- COEFC3[10]/integrate(LADfun,lower=0,upper=1,a=COEFC3[10],b=COEFC3[11],d=COEFC3[12])$value
COEFC3[13] <- COEFC3[13]/integrate(LADfun,lower=0,upper=1,a=COEFC3[13],b=COEFC3[14],d=COEFC3[15])$value

A11 <- An2[trat2=='S1'][age[trat2=='S1']==11]
H11 <- H2[trat2=='S1'][age[trat2=='S1']==11]
A16 <- An2[trat2=='S1'][age[trat2=='S1']==16]
H16 <- H2[trat2=='S1'][age[trat2=='S1']==16]
A23 <- An2[trat2=='S1'][age[trat2=='S1']==23]
H23 <- H2[trat2=='S1'][age[trat2=='S1']==23]
A28 <- An2[trat2=='S1'][age[trat2=='S1']==28]
H28 <- H2[trat2=='S1'][age[trat2=='S1']==28]
fit1 <- nls(3*A11~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
A36 <- An2[trat2=='S1'][age[trat2=='S1']==36]
H36 <- H2[trat2=='S1'][age[trat2=='S1']==36]
COEFS1 <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
COEFS1[1] <- COEFS1[1]/integrate(LADfun,lower=0,upper=1,a=COEFS1[1],b=COEFS1[2],d=COEFS1[3])$value
COEFS1[4] <- COEFS1[4]/integrate(LADfun,lower=0,upper=1,a=COEFS1[4],b=COEFS1[5],d=COEFS1[6])$value
COEFS1[7] <- COEFS1[7]/integrate(LADfun,lower=0,upper=1,a=COEFS1[7],b=COEFS1[8],d=COEFS1[9])$value
COEFS1[10] <- COEFS1[10]/integrate(LADfun,lower=0,upper=1,a=COEFS1[10],b=COEFS1[11],d=COEFS1[12])$value
COEFS1[13] <- COEFS1[13]/integrate(LADfun,lower=0,upper=1,a=COEFS1[13],b=COEFS1[14],d=COEFS1[15])$value

A11 <- An2[trat2=='S3'][age[trat2=='S3']==11]
H11 <- H2[trat2=='S3'][age[trat2=='S3']==11]
A16 <- An2[trat2=='S3'][age[trat2=='S3']==16]
H16 <- H2[trat2=='S3'][age[trat2=='S3']==16]
A23 <- An2[trat2=='S3'][age[trat2=='S3']==23]
H23 <- H2[trat2=='S3'][age[trat2=='S3']==23]
A28 <- An2[trat2=='S3'][age[trat2=='S3']==28]
H28 <- H2[trat2=='S3'][age[trat2=='S3']==28]
A36 <- An2[trat2=='S3'][age[trat2=='S3']==36]
H36 <- H2[trat2=='S3'][age[trat2=='S3']==36]
fit1 <- nls(3*A11~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
COEFS3 <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
COEFS3[1] <- COEFS3[1]/integrate(LADfun,lower=0,upper=1,a=COEFS3[1],b=COEFS3[2],d=COEFS3[3])$value
COEFS3[4] <- COEFS3[4]/integrate(LADfun,lower=0,upper=1,a=COEFS3[4],b=COEFS3[5],d=COEFS3[6])$value
COEFS3[7] <- COEFS3[7]/integrate(LADfun,lower=0,upper=1,a=COEFS3[7],b=COEFS3[8],d=COEFS3[9])$value
COEFS3[10] <- COEFS3[10]/integrate(LADfun,lower=0,upper=1,a=COEFS3[10],b=COEFS3[11],d=COEFS3[12])$value
COEFS3[13] <- COEFS3[13]/integrate(LADfun,lower=0,upper=1,a=COEFS3[13],b=COEFS3[14],d=COEFS3[15])$value

IDADE <- c(11,11,11,16,16,16,23,23,23,28,28,28,36,36,36)
MAT <- data.frame(IDADE,COEFC1,COEFC3,COEFS1,COEFS3)

write.table(MAT,'Vert_LAD.txt',col.names=T,row.names=F)

C1 11 8.70640366033788 16 1.30602528541323  23 3.3292463740355     1.77692769244567 1.80736404554344
C3  3.15838051049274 3.90196479530218 2.29792582815391 1.2309883197083 1.38935471290204
S1 6.82706572093726  16 prendre C1 3.20242787538901 1.65659671621912 1.38935471290204
S3 : 5.36047724938097 6.90698993348845 3.31322392219301 2.99401454319828 3.2207638158615

S1 : 16

                                            

                                        
AC1 <- An[trat=='C1'];HC1 <- H[trat=='C1']
AC3 <- An[trat=='C3'];HC3 <- H[trat=='C3']
AS1 <- An[trat=='S1'];HS1 <- H[trat=='S1']
AS3 <- An[trat=='S3'];HS3 <- H[trat=='S3']

ID <- c(11,16,23,28,36)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
par(mfrow=c(2,2))
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(AC1,HC1,col=1); for (i in 1:5){
points(COEFC1[IDADE==ID[i]][1] * X ^COEFC1[IDADE==ID[i]][2] * (1-X) ^COEFC1[IDADE==ID[i]][3]/3,X,type='l',lwd=2,col=i+1,lty=1)}
legend('topright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n')
legend('right',legend='C1',pch=NA,cex=1.6,text.font=2,bty='n')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
points(AC3,HC3,col=1); for (i in 1:5){
points(COEFC3[IDADE==ID[i]][1] * X ^COEFC3[IDADE==ID[i]][2] * (1-X) ^COEFC3[IDADE==ID[i]][3]/3,X,type='l',lwd=2,col=i+1,lty=1)}
legend('topright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n')
legend('right',legend='C3',pch=NA,cex=1.6,text.font=2,bty='n')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
points(AS1,HS1,col=1); for (i in 1:5){
points(COEFS1[IDADE==ID[i]][1] * X ^COEFS1[IDADE==ID[i]][2] * (1-X) ^COEFS1[IDADE==ID[i]][3]/3,X,type='l',lwd=2,col=i+1,lty=1)}
legend('topright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n')
legend('right',legend='S1',pch=NA,cex=1.6,text.font=2,bty='n')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
points(AS3,HS3,col=1); for (i in 1:5){
points(COEFS3[IDADE==ID[i]][1] * X ^COEFS3[IDADE==ID[i]][2] * (1-X) ^COEFS3[IDADE==ID[i]][3]/3,X,type='l',lwd=2,col=i+1,lty=1)}
legend('topright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n')
legend('right',legend='S3',pch=NA,cex=1.6,text.font=2,bty='n')




# effet traitement
Aall <- An2[trat2=='C1'|trat2=='C3'|trat2=='S1'|trat2=='S3']
Hall <- H2[trat2=='C1'|trat2=='C3'|trat2=='S1'|trat2=='S3']
fit0 <- nls(3*Aall ~a*Hall ^b*(1-Hall )^d,start=c(a=1,b=0.2,d=0.2))

AC1 <- An2[trat2=='C1'];HC1 <- H2[trat2=='C1']
AC3 <- An2[trat2=='C3'];HC3 <- H2[trat2=='C3']
AS1 <- An2[trat2=='S1'];HS1 <- H2[trat2=='S1']
AS3 <- An2[trat2=='S3'];HS3 <- H2[trat2=='S3']
fitC1 <- nls(3*AC1~a*HC1 ^b*(1-HC1 )^d,start=c(a=1,b=0.2,d=0.2))
fitC3 <- nls(3*AC3~a*HC3 ^b*(1-HC3 )^d,start=c(a=1,b=0.2,d=0.2))
fitS1 <- nls(3*AS1~a*HS1 ^b*(1-HS1 )^d,start=c(a=1,b=0.2,d=0.2))
fitS3 <- nls(3*AS3~a*HS3 ^b*(1-HS3 )^d,start=c(a=1,b=0.2,d=0.2))

Amall <- summary(fit0)$coef[1,1] * Hall ^summary(fit0)$coef[2,1] * (1-Hall) ^summary(fit0)$coef[3,1]/3
AmC1 <- summary(fitC1)$coef[1,1] * HC1 ^summary(fitC1)$coef[2,1] * (1-HC1) ^summary(fitC1)$coef[3,1]/3
AmC3 <- summary(fitC3)$coef[1,1] * HC3 ^summary(fitC3)$coef[2,1] * (1-HC3) ^summary(fitC3)$coef[3,1]/3
AmS1 <- summary(fitS1)$coef[1,1] * HS1 ^summary(fitS1)$coef[2,1] * (1-HS1) ^summary(fitS1)$coef[3,1]/3
AmS3 <- summary(fitS3)$coef[1,1] * HS3 ^summary(fitS3)$coef[2,1] * (1-HS3) ^summary(fitS3)$coef[3,1]/3

aic0 <- AIC2(480,Amall,Aall,1,3)
aic1 <- AIC2(480,c(AmC1,AmC3,AmS1,AmS3),c(AC1,AC3,AS1,AS3),1,12)
bic0 <- BIC2(480,Amall,Aall,1,3)
bic1 <- BIC2(480,c(AmC1,AmC3,AmS1,AmS3),c(AC1,AC3,AS1,AS3),1,12)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(c(AC1,AC3,AS1,AS3),c(HC1,HC3,HS1,HS3),col=1)
points(summary(fitC1)$coef[1,1] * X ^summary(fitC1)$coef[2,1] * (1-X) ^summary(fitC1)$coef[3,1]/3,X,type='l',lwd=2,col=1,lty=1)
points(summary(fitC3)$coef[1,1] * X ^summary(fitC3)$coef[2,1] * (1-X) ^summary(fitC3)$coef[3,1]/3,X,type='l',lwd=2,col=2,lty=1)
points(summary(fitS1)$coef[1,1] * X ^summary(fitS1)$coef[2,1] * (1-X) ^summary(fitS1)$coef[3,1]/3,X, type='l',lwd=2,col=1,lty=2)
points(summary(fitS3)$coef[1,1] * X ^summary(fitS3)$coef[2,1] * (1-X) ^summary(fitS3)$coef[3,1]/3,X, type='l',lwd=2,col=2,lty=2)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1','C3','S3','S1'),pch=NA,lty=c(1,1,2,2),col=c(1,2,1,2),bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic1,1)),paste('BIC = ',round(bic1,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()

#effet pluie
AC1C3 <- c(AC1,AC3);HC1C3 <- c(HC1,HC3)
AS1S3 <- c(AS1,AS3);HS1S3 <- c(HS1,HS3)
fitC1C3 <- nls(3*AC1C3~a*HC1C3 ^b*(1-HC1C3 )^d,start=c(a=1,b=0.2,d=0.2))
fitS1S3 <- nls(3*AS1S3~a*HS1S3 ^b*(1-HS1S3 )^d,start=c(a=1,b=0.2,d=0.2))
AmC1C3 <- summary(fitC1C3)$coef[1,1] * HC1C3 ^summary(fitC1C3)$coef[2,1] * (1-HC1C3) ^summary(fitC1C3)$coef[3,1]/3
AmS1S3 <- summary(fitS1S3)$coef[1,1] * HS1S3 ^summary(fitS1S3)$coef[2,1] * (1-HS1S3) ^summary(fitS1S3)$coef[3,1]/3
aic2 <- AIC2(480,c(AmC1C3,AmS1S3),c(AC1C3,AS1S3),1,6)
bic2 <- BIC2(480,c(AmC1C3,AmS1S3),c(AC1C3,AS1S3),1,6)

# effet K
AC1S1 <- c(AC1,AS1);HC1S1 <- c(HC1,HS1)
AC3S3 <- c(AC3,AS3);HC3S3 <- c(HC3,HS3)
fitC1S1 <- nls(3*AC1S1~a*HC1S1 ^b*(1-HC1S1 )^d,start=c(a=1,b=0.2,d=0.2))
fitC3S3 <- nls(3*AC3S3~a*HC3S3 ^b*(1-HC3S3 )^d,start=c(a=1,b=0.2,d=0.2))
AmC1S1 <- summary(fitC1S1)$coef[1,1] * HC1S1 ^summary(fitC1S1)$coef[2,1] * (1-HC1S1) ^summary(fitC1S1)$coef[3,1]/3
AmC3S3 <- summary(fitC3S3)$coef[1,1] * HC3S3 ^summary(fitC3S3)$coef[2,1] * (1-HC3S3) ^summary(fitC3S3)$coef[3,1]/3
aic3 <- AIC2(480,c(AmC1S1,AmC3S3),c(AC1S1,AC3S3),1,6)
bic3 <- BIC2(480,c(AmC1S1,AmC3S3),c(AC1S1,AC3S3),1,6)


AC1C3S1 <- c(AC1,AC3,AS1);HC1C3S1 <- c(HC1,HC3,HS1)
fitC1C3S1 <- nls(3*AC1C3S1~a*HC1C3S1 ^b*(1-HC1C3S1 )^d,start=c(a=1,b=0.2,d=0.2))
AmC1C3S1 <- summary(fitC1C3S1)$coef[1,1] * HC1C3S1 ^summary(fitC1C3S1)$coef[2,1] * (1-HC1C3S1) ^summary(fitC1C3S1)$coef[3,1]/3
aic3 <- AIC2(270,c(AmC1C3S1,AmS3),c(AC1C3S1,AS3),1,6)
bic3 <- BIC2(270,c(AmC1C3S1,AmS3),c(AC1C3S1,AS3),1,6)
AC1C3S3 <- c(AC1,AC3,AS3);HC1C3S3 <- c(HC1,HC3,HS3)
fitC1C3S3 <- nls(3*AC1C3S3~a*HC1C3S3 ^b*(1-HC1C3S3 )^d,start=c(a=1,b=0.2,d=0.2))
AmC1C3S3 <- summary(fitC1C3S3)$coef[1,1] * HC1C3S3 ^summary(fitC1C3S3)$coef[2,1] * (1-HC1C3S3) ^summary(fitC1C3S3)$coef[3,1]/3
aic4 <- AIC2(270,c(AmC1C3S3,AmS1),c(AC1C3S3,AS1),1,6)
bic4 <- AIC2(270,c(AmC1C3S3,AmS1),c(AC1C3S3,AS1),1,6)

AS1S3 <- c(AS1,AS3);HS1S3 <- c(HS1,HS3)
fitS1S3 <- nls(3*AS1S3~a*HS1S3 ^b*(1-HS1S3 )^d,start=c(a=1,b=0.2,d=0.2))
AmS1S3 <- summary(fitS1S3)$coef[1,1] * HS1S3 ^summary(fitS1S3)$coef[2,1] * (1-HS1S3) ^summary(fitS1S3)$coef[3,1]/3
aic5 <- AIC2(270,c(AmC1C3,AmS1S3),c(AC1C3,AS1S3),1,6)
bic5 <- BIC2(270,c(AmC1C3,AmS1S3),c(AC1C3,AS1S3),1,6)

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(c(AC1C3,AS1,AS3),c(HC1C3,HS1,HS3),col=1)
points(summary(fitC1C3)$coef[1,1] * X ^summary(fitC1C3)$coef[2,1] * (1-X) ^summary(fitC1C3)$coef[3,1]/3,X,type='l',lwd=2,col=1,lty=1)
points(summary(fitS1)$coef[1,1] * X ^summary(fitS1)$coef[2,1] * (1-X) ^summary(fitS1)$coef[3,1]/3,X, type='l',lwd=2,col=2,lty=1)
points(summary(fitS3)$coef[1,1] * X ^summary(fitS3)$coef[2,1] * (1-X) ^summary(fitS3)$coef[3,1]/3,X, type='l',lwd=2,col=4,lty=1)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1 & C3','S1','S3'),pch=NA,lty=c(1),col=c(1,2,4),bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic2,1)),paste('BIC = ',round(bic2,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()

dS1 <- deriv(~1.953183 * X ^0.1901688 * (1-X) ^0.6086362/3,"X")

par(mar=c(4.5,4.5,1,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(AC1C3S3,HC1C3S3,col=2)
points( summary(fitC1C3S3)$coef[1,1] * X ^summary(fitC1C3S3)$coef[2,1] * (1-X) ^summary(fitC1C3S3)$coef[3,1]/3,X,type='l',lwd=2,col=2)
points(AS1,HS1,col=3)
points(summary(fitS1)$coef[1,1] * X ^summary(fitS1)$coef[2,1] * (1-X) ^summary(fitS1)$coef[3,1]/3,X, type='l',lwd=2,col=3)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('C1 & C3 & S3','S1'),pch=1,lty=1,col=2:4,bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic4,1)),paste('BIC = ',round(bic4,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()



# effet type d'arbre

# S1
AnP <- An2[trat2=='S1'][tt[trat2=='S1']=='p']
HP <- H2 [trat2=='S1'][tt[trat2=='S1']=='p']
AnM <- An2[trat2=='S1'][tt[trat2=='S1']=='m']
HM <- H2 [trat2=='S1'][tt[trat2=='S1']=='m']
AnG <- An2[trat2=='S1'][tt[trat2=='S1']=='g']
HG <- H2 [trat2=='S1'][tt[trat2=='S1']=='g']

A0 <- An2[trat2=='S1']; H0 <- H2[trat2=='S1']
fit0 <- nls(3*A0 ~a*H0 ^b*(1-H0 )^d,start=c(a=1,b=0.2,d=0.2))
fit1 <- nls(3*AnP~a*HP^b*(1-HP)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*AnM~a*HM^b*(1-HM)^d,start=c(a=1,b=0.1,d=0.2))
fit3 <- nls(3*AnG~a*HG^b*(1-HG)^d,start=c(a=1,b=0.2,d=0.2))

Am0 <- summary(fit0)$coef[1,1] * H2 ^summary(fit0)$coef[2,1] * (1-H2) ^summary(fit0)$coef[3,1]/3
AmP <- summary(fit1)$coef[1,1] * HP ^summary(fit1)$coef[2,1] * (1-HP) ^summary(fit1)$coef[3,1]/3
AmM <- summary(fit2)$coef[1,1] * HM ^summary(fit2)$coef[2,1] * (1-HM) ^summary(fit2)$coef[3,1]/3
AmG <- summary(fit3)$coef[1,1] * HG ^summary(fit3)$coef[2,1] * (1-HG) ^summary(fit3)$coef[3,1]/3

aic0 <- AIC2(96,Am0,A0,1,3)
aic1 <- AIC2(96,c(AmP,AmM,AmG),c(AnP,AnM,AnG),1,9)
bic0 <- BIC2(96,Am0,A0,1,3)
bic1 <- BIC2(96,c(AmP,AmM,AmG),c(AnP,AnM,AnG),1,9)

AnPM <- c(AnP,AnM);HPM <- c(HP,HM)
fit4 <- nls(3*AnPM~a*HPM ^b*(1-HPM )^d,start=c(a=1,b=0.2,d=0.2))
AmPM <- summary(fit4)$coef[1,1] * HPM ^summary(fit4)$coef[2,1] * (1-HPM) ^summary(fit4)$coef[3,1]/3
aic2 <- AIC2(96,c(AmPM,AmG),c(AnPM,AnG),1,6)
bic2 <- BIC2(96,c(AmPM,AmG),c(AnPM,AnG),1,6)

par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(AnPM,HPM,col=2)
points( summary(fit4)$coef[1,1] * X ^summary(fit4)$coef[2,1] * (1-X) ^summary(fit4)$coef[3,1]/3,X,type='l',lwd=2,col=2)
points(AnG,HG,col=4)
points( summary(fit3)$coef[1,1] * X ^summary(fit3)$coef[2,1] * (1-X) ^summary(fit3)$coef[3,1]/3,X,type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('small and medium trees','big trees'),pch=1,lty=1,col=c(2,4),bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic2,1)),paste('BIC = ',round(bic2,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('Treatment S1')

# autre
AnP <- c(An2[trat2=='C1'][tt[trat2=='C1']=='p'],An2[trat2=='C3'][tt[trat2=='C3']=='p'],An2[trat2=='S3'][tt[trat2=='S3']=='p'])
HP <- c(H2[trat2=='C1'][tt[trat2=='C1']=='p'],H2[trat2=='C3'][tt[trat2=='C3']=='p'],H2[trat2=='S3'][tt[trat2=='S3']=='p'])
AnP <- c(An2[trat2=='C1'][tt[trat2=='C1']=='m'],An2[trat2=='C3'][tt[trat2=='C3']=='m'],An2[trat2=='S3'][tt[trat2=='S3']=='m'])
HP <- c(H2[trat2=='C1'][tt[trat2=='C1']=='m'],H2[trat2=='C3'][tt[trat2=='C3']=='m'],H2[trat2=='S3'][tt[trat2=='S3']=='m'])
AnP <- c(An2[trat2=='C1'][tt[trat2=='C1']=='g'],An2[trat2=='C3'][tt[trat2=='C3']=='g'],An2[trat2=='S3'][tt[trat2=='S3']=='g'])
HP <- c(H2[trat2=='C1'][tt[trat2=='C1']=='g'],H2[trat2=='C3'][tt[trat2=='C3']=='g'],H2[trat2=='S3'][tt[trat2=='S3']=='g'])

A0 <- c(An2[trat2=='C1'],An2[trat2=='C3'],An2[trat2=='S3'])
H0 <- c(H2[trat2=='C1'],H2[trat2=='C3'],H2[trat2=='S3'])
fit0 <- nls(3*A0 ~a*H0 ^b*(1-H0 )^d,start=c(a=1,b=0.2,d=0.2))
fit1 <- nls(3*AnP~a*HP^b*(1-HP)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*AnM~a*HM^b*(1-HM)^d,start=c(a=1,b=0.1,d=0.2))
fit3 <- nls(3*AnG~a*HG^b*(1-HG)^d,start=c(a=1,b=0.2,d=0.2))

Am0 <- summary(fit0)$coef[1,1] * H2 ^summary(fit0)$coef[2,1] * (1-H2) ^summary(fit0)$coef[3,1]/3
AmP <- summary(fit1)$coef[1,1] * HP ^summary(fit1)$coef[2,1] * (1-HP) ^summary(fit1)$coef[3,1]/3
AmM <- summary(fit2)$coef[1,1] * HM ^summary(fit2)$coef[2,1] * (1-HM) ^summary(fit2)$coef[3,1]/3
AmG <- summary(fit3)$coef[1,1] * HG ^summary(fit3)$coef[2,1] * (1-HG) ^summary(fit3)$coef[3,1]/3

aic0 <- AIC2(288,Am0,A0,1,3)
aic1 <- AIC2(288,c(AmP,AmM,AmG),c(AnP,AnM,AnG),1,9)
bic0 <- BIC2(288,Am0,A0,1,3)
bic1 <- BIC2(288,c(AmP,AmM,AmG),c(AnP,AnM,AnG),1,9)

AnPG <- c(AnP,AnG);HPG <- c(HP,HG)
fit4 <- nls(3*AnPG~a*HPG ^b*(1-HPG )^d,start=c(a=1,b=0.2,d=0.2))
AmPG <- summary(fit4)$coef[1,1] * HPG ^summary(fit4)$coef[2,1] * (1-HPG) ^summary(fit4)$coef[3,1]/3
aic2 <- AIC2(288,c(AmPG,AmM),c(AnPG,AnM),1,6)
bic2 <- BIC2(288,c(AmPG,AmM),c(AnPG,AnM),1,6)


par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(AnPG,HPG,col=2)
points( summary(fit4)$coef[1,1] * X ^summary(fit4)$coef[2,1] * (1-X) ^summary(fit4)$coef[3,1]/3,X,type='l',lwd=2,col=2)
points(AnM,HM,col=4)
points( summary(fit2)$coef[1,1] * X ^summary(fit2)$coef[2,1] * (1-X) ^summary(fit2)$coef[3,1]/3,X,type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('small and big trees','medium trees'),pch=1,lty=1,col=c(2,4),bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic2,1)),paste('BIC = ',round(bic2,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('Treatment C1, C3 & S3')


# age


A11W <- An2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==11]
A16W <- An2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==16]
A23W <- An2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==23]
A28W <- An2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==28]
A36W <- An2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==36]
H11 <- H2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==11]
H16 <- H2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==16]
H23 <- H2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==23]
H28 <- H2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==28]
H36 <- H2[trat2=='S1'|trat2=='S3'][age[trat2=='S1'|trat2=='S3']==36]
fit1 <- nls(3*A11W~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16W~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23W~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28W~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36W~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11W <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16W <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23W <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28W <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36W <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3

A11D <- An2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==11]
A16D <- An2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==16]
A23D <- An2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==23]
A28D <- An2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==28]
A36D <- An2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==36]
H11 <- H2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==11]
H16 <- H2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==16]
H23 <- H2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==23]
H28 <- H2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==28]
H36 <- H2[trat2=='C1'|trat2=='C3'][age[trat2=='C1'|trat2=='C3']==36]
fit1 <- nls(3*A11D~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16D~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23D~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28D~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36D~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11D <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16D <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23D <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28D <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36D <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3

aicWA <- AIC2(480,c(Am11W,Am16W,Am23W,Am28W,Am36W,Am11D,Am16D,Am23D,Am28D,Am36D),c(A11W,A16W,A23W,A28W,A36W,A11D,A16D,A23D,A28D,A36D),1,30)
bicWA <- BIC2(480,c(Am11W,Am16W,Am23W,Am28W,Am36W,Am11D,Am16D,Am23D,Am28D,Am36D),c(A11W,A16W,A23W,A28W,A36W,A11D,A16D,A23D,A28D,A36D),1,30)

A11K <- An2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==11]
A16K <- An2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==16]
A23K <- An2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==23]
A28K <- An2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==28]
A36K <- An2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==36]
H11 <- H2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==11]
H16 <- H2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==16]
H23 <- H2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==23]
H28 <- H2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==28]
H36 <- H2[trat2=='C3'|trat2=='S3'][age[trat2=='C3'|trat2=='S3']==36]
fit1 <- nls(3*A11K~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16K~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23K~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28K~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36K~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
COEFK <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
COEFK[1] <- COEFK[1]/integrate(LADfun,lower=0,upper=1,a=COEFK[1],b=COEFK[2],d=COEFK[3])$value
COEFK[4] <- COEFK[4]/integrate(LADfun,lower=0,upper=1,a=COEFK[4],b=COEFK[5],d=COEFK[6])$value
COEFK[7] <- COEFK[7]/integrate(LADfun,lower=0,upper=1,a=COEFK[7],b=COEFK[8],d=COEFK[9])$value
COEFK[10] <- COEFK[10]/integrate(LADfun,lower=0,upper=1,a=COEFK[10],b=COEFK[11],d=COEFK[12])$value
COEFK[13] <- COEFK[13]/integrate(LADfun,lower=0,upper=1,a=COEFK[13],b=COEFK[14],d=COEFK[15])$value

Am11K <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16K <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23K <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28K <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36K <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3
A11D <- An2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==11]
A16D <- An2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==16]
A23D <- An2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==23]
A28D <- An2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==28]
A36D <- An2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==36]
H11 <- H2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==11]
H16 <- H2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==16]
H23 <- H2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==23]
H28 <- H2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==28]
H36 <- H2[trat2=='C1'|trat2=='S1'][age[trat2=='C1'|trat2=='S1']==36]
fit1 <- nls(3*A11D~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16D~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23D~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28D~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36D~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
COEFDK <- c(summary(fit1)$coef[,1],summary(fit2)$coef[,1],summary(fit3)$coef[,1],summary(fit4)$coef[,1],summary(fit5)$coef[,1])
COEFDK[1] <- COEFDK[1]/integrate(LADfun,lower=0,upper=1,a=COEFDK[1],b=COEFDK[2],d=COEFDK[3])$value
COEFDK[4] <- COEFDK[4]/integrate(LADfun,lower=0,upper=1,a=COEFDK[4],b=COEFDK[5],d=COEFDK[6])$value
COEFDK[7] <- COEFDK[7]/integrate(LADfun,lower=0,upper=1,a=COEFDK[7],b=COEFDK[8],d=COEFDK[9])$value
COEFDK[10] <- COEFDK[10]/integrate(LADfun,lower=0,upper=1,a=COEFDK[10],b=COEFDK[11],d=COEFDK[12])$value
COEFDK[13] <- COEFDK[13]/integrate(LADfun,lower=0,upper=1,a=COEFDK[13],b=COEFDK[14],d=COEFDK[15])$value

Am11D <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16D <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23D <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28D <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36D <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3

aicKA <- AIC2(480,c(Am11K,Am16K,Am23K,Am28K,Am36K,Am11D,Am16D,Am23D,Am28D,Am36D),c(A11K,A16K,A23K,A28K,A36K,A11D,A16D,A23D,A28D,A36D),1,30)
bicKA <- BIC2(480,c(Am11K,Am16K,Am23K,Am28K,Am36K,Am11D,Am16D,Am23D,Am28D,Am36D),c(A11K,A16K,A23K,A28K,A36K,A11D,A16D,A23D,A28D,A36D),1,30)


IDADE <- c(11,11,11,16,16,16,23,23,23,28,28,28,36,36,36)
MAT <- data.frame(IDADE,COEFC1,COEFC3,COEFS1,COEFS3,COEFK,COEFDK)

write.table(MAT,'Vert_LAD.txt',col.names=T,row.names=F)


A11C1 <- An2[trat2=='C1'][age[trat2=='C1']==11]
A16C1 <- An2[trat2=='C1'][age[trat2=='C1']==16]
A23C1 <- An2[trat2=='C1'][age[trat2=='C1']==23]
A28C1 <- An2[trat2=='C1'][age[trat2=='C1']==28]
A36C1 <- An2[trat2=='C1'][age[trat2=='C1']==36]
H11 <- H2[trat2=='C1'][age[trat2=='C1']==11]
H16 <- H2[trat2=='C1'][age[trat2=='C1']==16]
H23 <- H2[trat2=='C1'][age[trat2=='C1']==23]
H28 <- H2[trat2=='C1'][age[trat2=='C1']==28]
H36 <- H2[trat2=='C1'][age[trat2=='C1']==36]
fit1 <- nls(3*A11C1~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16C1~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23C1~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28C1~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36C1~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11C1 <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16C1 <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23C1 <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28C1 <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36C1 <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3
A11C3 <- An2[trat2=='C3'][age[trat2=='C3']==11]
A16C3 <- An2[trat2=='C3'][age[trat2=='C3']==16]
A23C3 <- An2[trat2=='C3'][age[trat2=='C3']==23]
A28C3 <- An2[trat2=='C3'][age[trat2=='C3']==28]
A36C3 <- An2[trat2=='C3'][age[trat2=='C3']==36]
H11 <- H2[trat2=='C3'][age[trat2=='C3']==11]
H16 <- H2[trat2=='C3'][age[trat2=='C3']==16]
H23 <- H2[trat2=='C3'][age[trat2=='C3']==23]
H28 <- H2[trat2=='C3'][age[trat2=='C3']==28]
H36 <- H2[trat2=='C3'][age[trat2=='C3']==36]
fit1 <- nls(3*A11C3~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16C3~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23C3~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28C3~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36C3~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11C3 <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16C3 <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23C3 <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28C3 <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36C3 <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3
A11S1 <- An2[trat2=='S1'][age[trat2=='S1']==11]
A16S1 <- An2[trat2=='S1'][age[trat2=='S1']==16]
A23S1 <- An2[trat2=='S1'][age[trat2=='S1']==23]
A28S1 <- An2[trat2=='S1'][age[trat2=='S1']==28]
A36S1 <- An2[trat2=='S1'][age[trat2=='S1']==36]
H11 <- H2[trat2=='S1'][age[trat2=='S1']==11]
H16 <- H2[trat2=='S1'][age[trat2=='S1']==16]
H23 <- H2[trat2=='S1'][age[trat2=='S1']==23]
H28 <- H2[trat2=='S1'][age[trat2=='S1']==28]
H36 <- H2[trat2=='S1'][age[trat2=='S1']==36]
fit1 <- nls(3*A11S1~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16S1~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23S1~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28S1~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36S1~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11S1 <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16S1 <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23S1 <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28S1 <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36S1 <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3
A11S3 <- An2[trat2=='S3'][age[trat2=='S3']==11]
A16S3 <- An2[trat2=='S3'][age[trat2=='S3']==16]
A23S3 <- An2[trat2=='S3'][age[trat2=='S3']==23]
A28S3 <- An2[trat2=='S3'][age[trat2=='S3']==28]
A36S3 <- An2[trat2=='S3'][age[trat2=='S3']==36]
H11 <- H2[trat2=='S3'][age[trat2=='S3']==11]
H16 <- H2[trat2=='S3'][age[trat2=='S3']==16]
H23 <- H2[trat2=='S3'][age[trat2=='S3']==23]
H28 <- H2[trat2=='S3'][age[trat2=='S3']==28]
H36 <- H2[trat2=='S3'][age[trat2=='S3']==36]
fit1 <- nls(3*A11S3~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16S3~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23S3~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28S3~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36S3~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))
Am11S3 <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16S3 <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23S3 <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28S3 <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36S3 <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3


aicKWA <- AIC2(480,c(Am11C1,Am16C1,Am23C1,Am28C1,Am36C1,
                     Am11C3,Am16C3,Am23C3,Am28C3,Am36C3,
                     Am11S1,Am16S1,Am23S1,Am28S1,Am36S1,
                     Am11S3,Am16S3,Am23S3,Am28S3,Am36S3),
                   c(A11C1,A16C1,A23C1,A28C1,A36C1,
                     A11C3,A16C3,A23C3,A28C3,A36C3,
                     A11S1,A16S1,A23S1,A28S1,A36S1,
                     A11S3,A16S3,A23S3,A28S3,A36S3),1,60)
bicKWA <- BIC2(480,c(Am11C1,Am16C1,Am23C1,Am28C1,Am36C1,
                     Am11C3,Am16C3,Am23C3,Am28C3,Am36C3,
                     Am11S1,Am16S1,Am23S1,Am28S1,Am36S1,
                     Am11S3,Am16S3,Am23S3,Am28S3,Am36S3),
                   c(A11C1,A16C1,A23C1,A28C1,A36C1,
                     A11C3,A16C3,A23C3,A28C3,A36C3,
                     A11S1,A16S1,A23S1,A28S1,A36S1,
                     A11S3,A16S3,A23S3,A28S3,A36S3),1,60)


par(mfrow=c(1,2))
par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(A11,H11,col=1)
points( summary(fit1)$coef[1,1] * X ^summary(fit1)$coef[2,1] * (1-X) ^summary(fit1)$coef[3,1]/3,X,type='l',lwd=2,col=1)
points(A16,H16,col=2)
points( summary(fit2)$coef[1,1] * X ^summary(fit2)$coef[2,1] * (1-X) ^summary(fit2)$coef[3,1]/3,X,type='l',lwd=2,col=2)
points(A23,H23,col=3)
points( summary(fit3)$coef[1,1] * X ^summary(fit3)$coef[2,1] * (1-X) ^summary(fit3)$coef[3,1]/3,X,type='l',lwd=2,col=3)
points(A28,H28,col=4)
points( summary(fit4)$coef[1,1] * X ^summary(fit4)$coef[2,1] * (1-X) ^summary(fit4)$coef[3,1]/3,X,type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('11 months','16 months','23 months','28 months'),pch=1,lty=1,col=1:4,bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic1,1)),paste('BIC = ',round(bic1,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('Treatment S1')


A11 <- An2[age==11]
H11 <- H2[age==11]
A16 <- An2[age==16]
H16 <- H2[age==16]
A23 <- An2[age==23]
H23 <- H2[age==23]
A28 <- An2[age==28]
H28 <- H2[age==28]
A36 <- An2[age==36]
H36 <- H2[age==36]
fit1 <- nls(3*A11~a*H11^b*(1-H11)^d,start=c(a=1,b=0.2,d=0.2))
fit2 <- nls(3*A16~a*H16^b*(1-H16)^d,start=c(a=1,b=0.2,d=0.2))
fit3 <- nls(3*A23~a*H23^b*(1-H23)^d,start=c(a=1,b=0.2,d=0.2))
fit4 <- nls(3*A28~a*H28^b*(1-H28)^d,start=c(a=1,b=0.2,d=0.2))
fit5 <- nls(3*A36~a*H36^b*(1-H36)^d,start=c(a=1,b=0.2,d=0.2))

Am11 <- summary(fit1)$coef[1,1] * H11 ^summary(fit1)$coef[2,1] * (1-H11) ^summary(fit1)$coef[3,1]/3
Am16 <- summary(fit2)$coef[1,1] * H16 ^summary(fit2)$coef[2,1] * (1-H16) ^summary(fit2)$coef[3,1]/3
Am23 <- summary(fit3)$coef[1,1] * H23 ^summary(fit3)$coef[2,1] * (1-H23) ^summary(fit3)$coef[3,1]/3
Am28 <- summary(fit4)$coef[1,1] * H28 ^summary(fit4)$coef[2,1] * (1-H28) ^summary(fit4)$coef[3,1]/3
Am36 <- summary(fit5)$coef[1,1] * H36 ^summary(fit5)$coef[2,1] * (1-H36) ^summary(fit5)$coef[3,1]/3

aicAGE <- AIC2(480,c(Am11,Am16,Am23,Am28,Am36),c(A11,A16,A23,A28,A36),1,15)
bicAGE <- BIC2(480,c(Am11,Am16,Am23,Am28,Am36),c(A11,A16,A23,A28,A36),1,15)

par(mar=c(4.5,4.5,3,1))
par(yaxs='i')
plot(0.5,0.5, ylab='Hauteur relative dans la couronne',xlab='Relative leaf area',col=0,pch=0,xlim=c(0,1),ylim=c(0,1),cex.lab=1.2)
X <- seq(0,1,0.01)
points(A11,H11,col=1)
points( summary(fit1)$coef[1,1] * X ^summary(fit1)$coef[2,1] * (1-X) ^summary(fit1)$coef[3,1]/3,X,type='l',lwd=2,col=1)
points(A16,H16,col=2)
points( summary(fit2)$coef[1,1] * X ^summary(fit2)$coef[2,1] * (1-X) ^summary(fit2)$coef[3,1]/3,X,type='l',lwd=2,col=2)
points(A23,H23,col=3)
points( summary(fit3)$coef[1,1] * X ^summary(fit3)$coef[2,1] * (1-X) ^summary(fit3)$coef[3,1]/3,X,type='l',lwd=2,col=3)
points(A28,H28,col=4)
points( summary(fit4)$coef[1,1] * X ^summary(fit4)$coef[2,1] * (1-X) ^summary(fit4)$coef[3,1]/3,X,type='l',lwd=2,col=4)
grid(nx=NA,ny=NULL)
legend('topright',legend=c('11 months','16 months','23 months','28 months'),pch=1,lty=1,col=1:4,bg='white',box.col=0,cex=1.2)
legend('bottomright',legend=c(paste('AIC = ',round(aic1,1)),paste('BIC = ',round(bic1,1))),pch=NA,cex=1.2,bg='white',box.col=0);box()
title('Treatment C1, C3 & S3')


