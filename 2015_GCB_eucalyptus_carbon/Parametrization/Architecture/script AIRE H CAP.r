set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Extrapolation bordure et architecture copa")
source("C:/Users/mathias/Documents/R/R_fonctions_perso/fonctions_stats.r")
setwd(set)

ALT <- read.table('Alt.txt',header=T)

Dates_ALT <- c("20/12/2010", "28/01/2011", "24/02/2011", "28/03/2011", "10/05/2011",
"26/05/2011", "30/06/2011", "29/07/2011", "25/08/2011", "27/09/2011",
"31/10/2011", "23/11/2011", "23/04/2012", "20/09/2012","28/01/2013","02/05/2013")
Months_ALT <- c(6,7,8,9,10,11,12,13,14,15,16,17,22,27,31,36)

# manquant � 7 9  11 12 13 14 16 mois
for (i in 1:dim(ALT)[1]){
if (is.na(ALT[,6][i])==F){
if (is.na(ALT[,7][i])==T){
  ALT[,7][i] <- ALT[,6][i]/2 + ALT[,8][i]/2}
if (is.na(ALT[,9][i])==T){
  ALT[,9][i] <- ALT[,8][i]/2 + ALT[,10][i]/2}
if (is.na(ALT[,11][i])==T){
  ALT[,11][i] <- ALT[,10][i]*4/5 + ALT[,15][i]*1/5}
if (is.na(ALT[,12][i])==T){
  ALT[,12][i] <- ALT[,10][i]*3/5 + ALT[,15][i]*2/5}
if (is.na(ALT[,13][i])==T){
  ALT[,13][i] <- ALT[,10][i]*2/5 + ALT[,15][i]*3/5}
if (is.na(ALT[,14][i])==T){
  ALT[,14][i] <- ALT[,10][i]*1/5 + ALT[,15][i]*4/5}
if (is.na(ALT[,16][i])==T){
  ALT[,16][i] <- ALT[,15][i]*1/2 + ALT[,17][i]*1/2}
}}
# graphiquement
alt <- rep(NA,16); sdalt <-rep(NA,16)
for (i in levels(ALT$Trat)){
x1 <-c();x2 <-c(); for (j in 1:16){
x1 <- c(x1, mean(ALT[,(j+5)][ALT$Trat==i],na.rm=T))
x2 <- c(x2, sd(ALT[,(j+5)][ALT$Trat==i],na.rm=T))}
alt <- data.frame(alt,x1)
sdalt <- data.frame(sdalt,x2)} 
alt <- alt[,2:7]; sdalt <- sdalt[,2:7]

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(Months_ALT,alt[,1],xlab='Age (months)',ylab='Tree height (m)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,37),ylim=c(0,20),mgp=c(2.5,1,0))
points(Months_ALT,alt[,2],type='b',pch=17,lwd=2,col=1)
points(Months_ALT,alt[,3],type='b',pch=15,lwd=2,col=1)
points(Months_ALT,alt[,4],type='b',pch=1,lwd=2,col=1,lty=2)
points(Months_ALT,alt[,5],type='b',pch=2,lwd=2,col=1,lty=2)
points(Months_ALT,alt[,6],type='b',pch=22,lwd=2,col=1,lty=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,17,15,1,2,22),lty=c(1,1,1,2,2,2),col=1,text.col=1,bg='white',box.col=0,cex=1.2);box()



CAP <- read.table('CAP.txt',header=T)
Dates_CAP <- c("10/05/2011", "22/06/2011", "27/09/2011", "23/11/2011", "23/04/2012","20/09/2012","28/01/2013","02/05/2013")
Months_CAP <- c(11,12,15,17,22,27,31,36)

# CAP manquant � 12 mois
for (i in 1:dim(CAP)[1]){
if (is.na(CAP[,6][i])==F){
if (is.na(CAP[,7][i])==T){
  CAP[,7][i] <- CAP[,6][i]*3/4 + CAP[,8][i]*1/4}}}
 
# extrapolation des CAP � toutes les dates
CAP13 <- CAP[,7]*2/3 + CAP[,8]*1/3
CAP14 <- CAP[,7]*1/3 + CAP[,8]*2/3
CAP16 <- CAP[,8]*1/2 + CAP[,9]*1/2

ALTTOT <- c(ALT[,11],ALT[,12],ALT[,13],ALT[,14],ALT[,15],ALT[,16],ALT[,17],ALT[,18],ALT[,19],ALT[,20],ALT[,21])
CAPTOT <- c(CAP[,6],CAP[,7],CAP13,CAP14,CAP[,8],CAP16,CAP[,9],CAP[,10],CAP[,11],CAP[,12],CAP[,13])
TRAT <- rep(ALT$Trat,11)

# relation toutes les dates
XC1 <- CAPTOT[CAP$Trat=='C1']
YC1 <- ALTTOT[TRAT=='C1']
XC3 <- CAPTOT[CAP$Trat=='C3']
YC3 <- ALTTOT[TRAT=='C3']
XC2 <- CAPTOT[CAP$Trat=='C2']
YC2 <- ALTTOT[TRAT=='C2']
XS1 <- CAPTOT[CAP$Trat=='S1']
YS1 <- ALTTOT[TRAT=='S1']
XS2 <- CAPTOT[CAP$Trat=='S2']
YS2 <- ALTTOT[TRAT=='S2']
XS3 <- CAPTOT[CAP$Trat=='S3']
YS3 <- ALTTOT[TRAT=='S3']
ftC1 <- nls(XC1~a*YC1^b)
ftC2 <- nls(XC2~a*YC2^b)
ftC3 <- nls(XC3~a*YC3^b)
ftS1 <- nls(XS1~a*YS1^b)
ftS2 <- nls(XS2~a*YS2^b)
ftS3 <- nls(XS3~a*YS3^b)

X <- seq(min(ALTTOT,na.rm=T),max(ALTTOT,na.rm=T),length.out=100)
par(mar=c(4.5,4.5,3,1))
plot(ALTTOT,CAPTOT,xlab='Tree height (m)',ylab='CAP (cm)',cex.lab=1.2)
points(X,summary(ftC1)$coef[1,1]*X^summary(ftC1)$coef[2,1],type='l',col=2,lwd=2,lty=1)
points(X,summary(ftC2)$coef[1,1]*X^summary(ftC2)$coef[2,1],type='l',col=3,lwd=2,lty=1)
points(X,summary(ftC3)$coef[1,1]*X^summary(ftC3)$coef[2,1],type='l',col=4,lwd=2,lty=1)
points(X,summary(ftS1)$coef[1,1]*X^summary(ftS1)$coef[2,1],type='l',col=2,lwd=2,lty=2)
points(X,summary(ftS2)$coef[1,1]*X^summary(ftS2)$coef[2,1],type='l',col=3,lwd=2,lty=2)
points(X,summary(ftS3)$coef[1,1]*X^summary(ftS3)$coef[2,1],type='l',col=4,lwd=2,lty=2)
title('Alld dates')
legend('bottomright',legend=c('C1','C2','C3','S1','S2','S3'),col=c(2:4,2:4),lty=c(1,1,1,2,2,2),pch=NA,bty='n',cex=1.2)

# relation � 11 mois
XC1 <- CAP[,6][CAP$Trat=='C1']
YC1 <- ALT[,10][ALT$Trat=='C1']
XC3 <- CAP[,6][CAP$Trat=='C3']
YC3 <- ALT[,10][ALT$Trat=='C3']
XC2 <- CAP[,6][CAP$Trat=='C2']
YC2 <- ALT[,10][ALT$Trat=='C2']
XS1 <- CAP[,6][CAP$Trat=='S1']
YS1 <- ALT[,10][ALT$Trat=='S1']
XS2 <- CAP[,6][CAP$Trat=='S2']
YS2 <- ALT[,10][ALT$Trat=='S2']
XS3 <- CAP[,6][CAP$Trat=='S3']
YS3 <- ALT[,10][ALT$Trat=='S3']
ftC1 <- nls(XC1~a*YC1^b)
ftC2 <- nls(XC2~a*YC2^b)
ftC3 <- nls(XC3~a*YC3^b)
ftS1 <- nls(XS1~a*YS1^b)
ftS2 <- nls(XS2~a*YS2^b)
ftS3 <- nls(XS3~a*YS3^b)

X <- seq(min(ALT[,10],na.rm=T),max(ALT[,10],na.rm=T),length.out=100)
par(mar=c(4.5,4.5,3,1))
plot(ALT[,10],CAP[,6],xlab='Tree height (m)',ylab='CAP (cm)',cex.lab=1.2,ylim=c(0,20))
points(X,summary(ftC1)$coef[1,1]*X^summary(ftC1)$coef[2,1],type='l',col=2,lwd=2,lty=1)
points(X,summary(ftC2)$coef[1,1]*X^summary(ftC2)$coef[2,1],type='l',col=3,lwd=2,lty=1)
points(X,summary(ftC3)$coef[1,1]*X^summary(ftC3)$coef[2,1],type='l',col=4,lwd=2,lty=1)
points(X,summary(ftS1)$coef[1,1]*X^summary(ftS1)$coef[2,1],type='l',col=2,lwd=2,lty=2)
points(X,summary(ftS2)$coef[1,1]*X^summary(ftS2)$coef[2,1],type='l',col=3,lwd=2,lty=2)
points(X,summary(ftS3)$coef[1,1]*X^summary(ftS3)$coef[2,1],type='l',col=4,lwd=2,lty=2)
title('11 months')
legend('bottomright',legend=c('C1','C2','C3','S1','S2','S3'),col=c(2:4,2:4),lty=c(1,1,1,2,2,2),pch=NA,bty='n',cex=1.2)
points(YS3,XS3,col=4)

DAT <- read.table('Biomass.txt',header=T)
XC1 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C1']
YC1 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C1']
XC2 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C2']
YC2 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C2']
XC3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C3']
YC3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C3']
XS1 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S1']
YS1 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S1']
XS2 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S2']
YS2 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S2']
XS3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
YS3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
ftC1 <- nls(XC1~a*YC1^b)
ftC2 <- nls(XC2~a*YC2^b)
ftC3 <- nls(XC3~a*YC3^b)
ftS1 <- nls(XS1~a*YS1^b)
ftS2 <- nls(XS2~a*YS2^b)
ftS3 <- nls(XS3~a*YS3^b)

par(mar=c(4.5,4.5,3,1))
plot(DAT$Alt_tot[DAT$Idade==11],DAT$Cap[DAT$Idade==11],xlab='Tree height (m)',ylab='CAP (cm)',cex.lab=1.2,ylim=c(0,20))
X <- seq(min(DAT$Alt_tot[DAT$Idade==11],na.rm=T),max(DAT$Alt_tot[DAT$Idade==11],na.rm=T),length.out=100)
points(X,summary(ftC1)$coef[1,1]*X^summary(ftC1)$coef[2,1],type='l',col=2,lwd=2,lty=1)
points(X,summary(ftC2)$coef[1,1]*X^summary(ftC2)$coef[2,1],type='l',col=3,lwd=2,lty=1)
points(X,summary(ftC3)$coef[1,1]*X^summary(ftC3)$coef[2,1],type='l',col=4,lwd=2,lty=1)
points(X,summary(ftS1)$coef[1,1]*X^summary(ftS1)$coef[2,1],type='l',col=2,lwd=2,lty=2)
points(X,summary(ftS2)$coef[1,1]*X^summary(ftS2)$coef[2,1],type='l',col=3,lwd=2,lty=2)
points(X,summary(ftS3)$coef[1,1]*X^summary(ftS3)$coef[2,1],type='l',col=4,lwd=2,lty=2)
title('11 months from destructive biomass')
legend('bottomright',legend=c('C1','C2','C3','S1','S2','S3'),col=c(2:4,2:4),lty=c(1,1,1,2,2,2),pch=NA,bty='n',cex=1.2)

XC1 <- CAP[,6][CAP$Trat=='C1']
YC1 <- ALT[,10][ALT$Trat=='C1']
XC3 <- CAP[,6][CAP$Trat=='C3']
YC3 <- ALT[,10][ALT$Trat=='C3']
XC2 <- CAP[,6][CAP$Trat=='C2']
YC2 <- ALT[,10][ALT$Trat=='C2']
XS1 <- CAP[,6][CAP$Trat=='S1']
YS1 <- ALT[,10][ALT$Trat=='S1']
XS2 <- CAP[,6][CAP$Trat=='S2']
YS2 <- ALT[,10][ALT$Trat=='S2']
XS3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
YS3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
ftC1 <- nls(XC1~a*YC1^b)
ftC2 <- nls(XC2~a*YC2^b)
ftC3 <- nls(XC3~a*YC3^b)
ftS1 <- nls(XS1~a*YS1^b)
ftS2 <- nls(XS2~a*YS2^b)
ftS3 <- nls(XS3~a*YS3^b)

X <- seq(min(ALT[,10],na.rm=T),max(ALT[,10],na.rm=T),length.out=100)
par(mar=c(4.5,4.5,3,1))
plot(ALT[,10],CAP[,6],xlab='Tree height (m)',ylab='CAP (cm)',cex.lab=1.2,ylim=c(0,20))
points(X,summary(ftC1)$coef[1,1]*X^summary(ftC1)$coef[2,1],type='l',col=2,lwd=2,lty=1)
points(X,summary(ftC2)$coef[1,1]*X^summary(ftC2)$coef[2,1],type='l',col=3,lwd=2,lty=1)
points(X,summary(ftC3)$coef[1,1]*X^summary(ftC3)$coef[2,1],type='l',col=4,lwd=2,lty=1)
points(X,summary(ftS1)$coef[1,1]*X^summary(ftS1)$coef[2,1],type='l',col=2,lwd=2,lty=2)
points(X,summary(ftS2)$coef[1,1]*X^summary(ftS2)$coef[2,1],type='l',col=3,lwd=2,lty=2)
points(X,summary(ftS3)$coef[1,1]*X^summary(ftS3)$coef[2,1],type='l',col=4,lwd=2,lty=2)
title('11 months + S3 relation from destructive biomass')
legend('bottomright',legend=c('C1','C2','C3','S1','S2','S3'),col=c(2:4,2:4),lty=c(1,1,1,2,2,2),pch=NA,bty='n',cex=1.2)
points(YS3,XS3,col=4)

XC1 <- CAP[,6][CAP$Trat=='C1']
YC1 <- ALT[,10][ALT$Trat=='C1']
XC3 <- CAP[,6][CAP$Trat=='C3']
YC3 <- ALT[,10][ALT$Trat=='C3']
XC2 <- CAP[,6][CAP$Trat=='C2']
YC2 <- ALT[,10][ALT$Trat=='C2']
XS1 <- CAP[,6][CAP$Trat=='S1']
YS1 <- ALT[,10][ALT$Trat=='S1']
XS2 <- CAP[,6][CAP$Trat=='S2']
YS2 <- ALT[,10][ALT$Trat=='S2']
XS3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
YS3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']

DAT <- read.table('Biomass.txt',header=T)
XC1 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C1']
YC1 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C1']
XC2 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C2']
YC2 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C2']
XC3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C3']
YC3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='C3']
XS1 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S1']
YS1 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S1']
XS2 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S2']
YS2 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S2']
XS3 <- DAT$Cap[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
YS3 <- DAT$Alt_tot[DAT$Idade==11][DAT$Trat[DAT$Idade==11]=='S3']
ftC1 <- lm(XC1~YC1-1)
ftC2 <- lm(XC2~YC2-1)
ftC3 <- lm(XC3~YC3-1)
ftS1 <- lm(XS1~YS1-1)
ftS2 <- lm(XS2~YS2-1)
ftS3 <- lm(XS3~YS3-1)


CAP6<-c();CAP7<-c();CAP8<-c();CAP9<-c();CAP10<-c()
for (i in 1:dim(ALT)[1]){
if (ALT$Trat[i]=='C1'){
CAP6 <-c(CAP6,summary(ftC1)$coef[1,1]*ALT[i,6]^summary(ftC1)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftC1)$coef[1,1]*ALT[i,7]^summary(ftC1)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftC1)$coef[1,1]*ALT[i,8]^summary(ftC1)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftC1)$coef[1,1]*ALT[i,9]^summary(ftC1)$coef[2,1])
CAP10 <-c(CAP10,summary(ftC1)$coef[1,1]*ALT[i,10]^summary(ftC1)$coef[2,1])}
else if (ALT$Trat[i]=='C2'){
CAP6 <-c(CAP6,summary(ftC2)$coef[1,1]*ALT[i,6]^summary(ftC2)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftC2)$coef[1,1]*ALT[i,7]^summary(ftC2)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftC2)$coef[1,1]*ALT[i,8]^summary(ftC2)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftC2)$coef[1,1]*ALT[i,9]^summary(ftC2)$coef[2,1])
CAP10 <-c(CAP10,summary(ftC2)$coef[1,1]*ALT[i,10]^summary(ftC2)$coef[2,1])}
else if (ALT$Trat[i]=='C3'){
CAP6 <-c(CAP6,summary(ftC3)$coef[1,1]*ALT[i,6]^summary(ftC3)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftC3)$coef[1,1]*ALT[i,7]^summary(ftC3)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftC3)$coef[1,1]*ALT[i,8]^summary(ftC3)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftC3)$coef[1,1]*ALT[i,9]^summary(ftC3)$coef[2,1]) 
CAP10 <-c(CAP10,summary(ftC3)$coef[1,1]*ALT[i,10]^summary(ftC3)$coef[2,1])}
else if (ALT$Trat[i]=='S1'){
CAP6 <-c(CAP6,summary(ftS1)$coef[1,1]*ALT[i,6]^summary(ftS1)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftS1)$coef[1,1]*ALT[i,7]^summary(ftS1)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftS1)$coef[1,1]*ALT[i,8]^summary(ftS1)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftS1)$coef[1,1]*ALT[i,9]^summary(ftS1)$coef[2,1]) 
CAP10 <-c(CAP10,summary(ftS1)$coef[1,1]*ALT[i,10]^summary(ftS1)$coef[2,1])}
else if (ALT$Trat[i]=='S2'){
CAP6 <-c(CAP6,summary(ftS2)$coef[1,1]*ALT[i,6]^summary(ftS2)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftS2)$coef[1,1]*ALT[i,7]^summary(ftS2)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftS2)$coef[1,1]*ALT[i,8]^summary(ftS2)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftS2)$coef[1,1]*ALT[i,9]^summary(ftS2)$coef[2,1]) 
CAP10 <-c(CAP10,summary(ftS2)$coef[1,1]*ALT[i,10]^summary(ftS2)$coef[2,1])}
else if (ALT$Trat[i]=='S3'){
CAP6 <-c(CAP6,summary(ftS3)$coef[1,1]*ALT[i,6]^summary(ftS3)$coef[2,1]) 
CAP7 <-c(CAP7,summary(ftS3)$coef[1,1]*ALT[i,7]^summary(ftS3)$coef[2,1]) 
CAP8 <-c(CAP8,summary(ftS3)$coef[1,1]*ALT[i,8]^summary(ftS3)$coef[2,1]) 
CAP9 <-c(CAP9,summary(ftS3)$coef[1,1]*ALT[i,9]^summary(ftS3)$coef[2,1]) 
CAP10 <-c(CAP10,summary(ftS3)$coef[1,1]*ALT[i,10]^summary(ftS3)$coef[2,1])}
}

CAP6<-c();CAP7<-c();CAP8<-c();CAP9<-c();CAP10<-c()
for (i in 1:dim(ALT)[1]){
if (ALT$Trat[i]=='C1'){
CAP6 <-c(CAP6,summary(ftC1)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftC1)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftC1)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftC1)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftC1)$coef[1]*ALT[i,10])}
else if (ALT$Trat[i]=='C2'){
CAP6 <-c(CAP6,summary(ftC2)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftC2)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftC2)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftC2)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftC2)$coef[1]*ALT[i,10])}
else if (ALT$Trat[i]=='C3'){
CAP6 <-c(CAP6,summary(ftC3)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftC3)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftC3)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftC3)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftC3)$coef[1]*ALT[i,10])}
else if (ALT$Trat[i]=='S1'){
CAP6 <-c(CAP6,summary(ftS1)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftS1)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftS1)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftS1)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftS1)$coef[1]*ALT[i,10])}
else if (ALT$Trat[i]=='S2'){
CAP6 <-c(CAP6,summary(ftS2)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftS2)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftS2)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftS2)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftS2)$coef[1]*ALT[i,10])}
else if (ALT$Trat[i]=='S3'){
CAP6 <-c(CAP6,summary(ftS3)$coef[1]*ALT[i,6])
CAP7 <-c(CAP7,summary(ftS3)$coef[1]*ALT[i,7])
CAP8 <-c(CAP8,summary(ftS3)$coef[1]*ALT[i,8])
CAP9 <-c(CAP9,summary(ftS3)$coef[1]*ALT[i,9])
CAP10 <-c(CAP10,summary(ftS3)$coef[1]*ALT[i,10])}
}

capTOT <- data.frame(CAP6,CAP7,CAP8,CAP9,CAP10,CAP[,6],CAP[,7],CAP13,CAP14,CAP[,8],CAP16,CAP[,9],CAP[,10],CAP[,11],CAP[,12],CAP[,13])
cap <- rep(NA,16); sdcap <-rep(NA,16)
for (i in levels(ALT$Trat)){
x1 <-c();x2 <-c(); for (j in 1:16){
x1 <- c(x1, mean(capTOT[,j][ALT$Trat==i],na.rm=T))
x2 <- c(x2, sd(capTOT[,j][ALT$Trat==i],na.rm=T))}
cap<- data.frame(cap,x1)
sdcap <- data.frame(sdcap,x2)} 
cap <- cap[,2:7]; sdcap <- sdcap[,2:7]

par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')
par(mfrow=c(2,1))
plot(DAT$Alt_tot[DAT$Idade==11],DAT$Cap[DAT$Idade==11],xlab='Tree height (m)',ylab='CAP (cm)',cex.lab=1.2,ylim=c(0,20))
abline(ftC1,col=1,lwd=2,lty=1)
abline(ftC2,col=2,lwd=2,lty=1)
abline(ftC3,col=3,lwd=2,lty=1)
abline(ftS1,col=1,lwd=2,lty=2)
abline(ftS2,col=2,lwd=2,lty=2)
abline(ftS3,col=3,lwd=2,lty=2)
legend('bottomright',legend=c('C1','C2','C3','S1','S2','S3'),col=c(1:3,1:3),lty=c(1,1,1,2,2,2),pch=NA,bty='n',cex=1)
plot(Months_ALT,cap[,1],xlab='Age (months)',ylab='Tree CAP (cm)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,36),ylim=c(0,36))
points(Months_ALT,cap[,2],type='b',pch=16,lwd=2,col=3)
points(Months_ALT,cap[,3],type='b',pch=16,lwd=2,col=2)
points(Months_ALT,cap[,4],type='b',pch=4,lwd=2,col=1,lty=2)
points(Months_ALT,cap[,5],type='b',pch=4,lwd=2,col=3,lty=2)
points(Months_ALT,cap[,6],type='b',pch=4,lwd=2,col=2,lty=2)
add.errorbars(Months_ALT,cap[,1],sdcap[,1],direction='vert',col=1)
add.errorbars(Months_ALT,cap[,3],sdcap[,3],direction='vert',col=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,16,16,4,4,4),lty=c(1,1,1,2,2,2),col=c(1,3,2,1,3,2),text.col=c(1,3,2,1,3,2),bg='white',box.col=0,cex=1);box()
 
# avec toute
ALTTOT <- c(ALT[,6],ALT[,7],ALT[,8],ALT[,9],ALT[,10],ALT[,11],ALT[,12],ALT[,13],ALT[,14],ALT[,15],ALT[,16],ALT[,17],ALT[,18],ALT[,19],ALT[,20],ALT[,21])
CAPTOT <- c(CAP6,CAP7,CAP8,CAP9,CAP10,CAP[,6],CAP[,7],CAP13,CAP14,CAP[,8],CAP16,CAP[,9],CAP[,10],CAP[,11],CAP[,12],CAP[,13])
TRAT <- rep(ALT$Trat,16)
AGE <-c(); DATES <- c()
for (i in 1:length(Dates_ALT)){
AGE <- c(AGE,rep(Months_ALT[i],684))
DATES <- c(DATES,rep(Dates_ALT[i],684))}




par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(ALTTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ALTTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ALTTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ALTTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ALTTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ALTTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Tree height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(CAPTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(CAPTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(CAPTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(CAPTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(CAPTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(CAPTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='CAP (cm)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()


Diam <-c(NA,16)
for (i in levels(TRAT)){
x1<-c()
for (j in as.numeric(levels(as.factor(AGE)))){
x1 <- c(x1, mean(CAPTOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
}
Diam<- data.frame(Diam,x1)}
Diam <- Diam[,2:7]

par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(Months_ALT,Diam[,1],xlab='Age (months)',ylab='CAP (cm)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,37),ylim=c(0,40),mgp=c(2.5,1,0))
points(Months_ALT,Diam[,2],type='b',pch=17,lwd=2,col=1)
points(Months_ALT,Diam[,3],type='b',pch=15,lwd=2,col=1)
points(Months_ALT,Diam[,4],type='b',pch=1,lwd=2,col=1,lty=2)
points(Months_ALT,Diam[,5],type='b',pch=2,lwd=2,col=1,lty=2)
points(Months_ALT,Diam[,6],type='b',pch=22,lwd=2,col=1,lty=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,17,15,1,2,22),lty=c(1,1,1,2,2,2),col=1,text.col=1,bg='white',box.col=0,cex=1.2);box()



# Copa et aire

#Months_ALT <- c(6,7,8,9,10,11,12,13,14,15,16,17,22,27)
# age biomasse 8 11 16 23 28

# relation

DAT <- read.table('Biomass.txt',header=T)
attach(DAT)


Mat <- matrix(c(Alt_copa,Alt_tot-Alt_copa),nrow=2,byrow=T)
row.names(Mat) <- c('Tronc','Couronne')
barplot(Mat,legend.text=T)

Mat <- matrix(c(Alt_tot-Alt_copa,Alt_copa),nrow=2,byrow=T)
row.names(Mat) <- c('Tronc','Couronne')
barplot(Mat,legend.text=T)

AGEBIOM <- c(8,11,16,23,28,36)

COEFC1<-c();COEFC2<-c();COEFC3<-c();COEFS1<-c();COEFS2<-c();COEFS3<-c()
COEFC12<-c(NA,NA);COEFC22<-c(NA,NA);COEFC32<-c(NA,NA);COEFS12<-c(NA,NA);COEFS22<-c(NA,NA);COEFS32<-c(NA,NA)
for (i in 1:6){
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1']
ft <- lm(X~Y-1)
COEFC1 <- c(COEFC1,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFC12 <- data.frame(COEFC12,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2']
ft <- lm(X~Y-1)
COEFC2 <- c(COEFC2,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFC22 <- data.frame(COEFC22,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3']
ft <- lm(X~Y-1)
COEFC3 <- c(COEFC3,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFC32 <- data.frame(COEFC32,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1']
ft <- lm(X~Y-1)
COEFS1 <- c(COEFS1,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFS12 <- data.frame(COEFS12,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2']
ft <- lm(X~Y-1)
COEFS2 <- c(COEFS2,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFS22 <- data.frame(COEFS22,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3']
X <- Alt_copa[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3']
ft <- lm(X~Y-1)
COEFS3 <- c(COEFS3,as.numeric(summary(ft)$coef[,1]))
#ft <- nls(X~a*Y^b)
#COEFS32 <- data.frame(COEFS32,as.numeric(summary(ft)$coef[,1]))
}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(Alt_tot[Trat=='C1'],Alt_copa[Trat=='C1'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFC1[i],col=i,lwd=2)
points(Alt_tot[Trat=='C1'][Idade[Trat=='C1']==AGEBIOM[i]],Alt_copa[Trat=='C1'][Idade[Trat=='C1']==AGEBIOM[i]],col=i)}
legend('topleft',legend='C1',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='C2'],Alt_copa[Trat=='C2'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFC2[i],col=i,lwd=2)
points(Alt_tot[Trat=='C2'][Idade[Trat=='C2']==AGEBIOM[i]],Alt_copa[Trat=='C2'][Idade[Trat=='C2']==AGEBIOM[i]],col=i)}
legend('topleft',legend='C2',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='C3'],Alt_copa[Trat=='C3'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFC3[i],col=i,lwd=2)
points(Alt_tot[Trat=='C3'][Idade[Trat=='C3']==AGEBIOM[i]],Alt_copa[Trat=='C3'][Idade[Trat=='C3']==AGEBIOM[i]],col=i)}
legend('topleft',legend='C3',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S1'],Alt_copa[Trat=='S1'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFS1[i],col=i,lwd=2)
points(Alt_tot[Trat=='S1'][Idade[Trat=='S1']==AGEBIOM[i]],Alt_copa[Trat=='S1'][Idade[Trat=='S1']==AGEBIOM[i]],col=i)}
legend('topleft',legend='S1',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S2'],Alt_copa[Trat=='S2'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFS2[i],col=i,lwd=2)
points(Alt_tot[Trat=='S2'][Idade[Trat=='S2']==AGEBIOM[i]],Alt_copa[Trat=='S2'][Idade[Trat=='S2']==AGEBIOM[i]],col=i)}
legend('topleft',legend='S2',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S3'],Alt_copa[Trat=='S3'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFS3[i],col=i,lwd=2)
points(Alt_tot[Trat=='S3'][Idade[Trat=='S3']==AGEBIOM[i]],Alt_copa[Trat=='S3'][Idade[Trat=='S3']==AGEBIOM[i]],col=i)}
legend('topleft',legend='S3',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)

ALTTOT <- c(ALT[,6],ALT[,7],ALT[,8],ALT[,9],ALT[,10],ALT[,11],ALT[,12],ALT[,13],ALT[,14],ALT[,15],ALT[,16],ALT[,17],ALT[,18],ALT[,19],ALT[,20],ALT[,21])
CAPTOT <- c(CAP6,CAP7,CAP8,CAP9,CAP10,CAP[,6],CAP[,7],CAP13,CAP14,CAP[,8],CAP16,CAP[,9],CAP[,10],CAP[,11],CAP[,12],CAP[,13])
#Months_ALT <- c(6,7,8,9,10,11,12,13,14,15,16,17,22,27)
# age biomasse 8 11 16 23 28
COEFcopa <- data.frame(COEFC1,COEFC2,COEFC3,COEFS1,COEFS2,COEFS3)

# 28 mois COPA = ALT - COPA

COPATOT <- c() 
for (i in 1:length(ALTTOT)){
  if (AGE[i] <= 8){
    COPATOT <- c(COPATOT,ALTTOT[i])}
  else if (AGE[i]==9){
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[1,k]*2/3+COEFcopa[2,k]*1/3))}
    }}
  else if (AGE[i]==10){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[1,k]*1/3+COEFcopa[2,k]*2/3))}
    }}
  else if (AGE[i]==11){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*COEFcopa[2,k])}
    }}
  else if (AGE[i]==12){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[2,k]*4/5+COEFcopa[3,k]*1/5))}
    }}
  else if (AGE[i]==13){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[2,k]*3/5+COEFcopa[3,k]*2/5))}
    }}
  else if (AGE[i]==14){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[2,k]*2/5+COEFcopa[3,k]*3/5))}
    }}
  else if (AGE[i]==15){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[2,k]*1/5+COEFcopa[3,k]*4/5))}
    }}
  else if (AGE[i]==16){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*COEFcopa[3,k])}
    }}
  else if (AGE[i]==17){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[3,k]*6/7+COEFcopa[4,k]*1/7))}
    }}
  else if (AGE[i]==22){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i]*(COEFcopa[3,k]*1/7+COEFcopa[4,k]*6/7))}
    }}
  else if (AGE[i]==27){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i] - ALTTOT[i]*(COEFcopa[4,k]*1/5+COEFcopa[5,k]*4/5))}
    }}
  else if (AGE[i]==31){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i] - ALTTOT[i]*(COEFcopa[5,k]*5/8+COEFcopa[6,k]*3/8))}
    }}
  else if (AGE[i]==36){    
    for (k in 1:6){
      if (TRAT[i]==levels(TRAT)[k]){ COPATOT <- c(COPATOT, ALTTOT[i] - ALTTOT[i]*(COEFcopa[5,k]))}
    }}
}

COPA <- rep(NA,16); sdCOPA <-rep(NA,16)
for (i in levels(TRAT)){
x1<-c();x2<-c();for (j in as.numeric(levels(as.factor(AGE)))){
x1 <- c(x1, mean(COPATOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
x2 <- c(x2, sd(COPATOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))}
COPA<- data.frame(COPA,x1)
sdCOPA <- data.frame(sdCOPA,x2)} 
COPA <- COPA[,2:7]; sdCOPA <- sdCOPA[,2:7]

par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')
par(mfrow=c(2,1))
plot(Alt_tot[Trat=='C1'],Alt_copa[Trat=='C1'],xlab='Total height (m)',ylab='Crown height (m)',cex.lab=1.2)
for (i in 1:6){
abline(0,COEFC1[i],col=i,lwd=2)
points(Alt_tot[Trat=='C1'][Idade[Trat=='C1']==AGEBIOM[i]],Alt_copa[Trat=='C1'][Idade[Trat=='C1']==AGEBIOM[i]],col=i)}
legend('topleft',legend='C1',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('8 months','11 months','16 months','23 months','28 months','36 months'),col=1:6,pch=NA,lty=1,bty='n',cex=1.2)
plot(Months_ALT,COPA[,1],xlab='Age (months)',ylab='Crown height (cm)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,36),ylim=c(0,10))
points(Months_ALT,COPA[,2],type='b',pch=16,lwd=2,col=3)
points(Months_ALT,COPA[,3],type='b',pch=16,lwd=2,col=2)
points(Months_ALT,COPA[,4],type='b',pch=4,lwd=2,col=1,lty=2)
points(Months_ALT,COPA[,5],type='b',pch=4,lwd=2,col=3,lty=2)
points(Months_ALT,COPA[,6],type='b',pch=4,lwd=2,col=2,lty=2)
add.errorbars(Months_ALT,COPA[,1],sdCOPA[,1],direction='vert',col=1)
add.errorbars(Months_ALT,COPA[,3],sdCOPA[,3],direction='vert',col=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,16,16,4,4,4),lty=c(1,1,1,2,2,2),col=c(1,3,2,1,3,2),text.col=c(1,3,2,1,3,2),bg='white',box.col=0,cex=1);box()

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(COPATOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(COPATOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(COPATOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(COPATOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(COPATOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(COPATOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Crown height (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()

# rayon dans la ligne et l'interligne, attention les mesures sont des diam�tres

RCOPA <- read.table('COPA.txt', header=T)
AGECOPA <- c(6,8)

RTOT <- c(RCOPA[,6],(RCOPA[,6]+RCOPA[,8])/2,RCOPA[,8])
RELTOT <- c(RCOPA[,7],(RCOPA[,7]+RCOPA[,9])/2,RCOPA[,9])

RL <- replace(RaioL,c(Idade==23),RaioL[Idade==23]*2)
REL <- replace(RaioEnt,c(Idade==23),RaioEnt[Idade==23]*2)

AGEBIOM <- c(8,11,16,23,28,36)
COEFC1<-rep(NA,2);COEFC2<-rep(NA,2);COEFC3<-rep(NA,2);COEFS1<-rep(NA,2);COEFS2<-rep(NA,2);COEFS3<-rep(NA,2)
for (i in 2:6){
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1']
ft <- lm(X~Y)
COEFC1 <- data.frame(COEFC1,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2']
ft <- lm(X~Y)
COEFC2 <- data.frame(COEFC2,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3']
ft <- lm(X~Y)
COEFC3 <- data.frame(COEFC3,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1']
ft <- lm(X~Y)
COEFS1 <- data.frame(COEFS1,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2']
ft <- lm(X~Y)
COEFS2 <- data.frame(COEFS2,as.numeric(summary(ft)$coef[,1]))
Y <- Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3']
X <- RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3']
ft <- lm(X~Y)
COEFS3 <- data.frame(COEFS3,as.numeric(summary(ft)$coef[,1]))
}


par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(Alt_tot[Trat=='C1'][7:38],RL[Trat=='C1'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFC1[1,i],COEFC1[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C1'],col=i,pch=16)}
legend('topleft',legend='C1',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='C2'][7:38],RL[Trat=='C2'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFC2[1,i],COEFC2[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C2'],col=i,pch=16)}
legend('topleft',legend='C2',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='C3'][7:38],RL[Trat=='C3'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFC3[1,i],COEFC3[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='C3'],col=i,pch=16)}
legend('topleft',legend='C3',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S1'][7:38],RL[Trat=='S1'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFS1[1,i],COEFS1[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S1'],col=i,pch=16)}
legend('topleft',legend='S1',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S2'][7:38],RL[Trat=='S2'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFS2[1,i],COEFS2[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S2'],col=i,pch=16)}
legend('topleft',legend='S2',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
plot(Alt_tot[Trat=='S3'][7:38],RL[Trat=='S3'][7:38],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2)
for (i in 2:6){
abline(COEFS3[1,i],COEFS3[2,i],col=i,lwd=2)
points(Alt_tot[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3'],RL[Idade==AGEBIOM[i]][Trat[Idade==AGEBIOM[i]]=='S3'],col=i,pch=16)}
legend('topleft',legend='S3',pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)

# autre m�thode

RL <- replace(RaioL,c(Idade==23),RaioL[Idade==23]*2)
REL <- replace(RaioEnt,c(Idade==23),RaioEnt[Idade==23]*2)

COEFPU1<-rep(NA,2)
COEFPU2<-rep(NA,2)
for (i in 1:6){
Y <- Alt_tot[Trat==levels(Trat)[i]]
X1 <- RL[Trat==levels(Trat)[i]]
X2 <- REL[Trat==levels(Trat)[i]]
ft1 <- nls(X1~a*log(Y)^b,start=c(a=1,b=2))
ft2 <- nls(X2~a*log(Y)^b,start=c(a=1,b=2))
COEFPU1 <- data.frame(COEFPU1,as.numeric(summary(ft1)$coef[,1]))
COEFPU2 <- data.frame(COEFPU2,as.numeric(summary(ft2)$coef[,1]))
}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,1))
plot(Alt_tot[Trat=='C1'],RL[Trat=='C1'],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2,pch=16,col=1,ylim=c(1,4.5),xlim=c(1,13))
X <- seq(min(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),length.out=50)
points(X,COEFPU1[1,2]*log(X)^COEFPU1[2,2],type='l',col=1,lwd=2)
for (i in 2:6){
points(Alt_tot[Trat==levels(Trat)[i]],RL[Trat==levels(Trat)[i]],col=i,pch=16)
X <- seq(min(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),length.out=50)
points(X,COEFPU1[1,i+1]*log(X)^COEFPU1[2,i+1],type='l',col=i,lwd=2)
}
legend('topleft',legend=levels(Trat),col=1:6,pch=16,bty='n',cex=1)

plot(Alt_tot[Trat=='C1'],REL[Trat=='C1'],xlab='Total height (m)',ylab='Crown inter-line diameter (m)',cex.lab=1.2,pch=16,col=1,ylim=c(1,5),xlim=c(1,13))
X <- seq(min(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),length.out=50)
points(X,COEFPU2[1,2]*log(X)^COEFPU2[2,2],type='l',col=1,lwd=2)
for (i in 2:6){
points(Alt_tot[Trat==levels(Trat)[i]],REL[Trat==levels(Trat)[i]],col=i,pch=16)
X <- seq(min(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),length.out=50)
points(X,COEFPU2[1,i+1]*log(X)^COEFPU2[2,i+1],type='l',col=i,lwd=2)
}
legend('topleft',legend=levels(Trat),col=1:6,pch=16,bty='n',cex=1)

# on extrapole

ALTTOT <- c(ALT[,6],ALT[,7],ALT[,8],ALT[,9],ALT[,10],ALT[,11],ALT[,12],ALT[,13],ALT[,14],ALT[,15],ALT[,16],ALT[,17],ALT[,18],ALT[,19],ALT[,20],ALT[,21])

RTOT <- c(RCOPA[,6],(RCOPA[,6]+RCOPA[,8])/2,RCOPA[,8])
RELTOT <- c(RCOPA[,7],(RCOPA[,7]+RCOPA[,9])/2,RCOPA[,9])

for (i in 2053:10944){
  for (j in 1:6){
  if(TRAT[i]==levels(Trat)[j]){  RTOT <- c(RTOT, COEFPU1[1,j+1]*log(ALTTOT[i])^COEFPU1[2,j+1])
                                 RELTOT <- c(RELTOT, COEFPU2[1,j+1]*log(ALTTOT[i])^COEFPU2[2,j+1])}}}


R <- rep(NA,16); sdR <-rep(NA,16)
RE <- rep(NA,16); sdRE <-rep(NA,16)
for (i in levels(TRAT)){
x1<-c();x2<-c();
x3<-c();x4<-c();
for (j in as.numeric(levels(as.factor(AGE)))){
x1 <- c(x1, mean(RTOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
x2 <- c(x2, sd(RTOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
x3 <- c(x3, mean(RELTOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
x4 <- c(x4, sd(RELTOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))}
R<- data.frame(R,x1);sdR <- data.frame(sdR,x2)
RE<- data.frame(RE,x1);sdRE <- data.frame(sdRE,x2)}
R <- R[,2:7]; sdR <- sdR[,2:7];RE <- RE[,2:7]; sdRE <- sdRE[,2:7]

par(mar=c(4,4.5,1,1))
par(xaxs='i',yaxs='i')
par(mfrow=c(2,1))
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,1))
plot(Alt_tot[Trat=='C1'],RL[Trat=='C1'],xlab='Total height (m)',ylab='Crown line diameter (m)',cex.lab=1.2,pch=16,col=1,ylim=c(1,4.5),xlim=c(1,13))
X <- seq(min(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[1]],na.rm=T),length.out=50)
points(X,COEFPU1[1,2]*log(X)^COEFPU1[2,2],type='l',col=1,lwd=2)
for (i in 2:6){
points(Alt_tot[Trat==levels(Trat)[i]],RL[Trat==levels(Trat)[i]],col=i,pch=16)
X <- seq(min(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),max(Alt_tot[Trat==levels(Trat)[i]],na.rm=T),length.out=50)
points(X,COEFPU1[1,i+1]*log(X)^COEFPU1[2,i+1],type='l',col=i,lwd=2)
}
legend('topleft',legend=levels(Trat),col=1:6,pch=16,bty='n',cex=1)
plot(Months_ALT,R[,1],xlab='Age (months)',ylab='Crown diamter (m)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,36),ylim=c(1,4))
points(Months_ALT,R[,2],type='b',pch=16,lwd=2,col=3)
points(Months_ALT,R[,3],type='b',pch=16,lwd=2,col=2)
points(Months_ALT,R[,4],type='b',pch=4,lwd=2,col=1,lty=2)
points(Months_ALT,R[,5],type='b',pch=4,lwd=2,col=3,lty=2)
points(Months_ALT,R[,6],type='b',pch=4,lwd=2,col=2,lty=2)
add.errorbars(Months_ALT,R[,1],sdR[,1],direction='vert',col=1)
add.errorbars(Months_ALT,R[,3],sdR[,3],direction='vert',col=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,16,16,4,4,4),lty=c(1,1,1,2,2,2),col=c(1,3,2,1,3,2),text.col=c(1,3,2,1,3,2),bg='white',box.col=0,cex=1);box()

                                  
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(RTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Crown line diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()
  
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(RELTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RELTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RELTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RELTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RELTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(RELTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Crown interline diameter (m)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()


# aire 

# jeunes arbres
COEFYA <- rep(NA,2)
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Surf_feuil_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
if (i==6){ Y[3] <- NA}
ft <- nls(Y~a*X^b,start=c(a=2,b=1),na.action=na.omit)
COEFYA <- data.frame(COEFYA,summary(ft)$par[,1])}

par(mar=c(4.5,4.5,1,1))
plot((RaioL[Idade==8]+RaioEnt[Idade==8])/2*Alt_tot[Idade==8],Surf_feuil_tot[Idade==8],xlab='CH (m�)',ylab='Leaf Area (m�)',cex.lab=1.2)
COL=c(1,2,3,1,2,3); LTY=c(1,1,1,2,2,2)
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Surf_feuil_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFYA[1,i+1]*x^COEFYA[2,i+1],col=COL[i],type='l',lty=LTY[i],lwd=2)
legend('topleft',legend=levels(Trat),col=COL,lty=LTY,pch=NA,cex=1.2,bty='n')
}


# arbres ag�s
COEFOA1 <- rep(NA,6)
COEFOA2 <- rep(NA,6)
for (j in c(11,16,23,28,36)){
Y1 <-c();Y2<-c()
for (i in 1:6){
X <- (Cap[Idade==j][Trat[Idade==j]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==j][Trat[Idade==j]==levels(Trat)[i]]
Y <- Surf_feuil_tot[Idade==j][Trat[Idade==j]==levels(Trat)[i]]
ft <- nls(Y~a*X^b,start=c(a=200,b=0.8))
Y1 <- c(Y1, summary(ft)$par[1,1])
Y2 <- c(Y2, summary(ft)$par[2,1])
}
COEFOA1 <- data.frame(COEFOA1,Y1)
COEFOA2 <- data.frame(COEFOA2,Y2)
}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
for (i in 1:6){
X1 <- (Cap[Trat==levels(Trat)[i]]/pi/100)^2*Alt_tot [Trat==levels(Trat)[i]]
plot(X1,Surf_feuil_tot[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Leaf Area (m�)',cex.lab=1.2)
#plot(D2H[Trat==levels(Trat)[i]],Surf_feuil_tot[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Leaf Area (m�)',cex.lab=1.2)
for (j in 2:6){
X <- (Cap[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
#X <- D2H[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
Y <- Surf_feuil_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
points(X,Y,col=j,pch=16)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFOA1[i,j]*x^COEFOA2[i,j],col=j,type='l',lwd=2)}
legend('topleft',legend=levels(Trat)[i],pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n',cex=1.2)
}


ATOT <- c()
for (i in 1:(684*3)){
  for (j in 1:6){
    if (TRAT[i]==levels(TRAT)[j]){ ATOT <- c(ATOT, COEFYA[1,j+1]*( (RTOT[i]+RELTOT[i])/2/2 * ALTTOT[i]) ^COEFYA[2,j+1])}}}

A11 <- c()
    for (i in 1:684){
    X <- (CAPTOT[AGE==11][i]/pi/100)^2*ALTTOT[AGE==11][i]
        if(TRAT[AGE==11][i]=='C1'){A11 <- c(A11, 7.213127+4319.127312*X)}
        if(TRAT[AGE==11][i]=='C2'){A11 <- c(A11, 12.824393+1443.475037*X)}
        if(TRAT[AGE==11][i]=='C3'){A11 <- c(A11, 8.265786+1824.898170*X)}
        if(TRAT[AGE==11][i]=='S1'){A11 <- c(A11, 7.381934+2710.711909*X)}
        if(TRAT[AGE==11][i]=='S2'){A11 <- c(A11, 9.234194+1571.241146*X)}
        if(TRAT[AGE==11][i]=='S3'){A11 <- c(A11, 5.190282+1796.928088*X)}}

A16 <- c()
    for (i in 1:684){
    X <- (CAPTOT[AGE==16][i]/pi/100)^2*ALTTOT[AGE==16][i]
        if(TRAT[AGE==16][i]=='C1'){A16 <- c(A16, 3.495324+884.051492*X)}
        if(TRAT[AGE==16][i]=='C2'){A16 <- c(A16, 9.833614+336.969636*X)}
        if(TRAT[AGE==16][i]=='C3'){A16 <- c(A16, 12.475845+173.124583*X)}
        if(TRAT[AGE==16][i]=='S1'){A16 <- c(A16, 2.857078+820.316696*X)}
        if(TRAT[AGE==16][i]=='S2'){A16 <- c(A16, 12.277644+103.080152*X)}
        if(TRAT[AGE==16][i]=='S3'){A16 <- c(A16, 4.795496+447.170612*X)}}


      
#A11 <- c()
#    for (i in 1:684){
#      for (k in 1:6){
#        if(TRAT[AGE==11][i]==levels(TRAT)[k]){
#          X <- (CAPTOT[AGE==11][i]/pi/100)^2*ALTTOT[AGE==11][i]
#          if(k==1|k==4){A11 <- c(A11, COEFOA1[k,2] *X ^COEFOA2[k,2])}#*0.75)}
#          if(k==2|k==3|k==5|k==6){A11 <- c(A11, COEFOA1[k,2] *X ^COEFOA2[k,2])}
#          }}}
#A16 <- c()
#    for (i in 1:684){
#      k=1
#        if(TRAT[AGE==16][i]==levels(TRAT)[k]){
#          X <- (CAPTOT[AGE==16][i]/pi/100)^2*ALTTOT[AGE==16][i]
#          A16 <- c(A16, COEFOA1[4,3] *X ^COEFOA2[4,3])}
#      for (k in 1:6){
#        if(TRAT[AGE==16][i]==levels(TRAT)[k]){
#          X <- (CAPTOT[AGE==16][i]/pi/100)^2*ALTTOT[AGE==16][i]
#          A16 <- c(A16, COEFOA1[k,3] *X ^COEFOA2[k,3])
#          }}}
A22 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==22][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==22][i]/pi/100)^2*ALTTOT[AGE==22][i]
          A22 <- c(A22, COEFOA1[k,4] *X ^COEFOA2[k,4])
          }}}
A27 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==27][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==27][i]/pi/100)^2*ALTTOT[AGE==27][i]
          A27 <- c(A27, COEFOA1[k,5] *X ^COEFOA2[k,5])
          }}}
A36 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==36][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==36][i]/pi/100)^2*ALTTOT[AGE==36][i]
          A36 <- c(A36, COEFOA1[k,6] *X ^COEFOA2[k,6])
          }}}


ATOT <- c(ATOT,ATOT[(2*684+1):(3*684)]*2/3+A11*1/3,ATOT[(2*684+1):(3*684)]*1/3+A11*2/3,
          A11,(A11*4/5+A16*1/5),(A11*3/5+A16*2/5),(A11*2/5+A16*3/5),A11*1/5+A16*4/5,A16,A16*5/6+A22*1/6,A22,A27,A27*5/9+A36*4/9,A36)

#age <- c(rep(8,684),rep(11,684),rep(16,684),rep(23,684),rep(27,684),rep(36,684))
#ATOT <- c(ATOT[(684*2+1):(684*3)],A11,A16,A22,A27,A36)

AREA <-c(NA,16); sdAREA <-c(NA,16)
for (i in levels(TRAT)){
x1<-c();x2<-c();
for (j in as.numeric(levels(as.factor(age)))){
x1 <- c(x1, mean(ATOT[TRAT==i][age[TRAT==i]==j],na.rm=T)/6)
x2 <- c(x2, sd(ATOT[TRAT==i][age[TRAT==i]==j],na.rm=T))}
AREA<- data.frame(AREA,x1);sdAREA <- data.frame(sdAREA,x2)}
AREA <- AREA[,2:7]; sdAREA <- sdAREA[,2:7]

mois <- c(8,11,16,23,27,36)
par(mar=c(4,4,1,1))
par(xaxs='i',yaxs='i')
plot(mois,AREA[,1],xlab='Age (months)',ylab='LAI (m�/m�)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,37),ylim=c(0,7),mgp=c(2.5,1,0))
points(mois,AREA[,2],type='b',pch=17,lwd=2,col=1)
points(mois,AREA[,3],type='b',pch=15,lwd=2,col=1)
points(mois,AREA[,4],type='b',pch=1,lwd=2,col=1,lty=2)
points(mois,AREA[,5],type='b',pch=2,lwd=2,col=1,lty=2)
points(mois,AREA[,6],type='b',pch=22,lwd=2,col=1,lty=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,17,15,1,2,22),lty=c(1,1,1,2,2,2),col=1,text.col=1,bg='white',box.col=0,cex=1.2);box()

          
AREA <-c(NA,16); sdAREA <-c(NA,16)
for (i in levels(TRAT)){
x1<-c();x2<-c();
for (j in as.numeric(levels(as.factor(AGE)))){
x1 <- c(x1, mean(ATOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))
x2 <- c(x2, sd(ATOT[TRAT==i][AGE[TRAT==i]==j],na.rm=T))}
AREA<- data.frame(AREA,x1);sdAREA <- data.frame(sdAREA,x2)}
AREA <- AREA[,2:7]; sdAREA <- sdAREA[,2:7]

windows(10,8)
split.screen(c(2,1))
split.screen(c(1,2),1)
screen(3)
par(mar=c(4.5,4.5,1,1))
plot((RaioL[Idade==8]+RaioEnt[Idade==8])/2*Alt_tot[Idade==8],Surf_feuil_tot[Idade==8],xlab='CH (m�)',ylab='Leaf Area (m�)',cex.lab=1.2)
COL=c(1,2,3,1,2,3); LTY=c(1,1,1,2,2,2)
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Surf_feuil_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFYA[1,i+1]*x^COEFYA[2,i+1],col=COL[i],type='l',lty=LTY[i],lwd=2)
legend('topleft',legend='8 monhts',pch=NA,bty='n',cex=1)
legend('bottomright',legend=levels(Trat),col=COL,lty=LTY,pch=NA,cex=1,bty='n')
}
screen(4)
par(mar=c(4.5,4.5,1,1))
i=1
X1 <- (Cap[Trat==levels(Trat)[i]]/pi/100)^2*Alt_tot [Trat==levels(Trat)[i]]
plot(X1,Surf_feuil_tot[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Leaf Area (m�)',cex.lab=1.2)
for (j in 2:6){
X <- (Cap[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
Y <- Surf_feuil_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
points(X,Y,col=j,pch=16)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFOA1[i,j]*x^COEFOA2[i,j],col=j,type='l',lwd=2)}
legend('topleft',legend=levels(Trat)[i],pch=NA,bty='n',cex=1)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1)
screen(2)
par(mar=c(4.5,4.5,0.2,1))
par(xaxs='i',yaxs='i')
plot(Months_ALT,AREA[,1],xlab='Age (months)',ylab='Tree leaf area (m�)',cex.lab=1.2,type='b',pch=16,lwd=2,xlim=c(0,38),ylim=c(1,35))
points(Months_ALT,AREA[,2],type='b',pch=16,lwd=2,col=3)
points(Months_ALT,AREA[,3],type='b',pch=16,lwd=2,col=2)
points(Months_ALT,AREA[,4],type='b',pch=4,lwd=2,col=1,lty=2)
points(Months_ALT,AREA[,5],type='b',pch=4,lwd=2,col=3,lty=2)
points(Months_ALT,AREA[,6],type='b',pch=4,lwd=2,col=2,lty=2)
add.errorbars(Months_ALT,AREA[,1],sdAREA[,1],direction='vert',col=1)
add.errorbars(Months_ALT,AREA[,3],sdAREA[,3],direction='vert',col=2)
grid(nx=NA,ny=NULL,col=1)
legend('topleft',legend=c('C1','C2','C3','S1','S2','S3'),pch=c(16,16,16,4,4,4),lty=c(1,1,1,2,2,2),col=c(1,3,2,1,3,2),text.col=c(1,3,2,1,3,2),bg='white',box.col=0,cex=1);box()


 
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(ATOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ATOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ATOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ATOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ATOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(ATOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Tree leaf area (m�)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()


# biomasse

COEFYA <- rep(NA,2)
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Folhatotal[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
if (i==6){ Y[3] <- NA}
ft <- nls(Y~a*X^b,start=c(a=2,b=1))
COEFYA <- data.frame(COEFYA,summary(ft)$par[,1])}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Folhatotal[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
plot(X,Y,xlab='CH (m�)',ylab='Leaf dry mass (kg)',cex.lab=1.2)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFYA[1,i+1]*x^COEFYA[2,i+1],col=2,type='l',lwd=2)
legend('topleft',legend=levels(Trat)[i],pch=NA,cex=1.2,bty='n')
}

COEFOA1 <- rep(NA,6)
COEFOA2 <- rep(NA,6)
for (j in c(11,16,23,28,36)){
Y1 <-c();Y2<-c()
for (i in 1:6){
X <- (Cap[Idade==j][Trat[Idade==j]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==j][Trat[Idade==j]==levels(Trat)[i]]
Y <- Folhatotal[Idade==j][Trat[Idade==j]==levels(Trat)[i]]
ft <- nls(Y~a*X^b,start=c(a=5,b=0.2))
Y1 <- c(Y1, summary(ft)$par[1,1])
Y2 <- c(Y2, summary(ft)$par[2,1])
}
COEFOA1 <- data.frame(COEFOA1,Y1)
COEFOA2 <- data.frame(COEFOA2,Y2)
}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
for (i in 1:6){
X1 <- (Cap[Trat==levels(Trat)[i]]/pi/100)^2*Alt_tot [Trat==levels(Trat)[i]]
plot(X1,Folhatotal[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Tree leaf dry mass (kg)',cex.lab=1.2)
#plot(D2H[Trat==levels(Trat)[i]],Surf_feuil_tot[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Leaf Area (m�)',cex.lab=1.2)
for (j in 2:6){
X <- (Cap[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
#X <- D2H[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
Y <- Folhatotal [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
points(X,Y,col=j,pch=16)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFOA1[i,j]*x^COEFOA2[i,j],col=j,type='l',lwd=2)}
legend('topleft',legend=levels(Trat)[i],pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months','36 months'),col=2:6,pch=NA,lty=1,bty='n',cex=1.2)
}


BIOMTOT <- c()
for (i in 1:(684*3)){
  for (j in 1:6){
    if (TRAT[i]==levels(TRAT)[j]){ BIOMTOT <- c(BIOMTOT, COEFYA[1,j+1]*( (RTOT[i]+RELTOT[i])/2/2 * ALTTOT[i]) ^COEFYA[2,j+1])}}}

B11 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==11][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==11][i]/pi/100)^2*ALTTOT[AGE==11][i]
          B11 <- c(B11, COEFOA1[k,2] *X ^COEFOA2[k,2])
          }}}
B16 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==16][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==16][i]/pi/100)^2*ALTTOT[AGE==16][i]
          B16 <- c(B16, COEFOA1[k,3] *X ^COEFOA2[k,3])
          }}}
B22 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==22][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==22][i]/pi/100)^2*ALTTOT[AGE==22][i]
          B22 <- c(B22, COEFOA1[k,4] *X ^COEFOA2[k,4])
          }}}
B27 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==27][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==27][i]/pi/100)^2*ALTTOT[AGE==27][i]
          B27 <- c(B27, COEFOA1[k,5] *X ^COEFOA2[k,5])
          }}}
B36 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==36][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==36][i]/pi/100)^2*ALTTOT[AGE==36][i]
          B36 <- c(B36, COEFOA1[k,6] *X ^COEFOA2[k,6])
          }}}

BIOMTOT <- c(BIOMTOT,BIOMTOT[(2*684+1):(3*684)]*2/3+B11*1/3,BIOMTOT[(2*684+1):(3*684)]*1/3+B11*2/3,
          B11,(B11*4/5+B16*1/5),(B11*3/5+B16*2/5),(B11*2/5+B16*3/5),B11*1/5+B16*4/5,B16,B16*5/6+B22*1/6,B22,B27,B27*5/9+B36*4/9,B36)

  
          
 
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(BIOMTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(BIOMTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(BIOMTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(BIOMTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(BIOMTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(BIOMTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Tree leaf dry mass (kg)',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()


# nombre de feuille par arbre

COEFYA <- rep(NA,2)
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Nb_Feuil[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
ft <- nls(Y~a*X^b,start=c(a=5000,b=1))
COEFYA <- data.frame(COEFYA,summary(ft)$par[,1])}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
for (i in 1:6){
X <- (RaioL[Idade==8][Trat[Idade==8]==levels(Trat)[i]]+RaioEnt[Idade==8][Trat[Idade==8]==levels(Trat)[i]])/2* Alt_tot[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
Y <- Nb_Feuil[Idade==8][Trat[Idade==8]==levels(Trat)[i]]
plot(X,Y,xlab='CH (m�)',ylab='Number of leaves per tree',cex.lab=1.2)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFYA[1,i+1]*x^COEFYA[2,i+1],col=2,type='l',lwd=2)
legend('topleft',legend=levels(Trat)[i],pch=NA,cex=1.2,bty='n')
}

COEFOA1 <- rep(NA,6)
COEFOA2 <- rep(NA,6)
for (j in c(11,16,23,28)){
Y1 <-c();Y2<-c()
for (i in 1:6){
X <- (Cap[Idade==j][Trat[Idade==j]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==j][Trat[Idade==j]==levels(Trat)[i]]
Y <- Nb_Feuil[Idade==j][Trat[Idade==j]==levels(Trat)[i]]
ft <- nls(Y~a*X^b,start=c(a=5000,b=0.2))
Y1 <- c(Y1, summary(ft)$par[1,1])
Y2 <- c(Y2, summary(ft)$par[2,1])
}
COEFOA1 <- data.frame(COEFOA1,Y1)
COEFOA2 <- data.frame(COEFOA2,Y2)
}

par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
for (i in 1:6){
X1 <- (Cap[Trat==levels(Trat)[i]]/pi/100)^2*Alt_tot [Trat==levels(Trat)[i]]
plot(X1,Nb_Feuil[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Numer of leaves per tree',cex.lab=1.2)
#plot(D2H[Trat==levels(Trat)[i]],Surf_feuil_tot[Trat==levels(Trat)[i]],xlab='D�H (m3)',ylab='Leaf Area (m�)',cex.lab=1.2)
for (j in 2:5){
X <- (Cap[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]/pi/100)^2*Alt_tot [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
#X <- D2H[Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
Y <- Nb_Feuil [Idade==AGEBIOM[j]][Trat[Idade==AGEBIOM[j]]==levels(Trat)[i]]
points(X,Y,col=j,pch=16)
x <- seq(min(X,na.rm=T),max(X,na.rm=T),length.out=50)
points(x,COEFOA1[i,j]*x^COEFOA2[i,j],col=j,type='l',lwd=2)}
legend('topleft',legend=levels(Trat)[i],pch=NA,bty='n',cex=1.2)
legend('bottomright',legend=c('11 months','16 months','23 months','28 months'),col=2:5,pch=NA,lty=1,bty='n',cex=1.2)
}


NLTOT <- c()
for (i in 1:(684*3)){
  for (j in 1:6){
    if (TRAT[i]==levels(TRAT)[j]){ NLTOT <- c(NLTOT, COEFYA[1,j+1]*( (RTOT[i]+RELTOT[i])/2/2 * ALTTOT[i]) ^COEFYA[2,j+1])}}}

NL11 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==11][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==11][i]/pi/100)^2*ALTTOT[AGE==11][i]
          NL11 <- c(NL11, COEFOA1[k,2] *X ^COEFOA2[k,2])
          }}}
NL16 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==16][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==16][i]/pi/100)^2*ALTTOT[AGE==16][i]
          NL16 <- c(NL16, COEFOA1[k,3] *X ^COEFOA2[k,3])
          }}}
NL22 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==22][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==22][i]/pi/100)^2*ALTTOT[AGE==22][i]
          NL22 <- c(NL22, COEFOA1[k,4] *X ^COEFOA2[k,4])
          }}}
NL27 <- c()
    for (i in 1:684){
      for (k in 1:6){
        if(TRAT[AGE==27][i]==levels(TRAT)[k]){
          X <- (CAPTOT[AGE==27][i]/pi/100)^2*ALTTOT[AGE==27][i]
          NL27 <- c(NL27, COEFOA1[k,5] *X ^COEFOA2[k,5])
          }}}

NLTOT <- c(NLTOT,NLTOT[(2*684+1):(3*684)]*2/3+NL11*1/3,NLTOT[(2*684+1):(3*684)]*1/3+NL11*2/3,
          NL11,(NL11*4/5+NL16*1/5),(NL11*3/5+NL16*2/5),(NL11*2/5+NL16*3/5),NL11*1/5+NL16*4/5,NL16,NL16*5/6+NL22*1/6,NL22,NL27,rep(NA,length(NL27)*2))

  
          
 
par(mar=c(4.5,4.5,1,1))
par(mfrow=c(2,3))
plot(NLTOT[TRAT=='C1']~AGE[TRAT=='C1'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(NLTOT[TRAT=='C2']~AGE[TRAT=='C2'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(NLTOT[TRAT=='C3']~AGE[TRAT=='C3'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='C3',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(NLTOT[TRAT=='S1']~AGE[TRAT=='S1'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S1',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(NLTOT[TRAT=='S2']~AGE[TRAT=='S2'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S2',pch=NA,cex=1.2,box.col=0,bg='white');box()
plot(NLTOT[TRAT=='S3']~AGE[TRAT=='S3'],ylab='Number of leaves per tree',xlab='Age (months)',cex.lab=1.2)
grid(nx=NA,ny=NULL,lty='dotted',col=1)
legend('topleft',legend='S3',pch=NA,cex=1.2,box.col=0,bg='white');box()


BLOCO <- rep(CAP$Bloco,16)
MAT <- data.frame(DATES,BLOCO,AGE,TRAT,ALTTOT,CAPTOT,COPATOT,RTOT,RELTOT,ATOT,BIOMTOT,NLTOT)

write.table(MAT,'Datas_intern.txt',col.names=T,row.names=F)





 






