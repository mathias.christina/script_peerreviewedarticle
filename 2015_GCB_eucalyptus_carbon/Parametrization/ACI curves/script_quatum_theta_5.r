set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/Courbes r�ponse lumi�re")
setwd(set)

datas <- read.table("Alldatas.txt", header=T)
datas2 <- datas[4,]
for (i in 5:dim(datas)[1]){
if (datas$Obs[i] > 3){ datas2 <- rbind(datas2,datas[i,])}
else { datas2 <- datas2}}
attach(datas2)


Acal1 <- function (PARi,Jmax25,Vcmax25,Rd25, Ci,Tleaf,Press,Photo,gm,theta, alpha,N){
# constantes literature
EAVJ = 43790 ; EDVJ = 200000 ; DELSJ = 644.4338 ; EAVC = 60790 ; EDVC = 200000 ; DELSV = 636 ; EARD = 53791
Kc25 = 40.49 ; Ko25 = 27840 ; KcEa = 79430 ; KoEa = 36380 ;R = 8.314

d1 <- c(rep(1,length(PARi[N==1]),rep(0,30*5+1-3))
d2 <- c(rep(0,30),rep(1,30),rep(0,30*4+1-3))
d3 <- c(rep(0,30*2),rep(1,30),rep(0,30*3+1))
d4 <- c(rep(0,30*3-3),rep(1,30),rep(0,30*2+1))
d5 <- c(rep(0,30*4-3),rep(1,31),rep(0,30))
d6 <- c(rep(0,30*5+1-3),rep(1,30))

#d�pendance � la temp�rature
Jmax <-c();Vcmax <- c();Rd <-c()
for (i in 1:6){

Jmax <- c(Jmax,Jmax25[i] *exp(EAVJ*(Tleaf-25) / (R* ((Tleaf+273.15)*298.15)))* ( 1 +
    exp((DELSJ*298 - EDVJ) / (R*298.15)))/( 1 + exp((DELSJ*(Tleaf+273.15) - EDVJ) / (R*(Tleaf+273.15)) ) ))
Vcmax <- c(Vcmax,Vcmax25[i] *exp(EAVC*(Tleaf-25) / (R* ((Tleaf+273)*298)))* ( 1 +
    exp((DELSV*298 - EDVC) / (R*298.15)))/( 1 + exp((DELSV*(Tleaf+273.15) - EDVC) / (R*(Tleaf+273.15)) ) ))
Q10F <- EARD/(8.314 * ((Tleaf+273.15)*298.15))
Rd <- c(Rd,Rd25[i] *exp(Q10F*(Tleaf-25)))
}

Ko <- Ko25 * exp(KoEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))
Kc <- Kc25 * exp(KcEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))

#equations pr�liminaires
Gstar2 <- 3.69 * exp(37.8/R * (1/298.15 - 1/(Tleaf+273.15)))
Gstar <- 36.9 +1.88 * (Tleaf-25) + 0.036 * (Tleaf-25)^2
Pair <- Press * 1000         # Press est en kPa
O <- 0.21*Pair
KMFN <- Kc * (1 + O/Ko)
CI <- Ci*10^-6*Pair  # Ci doit �tre en Pa, or Ci du licor est en ppm
Cc <- CI - Photo*10^-6 *Pair/gm

if (min((alpha*PARi+Jmax)^2 - 4*theta*alpha*PARi*Jmax) <0){ J = 0}
else {
J <- ((alpha*PARi+Jmax) - sqrt((alpha*PARi+Jmax)^2 - 4*theta*alpha*PARi*Jmax))/(2*theta)
}

JM <- d1*Jmax[1:196]+d2*Jmax[197:392]+d3*Jmax[393:588]+d4*Jmax[589:784]+d5*Jmax[785:980]+d6*Jmax[981:1176]

#equation pour Ac
Ac <- Vcmax * (Cc - Gstar2) / (Cc + KMFN) - Rd
#equation pour Aj
Aj <- J/4*(Cc-Gstar2)/(Cc+2*Gstar2)

#finallement
A <-c();
for (i in 1:length(Ac)){
A <- c(A, min(c(Ac[i],Aj[i])) )}

OUT <-data.frame(A,Ac,Aj)
#print(data.frame(A,Ac,Aj,Ap))  # si Acal2
#print(A)
}

sce1  <-  function(param,x,  yobs)  {
Jmax25 <- param[1] ; Vcmax25 <- param[2] ; Rd25 <- param[3] ; gm = param[4]; theta = param[5]; alpha=param[6]
ytheo <- Acal1(x[,1],Jmax25,Vcmax25,Rd25, x[,2],x[,3],x[,4],x[,5],gm,theta, alpha)[,1]
return(sum((yobs  -  ytheo)^2))
}



COEF1 <- rep(NA,6)
COEF2 <- rep(NA,6)
for (i in 1:24){
xdatas <- data.frame(PARi[NUM==i], Ci[NUM==i],Tleaf[NUM==i],Press[NUM==i],Photo[NUM==i])
Aobs <- Photo[NUM==i]
parini <- c(180,90,1,2,0.9,0.2)
Lower <- c(0.001,0.01,0.01,0.01,0.01)
Upper <- c(500,500,50,50,5,5)
fit1 <- optim(par  = parini,fn  =  sce1, yobs=Aobs,x=xdatas,
              method="Nelder-Mead")
fit2 <- optim(par  = parini,fn  =  sce1, yobs=Aobs,x=xdatas,
              method="L-BFGS-B",lower=Lower,upper=Upper)
COEF1 <- data.frame(COEF1,as.numeric(fit1$par))
COEF2 <- data.frame(COEF2,as.numeric(fit2$par))
}

par(mfrow=c(2,3))
for (i in 1:6){
plot(PARi[NUM==i],Photo[NUM==i])
Y1 <- Acal1(PARi[NUM==i],COEF1[1,i+1],COEF1[2,i+1],COEF1[3,i+1],Ci[NUM==i],Tleaf[NUM==i],Press[NUM==i],Photo[NUM==i],
            COEF1[4,i+1],COEF1[5,i+1],COEF1[6,i+1])[,1]
Y2 <- Acal1(PARi[NUM==i],COEF2[1,i+1],COEF2[2,i+1],COEF2[3,i+1],Ci[NUM==i],Tleaf[NUM==i],Press[NUM==i],Photo[NUM==i],
            COEF2[4,i+1],COEF2[5,i+1],COEF2[6,i+1])[,1]
points(PARi[NUM==i],Y1,type='l',col=2)
points(PARi[NUM==i],Y2,type='l',col=4)
}


