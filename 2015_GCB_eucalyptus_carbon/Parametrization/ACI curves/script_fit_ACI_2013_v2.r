set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/courbes ACI")
setwd(set)

datas <- read.table("All_datas_2013.txt", header=T)

RMSE <- function(yobs,ytheo,k,n){
R <- sum((yobs  -  ytheo)^2)/(n-k-1)
print(R)}
add.errorbars <- function(x,y,SE,direction,barlen=0.04){
if(direction=="up")arrows(x0=x, x1=x, y0=y, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="down")arrows(x0=x, x1=x, y0=y, y1=y - SE, code=3,
angle=90, length=barlen)
if(direction=="left")arrows(x0=x, x1=x-SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="right")arrows(x0=x, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
if(direction=="vert")arrows(x0=x, x1=x, y0=y-SE, y1=y + SE, code=3,
angle=90, length=barlen)
if(direction=="hor")arrows(x0=x-SE, x1=x+SE, y0=y, y1=y, code=3,
angle=90, length=barlen)
}

# fonction � fitter

ACC <- function (PARi,Jmax25,Vcmax25,Rd25, Ci,Tleaf,Press,Photo,gm,TPU=0,trat){
# constantes literature
EAVJ = 43790 ; EDVJ = 200000 ; DELSJ = 644.4338 ; EAVC = 60790 ; EDVC = 200000 ; DELSV = 636 ; EARD = 53791
Kc25 = 40.49 ; Ko25 = 27840 ; KcEa = 79430 ; KoEa = 36380 ;R = 8.314
#constante calcul�e
if (trat=='C1'){alpha=0.30729;theta=0.6929147}
else if(trat=='C3'){theta = 0.6929147 ; alpha = 0.35422}
else if (trat=='S1'){theta = 0.6929147 ; alpha = 0.36110}
else if (trat=='S3'){theta = 0.6929147 ; alpha = 0.28804}

#d�pendance � la temp�rature
Jmax <- Jmax25 *exp(EAVJ*(Tleaf-25) / (R* ((Tleaf+273.15)*298.15)))* ( 1 +
    exp((DELSJ*298 - EDVJ) / (R*298.15)))/( 1 + exp((DELSJ*(Tleaf+273.15) - EDVJ) / (R*(Tleaf+273.15)) ) )
Vcmax <- Vcmax25 *exp(EAVC*(Tleaf-25) / (R* ((Tleaf+273)*298)))* ( 1 +
    exp((DELSV*298 - EDVC) / (R*298.15)))/( 1 + exp((DELSV*(Tleaf+273.15) - EDVC) / (R*(Tleaf+273.15)) ) )
Q10F <- EARD/(8.314 * ((Tleaf+273.15)*298.15))
Rd <- Rd25 *exp(Q10F*(Tleaf-25))
Ko <- Ko25 * exp(KoEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))
Kc <- Kc25 * exp(KcEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))

#equations pr�liminaires
Gstar2 <- 3.69 * exp(37.8/R * (1/298.15 - 1/(Tleaf+273.15)))
#Gstar <- 36.9 +1.88 * (Tleaf-25) + 0.0036 * (Tleaf-25)^2
Pair <- Press * 1000         # Press est en kPa
O <- 0.21*Pair
KMFN <- Kc * (1 + O/Ko)
CI <- Ci*10^-6*Pair  # Ci doit �tre en Pa, or Ci du licor est en ppm
Cc <- CI - Photo*10^-6 *Pair/gm
J <- ((alpha*PARi+Jmax) - sqrt((alpha*PARi+Jmax)^2 - 4*theta*alpha*PARi*Jmax))/(2*theta)

#equation pour Ac
Ac <- Vcmax * (Cc - Gstar2) / (Cc + KMFN) - Rd
#equation pour Aj
Aj <- J/4*(Cc-Gstar2)/(Cc+2*Gstar2)
#equation pour Ap
Ap <- 3* TPU - Rd

#finallement
A <-c();
for (i in 1:length(Ac)){
A <- c(A, min(c(Ac[i],Aj[i])) )}
#A <- c(A, min(c(Ac[i],Aj[i],Ap[i])) )}

print(data.frame(A,Ac,Aj))  
#print(data.frame(A,Ac,Aj,Ap))
#print(A)
}

ACI <- function (PARi,Jmax25,Vcmax25,Rd25, Ci,Tleaf,Press,Photo,TPU=0,trat){
# constantes literature
EAVJ = 43790 ; EDVJ = 200000 ; DELSJ = 644.4338 ; EAVC = 60790 ; EDVC = 200000 ; DELSV = 636 ; EARD = 53791
Kc25 = 40.49 ; Ko25 = 27840 ; KcEa = 79430 ; KoEa = 36380 ;R = 8.314
#constante calcul�e
if (trat=='C1'){alpha=0.30729;theta=0.6929147}
else if(trat=='C3'){theta = 0.6929147 ; alpha = 0.35422}
else if (trat=='S1'){theta = 0.6929147 ; alpha = 0.36110}
else if (trat=='S3'){theta = 0.6929147 ; alpha = 0.28804}

#d�pendance � la temp�rature
Jmax <- Jmax25 *exp(EAVJ*(Tleaf-25) / (R* ((Tleaf+273.15)*298.15)))* ( 1 +
    exp((DELSJ*298 - EDVJ) / (R*298.15)))/( 1 + exp((DELSJ*(Tleaf+273.15) - EDVJ) / (R*(Tleaf+273.15)) ) )
Vcmax <- Vcmax25 *exp(EAVC*(Tleaf-25) / (R* ((Tleaf+273)*298)))* ( 1 +
    exp((DELSV*298 - EDVC) / (R*298.15)))/( 1 + exp((DELSV*(Tleaf+273.15) - EDVC) / (R*(Tleaf+273.15)) ) )
Q10F <- EARD/(8.314 * ((Tleaf+273.15)*298.15))
Rd <- Rd25 *exp(Q10F*(Tleaf-25))
Ko <- Ko25 * exp(KoEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))
Kc <- Kc25 * exp(KcEa * (Tleaf-25)/ (R * ((Tleaf+273.15) +273.15)*(298.15+273.15)))

#equations pr�liminaires
Gstar2 <- 3.69 * exp(37.8/R * (1/298.15 - 1/(Tleaf+273.15)))
#Gstar <- 36.9 +1.88 * (Tleaf-25) + 0.0036 * (Tleaf-25)^2
Pair <- Press * 1000         # Press est en kPa
O <- 0.21*Pair
KMFN <- Kc * (1 + O/Ko)
CI <- Ci*10^-6*Pair  # Ci doit �tre en Pa, or Ci du licor est en ppm
Cc <- CI #- Photo*10^-6 *Pair/gm
J <- ((alpha*PARi+Jmax) - sqrt((alpha*PARi+Jmax)^2 - 4*theta*alpha*PARi*Jmax))/(2*theta)

#equation pour Ac
Ac <- Vcmax * (Cc - Gstar2) / (Cc + KMFN) - Rd
#equation pour Aj
Aj <- J/4*(Cc-Gstar2)/(Cc+2*Gstar2)
#equation pour Ap
Ap <- 3* TPU - Rd

#finallement
A <-c();
for (i in 1:length(Ac)){
A <- c(A, min(c(Ac[i],Aj[i])) )}
#A <- c(A, min(c(Ac[i],Aj[i],Ap[i])) )}

print(data.frame(A,Ac,Aj))  
#print(data.frame(A,Ac,Aj,Ap))
#print(A)
}



#retirons les points dans le noir
Ci13 <-c();PARi13 <-c();Photo13 <-c(); Tleaf13<-c();Press13 <-c();Nf <-c();trat13<-c()
ph13 <-c();pv13<-c();tt13<-c()
for (i in 1: length(datas$Photo)){
if( datas$PARi[i] >1500){
Ci13 <- c(Ci13, datas$Ci[i])
PARi13 <- c(PARi13,datas$PARi[i])
Photo13 <- c(Photo13, datas$Photo[i])
Tleaf13 <- c(Tleaf13,datas$Tleaf[i])
Press13 <- c(Press13,datas$Press[i])
ph13 <-c(ph13, as.vector(datas$PH[i]))
pv13 <-c(pv13, as.vector(datas$PV[i]))
tt13 <-c(tt13, as.vector(datas$TT[i]))
Nf <- c(Nf,datas$NUM[i])
trat13 <- c(trat13,as.vector(datas$TRAT)[i])
}
else { Ci13 <- Ci13; PARi13 <-PARi13;Photo13<-Photo13;Tleaf13<-Tleaf13;Press13<-Press13;Nf<-Nf;trat13<-trat13;ph13 <- ph13;pv13<-pv13;tt13<-tt13}}
CIREF13 <-c()
for (i in 1:69){
CIREF13 <- c(CIREF13, mean (Ci13[Nf==i][1:3]))
}


#equation de minimization
sceCI  <-  function(param,x,  yobs,trat)  {
Jmax25 <- param[1] ; Vcmax25 <- param[2] ; Rd25 <- param[3]  
ytheo <- ACI(x[,1],Jmax25,Vcmax25,Rd25, x[,2],x[,3],x[,4],x[,5],0,trat)[,1]
return(sum((yobs  -  ytheo)^2))
}
sceCC  <-  function(param,x,  yobs,trat)  {
Jmax25 <- param[1] ; Vcmax25 <- param[2] ; Rd25 <- param[3]  ; gm = param[4]#;TPU = param[5]
ytheo <- ACC(x[,1],Jmax25,Vcmax25,Rd25, x[,2],x[,3],x[,4],x[,5],gm,0,trat)[,1]
return(sum((yobs  -  ytheo)^2))
}


# en 2012 maintenant
datas <- read.table("All_datas.txt", header=T)

#retirons les points dans le noir
Ci12 <-c();PARi12 <-c();Photo12 <-c(); Tleaf12<-c();Press12 <-c();Nf <-c();trat12<-c()
ph12 <-c();pv12<-c();tt12<-c()
for (i in 1: length(datas$Photo)){
if( datas$PARi[i] >1500){
Ci12 <- c(Ci12, datas$Ci[i])
PARi12 <- c(PARi12,datas$PARi[i])
Photo12 <- c(Photo12, datas$Photo[i])
Tleaf12 <- c(Tleaf12,datas$Tleaf[i])
Press12 <- c(Press12,datas$Press[i])
ph12 <-c(ph12, as.vector(datas$PH[i]))
pv12 <-c(pv12, as.vector(datas$PV[i]))
tt12 <-c(tt12, as.vector(datas$TT[i]))
Nf <- c(Nf,datas$NUM[i])
trat12 <- c(trat12,as.vector(datas$TRAT)[i])
}
else { Ci12 <- Ci12; PARi12 <-PARi12;Photo12<-Photo12;Tleaf12<-Tleaf12;Press12<-Press12;Nf<-Nf;trat12<-trat12;ph12 <- ph12;pv12<-pv12;tt12<-tt12}}
CIREF12 <-c()
for (i in 1:72){
CIREF12 <- c(CIREF12, mean (Ci12[Nf==i][1:3]))
}


# on sort la sauveguarde
load("C:\\Users\\mathias\\Documents\\Cours et stage\\thesis\\Exclusion des pluies\\courbes ACI\\sauv_fit_ACI_2012")

RMSE_m1 <- replace(RMSE_m1,c(gmes1<0|gmes1<0),NA)
ID1 <- c();ID2 <- c();ID3 <- c();ID4<-c()
for (i in 1:72){
Val1 <- min(RMSE_m1[(1+500*(i-1)):(i*500)],na.rm=T)
ID1 <- c(ID1, which(RMSE_m1[(1+500*(i-1)):(i*500)]==Val1)+500*(i-1))
}

Jmax12 <- Jm1[ID1];Vcmax12 <- Vcm1[ID1];Rd12 <- Resp1[ID1];Gm12 <-gmes1[ID1]
Tt12 <- as.vector(TT2[ID1])
Ph12 <- as.vector(PH2[ID1])
Pv12 <- as.vector(PV2[ID1])
Trat12 <- as.vector(TRAT2[ID1])


AMAX <-c() ; Aci <- c();Acc <-c();Aci2 <-c()
for (i in 1:length(ID1)){
AMAX <- c(AMAX, ACC(1600,Jmax12[i],Vcmax12[i],Rd12[i],400,Tleaf12[Nf==i][1],Press12[Nf==i][1],Photo12[Nf==i][1],Gm12[i],0,Trat12[i][1])[,1])
Aci <- c(Aci, ACC(1600,Jmax12[i],Vcmax12[i],Rd12[i],CIREF12[i],Tleaf12[Nf==i][1],Press12[Nf==i][1],Photo12[Nf==i][1],Gm12[i],0,Trat12[i][1])[,1])
Acc <- c(Acc, ACC(1600,Jmax12[i],Vcmax12[i],Rd12[i],CIREF12[i],Tleaf12[Nf==i][1],Press12[Nf==i][1],Photo12[Nf==i][1],10^10,0,Trat12[i][1])[,1])
}

Ls12 <- 1-Aci/AMAX
Lm12 <- 1 - Aci/Acc
Lm12 <- replace(Lm12,c(Lm12>1),NA)
Ls12 <- replace(Ls12,c(Ls12>1),NA)

Trat12 <- replace(Trat12,c(Trat12=='C1'),'C32')
Trat12 <- replace(Trat12,c(Trat12=='C3'),'C12')
Trat12 <- replace(Trat12,c(Trat12=='S1'),'S32')
Trat12 <- replace(Trat12,c(Trat12=='S3'),'S12')
Trat12 <- replace(Trat12,c(Trat12=='S12'),'S1')
Trat12 <- replace(Trat12,c(Trat12=='C12'),'C1')
Trat12 <- replace(Trat12,c(Trat12=='S32'),'S3')
Trat12 <- replace(Trat12,c(Trat12=='C32'),'C3')



load("C:\\Users\\mathias\\Documents\\Cours et stage\\thesis\\Exclusion des pluies\\courbes ACI\\sauv_fit_ACI_2013")

RMSE_m1 <- replace(RMSE_m1,c(gmes1<0.1|gmes1>0.5),NA)
ID1 <- c();ID2 <- c();ID3 <- c();ID4<-c()
for (i in 1:69){
Val1 <- min(RMSE_m1[(1+500*(i-1)):(i*500)],na.rm=T)
ID1 <- c(ID1, which(RMSE_m1[(1+500*(i-1)):(i*500)]==Val1)+500*(i-1))
}

Jmax13 <- Jm1[ID1];Vcmax13 <- Vcm1[ID1];Rd13 <- Resp1[ID1];Gm13 <-gmes1[ID1]
Tt13 <- as.vector(TT2[ID1])
Ph13 <- as.vector(PH2[ID1])
Pv13 <- as.vector(PV2[ID1])
Trat13 <- as.vector(TRAT2[ID1])


AMAX <-c() ; Aci <- c();Acc <-c();Aci2 <-c()
for (i in 1:length(ID1)){
AMAX <- c(AMAX, ACC(1600,Jmax13[i],Vcmax13[i],Rd13[i],400,Tleaf13[Nf==i][1],Press13[Nf==i][1],Photo13[Nf==i][1],Gm13[i],0,Trat13[i][1])[,1])
Aci <- c(Aci, ACC(1600,Jmax13[i],Vcmax13[i],Rd13[i],CIREF13[i],Tleaf13[Nf==i][1],Press13[Nf==i][1],Photo13[Nf==i][1],Gm13[i],0,Trat13[i][1])[,1])
Acc <- c(Acc, ACC(1600,Jmax13[i],Vcmax13[i],Rd13[i],CIREF13[i],Tleaf13[Nf==i][1],Press13[Nf==i][1],Photo13[Nf==i][1],10^10,0,Trat13[i][1])[,1])
}

Ls13 <- 1-Aci/AMAX
Lm13 <- 1 - Aci/Acc

Ls <- c(Ls12,Ls13)












