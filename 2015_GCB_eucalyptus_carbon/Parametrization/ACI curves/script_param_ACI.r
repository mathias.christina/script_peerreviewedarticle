set <-c("C:/Users/mathias/Documents/Cours et stage/thesis/Exclusion des pluies/analyse_photosynthetical_parameter")
setwd(set)

datas <- read.table("donn�es_ACi.txt", header=T)
attach(datas)

# vcmax
fit1 <- lm(Vcmax_25~Treatment*Type_Tree*Pos_vert*Pos_hor)
fit2 <- lm(Vcmax_25~Treatment+Type_Tree+Pos_vert+Pos_hor)

library(nlme)
fit3 <- lme(Vcmax_25~Treatment+Pos_vert+Pos_hor, random=~1|num_arbre, na.action=na.omit)

contrasts (Treatment) <- matrix(c(0,1,0,0,0,0,1,0,0,0,0,1), ncol=3)
contrasts (Treatment) <- matrix(c(1,1,-1,-1,0,0,1,-1,1,-1,0,0), ncol=3)
contrasts (Pos_vert) <- matrix(c(0,1,0,0,0,1), ncol=2)
contrasts (Pos_vert) <- matrix(c(2,-1,-1,0,1,-1), ncol=2)

par(mfrow = c(1,3))
par(mar=c(5,5,1,1)+0.1)
par(yaxs="i")
boxplot(Vcmax_25~Treatment, ylab = expression (V[cmax]~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Treatment", cex.lab=1.4,
        ylim=c(0,200))
text('a', font=2, x=1, y=150, cex=1.4)
text('a', font=2, x=2, y=150, cex=1.4)
text('b', font=2, x=3, y=185, cex=1.4)
text('c', font=2, x=4, y=150, cex=1.4)
boxplot(Vcmax_25~Pos_vert, ylab = expression (V[cmax]~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Vertical position within the crown", cex.lab=1.4,
        ylim=c(0,200),names=c('Inferior','Medium','Superior'))
text('a', font=2, x=1, y=150, cex=1.4)
text('b', font=2, x=2, y=185, cex=1.4)
text('b', font=2, x=3, y=185, cex=1.4)
boxplot(Vcmax_25~Pos_hor, ylab = expression (V[cmax]~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Horizontal position within the crown", cex.lab=1.4,
        ylim=c(0,200),names=c('Outer','Inner'))
text('a', font=2, x=1, y=185, cex=1.4)
text('b', font=2, x=2, y=185, cex=1.4)

#J
fit1 <- lm(J_25~Treatment*Type_Tree*Pos_vert*Pos_hor)
fit2 <- lm(J_25~Treatment+Type_Tree+Pos_vert+Pos_hor)
library(nlme)
fit3 <- lme(J_25~Treatment+Pos_vert+Pos_hor, random=~1|num_arbre, na.action=na.omit)
contrasts (Pos_vert) <- matrix(c(0,1,0,0,0,1), ncol=2)
contrasts (Pos_vert) <- matrix(c(2,-1,-1,0,1,-1), ncol=2)
fit4 <- lme(J_25~Pos_vert+Pos_hor, random=~1|num_arbre, na.action=na.omit)

par(mfrow = c(1,2))
par(mar=c(5,5,1,1)+0.1)
par(yaxs="i")
boxplot(J_25~Pos_vert, ylab = expression (J~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Vertical position within the crown", cex.lab=1.4,
        ylim=c(0,200),names=c('Inferior','Medium','Superior'))
text('a', font=2, x=1, y=150, cex=1.4)
text('b', font=2, x=2, y=185, cex=1.4)
text('b', font=2, x=3, y=185, cex=1.4)
boxplot(J_25~Pos_hor, ylab = expression (J~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Horizontal position within the crown", cex.lab=1.4,
        ylim=c(0,200),names=c('Outer','Inner'))
text('a', font=2, x=1, y=185, cex=1.4)
text('b', font=2, x=2, y=185, cex=1.4)

#TPu
fit1 <- lm(TPU_25~Treatment*Type_Tree*Pos_vert*Pos_hor)
fit2 <- lm(TPU_25~Treatment+Type_Tree+Pos_vert+Pos_hor)
library(nlme)
fit3 <- lme(TPU_25~Treatment+Pos_vert+Pos_hor, random=~1|num_arbre, na.action=na.omit)
contrasts (Treatment) <- matrix(c(0,1,0,0,0,0,1,0,0,0,0,1), ncol=3)
contrasts (Treatment) <- matrix(c(1,1,-3,1,2,-1,0,-1,0,-1,0,1), ncol=3)
contrasts (Pos_vert) <- matrix(c(0,1,0,0,0,1), ncol=2)
contrasts (Pos_vert) <- matrix(c(2,-1,-1,0,1,-1), ncol=2)

par(mfrow = c(1,3))
par(mar=c(5,5,1,1)+0.1)
par(yaxs="i")
boxplot(TPU_25~Treatment, ylab = expression ('Triose Phosphate use'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Treatment", cex.lab=1.4,
        ylim=c(2,16))
text('a', font=2, x=1, y=14, cex=1.4)
text('a', font=2, x=2, y=12, cex=1.4)
text('b', font=2, x=3, y=15, cex=1.4)
text('a', font=2, x=4, y=12, cex=1.4)
boxplot(TPU_25~Pos_vert, ylab = expression ('Triose Phosphate use'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Vertical position within the crown", cex.lab=1.4,
        ylim=c(2,16),names=c('Inferior','Medium','Superior'))
text('a', font=2, x=1, y=15, cex=1.4)
text('b', font=2, x=2, y=15, cex=1.4)
text('b', font=2, x=3, y=15, cex=1.4)
boxplot(TPU_25~Pos_hor, ylab = expression ('Triose Phosphate use'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Horizontal position within the crown", cex.lab=1.4,
        ylim=c(2,16),names=c('Outer','Inner'))
text('a', font=2, x=1, y=15, cex=1.4)
text('b', font=2, x=2, y=15, cex=1.4)

#Rd
fit2 <- lm(Rd_25~Treatment+Type_Tree+Pos_vert+Pos_hor)
library(nlme)
fit3 <- lme(Rd_25~Treatment+Pos_vert+Pos_hor, random=~1|num_arbre, na.action=na.omit)
contrasts (Treatment) <- matrix(c(0,1,0,0,0,0,1,0,0,0,0,1), ncol=3)
contrasts (Treatment) <- matrix(c(1,1,-3,1,2,-1,0,-1,0,-1,0,1), ncol=3)
contrasts (Pos_vert) <- matrix(c(0,1,0,0,0,1), ncol=2)
contrasts (Pos_vert) <- matrix(c(-1,-1,2,-1,1,0), ncol=2)

par(mfrow = c(1,3))
par(mar=c(5,5,1,1)+0.1)
par(yaxs="i")
boxplot(Rd_25~Treatment, ylab = expression ('Rd'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Treatment", cex.lab=1.4,
        ylim=c(0,4))
text('a', font=2, x=1, y=3.5, cex=1.4)
text('a', font=2, x=2, y=3.5, cex=1.4)
text('b', font=2, x=3, y=3.5, cex=1.4)
text('a', font=2, x=4, y=3.5, cex=1.4)
boxplot(Rd_25~Pos_vert, ylab = expression ('Rd'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Vertical position within the crown", cex.lab=1.4,
        ylim=c(0,4),names=c('Inferior','Medium','Superior'))
text('a', font=2, x=1, y=3.5, cex=1.4)
text('a', font=2, x=2, y=3.5, cex=1.4)
text('b', font=2, x=3, y=3.7, cex=1.4)
boxplot(Rd_25~Pos_hor, ylab = expression ('Rd'~at~'25�C'~(�mol~m^-2~s^-1)), xlab="Horizontal position within the crown", cex.lab=1.4,
        ylim=c(0,4),names=c('Outer','Inner'))
text('a', font=2, x=1, y=3.7, cex=1.4)
text('b', font=2, x=2, y=3.5, cex=1.4)

#creusons les diff observ�es

fit2 <- lm(Vcmax_25[Treatment==i]~Type_Tree[Treatment==i]+Pos_vert[Treatment==i]+Pos_hor[Treatment==i])

par(mfrow=c(1,2))
par(mar=c(5,5,2,1)+0.1)
par(yaxs='i')
plot(J_25[Treatment=='C3'],Vcmax_25[Treatment=='C3'],ylim=c(20,130),xlim=c(30,130),
      xlab=expression('J at 25�C'~(�mol~m^-2~s^-1)), ylab=expression(V[cmax]~'at 25�C'~(�mol~m^-2~s^-1)),cex.lab=1.2,cex=1.4)
for (i in 1:3){
points(J_25[Treatment=='C3'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='E'],
       Vcmax_25[Treatment=='C3'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='E'],col=i+1,pch=16,ylim=c(20,130),xlim=c(30,130),cex=1.4)
points(J_25[Treatment=='C3'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='I'],
       Vcmax_25[Treatment=='C3'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='I'],col=i+1,pch=1,ylim=c(20,130),xlim=c(30,130),cex=1.4)
}
legend('bottomright',legend=c(paste(levels(Pos_vert)[1], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[1], ' and ',levels(Pos_hor)[2]),
                              paste(levels(Pos_vert)[2], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[2], ' and ',levels(Pos_hor)[2]),
                              paste(levels(Pos_vert)[3], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[3], ' and ',levels(Pos_hor)[2])), 
                    col=c(2,2,3,3,4,4),pch=c(16,1,16,1,16,1),bty='n',cex=1)
legend('topleft',legend="C3", cex=2,bty='n')                              

plot(J_25[Treatment=='C1'],Vcmax_25[Treatment=='C1'],ylim=c(20,130),xlim=c(30,130),
      xlab=expression('J at 25�C'~(�mol~m^-2~s^-1)), ylab=expression(V[cmax]~'at 25�C'~(�mol~m^-2~s^-1)),cex.lab=1.2,cex=1.4)
for (i in 1:3){
points(J_25[Treatment=='C1'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='E'],
       Vcmax_25[Treatment=='C1'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='E'],col=i+1,pch=16,ylim=c(20,130),xlim=c(30,130),cex=1.4)
points(J_25[Treatment=='C1'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='I'],
       Vcmax_25[Treatment=='C1'][Pos_vert==levels(Pos_vert)[i]][Pos_hor=='I'],col=i+1,pch=1,ylim=c(20,130),xlim=c(30,130),cex=1.4)
}
legend('bottomright',legend=c(paste(levels(Pos_vert)[1], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[1], ' and ',levels(Pos_hor)[2]),
                              paste(levels(Pos_vert)[2], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[2], ' and ',levels(Pos_hor)[2]),
                              paste(levels(Pos_vert)[3], ' and ',levels(Pos_hor)[1]),
                              paste(levels(Pos_vert)[3], ' and ',levels(Pos_hor)[2])), 
                    col=c(2,2,3,3,4,4),pch=c(16,1,16,1,16,1),bty='n',cex=1)
legend('topleft',legend="C1", cex=2,bty='n')                              





