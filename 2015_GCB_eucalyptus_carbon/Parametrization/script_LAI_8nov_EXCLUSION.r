set1 <-c("C:/Users/mathias/Documents/Cours et stage/thesis/LAI2000")

setwd(set1)

DatAboveAM <- read.table('above_8nov_AM_EXC.txt')
DatBelowAM <- read.table('below_8nov_AM_EXC.txt')
DatAbovePM <- read.table('above_8nov_PM_EXC.txt')
DatBelowPM <- read.table('below_8nov_PM_EXC.txt')

names(DatAboveAM) <- c('Loca','numa','Hra','Mina','Seca','an1a','an2a','an3a','an4a','an5a')
names(DatBelowAM) <- c('locb','Ty','numb','Hrb','Minb','Secb','an1b','an2b','an3b','an4b','an5b')
names(DatAbovePM) <- c('Loca','numa','Hra','Mina','Seca','an1a','an2a','an3a','an4a','an5a')
names(DatBelowPM) <- c('locb','Ty','numb','Hrb','Minb','Secb','an1b','an2b','an3b','an4b','an5b')

# tout d'abord AM ou PM
DatBelow <- DatBelowPM
DatAbove <- DatAbovePM

# on retire les ? dans DatBelow
DB <- DatBelow[1,]
for (i in 2:dim(DatBelow)[1]){
if( c(DatBelow[i,2]=='?')==FALSE){
DB <- rbind(DB, DatBelow[i,])}}

names(DB) <- c('Locb','Ty','numb','Hrb','Minb','Secb','an1b','an2b','an3b','an4b','an5b')

attach(DatAbove) ; attach(DB)


Houra <- Hra *60*60 + Mina*60 + Seca
Hourb <- Hrb *60*60 + Minb*60 + Secb

# Maintenant on extrapole les valeurs d'interception entre les mesures

HRA <- c(min(Houra):max(Houra))

An1A<-c();An2A<-c();An3A<-c();An4A<-c();An5A<-c();
for (j in 1:(length(Houra)-1)){

k = Houra[j+1]-Houra[j]
Extr1<-c();Extr2<-c();Extr3<-c();Extr4<-c();Extr5<-c();

for (i in 1:(k-1)){
Extr1 <- c(Extr1, an1a[j]+ (an1a[j+1]-an1a[j])/k*i)
Extr2 <- c(Extr2, an2a[j]+ (an2a[j+1]-an2a[j])/k*i)
Extr3 <- c(Extr3, an3a[j]+ (an3a[j+1]-an3a[j])/k*i)
Extr4 <- c(Extr4, an4a[j]+ (an4a[j+1]-an4a[j])/k*i)
Extr5 <- c(Extr5, an5a[j]+ (an5a[j+1]-an5a[j])/k*i)
}
An1A <- c(An1A, an1a[j], Extr1)
An2A <- c(An2A, an2a[j], Extr2)
An3A <- c(An3A, an3a[j], Extr3)
An4A <- c(An4A, an4a[j], Extr4)
An5A <- c(An5A, an5a[j], Extr5)
}
HRA <- HRA[1:(length(HRA)-1)]

par(mar=c(4.5,4.5,1,1))
plot(HRA,An1A,col=2,type='l',lwd=2,ylim=c(40,120),
     ylab='Sensor Above canopy',xlab='Time of the day (s)',cex.lab=1.2)
points(HRA,An2A,col=3,type='l',lwd=2)
points(HRA,An3A,col=4,type='l',lwd=2)
points(HRA,An4A,col=5,type='l',lwd=2)
points(HRA,An5A,col=6,type='l',lwd=2)
legend('topright',legend=c('ring 1: 0-13�','ring 2: 16-28�','ring 3: 32-43�','ring 4: 47-58�','ring 5: 61-74�'),
        col=c(2:6),pch=rep(16,5),bty='n')

for (i in 1:length(Hourb)){ abline(v=Hourb[i])}

# on calibre

CalB <- data.frame(Hourb[Locb=='Above'],an1b[Locb=='Above'],an2b[Locb=='Above'],an3b[Locb=='Above'],an4b[Locb=='Above'],an5b[Locb=='Above'])

CalA <- c(An1A[HRA==Hourb[Locb=='Above'][1]],An2A[HRA==Hourb[Locb=='Above'][1]],
                   An3A[HRA==Hourb[Locb=='Above'][1]],An4A[HRA==Hourb[Locb=='Above'][1]],
                   An5A[HRA==Hourb[Locb=='Above'][1]])
for (i in 2:length(Hourb[Locb=='Above'])){
X <- c(An1A[HRA==Hourb[Locb=='Above'][i]],An2A[HRA==Hourb[Locb=='Above'][i]],
                   An3A[HRA==Hourb[Locb=='Above'][i]],An4A[HRA==Hourb[Locb=='Above'][i]],
                   An5A[HRA==Hourb[Locb=='Above'][i]])
CalA <- rbind(CalA,X)}

par(mar=c(4.5,4.5,1.5,1))
plot(CalA[,1],CalB[,2],pch=16,col=2,
                        ylim=c(min(CalB[,2:6]),max(CalB[,2:6])),xlim=c(min(CalA),max(CalA)),
                        ylab='Sensor "below"',xlab='Sensor "above"',cex.lab=1.2)
for (i in 2:5){
points(CalA[,i],CalB[,(i+1)],pch=16,col=i+1)}
legend('bottomright',legend=c('ring 1: 0-13�','ring 2: 16-28�','ring 3: 32-43�','ring 4: 47-58�','ring 5: 61-74�'),
        col=c(2:6),pch=rep(16,5),bty='n')
title('Calibration')
fit1 <- lm(CalB[,2]~CalA[,1]-1)
fit2 <- lm(CalB[,3]~CalA[,2]-1)
fit3 <- lm(CalB[,4]~CalA[,3]-1)
fit4 <- lm(CalB[,5]~CalA[,4]-1)
fit5 <- lm(CalB[,6]~CalA[,5]-1)
abline(fit1,col=2)
abline(fit2,col=3)
abline(fit3,col=4)
abline(fit4,col=5)
abline(fit5,col=6)



# calcul des gap fraction

#TyB <- c(rep('C1',33),rep('C3',36),rep('S1',36),rep('S3',36))
#HrB <- c(Hourb[Locb=='C1'],Hourb[Locb=='C3'],Hourb[Locb=='S1'],Hourb[Locb=='S3'])
#Ab1 <- c(an1b[Locb=='C1']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],an1b[Locb=='C3']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],
#         an1b[Locb=='S1']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1],an1b[Locb=='S3']*summary(fit1)$coef[2,1]+summary(fit1)$coef[1,1])
#Ab2 <- c(an2b[Locb=='C1']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],an2b[Locb=='C3']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],
#         an2b[Locb=='S1']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1],an2b[Locb=='S3']*summary(fit2)$coef[2,1]+summary(fit2)$coef[1,1])
#Ab3 <- c(an3b[Locb=='C1']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],an3b[Locb=='C3']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],
#         an3b[Locb=='S1']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1],an3b[Locb=='S3']*summary(fit3)$coef[2,1]+summary(fit3)$coef[1,1])
#Ab4 <- c(an4b[Locb=='C1']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],an4b[Locb=='C3']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],
#         an4b[Locb=='S1']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1],an4b[Locb=='S3']*summary(fit4)$coef[2,1]+summary(fit4)$coef[1,1])
#Ab5 <- c(an5b[Locb=='C1']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],an5b[Locb=='C3']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],
#         an5b[Locb=='S1']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1],an5b[Locb=='S3']*summary(fit5)$coef[2,1]+summary(fit5)$coef[1,1])
TyB <- c(rep('C1',length(Ty[Locb=='C1'])),rep('C3',length(Ty[Locb=='C3'])),rep('S1',length(Ty[Locb=='S1'])),rep('S3',length(Ty[Locb=='S3'])))
HrB <- c(Hourb[Locb=='C1'],Hourb[Locb=='C3'],Hourb[Locb=='S1'],Hourb[Locb=='S3'])
Ab1 <- c(an1b[Locb=='C1']*summary(fit1)$coef[1],an1b[Locb=='C3']*summary(fit1)$coef[1],
         an1b[Locb=='S1']*summary(fit1)$coef[1],an1b[Locb=='S3']*summary(fit1)$coef[1])
Ab2 <- c(an2b[Locb=='C1']*summary(fit2)$coef[1],an2b[Locb=='C3']*summary(fit2)$coef[1],
         an2b[Locb=='S1']*summary(fit2)$coef[1],an2b[Locb=='S3']*summary(fit2)$coef[1])
Ab3 <- c(an3b[Locb=='C1']*summary(fit3)$coef[1],an3b[Locb=='C3']*summary(fit3)$coef[1],
         an3b[Locb=='S1']*summary(fit3)$coef[1],an3b[Locb=='S3']*summary(fit3)$coef[1])
Ab4 <- c(an4b[Locb=='C1']*summary(fit4)$coef[1],an4b[Locb=='C3']*summary(fit4)$coef[1],
         an4b[Locb=='S1']*summary(fit4)$coef[1],an4b[Locb=='S3']*summary(fit4)$coef[1])
Ab5 <- c(an5b[Locb=='C1']*summary(fit5)$coef[1],an5b[Locb=='C3']*summary(fit5)$coef[1],
         an5b[Locb=='S1']*summary(fit5)$coef[1],an5b[Locb=='S3']*summary(fit5)$coef[1])

Aa1<-c();Aa2<-c();Aa3<-c();Aa4<-c();Aa5<-c()
for (i in 1:length(HrB)){
Aa1 <- c(Aa1, An1A[HRA==HrB[i]])
Aa2 <- c(Aa2, An2A[HRA==HrB[i]])
Aa3 <- c(Aa3, An3A[HRA==HrB[i]])
Aa4 <- c(Aa4, An4A[HRA==HrB[i]])
Aa5 <- c(Aa5, An5A[HRA==HrB[i]])
}

par(mfrow=c(2,3))
par(mar=c(4.5,4.5,2,1))
par(xaxs='i',yaxs='i')
plot(Ab1/Aa1,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,1),cex.lab=1.2)
abline(v=33.5) ; abline(v=33.5+36) ; abline(v=33.5+2*36)
text(x=17,y=0.9,'C1',cex=1.2)
text(x=36+17,y=0.9,'C3',cex=1.2)
text(x=72+17,y=0.9,'S1',cex=1.2)
text(x=108+17,y=0.9,'S3',cex=1.2)
title('7�')
plot(Ab2/Aa2,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.8),cex.lab=1.2)
abline(v=33.5) ; abline(v=33.5+36) ; abline(v=33.5+2*36)
text(x=17,y=0.72,'C1',cex=1.2)
text(x=36+17,y=0.72,'C3',cex=1.2)
text(x=72+17,y=0.72,'S1',cex=1.2)
text(x=108+17,y=0.72,'S3',cex=1.2)
title('23�')
plot(Ab3/Aa3,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.6),cex.lab=1.2)
abline(v=33.5) ; abline(v=33.5+36) ; abline(v=33.5+2*36)
text(x=17,y=0.54,'C1',cex=1.2)
text(x=36+17,y=0.54,'C3',cex=1.2)
text(x=72+17,y=0.54,'S1',cex=1.2)
text(x=108+17,y=0.54,'S3',cex=1.2)
title('38�')
plot(Ab4/Aa4,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.4),cex.lab=1.2)
abline(v=33.5) ; abline(v=33.5+36) ; abline(v=33.5+2*36)
text(x=17,y=0.36,'C1',cex=1.2)
text(x=36+17,y=0.36,'C3',cex=1.2)
text(x=72+17,y=0.36,'S1',cex=1.2)
text(x=108+17,y=0.36,'S3',cex=1.2)
title('53�')
plot(Ab5/Aa5,ylab='Gap Fraction',xlab='Num mesure',ylim=c(0,0.2),cex.lab=1.2)
abline(v=33.5) ; abline(v=33.5+36) ; abline(v=33.5+2*36)
text(x=17,y=0.18,'C1',cex=1.2)
text(x=36+17,y=0.18,'C3',cex=1.2)
text(x=72+17,y=0.18,'S1',cex=1.2)
text(x=108+17,y=0.18,'S3',cex=1.2)
title('68�')

GF <- c(sum(Ab1[TyB=='C1'])/sum(Aa1[TyB=='C1']),sum(Ab2[TyB=='C1'])/sum(Aa2[TyB=='C1']),sum(Ab3[TyB=='C1'])/sum(Aa3[TyB=='C1']),
        sum(Ab4[TyB=='C1'])/sum(Aa4[TyB=='C1']),sum(Ab5[TyB=='C1'])/sum(Aa5[TyB=='C1']),
        sum(Ab1[TyB=='C3'])/sum(Aa1[TyB=='C3']),sum(Ab2[TyB=='C3'])/sum(Aa2[TyB=='C3']),sum(Ab3[TyB=='C3'])/sum(Aa3[TyB=='C3']),
        sum(Ab4[TyB=='C3'])/sum(Aa4[TyB=='C3']),sum(Ab5[TyB=='C3'])/sum(Aa5[TyB=='C3']),
        sum(Ab1[TyB=='S1'])/sum(Aa1[TyB=='S1']),sum(Ab2[TyB=='S1'])/sum(Aa2[TyB=='S1']),sum(Ab3[TyB=='S1'])/sum(Aa3[TyB=='S1']),
        sum(Ab4[TyB=='S1'])/sum(Aa4[TyB=='S1']),sum(Ab5[TyB=='S1'])/sum(Aa5[TyB=='S1']),
        sum(Ab1[TyB=='S3'],na.rm=T)/sum(Aa1[TyB=='S3'],na.rm=T),sum(Ab2[TyB=='S3'],na.rm=T)/sum(Aa2[TyB=='S3'],na.rm=T),sum(Ab3[TyB=='S3'],na.rm=T)/sum(Aa3[TyB=='S3'],na.rm=T),
        sum(Ab4[TyB=='S3'],na.rm=T)/sum(Aa4[TyB=='S3'],na.rm=T),sum(Ab5[TyB=='S3'],na.rm=T)/sum(Aa5[TyB=='S3'],na.rm=T))
TyGF <- c(rep('C1',5),rep('C3',5),rep('S1',5),rep('S3',5))
Ang <- rep(c(7,23,38,53,68),4)

par(mar=c(4.5,4.5,2,1))
par(xaxs='i',yaxs='i')
plot(Ang[TyGF=='C1'],GF[TyGF=='C1'],ylab='Mean Gap Fraction',xlab='View angle (�)',
          ylim=c(0,0.6),xlim=c(0,90),cex.lab=1.2,type='b',pch=16,lty=1,col=2)
points(Ang[TyGF=='C3'],GF[TyGF=='C3'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=3)
points(Ang[TyGF=='S1'],GF[TyGF=='S1'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=4)
points(Ang[TyGF=='S3'],GF[TyGF=='S3'],
          cex.lab=1.2,type='b',pch=16,lty=1,col=5)
legend('topright',legend=c('C1','C3','S1','S3'),col=c(2:5),pch=rep(16,4),lty=rep(1,4),bty='n')



Aa <- c(Aa1,Aa2,Aa3,Aa4,Aa5)
Ab <- c(Ab1,Ab2,Ab3,Ab4,Ab5)                                           
TRAT <- rep(TyB,5)
ANG <- c(rep(7,length(Aa1)),rep(23,length(Aa1)),rep(38,length(Aa1)),rep(53,length(Aa1)),rep(68,length(Aa1)))

MAT <- data.frame(TRAT,ANG,Aa,Ab)

write.table(MAT,'MeasTMP08112012.txt',row.names=F,col.names=T)




